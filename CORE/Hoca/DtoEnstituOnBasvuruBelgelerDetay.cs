﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sabis.Bolum.Core.BASVURU
{
    public class DtoEnstituOnBasvuruBelgelerDetay
    {
        public int ID { get; set; }
        public string DosyaAdi { get; set; }
        public string DosyaUrl { get; set; }
        public string DosyaTipi { get; set; }
        public string EklenmeTarihi { get; set; }
        public string OnayDurumu { get; set; }
        public string TCKimlikNo { get; set; }
        public string AdSoyad { get; set; }
    }
}