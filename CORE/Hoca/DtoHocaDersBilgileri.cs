﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sabis.Bolum.Core.Hoca
{
    public class DtoHocaDersBilgileri
    {
        public List<Sabis.Bolum.Core.ABS.DERS.HOME.HocaDersListesiViewModelv2> DersListesi { get; set; }
        public int PersonelID { get; set; }
        public string AdSoyad { get; set; }
        public string UnvanAd { get; set; }
    }
}