﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sabis.Bolum.Core.Hoca
{
    public class DtoPersonelBilgisi
    {
        public int ID { get; set; }
        public string TC { get; set; }
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public string Unvan { get; set; }
        public int BolumID { get; set; }
        public int FakulteID { get; set; }
        public string KullaniciAdi { get; set; }
        
    }
}