﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sabis.Bolum.Core.Hoca
{
    public class DtoHocaDersOgrenciBilgileri
    {
        public List<dto.Ogrenci.OgrenciBilgiDto> OgrenciListesi { get; set; }
        public int DersGrupID { get; set; }
        public int DersGrupHocaID { get; set; }
        public string DersKod { get; set; }
        public string DersAd { get; set; }
        public string GrupAd { get; set; }
        public int PersonelID { get; set; }
        public string Unvan { get; set; }
        public string AdSoyad { get; set; }
    }
}