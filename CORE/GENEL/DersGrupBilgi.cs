﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.GENEL
{
    public class DersGrupBilgi
    {
        public int FKBirimID { get; set; }
        public int? FKDersPlanID { get; set; }
        public int FKDersGrupID { get; set; }
        public string BirimAd { get; set; }
        public string DersAd { get; set; }
        public string GrupAd { get; set; }
        public string HocaAd { get; set; }
    }

    public class SanalSinifRapor
    {
        public int ID { get; set; }
        public int? PID { get; set; }
        public int? RoomID { get; set; }
        public string Ad { get; set; }
        public string Adres { get; set; }
        public int Hafta { get; set; }
        public int? AktifSure { get; set; }

    }
}
