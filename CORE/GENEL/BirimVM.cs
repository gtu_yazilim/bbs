﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.GENEL
{
    public class BirimVM
    {
        public int ID { get; set; }
        public string Ad { get; set; }
        public int? EnumBirimTuru { get; set; }
        public string BirimAdiUzun { get; set; }
        public int? UstBirimID { get; set; }
        public int? EnumOgretimTur { get; set; }
        public int? FKDersPlanID { get; set; }
    }
}
