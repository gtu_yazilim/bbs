﻿using Sabis.Bolum.Core.ABS.DERS.SINAV;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.GENEL
{
    public class GeriBildirimVM
    {
        public int FKDersPlanID { get; set; }
        public int FKBirimID { get; set; }
        public string DersAd { get; set; }
        public string BirimAd { get; set; }
        public int ModulTip { get; set; }
    }
    public class GeriBildirimSinav
    {
        public int SinavID { get; set; }
        public int FKDersGrupID { get; set; }
        public string GrupAd { get; set; }
        public string HocaAd { get; set; }
        public string SinavAd { get; set; }
        public int GeriBildirimID { get; set; }
        public string BildirimBaslik { get; set; }
        public string BildirimIcerik { get; set; }
        public string BildirimDosya { get; set; }
        public string BildirimCevap { get; set; }
        public DateTime BildirimTarih { get; set; }
        public int BildirimDurum { get; set; }
        public int FKOgrenciID { get; set; }
        public string OgrenciAd { get; set; }
        public string OgrenciSoyad { get; set; }
        public string OgrenciNumara { get; set; }

        public int OturumDurum { get; set; }

        public string OturumunDurumu { get; set; }
        public DateTime? TarihOturumBaslangic { get; set; }
        public DateTime? TarihOturumBitis { get; set; }
        public DateTime? TarihMazeretOturumBaslangic { get; set; }
        public DateTime? TarihMazeretOturumBitis { get; set; }
        public int SoruSayisi { get; set; }
        public int DogruSayisi { get; set; }
        public int YanlisSayisi { get; set; }
        public int MazeretDogruSayisi { get; set; }
        public int MazeretYanlisSayisi { get; set; }
        public int BosSayisi { get; set; }
        public int MazeretBosSayisi { get; set; }
        //public int? Notu { get; set; }
        public double? Notu { get; set; }
        //public int? MazeretNotu { get; set; }
        public double? MazeretNotu { get; set; }

        public List<OturumDetayDto> oturumdetay { get; set; }
    }

    public class GeriBildirimOdev
    {
        public int OdevID { get; set; }
        public int FKDersGrupID { get; set; }
        public string GrupAd { get; set; }
        public string HocaAd { get; set; }
        public string OdevAd { get; set; }
        public string OdevAciklama { get; set; }
        public DateTime? BaslangicTarihi { get; set; }
        public DateTime? BitisTarihi { get; set; }
        public bool OdevOnline { get; set; }
        public bool OdevGenel { get; set; }
        public DateTime OdevYuklemeTarih { get; set; }
        public string OdevDosya { get; set; }
        public string OdevOgrenciDosya { get; set; }
        public string OdevDosyaTuru { get; set; }
        public int GeriBildirimID { get; set; }
        public string BildirimBaslik { get; set; }
        public string BildirimIcerik { get; set; }
        public string BildirimDosya { get; set; }
        public string BildirimCevap { get; set; }
        public DateTime BildirimTarih { get; set; }
        public int BildirimDurum { get; set; }
        public int FKOgrenciID { get; set; }
        public string OgrenciAd { get; set; }
        public string OgrenciSoyad { get; set; }
        public string OgrenciNumara { get; set; }
        public double OdevNotu { get; set; }
        public string OdevNotuAciklamasi { get; set; }
    }

    public class GeriBildirimSanalSinif
    {
        public int SanalSinifID { get; set; }
        public int? RoomID { get; set; }
        public int? SunucuID { get; set; }
        public int? AktifSure { get; set; }
        public int FKDersGrupID { get; set; }
        public string GrupAd { get; set; }
        public string HocaAd { get; set; }
        public string SanalSinifAd { get; set; }
        public DateTime SanalSinifBaslangic { get; set; }
        public DateTime SanalSinifBitis { get; set; }
        public string OdevAciklama { get; set; }
        public DateTime OdevYuklemeTarih { get; set; }
        public string OdevDosya { get; set; }
        public string OdevOgrenciDosya { get; set; }
        public string OdevDosyaTuru { get; set; }
        public int GeriBildirimID { get; set; }
        public string BildirimBaslik { get; set; }
        public string BildirimIcerik { get; set; }
        public string BildirimDosya { get; set; }
        public string BildirimCevap { get; set; }
        public DateTime BildirimTarih { get; set; }
        public int BildirimDurum { get; set; }
        public int FKOgrenciID { get; set; }
        public string OgrenciAd { get; set; }
        public string OgrenciSoyad { get; set; }
        public string OgrenciNumara { get; set; }
        public string OgrenciSanalSinifAdres { get; set; }
    }


}
