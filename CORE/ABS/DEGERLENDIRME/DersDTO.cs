﻿using Sabis.Core;
using Sabis.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DEGERLENDIRME
{
    public class DersDTO
    {
        public int DersID { get; set; }
        public string DersAd { get; set; }
        public int? OgretimTuru { get; set; }
        public string OgretimTuruAd { get; set; }
        public int Donem { get; set; }
        public int OgrenciSayisi { get; set; }
        public int DersPlanAnaID { get; set; }
        public string DersGrupAd { get; set; }
        public int Yil { get; set; }
        public double? Kredi { get; set; }
        public int? DSaat { get; set; }
        public int? USaat { get; set; }
        public int? EnumZorunluDers { get; set; }
        public int? DersGrupID { get; set; }
        public string DersKod { get; set; }
        public int? Yariyil { get; set; }
        public int? DersPlanID { get; set; }
        public DtoBirimler Fakulte { get; set; }
        public DtoBirimler Bolum { get; set; }
        public DateTime? FinalTarihi { get; set; }
        public EnumSonHal SonHal { get; set; }
        public EnumDersPlanOrtalama EnumDersPlanOrtalama { get; set; }
        public int? EnumWebGosterim { get; set; }
        public IEnumerable<int> GrupCalismaOgrenciIDleri { get; set; }
        public PersonelDTO Personel { get; set; }
        public bool ButunlemeMi { get; set; }
        public bool ButunlemeMiOrijinal { get; set; }
        public EnumDegerlendirmeTipi EnumDegerlendirmeTipi { get; set; }
        public DersDTO()
        {
            Bolum = new DtoBirimler();
            Fakulte = new DtoBirimler();
        }

        public int? EnumSeviye { get; set; }

        public int? EnumOgrenimTur { get; set; }
        public int OgrenciSayisiButunleme { get; set; }
        public bool Hiyerarsik { get; set; }
        public EnumDersSureTip EnumDersSureTip { get; set; }
        public int? EnumKokDersTipi { get; set; }
    }
}
