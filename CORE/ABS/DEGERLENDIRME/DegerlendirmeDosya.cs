﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DEGERLENDIRME
{
    public class DegerlendirmeDosya
    {
        public string DosyaKey { get; set; }
        public DateTime Tarih { get; set; }

    }
}
