﻿using Sabis.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DEGERLENDIRME
{
    public class BasariListesiDTO
    {
        public DtoBirimler Fakulte { get; set; }
        public DtoBirimler Bolum { get; set; }
        public DersDTO Ders { get; set; }
        public List<OgrenciPayveNotBilgileriDTO> NotList { get; set; }
        public double AritmetikOrtalama { get; set; }
        public double StandartSapma { get; set; }
        public string Barkod { get; set; }
        public DateTime SonHalTarihi { get; set; }
        public int? EBYSDocTreeID { get; set; }
    }
}
