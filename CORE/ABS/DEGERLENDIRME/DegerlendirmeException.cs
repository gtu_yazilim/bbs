﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DEGERLENDIRME
{
    [Serializable]
    public class DegerlendirmeException : System.Exception
    {
        public DegerlendirmeException()
        {
            //put your custom code here
        }

        public DegerlendirmeException(string message)
            : base(message)
        {
            //put your custom code here
        }

        public DegerlendirmeException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
        protected DegerlendirmeException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            //put your custom code here
        }
    }
}
