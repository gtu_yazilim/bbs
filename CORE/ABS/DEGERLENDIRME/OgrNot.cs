﻿using Sabis.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DEGERLENDIRME
{
    public class OgrNot
    {
        /// <summary>
        /// Öğrenci ID
        /// </summary>
        public int OgrID { get; set; }
        
        public string AdSoyad { get; set; }

        public string Numara { get; set; }
        /// <summary>
        /// Öğrencinin bulunduğu grup bilgisini tutar (Ortak değerlendirme için şart)
        /// </summary>
        public int GrupID { get; set; }
        /// <summary>
        /// Ham Not
        /// </summary>
        public double Not { get; set; }
        /// <summary>
        /// Bağıl hesaplama sonucu hesaplanan not
        /// </summary>
        public double BasariNot { get; set; }
        /// <summary>
        /// Bağıl hesaplama sonucu hesaplanan harf notu
        /// </summary>
        public EnumHarfBasari BasariHarf { get; set; }
        /// <summary>
        /// Ham harf notu
        /// </summary>
        public EnumHarfMutlak MutlakNot { get; set; }

        public bool ButunlemeMi { get; set; }
        public double NotFinal { get; set; }
        public double NotYilIci { get; set; }
        public int DersPlanAnaID { get; set; }
        public int? DersPlanID { get; set; }
    }
}
