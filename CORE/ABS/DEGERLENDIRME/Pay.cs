﻿using Sabis.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DEGERLENDIRME
{
    public class GPay : IPay
    {
        public int PayID { get; set; }
        public EnumCalismaTip EnumCalismaTip { get; set; }
        public byte Sira { get; set; }
        public byte? Oran { get; set; }
        public int? Pay { get; set; }
        public int? Payda { get; set; }
        public double Yuzde { get; set; }
        public int DersPlanAnaID { get; set; }
        public int Yil { get; set; }
        public int? EnumDonem { get; set; }
        public int? DersPlanID { get; set; }
        public int? DersGrupID { get; set; }
    }
}
