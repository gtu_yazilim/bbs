﻿using Sabis.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DEGERLENDIRME.Hiyerarsik
{
    public class HPay : IPay
    {
        public int PayID { get; set; }
        public EnumCalismaTip EnumCalismaTip { get; set; }
        public int DersPlanAnaID { get; set; }
        public byte Sira { get; set; }
        public byte? Oran { get; set; }
        public int? Pay { get; set; }
        public int? Payda { get; set; }
        public double Yuzde { get; set; }
        public int Yil { get; set; }
        public virtual List<HPay> AltHPayList { get; set; }
        public List<HPay> GetirAltPaylarLineer(bool tumu = false)
        {
            List<HPay> result = new List<HPay>();
            if (!tumu)
            {
                if (!(EnumCalismaTip == EnumCalismaTip.AltDersinFinalKatkisi || EnumCalismaTip == EnumCalismaTip.AltDersinYiliciKatkisi || EnumCalismaTip == EnumCalismaTip.YilIcininBasariya))
                {
                    result.Add(this);
                }
            }
            else
            {
                result.Add(this);
            }
            if (AltHPayList != null)
            {
                foreach (var item in AltHPayList)
                {
                    result.AddRange(item.GetirAltPaylarLineer());
                }
            }
            return result;
        }
    }
}
