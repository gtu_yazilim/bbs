﻿using Sabis.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DEGERLENDIRME.Hiyerarsik
{
    public class HOgrenciSonuc : OgrenciSonuc
    {
        public List<HPayOgrenciNot> listPaylarVeNotlar { get; set; }

        public override string ToString()
        {
            string payVeNotlari = "";
            if (listPaylarVeNotlar != null)
            {
                foreach (var payNot in listPaylarVeNotlar)
                {
                    payVeNotlari += string.Format("Çalışma Tip={0}, Notu={1}, OgrenciNotID={2},Oran={3}", payNot.EnumCalismaTip, payNot.NotDeger.ToString(), payNot.NotID.ToString(), payNot.Oran);
                }
            }
            string str = string.Format("Numara={0},AdSoyad={1},OgrenciID={2},DersPlanAnaID={3},PersonelID={4},MutlakNot={5},PayVeNotlari={6}", Numara, AdSoyad, OgrenciID, DersPlanAnaID, PersonelID, MutlakNot, payVeNotlari);
            return str;
        }
    }
}
