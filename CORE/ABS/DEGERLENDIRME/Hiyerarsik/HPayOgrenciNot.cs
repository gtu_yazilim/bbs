﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DEGERLENDIRME.Hiyerarsik
{
    public class HPayOgrenciNot : HPay
    {
        public int OgrenciID { get; set; }
        public int? NotID { get; set; }
        public double? NotDeger { get; set; }
        public new List<HPayOgrenciNot> AltHPayList { get; set; }
        public HPayOgrenciNot(HPay pay)
        {
            this.DersPlanAnaID = pay.DersPlanAnaID;
            this.EnumCalismaTip = pay.EnumCalismaTip;
            this.Oran = pay.Oran;
            this.Pay = pay.Pay;
            this.Payda = pay.Payda;
            this.PayID = pay.PayID;
            this.Sira = pay.Sira;
            this.Yuzde = pay.Yuzde;
        }
        public HPayOgrenciNot()
        { }
        public bool NotlarinTumuGirilmisMi(bool tipMi = false)
        {
            bool result = false;
            if (this.EnumCalismaTip == Sabis.Enum.EnumCalismaTip.AltDersinFinalKatkisi || this.EnumCalismaTip == Sabis.Enum.EnumCalismaTip.AltDersinYiliciKatkisi || this.EnumCalismaTip == Sabis.Enum.EnumCalismaTip.AltDersinKatkisi || this.EnumCalismaTip == Sabis.Enum.EnumCalismaTip.YilIcininBasariya)
            {
                result = true;
            }
            else
            {
                if (tipMi)
                {
                    //TODO geçici #sil
#if DEBUG
                    if (!NotDeger.HasValue && !(EnumCalismaTip == Sabis.Enum.EnumCalismaTip.Final || EnumCalismaTip == Sabis.Enum.EnumCalismaTip.Butunleme))
                    {
                        NotDeger = 120;
                    }
#endif
                }

                result = NotDeger.HasValue;
                if ((EnumCalismaTip == Sabis.Enum.EnumCalismaTip.Final || EnumCalismaTip == Sabis.Enum.EnumCalismaTip.Butunleme) && tipMi)
                {
                    result = true;
                }
            }

            if (result)
            {
                foreach (var item in AltHPayList)
                {
                    result = item.NotlarinTumuGirilmisMi(tipMi);
                    if (!result)
                    {
                        return result;
                    }
                }
            }
            return result;
        }
    }
}
