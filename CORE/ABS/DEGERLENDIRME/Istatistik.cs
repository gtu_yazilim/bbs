﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DEGERLENDIRME
{
    public class Istatistik
    {
        public string Harf { get; set; }
        public int EnumHarf { get; set; }
        public int Adet { get; set; }
        public double Yuzde { get; set; }
    }
}
