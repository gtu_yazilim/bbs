﻿using Sabis.Core;
using Sabis.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DEGERLENDIRME
{
    public class Grup
    {
        public int DersGrupID { get; set; }
        public string Ad { get; set; }
        public DtoBirimler Bolum { get; set; }
        public DtoBirimler Fakulte { get; set; }
        public EnumOgretimTur EnumOgretimTur { get; set; }
        public int OgrenciSayisi { get; set; }
        public int DersPlanAnaID { get; set; }
        public List<PersonelDTO> PersonelList { get; set; }
    }
}
