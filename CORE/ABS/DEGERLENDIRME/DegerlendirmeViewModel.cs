﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DEGERLENDIRME
{
    public class DegerlendirmeViewModel
    {
        public CiktiDTO DegerlendirmeCikti { get; set; }
        public DersDTO Ders { get; set; }
        public Sabis.Core.DTO.AkademikTakvim AkademikTakvim { get; set; }
        public List<Notification> NotificationList { get; set; }
        public List<Grup> OrtakGruplar { get; set; }
        public EnumDegerlendirmeAsama EnumDegerlendirmeAsama { get; set; }
        public bool SonHalKaldirmaYetkisiVarMi { get; set; }
        public bool TarihAraligiUygunMu { get; set; }
        public bool ButunlemeMi { get; set; }
        public int IslemID { get; set; }
        public bool DegerlendiriciMi { get; set; }
    }
}
