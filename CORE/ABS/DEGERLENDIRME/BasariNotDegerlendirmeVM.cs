﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DEGERLENDIRME
{
    public class BasariNotDegerlendirmeVM
    {
        public int ID { get; set; }
        public Nullable<int> FKDersGrupID { get; set; }
        public Nullable<double> StandartSapma { get; set; }
        public Nullable<double> Ortalama { get; set; }
        public Nullable<double> EnBuyukNot { get; set; }
        public Nullable<System.DateTime> Tarih { get; set; }
        public Nullable<int> FKPersonelID { get; set; }
        public string SicilNo { get; set; }
        public Nullable<int> EnumSonHal { get; set; }
        public Nullable<System.DateTime> Zaman { get; set; }
        public string Kullanici { get; set; }
        public string Makina { get; set; }
        public Nullable<bool> AktarilanVeriMi { get; set; }
        public string BarkodNo { get; set; }
        public Nullable<System.Guid> OrtakDegerlendirmeGUID { get; set; }
        public Nullable<double> UstDeger { get; set; }
        public Nullable<double> BagilOrtalama { get; set; }
        public Nullable<int> Yil { get; set; }
        public Nullable<int> EnumDonem { get; set; }
        public Nullable<System.DateTime> AktarimTarihi { get; set; }
        public bool ButunlemeMi { get; set; }
        public int? EBYSDocTreeID { get; set; }
    }
}
