﻿using Sabis.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DEGERLENDIRME
{
    public class PaylarVeNotlarDTO
    {
        public int PayID { get; set; }

        public byte Sira { get; set; }

        public bool Aktif { get; set; }

        public EnumCalismaTip EnumCalismaTip { get; set; }

        public string Notu { get; set; }
        public double? NotDeger { get; set; }

        public int? OgrenciNotID { get; set; }
        public double Yuzde { get; set; }

    }
}
