﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DEGERLENDIRME
{
    public class Notification
    {
        public enum MasterMesajTip { Hata = 0, Uyari = 1, Bilgi = 2, Basari = 3 }
        public Notification(string _baslik, string _mesaj, MasterMesajTip _mesajTipi)
        {
            Baslik = _baslik;
            Mesaj = _mesaj;
            MesajTipi = _mesajTipi;
            SabitMi = true;
        }
        public string Baslik { get; set; }
        public string Mesaj { get; set; }
        public MasterMesajTip MesajTipi { get; set; }
        public bool SabitMi { get; set; }
    }
}
