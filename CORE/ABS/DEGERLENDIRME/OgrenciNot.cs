﻿using Sabis.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DEGERLENDIRME
{
    public class OgrenciNot
    {
        public int PayID { get; set; }
        public GPay Pay { get; set; }
        public double Not { get; set; }
        public EnumDonem EnumDonem { get; set; }

        private int enumDonemInt;
        public int EnumDonemInt
        {
            get { return enumDonemInt; }
            set { enumDonemInt = value; EnumDonem = ((EnumDonem)value); }
        }
    }
}
