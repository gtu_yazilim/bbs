﻿using Sabis.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DEGERLENDIRME
{
    public interface IPay
    {
        int PayID { get; set; }
        EnumCalismaTip EnumCalismaTip { get; set; }
        int DersPlanAnaID { get; set; }
        byte Sira { get; set; }
        byte? Oran { get; set; }
        int? Pay { get; set; }
        int? Payda { get; set; }
        double Yuzde { get; set; }
        int Yil { get; set; }
    }
}
