﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DEGERLENDIRME
{
    public enum EnumDegerlendirmeAsama
    {
        [Description("Değerlendirme Yapılmamış")]
        BOS = 0,
        [Description("Bağıl Ortalamalar Hesaplandı")]
        KAYITLI = 1,
        [Description("Notlar Yayınlandı")]
        YAYINLANDI = 2,
        [Description("Son Hal Verildi")]
        SONHAL = 3,
        [Description("Aktarım Tamamlandı")]
        AKTARILDI = 4,
        [Description("Hatalı Değerlendirme!")]
        HATALI = 9
    }

    public enum EnumYetkiDuzeyi
    {
        STANDART = 0,
        YETKILI = 1,
        YONETICI = 2
    }
}
