﻿using Sabis.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DEGERLENDIRME
{
    public enum OgrenciSinif
    {
        Lisans = 0,
        YuksekLisans = 1,
        Doktora = 2
    }
    public class CiktiDTO
    {
        /// <summary>
        /// Öğrenci Not Listesi
        /// </summary>
        public List<OgrNot> NotList { get; set; }
        /// <summary>
        /// Bağıl değeri hesaplamak için gerekli üst değer
        /// </summary>
        public double UstDeger { get; set; }

        /// <summary>
        /// Not listesine göre hesaplanan aritmetik ortalama
        /// </summary>
        public double Ortalama { get; set; }

        /// <summary>
        /// Not listesine göre hesaplanan standart sapma (30 adet altı için n-1)
        /// </summary>
        public double StandartSapma { get; set; }

        /// <summary>
        /// Bağıl değerlendirme (çan eğrisi) yapılacak mı? (default false)
        /// </summary>
        public bool MutlakDegerlendir { get; set; }
        /// <summary>
        /// Lisans
        /// YüksekLisans
        /// Doktora
        /// </summary>
        public OgrenciSinif Sinif { get; set; }
        /// <summary>
        /// Not listesindeki en büyük not
        /// </summary>
        public double EnBuyukNot { get; set; }

        /// <summary>
        /// Bağıl hesaplama yapıldıktan sonra hesaplanan bağıl ortalama (default değeri aritmetik ortalama)
        /// </summary>
        public double BagilAritmetikOrtalama { get; set; }
        /// <summary>
        /// Bağıl hesaplama yapılırken bu değerin altındaki notlar FF olur (default değeri 40)
        /// </summary>
        public double MinNot { get; set; }

        /// <summary>
        /// Mutlak değerlendirme sonucunda istatistikleri gösterir (Gruplama)
        /// </summary>
        public List<Istatistik> IstatistikMutlak { get; set; }
        /// <summary>
        /// Bağıl değerlendirme sonucunda istatistikleri gösterir (Gruplama)
        /// </summary>
        public List<Istatistik> IstatistikBagil { get; set; }

        public EnumDersPlanOrtalama EnumOrtalama { get; set; }
        public bool HatalariGec { get; set; }
        public bool Butunleme { get; set; }
        public Sabis.Bolum.Core.ABS.DEGERLENDIRME.DegerlendirmeKural DegerlendirmeKural { get; set; }
        public double EnKucukNot { get; set; }
    }
}
