﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DEGERLENDIRME
{

    public class BasariDegerlendirmeVM
    {
        public int DersGrupID { get; set; }
        public List<Grup> OrtakGruplar { get; set; }
        public EnumDegerlendirmeAsama EnumDegerlendirmeAsama { get; set; }
        public bool SonHalKaldirmaYetkisiVarMi { get; set; }
        public bool SonHalYetkisiVarMi { get; set; }
        public List<Notification> NotificationList { get; set; }
        public bool ButunlemeMi { get; set; }
        public bool TarihUygunMu { get; set; }
    }
}
