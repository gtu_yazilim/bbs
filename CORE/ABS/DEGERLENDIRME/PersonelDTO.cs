﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DEGERLENDIRME
{
    public class PersonelDTO
    {
        public int ID { get; set; }
        public string SicilNo { get; set; }
        public string TC { get; set; }
        public string KullaniciAdi { get; set; }
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public string UnvanAd { get; set; }
        public string GorevAd { get; set; }
        public string GorevFakAd { get; set; }
        public string GorevBolAd { get; set; }
        public Nullable<int> EnumPersonelTipi { get; set; }
        public Nullable<int> EnumPersonelDurum { get; set; }
    }
}
