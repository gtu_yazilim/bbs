﻿using Sabis.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DEGERLENDIRME
{
    public class OgrenciPayveNotBilgileriDTO : OgrenciSonuc
    {
        public List<PaylarVeNotlarDTO> listPaylarVeNotlar { get; set; }

        public override string ToString()
        {
            string payVeNotlari = "";
            if (listPaylarVeNotlar != null)
            {
                foreach (var payNot in listPaylarVeNotlar)
                {
                    payVeNotlari += string.Format("Çalışma Tip={0}, Notu={1}, OgrenciNotID={2},Oran={3}", payNot.EnumCalismaTip, payNot.Notu, payNot.OgrenciNotID, payNot.Yuzde);
                }
            }
            string str = string.Format("Numara={0},AdSoyad={1},OgrenciID={2},DersPlanAnaID={3},PersonelID={4},MutlakNot={5},PayVeNotlari={6}", Numara, AdSoyad, OgrenciID, DersPlanAnaID, PersonelID, MutlakNot, payVeNotlari);
            return str;
        }
    }
}
