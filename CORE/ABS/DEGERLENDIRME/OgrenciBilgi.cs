﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DEGERLENDIRME
{
    public class OgrenciBilgi
    {
        public int OgrenciID { get; set; }
        public string AdSoyad { get; set; }
        public string Numara { get; set; }
    }
}
