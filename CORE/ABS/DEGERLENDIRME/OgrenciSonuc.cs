﻿using Sabis.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DEGERLENDIRME
{
    public class OgrenciSonuc
    {
        public int OgrenciID { get; set; }
        public string AdSoyad { get; set; }
        public string Numara { get; set; }

        public int? DersGrupID { get; set; }

        public int? DersPlanID { get; set; }
        
        public int DersPlanAnaID { get; set; }
        public int PersonelID { get; set; }
        public double MutlakNot { get; set; }
        public EnumHarfMutlak MutlakHarf { get; set; }
        public EnumHarfBasari BasariHarf { get; set; }
        public bool ButunlemeMi { get; set; }
    }
}
