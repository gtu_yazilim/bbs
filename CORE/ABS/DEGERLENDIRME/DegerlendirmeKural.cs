﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DEGERLENDIRME
{
    public class DegerlendirmeKural
    {
        public int FKDersPlanAnaID { get; set; }
        public int Yil { get; set; }
        public int EnumDonem { get; set; }
        public DateTime TarihKayit { get; set; }
        public string Kullanici { get; set; }
        public int ID { get; set; }
        public int MinFinal { get; set; }
        public int MinYilIci { get; set; }
        public int MinOrtalama { get; set; }
        public int MinYilIciOrtalamaTip { get; set; }
        public int MinKurulOrtalamaTip { get; set; }
        /// <summary>
        /// Tıp fakültesinde sadece yıl içi ile değerlendirme yapılabilir. Finale girmeden dersi geçebilir.
        /// </summary>
        public bool YiliciDegerlendirTip { get; set; }
    }
}
