﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.ANKET
{
    public class SoruDegerlendirme
    {
        public int SoruNo { get; set; }
        public string SoruMetin { get; set; }
        public double Puan { get; set; }
        public int Adet { get; set; }
    }
}
