﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.ANKET
{
    public class AnketSonucVM
    {
        public string Soru { get; set; }
        public string SifirSayi { get; set; }
        public string BirSayi { get; set; }
        public string IkiSayi { get; set; }
        public string UcSayi { get; set; }
        public string DortSayi { get; set; }
        public string BesSayi { get; set; }
        public string DersOrtalamasi { get; set; }
        public string BolumOrtalamasi { get; set; }
        public string FakulteOrtalamasi { get; set; }
        public string TFakulteOrtalamasi { get; set; }
        public string UniversiteOrtalamasi { get; set; }
        public string DersBuyuk { get; set; }
        public string FakulteBuyuk { get; set; }
        public string TFakulteBuyuk { get; set; }
        public string UniversiteBuyuk { get; set; }
    }
}
