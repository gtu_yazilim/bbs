﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.ViewModel.Anket
{
    public class AnketDersListesiViewModel
    {
        public bool Yetkili { get; set; }
        public int? FakulteID { get; set; }
        public int? BolumID { get; set; }
       
    }
}
