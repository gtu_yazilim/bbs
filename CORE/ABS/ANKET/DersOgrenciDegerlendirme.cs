﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.ANKET
{
    public class DersOgrenciDegerlendirme
    {
        public int DersPlanAnaID { get; set; }
        public string DersAd { get; set; }
        public List<SoruDegerlendirme> SoruCevapList { get; set; }
        public List<DersDokuman> DokumanList { get; set; }
        public double DokumanToplam { get; set; }
        public double SoruCevapToplam { get; set; }
    }
}
