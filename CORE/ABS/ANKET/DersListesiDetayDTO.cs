﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.ANKET
{
  public  class DersListesiDetayDTO
    {
        public int FakulteID { get; set; }
        public int BolumID { get; set; }
        public int GrupID { get; set; }
        public string FakulteAd { get; set; }
        public string BolumAd { get; set; }
        public string DersGrupAd { get; set; }
        public string HocaAd { get; set; }
        public string HocaSoyad { get; set; }
        public string HocaUnvan { get; set; }
        public int OgrenciSayisi { get; set; }
        public string DersKod { get; set; }
        public int OgretimTuru { get; set; }
        public string DersAd { get; set; }
        public int OgretimYili { get; set; }

        public bool SonucVarMi { get; set; }

        public string OgretimTuruAd { get; set; }
        public int? FKProgramID { get; set; }
        public string ProgramAd { get; set; }
    }
}
