﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.ANKET
{
    public class DegerlendirmeSonuc
    {
        public int PersonelID { get; set; }
        public string AdSoyadUnvan { get; set; }
        public string Fotograf { get; set; }
        public string BirimAd { get; set; }
        public List<DersOgrenciDegerlendirme> DersDegerlendirmeList { get; set; }
        public PersonelDegerlendirme PersonelDegerlendirme { get; set; }
        public double GenelToplam { get; set; }
        public string KullaniciAdi { get; set; }
        public int BirimID { get; set; }
        public int? FKAkademikUnvanID { get; set; }
    }
}
