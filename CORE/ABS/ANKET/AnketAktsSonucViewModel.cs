﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.ViewModel.Anket
{
  public  class AnketAktsSonucViewModel
    {
        public decimal ToplamDersSaati { get; set; }

       public string OnHazirlikEvetSayisi { get; set; }
       public string OnHazirlikHayirSayisi { get; set; }
       public string OnHazirlikFikrimYokSaiyisi { get; set; }
       public string OnHazirlikSaati { get; set; }

        public string OnHazirlikSonucMesaj { get; set; }

       public string OdevEvetSayisi { get; set; }
       public string OdevHayirSayisi { get; set; }
       public string OdevFikrimYokSayisi { get; set; }
       public string OdevAdet { get; set; }
       public string OdevSaat { get; set; }
        public string OdevSonucMesaj { get; set; }



       public string SunumEvetSayisi { get; set; }
       public string SunumHayirSayisi { get; set; }
       public string SunumFikrimYokSayisi { get; set; }
       public string SunumSaat { get; set; }
        public string SunumSonucMesaj { get; set; }



       public string ArasinavEvetSayisi { get; set; }
       public string ArasinavHayirSayisi { get; set; }
       public string ArasinavFikrimYokSayisi { get; set; }
       public string ArasinavAdet { get; set; }
       public string ArasinavSaat { get; set; }
       public string ArasinavSonucMesaj { get; set; }

       public string ProjeEvetSayisi { get; set; }
       public string ProjeHayirSayisi { get; set; }
       public string ProjeFikrimYokSayisi { get; set; }
       public string ProjeAdet { get; set; }
       public string ProjeSaat { get; set; }
        public string ProjeSonucMesaj { get; set; }


       public string LabEvetSayisi { get; set; }
       public string LabHayirSayisi { get; set; }
       public string LabFikrimYokSayisi { get; set; }
       public string LabAdet { get; set; }
       public string LabSaat { get; set; }
        public string LabSonucMesaj { get; set; }





       public string YariyilEvetSayisi { get; set; }
       public string YariyilHayirSayisi { get; set; }
       public string YariyilFikrimYokSayisi { get; set; }
       public string YariyilSaat { get; set; }
        public string YariyilSonucMesaj { get; set; }




       public string AraziEvetSayisi { get; set; }
       public string AraziHayirSayisi { get; set; }
       public string AraziFikrimYokSayisi { get; set; }
       public string AraziSaat { get; set; }
        public string AraziSonucMesaj { get; set; }


        public string GenelSonucMesaj { get; set; }

       
        public string DerseDevamYuzdesi { get; set; }
        public string SunumAdet { get; set; }
        public string AraziAdet { get; set; }
        public string HataMesaji { get; set; }
        public string DerseDevamYuzdesiSonuc { get; set; }
    }
}
