﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.ANKET
{
    public class BirimSonuc
    {
        public int BirimID { get; set; }
        public string BirimAd { get; set; }
        public List<DegerlendirmeSonuc> PersonelList { get; set; }
        public Dictionary<int, BirimSonuc> BirimSonucList { get; set; }
    }
}
