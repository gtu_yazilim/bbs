﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.ANKET
{
    public class DersDokuman
    {
        public string DokumanTipi { get; set; }
        public int Adet { get; set; }
        public double Puan { get; set; }
    }
}
