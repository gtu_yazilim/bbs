﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.ANKET
{
    public class PersonelDegerlendirme
    {
        public List<SoruDegerlendirme> SoruCevapList { get; set; }
        public double GenelToplam { get; set; }
    }
}
