﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.NOT
{
    public class NotGirisListParamModel
    {
        public int? GrupID { get; set; }
        public int DersPlanAnaID { get; set; }
        public int PersonelID { get; set; }
        public List<int> PayIDList { get; set; }
        public int Yil { get; set; }
        public int Donem { get; set; }
    }
}
