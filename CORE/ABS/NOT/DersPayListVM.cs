﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.NOT
{
    public class DersPayListVM
    {
        public int DersPlanAnaID { get; set; }
        public string DersAd { get; set; }
        public List<Sabis.Bolum.Core.ABS.DERS.DERSDETAY.PayHiyerarsikVM> PayList { get; set; }
    }
}
