﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.NOT
{
    public class OgrenciNotlariVM
    {
        public int PayID { get; set; }
        public string Notu { get; set; }
        public int? OgrenciNotID { get; set; }
        public byte? Oran { get; set; }
        public byte? Sira { get; set; }
        public int EnumCalismaTip { get; set; }
        public bool Aktif { get; set; }
        public int? DersGrupID { get; set; }
        public int MyProperty { get; set; }
    }
}
