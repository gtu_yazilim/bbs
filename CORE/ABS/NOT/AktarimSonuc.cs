﻿using Sabis.Bolum.Core.ABS.NOT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.NOT
{
    public class AktarimSonuc
    {
        public EnumAktarimDurum EnumAktarimDurum { get; set; }
        public string Hata { get; set; }
        public NotKayitVM OgrenciNot { get; set; }
    }

    public enum EnumAktarimDurum
    {
        Eklendi,
        Guncellendi,
        Hata,
        Silindi,
        HataAbisGuncelleme
    }
}
