﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.NOT
{
    public class NotKayitVM
    {
        public string Notu { get; set; }
        public int OgrenciID { get; set; }
        public int? DersGrupID { get; set; }
        public int PayID { get; set; }
        public string KaydedenKulanici { get; set; }
        public int Yil { get; set; }
        public int Donem { get; set; }
        public int? DersPlanID { get; set; }
        public int DersPlanAnaID { get; set; }
        public Guid? UzemID { get; set; }
    }

    public class NotKayitModel
    {
        public List<NotKayitVM> NotKayitViewModelList { get; set; }
        public string KullaniciAdi { get; set; }
        public string IPAdresi { get; set; }
        public int Yil { get; set; }
        public int Donem { get; set; }
        public NotKayitModel()
        {
            NotKayitViewModelList = new List<NotKayitVM>();
        }
    }
}
