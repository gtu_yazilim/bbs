﻿using Sabis.Bolum.Core.ABS.DERS.PROJE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.NOT
{
    public class OgrenciListesiVM
    {
        public OgrenciListesiVM()
        {
            OgrenciNotListesi = new List<OgrenciNotlariVM>();
        }
        public int OgrenciID { get; set; }
        public string Numara { get; set; }
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public List<OgrenciNotlariVM> OgrenciNotListesi { get; set; }
        public string DersAd { get; set; }
        public int DersPlanAnaID { get; set; }
        public int DersGrupID { get; set; }
        public int DersPlanID { get; set; }
        public int YazilmaID { get; set; }
        public int PersonelID { get; set; }
        public double MutlakNot { get; set; }
        public int? MutlakHarf { get; set; }
        public int? BasariHarf { get; set; }
        public int? ButunlemeOncesiMutlakHarf { get; set; }
        public bool ButunlemeMi { get; set; }
        public override string ToString()
        {
            string payVeNotlari = "";
            if (OgrenciNotListesi != null)
            {
                foreach (var payNot in OgrenciNotListesi)
                {
                    payVeNotlari += string.Format("Çalışma Tip={0}, Notu={1}, OgrenciNotID={2},Oran={3}", payNot.EnumCalismaTip, payNot.Notu, payNot.OgrenciNotID, payNot.Oran);
                }
            }
            string str = string.Format("Numara={0},AdSoyad={1},OgrenciID={2},DersPlanAnaID={3},PersonelID={4},MutlakNot={5},PayVeNotlari={6}", Numara, Ad + " " + Soyad, OgrenciID, DersPlanAnaID, PersonelID, MutlakNot, payVeNotlari);
            return str;
        }
        public bool YayinlandiMi { get; set; }
        public List<ProjeRaporDto> OgrenciRaporlar { get; set; }
    }
}
