﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.NOT
{
    public class BasariNotKayitVM
    {
        public bool ButunlemeMi;
        public int? DersGrupID { get; set; }
        public int? Donem { get; set; }
        public int? PersonelID { get; set; }
        public int? Yil { get; set; }
        public List<OgrenciListesiVM> ListBasariNot { get; set; }
        public string KullaniciAdi { get; set; }
        public string IPAdresi { get; set; }
        public BasariNotKayitVM()
        {
            ListBasariNot = new List<OgrenciListesiVM>();
        }
    }
}
