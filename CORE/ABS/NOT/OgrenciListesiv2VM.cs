﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.NOT
{
    public class OgrenciListesiv2VM
    {
        public OgrenciListesiv2VM()
        {
            OgrenciNotListesi = new List<OgrenciNotlariVM>();
        }
        public int OgrenciID { get; set; }
        public string Numara { get; set; }
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public List<OgrenciNotlariVM> OgrenciNotListesi { get; set; }
        public int DersPlanAnaID { get; set; }
        public int? DersGrupID { get; set; }
        public int? DersPlanID { get; set; }
        public int? YazilmaID { get; set; }
        public bool? ButunlemeMi { get; set; }
    }
}
