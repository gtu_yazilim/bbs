﻿using Sabis.Bolum.Core.ABS.NOT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.PROJE
{
    public class ProjeKonuOgrenci
    {
        public List<ProjeKonu> projeKonu { get; set; }
        public List<OgrenciListesiVM> ogrenciListesi { get; set; }
    }
}
