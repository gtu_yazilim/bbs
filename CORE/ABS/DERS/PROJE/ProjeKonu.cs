﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.PROJE
{
    public class ProjeKonu
    {
        public int ID { get; set; }
        public int ProjeID { get; set; }
        public string KonuBaslik { get; set; }
        public string Konu { get; set; }

        public List<OgrenciBilgi> OgrenciListe { get; set; }
    }

    public class OgrenciBilgi
    {
        public int OgrenciID { get; set; }
    }
}
