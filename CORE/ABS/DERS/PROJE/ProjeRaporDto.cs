﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.PROJE
{
    public class ProjeRaporDto
    {
        public int ID { get; set; }
        public int FKProjeOgrenciID { get; set; }
        public int RaporNo { get; set; }
        public int RaporDurum { get; set; }
        public string ProjeDosyaKey { get; set; }
        public string HocaAciklama { get; set; }
    }
}
