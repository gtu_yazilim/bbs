﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.PROJE
{
    public class ProjeTakvimVM
    {
        public int? ID { get; set; }
        public int FKDersPlanAnaID { get; set; }
        public int FKDersGrupID { get; set; }
        public int Yil { get; set; }
        public int EnumDonem { get; set; }
        public string Baslik { get; set; }
        public string Aciklama { get; set; }
        public int MaxDosyaBoyut { get; set; }
        public int? RaporSayisi { get; set; }
        public DateTime Rapor1BitisTarihi { get; set; }
        public DateTime? Rapor2BitisTarihi { get; set; }
        public DateTime? Rapor3BitisTarihi { get; set; }
        public DateTime? Rapor4BitisTarihi { get; set; }
        public DateTime? Rapor5BitisTarihi { get; set; }
        public string Ekleyen { get; set; }
        public DateTime EklenmeTarihi { get; set; }
        public string EkleyenIP { get; set; }
    }
}
