﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS
{
    public class DersProgramiVM
    {
        public int ID { get; set; }

        public int Ay { get; set; }

        public int Yil { get; set; }

        public int Saat { get; set; }

        public int Gun { get; set; }

        public DateTime DersBaslangicTarihSaat { get; set; }

        public DateTime DersBitisTarihSaat { get; set; }

        public int? EnumDersSaatTipi { get; set; }

        public string MekanAd { get; set; }

        public string PersonelAdSoyad { get; set; }

        public int? FKDersKonuID { get; set; }

        public string DersAd { get; set; }
        public int? EnumOgretimTuru { get; set; }
        public int FKDersGrupHocaID { get; set; }
        public int? YilinHaftasi { get; set; }
        public string strTarih { get; set; }
    }

    //public class Event
    //{
    //    public int UID { get; set; }
    //    public DateTime Start { get; set; }
    //    public DateTime End { get; set; }
    //    public string Description { get; set; }
    //    public string Location { get; set; }
    //    public string Summary { get; set; }
    //}
}
