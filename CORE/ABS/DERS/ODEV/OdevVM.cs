﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sabis.Bolum.Core.ABS.DERS.SINAV;

namespace Sabis.Bolum.Core.ABS.DERS.ODEV
{
    public class OdevVM
    {
        public int? ID { get; set; }
        public string Ad { get; set; }
        public string Aciklama { get; set; }
        public DateTime? Baslangic { get; set; }
        public DateTime? Bitis { get; set; }
        public int FKAbsPaylarID { get; set; }
        public int FKPersonelID { get; set; }
        public int? FKDersGrupID { get; set; }
        public int EnumSinavTuru { get; set; }
        public int MaxYuklemeSayisi { get; set; }
        public string OdevDosyaKey { get; set; }
        public double OdevDosyaBoyut { get; set; }
        public string OdevDosyaTuru { get; set; }
        public bool Yayinlandi { get; set; }
        public DateTime SGTarih { get; set; }
        public string SGKullanici { get; set; }
        public string SGIP { get; set; }
        public bool Silindi { get; set; }
        public bool OnlineOdevMi { get; set; }
        public List<PaylarOlcmeVM> ABSPaylarOlcme { get; set; }
        public List<OdevGrupListesiVM> DersGrupListesi { get; set; }
        public List<OdevOgrenciVM> OdevOgrenciListesi { get; set; }
        public bool GenelOdevMi { get; set; }
        public bool OrtakOdevMi { get; set; }
    }

    public class OdevGrupListesiVM
    {
        public int? ID { get; set; }
        public int? FKOdevID { get; set; }
        public int FKDersGrupID { get; set; }
        public string GrupAd { get; set; }
        public bool GrupEkliMi { get; set; }
        public int FKPersonelID { get; set; }
        public bool DegisiklikYapilabilirMi { get; set; }
    }

    public class OdevOgrenciVM
    {
        public int? ID { get; set; }
        public int FKOdevID { get; set; }
        public int FKOgrenciID { get; set; }
        public string OgrenciNumara { get; set; }
        public string OgrenciAdSoyad { get; set; }
        public string DosyaAdi { get; set; }
        public string DosyaKey { get; set; }
        public DateTime YuklemeTarihi { get; set; }
        public string Aciklama { get; set; }
    }
}
