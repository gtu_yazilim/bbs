﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.ODEV
{
    public class OdevAciklamaVM
    {
        public int FKOgrenciID { get; set; }
        public int FKPayID { get; set; }
        public string Aciklama { get; set; }
    }
}
