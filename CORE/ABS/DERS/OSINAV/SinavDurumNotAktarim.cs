﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.OSINAV
{
    public enum SinavDurumNotAktarim
    {
        Girmedi = 0,
        Katildi = 2,
        MazeretHakVerildi = 3,
        MazereteKatildi = 5,       
    }
}
