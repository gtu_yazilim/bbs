﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.OSINAV
{
    public class OSinavSoruDurum
    {
        public int SoruID { get; set; }
        public bool Kullanimda { get; set; }
    }
}
