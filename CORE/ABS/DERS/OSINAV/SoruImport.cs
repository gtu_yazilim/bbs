﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.OSINAV
{
    public class SoruImport
    {
        public int SoruNo { get; set; }
        public string Baslik { get; set; }
        public string A { get; set; }
        public string B { get; set; }
        public string C { get; set; }
        public string D { get; set; }
        public string E { get; set; }
        public string DogruCevap { get; set; }
        public bool Durum { get; set; }
        public string Mesaj { get; set; }
    }
}
