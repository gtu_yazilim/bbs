﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.OSINAV
{
    public class OSinavVM : OSinavBase
    {
        public int SinavID { get; set; }
        [Required]
        public string Ad { get; set; }

        public int EklemeTuru { get; set; }
        public string Aciklama { get; set; }
        [Required]
        public Nullable<System.DateTime> Baslangic { get; set; }
        [Required]
        public Nullable<System.DateTime> Bitis { get; set; }
        [Required]
        public int? Sure { get; set; }
        [Required]
        public int? SoruSayisi { get; set; }
        public int Yayinlandi { get; set; }
        public string Ekleyen { get; set; }
        public Nullable<int> EkleyenID { get; set; }
        public Nullable<System.DateTime> EklemeTarihi { get; set; }
        public Nullable<int> GuncelleyenID { get; set; }
        public string Guncelleyen { get; set; }
        public Nullable<System.DateTime> GuncellemeTarihi { get; set; }
        public int GrupID { get; set; }
        public int PayID { get; set; }
        //[Required]
        //public IEnumerable<OSinavHocaGruplar> hocagruplar { get; set; }
        //public IEnumerable<OSinavHocaGruplar2> hocagruplar2 { get; set; }

        public IEnumerable<SinavOturumDto> sinavoturumlari { get; set; }

        public IEnumerable<HocaGrup> hocagrups { get; set; }

        public bool Mazeret { get; set; }
        public DateTime? MazeretBaslangic { get; set; }
        public DateTime? MazeretBitis { get; set; }
        public int? MazeretSure { get; set; }
    }
    
}
