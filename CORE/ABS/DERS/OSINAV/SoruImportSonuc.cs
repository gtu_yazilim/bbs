﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.OSINAV
{
    public class SoruImportSonuc
    {
        public int SinavID { get; set; }
        public List<SoruImport> Sorular { get; set; }
        public string Path { get; set; }
        public Guid ImportId { get; set; }
        public string DosyaUrl { get; set; }
        public string DosyaAdi { get; set; }
        public string DosyaId { get; set; }
        public byte Durum { get; set; }
        public string Mesaj { get; set; }
    }
}
