﻿using Sabis.Bolum.Core.ABS.DERS.DERSDETAY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.OSINAV
{
    public class OSinavListe : OSinavBase
    {
        public PayBilgileriVM paybilgi { get; set; }
        public Nullable<int> SinavID { get; set; }
        public int FKDersPlanAnaID { get; set; }
        public int FKDersPlanID { get; set; }
        public int FKDersGrupID { get; set; }
        public int PayID { get; set; }
    }
}
