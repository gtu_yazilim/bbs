﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.OSINAV
{
    public class OSinavSoruEkle : OSinavBase
    {
        public int SoruID { get; set; }
        public int SinavID { get; set; }
        public int SoruNo { get; set; }
        [Required]
        public string Baslik { get; set; }
        [Required]
        public int DogruCevap { get; set; }
        [Required(ErrorMessage = "A seçeneğini doldurmanız gereklidir!")]
        public string Cevap1 { get; set; }
        [Required(ErrorMessage = "B seçeneğini doldurmanız gereklidir!")]
        public string Cevap2 { get; set; }
        [Required(ErrorMessage = "C seçeneğini doldurmanız gereklidir!")]
        public string Cevap3 { get; set; }
        [Required(ErrorMessage = "D seçeneğini doldurmanız gereklidir!")]
        public string Cevap4 { get; set; }
        [Required(ErrorMessage = "E seçeneğini doldurmanız gereklidir!")]
        public string Cevap5 { get; set; }
        public Nullable<bool> Silindi { get; set; }
        public bool Iptal { get; set; }
        public string Ekleyen { get; set; }
        public int EkleyenID { get; set; }
        public System.DateTime EklemeTarihi { get; set; }
        public int GuncelleyenID { get; set; }
        public string Guncelleyen { get; set; }
        public System.DateTime GuncellemeTarihi { get; set; }
        public string SinavAd { get; set; }

        public List<int> ZorlukDerece { get; set; }
        public int? PaylarOlcmeID { get; set; }
        public int? PayOlcmeSoruNo { get; set; }
        public int? PayID { get; set; }
        public int? GrupHocaID { get; set; }
        public int SoruDosyaID { get; set; }
        public string SoruDosyaKey { get; set; }

        public string Cevap1DosyaKey { get; set; }
        public string Cevap2DosyaKey { get; set; }
        public string Cevap3DosyaKey { get; set; }
        public string Cevap4DosyaKey { get; set; }
        public string Cevap5DosyaKey { get; set; }

        public int SinavTur { get; set; }
        public bool TumGruplarimaEkle { get; set; }
        public bool MazeretSoru { get; set; }

    }
}
