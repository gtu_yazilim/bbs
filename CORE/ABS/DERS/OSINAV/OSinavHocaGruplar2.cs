﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.OSINAV
{
    public class OSinavHocaGruplar2
    {
        [Required]
        public int? FKDersGrupID { get; set; }
        public int? FKPersonelID { get; set; }
        public string GrupAd { get; set; }
        public int SinavID { get; set; }
        public int OgretimTuru { get; set; }
        [Required]
        public bool Selected { get; set; }
        public bool SelectedOld { get; set; }
    }
}
