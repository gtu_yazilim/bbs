﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.OSINAV
{
    public class DegerlendirmeGruplariDto
    {
        public int FKHocaGrupID { get; set; }
        public int? FKDersPlanAnaID { get; set; }
        public int OrtakDegerlendirmeID { get; set; }
        public int FKPersonelID { get; set; }
        public int FKOrtakDegerlendirmeGrupID { get; set; }
        public int FKGrupID { get; set; }
        public string GrupAd { get; set; }
        public int OgretimTur { get; set; }
    }
}
