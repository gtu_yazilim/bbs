﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.OSINAV
{
    public class OSinavSoruListe : OSinavBase
    {
        public int SinavID { get; set; }
        public IEnumerable<OSinavSoruItem> Sorular { get; set; }
    }
}
