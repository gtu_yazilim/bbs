﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.OSINAV
{
    public class SinavOturumDto
    {
        public int SinavID { get; set; }
        public string SinavAd { get; set; }
        public int GrupID { get; set; }
        public int EID { get; set; }
        public int YazilmaID { get; set; }
        public int OturumID { get; set; }
        public int OgrenciID { get; set; }
        public string OgrenciNo { get; set; }
        public int KisiID { get; set; }
        //public decimal? Notu { get; set; }
        public double? Notu { get; set; }
        //public int SonNot { get; set; }
        public double? SonNot { get; set; }
        public DateTime? OturumTarih { get; set; }
        public int? DogruSayisi { get; set; }
        public int? YanlisSayisi { get; set; }
        public int? BosSayisi { get; set; }
        public int MazeretDogruSayisi { get; set; }
        public int MazeretYanlisSayisi { get; set; }
        public int MazeretBosSayisi { get; set; }
        public string OgrenciAd { get; set; }
        public string OgrenciSoyad { get; set; }
        public string OgrenciNumara { get; set; }
        public string OgrenciKullaniciAdi { get; set; }
        public string FotografAdres { get; set; }
        public int GrupHocaID { get; set; }
        public string OturumMesaj { get; set; }
        public int OturumDurum { get; set; }

        public bool MazeretSinavi { get; set; }
        public DateTime? TarihOturumBaslangic { get; set; }
        public DateTime? TarihOturumBitis { get; set; }
        public DateTime? TarihMazeretOturumBaslangic { get; set; }
        public DateTime? TarihMazeretOturumBitis { get; set; }

        public bool SorunBildirenler { get; set; }
        public int GeriBildirimID { get; set; }
    }
}
