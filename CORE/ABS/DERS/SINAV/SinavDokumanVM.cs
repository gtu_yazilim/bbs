﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.SINAV
{
    public class SinavDokumanVM
    {
        public int? ID { get; set; }
        public int? FKSinavID { get; set; }
        public string DosyaAdi { get; set; }
        public string DosyaKey { get; set; }
        public int EnumDokumanTuru { get; set; }
        public DateTime SGTarihi { get; set; }
        public string SGKullanici { get; set; }
        public string SGIP { get; set; }
        public bool Silindi { get; set; }
        public string SilenKullanici { get; set; }
        public string SilenIP { get; set; }
        public DateTime SilindiTarih { get; set; }
    }
}
