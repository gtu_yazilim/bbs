﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.SINAV
{
    public class CevapAnahtariVM
    {
        public int ID { get; set; }
        public int FKSinavID { get; set; }
        public string CevapA { get; set; }
        public string SiraB { get; set; }
        public string CevapB { get; set; }
        public string SiraC { get; set; }
        public string CevapC { get; set; }
        public string SiraD { get; set; }
        public string CevapD { get; set; }
    }

    public class CevapAnahtari2VM
    {
        public int ID { get; set; }
        public int FKSinavID { get; set; }
        public List<string> CevapA { get; set; }
        public List<double> PuanA { get; set; }
        public List<string> SiraB { get; set; }
        public List<string> CevapB { get; set; }
        public List<double> PuanB { get; set; }
        public List<string> SiraC { get; set; }
        public List<string> CevapC { get; set; }
        public List<double> PuanC { get; set; }
        public List<string> SiraD { get; set; }
        public List<string> CevapD { get; set; }
        public List<double> PuanD { get; set; }
    }
}
