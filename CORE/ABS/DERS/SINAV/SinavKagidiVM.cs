﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.SINAV
{
    public class SinavKagidiVM
    {
        public int? ID { get; set; }
        public string Kullanici { get; set; }
        public string IP { get; set; }
        public DateTime TarihKayit { get; set; }
        public DateTime TarihOkuma { get; set; }
        public int? FKSinavID { get; set; }
        public string DosyaKey { get; set; }
        public string DosyaAdi { get; set; }
        public int? EnumKagitOkumaDurumu { get; set; }
        public string HataDetay { get; set; }
        public string OgrenciNo { get; set; }
        public int? FKKagitSonuc { get; set; }
        public string OgrenciAdSoyad { get; set; }
        public string KagitGrubu { get; set; }
        public string Sonuclar { get; set; }
        public int? FKAbsPaylarID { get; set; }
    }
}
