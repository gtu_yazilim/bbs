﻿using System;
using System.ComponentModel;

namespace Sabis.Bolum.Core.ABS.DERS.SINAV
{
    public class OgrenciSinav
    {
        public int SinavID { get; set; }
        public string SinavAd { get; set; }
        public DateTime BaslangicTarih { get; set; }
        public DateTime BitisTarih { get; set; }
        public DateTime? TarihMazeretBaslangic { get; set; }
        public DateTime? TarihMazeretBitis { get; set; }
        public int SinavSure { get; set; }
        public SinavDurum Durum { get; set; }
        public string Sonuc { get; set; }
        public DateTime TarihKayit { get; set; }
        public int DersGrupID { get; set; }
        public string DersAd { get; set; }
        public bool Yayinlandi { get; set; }
    }
    public enum SinavDurum
    {
        [Description("Giriş Yapılmadı")]
        GirisYapilmadi = 0,
        [Description("Devam Ediyor")]
        DevamEdiyor = 1,
        [Description("Tamamlandı")]
        Tamamlandi = 2,
        [Description("Mazeret Hakkı Verildi")]
        MazeretHakkiVerildi = 3,
        [Description("Mazeret Sınavı Devam Ediyor")]
        MazeretSinavDevamEdiyor = 4,
        [Description("Mazeret Sınavı Tamamlandı.")]
        MazeretSinavTamamlandi = 5,
    }






















    public enum RaporDurum
    {
        [Description("Rapor Gönderildi,Onay Bekliyor")]
        Gonderildi = 1,
        [Description("Rapor Onaylandı")]
        Onaylandi = 2,
        [Description("Rapor Reddedildi")]
        Reddedildi = 3,
        [Description("Raporu Yeniden Yükleme Hakkı Verildi")]
        HakVerildi = 4,

    }
}
