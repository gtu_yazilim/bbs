﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.SINAV
{
    public class SinavDetaylariVM
    {
        public int? FKSinavID { get; set; }
        public int? EnumCalismaTip { get; set; }
        public int? SoruSayisi { get; set; }
        public int? EnumSinavTuru { get; set; }
        public bool SinavOlusturulmusMu { get; set; }
        public bool NotlarAktarilabilirMi { get; set; }
        public int OkunmayanKagitSayisi { get; set; }
    }
}
