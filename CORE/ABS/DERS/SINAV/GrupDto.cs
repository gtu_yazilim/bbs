﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.SINAV
{
    public class GrupDto
    {
        public int value { get; set; }
        public string text { get; set; }
    }
}
