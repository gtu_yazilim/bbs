﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.SINAV
{
    public class YazilmaKontrolDto
    {
        public int YazilmaID { get; set; }
        public int FKDersPlanAnaID { get; set; }
        public int FKDersPlanID { get; set; }
        public int FKDersGrupID { get; set; }
    }
}
