﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.SINAV
{
    public class PaylarOlcmeVM
    {
        public int ID { get; set; }
        public int? FKPaylarID { get; set; }
        public byte? SoruNo { get; set; }
        public double? SoruPuan { get; set; }
        public int? FKKoordinatorID { get; set; }
        public DateTime? Zaman { get; set; }
        public List<PaylarOlcmeProgramCiktilariVM> ProgramCiktilari { get; set; }
        public List<PaylarOlcmeOgrenmeCiktilariVM> OgrenmeCiktilari { get; set; }
        public bool? KayitliMi { get; set; }
        public int? EnumSinavTuru { get; set; }
        public int? EnumSoruTipi { get; set; }
        public int? OSinavSoruID { get; set; }
        public string OSinavSoruBaslik { get; set; }
        public int? OOdevID { get; set; }
        public bool SoruIptal { get; set; }
    }

    public class PaylarOlcmeProgramCiktilariVM
    {
        public int ID { get; set; }
        public int? FKPaylarOlcmeID { get; set; }
        public int? Sira { get; set; }
        public int? FKProgramCiktiID { get; set; }
        public string Icerik { get; set; }
    }

    public class PaylarOlcmeOgrenmeCiktilariVM
    {
        public int ID { get; set; }
        public int? FKPaylarOlcmeID { get; set; }
        public int? Sira { get; set; }
        public int? FKOgrenmeCiktiID { get; set; }
        public string Icerik { get; set; }
    }
}
