﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.SINAV
{
    public class GrupBilgiVM
    {
        public List<DegerlendirmeGrup> GruplarDegerlendirme { get; set; }
        public List<KoordinatorGrup> GruplarKoordinator { get; set; }
    }
    public class DegerlendirmeGrup
    {
        public int GrupID { get; set; }
        public string GrupAd { get; set; }
        public int OgretimTuru { get; set; }
        public string BirimAd { get; set; }
        public string HocaAd { get; set; }
    }
    public class KoordinatorGrup
    {
        public int GrupID { get; set; }
        public string GrupAd { get; set; }
        public int OgretimTuru { get; set; }
        public string BirimAd { get; set; }
    }
}
