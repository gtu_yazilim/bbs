﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.SINAV
{
    public class SinavIstatistikleriVM
    {
        public IstatistikVM GrupIstatistikleri { get; set; }
        public IstatistikVM GenelIstatistikler { get; set; }
    }

    public class IstatistikVM
    {
        public double AzamiNot { get; set; }
        public double AsgariNot { get; set; }
        public double OrtalamaNot { get; set; }
        public int OgrenciSayisi { get; set; }
        public int SinavaKatilan { get; set; }
        public int SinavaKatilmayan { get; set; }
        public int YuzAlanSayisi { get; set; }
        public int SifirAlanSayisi { get; set; }
    }
}
