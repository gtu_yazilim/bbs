﻿using Sabis.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.SINAV
{
    public class SinavKagidiObisVM
    {
        public string DosyaKey { get; set; }
        public EnumCalismaTip CalismaTip { get; set; }
        public byte Sira { get; set; }
        public int PayID { get; set; }
        public string OgrenciCevap { get; set; }
        public string CevapAnahtari { get; set; }
    }
}
