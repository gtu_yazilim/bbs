﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.SINAV
{
    public class OnlineSinavDosya
    {
        public int ID { get; set; }
        public int SinavID { get; set; }
        public int SoruID { get; set; }
        public string DosyaKey { get; set; }
        public string Tip { get; set; }
    }
}
