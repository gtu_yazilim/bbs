﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.SINAV
{
    public class OSinavRapor
    {
        public int SinavID { get; set; }
        public int DersGrupID { get; set; }
        public int OgrenciID { get; set; }
        public string OgrenciNumara { get; set; }
        public string OgrenciAdSoyad { get; set; }
        public int OSinavOgrenciID { get; set; }
        public int OturumID { get; set; }
        public int Notu { get; set; }
        public int DogruSayi { get; set; }
        public int YanlisSayi { get; set; }
        public int BosSayis { get; set; }
    }
}
