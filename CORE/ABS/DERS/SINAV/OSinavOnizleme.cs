﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.SINAV
{
    public class OSinavOnizleme
    {
        public int SoruID { get; set; }
        public int SinavID { get; set; }
        public int SoruNo { get; set; }
        public string Baslik { get; set; }
        public string ImageUrl { get; set; }
        public int DogruCevap { get; set; }
        public string Cevap1 { get; set; }
        public string Cevap2 { get; set; }
        public string Cevap3 { get; set; }
        public string Cevap4 { get; set; }
        public string Cevap5 { get; set; }

        public string SoruResim { get; set; }
        public string Cevap1Resim { get; set; }
        public string Cevap2Resim { get; set; }
        public string Cevap3Resim { get; set; }
        public string Cevap4Resim { get; set; }
        public string Cevap5Resim { get; set; }
        public int? CevapID { get; set; }
        public int? Cevap { get; set; }
        public bool CevapDogru { get; set; }
        public string CevapDogruText { get; set; }
        public DateTime? CevapTarih { get; set; }
        public string CevapText { get; set; }
        public bool Iptal { get; set; }
    }
}
