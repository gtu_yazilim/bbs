﻿using Sabis.Bolum.Core.ENUM.ABS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.SINAV
{
    public class GeriBildirimDto
    {
        public int ID { get; set; }
        public DateTime TarihKayit { get; set; }
        public int OgrenciID { get; set; }
        public int AktiviteID { get; set; }
        public int GrupID { get; set; }
        public int EnumModulTipi { get; set; }
        public string Baslik { get; set; }
        public string Icerik { get; set; }
        public int EnumGeriBildirimDurum { get; set; }
        public string DosyaKey { get; set; }
        public string Cevap { get; set; }

        public List<GeriBildirimDto> GecmisBildirimler { get; set; } 

    }
}
