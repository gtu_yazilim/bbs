﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.SINAV
{
    public class SinavOgrenciListeVM
    {
        public SinavOgrenciListeVM()
        {
            OgrenciNotListesi = new List<SinavNotGirisSoruListeVM>();
        }
        public int OgrenciID { get; set; }
        public string Numara { get; set; }
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public List<SinavNotGirisSoruListeVM> OgrenciNotListesi { get; set; }
        public int DersPlanAnaID { get; set; }
        public int DersGrupID { get; set; }
        public int DersPlanID { get; set; }
        public int YazilmaID { get; set; }
        public int PersonelID { get; set; }
        public double? MutlakNot { get; set; }
        public int? MutlakHarf { get; set; }
        public int? BasariHarf { get; set; }
        public bool ButunlemeMi { get; set; }
        public int SoruSayisi { get; set; }
        public int PayID { get; set; }
        public string KullaniciAdi { get; set; }
        public bool SinavKagidiGorebilirMi { get; set; }
        public string OdevDosyaKey { get; set; }
        public string OdevDosyaAdi { get; set; }
        public string OdevAciklama { get; set; }
        public bool OdevMi { get; set; }
        public double MutlakOrtalama { get; set; }
        public byte FinalOran { get; set; }
    }

    public class SinavNotGirisSoruListeVM
    {
        public DateTime? Zaman { get; set; }
        public string Kullanici { get; set; }
        public string Makina { get; set; }
        public bool AktarilanVeriMi { get; set; }
        public int? SoruNo { get; set; }
        public double? Notu { get; set; }
        public int? FKOgrenciNotID { get; set; }
        public int? FKABSPaylarID { get; set; }
        public int FKOgrenciID { get; set; }
        public int FKABSPaylarOlcmeID { get; set; }
        public int EnumSoruTip { get; set;  }
        public int? EnumSinavTip { get; set; }
        public int SoruSayisi { get; set; }
    }
}
