﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.SINAV
{
    public class OturumDetayDto
    {
        public int SinavID { get; set; }
        public int SoruNo { get; set; }
        public int EID { get; set; }
        //public int? Notu { get; set; }
        public double? Notu { get; set; }
        //public int? MazeretNotu { get; set; }
        public double? MazeretNotu { get; set; }
        public string Baslik { get; set; }
        public bool? CevapDogru { get; set; }
        public int CevapDogru1 { get; set; }
        public string OgrenciCevap { get; set; }

        public string DogruCevap { get; set; }
        public int OturumTip { get; set; }
        public bool MazeretSoru { get; set; }
        public bool MazeretCevap { get; set; }
        public bool SoruIptal { get; set; }

        public DateTime? TarihOturumBaslangic { get; set; }
        public DateTime? TarihOturumBitis { get; set; }
        public DateTime? TarihMazeretOturumBaslangic { get; set; }
        public DateTime? TarihMazeretOturumBitis { get; set; }
    }
}
