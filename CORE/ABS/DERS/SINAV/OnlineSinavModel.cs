﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.SINAV
{
    public class OnlineSinavModel
    {
        public string Ad { get; set; }
        public string Aciklama { get; set; }
        public string Baslangic { get; set; }
        public string Bitis { get; set; }
        public string Sure { get; set; }
        public string SoruSayisi { get; set; }
        public string Yayinlandi { get; set; }
        public string FKPaylarID { get; set; }
        public string FKKoordinatorID { get; set; }
        public string FKDersGrupID  { get; set; }
        public string dersGrupHocaID { get; set; }
        public string Mazeret { get; set; }
        public string MazeretBaslangic { get; set; }
        public string MazeretBitis { get; set; }
        public string MazeretSure { get; set; }
        public string sinavTuru { get; set; }
        public EklemeTur EklemeTuru { get; set; }
    }
    public enum EklemeTur
    {
        Sinif = 1,
        Genel = 2
    }
}