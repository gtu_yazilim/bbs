﻿using Sabis.Bolum.Core.ABS.DERS.OSINAV;
using Sabis.Bolum.Core.ABS.DERS.SANALSINIF.Dto;
using Sabis.Bolum.Core.ABS.DERS.SANALSINIF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.SANALSINIF.Interface
{
    public interface ISanalSinif
    {
        List<SanalSinifDto> SanalSinifListe(int hocaid);
        List<HocaGruplar> HocaDegerlendirmeGrup(int personelid, int yil, int donem);
        int SanalSinifKaydet(SanalSinifDto model);

        bool SanalSinifGrupKontrol(int sanalsinifid, int grupid);
        bool SanalSinifGrupEkle(SanalSinifDto model, int sanalsinifid, int FKDersPlanAnaID, int FKDersPlanID);
        SanalSinifDto SanalSinifGetir(int sanalsinifid);
        List<SanalSinifGrupDto> SanalSinifGrupGetir(int sanalsinifid);
        bool SanalSinifGrupSil(int sanalsinifid);
        int SanalSinifDuzenle(SanalSinifDto model);
        List<HocaGruplar> HocaninGruplari_Ekle(int personelid, int anadersid, int yil, int donem);
        List<HocaGruplar> HocaninGruplari_Duzenle(int personelid, int anadersid, int yil, int donem, int sanalsinifid);
        SanalSinifSunucuDto SunucuSec(DateTime baslangic, int sure);

        bool SorunBildir(int sanalsinifid, bool deger);

        List<SorunBildirimDto> SorunBildirimleriniGetir(int sanalsinifid);

        SorunBildirimDto SorunBildirimGetirDetay (int gid);

        bool SanalSinifBildirimKaydet(int gid, string Cevap);

    }
}
