﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.SANALSINIF.Models
{
    public enum KullaniciTipEnum
    {
        Ogrenci = 1,
        Hoca = 2,
        Yonetici = 3,
        Admin = 4,
        Diger = 9
    }
}
