﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.SANALSINIF.Models
{
    public class KatilVM
    {
        public string DersAd { get; set; }
        public int DersID { get; set; }
        public string Ad { get; set; }
        public string Adres { get; set; }
        public int SSinifID { get; set; }
        public SSDurum Durum { get; set; }
        public string DurumMesaj { get; set; }
    }
}
