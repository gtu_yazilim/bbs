﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.SANALSINIF.Dto
{
    public class SanalSinifSunucuDto
    {
        public int SunucuID { get; set; }
        public string SunucuAd { get; set; }
    }
}
