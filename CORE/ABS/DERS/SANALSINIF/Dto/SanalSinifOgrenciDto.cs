﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.SANALSINIF.Dto
{
    public class SanalSinifOgrenciDto
    {
        public int SSinifID { get; set; }
        public int OgrenciID { get; set; }
        public Nullable<System.DateTime> KatilimIlk { get; set; }
        public Nullable<System.DateTime> KatilimSon { get; set; }
        public Nullable<int> CanliIzleme { get; set; }
        public Nullable<int> TekrarIzleme { get; set; }
        public string IzlemeKey { get; set; }
    }
}
