﻿using Sabis.Bolum.Core.ENUM.ABS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.SANALSINIF.Dto
{
    public class SorunBildirimDto
    {
        public string AdSoyad { get; set; }
        public string Baslik { get; set; }
        public string Icerik { get; set; }
        public string DosyaKey { get; set; }
        public DateTime Tarih { get; set; }
        public EnumGeriBildirimDurum Durum { get; set; }
        public string Cevap { get; set; }
        public int SanalSinifID { get; set; }
        public int GeriBildirimID { get; set; }
    }
}
