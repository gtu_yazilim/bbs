﻿using Sabis.Bolum.Core.ABS.DERS.OSINAV;
using Sabis.Bolum.Core.ABS.DERS.SANALSINIF.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.SANALSINIF.Dto
{
    public class SanalSinifDto
    {
        public int ID { get; set; }
        public Nullable<int> SunucuID { get; set; }
        public Nullable<int> SSinifKey { get; set; }
        public string Adres { get; set; }
        [Required]
        public string Ad { get; set; }
        [Required]
        public int Hafta { get; set; }
        public bool Yayinlandi { get; set; }
        public bool Silindi { get; set; }
        public string Ekleyen { get; set; }
        public int EkleyenID { get; set; }
        public System.DateTime EklemeTarihi { get; set; }
        public int GuncelleyenID { get; set; }
        public string Guncelleyen { get; set; }
        public System.DateTime GuncellemeTarihi { get; set; }
        public bool Replay { get; set; }
        [Required]
        public System.DateTime Baslangic { get; set; }
        public System.DateTime Bitis { get; set; }
        [Required]
        public int Sure { get; set; }
        [Required]
        public IEnumerable<HocaGruplar> hocagrups { get; set; }
        public int FKDersPlanAnaID { get; set; }
        public int FKDersPlanID { get; set; }
        public int FKDersGrupHocaID { get; set; }
        public bool TumDersGruplarinaEkle { get; set; }

        public bool SorunBildirim { get; set; }
        public bool SauLive { get; set; }
        public int ProviderType { get; set; }
        public int YeniSorunBildirim { get; set; }
        public int OkunanSorunBildirim { get; set; }
        public int TamamlananSorunBildirim { get; set; }
    }
}
