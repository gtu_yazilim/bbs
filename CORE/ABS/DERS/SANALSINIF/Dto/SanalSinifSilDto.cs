﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.SANALSINIF.Dto
{
    public class SanalSinifSilDto
    {
        public SanalSinifDto sanalsinif { get; set; }
        public IEnumerable<SanalSinifGrupDto> sanalsinif_gruplar { get; set; }
        public int FKDersGrupHocaID { get; set; }
    }
    public class SanalSinifGrupDto
    {
        public int GrupID { get; set; }
        public string GrupAd { get; set; }
        public int OgretimTuru { get; set; }
    }
}
