﻿using Sabis.Bolum.Core.ABS.DERS.SANALSINIF.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.SANALSINIF.Ogrenci
{
    public class OgrenciSanalSinif
    {
        public int SSinifID { get; set; }
        public Nullable<int> SSinifKey { get; set; }
        public string Ad { get; set; }
        public string Adres { get; set; }
        public System.DateTime Baslangic { get; set; }
        public int Hafta { get; set; }
        public int Sure { get; set; }
        public string DersAd { get; set; }
        public int DersGrupID { get; set; }
    }

    //public class EnrollVm
    //{
    //    public int EId { get; set; }
    //    public string CourseName { get; set; }
    //    public int CourseId { get; set; }
    //    public string Name { get; set; }
    //    public string Url { get; set; }
    //    public int VClassId { get; set; }
    //    public int CeId { get; set; }
    //    public int ActiveVClassId { get; set; }
    //    public string StatusMessage { get; set; }
    //    public List<LMSContent> Contents { get; set; }
    //}

    //public class EnrollItem
    //{
    //    public SanalSinifDto Vitem { get; set; }
    //    public SanalSinifOgrenciDto en { get; set; }
    //}
}
