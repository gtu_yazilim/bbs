﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.DERSDETAY
{
    public class DokumanVM
    {
        public DokumanVM()
        {
            DokumanDetay = new List<DokumanDetayVM>();
        }
        public int Yil { get; set; }
        public int Donem { get; set; }
        
        public string DersinAmaci { get; set; }
        public string DersinIcerigi { get; set; }
        public List<DokumanDetayVM> DokumanDetay { get; set; }
    }

    public class DokumanDetayVM
    {
        public int Dil { get; set; }
        public string HaftaAd { get; set; }
        public string HaftaAciklama { get; set; }
        public int HaftaNo { get; set; }
    }
}
