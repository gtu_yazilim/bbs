﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.DERSDETAY
{
    public class PayBilgileriVM
    {
        public int? PayID { get; set; }
        public int? EnumCalismaTipi { get; set; }
        public int? SiraNo { get; set; }
        public int? KatkiOrani { get; set; }
        public int NotuGirilenOgrenciSayisi { get; set; }
        public int ToplamOgrenciSayisi { get; set; }
        public bool YayinlanmaDurumu { get; set; }
        public int? Donem { get; set; }
        public int? DersPlanID { get; set; }
        public int? DersGrupID { get; set; }
        public int? DersPlanAnaID { get; set; }
        public int? DersPlanAnaUstID { get; set; }
        public int? PersonelID { get; set; }
        public int Yil { get; set; }
        public bool TarihAraligiUygunMu { get; set; }
        public int? EnumDersTipi { get; set; }
        public int? KatKiOraniPay { get; set; }
        public int? KatKiOraniPayda { get; set; }
        public DateTime? Zaman { get; set; }
        public string KullaniciAdi { get; set; }
        public string IP { get; set; }
        public bool NotGirilmisMi { get; set; }
        public bool EskiNotGirisiAktif { get; set; }
    }
}
