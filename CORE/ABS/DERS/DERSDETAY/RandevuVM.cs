﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.DERSDETAY
{
    public class RandevuVM
    {
        public int ID { get; set; }
        public int FKPayID { get; set; }
        public int FKDersGrupID { get; set; }
        public int FKRandevuID { get; set; }
        public int EnumCalismaTipi { get; set; }
    }
}
