﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.DERSDETAY
{
    public class YoklamaVM
    {
        public YoklamaVM()
        {
            ListYoklamaDetay = new List<YoklamaDetayVM>();
        }
        public int OgrenciID { get; set; }
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public string Numara { get; set; }

        public List<YoklamaDetayVM> ListYoklamaDetay { get; set; }
    }

    public class YoklamaDetayVM
    {
        public int? DevamTakipID { get; set; }
        public int Yil { get; set; }
        public int Donem { get; set; }
        public int FKDersProgramV2ID { get; set; }
        public int FKOgrenciID { get; set; }
        public string IP { get; set; }
        public string Kullanici { get; set; }
        public DateTime? YoklamaTarihi { get; set; }
        public bool Secili { get; set; }
        public string DersSaati { get; set; }
    }
}
