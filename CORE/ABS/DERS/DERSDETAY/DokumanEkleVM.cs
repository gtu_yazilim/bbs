﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.DERSDETAY
{
    public class DokumanEkleVM
    {
        public string Aciklama { get; set; }
        public string DokumanAdi { get; set; }
        public byte[] Dokuman { get; set; }
        public decimal DokumanBoyutu { get; set; }
        public string EkleyenKullaniciAdSoyad { get; set; }
        public int EkleyenKullaniciID { get; set; }
        public int EnumDokumanTur { get; set; }
        public int Yil { get; set; }
        public int Donem { get; set; }
        public int Hafta { get; set; }
        public bool YayinDurumu { get; set; }
        public bool SilindiMi { get; set; }
        public int? DersPlanAnaID { get; set; }
        public int? DersPlanID { get; set; }
    }
}
