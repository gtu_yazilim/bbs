﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.DERSDETAY
{
    public class SecilenDersVM
    {
        public SecilenDersVM()
        {
            DersDetay = new List<DersDetayVM>();
        }
        public string DersAd { get; set; }

        public List<DersDetayVM> DersDetay { get; set; }

        public string DersKod { get; set; }

        public string BolKod { get; set; }

        public int? Akts { get; set; }
    }
}
