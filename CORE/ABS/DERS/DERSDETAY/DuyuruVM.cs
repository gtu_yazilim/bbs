﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.DERSDETAY
{
    public class DuyuruVM
    {
        public int ID { get; set; }
        public int? FKPersonelID { get; set; }
        public int? FKDersGrupID { get; set; }
        public string Konu { get; set; }
        public string Mesaj { get; set; }
        public DateTime? Tarih { get; set; }
        public int? Yil { get; set; }
        public int? Donem { get; set; }
        public string EkleyenIP { get; set; }
    }
}
