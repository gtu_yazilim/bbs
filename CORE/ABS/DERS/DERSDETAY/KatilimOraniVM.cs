﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.DERSDETAY
{
    public class KatilimOraniVM
    {
        public KatilimOraniVM()
        {
            KatilimOraniHaftaVM = new List<KatilimOraniHaftaVM>();
        }
        public string AdSoyad { get; set; }
        public int OgrenciID { get; set; }
        public string Numara { get; set; }
        public List<KatilimOraniHaftaVM> KatilimOraniHaftaVM { get; set; }
        public double Orani { get; set; }
    }

    public class KatilimOraniHaftaVM
    {
        public int KatilimSaati { get; set; }
        public DateTime Tarih { get; set; }
        public int ToplamDersSaati { get; set; }
    }
}
