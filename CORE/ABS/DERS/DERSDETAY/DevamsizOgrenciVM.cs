﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.DERSDETAY
{
    public class DevamsizOgrenciVM
    {
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public bool Devamsiz { get; set; }
        public string Numara { get; set; }
        public int? OgrenciID { get; set; }
        public int? PayID { get; set; }
        public int? CalismaTip { get; set; }
        public int? DersPlanAnaID { get; set; }
        public int? DersGrupID { get; set; }
        public int? DersPlanID { get; set; }
        public double? Notu { get; set; }
        public int YazilmaID { get; set; }
        public bool Aktif { get; set; }
    }
}
