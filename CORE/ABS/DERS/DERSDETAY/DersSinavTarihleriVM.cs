﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sabis.Enum;

namespace Sabis.Bolum.Core.ABS.DERS.DERSDETAY
{
    public class DersSinavTarihleriVM
    {
        public EnumAkademikTarih EnumAkademikTarih { get; set; }
        public DateTime TarihBaslangic { get; set; }
        public DateTime? TarihBitis { get; set; }
    }
}
