﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.DERSDETAY
{
    public class HiyerarsikPayDersListVM
    {
        public int DersPlanAnaID { get; set; }
        public string DersAd { get; set; }
        public int? UstDersID { get; set; }
        public List<PayVM> Paylar { get; set; }
    }
    public class PayVM
    {
        public int? ID { get; set; }
        public int? EnumCalismaTip { get; set; }
        public byte? Oran { get; set; }
        public int? Yil { get; set; }
        public int? EnumDonem { get; set; }
        public int? FKPersonelID { get; set; }
        public int? FKDersPlanAnaID { get; set; }
        public int? AltDersPlanAnaID { get; set; }
        public int? FKDersPlanID { get; set; }
        public int? FKDersGrupID { get; set; }
        public byte? Sira { get; set; }
        public DateTime? Zaman { get; set; }
        public string KullaniciAdi { get; set; }
        public string IP { get; set; }
        public int? Pay { get; set; }
        public int? Payda { get; set; }
    }
}
