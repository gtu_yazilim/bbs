﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.DERSDETAY
{
    public class PayHiyerarsikVM
    {
        public int? ID { get; set; }
        public int? EnumCalismaTip { get; set; }
        public byte? Oran { get; set; }
        public int? Yil { get; set; }
        public int? EnumDonem { get; set; }
        public int? FKPersonelID { get; set; }
        public int? FKDersPlanAnaID { get; set; }
        public int? FKDersPlanID { get; set; }
        public int? FKDersGrupID { get; set; }
        public byte? Sira { get; set; }
        public int? FKAltDersPlanAnaID { get; set; }
        public PayNotAdetVM PayNotAdet { get; set; }
        public bool YayinlanmaDurumu { get; set; }
        public int? Payda { get; set; }
        public int? Pay { get; set; }
    }

    public class PayNotAdetVM
    {
        public int PayID { get; set; }
        public int Toplam { get; set; }
        public int Girilen { get; set; }
    }

    public class HiyerarsikDersListesiVM
    {
        public int DersPlanAnaID { get; set; }
        public string DersAd { get; set; }
        public List<PayHiyerarsikVM> PayListesi { get; set; }
        public List<AltDersListesi> AltDersListesi { get; set; }
    }

    public class AltDersListesi
    {
        public int DersPlanAnaID { get; set; }
        public string DersAd { get; set; }
    }
}
