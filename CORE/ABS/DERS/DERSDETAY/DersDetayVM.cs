﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.Serialization;

namespace Sabis.Bolum.Core.ABS.DERS.DERSDETAY
{
    public class DersDetayVM
    {
        public int DersGrupHocaID { get; set; }
        public int DersGrupID { get; set; }
        public int DersPlanAnaID { get; set; }
        public int DersPlanID { get; set; }
        public string BolumKod { get; set; }
        public string DersKod { get; set; } 
        public string DersAd { get; set; }
        public double Kredi { get; set; }
        public string GrupAd { get; set; }
        public int EnumOgretimTur { get; set; }
        public int Yil { get; set; }
        public int? Yariyil { get; set; }
        public int Donem { get; set; }
        public string FakulteAd { get; set; }
        public string BolumAd { get; set; }
        public string ProgramAd { get; set; }
        public int? FakulteID { get; set; }
        public int? BolumID { get; set; }
        public int? ProgramID { get; set; }
        public int OgrenciSayisi { get; set; }
        public int OgrenciSayisiButunleme { get; set; }
        public int? EnumDersPlanZorunlu { get; set; }
        public int? EnumOgretimSeviye { get; set; }
        public int? EnumDersPlanOrtalama { get; set; }
        public int? DersSaati { get; set; }
        public int? UygulamaSaati { get; set; }
        public DateTime? FinalTarihi { get; set; }
        public bool? UzemMi { get; set; }
        public int? PersonelID { get; set; }
        public bool ButunlemeMiOrijinal { get; set; }
        public bool Hiyerarsik { get; set; }
        public int? HiyerarsikKokID { get; set; }
        public int? EnumKokDersTipi { get; set; }
        public string Koordinator { get; set; }
        public int? DersProgramiv2ID { get; set; }
        public string KoordinatorAdSoyad { get; set; }
        public int? FKKoordinatorID { get; set; }
        public int? EnumDersSureTip { get; set; }
        public string SinifAdi { get; set; }
        public int DersGrupBirimID { get; set; }
        public bool SonHalVerildiMi { get; set; }
        public int? EnumDegerlendirmeTip { get; set; }
    }
}
