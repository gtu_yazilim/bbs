﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sabis.Bolum.Core.ABS.DERS.HOME
{
    public class AcilanDerslerExcelVM
    {
        public int Sira { get; set; }
        public string DersKodu { get; set; }
        public string DersAdi { get; set; }
        public string GrupAdi { get; set; }
        public double GTUKredi { get; set; }
        public double AKTSKredi { get; set; }
        public string DUSaat { get; set; }
        public string SecimTipi { get; set; }
        public string HocaAdi { get; set; }
        public string Etiketler { get; set; }
        public string Mufredatlar { get; set; }
        public int OgrenciSayisi { get; set; }
    }
}