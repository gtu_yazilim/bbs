﻿using System.Collections.Generic;
namespace Sabis.Bolum.Core.ABS.DERS.HOME
{
    public class HocaDersListesiViewModelv2
    {
        public int Sira { get; set; }
        public int? DersGrupHocaID { get; set; }
        public string BolumKod { get; set; }
        public string DersKod { get; set; }
        public string DersAd { get; set; }
        public string DersYabAd { get; set; }
        public string GorunenDersAd { get; set; }
        public double ECTSKredi { get; set; }
        public double Kredi { get; set; }
        public string GrupAd { get; set; }
        public int? EnumOgretimTur { get; set; }
        public string BolumAd { get; set; }
        public int OgrenciSayisi { get; set; }
        public int OgrenciSayisiButunleme { get; set; }
        public int Yil { get; set; }
        public int Donem { get; set; }

        public int DersGrupID { get; set; }

        public int? EnumDersPlanZorunlu { get; set; }

        public int? FakulteID { get; set; }

        public string FakulteAd { get; set; }

        public int? ProgramID { get; set; }

        public int? DSaat { get; set; }

        public int? USaat { get; set; }

        public int? YariYil { get; set; }

        public int? EnumWebGosterim { get; set; }

        public int? EnumSeviye { get; set; }
        public string ProgramAd { get; set; }
        public int DPAnaID { get; set; }
        public int? DPAnaUstID { get; set; }
        public bool ButunlemeMi { get; set; }

        public int? EnumSonHal { get; set; }
        public bool YayinlanmaDurumu { get; set; }
        public bool Hiyerarsik { get; set; }
        public int? KokDPAnaID { get; set; }
        public List<DersDegTemp> DersDegTempList { get; set; }
        public int? EnumKokDersTipi { get; set; }
        public List<string> DersEtiketleri { get; set; }
        public List<string> Mufredatlar { get; set; }
        public string DersSecimTipi { get; set; }
        public List<string> HocaListesi { get; set; }
        public string Seviye { get; set; }
    }

    public class DersDegTemp
    {
        public int ID { get; set; }
        public bool Butunleme { get; set; }
        public int? EnumSonHal { get; set; }
    }
}