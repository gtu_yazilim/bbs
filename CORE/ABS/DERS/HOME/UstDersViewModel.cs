﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.HOME
{
    public class UstDersViewModel
    {
        public int DersPlanAnaID { get; set; }
        public int? FKUstDersPlanAnaID { get; set; }
        public string DersAd { get; set; }
        public List<UstDersViewModel> AltDersList { get; set; }
        public List<HocaDersListesiViewModelv2> DersGrupList { get; set; }
        public int? KokDersPlanAnaID { get; set; }
        public bool Hiyerarsik { get; set; }
    }
}
