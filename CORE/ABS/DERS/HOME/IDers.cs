﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS.HOME
{
    public interface IDers
    {
        List<UstDersViewModel> ListelePersonelDersleriv2(int personelID, int Yil, int Donem);
        List<UstDersViewModel> ListeleAcilanPersonelDersleri(int personelID, int Yil, int Donem);
        List<AcilanDerslerExcelVM> AcilanDerslerExcelFormat(List<UstDersViewModel> ustDersViewModel);
    }
}
