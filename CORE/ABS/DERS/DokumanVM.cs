﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.DERS
{
    public class DokumanVM
    {
        public int? ID { get; set; }
        public string DosyaAdi { get; set; }
        public string DosyaKey { get; set; }

        public string DosyaAdres { get; set; }
        public string Aciklama { get; set; }
        public string DosyaTuru { get; set; }
        public double DosyaBoyutu { get; set; }
        public int EnumIcerikTipi { get; set; }
        public int Yil { get; set; }
        public int EnumDonem { get; set; }
        public int? Hafta { get; set; }
        public bool DigerHocalarGorebilir { get; set; }
        public bool EBSGosterilsin { get; set; }
        public bool DigerGruplaraEkle { get; set; }
        public bool TumGruplaraEkle { get; set; }
        public int? FKDersPlanAnaID { get; set; }
        public int? FKDersGrupHocaID { get; set; }
        public List<int> FKDersGrupIDList { get; set; }
        public string EkleyenAdSoyadUnvan { get; set; }
        public int FKPersonelID { get; set; }
        public string SGKullanici { get; set; }
        public string SGIP { get; set; }
        public DateTime SGTarih { get; set; }
        public bool Silindi { get; set; }
    }
}
