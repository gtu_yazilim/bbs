﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.OGRENCI
{
    public class SecilenDersDetayVM
    {
        public int? CalismaTip { get; set; }

        public int PayID { get; set; }

        public byte? Oran { get; set; }

        public int? Notu { get; set; }

        public string CalismaTipAd { get; set; }
    }
}
