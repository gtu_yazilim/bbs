﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.OGRENCI
{
    public class SecilenDerslerVM
    {
        public SecilenDerslerVM()
        {
            DersDetay = new List<SecilenDersDetayVM>();
        }
        public string DersAd { get; set; }

        public List<SecilenDersDetayVM> DersDetay { get; set; }

        public string DersKod { get; set; }

        public string BolKod { get; set; }

        public double? Akts { get; set; }

        public double? Kredi { get; set; }
    }
}
