﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.OGRENCI
{
    public class OgrenciBilgileriExcelVM
    {
        public string Numara { get; set; }
        public string AdSoyad { get; set; }
        public string GirisYili { get; set; }
        public string Sinif { get; set; }
        public string AktifYariyil { get; set; }
        public string OgrencilikStatusu { get; set; }

    }

}
