﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.OGRENCI
{
    public class OgrenciListesiVM
    {
        public int OgrenciID { get; set; }
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public string Numara { get; set; }
        public string FotoAdres { get; set; }
        public string KullaniciAdi { get; set; }
        public int? KullaniciID { get; set; }
        public int KisiID { get; set; }
        public string OgrenciBirim { get; set; }
        public int? GirisYil { get; set; }
        public int? EnumArsivDurum { get; set; }
        public string BagliOlduguYonetmelik { get; set; }
        public string DanismanAdSoyad { get; set; }
        public string BagliOlduguMufredat { get; set; }
        public string Telefon { get; set; }
    }

    public class DersOgrenciListesiVM : OgrenciListesiVM
    {
        public string Fakulte { get; set; }
        public string Bolum { get; set; }
        public int FKBirimID { get; set; }
        public string OgretimGorevlisi { get; set; }
        public string Grubu { get; set; }
        public int FKDersGrupID { get; set; }
    }

    public class HiyerarsikOgrenciList : OgrenciListesiVM
    {
        public int? FKDersPlanAnaID { get; set; }
        public int? FKDersPlanID { get; set; }
        public int? FKDersGrupID { get; set; }
    }

    public class OgrenciBilgileri : OgrenciListesiVM
    {
        public string BolumAdi { get; set; }
        public int? KayitYili { get; set; }
        public string Sinif { get; set; }
        public int AktifYariyil { get; set; }
        public string EnumOgrencilikStatusuDesc { get; set; }
        public int EnumOgrencilikStatusu { get; set; }
        public int EnumSeviye { get; set; }
    }
}
