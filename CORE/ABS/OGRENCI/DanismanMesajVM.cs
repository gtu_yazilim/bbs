﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.OGRENCI
{
    public class DanismanMesajVM
    {
        public int FKOgrenciID { get; set; }
        public int FKPersonelID { get; set; }
        public bool GonderenOgrenci { get; set; }
        public int ID { get; set; }
        public string Mesaj { get; set; }
        public string OgrenciAdSoyad { get; set; }
        public string OgrenciNumara { get; set; }
        public string PersonelUnvanAdSoyad { get; set; }
        public DateTime Tarih { get; set; }
        public DateTime? TarihOkunma { get; set; }
    }
}
