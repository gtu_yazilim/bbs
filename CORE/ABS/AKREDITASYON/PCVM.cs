﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.AKREDITASYON
{
    public class PCVM
    {
            public int ID { get; set; }
            public int BirimID { get; set; }
            public int Yil { get; set; }
            public int EnumDonem { get; set; }
            public string PCNo { get; set; }
            public double PCBasariYuzde { get; set; }
    }
}
