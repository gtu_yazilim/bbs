﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.AKREDITASYON
{
    public class BirimDersListesiVM
    {
        public List<BirimDersListVM> ZorunluDersListe { get; set; }
        public List<BirimDersListVM> SecmeliDersListe { get; set; }
    }
    public class BirimDersListVM
    {
        public int ID { get; set; }
        public int BirimID { get; set; }
        public int Yil { get; set; }
        public int EnumDonem { get; set; }
        public string DersAd { get; set; }
        public int? EnumDersPlanAnaZorunlu { get; set; }
        public double? DersAKTS { get; set; }
        public List<BirimPCListe> DersPCList { get; set; }

    }
    public class BirimPCListe
    {
        public int ID { get; set; }
        public int BirimID { get; set; }
        public int PCNo { get; set; }
        public string PCIcerik { get; set; }
        public int? KatkiDuzey { get; set; }
    }
    public class DersOCListe
    {
        public int ID { get; set; }
        public int DersPlanAnaID { get; set; }
        public int OCNo { get; set; }
        public string OCIcerik { get; set; }
        public int? KatkiDuzey { get; set; }
    }
}
