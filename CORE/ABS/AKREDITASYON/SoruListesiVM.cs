﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.AKREDITASYON
{
    public class SoruListesiVM
    {
        public string Soru { get; set; }
        public double? SoruPuan { get; set; }
        public double? SoruPuanOrtalama { get; set; }
        public string Renk { get; set; }
    }

}
