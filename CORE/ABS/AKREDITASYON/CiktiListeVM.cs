﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.AKREDITASYON
{
    public class CiktiListeVM
    {
        public int? ID { get; set; }
        public string Cikti { get; set; }
        public int SoruID { get; set; }
        public double? CiktiToplamPuan { get; set; }
        public double? CiktiPuanOrtalama { get; set; }
        public int Sira { get; set; }
        public int? CiktiSoru { get; set; }
        public string Renk { get; set; }
        public int? FKDersPlanAnaID { get; set; }
        public int? KatkiDuzeyi { get; set; }
    }
}
