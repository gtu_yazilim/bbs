﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.AKREDITASYON
{
    public class ToplamSonucVM
    {
        public int EnumCalismaTip { get; set; }
        public string CalismaTip { get; set; }
        public int FKABSPaylarID { get; set; }
        public List<SoruToplamListeVM> SoruListesi { get; set; }
        public List<PCToplamListeVM> PCListe { get; set; }
        public List<OCToplamListeVM> OCListe { get; set; }
        //public double GenelPCPuan { get; set; }
        public List<MutlakSonucVM> MutlakSonuc { get; set; }
        public double? MutlakOrtalamaYuzde { get; set; }
        public double? MutlakGenelortalamaYuzde { get; set; }
    }

    public class MutlakSonucVM
    {
        public string Ad { get; set; }
        public double? Sonuc { get; set; }
    }

    public class SoruToplamListeVM
    {
        public int ID { get; set; }
        public string SoruNo { get; set; }
        public double SoruPuan { get; set; }
        public int SoruPCSayisi { get; set; }
        public int SoruOCSayisi { get; set; }
        public double SoruOrtalamaNot { get; set; }
        public double? SoruBasariYuzde { get; set; }
        public double? SoruBasariYuzdeGenel { get; set; }
    }

    public class PCToplamListeVM
    {
        public int? ID { get; set; }
        public string PCNo { get; set; }
        public double PCPuan { get; set; }
        public double PCBasariYuzde { get; set; }
        public double PCBasariYuzdeGenel { get; set; }
    }

    public class OCToplamListeVM
    {
        public int? ID { get; set; }
        public string OCNo { get; set; }
        public double OCPuan { get; set; }
        public double OCBasariYuzde { get; set; }
        public double OCBasariYuzdeGenel { get; set; }
    }
}
