﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.SORUNBILDIRIM.Dto
{
    public class SorunListeleme
    {
        public int FKSinavID { get; set; }
        public int FKDersGrupID { get; set; }
        public int EnumModulTipi { get; set; }
        public int EnumGeriBildirimDurum { get; set; }
        public int FKOgrenciID { get; set; }
        public string OgrenciNo { get; set; }
        public string AdSoyad { get; set; }
        public string Baslik { get; set; }
        public string Icerik { get; set; }
        public string EklemeTarih { get; set; }
        public string DosyaKey { get; set; }
    }
}
