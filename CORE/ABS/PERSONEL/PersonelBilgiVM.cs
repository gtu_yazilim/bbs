﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ABS.PERSONEL
{
    public class PersonelBilgiVM
    {
        public int ID { get; set; }
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public string Unvan { get; set; }
        public string UnvanAdSoyad { get; set; }
        public string KullaniciAdi { get; set; }
        public int? EnumPersonelTipi { get; set; }
        public string SicilNo { get; set; }
        public int? UnvanID { get; set; }
        public int? FK_GorevFakID { get; set; }
        public int? FK_GorevBolID { get; set; }
        public int? FK_GorevAbdID { get; set; }
        public int? FKGorevBirimID { get; set; }
    }
}
