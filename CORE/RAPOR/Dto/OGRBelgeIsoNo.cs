﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.RAPOR.Dto
{
    public class OGRBelgeIsoNo
    {
        public int ID { get; set; }
        public Nullable<System.DateTime> Zaman { get; set; }
        public string Kullanici { get; set; }
        public string Makina { get; set; }
        public Nullable<bool> AktarilanVeriMi { get; set; }
        public Nullable<int> InKod { get; set; }
        public Nullable<int> FormNo { get; set; }
        public string Parametre1 { get; set; }
        public string Parametre2 { get; set; }
        public Nullable<System.DateTime> SGZaman { get; set; }
        public string SGKullanici { get; set; }
    }
}
