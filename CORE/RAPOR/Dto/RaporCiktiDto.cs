﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.RAPOR
{
    public class RaporCiktiDto
    {
        public byte[] Pdf { get; set; }
        public Uri Url { get; set; }
        public string DosyaAdi { get; set; }
    }
}
