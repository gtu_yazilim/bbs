﻿using Sabis.Bolum.Core.RAPOR.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.RAPOR
{
    public class DosyaDto
    {
        public int ID { get; set; }
        public System.DateTime TarihKayit { get; set; }
        public string DosyaKey { get; set; }
        public virtual ICollection<BelgeSayiDto> BelgeSayiDto { get; set; }
    }
}
