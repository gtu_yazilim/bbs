﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.RAPOR.Dto
{
    public class BelgeSayiDto
    {
        public int ID { get; set; }
        public Nullable<System.DateTime> Zaman { get; set; }
        public string Kullanici { get; set; }
        public string Makina { get; set; }
        public Nullable<int> Adet { get; set; }
        public Nullable<System.DateTime> Tarih { get; set; }
        public Nullable<int> Sayi { get; set; }
        public Nullable<int> EnumBelgeTuru { get; set; }
        public Nullable<int> EnumBelgeDil { get; set; }
        public Nullable<int> FKOgrenciID { get; set; }
        public Nullable<int> FKDosyaID { get; set; }
        public virtual DosyaDto DosyaDto { get; set; }
    }
}
