﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.RAPOR.Dto
{
    public class BelgeOgrenciDto : BelgeDto
    {
        public OgrenciKimlikDto OgrenciKimlik { get; set; }
        public string EkAciklama { get; set; }
    }
}
