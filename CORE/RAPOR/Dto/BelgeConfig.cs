﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.RAPOR.Dto
{
    public class BelgeConfig
    {
        public Enum.EnumDil EnumDil { get; set; }
        public Enum.EnumBelgeTuru EnumBelgeTuru { get; set; }
        public int Adet { get; set; }
        public bool DogrulamaKoduEkle { get; set; }
        public bool IkiAdimBelgeDogrulama { get; set; }
        public DateTime Tarih { get; set; }
    }
}
