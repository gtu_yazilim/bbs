﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.RAPOR
{
    public class BelgeDto
    {
        public DateTime Tarih { get; set; }
        public string SayiOnEk { get; set; }
        public int Sayi { get; set; }
        public string DogrulamaKodu { get; set; }
        public KeyValuePair<string, string> DogrulamaAnahtari { get; set; }
        public Enum.EnumBelgeTuru EnumBelgeTuru { get; set; }
        public string DosyaAdi { get; set; }
        public int? BelgeSayiID { get; set; }
    }
}
