﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Abs.Core.RAPOR.Dto
{
    public class OGRDosya
    {
        public OGRDosya()
        {
            this.OGRBelgeSayi = new HashSet<OGRBelgeSayi>();
        }

        public int ID { get; set; }
        public System.DateTime TarihKayit { get; set; }
        public string DosyaKey { get; set; }

        public virtual ICollection<OGRBelgeSayi> OGRBelgeSayi { get; set; }
    }
}
