﻿using Sabis.Bolum.Core.ENUM.RAPOR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.RAPOR
{
    public class RaporDto
    {
        public BelgeDto Data { get; private set; }
        public RaporDto(BelgeDto data)
        {
            Data = data;
        }

        public Tuple<string, string, EnumRaporServisTuru> RaporServisKey { get; set; }
    }
}
