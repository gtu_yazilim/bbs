﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.RAPOR
{
    public interface IDosyaIslem
    {
        DosyaDto Kaydet(byte[] dosya, string dosyaKey, Dictionary<string, string> sdfsFileAttributes);
        Uri UrlOlustur(string dosyaKey);
    }
}
