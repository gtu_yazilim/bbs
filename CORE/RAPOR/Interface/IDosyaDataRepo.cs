﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.RAPOR
{
    public interface IDosyaDataRepo
    {
        DosyaDto Kaydet(string dosyaKey);
    }
}
