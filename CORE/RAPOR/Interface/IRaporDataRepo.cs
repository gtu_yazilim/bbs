﻿using Sabis.Bolum.Core.ENUM.RAPOR;
using Sabis.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.RAPOR
{
    public interface IRaporDataRepo
    {
        Tuple<string, string, EnumRaporServisTuru> GetirRaporServisKey(EnumBelgeTuru enumBelgeTuru, List<int> HiyerarsikBirimIDList = null, EnumOgretimSeviye? enumSeviye = null, EnumDil enumDil = EnumDil.TURKCE);
    }
}
