﻿using Sabis.Bolum.Core.RAPOR.Dto;
using Sabis.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.RAPOR
{
    public interface IRaporIslem
    {
        IslemSonuc<RaporCiktiDto> Olustur(BelgeDto belgeDto, BelgeConfig config, int birimID, Dictionary<string, string> sdfsFileAttributes = null);

        IslemSonuc<RaporCiktiDto> raporlariBirlestir(List<Tuple<RaporCiktiDto, int>> raporList, string yenidosyaadi = "Birleştirilmiş Rapor", string sayfatipi = "a4", bool yatay = false);

        IslemSonuc<RaporCiktiDto> raporlariBirlestir(Tuple<RaporCiktiDto, int> rapor, string sayfatipi = "a4", bool yatay = false);

        IslemSonuc<RaporCiktiDto> Olustur<T>(List<T> belgeDto, BelgeConfig config, int birimID, Dictionary<string, string> sdfsFileAttributes = null) where T : BelgeDto;
    }
   
}
