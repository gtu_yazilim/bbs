﻿using Sabis.Bolum.Core.RAPOR.Dto;
using Sabis.Core;
using Sabis.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.RAPOR
{
    public interface IBelgeIslem
    {
        BelgeAyarDto GetirBelgeAyar(EnumBelgeTuru enumBelgeTuru, int birimID, EnumDil enumDil = EnumDil.TURKCE);

        int GetirBelgeSayi();

        //IslemSonuc<string> GetirBelgeSayi(int birimID, out int maxBelgeSayisi);

        //OGRBelgeSayi KaydetBelgeSayi(BelgeOgrenciDto belge, BelgeConfig config);

        IslemSonuc<bool> KaydetBelgeSayi(OGRBelgeSayi ogrBelge);

        void GuncelleBelgeSayiWithDosyaID(int FKDosyaID, int BelgeSayiID);

        string OlusturDogrulamaKodu();

        List<BelgeSayiDto> GetirBelgelerOgrenciID(int ogrenciID, EnumBelgeTuru belgeTuru);

        IslemSonuc<byte[]> CombineMultiplePDFs(List<byte[]> files);

        //IslemSonuc<OGRTNMBelgeDto> getirBelge(int? fakID);

        //IslemSonuc<bool> kaydetDosyaSayiBul(OGRDosya dosya, int sayi, int yil);

        List<OGRBelgeIsoNo> getirBelgeIsoNo(int formNo);

        OGRBelgeIsoNo getirBelgeIsoNoIDIle(int ID);
    }
}
