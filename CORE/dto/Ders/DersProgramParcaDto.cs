﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.dto.Ders
{
    public class DersProgramParcaDto
    {
        public int DersProgramID { get; set; }
        public DateTime Tarih { get; set; }
        public int Saat { get; set; }
    }
}
