﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.dto.Ders
{
    public class DersProgramiDto
    {
        public int ID { get; set; }
        public DateTime Baslangic { get; set; }
        public DateTime Bitis { get; set; }
        public DersBilgisiDto DersBilgisi { get; set; }
        public Mekan.MekanDto Mekan { get; set; }
        public List<DersProgramParcaDto> ParcaList { get; set; }
    }
    public class HaftalikDersProgramiDto
    {
        public int ID { get; set; }
        public string DersBilgisi { get; set; }
        public DateTime Baslangic { get; set; }
        public DateTime Bitis { get; set; }
        public string Color { get; set; }
        public int FKDersGrupHocaID { get; set; }
        public string Mekan { get; set; }
    }
}
