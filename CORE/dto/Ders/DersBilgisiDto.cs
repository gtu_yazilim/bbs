﻿using Sabis.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.dto.Ders
{
    public class DersBilgisiDto
    {
        public int DersGrupID { get; set; }
        public string DersAd { get; set; }
        public string DersKod { get; set; }
        public int DersPlanAnaID { get; set; }
        public EnumOgretimTur EnumOgretimTur { get; set; }
        public int FKBirimID { get; set; }
        public Sabis.Core.DtoBirimler Birim { get; set; }
        public int DersGrupHocaID { get; set; }
    }
}
