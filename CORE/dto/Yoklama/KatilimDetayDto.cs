﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.dto.Yoklama
{
    public class KatilimDetayDto
    {
        public int DersProgramID { get; set; }
        public DateTime Tarih { get; set; }
        public bool Durum { get; set; }
    }
}
