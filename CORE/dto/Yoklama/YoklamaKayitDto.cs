﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.dto.Yoklama
{
    public class YoklamaKayitDto
    {
        public List<int> OgrenciIDList { get; set; }
        public List<int> DersProgramIDList { get; set; }
    }
}
