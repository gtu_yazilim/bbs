﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sabis.Bolum.Core.dto.Ogrenci;

namespace Sabis.Bolum.Core.dto.Yoklama
{
    public class KatilimOraniDto
    {
        public KatilimOraniDto()
        {
            HaftaList = new List<KatilimOraniHaftaDto>();
        }
        public OgrenciBilgiDto Ogrenci { get; set; }
        public List<KatilimOraniHaftaDto> HaftaList { get; set; }
        public double Orani { get; set; }
    }
}
