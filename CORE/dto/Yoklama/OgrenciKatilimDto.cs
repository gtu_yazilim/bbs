﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.dto.Yoklama
{
    public class OgrenciKatilimDto
    {
        public dto.Ogrenci.OgrenciBilgiDto Ogrenci { get; set; }
        public List<KatilimDetayDto> DetayList { get; set; }
    }
}
