﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.dto.Yoklama
{
    public class KatilimOraniHaftaDto
    {
        public int KatilimSaati { get; set; }
        public DateTime Tarih { get; set; }
        public int ToplamDersSaati { get; set; }
    }
}
