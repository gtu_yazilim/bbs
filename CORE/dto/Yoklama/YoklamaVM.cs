﻿using Sabis.Bolum.Core.ABS.DERS.DERSDETAY;
using Sabis.Bolum.Core.ABS.OGRENCI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.dto.Yoklama
{
    public class YoklamaVM
    {
        public DersDetayVM DersDetay { get; set; }
        public DateTime Tarih { get; set; }
        public int? Sure { get; set; }
        public string qrCode { get; set; }
        public List<OgrenciListesiVM> OgrenciListe { get; set; }
    }
}
