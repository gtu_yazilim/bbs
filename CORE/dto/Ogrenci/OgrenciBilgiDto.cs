﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.dto.Ogrenci
{
    public class OgrenciBilgiDto
    {
        public int ID { get; set; }
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public string Numara { get; set; }
        public string FotoUrl { get; set; }
        public string KullaniciAdi { get; set; }
        public int KullaniciID { get; set; }
        public int KisiID { get; set; }
        public string BirimAdi { get; set; }
        public string Telefon { get; set; }
    }
}
