﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.dto.Mekan
{
    public class MekanDto
    {
        public int ID { get; set; }
        public string Ad { get; set; }
        public string UzunAd { get; set; }
        public GeoCoordinate Koordinat { get; set; }
    }
}
