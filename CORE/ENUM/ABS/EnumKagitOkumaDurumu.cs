﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ENUM.ABS
{
    public enum EnumKagitOkumaDurumu
    {
        BEKLEMEDE = 1,
        OKUNDU = 2,
        HATA = 3,
        UYARI = 4
    }
}
