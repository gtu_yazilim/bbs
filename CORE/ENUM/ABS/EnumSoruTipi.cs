﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ENUM.ABS
{
    public enum EnumSoruTipi
    {
        BOS = 0,
        KLASIK = 1,
        TEST = 2,
        UYGULAMA_ODEV = 3,
        ONLINESINAV = 4
    }
}
