﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ENUM.ABS
{
    public enum EnumABSNotIslemTipi
    {
        EKLENDI = 1,
        SILINDI = 2,
        GUNCELLENDI = 3,
    }
}
