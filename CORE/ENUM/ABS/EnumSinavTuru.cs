﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ENUM.ABS
{
    public enum EnumSinavTuru
    {
        BELIRSIZ = 0,
        KLASIK = 1,
        TEST = 2,
        ODEV = 3,
        KARMA = 4,
        ONLINESINAV = 5,
        ONLINEODEV = 6,
    }
}
