﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ENUM.ABS
{
    public enum EnumSanalSinifProvider
    {
        [Description("Perculus")]
        PERCULUS = 1,
        [Description("SauLive")]
        SAULIVE = 2
        
    }
}
