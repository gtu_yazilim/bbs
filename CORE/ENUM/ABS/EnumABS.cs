﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ENUM.ABS
{
    public enum EnumGeriBildirimDurum
    {
        [Description("Yeni")]
        YENI = 0,
        [Description("Okundu")]
        OKUNDU = 1,
        [Description("Tamamlandı")]
        TAMAMLANDI = 2
    }

    public enum EnumModulTipi
    {
        [Description("Sınav")]
        SINAV = 0,
        [Description("Ödev")]
        ODEV = 1,
        [Description("Sanal Sınıf")]
        SANAL_SINIF = 2,
        [Description("Döküman")]
        DOKUMAN = 3
    }
    
}
