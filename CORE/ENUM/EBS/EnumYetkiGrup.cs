﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ENUM.EBS
{
    public enum EnumYetkiGrup
    {
        UniversiteKoordinator = 1,
        FakulteKoordinator = 2,
        BolumKoordinator = 3
    }
}
