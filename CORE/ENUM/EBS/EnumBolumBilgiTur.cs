﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ENUM.EBS
{
    public enum EnumBolumBilgiTur
    {
        AMAÇLAR = 1,
        HEDEFLER = 2,
        ALINACAK_DERECE = 3,
        GENEL_KABUL_KOŞULLARI = 4,
        ÖZEL_KABUL_KOŞULLARI = 5,
        ÖNCEKİ_ÖĞRETİMİ_TANIMA = 6,
        MEZUNİYET_KOŞULLARI = 7,
        ÖLÇME_VE_DEĞERLENDİRME = 8,
        İSTİHDAM_OLANAKLARI = 9,
        BÖLÜM_BAŞKANI = 10,
        AKTS_VE_ERASMUS_KOORDİNATÖRÜ = 11
    }
}
