﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ADMIN
{
    public class HaftaVM
    {
        public int ID { get; set; }
        public string HaftaAd { get; set; }
        public DateTime BaslangicTarihi { get; set; }
        public DateTime BitisTarihi { get; set; }
        public string Aciklama { get; set; }
        public int Yil { get; set; }
        public int Donem { get; set; }
        public string EkleyenKullanici { get; set; }
        public int HaftaNumarasi { get; set; }
    }
}
