﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ADMIN
{
    public class DokumanRaporVM
    {
        public int FKDersPlanAnaID { get; set; }
        public string DersAd { get; set; }
        public string DersGrupAd { get; set; }
        public string HocaAdSoyadUnvan { get; set; }
        public int DokumanSayisi { get; set; }
    }
}
