﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ADMIN
{
    public class IPYetkiVM
    {
        public int IpYetkiID { get; set; }
        public string AdSoyad { get; set; }
        public string Aciklama { get; set; }
        public string BirimBolum { get; set; }
        public string IPAdresi { get; set; }
        public string KullaniciAdi { get; set; }
        public DateTime Tarih { get; set; }
        public string YetkiVeren { get; set; }
        public bool YetkiVar { get; set; }
        public bool SimuleYetkisiVarMi { get; set; }
        public bool SonHalKaldirmaVarMi { get; set; }
    }
}
