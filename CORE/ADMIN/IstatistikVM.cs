﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.ADMIN
{
    public class IstatistikVM
    {
        public int Yil { get; set; }
        public int Donem { get; set; }

        public double GrupSayisi { get; set; }
        public double SonHalSayisi { get; set; }
        public double SonHalOran { get; set; }
        public double NotGirisOran { get; set; }

        public double OgrenciSayisi { get; set; }
        public double YazilmaSayisi { get; set; }
        public double YazilmaOran { get; set; }
    }
}
