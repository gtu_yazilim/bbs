﻿using Sabis.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.EBS.BIRIM
{
    public class OGRCapDersPlanDto
    {
        public int ID { get; set; }
        public int FKCapTanimID { get; set; }
        public DateTime TarihKayit { get; set; }
        public string Kullanici { get; set; }
        public int Yil { get; set; }
        public int FKAnaDersPlanAnaID { get; set; }
        public int FKCapDersPlanAnaID { get; set; }
        public int FKAnaDersPlanID { get; set; }
        public int FKCapDersPlanID { get; set; }
        public bool Silindi { get; set; }
        public DateTime TarihSilinme { get; set; }
    }
    public class OGRCapYandalTanimDto
    {
        public int ID { get; set; }
        public int EnumGecisTipi { get; set; }
        public int FKAnaBirimID { get; set; }
        public int UstBirimID { get; set; }
        public string AnaBirimAd { get; set; }
        public string AnaBolumAd { get; set; }
        public int FKAltBirimID { get; set; }
        public string AltBirimAd { get; set; }
        public string AltBolumAd { get; set; }
        public int FKAnaBolumID { get; set; }
        public string FKAnaBolumAd { get; set; }
        public int FKAltBolumID { get; set; }
        public bool Silindi { get; set; }
        public string Kullanici { get; set; }
        public DateTime TarihKayit { get; set; }
        public int BolumID { get; set; }
        public string BolumAd { get; set; }
        public int AltBirimID { get; set; }
        public string FKFakulteAd { get; set; }
        public string FKCapFakulteAd { get; set; }
        public int Kontenjan { get; set; }
    }
    public class BirimDto
    {
        public int BirimID { get; set; }
        public string BirimAdi { get; set; }

        public int AltBirimID { get; set; }
        public string AltBirimAdi { get; set; }
    }
    public class BolumPlanDto
    {
        public int? FKDersPlanID { get; set; }
        public int? FKDersPlanAnaID { get; set; }
        public string DersAd { get; set; }
        public int? Yariyil { get; set; }
        public int? DersSaat { get; set; }
        public double? AKTS { get; set; }
        public int FKCapTanimID { get; set; }
    }
    public class BirimIDs
    {
        public int ID { get; set; }
    }
    public class BolumDto
    {
        public int BolumID { get; set; }
        public string BolumAd { get; set; }
        public int AltBirimID { get; set; }
        public string AltBirimAd { get; set; }
        public int FKFakulteID { get; set; }
        public string FKFakulteAd { get; set; }
        public List<DtoBirimler> AltBirimler { get; set; }
    }
    public class CapDersPlanDto
    {
        public int ID { get; set; }
        public int FKCapTanimID { get; set; }
        public int Yil { get; set; }
        public int? FKAnaDersPlanAnaID { get; set; }
        public int? FKCapDersPlanAnaID { get; set; }
        public string FKAnaDersPlanAnaAd { get; set; }
        public string FKCapDersPlanAnaAd { get; set; }
        public int FKAnaDersPlanID { get; set; }
        public int FKCapDersPLanID { get; set; }
        public bool Silindi { get; set; }
        public DateTime TarihSilinme { get; set; }
    }


    #region YENISEKME
    public class CapProgramDto
    {
        public int FKCapTanimID { get; set; }
        public int FKFakulteID { get; set; }
        public string FakulteAd { get; set; }
        public int FKBolumID { get; set; }
        public string BolumAd { get; set; }
        public int DersAdedi { get; set; }
        public double? AKTS { get; set; }
        public int Kontenjan { get; set; }

        public int AnaFKFakulteID { get; set; }
        public string AnaFakulteAd { get; set; }
        public int AnaFKBolumID { get; set; }
        public string AnaBolumAd { get; set; }
    }
    public class IntibakPlanDto
    {
        public int ID { get; set; }
        public int FKCapTanimID { get; set; }
        public int? FkAnaDersPlanAnaID { get; set; }
        public int? FKCapDersPlanAnaID { get; set; }
        public int FKAnaDersPlanID { get; set; }
        public int FKCapDersPlanID { get; set; }
        public string FKAnaDersPlanAnaAd { get; set; }
        public string FKCapDersPlanAnaAd { get; set; }
        public int? Yariyil { get; set; }
        public int? DersSaat { get; set; }
        public double? AKTS { get; set; }
        public int? YariyilAna { get; set; }
        public double? AKTSAna { get; set; }
    }
    public class IntibakOlusturDto
    {
        public int FKCapTanimID { get; set; }
        public int FKDersPlanID { get; set; }
        public string FKDersPlanAd { get; set; }
        public List<BolumPlanDto> IntibakYapilacakListe { get; set; }
        public int Yil { get; set; }
    }

    public class SecmeliIntibakOlusturDto
    {
        public int FKCapTanimID { get; set; }
        public int FKAnaEtiketID { get; set; }
        public string EtiketAd { get; set; }
        public List<BolumEtiketPlanDto> SecmeliListe { get; set; }
    }

    public class BolumDersPlanYariyilViewModel
    {
        public string DersKod { get; set; }
        public string DersAd { get; set; }
        public string YabDersAd { get; set; }
        public int EnumZorunluTur { get; set; }
        public int EnumDersSecimTip { get; set; }
        public int EnumDilDersIcerik { get; set; }
        public int? EnumDilDersIcerik2 { get; set; }
        public int EnumDersTip { get; set; }
        public int TSaat { get; set; }
        public int USaat { get; set; }
        public int Kredi { get; set; }
        public int AKTS { get; set; }
        public int DersPlanAnaID { get; set; }
        public int DersPlanID { get; set; }
        public int FKBirimID { get; set; }
        public string Renk { get; set; }

    }
    public class BolumDersPlanViewModel
    {
        public BolumDersPlanViewModel()
        {
            YariyilZorunluDetayListe = new List<BolumDersPlanYariyilViewModel>();
            YariyilSecmeliDetayListe = new List<BolumDersPlanYariyilViewModel>();
        }
        public int Yariyil { get; set; }
        public List<BolumDersPlanYariyilViewModel> YariyilZorunluDetayListe { get; set; }
        public List<BolumDersPlanYariyilViewModel> YariyilSecmeliDetayListe { get; set; }
    }

    public class BolumEtiketPlanDto
    {
        public int FKSaatBilgiID { get; set; }
        public int? FKEtiketID { get; set; }
        public string Etiket { get; set; }
        public int? YariYil { get; set; }
        public int AKTS { get; set; }
        public int? Sayi { get; set; }
    }

    public class BolumEtiketDto
    {
        public int ID { get; set; }
        public int FKCapTanimID { get; set; }
        public int FKAnaEtiketID { get; set; }
        public int FKCapEtiketID { get; set; }
        public int Adet { get; set; }
        public string AnaEtiket { get; set; }
        public string CapEtiket { get; set; }
        public int? YariYil { get; set; }
        public int DersSaat { get; set; }
        public int AKTS { get; set; }
    }

    public class CapTanimDuzenleDto
    {
        public int FKCapTanimID { get; set; }
        public int AnaBolumID { get; set; }
        public int AltBolumID { get; set; }
        public string AnaBolumAd { get; set; }
        public string AltBolumAd { get; set; }
        public int Kontenjan { get; set; }
    }

    public class TEST
    {
        public int EtiketID { get; set; }
        public int AKTS { get; set; }
        public int Sayi { get; set; }
    }
    #endregion
}
