﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.EBS.BIRIM
{
    public class BolumBilgiVM
    {
        public int? FKDilBolumBilgiID { get; set; }
        public int? BolumID { get; set; }
        public int? ProgramID { get; set; }
        public byte? EnumBolumBilgiTur { get; set; }
        [DataType(DataType.Html)]
        public string Icerik { get; set; }
        public byte? EnumDilID { get; set; }
    }
}
