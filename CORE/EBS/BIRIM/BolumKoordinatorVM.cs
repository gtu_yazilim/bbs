﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.EBS.BIRIM
{
    public class BolumKoordinatorVM
    {
        public int? ID { get; set; }
        public int YetkiGrupID { get; set; }
        public string YetkiAd { get; set; }
        public string KullaniciAdi { get; set; }
        public string AdSoyad { get; set; }
        public int? FKPersonelID { get; set; }
        public DateTime? YetkiBaslangicTarihi { get; set; }
        public DateTime? YetkiBitisTarihi { get; set; }
        public DateTime? Zaman { get; set; }
        public int? FakulteID { get; set; }
        public int? BolumID { get; set; }
        public int? FKProgramBirimID { get; set; }
        public string YetkiliBirimAd { get; set; }
        public bool IdariMi { get; set; }
    }
}
