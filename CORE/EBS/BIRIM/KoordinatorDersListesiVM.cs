﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.EBS.BIRIM
{
    public class KoordinatorDersListesiVM
    {
        public int? ID { get; set; }
        public int? FKPersonelID { get; set; }
        public string PersonelAdSoyadUnvan { get; set; }
        public int? FKDersBilgiID { get; set; }
        public int FKDersPlanAnaID { get; set; }
        public int? FKDersPlanID { get; set; }
        public string DersAdi { get; set; }
        public string Ekleyen { get; set; }
        public DateTime? Zaman { get; set; }
        public int? Yariyil { get; set; }
        public int? FakulteID { get; set; }
        public int? BolumID { get; set; }
        public bool? YazOkuluDersiMi { get; set; }
        public int? EnumDersBirimTipi { get; set; }
        public int? EnumDersSecimTipi { get; set; }
        public int? Donem { get; set; }
        public int? Yil { get; set; }
        public int? GrupSayisi { get; set; }

        public int YeniGeriBildirimSayisi { get; set; }
        public int CevaplananGeriBildirimSayisi { get; set; }

        public int Sinav_YeniBildirimSayi { get; set; }
        public int Sinav_CevaplananBildirimSayi { get; set; }

        public int Odev_YeniBildirimSayi { get; set; }
        public int Odev_CevaplananBildirimSayi { get; set; }

        public int SanalSinif_YeniBildirimSayi { get; set; }
        public int SanalSinif_CevaplananBildirimSayi { get; set; }

        public int Icerik_YeniBildirimSayi { get; set; }
        public int Icerik_CevaplananBildirimSayi { get; set; }
    }
}
