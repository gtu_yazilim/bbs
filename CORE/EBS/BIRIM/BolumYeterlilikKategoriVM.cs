﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.EBS.BIRIM
{
    public class BolumYeterlilikKategoriVM
    {
        public int ID { get; set; }
        public int? FKBolumYeterlilikID { get; set; }
        public string IcerikAna { get; set; }
        public string IcerikAlt { get; set; }
        public byte EnumDilID { get; set; }
        public string Ekleyen { get; set; }
        public DateTime Zaman { get; set; }
    }

    public class BolumYeterlilikAnaKategoriVM : BolumYeterlilikKategoriVM
    {
        public int? FKYeterlilikAnaKategoriID { get; set; }
    }

    public class BolumYeterlilikAltKategoriVM : BolumYeterlilikKategoriVM
    {
        public int? FKAnaKategoriID { get; set; }
        public int? FKYeterlilikAltKategoriID { get; set; }
        public int? FKProgramBirimID { get; set; }
    }
}
