﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.EBS.BIRIM
{
    public class BolumProgramCiktilariVM
    {
        public int ID { get; set; }
        public int? BirimID { get; set; }
        public int? ProgramID { get; set; }
        public int EnumDilID { get; set; }
        public int? YetID { get; set; }
        public string Icerik { get; set; }
        public int? Yil { get; set; }
        public string Ekleyen { get; set; }
        public DateTime? Zaman { get; set; }
        public int? DilBolumYeterlilikID { get; set; }
        public List<BolumYeterlilikAltKategoriVM> BolumYeterlilikKategoriList { get; set; }
    }
}
