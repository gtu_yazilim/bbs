﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.EBS.DERS
{
    
    public class DegerlendirmePayListesiVM
    {
        public int PayID { get; set; }
        public int? EnumCalismaTip { get; set; }
        public byte? Sira { get; set; }
        public int? Oran { get; set; }
        public bool NotGirilmisMi { get; set; }
        public int? EnumSeviye { get; set; }
        public int? FKDersPlanID { get; set; }
        public bool SinavOdevVarMi { get; set; }
    }

    public class EkleDegerlendirmePayVM : DegerlendirmePayListesiVM
    {
        public DateTime Zaman { get; set; }
        public string KullaniciAdi { get; set; }
        public int Yil { get; set; }
        public int EnumDonem { get; set; }
        public int DersPlanAnaID { get; set; }
        public int? Yayinlandi { get; set; }
    }
}
