﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.EBS.DERS
{
    public class DersBilgiVM
    {
        public int FKDersPlanAnaID { get; set; }
        public string DersAd { get; set; }
        public string DersKodu { get; set; }
        public int YariYil { get; set; }
        public string TUSaat { get; set; }
        public double? Kredi { get; set; }
        public int? HesaplananKredi { get; set; }
        public double AKTS { get; set; }
        public string DersinDili { get; set; }
        public int EnumDilID { get; set; }
        public int? DersinSeviyesi { get; set; }
        public string DersinAmaci { get; set; }
        public string DersinIcerigi { get; set; }
        public string OnKosulDersleri { get; set; }
        public string OnerilenSecmeliDersler { get; set; }
        public string DersinYardimcilari { get; set; }
        public int? FKKoordinatorID { get; set; }
        public string KoordinatorAdSoyad { get; set; }
        public List<int?> DersiVerenPersonelIDList { get; set; }
        public string DersNotlari { get; set; }
        public string DersKaynaklari { get; set; }
        public int? EnumDersPlanAnaZorunlu { get; set; }
        public bool HiyerarsikMi { get; set; }
    }
}
