﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.EBS.DERS
{
    public class DersProgramCiktilariVM
    {
        public int? ID { get; set; }
        public int DersPlanAnaID { get; set; }
        public int FKBirimID { get; set; }
        public int? Sira { get; set; }
        public int Yil { get; set; }
        public string Icerik { get; set; }
        public int? EnumDilID { get; set; }
        public int KatkiDuzeyi { get; set; }
        public int? YeterlilikID { get; set; }
        public string KullaniciAdi { get; set; }
        public DateTime? Zaman { get; set; }
    }
}
