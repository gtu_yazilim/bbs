﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.EBS.DERS
{
    public class DegerlendirmeGruplariVM
    {
        public int ID { get; set; }
        public DateTime TarihKayit { get; set; }
        public string Kullanici { get; set; }
        public int FKDersPlanAnaID { get; set; }
        public int FKPersonelID { get; set; }
        public string PersonelAdSoyadUnvan { get; set; }
        public bool Silindi { get; set; }
        public List<DegerlendirmeDersGruplariVM> DersGrupListesi { get; set; }
        public bool DegisiklikYapilabilirMi { get; set; }
        public int? FKDersGrupHocaID { get; set; }
    }

    public class DegerlendirmeDersGruplariVM
    {
        public int ID { get; set; }
        public DateTime TarihKayit { get; set; }
        public int FKDersGrupID { get; set; }
        public int FKOrtakDegerlendirmeID { get; set; }
        public string BirimAdi { get; set; }
        public string Kullanici { get; set; }
        public int? EnumDil { get; set; }
        public bool Silindi { get; set; }
    }
}
