﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.EBS.DERS
{
    public class DersKategoriVM
    {
        public int ID { get; set; }
        public int Yil { get; set; }
        public string Icerik { get; set; }
        public int EnumDilID { get; set; }
        public bool SeciliMi { get; set; }
    }
}
