﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.EBS.DERS
{
    public class YandalVM
    {
        public int ID { get; set; }
        public int EnumGecisTipi { get; set; }
        public string BirimAdi { get; set; }
        public int FKAnaBirimID { get; set; }
        public int FKAltBirimID { get; set; }
        public bool Silindi { get; set; }
        public string Kullanici { get; set; }
        public DateTime TarihKayit { get; set; }
        public int Kontenjan { get; set; }
        public int Yil { get; set; }
        public int Donem { get; set; }
        public int DersAdet { get; set; }
        public double AKTS { get; set; }
    }

    public class YandalDersVM
    {
        public int ID { get; set; }
        public int FKDersPlanAnaID { get; set; }
        public string DersAdi { get; set; }
        public double AKTS { get; set; }
        public int? FKCapTanimID { get; set; }
        public string Kullanici { get; set; }
        public int Yil { get; set; }
        public bool Silindi { get; set; }
        public bool Secili { get; set; }
    }
}
