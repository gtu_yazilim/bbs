﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.EBS.DERS
{
    public class DersAktsVM
    {
        public int? AktsID { get; set; }
        public int DersPlanAnaID { get; set; }
        public string AktsAd { get; set; }
        public short? Sayi { get; set; }
        public short? Sure { get; set; }
        public short? Yil { get; set; }
        public byte? EnumDilID { get; set; }
        public int? AktsNo { get; set; }
        public int? AktsYukID { get; set; }
        public bool KayitliMi { get; set; }
    }
}
