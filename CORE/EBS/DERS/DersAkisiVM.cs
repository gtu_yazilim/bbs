﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.EBS.DERS
{
    public class DersAkisiVM
    {
        public int ID { get; set; }
        public int? FKDersPlanAnaID { get; set; }
        public int? Yil { get; set; }
        public List<DersAkisiDilVM> DersAkisiIcerik
        {
            get; set;
        }
        public byte? HaftaNo { get; set; }
    }

    public class DersAkisiDilVM
    {
        public int? ID { get; set; }
        public int? FKDersKonuID { get; set; }
        public int? HaftaNo { get; set; }
        public string Icerik { get; set; }
        public string OnHazirlikIcerik { get; set; }
        public byte? EnumDilID { get; set; }
    }
}
