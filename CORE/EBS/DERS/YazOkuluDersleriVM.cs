﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.EBS.DERS
{
    public class YazOkuluDersleriVM
    {
        public int FakulteID { get; set; }
        public int FKBirimID { get; set; }
        public int Yil { get; set; }
        public int FKPersonelID { get; set; }
        public List<int> DersList { get; set; }
        

    }
}
