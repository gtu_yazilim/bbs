﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.EBS.DERS
{
    public class OgrenmeCiktilariVM
    {
        public int ID { get; set; }
        public int? FKDersPlanAnaID { get; set; }
        public int EnumDilID { get; set; }
        public int? CiktiNo { get; set; }
        public string Icerik { get; set; }
        public int? Yil { get; set; }
        public string Ekleyen { get; set; }
        public DateTime? Zaman { get; set; }
        public int? DilDersCiktiID { get; set; }
        public List<DersCiktiYontemVM> DersCiktiYontemleri { get; set; }
        public List<DersCiktiYeterlilikVM> DersCiktiYeterlilikListe { get; set; }
    }
    public class DersCiktiYontemVM
    {
        public int ID { get; set; }
        public byte? YontemTur { get; set; }
        public string YontemNo { get; set; }
        public int Yil { get; set; }
        public string Icerik { get; set; }
    }
    public class DersCiktiYeterlilikVM
    {
        public int ID { get; set; }
        public int FKDersPlanAnaID { get; set; }
        public int FKCiktiID { get; set; }
        public int FKYeterlilikID { get; set; }
        public int FKBirimID { get; set; }
        public int Yil { get; set; }
        public string Ekleyen { get; set; }
        public DateTime Zaman { get; set; }
        public string Icerik { get; set; }
        public int? YetID { get; set; }
    }
}
