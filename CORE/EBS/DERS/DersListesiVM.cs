﻿using Sabis.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Core.EBS.DERS
{
    public class DersListesiVM
    {
        public int ID { get; set; }
        public string DersKod { get; set; }
        public string DersAd { get; set; }
        public string Fakulte { get; set; }
        public int? FakulteID { get; set; }
        public int? BolumID { get; set; }
        public int? Yil { get; set; }
        public int? DSaat { get; set; }
        public int? USaat { get; set; }
        public int? YariYil { get; set; }
        public int? DersTipi { get; set; }
        public int? GrupSayisi { get; set; }
        public int? ToplamOgrenciSayisi { get; set; }
        public List<DersBirimListeVM> BirimListe { get; set; }
        public double? AKTS { get; set; }
        public bool YazOkuluDersiMi { get; set; }
        public int? FKKoordinatorID { get; set; }
        public string KoordinatorAdSoyad { get; set; }
        public int? FKDersGrupHocaID { get; set; }
        public int? EnumKokDersTipi { get; set; }
        public int? EnumDersBirimTipi { get; set; }

        public EnumOgretimSeviye EnumOgretimSeviye { get; set; }
    }

    public class DersBirimListeVM
    {
        public int BirimID { get; set; }
        public string BirimAdi { get; set; }
        public string BirimDersKod { get; set; }
    }
}
