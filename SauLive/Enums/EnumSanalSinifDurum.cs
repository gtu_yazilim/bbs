﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SauLive.Enums
{
    public enum EnumSanalSinifDurum
    {
        [Display(Name = "Ders Henüz Başlamadı")]
        Baslamadi = -1,
        [Display(Name = "Ders Aktif")]
        Basladi = 0,
        [Display(Name = "Dersin Süresi Geçmiş")]
        Bitti = 1,
        [Display(Name = "Ders Yapılmadı")]
        Yapilmadi = 2
    }
}
