﻿using SauLive.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SauLive.Models.Session
{

        public class SauLiveKatilVM
        {
            public string DersAd { get; set; }
            public int DersID { get; set; }
            public string Ad { get; set; }
            public string Adres { get; set; }
            public int SSinifID { get; set; }
            public EnumSanalSinifDurum Durum { get; set; }
            public string DurumMesaj { get; set; }
        }
    
}
