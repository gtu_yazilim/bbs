﻿using SauLive.Models.Participants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SauLive.Models.Session
{
    public class SessionResult : TApiResult
    {
        public int? SessionId { get; set; }
        public List<ParticipantResult> Participants { get; set; }
    }
}
