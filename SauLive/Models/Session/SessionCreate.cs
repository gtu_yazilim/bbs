﻿using SauLive.Models.Participants;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SauLive.Models.Session
{
    public class SessionCreate
    {
        public int? SessionId { get; set; }
        public int? UnitId { get; set; }
        public string UnitName { get; set; }
        public int? CourseId { get; set; }
        public string CourseName { get; set; }
        public int? GroupId { get; set; }
        public string GroupName { get; set; }
        public int? GroupingId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int? Duration { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        [Required]
        [DisplayName("Erişim")]
        public int? AccessMode { get; set; }

        [Required]
        [DisplayName("Sonradan izlenebilir kayıt oluştur.")]
        public bool ReplayEnable { get; set; }
        [Required]
        [DisplayName("Davet Gönder ?")]
        public bool InvitationEnable { get; set; }

        [Required]
        [DisplayName("Tür")]
        public int? TypeId { get; set; }

        [DisplayName("Kategori")]
        public string ParentTitle { get; set; }

        [Required]
        [DisplayName("Login gerekli ?")]
        public int AccessMethod { get; set; }

        [DisplayName("Parola")]
        public string AccessPassword { get; set; }

        public List<ParticipantCreate> Participants { get; set; }

        public string ManagerMail { get; set; }
    }
}
