﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SauLive.Models.Participants
{
    public class ParticipantUpdate
    {
        public int? SessionId { get; set; }
        public int? UserId { get; set; }
        public int? ParticipantId { get; set; }
        public string Role { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public bool UserByEmail { get; set; }
    }
}
