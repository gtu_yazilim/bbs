﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SauLive.Models.Participants
{
    public class ParticipantDelete
    {
        public int? SessionId { get; set; }
        public int? ParticipantId { get; set; }
    }
}
