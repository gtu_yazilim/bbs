﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SauLive.Models.Participants
{
    public class ParticipantResult : TApiResult
    {
        public Guid? Code { get; set; }
        public int? ParticipantId { get; set; }
        public string Title { get; set; }
    }
}
