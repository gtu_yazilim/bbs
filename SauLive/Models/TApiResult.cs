﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SauLive.Models
{
    public class TModelError
    {
        public string Key { get; set; }
        public string Message { get; set; }
    }

    public class TApiResult
    {
        public TApiResult()
        {
            this.ModelErrors = new List<TModelError>();
        }
        public bool Success { get; set; }
        public string Message { get; set; }
        public bool Error { get; set; }
        public string ErrorMessage { get; set; }
        public bool IsValid { get; set; }
        public string ValidMessage { get; set; }

        public void SetError(Exception ex)
        {
            this.Error = true;
            this.ErrorMessage = ex.Message;
            this.Success = false;
            this.Message = "Hata oluştu";
        }
        public void AddModelError(string key, string message)
        {
            this.IsValid = false;
            this.ModelErrors.Add(new TModelError() { Key = key, Message = message });

        }
        public List<TModelError> ModelErrors { get; set; }
    }

    public class TApiResult<T> : TApiResult
    {
        public T Data { get; set; }
    }
}
