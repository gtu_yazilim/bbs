﻿using RestSharp;
using SauLive.Models;
using SauLive.Models.Participants;
using SauLive.Models.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SauLive
{
    public class SessionService
    {
        public string Url { get; set; }
        public string UName { get; set; }
        public string UPass { get; set; }
        public SessionService(string url, string uname, string upass)
        {
            this.Url = url;
            this.UName = uname;
            this.UPass = upass;
        }
        #region "Session"
        public SessionResult Create(SessionCreate model)
        {
            return this.PostObject<SessionCreate, SessionResult>("api/Session/Create", model);
        }
        public SessionResult Delete(int id)
        {
            return this.PostObject<int, SessionResult>("api/Session/delete", id);
        }
        public SessionResult Update(SessionUpdate model)
        {
            return this.PostObject<SessionUpdate, SessionResult>("api/Session/Update", model);
        }
        #endregion

        #region "Participant"
        public ParticipantResult ParticipantCreate(ParticipantCreate model)
        {
            return this.PostObject<ParticipantCreate, ParticipantResult>("api/Session/ParticipantCreate", model);
        }
        public ParticipantResult ParticipantUpdate(ParticipantUpdate model)
        {
            return this.PostObject<ParticipantUpdate, ParticipantResult>("api/Session/ParticipantUpdate", model);
        }
        public ParticipantResult ParticipantDelete(ParticipantDelete model)
        {
            return this.PostObject<ParticipantDelete, ParticipantResult>("api/Session/ParticipantDelete", model);
        }
        #endregion

        #region "Base"
        public TResult PostObject<T, TResult>(string url, T model) where TResult : TApiResult, new()
        {
            var result = new TResult();// (TResult)Activator.CreateInstance(typeof(TResult));
            result.Success = false;
            result.Message = "None";
            try
            {
                var client = new RestClient(this.Url);
                var request = new RestRequest(url, Method.POST);
                request.AddHeader("Content-type", "application/json");
                request.AddJsonBody(model);
                var response = client.Execute<TResult>(request);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    result = response.Data;
                }
                else if (response.ErrorException != null) // TODO check status
                {
                    result.SetError(response.ErrorException);
                }
            }
            catch (Exception ex)
            {
                result.SetError(ex);
            }
            return result;
        }
        #endregion
    }
}
