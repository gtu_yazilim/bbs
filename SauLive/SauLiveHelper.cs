﻿using SauLive.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SauLive
{
    public static class SauLiveHelper
    {
        public static EnumSanalSinifDurum TarihKontrol(DateTime BaslangicTarih, int Sure)
        {
            //ders başlamadan 30 dk önce ve bittikten 10 dakika sonraya kadar giriş yapılabilir
            int BaslamadanOnce = 30;
            int BittiktenSonra = 10;
            var sDate = BaslangicTarih.AddMinutes(-BaslamadanOnce);
            var eDate = BaslangicTarih.AddMinutes(Sure + BittiktenSonra);
            if (DateTime.Now < sDate)
            {
                return EnumSanalSinifDurum.Baslamadi;
            }
            if (sDate <= DateTime.Now && DateTime.Now <= eDate)
            {
                return EnumSanalSinifDurum.Basladi;
            }
            return EnumSanalSinifDurum.Bitti;
        }
    }
}
