﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SauLive
{
    public static class SettingsService
    {
        public static string Get(string key)
        {
            return System.Configuration.ConfigurationManager.AppSettings[key];
        }
        public static string LiveUrl
        {
            get
            {
                return Get("LiveUrl");
            }
        }
        public static string LiveApiUrl
        {
            get
            {
                return Get("LiveApiUrl");
            }
        }
        public static string UName
        {
            get
            {
                return Get("UName");
            }
        }
        public static string UPass
        {
            get
            {
                return Get("UPass");
            }
        }
    }
}
