﻿using AutoMapper;
using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Bolum.Core.ABS.DEGERLENDIRME;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Bll
{
    public static class Startup
    {
        public static void Init()
        {
            Mapper.Initialize(cfg => {
                cfg.CreateMap<ABSDegerlendirme, DegerlendirmeDTO>();
            });
        }
    }
}
