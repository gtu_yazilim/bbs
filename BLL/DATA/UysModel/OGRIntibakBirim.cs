namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRIntibakBirim")]
    public partial class OGRIntibakBirim
    {
        public int ID { get; set; }

        public int FKIntibakID { get; set; }

        public int FKBirimID { get; set; }

        public DateTime TarihKayit { get; set; }

        public virtual Birimler Birimler { get; set; }

        public virtual OGRIntibak OGRIntibak { get; set; }
    }
}
