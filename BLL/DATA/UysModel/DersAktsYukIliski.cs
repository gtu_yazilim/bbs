namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.DersAktsYukIliski")]
    public partial class DersAktsYukIliski
    {
        [Column(TypeName = "numeric")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public decimal ID { get; set; }

        public int? FKDersPlanAnaID { get; set; }

        public int? FKAktsID { get; set; }

        public short? Sayi { get; set; }

        public short? Sure { get; set; }

        public short Yil { get; set; }

        [StringLength(30)]
        public string Ekleyen { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? Zaman { get; set; }

        public virtual DersAktsTnm DersAktsTnm { get; set; }

        public virtual OGRDersPlanAna OGRDersPlanAna { get; set; }
    }
}
