namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.ABSYetkiModul")]
    public partial class ABSYetkiModul
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ABSYetkiModul()
        {
            ABSYetki = new HashSet<ABSYetki>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string ModulAd { get; set; }

        [StringLength(250)]
        public string ModulYol { get; set; }

        [StringLength(50)]
        public string KisaUrl { get; set; }

        public int? UstID { get; set; }

        public bool HerkeseAcik { get; set; }

        public int Sira { get; set; }

        public bool UstMenudeGorunsun { get; set; }

        [StringLength(50)]
        public string ModulAdEng { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSYetki> ABSYetki { get; set; }
    }
}
