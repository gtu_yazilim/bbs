namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("basvuru.MuafiyetSinavSalonTanim")]
    public partial class MuafiyetSinavSalonTanim
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MuafiyetSinavSalonTanim()
        {
            MuafiyetSinavOturum = new HashSet<MuafiyetSinavOturum>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(250)]
        public string Ad { get; set; }

        public int Kapasite { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MuafiyetSinavOturum> MuafiyetSinavOturum { get; set; }
    }
}
