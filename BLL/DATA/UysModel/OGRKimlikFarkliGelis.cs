namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRKimlikFarkliGelis")]
    public partial class OGRKimlikFarkliGelis
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(100)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? InKod { get; set; }

        [StringLength(100)]
        public string EskiNumara { get; set; }

        public int? Yil { get; set; }

        public DateTime? YonKurTarih { get; set; }

        [StringLength(50)]
        public string YonKurNo { get; set; }

        [StringLength(100)]
        public string GelUniversite { get; set; }

        [StringLength(100)]
        public string GelFakulte { get; set; }

        [StringLength(100)]
        public string GelBolum { get; set; }

        [StringLength(255)]
        public string Notlar { get; set; }

        public int? FKOgrenciID { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public int? FKUniversiteID { get; set; }

        public int? FKAFKanunID { get; set; }

        public long? GelBirimYoksisID { get; set; }

        public DateTime? GelBirimeKayitTarihi { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }

        public virtual OGRTnmAFKanunlari OGRTnmAFKanunlari { get; set; }
    }
}
