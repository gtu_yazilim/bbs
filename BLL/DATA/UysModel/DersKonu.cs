namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.DersKonu")]
    public partial class DersKonu
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DersKonu()
        {
            DilDersKonu = new HashSet<DilDersKonu>();
            OGRDersProgramiv2 = new HashSet<OGRDersProgramiv2>();
        }

        public int ID { get; set; }

        public int? FKDersPlanAnaID { get; set; }

        public int? FKDersHaftaIliskiID { get; set; }

        public int? Yil { get; set; }

        [StringLength(30)]
        public string Ekleyen { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? Zaman { get; set; }

        public virtual DersHaftaIliski DersHaftaIliski { get; set; }

        public virtual OGRDersPlanAna OGRDersPlanAna { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DilDersKonu> DilDersKonu { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRDersProgramiv2> OGRDersProgramiv2 { get; set; }
    }
}
