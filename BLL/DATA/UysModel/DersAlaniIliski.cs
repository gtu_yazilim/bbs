namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.DersAlaniIliski")]
    public partial class DersAlaniIliski
    {
        public int ID { get; set; }

        public int? FKDersPlanAnaID { get; set; }

        public int? FKDersAlanID { get; set; }

        public short? Yil { get; set; }

        [StringLength(30)]
        public string Ekleyen { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? Zaman { get; set; }

        public virtual DersAlanTnm DersAlanTnm { get; set; }

        public virtual OGRDersPlanAna OGRDersPlanAna { get; set; }
    }
}
