namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.OSinavGrup")]
    public partial class OSinavGrup
    {
        public int ID { get; set; }

        public int FKSinavID { get; set; }

        public int FKDersPlanAnaID { get; set; }

        public int FKDersPlanID { get; set; }

        public int FKDersGrupID { get; set; }

        public int PayID { get; set; }

        public bool? Silindi { get; set; }

        public virtual ABSPaylar ABSPaylar { get; set; }

        public virtual OSinav OSinav { get; set; }

        public virtual OGRDersGrup OGRDersGrup { get; set; }

        public virtual OGRDersPlan OGRDersPlan { get; set; }

        public virtual OGRDersPlanAna OGRDersPlanAna { get; set; }
    }
}
