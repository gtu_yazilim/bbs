namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.DersCikti")]
    public partial class DersCikti
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DersCikti()
        {
            ABSPaylarOlcmeOgrenme = new HashSet<ABSPaylarOlcmeOgrenme>();
            AKRGenelOgrenmeCiktilari = new HashSet<AKRGenelOgrenmeCiktilari>();
            DersCiktiYontemIliski = new HashSet<DersCiktiYontemIliski>();
            DilDersCikti = new HashSet<DilDersCikti>();
        }

        public int ID { get; set; }

        public int? FKDersPlanAnaID { get; set; }

        public int? CiktiNo { get; set; }

        public short? Yil { get; set; }

        [StringLength(30)]
        public string Ekleyen { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? Zaman { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSPaylarOlcmeOgrenme> ABSPaylarOlcmeOgrenme { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AKRGenelOgrenmeCiktilari> AKRGenelOgrenmeCiktilari { get; set; }

        public virtual OGRDersPlanAna OGRDersPlanAna { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DersCiktiYontemIliski> DersCiktiYontemIliski { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DilDersCikti> DilDersCikti { get; set; }
    }
}
