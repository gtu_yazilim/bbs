namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("gen.BirimKurulusDayanagi")]
    public partial class BirimKurulusDayanagi
    {
        public int ID { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? KurulTarihi { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? EvrakTarihi { get; set; }

        public int FKBirimID { get; set; }

        [StringLength(20)]
        public string Makina { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(20)]
        public string EvrakNoKararSayi { get; set; }

        [StringLength(255)]
        public string Aciklama { get; set; }

        public int ENUMDayanakTuru { get; set; }

        [StringLength(250)]
        public string DosyaKey { get; set; }

        public virtual Birimler Birimler { get; set; }
    }
}
