namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGREnstituKimlik")]
    public partial class OGREnstituKimlik
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public int? InKod { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        [StringLength(11)]
        public string Numara { get; set; }

        [StringLength(12)]
        public string TC { get; set; }

        public int? GirisYil { get; set; }

        [StringLength(25)]
        public string Ad { get; set; }

        [StringLength(25)]
        public string Soyad { get; set; }

        [StringLength(25)]
        public string DYeri { get; set; }

        public DateTime? DTarih { get; set; }

        [StringLength(2)]
        public string Sinif { get; set; }

        public int? EnumOgrDurum { get; set; }

        public int? EnumCinsiyet { get; set; }

        public int? EnumAskerlikDurumu { get; set; }

        public int? EnumKayitNedeni { get; set; }

        public int? EnumOgretimTur { get; set; }

        public int? EnumHarcDurum { get; set; }

        public int? EnumMezuniyetAsamasi { get; set; }

        public int? FKUlkeID { get; set; }

        public int? FKDanismanID { get; set; }

        [StringLength(100)]
        public string foto { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public int? FKBolumPrBirimID { get; set; }

        public virtual Birimler Birimler { get; set; }
    }
}
