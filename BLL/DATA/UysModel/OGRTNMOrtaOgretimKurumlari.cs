namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRTNMOrtaOgretimKurumlari")]
    public partial class OGRTNMOrtaOgretimKurumlari
    {
        public int ID { get; set; }

        public int? IlKodu { get; set; }

        [StringLength(50)]
        public string IlAdi { get; set; }

        [StringLength(6)]
        public string OkulKodu { get; set; }

        [StringLength(255)]
        public string OkulAdi { get; set; }
    }
}
