namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.SBTbilimAlanlari")]
    public partial class SBTbilimAlanlari
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SBTbilimAlanlari()
        {
            UAKtemelAlan = new HashSet<UAKtemelAlan>();
        }

        [StringLength(8)]
        public string ID { get; set; }

        [StringLength(100)]
        public string bilim_alani { get; set; }

        public int? temel_alan_id { get; set; }

        public int? kosul_no { get; set; }

        public virtual SBTtemelAlanlar SBTtemelAlanlar { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UAKtemelAlan> UAKtemelAlan { get; set; }
    }
}
