namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.IzinSakliToplam")]
    public partial class IzinSakliToplam
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        public string Kullanici { get; set; }

        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int InKod { get; set; }

        public int Derece { get; set; }

        public int Toplam { get; set; }

        public int Adet { get; set; }

        public int KullaniciKod { get; set; }

        public DateTime IslemTarihi { get; set; }

        public int FKFormID { get; set; }

        public int FKKadroUnvaniID { get; set; }

        public int? FKEUlkeID { get; set; }

        public int? FKEUniversiteID { get; set; }

        public int? FKEFakulteID { get; set; }

        public int? FKEBolumID { get; set; }

        public int? FKEAnaBilimDaliID { get; set; }

        public int? FKYUlkeID { get; set; }

        public int? FKYUniversiteID { get; set; }

        public int? FKYFakulteID { get; set; }

        public int? FKYBolumID { get; set; }

        public int? FKYAnaBilimDaliID { get; set; }

        public virtual IzinSakli IzinSakli { get; set; }

        public virtual TNMAnaBilimDali TNMAnaBilimDali { get; set; }

        public virtual TNMBolum TNMBolum { get; set; }

        public virtual TNMFakulte TNMFakulte { get; set; }

        public virtual TNMUniversite TNMUniversite { get; set; }

        public virtual TNMKadroUnvani TNMKadroUnvani { get; set; }

        public virtual TNMAnaBilimDali TNMAnaBilimDali1 { get; set; }

        public virtual TNMBolum TNMBolum1 { get; set; }

        public virtual TNMFakulte TNMFakulte1 { get; set; }

        public virtual TNMUniversite TNMUniversite1 { get; set; }
    }
}
