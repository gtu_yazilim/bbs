namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("gen.mekanBinalar")]
    public partial class mekanBinalar
    {
        public int ID { get; set; }

        [StringLength(50)]
        public string kodBina { get; set; }

        public int? KampusID { get; set; }

        public int? birimID { get; set; }

        [StringLength(2)]
        public string FakKod { get; set; }

        [StringLength(2)]
        public string BolKod { get; set; }

        public int? islevID { get; set; }

        public byte? Kat { get; set; }

        public byte? Kapi { get; set; }

        public bool? Asansor { get; set; }

        public bool? Jenerator { get; set; }

        public bool? Klima { get; set; }

        public int? En { get; set; }

        public int? Boy { get; set; }

        public int? Toplam { get; set; }

        public int? AcikAlan { get; set; }

        public int? ToplamAlan { get; set; }

        [StringLength(256)]
        public string Aciklama { get; set; }

        [StringLength(50)]
        public string foto { get; set; }

        public int? idari { get; set; }

        public int? derslik { get; set; }

        public int? laboratuvar { get; set; }

        public int? sosyal { get; set; }

        public int? spor { get; set; }

        public int? ofis { get; set; }

        [StringLength(100)]
        public string fakAd { get; set; }

        [StringLength(100)]
        public string EngfakAd { get; set; }

        public bool? goster { get; set; }
    }
}
