namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.DilDersKategoriTnm")]
    public partial class DilDersKategoriTnm
    {
        public int ID { get; set; }

        public int? FKDersKategoriTnmID { get; set; }

        [Column(TypeName = "text")]
        public string Icerik { get; set; }

        public byte? EnumDilID { get; set; }

        [StringLength(50)]
        public string Ekleyen { get; set; }

        public DateTime? Zaman { get; set; }

        public virtual DersKategoriTnm DersKategoriTnm { get; set; }
    }
}
