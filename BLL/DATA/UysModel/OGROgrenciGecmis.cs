namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGROgrenciGecmis")]
    public partial class OGROgrenciGecmis
    {
        public int ID { get; set; }

        public DateTime Zaman { get; set; }

        public DateTime SGZaman { get; set; }

        public DateTime? TarihOlay { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        public int FKOgrenciID { get; set; }

        public int EnumGecmisOlayTip { get; set; }

        [StringLength(100)]
        public string Aciklama { get; set; }

        public int Yil { get; set; }

        public int? EnumDonem { get; set; }

        [Required]
        public string DetayJson { get; set; }

        public DateTime? TarihYOKAktarim { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
