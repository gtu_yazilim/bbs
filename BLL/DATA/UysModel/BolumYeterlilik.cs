namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.BolumYeterlilik")]
    public partial class BolumYeterlilik
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BolumYeterlilik()
        {
            ABSPaylarOlcmeProgram = new HashSet<ABSPaylarOlcmeProgram>();
            AKRGenelProgramCiktilari = new HashSet<AKRGenelProgramCiktilari>();
            BolumYeterlilikKategoriIliski = new HashSet<BolumYeterlilikKategoriIliski>();
            DersYeterlilikIliski = new HashSet<DersYeterlilikIliski>();
            DilBolumYeterlilik = new HashSet<DilBolumYeterlilik>();
        }

        public int ID { get; set; }

        public int? FKProgramBirimID { get; set; }

        public int? YetID { get; set; }

        public int Yil { get; set; }

        [StringLength(20)]
        public string Ekleyen { get; set; }

        public DateTime? Zaman { get; set; }

        public int? ProgramID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSPaylarOlcmeProgram> ABSPaylarOlcmeProgram { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AKRGenelProgramCiktilari> AKRGenelProgramCiktilari { get; set; }

        public virtual Birimler Birimler { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BolumYeterlilikKategoriIliski> BolumYeterlilikKategoriIliski { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DersYeterlilikIliski> DersYeterlilikIliski { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DilBolumYeterlilik> DilBolumYeterlilik { get; set; }
    }
}
