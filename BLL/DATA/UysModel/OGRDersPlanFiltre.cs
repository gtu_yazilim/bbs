namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRDersPlanFiltre")]
    public partial class OGRDersPlanFiltre
    {
        public int ID { get; set; }

        public DateTime Zaman { get; set; }

        public int FKDersPlanID { get; set; }

        public int FKBirimID { get; set; }

        public bool Harici { get; set; }

        public virtual Birimler Birimler { get; set; }

        public virtual OGRDersPlan OGRDersPlan { get; set; }
    }
}
