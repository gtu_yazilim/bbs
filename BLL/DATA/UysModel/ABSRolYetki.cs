namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.ABSRolYetki")]
    public partial class ABSRolYetki
    {
        public int ID { get; set; }

        public int EnumRolTip { get; set; }

        public int? FK_PersonelID { get; set; }

        public int? FK_YonetecegiFakID { get; set; }

        public int? FK_YonetecegiBolID { get; set; }

        [StringLength(200)]
        public string Aciklama { get; set; }

        public bool DanismanAtamaYetkisi { get; set; }

        public bool IpYetkilendirmeYetkisi { get; set; }

        public bool HaftaYonetimiYetkisi { get; set; }

        public bool SistemTarihleriYonetimi { get; set; }

        public DateTime? IslemTarihi { get; set; }

        [StringLength(50)]
        public string IslemYapanKullanici { get; set; }

        public bool GenelYonetici { get; set; }

        public bool OgrenciListelemeYetkisi { get; set; }

        public bool DanismanAtamaYetkisiVermeYetkisi { get; set; }

        public bool SimuleEtmeYetkisi { get; set; }

        public virtual Birimler Birimler { get; set; }

        public virtual Birimler Birimler1 { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }
    }
}
