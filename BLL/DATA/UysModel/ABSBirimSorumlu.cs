namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.ABSBirimSorumlu")]
    public partial class ABSBirimSorumlu
    {
        public int ID { get; set; }

        public int FKBirimID { get; set; }

        [Required]
        [StringLength(50)]
        public string KullaniciAdi { get; set; }

        public int EnumBirimSorumluTip { get; set; }

        public virtual Birimler Birimler { get; set; }
    }
}
