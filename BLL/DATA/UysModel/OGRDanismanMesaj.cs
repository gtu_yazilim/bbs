namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRDanismanMesaj")]
    public partial class OGRDanismanMesaj
    {
        public int ID { get; set; }

        public DateTime Tarih { get; set; }

        public int FKDanismanPersonelID { get; set; }

        public int FKOgrenciID { get; set; }

        [Required]
        public string Mesaj { get; set; }

        public DateTime? TarihOkunma { get; set; }

        public bool GonderenOgrenci { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }
    }
}
