namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRTNMGenel")]
    public partial class OGRTNMGenel
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? InKod { get; set; }

        public int? Yil { get; set; }

        [StringLength(500)]
        public string UniversiteAd { get; set; }

        [StringLength(500)]
        public string UniversiteAdEng { get; set; }

        [StringLength(250)]
        public string RektorUnvan { get; set; }

        [StringLength(250)]
        public string RektorUnvanEng { get; set; }

        [StringLength(250)]
        public string RektorMakam { get; set; }

        [StringLength(250)]
        public string RektorMakamEng { get; set; }

        [StringLength(500)]
        public string RektorAd { get; set; }

        [StringLength(250)]
        public string RektorYrdUnvan { get; set; }

        [StringLength(250)]
        public string RektorYrdUnvanEng { get; set; }

        [StringLength(250)]
        public string RektorYrdMakam { get; set; }

        [StringLength(250)]
        public string RektorYrdMakamEng { get; set; }

        [StringLength(250)]
        public string RektorYrdAd { get; set; }

        [StringLength(250)]
        public string DaireBaskanUnvan { get; set; }

        [StringLength(250)]
        public string DaireBaskanUnvanEng { get; set; }

        [StringLength(500)]
        public string DaireBaskanAd { get; set; }

        [StringLength(250)]
        public string WebAdres { get; set; }

        public int? EnumDonem { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }
    }
}
