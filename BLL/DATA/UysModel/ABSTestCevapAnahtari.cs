namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.ABSTestCevapAnahtari")]
    public partial class ABSTestCevapAnahtari
    {
        [Key]
        [Column(Order = 0)]
        public int ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FKSinavID { get; set; }

        [StringLength(500)]
        public string CevapA { get; set; }

        [StringLength(500)]
        public string SiraB { get; set; }

        [StringLength(500)]
        public string CevapB { get; set; }

        [StringLength(500)]
        public string SiraC { get; set; }

        [StringLength(500)]
        public string CevapC { get; set; }

        [StringLength(500)]
        public string SiraD { get; set; }

        [StringLength(500)]
        public string CevapD { get; set; }
    }
}
