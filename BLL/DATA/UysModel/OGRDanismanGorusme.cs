namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRDanismanGorusme")]
    public partial class OGRDanismanGorusme
    {
        public int ID { get; set; }

        [StringLength(500)]
        public string GorusmeKayit { get; set; }

        public DateTime? GorusmeTarihi { get; set; }

        public int? FKPersonelID { get; set; }

        public int? FKOgrenciID { get; set; }

        public DateTime? zaman { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }
    }
}
