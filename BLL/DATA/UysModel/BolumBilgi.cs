namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.BolumBilgi")]
    public partial class BolumBilgi
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BolumBilgi()
        {
            DilBolumBilgi = new HashSet<DilBolumBilgi>();
        }

        public int ID { get; set; }

        public int? FKBolumID { get; set; }

        public int? FKProgramBirimID { get; set; }

        public int? FKProgramBirimID2 { get; set; }

        public int? FKProgramBirimID3 { get; set; }

        public short Yil { get; set; }

        [StringLength(20)]
        public string Ekleyen { get; set; }

        public DateTime? Zaman { get; set; }

        public virtual Birimler Birimler { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DilBolumBilgi> DilBolumBilgi { get; set; }
    }
}
