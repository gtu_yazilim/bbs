namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.ABSYayinlananPayNotlar")]
    public partial class ABSYayinlananPayNotlar
    {
        public int ID { get; set; }

        public int? FKDersGrupID { get; set; }

        public int FKABSPayID { get; set; }

        public DateTime YayinlanmaTarihi { get; set; }

        public bool Yayinlandi { get; set; }

        public string LogTakip { get; set; }

        public int? EnumDonem { get; set; }

        public int? Yil { get; set; }

        public int? FKDersPlanAnaID { get; set; }

        public virtual ABSPaylar ABSPaylar { get; set; }

        public virtual OGRDersGrup OGRDersGrup { get; set; }

        public virtual OGRDersPlanAna OGRDersPlanAna { get; set; }
    }
}
