namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.EgitimBilgisi")]
    public partial class EgitimBilgisi
    {
        public int ID { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public DateTime? Zaman { get; set; }

        public DateTime? BaslangicTarihi { get; set; }

        public DateTime? BitisTarihi { get; set; }

        public bool? Akademik { get; set; }

        public int? Sure { get; set; }

        public bool? Sertifika { get; set; }

        public int FKPersonelBilgisiID { get; set; }

        public int? FKKadroUnvaniID { get; set; }

        public int? FKGorevYeriID { get; set; }

        public int? FKEgitimYeriID { get; set; }

        public int? FKEgitimKonusuID { get; set; }

        public int FKEgitimBilgisiTanimID { get; set; }

        public virtual Birimler Birimler { get; set; }

        public virtual EgitimBilgisi EgitimBilgisi1 { get; set; }

        public virtual EgitimBilgisi EgitimBilgisi2 { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }

        public virtual TNMEgitimBilgisi TNMEgitimBilgisi { get; set; }

        public virtual TNMEgitimKonusu TNMEgitimKonusu { get; set; }

        public virtual TNMEgitimYeri TNMEgitimYeri { get; set; }

        public virtual TNMKadroUnvani TNMKadroUnvani { get; set; }
    }
}
