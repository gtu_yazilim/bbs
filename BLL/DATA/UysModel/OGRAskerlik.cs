namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRAskerlik")]
    public partial class OGRAskerlik
    {
        public int ID { get; set; }

        public int FKKisiID { get; set; }

        public int FKOgrenciID { get; set; }

        public DateTime? AskerlikTecilTarihi { get; set; }

        public DateTime? SevkTehiri { get; set; }

        public DateTime? SevkIptali { get; set; }

        public DateTime? SevkTehirUzatma { get; set; }

        public DateTime? AskerlikUzatma { get; set; }

        public int? EnumAskerlikDurum { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public DateTime? Zaman { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public virtual Kisi Kisi { get; set; }
    }
}
