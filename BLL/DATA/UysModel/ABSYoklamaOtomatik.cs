namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.ABSYoklamaOtomatik")]
    public partial class ABSYoklamaOtomatik
    {
        [Key]
        [Column(Order = 0)]
        public int ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Pin { get; set; }

        [Key]
        [Column(Order = 2)]
        public DateTime BaslangisTarihi { get; set; }

        [Key]
        [Column(Order = 3)]
        public DateTime BitisTarihi { get; set; }

        [Key]
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FKPersonelID { get; set; }

        [StringLength(50)]
        public string IP { get; set; }

        [Key]
        [Column(Order = 5)]
        public bool Silindi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSYoklamaOtomatikDersProgram> ABSYoklamaOtomatikDersProgram { get; set; }
        public virtual PersonelBilgisi PersonelBilgisi { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSYoklamaOtomatikDersProgram> ABSYoklamaOtomatikDersProgram1 { get; set; }

    }
}
