namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("obs.OBSOnKayit")]
    public partial class OBSOnKayit
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OBSOnKayit()
        {
            OBSOnKayitAdres = new HashSet<OBSOnKayitAdres>();
        }

        public int ID { get; set; }

        public DateTime? OnKayitTarihi { get; set; }

        public DateTime? KayitTarihi { get; set; }

        public string BelgeNo { get; set; }

        public bool Hazirlik { get; set; }

        public bool YabanciDilSinavi { get; set; }

        public bool OncedenYuksekOgrenimeKayitli { get; set; }

        public bool OSYMBilgilerimdeHataVar { get; set; }

        public int? SiraNo { get; set; }

        public DateTime RandevuTarihi { get; set; }

        public int FKOsymGeciciID { get; set; }

        public bool MailAcildiMi { get; set; }

        [StringLength(50)]
        public string NufusIl { get; set; }

        public int? NufusIlKod { get; set; }

        [StringLength(50)]
        public string NufusIlce { get; set; }

        public int? NufusIlceKod { get; set; }

        public int? AileSiraNo { get; set; }

        public int? BireySiraNo { get; set; }

        [StringLength(100)]
        public string Mahalle { get; set; }

        public int? CiltNo { get; set; }

        [StringLength(50)]
        public string AnaAdi { get; set; }

        [StringLength(50)]
        public string BabaAdi { get; set; }

        public int? AileYillikGeliri { get; set; }

        public int? EnumEngelDurumu { get; set; }

        [StringLength(100)]
        public string SaglikSorunu { get; set; }

        public int? GormeEngeliYuzdesi { get; set; }

        public int? IsitmeEngeliYuzdesi { get; set; }

        public int? OrtopedikEngeliYuzdesi { get; set; }

        [StringLength(100)]
        public string KronikHastaligi { get; set; }

        public int? KardesSayisi { get; set; }

        [StringLength(100)]
        public string BabaMeslegi { get; set; }

        [StringLength(100)]
        public string AnneMeslegi { get; set; }

        [StringLength(150)]
        public string MezunOlduguLise { get; set; }

        public int? LiseIlID { get; set; }

        public int? LiseIlceID { get; set; }

        public int? LiseMezuniyetYili { get; set; }

        public int? EnumKanGrubu { get; set; }

        [StringLength(100)]
        public string BankaAd { get; set; }

        [StringLength(26)]
        public string IBAN { get; set; }

        [StringLength(150)]
        public string EPosta { get; set; }

        [StringLength(150)]
        public string WebSayfasi { get; set; }

        [Required]
        [StringLength(11)]
        public string TCKimlik { get; set; }

        [StringLength(9)]
        public string OSYMPuani { get; set; }

        public int? BasariSirasi { get; set; }

        [Required]
        [StringLength(100)]
        public string Ad { get; set; }

        [Required]
        [StringLength(100)]
        public string Soyad { get; set; }

        [Required]
        [StringLength(40)]
        public string DogumYeri { get; set; }

        public DateTime DogumTarihi { get; set; }

        public int EnumCinsiyet { get; set; }

        public int? Uyruk { get; set; }

        [Required]
        [StringLength(15)]
        public string OgrenciNo { get; set; }

        public int FKBolumID { get; set; }

        public int Yil { get; set; }

        public bool BabaSagMi { get; set; }

        public bool AnneSagMi { get; set; }

        [StringLength(4)]
        public string KimlikSeri { get; set; }

        [StringLength(10)]
        public string KimlikNo { get; set; }

        public bool AilemeBilgiVerilsin { get; set; }

        public int? EnumKayitNedeni { get; set; }

        public int FKBolumPrBirimID { get; set; }

        public int EnumHazirlikTipi { get; set; }

        [StringLength(25)]
        public string CepTel { get; set; }

        [StringLength(25)]
        public string EvTel { get; set; }

        [StringLength(25)]
        public string IsTel { get; set; }

        public virtual Birimler Birimler { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OBSOnKayitAdres> OBSOnKayitAdres { get; set; }
    }
}
