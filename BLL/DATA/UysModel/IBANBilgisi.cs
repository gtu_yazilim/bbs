namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("gen.IBANBilgisi")]
    public partial class IBANBilgisi
    {
        public int ID { get; set; }

        public int FKKisiID { get; set; }

        [Required]
        [StringLength(50)]
        public string IBAN { get; set; }

        public DateTime TarihKayit { get; set; }

        public bool AktifMi { get; set; }

        public string Kullanici { get; set; }

        public int? FKEskiID { get; set; }

        public virtual Kisi Kisi { get; set; }
    }
}
