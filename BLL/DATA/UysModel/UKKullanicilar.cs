namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UKKullanicilar")]
    public partial class UKKullanicilar
    {
        public int ID { get; set; }

        public int? PersonelID { get; set; }

        public int? RoleID { get; set; }

        [StringLength(2)]
        public string ogrFakKod { get; set; }

        [StringLength(2)]
        public string ogrBolKod { get; set; }

        public int? FakulteID { get; set; }

        public int? BolumID { get; set; }

        public int? Kategori { get; set; }

        public int? FKFakulteBirimID { get; set; }

        public int? FKBolumBirimID { get; set; }

        public virtual Birimler Birimler { get; set; }

        public virtual Birimler Birimler1 { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }
    }
}
