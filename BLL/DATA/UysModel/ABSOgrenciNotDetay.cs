namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.ABSOgrenciNotDetay")]
    public partial class ABSOgrenciNotDetay
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        public string Kullanici { get; set; }

        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? SoruNo { get; set; }

        public double? Notu { get; set; }

        public int? FKOgrenciNotID { get; set; }

        public int? FKABSPaylarID { get; set; }

        public int? FKOgrenciID { get; set; }

        public int? FKAbsPaylarOlcmeID { get; set; }

        public int? Yil { get; set; }

        public int? EnumDonem { get; set; }

        public bool Silindi { get; set; }

        public DateTime? SilindiTarih { get; set; }

        [StringLength(50)]
        public string SilenKullanici { get; set; }

        [StringLength(50)]
        public string SilenIP { get; set; }

        public int? FKDersGrupID { get; set; }

        public Guid? GUID { get; set; }

        public virtual ABSOgrenciNot ABSOgrenciNot { get; set; }

        public virtual ABSPaylarOlcme ABSPaylarOlcme { get; set; }

        public virtual OGRDersGrup OGRDersGrup { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
