namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.SGKHizmetBorclanma")]
    public partial class SGKHizmetBorclanma
    {
        public int ID { get; set; }

        [StringLength(11)]
        public string Tckn { get; set; }

        public int? kayitNo { get; set; }

        [StringLength(200)]
        public string ad { get; set; }

        [StringLength(200)]
        public string soyad { get; set; }

        public int? emekliSicil { get; set; }

        public int? derece { get; set; }

        public int? kademe { get; set; }

        public int? ekgosterge { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? baslamaTarihi { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? bitisTarihi { get; set; }

        public int? gunSayisi { get; set; }

        public int? kanunKod { get; set; }

        [StringLength(200)]
        public string borcNevi { get; set; }

        public double? toplamTutar { get; set; }

        public double? odenenMiktar { get; set; }

        [StringLength(200)]
        public string calistigiKurum { get; set; }

        [StringLength(200)]
        public string isyeriIl { get; set; }

        [StringLength(200)]
        public string isyeriIlce { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? borclanmaTarihi { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? odemeTarihi { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? kurumOnayTarihi { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? zaman { get; set; }

        public int? FKPersonelBilgisiID { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool Durum { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }
    }
}
