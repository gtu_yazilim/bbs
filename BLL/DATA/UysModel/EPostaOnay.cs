namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ytk.EPostaOnay")]
    public partial class EPostaOnay
    {
        public int ID { get; set; }

        public int FKKullaniciID { get; set; }

        public DateTime Tarih { get; set; }

        public DateTime? BitisTarihi { get; set; }

        [Required]
        [StringLength(255)]
        public string EPosta { get; set; }

        public Guid OnayKodu { get; set; }

        public bool OnaylandiMi { get; set; }

        public virtual Kullanici Kullanici { get; set; }
    }
}
