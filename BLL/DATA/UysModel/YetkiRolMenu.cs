namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.YetkiRolMenu")]
    public partial class YetkiRolMenu
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool AktifMi { get; set; }

        public int FKRolID { get; set; }

        public int FKMenuID { get; set; }

        public virtual Menuler Menuler { get; set; }

        public virtual YetkiRolTanim YetkiRolTanim { get; set; }
    }
}
