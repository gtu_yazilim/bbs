namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.TNMLojman")]
    public partial class TNMLojman
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TNMLojman()
        {
            LojmanBilgisi = new HashSet<LojmanBilgisi>();
        }

        public int ID { get; set; }

        public DateTime Zaman { get; set; }

        [Required]
        [StringLength(50)]
        public string Kullanici { get; set; }

        [Required]
        [StringLength(50)]
        public string Makina { get; set; }

        [Required]
        [StringLength(50)]
        public string LojmanAd { get; set; }

        [StringLength(500)]
        public string Aciklama { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LojmanBilgisi> LojmanBilgisi { get; set; }
    }
}
