namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.WebDersTipHocalar")]
    public partial class WebDersTipHocalar
    {
        [Key]
        public int InKod { get; set; }

        public int? AnaDersPlanID { get; set; }

        [StringLength(50)]
        public string DersiVerenHoca { get; set; }

        [StringLength(255)]
        public string Alan_tr { get; set; }

        [StringLength(255)]
        public string Alan_en { get; set; }

        public int? Yil1 { get; set; }

        [StringLength(50)]
        public string Ekleyen { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? Zaman { get; set; }
    }
}
