namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGREnstituBasvuruAcilmaDonemleri")]
    public partial class OGREnstituBasvuruAcilmaDonemleri
    {
        public OGREnstituBasvuruAcilmaDonemleri()
        {
            OGREnstituKontenjan = new HashSet<OGREnstituKontenjan>();
        }


        public int ID { get; set; }
        public string BasvuruAdi { get; set; }
        public int? Yil { get; set; }
        public int? EnumDonem { get; set; }
        public int? BasvuruSayisi { get; set; }
        public DateTime? BasvuruSonTarihi { get; set; }
        public DateTime? ABSSonGoruntulemeTarihi { get; set; }
        public bool? Aktif { get; set; }

        public virtual ICollection<OGREnstituKontenjan> OGREnstituKontenjan { get; set; }
    }
}
