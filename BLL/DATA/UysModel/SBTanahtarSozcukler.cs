namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.SBTanahtarSozcukler")]
    public partial class SBTanahtarSozcukler
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SBTanahtarSozcukler()
        {
            UAKtemelAlan = new HashSet<UAKtemelAlan>();
            UAKtemelAlan1 = new HashSet<UAKtemelAlan>();
            UAKtemelAlan2 = new HashSet<UAKtemelAlan>();
            UAKtemelAlan3 = new HashSet<UAKtemelAlan>();
            UAKtemelAlan4 = new HashSet<UAKtemelAlan>();
        }

        [StringLength(5)]
        public string ID { get; set; }

        [StringLength(255)]
        public string anahtarSozcuk { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UAKtemelAlan> UAKtemelAlan { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UAKtemelAlan> UAKtemelAlan1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UAKtemelAlan> UAKtemelAlan2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UAKtemelAlan> UAKtemelAlan3 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UAKtemelAlan> UAKtemelAlan4 { get; set; }
    }
}
