namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRStaj")]
    public partial class OGRStaj
    {
        public int ID { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? CalismaGunSayisi { get; set; }

        public int? EnumDonem { get; set; }

        public int? EnumStajBasariDurumu { get; set; }

        public int? EnumStajTuru { get; set; }

        public int? FKOgrenciID { get; set; }

        public string Kullanici { get; set; }

        public string Makina { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? StajBaslangicTarihi { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? StajBitisTarihi { get; set; }

        public int? Yil { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? Zaman { get; set; }

        public string Aciklama { get; set; }

        public string SGKullanici { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? SGZaman { get; set; }

        public int? EnumOgrenciStajDurumu { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
