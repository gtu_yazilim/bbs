namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("mkn.MekanKisiler")]
    public partial class MekanKisiler
    {
        public int ID { get; set; }

        public int? FKMekanID { get; set; }

        public int? FKPersonelID { get; set; }

        public virtual Mekan Mekan { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi1 { get; set; }
    }
}
