namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.IzinYOKDoluDetay")]
    public partial class IzinYOKDoluDetay
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public int HareketIslemID { get; set; }

        [Required]
        [StringLength(50)]
        public string Tahsil { get; set; }

        [Required]
        [StringLength(50)]
        public string KadroYeriFakulte { get; set; }

        public int EKadroDerece { get; set; }

        public int EGorevAyligiDerece { get; set; }

        public int EGorevAyligiKademe { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? EGorevAyligiTarihi { get; set; }

        public int EKazanilmisHakAyligiDerece { get; set; }

        public int EKazanilmisHakAyligiKademe { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? EKazanilmisHakAyligiTarihi { get; set; }

        public int EEmekliAyligiDerece { get; set; }

        public int EEmekliAyligiKademe { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? EEmekliAyligiTarihi { get; set; }

        public int YKadroDerece { get; set; }

        public int YGorevAyligiDerece { get; set; }

        public int YGorevAyligiKademe { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? YGorevAyligiTarihi { get; set; }

        public int YKazanilmisHakAyligiDerece { get; set; }

        public int YKazanilmisHakAyligiKademe { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? YKazanilmisHakAyligiTarihi { get; set; }

        public int YEmekliAyligiDerece { get; set; }

        public int YEmekliAyligiKademe { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? YEmekliAyligiTarihi { get; set; }

        public int? YEkGosterge { get; set; }

        public int? YUniversiteOdenegi { get; set; }

        public bool Durum { get; set; }

        public int? ENUMOgrenimTuru { get; set; }

        public int FKIzinYOKID { get; set; }

        public int FKPersonelBilgisiID { get; set; }

        public int FKEKadroBilgisiID { get; set; }

        public int? FKYKadroBilgisiID { get; set; }

        public virtual IzinYOK IzinYOK { get; set; }

        public virtual KadroBilgisi KadroBilgisi { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }
    }
}
