namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.DersYeterlilikIliski")]
    public partial class DersYeterlilikIliski
    {
        public int ID { get; set; }

        public byte? KatkiDuzeyi { get; set; }

        public int? FKYeterlilikID { get; set; }

        public int? FKDersPlanAnaID { get; set; }

        public int? FKDersPlanID { get; set; }

        public int? FKProgramBirimID { get; set; }

        public short Yil { get; set; }

        [StringLength(30)]
        public string Ekleyen { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? Zaman { get; set; }

        public byte? OgrenimSeviye { get; set; }

        public bool Silindi { get; set; }

        public virtual BolumYeterlilik BolumYeterlilik { get; set; }

        public virtual Birimler Birimler { get; set; }

        public virtual OGRDersPlan OGRDersPlan { get; set; }

        public virtual OGRDersPlanAna OGRDersPlanAna { get; set; }
    }
}
