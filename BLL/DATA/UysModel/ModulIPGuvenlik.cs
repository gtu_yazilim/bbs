namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ytk.ModulIPGuvenlik")]
    public partial class ModulIPGuvenlik
    {
        public int ID { get; set; }

        public int FKProjeID { get; set; }

        public int? FKModulID { get; set; }

        [Required]
        [StringLength(50)]
        public string IP { get; set; }

        public int EnumIPTipi { get; set; }

        public bool Engelle { get; set; }

        public DateTime TarihKayit { get; set; }

        public DateTime? TarihGuncelleme { get; set; }

        [Required]
        [StringLength(50)]
        public string Kullanici { get; set; }

        public bool Silindi { get; set; }

        [StringLength(500)]
        public string Aciklama { get; set; }

        public int FKSorumluKullaniciID { get; set; }

        public int Sira { get; set; }

        public DateTime TarihBaslangic { get; set; }

        public DateTime? TarihBitis { get; set; }

        public virtual Kullanici Kullanici1 { get; set; }

        public virtual Modul Modul { get; set; }

        public virtual Proje Proje { get; set; }
    }
}
