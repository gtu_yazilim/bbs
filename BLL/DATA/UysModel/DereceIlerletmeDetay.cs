namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.DereceIlerletmeDetay")]
    public partial class DereceIlerletmeDetay
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        [StringLength(10)]
        public string SicilNo { get; set; }

        [StringLength(50)]
        public string Adi { get; set; }

        [StringLength(50)]
        public string Soyadi { get; set; }

        [StringLength(50)]
        public string KadroUnvani { get; set; }

        [StringLength(20)]
        public string Tahsili { get; set; }

        public int? KadroDerecesi { get; set; }

        public int? EGorevAyligiDerece { get; set; }

        public int? EGorevAyligiKademe { get; set; }

        public DateTime? EGorevAyligiTarihi { get; set; }

        public int? YGorevAyligiDerece { get; set; }

        public int? YGorevAyligiKademe { get; set; }

        public DateTime? YGorevAyligiTarihi { get; set; }

        public int? EKazanilmisHakAyligiDerece { get; set; }

        public int? EKazanilmisHakAyligiKademe { get; set; }

        public DateTime? EKazanilmisHakAyligiTarihi { get; set; }

        public int? YKazanilmisHakAyligiDerece { get; set; }

        public int? YKazanilmisHakAyligiKademe { get; set; }

        public DateTime? YKazanilmisHakAyligiTarihi { get; set; }

        public int? EEmekliAyligiDerece { get; set; }

        public int? EEmekliAyligiKademe { get; set; }

        public DateTime? EEmekliAyligiTarihi { get; set; }

        public int? YEmekliAyligiDerece { get; set; }

        public int? YEmekliAyligiKademe { get; set; }

        public DateTime? YEmekliAyligiTarihi { get; set; }

        public int? EKidemYili { get; set; }

        public DateTime? EKidemYiliTarihi { get; set; }

        public int? EKidemGostergesi { get; set; }

        public int? EEkGosterge { get; set; }

        public int? YKidemYili { get; set; }

        public DateTime? YKidemYiliTarihi { get; set; }

        public int? YKidemGostergesi { get; set; }

        public int? YEkGosterge { get; set; }

        [StringLength(50)]
        public string YGorevYeri { get; set; }

        public int ENUMKadroTipi { get; set; }

        public int FKDereceIlerletmeID { get; set; }

        public int? FKHareketID { get; set; }

        public int FKPersonelBilgisiID { get; set; }

        public int? FKUlkeID { get; set; }

        public int? FKUniversiteID { get; set; }

        public int? FKFakulteID { get; set; }

        public int? ENUMSGKDurum { get; set; }

        [StringLength(250)]
        public string SGKDurumAciklama { get; set; }

        public virtual Birimler Birimler { get; set; }

        public virtual Birimler Birimler1 { get; set; }

        public virtual TNMUlke TNMUlke { get; set; }

        public virtual DereceIlerletme DereceIlerletme { get; set; }
    }
}
