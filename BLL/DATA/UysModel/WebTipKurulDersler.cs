namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.WebTipKurulDersler")]
    public partial class WebTipKurulDersler
    {
        [Key]
        public int InKod { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        public int? AnaDersPlanID { get; set; }

        [StringLength(255)]
        public string Ders_tr { get; set; }

        [StringLength(255)]
        public string Ders_en { get; set; }

        public int? Teorik { get; set; }

        public int? Pratik { get; set; }

        public bool? KulturDersi { get; set; }

        public int? Yil1 { get; set; }

        [StringLength(50)]
        public string Ekleyen { get; set; }

        public DateTime? Zaman { get; set; }
    }
}
