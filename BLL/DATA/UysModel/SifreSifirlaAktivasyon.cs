namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ytk.SifreSifirlaAktivasyon")]
    public partial class SifreSifirlaAktivasyon
    {
        public int ID { get; set; }

        public DateTime Tarih { get; set; }

        public DateTime BitisTarihi { get; set; }

        public int FKKullaniciID { get; set; }

        public Guid GeciciKod { get; set; }

        [StringLength(50)]
        public string IstekIPAdresi { get; set; }

        [StringLength(50)]
        public string OnaylamaIPAdresi { get; set; }

        public DateTime? OnaylamaTarihi { get; set; }

        public virtual Kullanici Kullanici { get; set; }
    }
}
