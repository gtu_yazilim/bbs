namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.TNMEgitimBilgisi")]
    public partial class TNMEgitimBilgisi
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TNMEgitimBilgisi()
        {
            EgitimBilgisi = new HashSet<EgitimBilgisi>();
        }

        public int ID { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public DateTime? Zaman { get; set; }

        public DateTime? BaslangicTarihi { get; set; }

        public DateTime? BitisTarihi { get; set; }

        public int FKEgitimYeriID { get; set; }

        public int FKEgitimKonusuID { get; set; }

        public int? Sure { get; set; }

        public bool? Sertifika { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EgitimBilgisi> EgitimBilgisi { get; set; }

        public virtual TNMEgitimKonusu TNMEgitimKonusu { get; set; }

        public virtual TNMEgitimYeri TNMEgitimYeri { get; set; }
    }
}
