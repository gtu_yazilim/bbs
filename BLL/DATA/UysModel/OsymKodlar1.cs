namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("osym.OsymKodlar")]
    public partial class OsymKodlar1
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }

        public double? ProgramKod { get; set; }

        public double? UnvKod { get; set; }

        [StringLength(255)]
        public string UnvAd { get; set; }

        [StringLength(255)]
        public string FakulteAd { get; set; }

        [StringLength(255)]
        public string YOAd { get; set; }

        [StringLength(255)]
        public string ProgramAd { get; set; }

        [StringLength(255)]
        public string TabloAd { get; set; }

        public double? OgrenimSuresi { get; set; }

        public double? YoksisBirimID { get; set; }

        public bool? Gosterim { get; set; }

        public int? FKBirimID { get; set; }
    }
}
