namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRDersPlan")]
    public partial class OGRDersPlan
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OGRDersPlan()
        {
            ABSBasariNot = new HashSet<ABSBasariNot>();
            ABSOgrenciNot = new HashSet<ABSOgrenciNot>();
            ABSPaylar = new HashSet<ABSPaylar>();
            OSinavGrup = new HashSet<OSinavGrup>();
            DersYeterlilikIliski = new HashSet<DersYeterlilikIliski>();
            OGRDersGrup = new HashSet<OGRDersGrup>();
            OGRDersPlan1 = new HashSet<OGRDersPlan>();
            OGRDersPlanDegisiklikAciklama = new HashSet<OGRDersPlanDegisiklikAciklama>();
            OGRDersPlanFiltre = new HashSet<OGRDersPlanFiltre>();
            OGROgrenciYazilma = new HashSet<OGROgrenciYazilma>();
            OGRMazeret = new HashSet<OGRMazeret>();
            OGROgrenciNot = new HashSet<OGROgrenciNot>();
            OGRTekDers = new HashSet<OGRTekDers>();
            SinavTakvim = new HashSet<SinavTakvim>();
            OGRDersPlanEtiket = new HashSet<OGRDersPlanEtiket>();
            OGROgrenciDersPlanAlis = new HashSet<OGROgrenciDersPlanAlis>();
        }

        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(500)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? InKod { get; set; }

        [StringLength(50)]
        public string BolKodAd { get; set; }

        [StringLength(50)]
        public string DersKod { get; set; }

        public int? Yariyil { get; set; }

        public int? GrupSayisi { get; set; }

        public int? Yil { get; set; }

        public int? EnumEBSOnay { get; set; }

        public int? EnumOgretimTur { get; set; }

        public int? EnumDonem { get; set; }

        public int? EnumDersPlanKapsam { get; set; }

        public int? EnumDersPlanOrtalama { get; set; }

        public int? EnumDersPlanZorunlu { get; set; }

        public int? EnumDersPlanIliskiNo { get; set; }

        public int? EnumDersPlanAktarma { get; set; }

        public int? EnumDersPlanWebGosterim { get; set; }

        public int? FKDersPlanAnaID { get; set; }

        public int? FKDersPlanID { get; set; }

        public DateTime? SGZaman { get; set; }

        public int? FKEtiketID { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public int? EnumOgretimSeviye { get; set; }

        public bool? UzemMi { get; set; }

        public int? FKFakulteBirimID { get; set; }

        public int? FKBolumPrBirimID { get; set; }

        public int? EnumEBSGosterim { get; set; }

        public int? KatkiOran { get; set; }

        public int EnumDersSureTip { get; set; }

        public bool OgrenciPlanaEklenmesin { get; set; }

        public int EnumDersSecimTipi { get; set; }

        public int EnumDersBirimTipi { get; set; }

        public bool Intibak { get; set; }

        public bool Butunleme { get; set; }

        public int FKBirimID { get; set; }

        public int EnumDil { get; set; }

        public int? EnumDil2 { get; set; }

        public int EnumDegerlendirmeTipi { get; set; }

        [StringLength(50)]
        public string BolKodYabAd { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSBasariNot> ABSBasariNot { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSOgrenciNot> ABSOgrenciNot { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSPaylar> ABSPaylar { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OSinavGrup> OSinavGrup { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DersYeterlilikIliski> DersYeterlilikIliski { get; set; }

        public virtual Birimler Birimler { get; set; }

        public virtual Birimler Birimler1 { get; set; }

        public virtual Birimler Birimler2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRDersGrup> OGRDersGrup { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRDersPlan> OGRDersPlan1 { get; set; }

        public virtual OGRDersPlan OGRDersPlan2 { get; set; }

        public virtual OGRDersPlanAna OGRDersPlanAna { get; set; }

        public virtual OGRTNMDersEtiket OGRTNMDersEtiket { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRDersPlanDegisiklikAciklama> OGRDersPlanDegisiklikAciklama { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRDersPlanFiltre> OGRDersPlanFiltre { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGROgrenciYazilma> OGROgrenciYazilma { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRMazeret> OGRMazeret { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGROgrenciNot> OGROgrenciNot { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRTekDers> OGRTekDers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SinavTakvim> SinavTakvim { get; set; }


        public virtual ICollection<OGRDersPlanEtiket> OGRDersPlanEtiket { get; set; }
        public virtual ICollection<OGROgrenciDersPlanAlis> OGROgrenciDersPlanAlis { get; set; }
    }
}
