namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.DersPlanAnaEtiket")]
    public partial class DersPlanAnaEtiket
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DersPlanAnaEtiket()
        {
            DersPlanEtiket = new HashSet<DersPlanEtiket>();
        }

        public int ID { get; set; }

        public short Yil { get; set; }

        public int? FKFakulteBirimID { get; set; }

        [Required]
        [StringLength(150)]
        public string HavuzDersAd { get; set; }

        [StringLength(150)]
        public string HavuzYabDersAd { get; set; }

        public byte? Zorunlu { get; set; }

        public byte DSaat { get; set; }

        public byte USaat { get; set; }

        public byte Kredi { get; set; }

        public byte? AKTS { get; set; }

        public byte? EnumDersTipi { get; set; }

        [StringLength(7)]
        public string RenkKod { get; set; }

        [StringLength(30)]
        public string Ekleyen { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? Zaman { get; set; }

        public virtual Birimler Birimler { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DersPlanEtiket> DersPlanEtiket { get; set; }
    }
}
