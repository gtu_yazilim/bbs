namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.ABSDevamTakipKontrol")]
    public partial class ABSDevamTakipKontrol
    {
        public int ID { get; set; }

        public int FKOgrenciID { get; set; }

        public int FKDersGrupID { get; set; }

        public int FKPersonelID { get; set; }

        public int FKHaftaID { get; set; }

        public int Yil { get; set; }

        public int Donem { get; set; }

        public DateTime YoklamaTarihi { get; set; }

        [StringLength(250)]
        public string DersinKonusu { get; set; }

        public int? YoklamaYapilanSaat { get; set; }

        public virtual ABSTNMHafta ABSTNMHafta { get; set; }

        public virtual OGRDersGrup OGRDersGrup { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }
    }
}
