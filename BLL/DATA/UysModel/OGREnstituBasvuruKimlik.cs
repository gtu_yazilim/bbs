namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGREnstituBasvuruKimlik")]
    public partial class OGREnstituBasvuruKimlik
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OGREnstituBasvuruKimlik()
        {
            OGREnstituBasvuruTercih = new HashSet<OGREnstituBasvuruTercih>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(12)]
        public string TCKimlikNo { get; set; }
        public bool? YuksekLisans { get; set; }
        public bool? Doktora { get; set; }
        public bool? ButunlesikDoktora { get; set; }

        [StringLength(50)]
        public string CwsOpnID { get; set; }

        [StringLength(25)]
        public string Ad { get; set; }

        [StringLength(25)]
        public string Soyad { get; set; }

        [StringLength(50)]
        public string DYeri { get; set; }

        public DateTime? DogumTarihi { get; set; }

        public int? EnumCinsiyet { get; set; }

        [StringLength(25)]
        public string BabaAd { get; set; }

        [StringLength(25)]
        public string AnaAd { get; set; }

        public int? KanGrubu { get; set; }

        public int? FKUyrukID { get; set; }

        public int? AskerlikDurumu { get; set; }

        public int? NufusIlID { get; set; }

        public int? NufusIlceID { get; set; }

        [StringLength(50)]
        public string MahKoy { get; set; }

        public int? HaneKutukAile { get; set; }

        public int? CiltNo { get; set; }

        public int? SayfaSira { get; set; }

        public string EvAdres { get; set; }

        public int? EvIlceID { get; set; }

        [StringLength(5)]
        public string EvPKod { get; set; }

        public int? EvIlID { get; set; }

        [StringLength(11)]
        public string EvTel { get; set; }

        [StringLength(11)]
        public string Gsm { get; set; }

        public string isAdres { get; set; }

        public int? isIlceID { get; set; }

        [StringLength(5)]
        public string isPKod { get; set; }

        public int? isIlID { get; set; }

        [StringLength(11)]
        public string isTel { get; set; }

        [StringLength(60)]
        public string LisansUnv { get; set; }

        public int? LisansUnvID { get; set; }

        [StringLength(60)]
        public string LisansFak { get; set; }

        public int? LisansFakID { get; set; }

        [StringLength(60)]
        public string LisansBol { get; set; }

        public int? LisansBolID { get; set; }

        [StringLength(100)]
        public string YLisansBilimDali { get; set; }

        public int? YLisansBilimDaliID { get; set; }

        [StringLength(4)]
        public string LisansMezYil { get; set; }

        public int? LisansOrtTip { get; set; }

        public double? LisansOrt { get; set; }

        [StringLength(100)]
        public string YLisansUnv { get; set; }

        public int? YLisansUnvID { get; set; }

        [StringLength(100)]
        public string YLisansFak { get; set; }

        public int? YLisansFakID { get; set; }

        [StringLength(100)]
        public string YLisansBol { get; set; }

        public int? YLisansBolID { get; set; }

        [StringLength(4)]
        public string YLisansMezYil { get; set; }

        public int? YLisansOrtTip { get; set; }

        public double? YLisansOrt { get; set; }

        public int? LesTur { get; set; }

        public double? LesSayisal { get; set; }

        public double? LesSozel { get; set; }

        public double? LesEA { get; set; }

        public int? YabDilTur { get; set; }

        public double? YabDilPuan { get; set; }

        public double? GTUYabanciDilPuan { get; set; }

        public int? YabDilSinavTip { get; set; }


        public int? Yil1 { get; set; }

        public int? Donem { get; set; }

        public DateTime? KayitTarihi { get; set; }

        [StringLength(50)]
        public string Mail { get; set; }

        [StringLength(26)]
        public string IbanNo { get; set; }

        public bool? ArsGorMu { get; set; }

        [StringLength(20)]
        public string Makina { get; set; }

        [StringLength(20)]
        public string SGMakina { get; set; }

        public DateTime? SGZamani { get; set; }

        [StringLength(10)]
        public string SeriNo { get; set; }
        public DateTime? LesTarihi { get; set; }
        public DateTime? YabanciDilTarihi { get; set; }
        public bool? GTUYabanciDilSinavinaGirecekMi { get; set; }
        public bool? KayitTamamlandi { get; set; }
        public bool? YabanciUniMezunu { get; set; }
        public bool? OgrenciIsleriOnay { get; set; }
        public bool? KayitMailiYollandiMi { get; set; }
        public string DegerlendirenKullanici { get; set; }
        public DateTime? DegerlendirmeZamani { get; set; }
        public bool? DegerlendirmeAsamasindaMi { get; set; }

        public string SonMailBilgi { get; set; }

        public virtual TNMUlke TNMUlke { get; set; }


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituBasvuruTercih> OGREnstituBasvuruTercih { get; set; }
    }
}
