namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRHarcKrediUcretlendirmesiOdeyecek")]
    public partial class OGRHarcKrediUcretlendirmesiOdeyecek
    {
        public int ID { get; set; }

        public int FKOgrenciID { get; set; }
    }
}
