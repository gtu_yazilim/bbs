namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.WebDersCiktilari")]
    public partial class WebDersCiktilari
    {
        [Key]
        public int InKod { get; set; }

        public int? AnaDersPlanID { get; set; }

        public byte? FormDil { get; set; }

        public byte? CiktiNo { get; set; }

        [StringLength(255)]
        public string CiktiMetin { get; set; }

        public short? Yil1 { get; set; }

        [StringLength(30)]
        public string Ekleyen { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? Zaman { get; set; }
    }
}
