namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.WebDersPaylarTnm")]
    public partial class WebDersPaylarTnm
    {
        [Key]
        public int InKod { get; set; }

        public byte? FormDil { get; set; }

        public byte? PayId { get; set; }

        [StringLength(20)]
        public string PayAd { get; set; }

        [StringLength(50)]
        public string PayUzunAd { get; set; }

        public short? Yil1 { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? Zaman { get; set; }
    }
}
