namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.IzinYOK")]
    public partial class IzinYOK
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public IzinYOK()
        {
            IzinYOKDetay = new HashSet<IzinYOKDetay>();
            IzinYOKDoluDetay = new HashSet<IzinYOKDoluDetay>();
        }

        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        public string Kullanici { get; set; }

        public string Makina { get; set; }

        [Required]
        [StringLength(50)]
        public string EvrakNo { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? EvrakTarihi { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? HazirlanmaTarihi { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? OnayTarihi { get; set; }

        [Required]
        public string Konu { get; set; }

        public string Aciklama { get; set; }

        [StringLength(50)]
        public string YOKEvrakNo { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? YOKEvrakTarihi { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? YOKKurulTarihi { get; set; }

        [StringLength(50)]
        public string UYKEvrakNo { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? UYKEvrakTarihi { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? UYKToplantiTarihi { get; set; }

        public int? UYKToplantiNo { get; set; }

        public int? UYKKararNo { get; set; }

        public int ENUMFormTipi { get; set; }

        public int ENUMKadroTipi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IzinYOKDetay> IzinYOKDetay { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IzinYOKDoluDetay> IzinYOKDoluDetay { get; set; }
    }
}
