namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OSYMSonuc")]
    public partial class OSYMSonuc
    {
        public int ID { get; set; }

        public int? FKOgrenciID { get; set; }

        public int? FKBolumPrID { get; set; }

        [Required]
        [StringLength(11)]
        public string TCKIMLIKNO { get; set; }

        [StringLength(11)]
        public string Numara { get; set; }

        public int? GirisYil { get; set; }

        public int? EnumKayitNedeni { get; set; }

        [StringLength(255)]
        public string AD { get; set; }

        [StringLength(255)]
        public string SOYAD { get; set; }

        [StringLength(20)]
        public string YABANCI_DIL { get; set; }

        [StringLength(20)]
        public string OKUL_KOD { get; set; }

        [StringLength(20)]
        public string OKUL_TUR { get; set; }

        public double? DIPLOMA_NOTU { get; set; }

        [StringLength(20)]
        public string ONCEKI_YIL_YERLESME_DURUMU { get; set; }

        [StringLength(2)]
        public string LYS1_MATEMATIK_DOGRU { get; set; }

        [StringLength(2)]
        public string LYS1_MATEMATIK_YANLIS { get; set; }

        [StringLength(2)]
        public string LYS1_GEOMETRI_DOGRU { get; set; }

        [StringLength(2)]
        public string LYS1_GEOMETRI_YANLIS { get; set; }

        [StringLength(2)]
        public string LYS2_FIZIK_DOGRU { get; set; }

        [StringLength(2)]
        public string LYS2_FIZIK_YANLIS { get; set; }

        [StringLength(2)]
        public string LYS2_KIMYA_DOGRU { get; set; }

        [StringLength(2)]
        public string LYS2_KIMYA_YANLIS { get; set; }

        [StringLength(2)]
        public string LYS2_BIYOLOJI_DOGRU { get; set; }

        [StringLength(2)]
        public string LYS2_BIYOLOJI_YANLIS { get; set; }

        [StringLength(2)]
        public string LYS3_EDEBIYAT_DOGRU { get; set; }

        [StringLength(2)]
        public string LYS3_EDEBIYAT_YANLIS { get; set; }

        [StringLength(2)]
        public string LYS3_COGRAFYA1_DOGRU { get; set; }

        [StringLength(2)]
        public string LYS3_COGRAFYA1_YANLIS { get; set; }

        [StringLength(2)]
        public string LYS4_TARIH_DOGRU { get; set; }

        [StringLength(2)]
        public string LYS4_TARIH_YANLIS { get; set; }

        [StringLength(2)]
        public string LYS4_COGRAFYA2_DOGRU { get; set; }

        [StringLength(2)]
        public string LYS4_COGRAFYA2_YANLIS { get; set; }

        [StringLength(2)]
        public string LYS4_FELSEFE_DOGRU { get; set; }

        [StringLength(2)]
        public string LYS4_FELSEFE_YANLIS { get; set; }

        [StringLength(2)]
        public string LYS5_DIL_DOGRU { get; set; }

        [StringLength(2)]
        public string LYS5_DIL_YANLIS { get; set; }

        [StringLength(2)]
        public string YGS_TURKCE_DOGRU { get; set; }

        [StringLength(2)]
        public string YGS_TURKCE_YANLIS { get; set; }

        [StringLength(2)]
        public string YGS_SOSYAL_BILIMLER_DOGRU { get; set; }

        [StringLength(2)]
        public string YGS_SOSYAL_BILIMLER_YANLIS { get; set; }

        [StringLength(2)]
        public string YGS_TEMEL_MATEMATIK_DOGRU { get; set; }

        [StringLength(2)]
        public string YGS_TEMEL_MATEMATIK_YANLIS { get; set; }

        [StringLength(2)]
        public string YGS_FEN_BILIMLERI_DOGRU { get; set; }

        [StringLength(2)]
        public string YGS_FEN_BILIMLERI_YANLIS { get; set; }

        [StringLength(10)]
        public string MF1_YERLESTIRME { get; set; }

        [StringLength(10)]
        public string MF1_YERLESTIRME_BASARI_SIRASI { get; set; }

        [StringLength(10)]
        public string MF2_YERLESTIRME { get; set; }

        [StringLength(10)]
        public string MF2_YERLESTIRME_BASARI_SIRASI { get; set; }

        [StringLength(10)]
        public string MF3_YERLESTIRME { get; set; }

        [StringLength(10)]
        public string MF3_YERLESTIRME_BASARI_SIRASI { get; set; }

        [StringLength(10)]
        public string MF4_YERLESTIRME { get; set; }

        [StringLength(10)]
        public string MF4_YERLESTIRME_BASARI_SIRASI { get; set; }

        [StringLength(10)]
        public string TM1_YERLESTIRME { get; set; }

        [StringLength(10)]
        public string TM1_YERLESTIRME_BASARI_SIRASI { get; set; }

        [StringLength(10)]
        public string TM2_YERLESTIRME { get; set; }

        [StringLength(10)]
        public string TM2_YERLESTIRME_BASARI_SIRASI { get; set; }

        [StringLength(10)]
        public string TM3_YERLESTIRME { get; set; }

        [StringLength(10)]
        public string TM3_YERLESTIRME_BASARI_SIRASI { get; set; }

        [StringLength(10)]
        public string TS1_YERLESTIRME { get; set; }

        [StringLength(10)]
        public string TS1_YERLESTIRME_BASARI_SIRASI { get; set; }

        [StringLength(10)]
        public string TS2_YERLESTIRME { get; set; }

        [StringLength(10)]
        public string TS2_YERLESTIRME_BASARI_SIRASI { get; set; }

        [StringLength(10)]
        public string DIL1_YERLESTIRME { get; set; }

        [StringLength(10)]
        public string DIL1_YERLESTIRME_BASARI_SIRASI { get; set; }

        [StringLength(10)]
        public string DIL2_YERLESTIRME { get; set; }

        [StringLength(10)]
        public string DIL2_YERLESTIRME_BASARI_SIRASI { get; set; }

        [StringLength(10)]
        public string DIL3_YERLESTIRME { get; set; }

        [StringLength(10)]
        public string DIL3_YERLESTIRME_BASARI_SIRASI { get; set; }

        [StringLength(10)]
        public string YGS1_YERLESTIRME { get; set; }

        [StringLength(10)]
        public string YGS1_YERLESTIRME_BASARI_SIRASI { get; set; }

        [StringLength(10)]
        public string YGS2_YERLESTIRME { get; set; }

        [StringLength(10)]
        public string YGS2_YERLESTIRME_BASARI_SIRASI { get; set; }

        [StringLength(10)]
        public string YGS3_YERLESTIRME { get; set; }

        [StringLength(10)]
        public string YGS3_YERLESTIRME_BASARI_SIRASI { get; set; }

        [StringLength(10)]
        public string YGS4_YERLESTIRME { get; set; }

        [StringLength(10)]
        public string YGS4_YERLESTIRME_BASARI_SIRASI { get; set; }

        [StringLength(10)]
        public string YGS5_YERLESTIRME { get; set; }

        [StringLength(10)]
        public string YGS5_YERLESTIRME_BASARI_SIRASI { get; set; }

        [StringLength(10)]
        public string YGS6_YERLESTIRME { get; set; }

        [StringLength(10)]
        public string YGS6_YERLESTIRME_BASARI_SIRASI { get; set; }
    }
}
