namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("osym.UniversiteKodlari")]
    public partial class UniversiteKodlari
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Kod { get; set; }

        [StringLength(255)]
        public string Ad { get; set; }

        public int? EnumUniversiteTip { get; set; }
    }
}
