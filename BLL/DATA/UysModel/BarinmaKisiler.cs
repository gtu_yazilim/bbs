namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("mkn.BarinmaKisiler")]
    public partial class BarinmaKisiler
    {
        public int ID { get; set; }

        public int? FKMekanID { get; set; }

        public int? FKPersonelID { get; set; }

        [StringLength(100)]
        public string Gorev { get; set; }

        public int? EnumTahsis { get; set; }

        public DateTime? GirisTarihi { get; set; }

        public DateTime? CikisTarihi { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(255)]
        public string Aciklama { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }

        public virtual Mekan Mekan { get; set; }
    }
}
