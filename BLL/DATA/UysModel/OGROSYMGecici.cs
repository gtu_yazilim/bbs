namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGROSYMGecici")]
    public partial class OGROSYMGecici
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(500)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? InKod { get; set; }

        public int? TC { get; set; }

        [StringLength(30)]
        public string Ad { get; set; }

        [StringLength(30)]
        public string Soyad { get; set; }

        [StringLength(30)]
        public string BabaAd { get; set; }

        [StringLength(30)]
        public string AnaAd { get; set; }

        [StringLength(2)]
        public string DogumGunu { get; set; }

        [StringLength(2)]
        public string DogumAy { get; set; }

        [StringLength(2)]
        public string DogumYil { get; set; }

        public int? Cinsiyet { get; set; }

        public int? Uyruk { get; set; }

        [StringLength(30)]
        public string DogumYeri { get; set; }

        [StringLength(2)]
        public string NufusIl { get; set; }

        [StringLength(4)]
        public string NufusIlce { get; set; }

        [StringLength(6)]
        public string OkulKodu { get; set; }

        [StringLength(5)]
        public string OkulTuru { get; set; }

        [StringLength(4)]
        public string OkulKolu { get; set; }

        public int? OgrenimDurumu { get; set; }

        public int? MezuniyetYil { get; set; }

        public int? LiseAdayTurKod { get; set; }

        public int? MeslekLisMezun { get; set; }

        [StringLength(7)]
        public string MSGBasariPuan { get; set; }

        public int? YabanciDil { get; set; }

        public int? KatkiKredisiIstek { get; set; }

        public int? OgrenimKredisiIstek { get; set; }

        [StringLength(51)]
        public string Adres1 { get; set; }

        [StringLength(50)]
        public string Adres2 { get; set; }

        [StringLength(30)]
        public string Semt_Ilce { get; set; }

        [StringLength(5)]
        public string PostaKodu { get; set; }

        [StringLength(2)]
        public string AdresIl { get; set; }

        public int? TelefonAlanKod { get; set; }

        public int? TelefonNo { get; set; }

        public int? OkulBirincisi { get; set; }

        [StringLength(15)]
        public string SOZ_1 { get; set; }

        [StringLength(15)]
        public string SAY_1 { get; set; }

        [StringLength(15)]
        public string EA_1 { get; set; }

        [StringLength(15)]
        public string Dil { get; set; }

        [StringLength(15)]
        public string SOZ_2 { get; set; }

        [StringLength(15)]
        public string SAY_2 { get; set; }

        [StringLength(15)]
        public string EA_2 { get; set; }

        [StringLength(15)]
        public string Y_SOZ_1_03 { get; set; }

        [StringLength(15)]
        public string Y_SAY_1_03 { get; set; }

        [StringLength(15)]
        public string Y_EA_1_03 { get; set; }

        [StringLength(15)]
        public string Y_Dil_03 { get; set; }

        [StringLength(15)]
        public string Y_SOZ_2_03 { get; set; }

        [StringLength(15)]
        public string Y_SAY_2_03 { get; set; }

        [StringLength(15)]
        public string Y_EA_2_03 { get; set; }

        [StringLength(15)]
        public string Y_SOZ_1_08 { get; set; }

        [StringLength(15)]
        public string Y_SAY_1_08 { get; set; }

        [StringLength(15)]
        public string Y_EA_1_08 { get; set; }

        [StringLength(15)]
        public string Y_Dil_08 { get; set; }

        [StringLength(15)]
        public string Y_SOZ_2_08 { get; set; }

        [StringLength(15)]
        public string Y_SAY_2_08 { get; set; }

        [StringLength(15)]
        public string Y_EA_2_08 { get; set; }

        [StringLength(15)]
        public string Y_SOZ_1_0824 { get; set; }

        [StringLength(15)]
        public string Y_SAY_1_0824 { get; set; }

        [StringLength(15)]
        public string Y_EA_1_0824 { get; set; }

        [StringLength(15)]
        public string Y_Dil_0824 { get; set; }

        [StringLength(15)]
        public string Y_SOZ_2_0824 { get; set; }

        [StringLength(15)]
        public string Y_SAY_2_0824 { get; set; }

        [StringLength(15)]
        public string Y_EA_2_0824 { get; set; }

        [StringLength(15)]
        public string Y_SOZ_1_BasariSira_03 { get; set; }

        [StringLength(15)]
        public string Y_SAY_1_BasariSira_03 { get; set; }

        [StringLength(15)]
        public string Y_EA_1_BasariSira_03 { get; set; }

        [StringLength(15)]
        public string Y_Dil_BasariSira_03 { get; set; }

        [StringLength(15)]
        public string Y_SOZ_2_BasariSira_03 { get; set; }

        [StringLength(15)]
        public string Y_SAY_2_BasariSira_03 { get; set; }

        [StringLength(15)]
        public string Y_EA_2_BasariSira_03 { get; set; }

        [StringLength(15)]
        public string Y_SOZ_1_BasariSira_08 { get; set; }

        [StringLength(15)]
        public string Y_SAY_1_BasariSira_08 { get; set; }

        [StringLength(15)]
        public string Y_EA_1_BasariSira_08 { get; set; }

        [StringLength(15)]
        public string Y_Dil_BasariSira_08 { get; set; }

        [StringLength(15)]
        public string Y_SOZ_2_BasariSira_08 { get; set; }

        [StringLength(15)]
        public string Y_SAY_2_BasariSira_08 { get; set; }

        [StringLength(15)]
        public string Y_EA_2_BasariSira_08 { get; set; }

        [StringLength(15)]
        public string Y_SOZ_1_BasariSira_0824 { get; set; }

        [StringLength(15)]
        public string Y_SAY_1_BasariSira_0824 { get; set; }

        [StringLength(15)]
        public string Y_EA_1_BasariSira_0824 { get; set; }

        [StringLength(15)]
        public string Y_Dil_BasariSira_0824 { get; set; }

        [StringLength(15)]
        public string Y_SOZ_2_BasariSira_0824 { get; set; }

        [StringLength(15)]
        public string Y_SAY_2_BasariSira_0824 { get; set; }

        [StringLength(15)]
        public string Y_EA_2_BasariSira_0824 { get; set; }

        [StringLength(15)]
        public string Y_SOZ_1_PuanYuzde_03 { get; set; }

        [StringLength(15)]
        public string Y_SAY_1_PuanYuzde_03 { get; set; }

        [StringLength(15)]
        public string Y_EA_1_PuanYuzde_03 { get; set; }

        [StringLength(15)]
        public string Y_Dil_PuanYuzde_03 { get; set; }

        [StringLength(15)]
        public string Y_SOZ_2_PuanYuzde_03 { get; set; }

        [StringLength(15)]
        public string Y_SAY_2_PuanYuzde_03 { get; set; }

        [StringLength(15)]
        public string Y_EA_2_PuanYuzde_03 { get; set; }

        [StringLength(15)]
        public string Y_SOZ_1_PuanYuzde_08 { get; set; }

        [StringLength(15)]
        public string Y_SAY_1_PuanYuzde_08 { get; set; }

        [StringLength(15)]
        public string Y_EA_1_PuanYuzde_08 { get; set; }

        [StringLength(15)]
        public string Y_Dil_PuanYuzde_08 { get; set; }

        [StringLength(15)]
        public string Y_SOZ_2_PuanYuzde_08 { get; set; }

        [StringLength(15)]
        public string Y_SAY_2_PuanYuzde_08 { get; set; }

        [StringLength(15)]
        public string Y_EA_2_PuanYuzde_08 { get; set; }

        [StringLength(15)]
        public string Y_SOZ_1_PuanYuzde_0824 { get; set; }

        [StringLength(15)]
        public string Y_SAY_1_PuanYuzde_0824 { get; set; }

        [StringLength(15)]
        public string Y_EA_1_PuanYuzde_0824 { get; set; }

        [StringLength(15)]
        public string Y_Dil_PuanYuzde_0824 { get; set; }

        [StringLength(15)]
        public string Y_SOZ_2_PuanYuzde_0824 { get; set; }

        [StringLength(15)]
        public string Y_SAY_2_PuanYuzde_0824 { get; set; }

        [StringLength(15)]
        public string Y_EA_2_PuanYuzde_0824 { get; set; }

        [StringLength(15)]
        public string YerlesKullanilanPuan { get; set; }

        [StringLength(15)]
        public string YerlesKullanilanBasariSira { get; set; }

        [StringLength(15)]
        public string YerlesKullanilanBasariYuzde { get; set; }

        public int? GirdigiBolumKod { get; set; }

        public int? YerlesTercihSira { get; set; }

        public int? YerlesPuanTuru { get; set; }

        [StringLength(14)]
        public string YerlesmeTuru { get; set; }

        public int? YerlesmeDurumu { get; set; }

        public int? Yil1 { get; set; }

        [StringLength(2)]
        public string FakKod { get; set; }

        [StringLength(2)]
        public string BolKod { get; set; }

        public int? OgretimTur { get; set; }

        [StringLength(11)]
        public string Numara { get; set; }

        [StringLength(15)]
        public string Harc { get; set; }

        public DateTime? Harc_Tarih { get; set; }

        public int? Aktarma { get; set; }

        public int? Uyruk1 { get; set; }

        public int? Uyruk2 { get; set; }

        public int? Uyruk3 { get; set; }

        public int? TelEvUlkeKodu { get; set; }

        public int? CepTelUlkeKodu { get; set; }

        public int? CepTelAlanKod { get; set; }

        public int? CepTel { get; set; }

        [StringLength(15)]
        public string YG1 { get; set; }

        [StringLength(15)]
        public string YG2 { get; set; }

        [StringLength(15)]
        public string YG3 { get; set; }

        [StringLength(15)]
        public string YG4 { get; set; }

        [StringLength(15)]
        public string YG5 { get; set; }

        [StringLength(15)]
        public string YG6 { get; set; }

        [StringLength(15)]
        public string MF1 { get; set; }

        [StringLength(15)]
        public string MF2 { get; set; }

        [StringLength(15)]
        public string MF3 { get; set; }

        [StringLength(15)]
        public string MF4 { get; set; }

        [StringLength(15)]
        public string TM1 { get; set; }

        [StringLength(15)]
        public string TM2 { get; set; }

        [StringLength(15)]
        public string TM3 { get; set; }

        [StringLength(15)]
        public string TS1 { get; set; }

        [StringLength(15)]
        public string TS2 { get; set; }

        [StringLength(15)]
        public string DIL1 { get; set; }

        [StringLength(15)]
        public string DIL2 { get; set; }

        [StringLength(15)]
        public string DIL3 { get; set; }

        [StringLength(15)]
        public string YG1_012 { get; set; }

        [StringLength(15)]
        public string YG2_012 { get; set; }

        [StringLength(15)]
        public string YG3_012 { get; set; }

        [StringLength(15)]
        public string YG4_012 { get; set; }

        [StringLength(15)]
        public string YG5_012 { get; set; }

        [StringLength(15)]
        public string YG6_012 { get; set; }

        [StringLength(15)]
        public string MF1_012 { get; set; }

        [StringLength(15)]
        public string MF2_012 { get; set; }

        [StringLength(15)]
        public string MF3_012 { get; set; }

        [StringLength(15)]
        public string MF4_012 { get; set; }

        [StringLength(15)]
        public string TM1_012 { get; set; }

        [StringLength(15)]
        public string TM2_012 { get; set; }

        [StringLength(15)]
        public string TM3_012 { get; set; }

        [StringLength(15)]
        public string TS1_012 { get; set; }

        [StringLength(15)]
        public string TS2_012 { get; set; }

        [StringLength(15)]
        public string DIL1_012 { get; set; }

        [StringLength(15)]
        public string DIL2_012 { get; set; }

        [StringLength(15)]
        public string DIL3_012 { get; set; }

        [StringLength(15)]
        public string YG1_015 { get; set; }

        [StringLength(15)]
        public string YG2_015 { get; set; }

        [StringLength(15)]
        public string YG3_015 { get; set; }

        [StringLength(15)]
        public string YG4_015 { get; set; }

        [StringLength(15)]
        public string YG5_015 { get; set; }

        [StringLength(15)]
        public string YG6_015 { get; set; }

        [StringLength(15)]
        public string MF1_015 { get; set; }

        [StringLength(15)]
        public string MF2_015 { get; set; }

        [StringLength(15)]
        public string MF3_015 { get; set; }

        [StringLength(15)]
        public string MF4_015 { get; set; }

        [StringLength(15)]
        public string TM1_015 { get; set; }

        [StringLength(15)]
        public string TM2_015 { get; set; }

        [StringLength(15)]
        public string TM3_015 { get; set; }

        [StringLength(15)]
        public string TS1_015 { get; set; }

        [StringLength(15)]
        public string TS2_015 { get; set; }

        [StringLength(15)]
        public string DIL1_015 { get; set; }

        [StringLength(15)]
        public string DIL2_015 { get; set; }

        [StringLength(15)]
        public string DIL3_015 { get; set; }

        [StringLength(15)]
        public string YG1_EkPuan { get; set; }

        [StringLength(15)]
        public string YG2_EkPuan { get; set; }

        [StringLength(15)]
        public string YG3_EkPuan { get; set; }

        [StringLength(15)]
        public string YG4_EkPuan { get; set; }

        [StringLength(15)]
        public string YG5_EkPuan { get; set; }

        [StringLength(15)]
        public string YG6_EkPuan { get; set; }

        [StringLength(15)]
        public string MF1_EkPuan { get; set; }

        [StringLength(15)]
        public string MF2_EkPuan { get; set; }

        [StringLength(15)]
        public string MF3_EkPuan { get; set; }

        [StringLength(15)]
        public string MF4_EkPuan { get; set; }

        [StringLength(15)]
        public string TM1_EkPuan { get; set; }

        [StringLength(15)]
        public string TM2_EkPuan { get; set; }

        [StringLength(15)]
        public string TM3_EkPuan { get; set; }

        [StringLength(15)]
        public string TS1_EkPuan { get; set; }

        [StringLength(15)]
        public string TS2_EkPuan { get; set; }

        [StringLength(15)]
        public string DIL1_EkPuan { get; set; }

        [StringLength(15)]
        public string DIL2_EkPuan { get; set; }

        [StringLength(15)]
        public string DIL3_EkPuan { get; set; }

        [StringLength(15)]
        public string YG1_012_BasariSira { get; set; }

        [StringLength(15)]
        public string YG2_012_BasariSira { get; set; }

        [StringLength(15)]
        public string YG3_012_BasariSira { get; set; }

        [StringLength(15)]
        public string YG4_012_BasariSira { get; set; }

        [StringLength(15)]
        public string YG5_012_BasariSira { get; set; }

        [StringLength(15)]
        public string YG6_012_BasariSira { get; set; }

        [StringLength(15)]
        public string MF1_012_BasariSira { get; set; }

        [StringLength(15)]
        public string MF2_012_BasariSira { get; set; }

        [StringLength(15)]
        public string MF3_012_BasariSira { get; set; }

        [StringLength(15)]
        public string MF4_012_BasariSira { get; set; }

        [StringLength(15)]
        public string TM1_012_BasariSira { get; set; }

        [StringLength(15)]
        public string TM2_012_BasariSira { get; set; }

        [StringLength(15)]
        public string TM3_012_BasariSira { get; set; }

        [StringLength(15)]
        public string TS1_012_BasariSira { get; set; }

        [StringLength(15)]
        public string TS2_012_BasariSira { get; set; }

        [StringLength(15)]
        public string DIL1_012_BasariSira { get; set; }

        [StringLength(15)]
        public string DIL2_012_BasariSira { get; set; }

        [StringLength(15)]
        public string DIL3_012_BasariSira { get; set; }

        [StringLength(15)]
        public string YG1_015_BasariSira { get; set; }

        [StringLength(15)]
        public string YG2_015_BasariSira { get; set; }

        [StringLength(15)]
        public string YG3_015_BasariSira { get; set; }

        [StringLength(15)]
        public string YG4_015_BasariSira { get; set; }

        [StringLength(15)]
        public string YG5_015_BasariSira { get; set; }

        [StringLength(15)]
        public string YG6_015_BasariSira { get; set; }

        [StringLength(15)]
        public string MF1_015_BasariSira { get; set; }

        [StringLength(15)]
        public string MF2_015_BasariSira { get; set; }

        [StringLength(15)]
        public string MF3_015_BasariSira { get; set; }

        [StringLength(15)]
        public string MF4_015_BasariSira { get; set; }

        [StringLength(15)]
        public string TM1_015_BasariSira { get; set; }

        [StringLength(15)]
        public string TM2_015_BasariSira { get; set; }

        [StringLength(15)]
        public string TM3_015_BasariSira { get; set; }

        [StringLength(15)]
        public string TS1_015_BasariSira { get; set; }

        [StringLength(15)]
        public string TS2_015_BasariSira { get; set; }

        [StringLength(15)]
        public string DIL1_015_BasariSira { get; set; }

        [StringLength(15)]
        public string DIL2_015_BasariSira { get; set; }

        [StringLength(15)]
        public string DIL3_015_BasariSira { get; set; }

        [StringLength(15)]
        public string YG1_EkPuan_BasariSira { get; set; }

        [StringLength(15)]
        public string YG2_EkPuan_BasariSira { get; set; }

        [StringLength(15)]
        public string YG3_EkPuan_BasariSira { get; set; }

        [StringLength(15)]
        public string YG4_EkPuan_BasariSira { get; set; }

        [StringLength(15)]
        public string YG5_EkPuan_BasariSira { get; set; }

        [StringLength(15)]
        public string YG6_EkPuan_BasariSira { get; set; }

        [StringLength(15)]
        public string MF1_EkPuan_BasariSira { get; set; }

        [StringLength(15)]
        public string MF2_EkPuan_BasariSira { get; set; }

        [StringLength(15)]
        public string MF3_EkPuan_BasariSira { get; set; }

        [StringLength(15)]
        public string MF4_EkPuan_BasariSira { get; set; }

        [StringLength(15)]
        public string TM1_EkPuan_BasariSira { get; set; }

        [StringLength(15)]
        public string TM2_EkPuan_BasariSira { get; set; }

        [StringLength(15)]
        public string TM3_EkPuan_BasariSira { get; set; }

        [StringLength(15)]
        public string TS1_EkPuan_BasariSira { get; set; }

        [StringLength(15)]
        public string TS2_EkPuan_BasariSira { get; set; }

        [StringLength(15)]
        public string DIL1_EkPuan_BasariSira { get; set; }

        [StringLength(15)]
        public string DIL2_EkPuan_BasariSira { get; set; }

        [StringLength(15)]
        public string DIL3_EkPuan_BasariSira { get; set; }
    }
}
