namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRDersProgramiv2")]
    public partial class OGRDersProgramiv2
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OGRDersProgramiv2()
        {
            ABSDevamTakipV2 = new HashSet<ABSDevamTakipV2>();
        }

        public int ID { get; set; }

        public DateTime TarihKayit { get; set; }

        public DateTime DersTarihSaat { get; set; }

        public int DersYil { get; set; }

        public int DersAy { get; set; }

        public int DersGun { get; set; }

        public int DersSaat { get; set; }

        public int? FKDersHaftaIliskiID { get; set; }

        public int FKMekanv2ID { get; set; }

        public int? FKDersKonuID { get; set; }

        public int FKDersGrupHocaID { get; set; }

        public int? EnumDersSaatTipi { get; set; }

        public int? YilinHaftasi { get; set; }

        [StringLength(50)]
        public string EkleyenKullanici { get; set; }

        [StringLength(50)]
        public string EkleyenMakina { get; set; }

        public int? FKEskiMekanID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSDevamTakipV2> ABSDevamTakipV2 { get; set; }

        public virtual DersHaftaIliski DersHaftaIliski { get; set; }

        public virtual DersKonu DersKonu { get; set; }

        public virtual MekanV2 MekanV2 { get; set; }

        public virtual Mekan Mekan { get; set; }

        public virtual OGRDersGrupHoca OGRDersGrupHoca { get; set; }
    }
}
