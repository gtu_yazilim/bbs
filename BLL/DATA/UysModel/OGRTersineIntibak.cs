namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRTersineIntibak")]
    public partial class OGRTersineIntibak
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(200)]
        public string Kullanici { get; set; }

        [StringLength(200)]
        public string Makina { get; set; }

        [StringLength(500)]
        public string DersiniAlan { get; set; }

        [StringLength(500)]
        public string DersiniAlmisSayilir { get; set; }

        [StringLength(500)]
        public string EkAciklama { get; set; }

        public int? FKDPADersiniAlanID { get; set; }

        public int? FKDPADersiniAlmisSayilirID { get; set; }

        public virtual OGRDersPlanAna OGRDersPlanAna { get; set; }

        public virtual OGRDersPlanAna OGRDersPlanAna1 { get; set; }
    }
}
