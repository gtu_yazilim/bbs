namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("gen.osymUniversite")]
    public partial class osymUniversite
    {
        [Key]
        public double universite_kodu { get; set; }

        [StringLength(255)]
        public string universite_adi { get; set; }
    }
}
