namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("bnk.BankaWebServisHareketi")]
    public partial class BankaWebServisHareketi
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BankaWebServisHareketi()
        {
            BankaOdemeler = new HashSet<BankaOdemeler>();
        }

        public int ID { get; set; }

        public int FKHesapID { get; set; }

        public DateTime Zaman { get; set; }

        public DateTime? BankaSaati { get; set; }

        public DateTime? SorguBaslamaZamani { get; set; }

        public DateTime? SorguBitisZamani { get; set; }

        [StringLength(10)]
        public string ReturnValue { get; set; }

        public string ReturnText { get; set; }

        public DateTime? SonHesaphareketTarihi { get; set; }

        public int? HareketSayisi { get; set; }

        public int? AktarilanOdemeSayisi { get; set; }

        public virtual BankaKurumHesapNumaralari BankaKurumHesapNumaralari { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BankaOdemeler> BankaOdemeler { get; set; }
    }
}
