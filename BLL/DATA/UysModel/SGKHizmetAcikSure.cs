namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.SGKHizmetAcikSure")]
    public partial class SGKHizmetAcikSure
    {
        public int ID { get; set; }

        [StringLength(11)]
        public string Tckn { get; set; }

        public int? kayitNo { get; set; }

        public int? acikSekil { get; set; }

        public int? durum { get; set; }

        public int? hizmetDurum { get; set; }

        public string husus { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? acigaAlinmaTarih { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? goreveSonTarih { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? goreveIadeIstemTarih { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? goreveIadeTarih { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? acikAylikBasTarihi { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? acikAylikBitTarihi { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? goreveSonAylikBasTarihi { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? goreveSonAylikBitTarihi { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? sYonetimKaldTarih { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? aciktanAtanmaTarih { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? kurumOnayTarihi { get; set; }

        [StringLength(100)]
        public string kullaniciAd { get; set; }

        [StringLength(100)]
        public string sifre { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? zaman { get; set; }

        public int? FKPersonelBilgisiID { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }
    }
}
