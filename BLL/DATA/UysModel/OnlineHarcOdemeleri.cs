namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("bnk.OnlineHarcOdemeleri")]
    public partial class OnlineHarcOdemeleri
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OnlineHarcOdemeleri()
        {
            OnlineHarcBorclari = new HashSet<OnlineHarcBorclari>();
            OGRGeciciOgrenciNumaralari = new HashSet<OGRGeciciOgrenciNumaralari>();
            OGRHarcOdeme = new HashSet<OGRHarcOdeme>();
        }

        public int ID { get; set; }

        public long? TCKimlikNo { get; set; }

        [StringLength(11)]
        public string OgrenciNo { get; set; }

        public int Yil { get; set; }

        public int Donem { get; set; }

        public DateTime Zaman { get; set; }

        public double Odeme { get; set; }

        [Required]
        [StringLength(30)]
        public string OdemeNumarasi { get; set; }

        public long RefersansID { get; set; }

        public DateTime OdemeTarihi { get; set; }

        [StringLength(20)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public int? FKIptalID { get; set; }

        [Required]
        [StringLength(50)]
        public string FaturaTipi { get; set; }

        public bool? AktarildiMi { get; set; }

        public DateTime? AktarimZamani { get; set; }

        [StringLength(11)]
        public string HataliOgrenciNoTCNo { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGMakina { get; set; }

        public int? FKOgrenciID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OnlineHarcBorclari> OnlineHarcBorclari { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRGeciciOgrenciNumaralari> OGRGeciciOgrenciNumaralari { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRHarcOdeme> OGRHarcOdeme { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
