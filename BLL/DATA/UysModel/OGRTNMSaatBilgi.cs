namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRTNMSaatBilgi")]
    public partial class OGRTNMSaatBilgi
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public int? InKod { get; set; }

        public int? Sayi { get; set; }

        public int? Yariyil { get; set; }

        public int? Yil { get; set; }

        public int? EnumDersPlanZorunlu { get; set; }

        public int? FKEtiketID { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public int? FKBolumPrBirimID { get; set; }

        public int AKTS { get; set; }

        public int EnumDersSecimTipi { get; set; }

        public virtual Birimler Birimler { get; set; }

        public virtual OGRTNMDersEtiket OGRTNMDersEtiket { get; set; }
    }
}
