namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ytk.RolKullanici")]
    public partial class RolKullanici
    {
        public int ID { get; set; }

        public int FKRolID { get; set; }

        public string MetaData { get; set; }

        public int FKKullaniciID { get; set; }

        public virtual Kullanici Kullanici { get; set; }

        public virtual Rol Rol { get; set; }
    }
}
