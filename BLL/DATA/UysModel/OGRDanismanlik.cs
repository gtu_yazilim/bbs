namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRDanismanlik")]
    public partial class OGRDanismanlik
    {
        public int ID { get; set; }

        public int? FKOgrenciID { get; set; }

        public int? EnumKaldigiYer { get; set; }

        public int? KaldigiYerKisi { get; set; }

        [StringLength(100)]
        public string CalistigiKurum { get; set; }

        public bool? SosyalGuvence { get; set; }

        public bool? AnneSag { get; set; }

        public bool? BabaSag { get; set; }

        public bool? AnneBabaAyri { get; set; }

        [StringLength(50)]
        public string AnneMeslek { get; set; }

        [StringLength(50)]
        public string BabaMeslek { get; set; }

        public int? ToplamGelir { get; set; }

        public int? EnumOturdugunuzEv { get; set; }

        [StringLength(50)]
        public string KronikHastalik { get; set; }

        public int? ToplamKardes { get; set; }

        public int? EnumEkonomikDurum { get; set; }

        public bool? AldiginizBurs { get; set; }

        [StringLength(200)]
        public string Burslar { get; set; }

        [StringLength(255)]
        public string BilgisayarBilgisi { get; set; }

        [StringLength(255)]
        public string YabancidilBilgisi { get; set; }

        [StringLength(255)]
        public string KursSertifikalar { get; set; }

        [StringLength(255)]
        public string isDeneyimi { get; set; }

        public int? FKPersonelID { get; set; }

        public DateTime? zaman { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
