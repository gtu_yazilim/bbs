namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.WebBolumAlanYeterlilikler")]
    public partial class WebBolumAlanYeterlilikler
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }

        [StringLength(3)]
        public string TemelAlan { get; set; }

        public int? Duzey { get; set; }

        [StringLength(16)]
        public string Turu { get; set; }

        public byte? Kategori { get; set; }

        [Column(TypeName = "text")]
        public string Aciklama { get; set; }

        public int? Sira { get; set; }

        public byte? FormDil { get; set; }
    }
}
