namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.DilBolumBilgi")]
    public partial class DilBolumBilgi
    {
        public int ID { get; set; }

        public int? FKBolumBilgiID { get; set; }

        public byte? EnumBolumBilgiTur { get; set; }

        [Column(TypeName = "text")]
        public string Icerik { get; set; }

        public byte? EnumDilID { get; set; }

        [StringLength(50)]
        public string Ekleyen { get; set; }

        public DateTime? Zaman { get; set; }

        public virtual BolumBilgi BolumBilgi { get; set; }
    }
}
