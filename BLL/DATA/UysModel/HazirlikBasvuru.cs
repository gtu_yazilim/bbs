namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("basvuru.HazirlikBasvuru")]
    public partial class HazirlikBasvuru
    {
        public int ID { get; set; }

        public DateTime TarihKayit { get; set; }

        [Required]
        [StringLength(50)]
        public string Kullanici { get; set; }

        public int EnumSecimTipi { get; set; }

        public int FKOgrenciID { get; set; }

        public DateTime? TarihGuncelleme { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
