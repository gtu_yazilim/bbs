namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.TNMSporYilPuan")]
    public partial class TNMSporYilPuan
    {
        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string SporYili { get; set; }

        public int Puan { get; set; }

        public int? Yil { get; set; }
    }
}
