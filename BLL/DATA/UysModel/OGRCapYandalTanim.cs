namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRCapYandalTanim")]
    public partial class OGRCapYandalTanim
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OGRCapYandalTanim()
        {
            CapYandalKontenjan = new HashSet<CapYandalKontenjan>();
        }

        public int ID { get; set; }

        public int EnumGecisTipi { get; set; }

        public int FKAnaBirimID { get; set; }

        public int FKAltBirimID { get; set; }

        public bool Silindi { get; set; }

        [Required]
        [StringLength(50)]
        public string Kullanici { get; set; }

        public DateTime TarihKayit { get; set; }

        public int? FKEskiAnaBirimID { get; set; }

        public int? FKEskiAltBirimID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CapYandalKontenjan> CapYandalKontenjan { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRCapDersPlan> OGRCapDersPlan { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRCapSecmeliTanim> OGRCapSecmeliTanim { get; set; }

    }
}
