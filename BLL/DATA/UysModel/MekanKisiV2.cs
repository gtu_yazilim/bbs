namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("mek.MekanKisiV2")]
    public partial class MekanKisiV2
    {
        public int ID { get; set; }

        public int? FKKisiID { get; set; }

        public int? FKMekanID { get; set; }

        public bool? Aktif { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        public DateTime? Zaman { get; set; }

        public DateTime? SGZaman { get; set; }

        public virtual MekanV2 MekanV2 { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }
    }
}
