namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGREnstituYeterlilikKomite")]
    public partial class OGREnstituYeterlilikKomite
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public DateTime? Tarih { get; set; }

        [StringLength(10)]
        public string YKNo { get; set; }

        public int? FKBaskanID { get; set; }

        public int? FKUye1ID { get; set; }

        public int? FKUye2ID { get; set; }

        public int? FKUye3ID { get; set; }

        public int? FKUye4ID { get; set; }

        public DateTime? BaslamaTarih { get; set; }

        public DateTime? BitisTarih { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public int? FKBolumPrBirimID { get; set; }

        public virtual Birimler Birimler { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi1 { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi2 { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi3 { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi4 { get; set; }
    }
}
