namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.ABSPaylarOlcme")]
    public partial class ABSPaylarOlcme
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ABSPaylarOlcme()
        {
            ABSOgrenciNotDetay = new HashSet<ABSOgrenciNotDetay>();
            ABSPaylarOlcmeOgrenme = new HashSet<ABSPaylarOlcmeOgrenme>();
            ABSPaylarOlcmeProgram = new HashSet<ABSPaylarOlcmeProgram>();
            OSinavSoru = new HashSet<OSinavSoru>();
        }

        public int ID { get; set; }

        public int? FKPaylarID { get; set; }

        public byte? SoruNo { get; set; }

        public double? SoruPuan { get; set; }

        public int? FKKoordinatorID { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? Zaman { get; set; }

        public int? EnumSinavTuru { get; set; }

        public int? FKABSOrtakDegerlendirmeID { get; set; }

        public int? EnumSoruTipi { get; set; }

        public int? FKOSinavSoruID { get; set; }

        public int? FKOOdevID { get; set; }

        public bool? Silindi { get; set; }

        public DateTime? SilindiTarih { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        [StringLength(30)]
        public string SGIP { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSOgrenciNotDetay> ABSOgrenciNotDetay { get; set; }

        public virtual ABSPaylar ABSPaylar { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSPaylarOlcmeOgrenme> ABSPaylarOlcmeOgrenme { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSPaylarOlcmeProgram> ABSPaylarOlcmeProgram { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OSinavSoru> OSinavSoru { get; set; }
    }
}
