namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.YoksisAktarimDurumu")]
    public partial class YoksisAktarimDurumu
    {
        public int ID { get; set; }

        public DateTime Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        public int FKOgrenciID { get; set; }

        [StringLength(500)]
        public string Aciklama { get; set; }

        public DateTime? SonYoksisAktarimGirisimiZamani { get; set; }

        public DateTime? SonBasariliYoksisAktarimZamani { get; set; }

        public DateTime? SonAktarimTalepZamani { get; set; }

        public bool AktarildiMi { get; set; }

        public int? FKYokAktarimID { get; set; }

        public Guid? TalepGuid { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }

        public virtual YOKAktarim YOKAktarim { get; set; }
    }
}
