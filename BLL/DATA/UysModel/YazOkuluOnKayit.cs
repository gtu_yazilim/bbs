namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("basvuru.YazOkuluOnKayit")]
    public partial class YazOkuluOnKayit
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public YazOkuluOnKayit()
        {
            YazOkuluOnKayitDersler = new HashSet<YazOkuluOnKayitDersler>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(11)]
        public string TCNo { get; set; }

        public long OgrencilikYoksisID { get; set; }

        [Required]
        [StringLength(50)]
        public string ReferansNo { get; set; }

        public DateTime TarihKayit { get; set; }

        [Required]
        [StringLength(50)]
        public string IP { get; set; }

        public DateTime? TarihGuncelleme { get; set; }

        public int Yil { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<YazOkuluOnKayitDersler> YazOkuluOnKayitDersler { get; set; }
    }
}
