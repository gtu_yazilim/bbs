namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yoksis.YokApiUygulamaYetkiIP")]
    public partial class YokApiUygulamaYetkiIP
    {
        public int ID { get; set; }

        public int FKUygulamaID { get; set; }

        [Required]
        [StringLength(250)]
        public string IP { get; set; }

        [Required]
        [StringLength(50)]
        public string Kullanici { get; set; }

        public DateTime TarihKayit { get; set; }

        public bool Silindi { get; set; }

        public virtual YokApiUygulamaYetki YokApiUygulamaYetki { get; set; }
    }
}
