namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGREnstituKontenjan")]
    public partial class OGREnstituKontenjan
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OGREnstituKontenjan()
        {
            OGREnstituBasvuruTercih = new HashSet<OGREnstituBasvuruTercih>();
        }

        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public int Kontenjan { get; set; }

        [Column(TypeName = "text")]
        [Required]
        public string Nitelik { get; set; }

        public bool AlesVarMi { get; set; }

        public bool EAVarMi { get; set; }

        public bool SOZVarMi { get; set; }

        public bool SAYVarMi { get; set; }

        public int MinAlesNotu { get; set; }

        public int AlesYuzdesi { get; set; }

        public int MinBilimSinavNotu { get; set; }

        public int BilimSinavYuzdesi { get; set; }

        //public int MinDilSinavNotu { get; set; }

        public int DilSinavYuzdesi { get; set; }

        //public double MinMezNotu { get; set; }

        public int MezNotuYuzdesi { get; set; }

        public int EnumProgramTipi { get; set; }

        public int Yil { get; set; }

        public int EnumDonem { get; set; }

        public int? FKBilimDaliID { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public int FKBolumPrBirimID { get; set; }

        public bool SonHal { get; set; }

        public int KesinlesenYedekListe { get; set; }

        public bool? TercihUcretsizMi { get; set; }

        public int? FKKontenjanAdiID { get; set; }

        public bool Aktif { get; set; }

        public int? FKOGREnstituBasvuruAcilmaDonemleriID { get; set; }

        public virtual Birimler Birimler { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituBasvuruTercih> OGREnstituBasvuruTercih { get; set; }

        public virtual OGRTNMBilimDali OGRTNMBilimDali { get; set; }

        public virtual TNMEnstituKontenjanAdi TNMEnstituKontenjanAdi { get; set; }

        public virtual OGREnstituBasvuruAcilmaDonemleri OGREnstituBasvuruAcilmaDonemleri { get; set; }
    }
}
