namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("gen.TelefonBilgisi")]
    public partial class TelefonBilgisi
    {
        public int ID { get; set; }

        public DateTime TarihKayit { get; set; }

        [StringLength(250)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public DateTime GuncellemeTarih { get; set; }

        [StringLength(250)]
        public string GuncellemeKullanici { get; set; }

        public int FKKisiID { get; set; }

        public int? FKOgrenciID { get; set; }

        public int? FKPersonelID { get; set; }

        public int EnumTelefonTip { get; set; }

        [Required]
        [StringLength(50)]
        public string Numara { get; set; }

        public bool Silinemez { get; set; }

        public bool TercihEdilen { get; set; }

        public int EnumVeriKayitTip { get; set; }

        public bool Silindi { get; set; }

        [StringLength(50)]
        public string DahiliNo { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public virtual Kisi Kisi { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }
    }
}
