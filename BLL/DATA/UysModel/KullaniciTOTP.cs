namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ytk.KullaniciTOTP")]
    public partial class KullaniciTOTP
    {
        public int ID { get; set; }

        public DateTime TarihKayit { get; set; }

        public int FKKullaniciID { get; set; }

        public Guid Anahtar { get; set; }

        public bool AktifMi { get; set; }

        public bool DogrulandiMi { get; set; }

        public virtual Kullanici Kullanici { get; set; }
    }
}
