namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.WebBolumTemelAlanIliski")]
    public partial class WebBolumTemelAlanIliski
    {
        [Key]
        public int InKod { get; set; }

        [StringLength(2)]
        public string FakKod { get; set; }

        [StringLength(2)]
        public string BolKod { get; set; }

        public int? TemelAlanKod { get; set; }

        public byte? OgretimSeviye { get; set; }

        [StringLength(15)]
        public string Tur { get; set; }

        public int? FKFakulteBirimID { get; set; }

        public int? FKBolumBirimID { get; set; }

        public int? FKProgramBirimID { get; set; }
    }
}
