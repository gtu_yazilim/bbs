namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("gen.YoksisUlkeler")]
    public partial class YoksisUlkeler
    {
        public int ID { get; set; }

        [StringLength(250)]
        public string AD { get; set; }

        public long? KOD { get; set; }
    }
}
