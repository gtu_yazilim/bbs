namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRTNMBilimDali")]
    public partial class OGRTNMBilimDali
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OGRTNMBilimDali()
        {
            OGREnstituKontenjan = new HashSet<OGREnstituKontenjan>();
            OGRKimlik = new HashSet<OGRKimlik>();
        }

        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? InKod { get; set; }

        [StringLength(500)]
        public string Ad { get; set; }

        [StringLength(500)]
        public string AdEng { get; set; }

        [StringLength(300)]
        public string KisaAd { get; set; }

        [StringLength(300)]
        public string KisaAdEng { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        [StringLength(500)]
        public string AdGerman { get; set; }

        [StringLength(300)]
        public string KisaAdGerman { get; set; }

        public int? YOKYL { get; set; }

        public int? YOKDR { get; set; }

        public int? YOKYL2O { get; set; }

        public int? YOKYLUZ { get; set; }

        public int? FKBolumPrBirimID { get; set; }

        public virtual Birimler Birimler { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituKontenjan> OGREnstituKontenjan { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRKimlik> OGRKimlik { get; set; }
    }
}
