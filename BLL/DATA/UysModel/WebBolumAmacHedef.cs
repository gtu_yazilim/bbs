namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.WebBolumAmacHedef")]
    public partial class WebBolumAmacHedef
    {
        [Key]
        [Column(Order = 0)]
        public int InKod { get; set; }

        [StringLength(2)]
        public string FakKod { get; set; }

        [StringLength(2)]
        public string BolKod { get; set; }

        public byte? FormDil { get; set; }

        public byte? OgretimSeviye { get; set; }

        [Column(TypeName = "text")]
        public string Amac { get; set; }

        [Column(TypeName = "text")]
        public string Hedef { get; set; }

        [Column(TypeName = "text")]
        public string AlDerece { get; set; }

        [Column(TypeName = "text")]
        public string KabKosul { get; set; }

        [Column(TypeName = "text")]
        public string OzelKabulKosul { get; set; }

        [Column(TypeName = "text")]
        public string OncekiOgretimTanima { get; set; }

        public byte? OgretimDurumu { get; set; }

        [Column(TypeName = "text")]
        public string MezKosul { get; set; }

        [Column(TypeName = "text")]
        public string SinavDegerKural { get; set; }

        [Column(TypeName = "text")]
        public string UstKadGecis { get; set; }

        [Column(TypeName = "text")]
        public string BolumBsk { get; set; }

        [Column(TypeName = "text")]
        public string AKTSKoord { get; set; }

        public byte? Dil { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short Yil1 { get; set; }

        [StringLength(20)]
        public string Ekleyen { get; set; }

        public DateTime? Tarih { get; set; }

        public byte? BolumPlanOnay { get; set; }

        public int? FKFakulteBirimID { get; set; }

        public int? FKBolumBirimID { get; set; }

        public int? FKProgramBirimID { get; set; }
    }
}
