namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("mkn.MekanFoto")]
    public partial class MekanFoto
    {
        public int ID { get; set; }

        [Column(TypeName = "image")]
        public byte[] foto { get; set; }

        public int? FKMekanID { get; set; }

        public virtual Mekan Mekan { get; set; }
    }
}
