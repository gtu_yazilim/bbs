namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.DersKategoriIliski")]
    public partial class DersKategoriIliski
    {
        public int ID { get; set; }

        public int? FKDersPlanAnaID { get; set; }

        public int? FKDersKategoriID { get; set; }

        public int? Oran { get; set; }

        public short? Yil { get; set; }

        [StringLength(30)]
        public string Ekleyen { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? Zaman { get; set; }

        public virtual DersKategoriTnm DersKategoriTnm { get; set; }

        public virtual OGRDersPlanAna OGRDersPlanAna { get; set; }
    }
}
