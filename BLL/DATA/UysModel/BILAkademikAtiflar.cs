namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.BILAkademikAtiflar")]
    public partial class BILAkademikAtiflar
    {
        public int ID { get; set; }

        public int? FKYayinID { get; set; }

        [StringLength(4)]
        public string PY { get; set; }

        public int? Sayi { get; set; }

        public int? YT1 { get; set; }
    }
}
