namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.Hareket")]
    public partial class Hareket
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Hareket()
        {
            KadroTakasi = new HashSet<KadroTakasi>();
        }

        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? IslemID { get; set; }

        public int FormNo { get; set; }

        public DateTime? HazirlanmaTarihi { get; set; }

        public int EvrakKayitNo { get; set; }

        public DateTime? EvrakKayitTarihi { get; set; }

        public DateTime? OnayTarihi { get; set; }

        public DateTime? YururlulukTarihi { get; set; }

        [Required]
        [StringLength(50)]
        public string Adi { get; set; }

        [Required]
        [StringLength(50)]
        public string Soyadi { get; set; }

        [Required]
        [StringLength(10)]
        public string SicilNo { get; set; }

        [StringLength(8)]
        public string EmekliSicilNo { get; set; }

        public double? AtanmaPuani { get; set; }

        public string Ilkokul { get; set; }

        public string Ortaokul { get; set; }

        public string Lise { get; set; }

        public string OnLisans { get; set; }

        public string Lisans { get; set; }

        public string YuksekLisans { get; set; }

        public string Doktora { get; set; }

        public string Docentlik { get; set; }

        public string Profesorluk { get; set; }

        public string SanattaYeterlilik { get; set; }

        public string TiptaUzmanlik { get; set; }

        [StringLength(50)]
        public string ESinifi { get; set; }

        [StringLength(50)]
        public string EUlke { get; set; }

        [StringLength(50)]
        public string EUniversite { get; set; }

        [StringLength(50)]
        public string EFakulte { get; set; }

        [StringLength(75)]
        public string EBolum { get; set; }

        [StringLength(75)]
        public string EAnaBilimDali { get; set; }

        [StringLength(50)]
        public string EKadroUnvani { get; set; }

        [StringLength(50)]
        public string EAkademikUnvan { get; set; }

        public int EKadroDerece { get; set; }

        public int EGorevAyligiDerece { get; set; }

        public int EGorevAyligiKademe { get; set; }

        public DateTime? EGorevAyligiTarihi { get; set; }

        public int EKazanilmisHakAyligiDerece { get; set; }

        public int EKazanilmisHakAyligiKademe { get; set; }

        public DateTime? EKazanilmisHakAyligiTarihi { get; set; }

        public int EEmekliAyligiDerece { get; set; }

        public int EEmekliAyligiKademe { get; set; }

        public DateTime? EEmekliAyligiTarihi { get; set; }

        public int EKidemYil { get; set; }

        public DateTime? EKidemTerfiTarihi { get; set; }

        public int EEkGosterge { get; set; }

        public int EEmekliEkGosterge { get; set; }

        public int? EUniversiteOdenegi { get; set; }

        public int? EMakamTazminati { get; set; }

        public int? EGorevTazminati { get; set; }

        public int? EIsGucluguZammi { get; set; }

        public int? ETeminatGuclukZammi { get; set; }

        public int? EMaliSorumlukZammi { get; set; }

        public int? EIsRiskiZammi { get; set; }

        public int? EOzelHizmetTazminati { get; set; }

        [StringLength(50)]
        public string YSinifi { get; set; }

        [StringLength(50)]
        public string YUlke { get; set; }

        [StringLength(50)]
        public string YUniversite { get; set; }

        [StringLength(50)]
        public string YFakulte { get; set; }

        [StringLength(75)]
        public string YBolum { get; set; }

        [StringLength(75)]
        public string YAnaBilimDali { get; set; }

        [StringLength(50)]
        public string YKadroUnvani { get; set; }

        [StringLength(50)]
        public string YAkademikUnvan { get; set; }

        public int YKadroDerece { get; set; }

        public int YGorevAyligiDerece { get; set; }

        public int YGorevAyligiKademe { get; set; }

        public DateTime? YGorevAyligiTarihi { get; set; }

        public int YKazanilmisHakAyligiDerece { get; set; }

        public int YKazanilmisHakAyligiKademe { get; set; }

        public DateTime? YKazanilmisHakAyligiTarihi { get; set; }

        public int YEmekliAyligiDerece { get; set; }

        public int YEmekliAyligiKademe { get; set; }

        public DateTime? YEmekliAyligiTarihi { get; set; }

        public int YKidemYil { get; set; }

        public DateTime? YKidemTerfiTarihi { get; set; }

        public int YEkGosterge { get; set; }

        public int YEmekliEkGosterge { get; set; }

        public int? YUniversiteOdenegi { get; set; }

        public int? YMakamTazminati { get; set; }

        public int? YGorevTazminati { get; set; }

        public int? YIsGucluguZammi { get; set; }

        public int? YTeminatGuclukZammi { get; set; }

        public int? YMaliSorumlukZammi { get; set; }

        public int? YIsRiskiZammi { get; set; }

        public int? YOzelHizmetTazminati { get; set; }

        public string Dayanagi { get; set; }

        public string Aciklama { get; set; }

        public string AyrilmaNedeni { get; set; }

        public string DagitimYeri { get; set; }

        [StringLength(50)]
        public string Ek { get; set; }

        [StringLength(50)]
        public string Sorumlu1Ad { get; set; }

        [StringLength(50)]
        public string Sorumlu1Soyad { get; set; }

        [StringLength(50)]
        public string Sorumlu1GorevAdi { get; set; }

        [StringLength(50)]
        public string Sorumlu2Ad { get; set; }

        [StringLength(50)]
        public string Sorumlu2Soyad { get; set; }

        [StringLength(50)]
        public string Sorumlu2GorevAdi { get; set; }

        [StringLength(50)]
        public string Sorumlu3Ad { get; set; }

        [StringLength(50)]
        public string Sorumlu3Soyad { get; set; }

        [StringLength(50)]
        public string Sorumlu3GorevAdi { get; set; }

        [StringLength(20)]
        public string ParafDaktilograf { get; set; }

        [StringLength(20)]
        public string ParafSef { get; set; }

        [StringLength(20)]
        public string ParafSubeMuduru { get; set; }

        public bool? ParafVekilSM { get; set; }

        [StringLength(20)]
        public string ParafDaireBaskani { get; set; }

        public bool? ParafVekilDB { get; set; }

        public bool? HizmetteGoster { get; set; }

        public int? KullaniciKod { get; set; }

        public DateTime? IslemTarihi { get; set; }

        public int ENUMKadroTipi { get; set; }

        public int FKPersonelBilgisiID { get; set; }

        public int ENUMHareketTipi { get; set; }

        public int? FKEKadroBilgisiID { get; set; }

        public int? FKEUlkeID { get; set; }

        public int? FKEUniversiteID { get; set; }

        public int? FKEFakulteID { get; set; }

        public int? FKEBolumID { get; set; }

        public int? FKEAnaBilimDaliID { get; set; }

        public int? FKEKadroUnvaniID { get; set; }

        public int? FKEAkademikUnvanID { get; set; }

        public int? FKYKadroBilgisiID { get; set; }

        public int? FKYUlkeID { get; set; }

        public int? FKYUniversiteID { get; set; }

        public int? FKYFakulteID { get; set; }

        public int? FKYBolumID { get; set; }

        public int? FKYAnaBilimDaliID { get; set; }

        public int? FKYKadroUnvaniID { get; set; }

        public int? FKYAkademikUnvanID { get; set; }

        public int? FKSorumlu1UnvanID { get; set; }

        public int? FKSorumlu1GorevID { get; set; }

        public int? FKSorumlu2UnvanID { get; set; }

        public int? FKSorumlu2GorevID { get; set; }

        public int? FKSorumlu3UnvanID { get; set; }

        public int? FKSorumlu3GorevID { get; set; }

        public int ENUMAskerlikDurumu { get; set; }

        public DateTime? TecilBitisTarihi { get; set; }

        public DateTime? AskerlikBaslamaTarihi { get; set; }

        public DateTime? AskerlikBitisTarihi { get; set; }

        public int? islem_kod { get; set; }

        public int? EYuksekOgretimTazminati { get; set; }

        public int? YYuksekOgretimTazminati { get; set; }

        public virtual Birimler Birimler { get; set; }

        public virtual Birimler Birimler1 { get; set; }

        public virtual Birimler Birimler2 { get; set; }

        public virtual Birimler Birimler3 { get; set; }

        public virtual Birimler Birimler4 { get; set; }

        public virtual Birimler Birimler5 { get; set; }

        public virtual Birimler Birimler6 { get; set; }

        public virtual Birimler Birimler7 { get; set; }

        public virtual TNMUnvan TNMUnvan { get; set; }

        public virtual KadroBilgisi KadroBilgisi { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }

        public virtual TNMUnvan TNMUnvan1 { get; set; }

        public virtual KadroBilgisi KadroBilgisi1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<KadroTakasi> KadroTakasi { get; set; }
    }
}
