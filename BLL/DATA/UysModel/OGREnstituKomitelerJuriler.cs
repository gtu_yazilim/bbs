namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGREnstituKomitelerJuriler")]
    public partial class OGREnstituKomitelerJuriler
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OGREnstituKomitelerJuriler()
        {
            OGREnstituKomiteJuriUyeleri = new HashSet<OGREnstituKomiteJuriUyeleri>();
            OGREnstituTezTizTosSinavi = new HashSet<OGREnstituTezTizTosSinavi>();
        }

        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public DateTime TarihBaslangic { get; set; }

        [StringLength(10)]
        public string YKNo { get; set; }

        public DateTime? TarihBitis { get; set; }

        public int EnumKomiteJuriTipi { get; set; }

        public int? FKOgrenciID { get; set; }

        public int? FKBirimID { get; set; }

        public bool GuncelMi { get; set; }

        [StringLength(250)]
        public string Aciklama { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public virtual Birimler Birimler { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituKomiteJuriUyeleri> OGREnstituKomiteJuriUyeleri { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituTezTizTosSinavi> OGREnstituTezTizTosSinavi { get; set; }
    }
}
