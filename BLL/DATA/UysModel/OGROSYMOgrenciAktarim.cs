namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGROSYMOgrenciAktarim")]
    public partial class OGROSYMOgrenciAktarim
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        public string Kullanici { get; set; }

        public string Makina { get; set; }

        [StringLength(11)]
        public string TC { get; set; }

        [StringLength(40)]
        public string Ad { get; set; }

        [StringLength(40)]
        public string Soyad { get; set; }

        [StringLength(40)]
        public string BabaAd { get; set; }

        [StringLength(40)]
        public string AnaAd { get; set; }

        [StringLength(2)]
        public string DogumGunu { get; set; }

        [StringLength(2)]
        public string DogumAy { get; set; }

        [StringLength(4)]
        public string DogumYil { get; set; }

        public int? Cinsiyet { get; set; }

        public int? Uyruk { get; set; }

        [StringLength(40)]
        public string DogumYeri { get; set; }

        [StringLength(2)]
        public string NufusIl { get; set; }

        [StringLength(4)]
        public string NufusIlce { get; set; }

        [StringLength(6)]
        public string OkulKodu { get; set; }

        [StringLength(5)]
        public string OkulTuru { get; set; }

        [StringLength(4)]
        public string OkulKolu { get; set; }

        [StringLength(100)]
        public string OkulKoluAd { get; set; }

        public int? OgrenimDurumu { get; set; }

        public int? MezuniyetYil { get; set; }

        public int? MeslekLisMezun { get; set; }

        [StringLength(15)]
        public string BasariPuan { get; set; }

        public int? YabanciDil { get; set; }

        [StringLength(150)]
        public string Adres1 { get; set; }

        [StringLength(50)]
        public string Adres2 { get; set; }

        [StringLength(100)]
        public string Semt_Ilce { get; set; }

        [StringLength(5)]
        public string PostaKodu { get; set; }

        [StringLength(50)]
        public string AdresIl { get; set; }

        [StringLength(4)]
        public string EvTelUlkeAlanKod { get; set; }

        [StringLength(3)]
        public string EvTelAlanKod { get; set; }

        [StringLength(7)]
        public string EvTelNumara { get; set; }

        [StringLength(4)]
        public string CepTelUlkeAlanKod { get; set; }

        [StringLength(3)]
        public string CepTelAlanKod { get; set; }

        [StringLength(7)]
        public string CepTelNumara { get; set; }

        public int? OkulBirincisi { get; set; }

        [StringLength(15)]
        public string YerlesKulPuan { get; set; }

        [StringLength(7)]
        public string YerlesKulSira { get; set; }

        [StringLength(20)]
        public string GirdigiBolumKod { get; set; }

        public int? GirdigiBolumTercihSira { get; set; }

        [StringLength(10)]
        public string YerlesPuanTuru { get; set; }

        [StringLength(15)]
        public string EkPuan { get; set; }

        [StringLength(14)]
        public string Kontenjan { get; set; }

        public int? OgretimYili { get; set; }

        public int? EnumOsymYerlesme { get; set; }

        [StringLength(50)]
        public string OnlisansAlani { get; set; }

        public bool? AktarildiMi { get; set; }

        [StringLength(15)]
        public string Numara { get; set; }

        [StringLength(2)]
        public string FakulteKod { get; set; }

        [StringLength(2)]
        public string BolumKod { get; set; }

        public int? EnumOgretimTur { get; set; }

        [StringLength(100)]
        public string Eposta { get; set; }

        public int? EngelDurumu { get; set; }

        [StringLength(500)]
        public string FotoURL { get; set; }

        [StringLength(15)]
        public string KullaniciAdi { get; set; }

        public int? FKBirimID { get; set; }

        public int? YerlesmeTuru { get; set; }

        public int? MebDurum { get; set; }

        public int? AsalDurum { get; set; }

        public int? ObsKontrol { get; set; }

        public int? OkuduguProgram { get; set; }

        public string AktarimLog { get; set; }

        public virtual Birimler Birimler { get; set; }
    }
}
