namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.TNMKurumBilgisi")]
    public partial class TNMKurumBilgisi
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        [StringLength(250)]
        public string KurumAd { get; set; }

        [StringLength(5)]
        public string PostaKodu { get; set; }

        public bool? Vakif { get; set; }

        public int? KullaniciKod { get; set; }

        public DateTime? IslemTarihi { get; set; }

        [StringLength(50)]
        public string KurumDomainAdi { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FKUniversiteID { get; set; }

        public int? FKKurumIlID { get; set; }

        public int? FKKurumIlceID { get; set; }

        public virtual Birimler Birimler { get; set; }
    }
}
