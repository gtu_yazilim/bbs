namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.WebDersSosyalYeterlilikIliski")]
    public partial class WebDersSosyalYeterlilikIliski
    {
        [Key]
        [Column(Order = 0, TypeName = "numeric")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public decimal InKod { get; set; }

        public decimal? AnaDersPlanID { get; set; }

        [StringLength(2)]
        public string FakKod { get; set; }

        [StringLength(2)]
        public string BolKod { get; set; }

        public int? YeterlilikID { get; set; }

        public byte? KatkiDuzeyi { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short Yil1 { get; set; }

        [StringLength(30)]
        public string Ekleyen { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? Zaman { get; set; }
    }
}
