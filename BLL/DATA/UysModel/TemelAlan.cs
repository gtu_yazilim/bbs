namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.TemelAlan")]
    public partial class TemelAlan
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TemelAlan()
        {
            DilTemelAlan = new HashSet<DilTemelAlan>();
            TemelAlanBolumIliski = new HashSet<TemelAlanBolumIliski>();
            TemelAlanYeterlilikKategoriIliski = new HashSet<TemelAlanYeterlilikKategoriIliski>();
        }

        public int ID { get; set; }

        [StringLength(3)]
        public string Kod { get; set; }

        public DateTime? Zaman { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DilTemelAlan> DilTemelAlan { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TemelAlanBolumIliski> TemelAlanBolumIliski { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TemelAlanYeterlilikKategoriIliski> TemelAlanYeterlilikKategoriIliski { get; set; }
    }
}
