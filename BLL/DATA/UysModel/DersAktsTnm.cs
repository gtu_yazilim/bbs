namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.DersAktsTnm")]
    public partial class DersAktsTnm
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DersAktsTnm()
        {
            DersAktsYukIliski = new HashSet<DersAktsYukIliski>();
            DilDersAktsTnm = new HashSet<DilDersAktsTnm>();
        }

        public int ID { get; set; }

        public int? AktsNo { get; set; }

        public short Yil { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DersAktsYukIliski> DersAktsYukIliski { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DilDersAktsTnm> DilDersAktsTnm { get; set; }
    }
}
