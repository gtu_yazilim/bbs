namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.AtanmaSureleri")]
    public partial class AtanmaSureleri
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AtanmaSureleri()
        {
            AtanmaSureleriDetay = new HashSet<AtanmaSureleriDetay>();
        }

        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        public string Kullanici { get; set; }

        public string Makina { get; set; }

        public DateTime? HazirlanmaTarihi { get; set; }

        public DateTime? BaslangicTarihi { get; set; }

        public DateTime? BitisTarihi { get; set; }

        public DateTime? OnayTarihi { get; set; }

        [StringLength(50)]
        public string EvrakNo { get; set; }

        public DateTime? EvrakTarihi { get; set; }

        [Required]
        [StringLength(250)]
        public string Aciklama { get; set; }

        public int ENUMDonem { get; set; }

        public int? ENUMSurec { get; set; }

        public int FKFakulteID { get; set; }

        public virtual Birimler Birimler { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AtanmaSureleriDetay> AtanmaSureleriDetay { get; set; }
    }
}
