namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("obs.OBSOnKayitAyar")]
    public partial class OBSOnKayitAyar
    {
        public int ID { get; set; }

        public int Yil { get; set; }

        public DateTime BaslangicTarihi { get; set; }

        public DateTime BitisTarihi { get; set; }

        public int KisiSayisi { get; set; }

        public int FakulteID { get; set; }
    }
}
