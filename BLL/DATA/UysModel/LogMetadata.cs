namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("LogMetadata")]
    public partial class LogMetadata
    {
        public long Id { get; set; }

        public long AuditLogId { get; set; }

        public string Key { get; set; }

        public string Value { get; set; }

        public virtual AuditLogs AuditLogs { get; set; }
    }
}
