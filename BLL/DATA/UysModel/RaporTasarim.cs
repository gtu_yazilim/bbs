namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("gen.RaporTasarim")]
    public partial class RaporTasarim
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public RaporTasarim()
        {
            OGRDiplomaFormat = new HashSet<OGRDiplomaFormat>();
            OGRDiplomaFormat1 = new HashSet<OGRDiplomaFormat>();
        }

        public int ID { get; set; }

        public DateTime TarihKayit { get; set; }

        [Required]
        public string RaporXML { get; set; }

        [Required]
        [StringLength(50)]
        public string Kullanici { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRDiplomaFormat> OGRDiplomaFormat { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRDiplomaFormat> OGRDiplomaFormat1 { get; set; }
    }
}
