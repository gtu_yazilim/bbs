namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("obs.OBSYazilmaAyar")]
    public partial class OBSYazilmaAyar
    {
        public int ID { get; set; }

        public DateTime? Tarih { get; set; }

        public int Yil { get; set; }

        public int Donem { get; set; }

        public DateTime? YazilmaBaslangic { get; set; }

        public DateTime? YazilmaBitis { get; set; }

        public DateTime? YazilmaBaslangic2 { get; set; }

        public DateTime? YazilmaBitis2 { get; set; }

        public int FKBirimID { get; set; }

        public DateTime? YazilmaSimulasyonBaslangic { get; set; }

        public DateTime? YazilmaSimulasyonBitis { get; set; }

        public virtual Birimler Birimler { get; set; }
    }
}
