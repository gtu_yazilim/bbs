namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ytk.Modul")]
    public partial class Modul
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Modul()
        {
            Modul1 = new HashSet<Modul>();
            ModulIPGuvenlik = new HashSet<ModulIPGuvenlik>();
            ModulRol = new HashSet<ModulRol>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(500)]
        public string Ad { get; set; }

        public int FKProjeID { get; set; }

        public int Sira { get; set; }

        public bool Gorunurluk { get; set; }

        public bool AktifMi { get; set; }

        [Required]
        [StringLength(500)]
        public string Url { get; set; }

        [StringLength(150)]
        public string GorunenAd { get; set; }

        public int? FKUstModulID { get; set; }

        [StringLength(50)]
        public string MenuIcon { get; set; }

        [StringLength(50)]
        public string MenuTile { get; set; }

        public int EnumModulTipi { get; set; }

        [StringLength(250)]
        public string DilAnahtar { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Modul> Modul1 { get; set; }

        public virtual Modul Modul2 { get; set; }

        public virtual Proje Proje { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ModulIPGuvenlik> ModulIPGuvenlik { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ModulRol> ModulRol { get; set; }
    }
}
