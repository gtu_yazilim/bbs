namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGROgrenciYazilma")]
    public partial class OGROgrenciYazilma
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OGROgrenciYazilma()
        {
            OGROgrenciDersPlan = new HashSet<OGROgrenciDersPlan>();
        }

        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(500)]
        public string Kullanici { get; set; }

        [StringLength(500)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public bool? WebYazilma { get; set; }

        public int? InKod { get; set; }

        public int? Yariyil { get; set; }

        public int? Yil { get; set; }

        public int? EnumDonem { get; set; }

        public int? FKOgrenciID { get; set; }

        public int? FKDersPlanAnaID { get; set; }

        public int? FKDersPlanID { get; set; }

        public int? FKDersGrupID { get; set; }

        public int? FKOgrenciNotID { get; set; }

        public bool? Canceled { get; set; }

        public int? FKRedirectFromDersGrupID { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public bool? ButunlemeMi { get; set; }

        public int? EnumIptalSebebi { get; set; }

        public int? FKDanismanOnayID { get; set; }

        public int? FKOgrenciDersPlanID { get; set; }

        public bool Iptal { get; set; }

        public virtual OGRDersGrup OGRDersGrup { get; set; }

        public virtual OGRDersGrup OGRDersGrup1 { get; set; }

        public virtual OGRDersPlan OGRDersPlan { get; set; }

        public virtual OGRDersPlanAna OGRDersPlanAna { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGROgrenciDersPlan> OGROgrenciDersPlan { get; set; }

        public virtual OGROgrenciDersPlan OGROgrenciDersPlan1 { get; set; }

        public virtual OGROgrenciNot OGROgrenciNot { get; set; }

        public virtual OGRYazilmaDanismanOnay OGRYazilmaDanismanOnay { get; set; }
    }
}
