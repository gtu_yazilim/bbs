namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("mkn.Mekan")]
    public partial class Mekan
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Mekan()
        {
            MekanV2 = new HashSet<MekanV2>();
            BarinmaKisiler = new HashSet<BarinmaKisiler>();
            OGRDersProgramiv2 = new HashSet<OGRDersProgramiv2>();
            OGREnstituSeminer = new HashSet<OGREnstituSeminer>();
            OGREnstituTIZ = new HashSet<OGREnstituTIZ>();
            OGREnstituTOS = new HashSet<OGREnstituTOS>();
            OGREnstituYeterlik = new HashSet<OGREnstituYeterlik>();
            MekanCihazlar = new HashSet<MekanCihazlar>();
            MekanFoto = new HashSet<MekanFoto>();
            MekanKisiler = new HashSet<MekanKisiler>();
        }

        public int ID { get; set; }

        [StringLength(200)]
        public string MekanKod { get; set; }

        [StringLength(50)]
        public string MekanAD { get; set; }

        [StringLength(2)]
        public string FakKod { get; set; }

        [StringLength(2)]
        public string BolKod { get; set; }

        public bool? Klima { get; set; }

        public bool? Datashow { get; set; }

        public byte? KacinciKat { get; set; }

        public int? En { get; set; }

        public int? Boy { get; set; }

        public int? Alan { get; set; }

        public int? toplamSayi { get; set; }

        public int? sinavSayi { get; set; }

        public int? bilgisayar { get; set; }

        [StringLength(100)]
        public string Aciklama { get; set; }

        [StringLength(20)]
        public string kullanici { get; set; }

        public DateTime? islem_tarihi { get; set; }

        [StringLength(100)]
        public string Mekan1 { get; set; }

        [StringLength(200)]
        public string MekanEmail { get; set; }

        public int? FKBinaID { get; set; }

        public int? FKMekanTipID { get; set; }

        [Column(TypeName = "image")]
        public byte[] Foto { get; set; }

        [StringLength(20)]
        public string Kod { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public DateTime? Zaman { get; set; }

        public int? FKBolumPrBirimID { get; set; }

        public bool? CakismaOlabilir { get; set; }

        public virtual Birimler Birimler { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MekanV2> MekanV2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BarinmaKisiler> BarinmaKisiler { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRDersProgramiv2> OGRDersProgramiv2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituSeminer> OGREnstituSeminer { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituTIZ> OGREnstituTIZ { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituTOS> OGREnstituTOS { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituYeterlik> OGREnstituYeterlik { get; set; }

        public virtual MekanBinalar1 MekanBinalar1 { get; set; }

        public virtual TNMMekanTip TNMMekanTip { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MekanCihazlar> MekanCihazlar { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MekanFoto> MekanFoto { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MekanKisiler> MekanKisiler { get; set; }
    }
}
