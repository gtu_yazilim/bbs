namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ytk.LoginYetkiliIP")]
    public partial class LoginYetkiliIP
    {
        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string IP { get; set; }

        public DateTime Tarih { get; set; }

        public int EkleyenKullaniciID { get; set; }

        public bool AktifMi { get; set; }

        public bool EngelliMi { get; set; }
    }
}
