namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.WebBolumKategoriTnm")]
    public partial class WebBolumKategoriTnm
    {
        [Key]
        [Column(TypeName = "numeric")]
        public decimal InKod { get; set; }

        public byte? FormDil { get; set; }

        public int? AnaKategoriNo { get; set; }

        public int? AltKategoriNo { get; set; }

        [StringLength(100)]
        public string AnaKategoriAd { get; set; }

        [StringLength(150)]
        public string AltKategoriAd { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? Tarih { get; set; }
    }
}
