namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.WebZorunluTipTnm")]
    public partial class WebZorunluTipTnm
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int InKod { get; set; }

        public byte FormDil { get; set; }

        public byte ZorunluTipKod { get; set; }

        [Required]
        [StringLength(50)]
        public string ZorunluTipAd { get; set; }

        [StringLength(7)]
        public string RenkKod { get; set; }

        public DateTime Zaman { get; set; }

        [StringLength(20)]
        public string ZorunTipAdUzun { get; set; }
    }
}
