namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.TNMKadroUnvani")]
    public partial class TNMKadroUnvani
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TNMKadroUnvani()
        {
            AtanmaSureleriDetay = new HashSet<AtanmaSureleriDetay>();
            EgitimBilgisi = new HashSet<EgitimBilgisi>();
            Izin = new HashSet<Izin>();
            Izin1 = new HashSet<Izin>();
            Izin2 = new HashSet<Izin>();
            IzinSakliToplam = new HashSet<IzinSakliToplam>();
            IzinYOKDetay = new HashSet<IzinYOKDetay>();
            IzinYOKDetay1 = new HashSet<IzinYOKDetay>();
            KadroBilgisi = new HashSet<KadroBilgisi>();
            PersonelBilgisi = new HashSet<PersonelBilgisi>();
            TNMEkGosterge = new HashSet<TNMEkGosterge>();
            TNMGorevTazminati = new HashSet<TNMGorevTazminati>();
            TNMIdariOdenekler = new HashSet<TNMIdariOdenekler>();
            TNMKanunMaddeNo = new HashSet<TNMKanunMaddeNo>();
            TNMMakamTazminati = new HashSet<TNMMakamTazminati>();
            TNMUniversiteOdenegi = new HashSet<TNMUniversiteOdenegi>();
        }

        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        [Required]
        [StringLength(10)]
        public string UnvanKodu { get; set; }

        [StringLength(50)]
        public string UnvanKisaltma { get; set; }

        [Required]
        [StringLength(50)]
        public string UnvanAd { get; set; }

        [StringLength(50)]
        public string UnvanEngAd { get; set; }

        [StringLength(50)]
        public string UnvanEngKisaltma { get; set; }

        [StringLength(50)]
        public string UnvanSinifi { get; set; }

        public int UnvanAkademikMi { get; set; }

        public int? KullaniciKod { get; set; }

        public DateTime? IslemTarihi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AtanmaSureleriDetay> AtanmaSureleriDetay { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EgitimBilgisi> EgitimBilgisi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Izin> Izin { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Izin> Izin1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Izin> Izin2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IzinSakliToplam> IzinSakliToplam { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IzinYOKDetay> IzinYOKDetay { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IzinYOKDetay> IzinYOKDetay1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<KadroBilgisi> KadroBilgisi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PersonelBilgisi> PersonelBilgisi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TNMEkGosterge> TNMEkGosterge { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TNMGorevTazminati> TNMGorevTazminati { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TNMIdariOdenekler> TNMIdariOdenekler { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TNMKanunMaddeNo> TNMKanunMaddeNo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TNMMakamTazminati> TNMMakamTazminati { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TNMUniversiteOdenegi> TNMUniversiteOdenegi { get; set; }
    }
}
