namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.SGKHizmetNufus")]
    public partial class SGKHizmetNufus
    {
        public int ID { get; set; }

        [Required]
        [StringLength(11)]
        public string Tckn { get; set; }

        [StringLength(200)]
        public string ad { get; set; }

        [StringLength(200)]
        public string soyad { get; set; }

        [StringLength(200)]
        public string ilkSoyAd { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? dogumTarihi { get; set; }

        [StringLength(1)]
        public string cinsiyet { get; set; }

        public int? emekliSicilNo { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? memuriyetBaslamaTarihi { get; set; }

        [StringLength(100)]
        public string kurumSicil { get; set; }

        [StringLength(1)]
        public string maluliyetKod { get; set; }

        public int? sebep { get; set; }

        [StringLength(1)]
        public string durum { get; set; }

        public int? yetkiSeviyesi { get; set; }

        public string aciklama { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? kurumaBaslamaTarihi { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? gorevTarihi6495 { get; set; }

        public int? emekliSicil6495 { get; set; }

        [StringLength(250)]
        public string SGKDurumAciklama { get; set; }

        public int? ENUMSGKDurum { get; set; }

        public int? FKPersonelBilgisiID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(100)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }
    }
}
