namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.SGKHizmetAskerlik")]
    public partial class SGKHizmetAskerlik
    {
        public int ID { get; set; }

        public long? kayitNo { get; set; }

        [StringLength(11)]
        public string Tckn { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? Zaman { get; set; }

        public int? askerlikNevi { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? baslamaTarihi { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? bitisTarihi { get; set; }

        public int? sayilmayanGunSayisi { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? subayOkuluGirisTarihi { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? astegmenNaspTarihi { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? tegmenNaspTarihi { get; set; }

        [StringLength(50)]
        public string sinifOkuluSicil { get; set; }

        public string muafiyetNedeni { get; set; }

        [StringLength(100)]
        public string yedekSubayErOgrenimYer { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? subayliktanErligeGecisTarihi { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? kitaBaslamaTarihi { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? kitaBitisTarihi { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? kurumOnayTarihi { get; set; }

        [StringLength(200)]
        public string gorevYeri { get; set; }

        [StringLength(250)]
        public string SGKDurumAciklama { get; set; }

        public int? ENUMSGKDurum { get; set; }

        public int? FKPersonelBilgisiID { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? tecilBitisTarihi { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }
    }
}
