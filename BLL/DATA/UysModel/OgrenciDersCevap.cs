namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("anket.OgrenciDersCevap")]
    public partial class OgrenciDersCevap
    {
        public int ID { get; set; }

        public int FKOgrenciID { get; set; }

        public int SoruID { get; set; }

        public int Puan { get; set; }

        public int FKDersGrupID { get; set; }

        public DateTime Tarih { get; set; }

        public int FKPersonelID { get; set; }

        public virtual OGRDersGrup OGRDersGrup { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }
    }
}
