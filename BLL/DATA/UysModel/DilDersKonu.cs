namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.DilDersKonu")]
    public partial class DilDersKonu
    {
        public int ID { get; set; }

        public int? FKDersKonuID { get; set; }

        [StringLength(255)]
        public string Icerik { get; set; }

        [StringLength(255)]
        public string OnHazirlikIcerik { get; set; }

        public byte? EnumDilID { get; set; }

        [StringLength(50)]
        public string Ekleyen { get; set; }

        public DateTime? Zaman { get; set; }

        public virtual DersKonu DersKonu { get; set; }
    }
}
