namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.TNMKullanicilar")]
    public partial class TNMKullanicilar
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TNMKullanicilar()
        {
            YetkiKullaniciMenu = new HashSet<YetkiKullaniciMenu>();
            YetkiKullaniciRol = new HashSet<YetkiKullaniciRol>();
        }

        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int KullaniciKod { get; set; }

        [Required]
        [StringLength(20)]
        public string KullaniciAd { get; set; }

        [Required]
        [StringLength(50)]
        public string AdSoyad { get; set; }

        public bool Durum { get; set; }

        public int? FKGrupID { get; set; }

        public int? FKPersonelBilgisiID { get; set; }

        public int? FKFakulteID { get; set; }

        [StringLength(32)]
        public string Sifre { get; set; }

        public virtual Birimler Birimler { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }

        public virtual TNMYetkiGrup TNMYetkiGrup { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<YetkiKullaniciMenu> YetkiKullaniciMenu { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<YetkiKullaniciRol> YetkiKullaniciRol { get; set; }
    }
}
