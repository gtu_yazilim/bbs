namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.LojmanBilgisi")]
    public partial class LojmanBilgisi
    {
        public int ID { get; set; }

        public DateTime Zaman { get; set; }

        [Required]
        [StringLength(50)]
        public string Kullanici { get; set; }

        [Required]
        [StringLength(50)]
        public string Makina { get; set; }

        public DateTime? GirisTarihi { get; set; }

        public DateTime? AyrilisTarihi { get; set; }

        [StringLength(100)]
        public string Adres { get; set; }

        [StringLength(250)]
        public string Aciklama { get; set; }

        public int FKPersonelBilgisiID { get; set; }

        public int FKLojmanID { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }

        public virtual TNMLojman TNMLojman { get; set; }
    }
}
