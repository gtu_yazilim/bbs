namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("randevu.RandevuAyar")]
    public partial class RandevuAyar
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public RandevuAyar()
        {
            RandevuKayit = new HashSet<RandevuKayit>();
        }

        public int ID { get; set; }

        public int FKRandevuOturumID { get; set; }

        public DateTime TarihBaslangic { get; set; }

        public DateTime TarihBitis { get; set; }

        public int AralikDakika { get; set; }

        public int AralikAdet { get; set; }

        public virtual RandevuOturum RandevuOturum { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RandevuKayit> RandevuKayit { get; set; }
    }
}
