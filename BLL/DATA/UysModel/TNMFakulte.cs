namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.TNMFakulte")]
    public partial class TNMFakulte
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TNMFakulte()
        {
            IzinSakliDetay = new HashSet<IzinSakliDetay>();
            IzinSakliDetay1 = new HashSet<IzinSakliDetay>();
            IzinSakliToplam = new HashSet<IzinSakliToplam>();
            IzinSakliToplam1 = new HashSet<IzinSakliToplam>();
            KaraListe = new HashSet<KaraListe>();
            TNMAnaBilimDali = new HashSet<TNMAnaBilimDali>();
            TNMBolum = new HashSet<TNMBolum>();
        }

        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? UlkeKod { get; set; }

        public int? UniversiteKod { get; set; }

        public int? FakulteKod { get; set; }

        [Required]
        [StringLength(100)]
        public string FakulteAd { get; set; }

        public bool AkademikGorsun { get; set; }

        public int FKBirimTuru { get; set; }

        [StringLength(20)]
        public string ACYokEvrakNo { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? ACYokEvrakTarihi { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? ACYokKurulTarihi { get; set; }

        [StringLength(20)]
        public string ACResmiGazeteSayisi { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? ACResmiGazeteTarihi { get; set; }

        public bool ACResmiGazeteMukerrerMi { get; set; }

        public bool Durum { get; set; }

        public int FKUlkeID { get; set; }

        public int FKUniversiteID { get; set; }

        public int? KullaniciKod { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? IslemTarihi { get; set; }

        public int? FKYoksisID { get; set; }

        public int? FKBirimID { get; set; }

        public virtual TNMUlke TNMUlke { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IzinSakliDetay> IzinSakliDetay { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IzinSakliDetay> IzinSakliDetay1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IzinSakliToplam> IzinSakliToplam { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IzinSakliToplam> IzinSakliToplam1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<KaraListe> KaraListe { get; set; }

        public virtual SBTBirimTuru SBTBirimTuru { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TNMAnaBilimDali> TNMAnaBilimDali { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TNMBolum> TNMBolum { get; set; }

        public virtual TNMUniversite TNMUniversite { get; set; }
    }
}
