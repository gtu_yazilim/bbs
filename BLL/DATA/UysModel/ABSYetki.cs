namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.ABSYetki")]
    public partial class ABSYetki
    {
        public int ID { get; set; }

        public int? FKYetkiliPersonelID { get; set; }

        public int? FKYetkiliRolID { get; set; }

        public int FKYetliliModulID { get; set; }

        public bool IzinVer { get; set; }

        public DateTime? EklenmeTarihi { get; set; }

        [StringLength(50)]
        public string EkleyenKullanici { get; set; }

        [StringLength(200)]
        public string Aciklama { get; set; }

        [StringLength(50)]
        public string EkleyenIPNumarasi { get; set; }

        public virtual ABSYetkiliKullanici ABSYetkiliKullanici { get; set; }

        public virtual ABSYetkiModul ABSYetkiModul { get; set; }

        public virtual ABSYetkliRolleri ABSYetkliRolleri { get; set; }
    }
}
