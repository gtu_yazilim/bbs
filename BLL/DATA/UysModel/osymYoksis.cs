namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("gen.osymYoksis")]
    public partial class osymYoksis
    {
        [Key]
        public double program_kodu { get; set; }

        public double? universite_kodu { get; set; }

        [StringLength(255)]
        public string universite_adi { get; set; }

        [StringLength(255)]
        public string fakulte_adi { get; set; }

        [StringLength(255)]
        public string yuksekokul_adi { get; set; }

        [StringLength(255)]
        public string program_adi { get; set; }

        [StringLength(255)]
        public string tabloadi { get; set; }

        public double? ogrenim_suresi { get; set; }

        public double? yoksis_birim_id { get; set; }
    }
}
