namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRDersPlanEtiket")]
    public partial class OGRDersPlanEtiket
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OGRDersPlanEtiket()
        {
            OGROgrenciDersPlanAlis = new HashSet<OGROgrenciDersPlanAlis>();
        }

        public int ID { get; set; }
        public int FKDersEtiketID { get; set; }
        public int FKDersPlanID { get; set; }
        public DateTime TarihBaslangic { get; set; }
        public DateTime TarihBitis { get; set; }
        public string Kullanici { get; set; }


        public virtual OGRDersPlan OGRDersPlan { get; set; }

        public virtual OGRTNMDersEtiket OGRTNMDersEtiket { get; set; }

        public ICollection<OGROgrenciDersPlanAlis> OGROgrenciDersPlanAlis { get; set; }
    }
}
