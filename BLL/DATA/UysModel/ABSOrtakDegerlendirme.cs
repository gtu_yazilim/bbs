namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.ABSOrtakDegerlendirme")]
    public partial class ABSOrtakDegerlendirme
    {
        public ABSOrtakDegerlendirme()
        {
            ABSOrtakDegerlendirmeGrup = new HashSet<ABSOrtakDegerlendirmeGrup>();
        }
        [Key]
        [Column(Order = 0)]
        public int ID { get; set; }
        
        [Column(Order = 1)]
        public DateTime TarihKayit { get; set; }
        
        [Column(Order = 2)]
        [StringLength(50)]
        public string Kullanici { get; set; }
        
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FKDersPlanAnaID { get; set; }
        
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FKPersonelID { get; set; }

        public int? Yil { get; set; }

        public int? Donem { get; set; }
        
        [Column(Order = 5)]
        public bool Silindi { get; set; }

        public virtual OGRDersPlanAna OGRDersPlanAna { get; set; }
        public virtual PersonelBilgisi PersonelBilgisi { get; set; }
        public virtual ICollection<ABSOrtakDegerlendirmeGrup> ABSOrtakDegerlendirmeGrup { get; set; }

    }
}
