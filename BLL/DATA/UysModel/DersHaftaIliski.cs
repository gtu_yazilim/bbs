namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.DersHaftaIliski")]
    public partial class DersHaftaIliski
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DersHaftaIliski()
        {
            DersKonu = new HashSet<DersKonu>();
            OGRDersProgramiv2 = new HashSet<OGRDersProgramiv2>();
        }

        public int ID { get; set; }

        public int? FKDersPlanAnaID { get; set; }

        public int? FKDersBilgiID { get; set; }

        public byte? HaftaNo { get; set; }

        [StringLength(30)]
        public string Ekleyen { get; set; }

        public DateTime? Zaman { get; set; }

        public virtual OGRDersPlanAna OGRDersPlanAna { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DersKonu> DersKonu { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRDersProgramiv2> OGRDersProgramiv2 { get; set; }
    }
}
