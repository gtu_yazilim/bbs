namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGREnstituTOS")]
    public partial class OGREnstituTOS
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? InKod { get; set; }

        public DateTime? TarihBaslangic { get; set; }

        [StringLength(10)]
        public string YKNo { get; set; }

        [StringLength(10)]
        public string DanismanSicilNo { get; set; }

        public DateTime? TarihBitis { get; set; }

        public int? EnumEnstituYeterlikDurum { get; set; }

        public int? FKDanismanID { get; set; }

        public int? FKOgrenciID { get; set; }

        public int? FKmekanID { get; set; }

        public TimeSpan? Saat { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public virtual Mekan Mekan { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }
    }
}
