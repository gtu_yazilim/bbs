namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("mkn.TNMMekanIslevler")]
    public partial class TNMMekanIslevler
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TNMMekanIslevler()
        {
            MekanBinalar1 = new HashSet<MekanBinalar1>();
        }

        public int ID { get; set; }

        [StringLength(50)]
        public string islev { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MekanBinalar1> MekanBinalar1 { get; set; }
    }
}
