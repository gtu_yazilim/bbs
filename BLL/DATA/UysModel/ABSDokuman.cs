namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.ABSDokuman")]
    public partial class ABSDokuman
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ABSDokuman()
        {
            ABSDokumanGrup = new HashSet<ABSDokumanGrup>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(100)]
        public string DosyaAdi { get; set; }

        [Required]
        [StringLength(250)]
        public string DosyaKey { get; set; }

        [StringLength(250)]
        public string Aciklama { get; set; }

        [Required]
        [StringLength(10)]
        public string DosyaTuru { get; set; }

        public double? DosyaBoyutu { get; set; }

        public int EnumIcerikTipi { get; set; }

        public int Yil { get; set; }

        public int EnumDonem { get; set; }

        public int? Hafta { get; set; }

        public bool DigerHocalarGorebilir { get; set; }

        public bool EBSGosterilsin { get; set; }

        public bool? TumGruplarimaEkle { get; set; }

        public bool? TumGruplaraEkle { get; set; }

        public int FKDersPlanAnaID { get; set; }

        public int FKPersonelID { get; set; }

        [Required]
        [StringLength(30)]
        public string SGKullanici { get; set; }

        [Required]
        [StringLength(20)]
        public string SGIP { get; set; }

        public DateTime SGTarih { get; set; }

        public bool Silindi { get; set; }

        public virtual OGRDersPlanAna OGRDersPlanAna { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSDokumanGrup> ABSDokumanGrup { get; set; }
    }
}
