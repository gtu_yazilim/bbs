namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.ABSCevapAnahtari")]
    public partial class ABSCevapAnahtari
    {
        public int ID { get; set; }

        public int FKSinavID { get; set; }

        [Required]
        [StringLength(1)]
        public string Grup { get; set; }

        [StringLength(500)]
        public string CevapAnahtari { get; set; }
    }
}
