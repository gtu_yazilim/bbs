namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("obs.FormasyonBasvuru")]
    public partial class FormasyonBasvuru
    {
        public int ID { get; set; }

        public int FKOgrenciID { get; set; }

        [Required]
        [StringLength(50)]
        public string Ad { get; set; }

        [Required]
        [StringLength(50)]
        public string Soyad { get; set; }

        [Required]
        [StringLength(50)]
        public string Numara { get; set; }

        [Required]
        [StringLength(50)]
        public string Sinif { get; set; }

        [Required]
        [StringLength(50)]
        public string FakulteAd { get; set; }

        [Required]
        [StringLength(50)]
        public string BolumAd { get; set; }

        public int FakulteID { get; set; }

        public int BolumID { get; set; }

        public int ProgramID { get; set; }

        public DateTime? BasvuruTarihi { get; set; }

        public DateTime? BasvuruIptalTarihi { get; set; }

        public bool Durum { get; set; }

        public double? GenelAgirlikliNotOrtalamasi { get; set; }

        public double? TranskriptGenelAgirlikliNotOrtalamasi { get; set; }

        public double? TrasnkriptBirOncekiYariyilOrtalamasi { get; set; }

        public int? OgretimTuru { get; set; }

        [StringLength(50)]
        public string OgretimTuruAdi { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
