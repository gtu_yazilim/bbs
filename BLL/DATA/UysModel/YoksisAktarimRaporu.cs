namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.YoksisAktarimRaporu")]
    public partial class YoksisAktarimRaporu
    {
        public int ID { get; set; }

        public DateTime BaslamaZamani { get; set; }

        public DateTime? BitisZamani { get; set; }

        public int AktarilacakOgrenciSayisi { get; set; }

        public int? BasariliAktarim { get; set; }

        public int? BasarisizAktarim { get; set; }

        [Required]
        [StringLength(500)]
        public string Aciklama { get; set; }
    }
}
