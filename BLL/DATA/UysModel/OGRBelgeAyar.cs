namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRBelgeAyar")]
    public partial class OGRBelgeAyar
    {
        public int ID { get; set; }

        public DateTime TarihKayit { get; set; }

        [Required]
        [StringLength(50)]
        public string Kullanici { get; set; }

        [Required]
        [StringLength(50)]
        public string IP { get; set; }

        public int EnumBelgeTuru { get; set; }

        [Required]
        [StringLength(50)]
        public string SayiOnEk { get; set; }

        public int FKBirimID { get; set; }

        public string ImzaUrl { get; set; }

        public int? FKKisiID { get; set; }

        [StringLength(500)]
        public string BelgeBaslik { get; set; }

        [StringLength(250)]
        public string YetkiliUnvan { get; set; }

        [StringLength(500)]
        public string YetkiliAd { get; set; }

        public int EnumDil { get; set; }

        public virtual Kisi Kisi { get; set; }
    }
}
