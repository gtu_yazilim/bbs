namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRGeribildirim")]
    public partial class OGRGeribildirim
    {
        public int ID { get; set; }

        public DateTime Zaman { get; set; }

        [StringLength(100)]
        public string Kullanici { get; set; }

        [StringLength(100)]
        public string Makina { get; set; }

        [StringLength(500)]
        public string Ekran { get; set; }

        [StringLength(500)]
        public string Sorun { get; set; }

        [StringLength(500)]
        public string OlmasiGereken { get; set; }

        [Column("Ek Aciklama")]
        [StringLength(500)]
        public string Ek_Aciklama { get; set; }

        public int? Durum { get; set; }
    }
}
