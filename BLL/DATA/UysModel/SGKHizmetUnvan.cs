namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.SGKHizmetUnvan")]
    public partial class SGKHizmetUnvan
    {
        public int ID { get; set; }

        public int? kayitNo { get; set; }

        [StringLength(11)]
        public string Tckn { get; set; }

        public int? unvanKod { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? unvanTarihi { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? unvanBitisTarihi { get; set; }

        [StringLength(4)]
        public string hizmetSinifi { get; set; }

        [StringLength(1)]
        public string asilVekil { get; set; }

        [StringLength(1)]
        public string atamaSekli { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? kurumOnayTarihi { get; set; }

        public decimal? fhzOrani { get; set; }

        [StringLength(250)]
        public string SGKDurumAciklama { get; set; }

        public int? ENUMSGKDurum { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(100)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public int? FKPersonelBilgisiID { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }
    }
}
