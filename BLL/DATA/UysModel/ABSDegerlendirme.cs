namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.ABSDegerlendirme")]
    public partial class ABSDegerlendirme
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ABSDegerlendirme()
        {
            ABSBasariNot = new HashSet<ABSBasariNot>();
            ABSDegerlendirmeDosya = new HashSet<ABSDegerlendirmeDosya>();
            ABSOgrenciNot = new HashSet<ABSOgrenciNot>();
            ABSYayinlananBasariNotlar = new HashSet<ABSYayinlananBasariNotlar>();
            OGRBarkodOkuma = new HashSet<OGRBarkodOkuma>();
        }

        public int ID { get; set; }

        public int? FKDersGrupID { get; set; }

        public double? StandartSapma { get; set; }

        public double? Ortalama { get; set; }

        public double? EnBuyukNot { get; set; }

        public DateTime? Tarih { get; set; }

        public int? FKPersonelID { get; set; }

        [StringLength(10)]
        public string SicilNo { get; set; }

        public int? EnumSonHal { get; set; }

        public DateTime? Zaman { get; set; }

        public string Kullanici { get; set; }

        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        [StringLength(50)]
        public string BarkodNo { get; set; }

        public Guid? OrtakDegerlendirmeGUID { get; set; }

        public double? UstDeger { get; set; }

        public double? BagilOrtalama { get; set; }

        public int? Yil { get; set; }

        public int? EnumDonem { get; set; }

        public DateTime? AktarimTarihi { get; set; }

        public bool ButunlemeMi { get; set; }

        public bool SilindiMi { get; set; }

        public int? EBYSDocTreeID { get; set; }

        [StringLength(200)]
        public string DosyaKey { get; set; }

        public int? FKDersPlanAnaID { get; set; }

        public DateTime? TarihSonHal { get; set; }

        public double? EnKucukNot { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSBasariNot> ABSBasariNot { get; set; }

        public virtual OGRDersGrup OGRDersGrup { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSDegerlendirmeDosya> ABSDegerlendirmeDosya { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSOgrenciNot> ABSOgrenciNot { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSYayinlananBasariNotlar> ABSYayinlananBasariNotlar { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRBarkodOkuma> OGRBarkodOkuma { get; set; }
    }
}
