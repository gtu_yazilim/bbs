namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.Yetkililer")]
    public partial class Yetkililer
    {
        public int ID { get; set; }

        public int YetkiGrupID { get; set; }

        public int FKPersonelID { get; set; }

        public DateTime? YetkiBaslangic { get; set; }

        public DateTime? YetkiBitis { get; set; }

        [StringLength(15)]
        public string SGKullanici { get; set; }

        public DateTime? SGZaman { get; set; }

        public int? FKFakulteBirimID { get; set; }

        public int? FKBolumBirimID { get; set; }

        public int? FKAnaBilimDaliID { get; set; }

        public int? FKProgramBirimID { get; set; }
    }
}
