namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.ABSProjeTakvim")]
    public partial class ABSProjeTakvim
    {
        [Key]
        [Column(Order = 0)]
        public int ID { get; set; }
        
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FKDersPlanAnaID { get; set; }
        
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FKDersGrupID { get; set; }
        
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Yil { get; set; }
        
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EnumDonem { get; set; }
        
        [Column(Order = 5)]
        [StringLength(30)]
        public string Baslik { get; set; }

        public string Aciklama { get; set; }

        public int? MaxBoyut { get; set; }
        
        [Column(Order = 6)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int RaporSayisi { get; set; }
        
        [Column(Order = 7)]
        public DateTime Rapor1BitisTarihi { get; set; }

        public DateTime? Rapor2BitisTarihi { get; set; }

        public DateTime? Rapor3BitisTarihi { get; set; }

        public DateTime? Rapor4BitisTarihi { get; set; }

        public DateTime? Rapor5BitisTarihi { get; set; }
        
        [Column(Order = 8)]
        [StringLength(30)]
        public string Ekleyen { get; set; }
        
        [Column(Order = 9)]
        public DateTime EklenmeTarihi { get; set; }
        
        [Column(Order = 10)]
        [StringLength(20)]
        public string EkleyenIP { get; set; }

        public virtual OGRDersGrup OGRDersGrup { get; set; }

        public virtual OGRDersPlanAna OGRDersPlanAna { get; set; }
    }
}
