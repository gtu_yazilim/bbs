namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.SanalSinif")]
    public partial class SanalSinif
    {
        [Key]
        [Column(Order = 0)]
        public int ID { get; set; }

        public int? SunucuID { get; set; }

        public int? SSinifKey { get; set; }

        public string Adres { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(225)]
        public string Ad { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Hafta { get; set; }

        [Key]
        [Column(Order = 3)]
        public bool Yayinlandi { get; set; }

        [Key]
        [Column(Order = 4)]
        public bool Silindi { get; set; }

        [Key]
        [Column(Order = 5)]
        [StringLength(50)]
        public string Ekleyen { get; set; }

        [Key]
        [Column(Order = 6)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EkleyenID { get; set; }

        [Key]
        [Column(Order = 7)]
        public DateTime EklemeTarihi { get; set; }

        public int? GuncelleyenID { get; set; }

        [StringLength(50)]
        public string Guncelleyen { get; set; }

        public DateTime? GuncellemeTarihi { get; set; }

        [Key]
        [Column(Order = 8)]
        public bool Replay { get; set; }

        [Key]
        [Column(Order = 9)]
        public DateTime Baslangic { get; set; }

        [Key]
        [Column(Order = 10)]
        public DateTime Bitis { get; set; }

        [Key]
        [Column(Order = 11)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Sure { get; set; }

        [Key]
        [Column(Order = 12)]
        public bool SorunBildirim { get; set; }

        public int? AktifSure { get; set; }

        [Key]
        [Column(Order = 13)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ProviderType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SanalSinifGrup> SanalSinifGrup { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SanalSinifOgrenci> SanalSinifOgrenci { get; set; }

    }
}
