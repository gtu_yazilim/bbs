namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRMufredatAKTS")]
    public partial class OGRMufredatAKTS
    {
        public OGRMufredatAKTS()
        {

        }
        public int ID { get; set; }

        public int FKMufredatID { get; set; }

        public int Yariyil { get; set; }

        public int AKTS { get; set; }

        public DateTime BaslangicTarih { get; set; }

        public DateTime BitisTarih { get; set; }

        public string Kullanici { get; set; }

        public DateTime Zaman { get; set; }

        public virtual OGRMufredat OGRMufredat { get; set; }
    }
}
