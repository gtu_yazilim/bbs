namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.TNMKanunMaddeNo")]
    public partial class TNMKanunMaddeNo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TNMKanunMaddeNo()
        {
            DisGorevlendirme = new HashSet<DisGorevlendirme>();
            FiiliGorevYeri = new HashSet<FiiliGorevYeri>();
        }

        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int KanunNoID { get; set; }

        [Required]
        [StringLength(15)]
        public string KanunNo { get; set; }

        [Required]
        [StringLength(250)]
        public string KanunMaddesi { get; set; }

        public int ENUMGorevlendirmeTuru { get; set; }

        public bool AkademikGorsun { get; set; }

        public int? KullaniciKod { get; set; }

        public DateTime? IslemTarihi { get; set; }

        public int? FKKadroUnvaniID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DisGorevlendirme> DisGorevlendirme { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FiiliGorevYeri> FiiliGorevYeri { get; set; }

        public virtual TNMKadroUnvani TNMKadroUnvani { get; set; }
    }
}
