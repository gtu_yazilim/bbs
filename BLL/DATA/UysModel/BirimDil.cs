namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("gen.BirimDil")]
    public partial class BirimDil
    {
        [Key]
        [Column(Order = 0)]
        public int ID { get; set; }

        [Key]
        [Column(Order = 1)]
        public DateTime TarihKayit { get; set; }

        public DateTime? TarihGuncelleme { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(50)]
        public string Kullanici { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FKBirimID { get; set; }

        [Key]
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EnumDil { get; set; }

        [Key]
        [Column(Order = 5)]
        [StringLength(300)]
        public string BirimAdi { get; set; }

        [Key]
        [Column(Order = 6)]
        [StringLength(300)]
        public string BasilanBirimAdi { get; set; }

        [Key]
        [Column(Order = 7)]
        [StringLength(300)]
        public string BasilanBirimAdiKisa { get; set; }

        [Key]
        [Column(Order = 8)]
        public bool Silindi { get; set; }

        public virtual Birimler Birimler { get; set; }
    }
}
