namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRKayitBelgeleri")]
    public partial class OGRKayitBelgeleri
    {
        public int ID { get; set; }

        public int FKOgrenciID { get; set; }

        public bool Diploma { get; set; }

        public bool Transkript { get; set; }

        public bool SinavBelgesi { get; set; }

        public bool NufusCuzdan { get; set; }

        public bool Fotograf { get; set; }

        public bool SaglikRaporu { get; set; }

        public bool GMezuniyetBelgesi { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public DateTime? Zaman { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public bool BankaDekontu { get; set; }

        [StringLength(500)]
        public string NotAlani { get; set; }

        [StringLength(500)]
        public string BelgeKey { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
