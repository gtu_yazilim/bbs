namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.TNMAyrilma")]
    public partial class TNMAyrilma
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int AyrilmaID { get; set; }

        [StringLength(50)]
        public string AyrilmaAd { get; set; }

        public int? KullaniciKod { get; set; }

        public DateTime? IslemTarihi { get; set; }
    }
}
