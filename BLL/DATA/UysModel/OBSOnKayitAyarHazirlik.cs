namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("obs.OBSOnKayitAyarHazirlik")]
    public partial class OBSOnKayitAyarHazirlik
    {
        public int ID { get; set; }

        public int FKBirimPrgID { get; set; }

        public int Yil { get; set; }

        public bool HazirlikZorunlu { get; set; }

        public bool HazirlikVar { get; set; }

        public int EnumHazirlikTipi { get; set; }

        public virtual Birimler Birimler { get; set; }
    }
}
