namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRBelgeSayi")]
    public partial class OGRBelgeSayi
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(500)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? InKod { get; set; }

        public int? Adet { get; set; }

        public DateTime? Tarih { get; set; }

        public int? Sayi { get; set; }

        public int? EnumBelgeTuru { get; set; }

        public int? EnumBelgeDil { get; set; }

        public int? FKOgrenciID { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        [StringLength(500)]
        public string BelgeMetni { get; set; }

        public int? FKDosyaID { get; set; }

        public int Yil { get; set; }

        public virtual OGRDosya OGRDosya { get; set; }
    }
}
