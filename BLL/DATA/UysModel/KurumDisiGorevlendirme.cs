namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.KurumDisiGorevlendirme")]
    public partial class KurumDisiGorevlendirme
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        public string Kullanici { get; set; }

        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? PerKod { get; set; }

        public int? InKod { get; set; }

        public int? IslemNo { get; set; }

        public DateTime? BaslangicTarihi { get; set; }

        public DateTime? BitisTarihi { get; set; }

        public DateTime? YOKYaziTarihi { get; set; }

        [StringLength(20)]
        public string YOKYaziEvrakNo { get; set; }

        [StringLength(20)]
        public string YOKYaziSayisi { get; set; }

        [StringLength(250)]
        public string EvAdres { get; set; }

        [StringLength(20)]
        public string Telefon { get; set; }

        [StringLength(20)]
        public string DigerTelefon { get; set; }

        [StringLength(250)]
        public string Aciklama { get; set; }

        [StringLength(200)]
        public string Universite { get; set; }

        [StringLength(200)]
        public string Fakulte { get; set; }

        [StringLength(200)]
        public string Bolum { get; set; }

        [StringLength(200)]
        public string AnaBilimDali { get; set; }

        public int? EnumOgrenimTuru { get; set; }

        public int FKPersonelBilgisiID { get; set; }

        public int? FKKanunMaddeNoID { get; set; }

        public int FKUlkeID { get; set; }

        public int? FKUniversiteID { get; set; }

        public int? FKFakulteID { get; set; }

        public int? FKBolumID { get; set; }

        public int? FKAnaBilimDaliID { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }
    }
}
