namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRRaporTanim")]
    public partial class OGRRaporTanim
    {
        public int ID { get; set; }

        public DateTime TarihKayit { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [Required]
        [StringLength(250)]
        public string FKRaporTasarimKey { get; set; }

        [StringLength(250)]
        public string FKRaporTasarimArkaKey { get; set; }

        public int FKBirimID { get; set; }

        public DateTime TarihBaslangic { get; set; }

        public DateTime TarihBitis { get; set; }

        public DateTime? TarihSilinme { get; set; }

        public bool Aktif { get; set; }

        public int EnumBelgeTuru { get; set; }

        public bool Silindi { get; set; }

        public int EnumDil { get; set; }

        public int EnumRaporServisTuru { get; set; }

        public int? EnumOgretimSeviye { get; set; }

        public int? FKRaporTasarimID { get; set; }

        public int? FKRaporTasarimArkaID { get; set; }

        public virtual Birimler Birimler { get; set; }
    }
}
