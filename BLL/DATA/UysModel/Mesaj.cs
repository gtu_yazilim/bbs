namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("msg.Mesaj")]
    public partial class Mesaj
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Mesaj()
        {
            Bildirim = new HashSet<Bildirim>();
        }

        public int ID { get; set; }

        public DateTime TarihKayit { get; set; }

        public int? FKMesajID { get; set; }

        [Required]
        [StringLength(250)]
        public string Baslik { get; set; }

        public string Metin { get; set; }

        public int FKProjeID { get; set; }

        public bool TumKullanicilar { get; set; }

        public int FKGonderenKullaniciID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Bildirim> Bildirim { get; set; }

        public virtual Kullanici Kullanici { get; set; }

        public virtual Proje Proje { get; set; }
    }
}
