namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGROgrenciDersPlanAlis")]
    public partial class OGROgrenciDersPlanAlis
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OGROgrenciDersPlanAlis()
        {

        }

        public int ID { get; set; }

        public DateTime Zaman { get; set; }


        public string Kullanici { get; set; }
        public int Yil { get; set; }
        public int EnumDonem { get; set; }
        public int? FKOgrenciDersPlanID { get; set; }
        public bool TransferMi { get; set; }
        public int? FKDersPlanEtiketID { get; set; }
        public int? FKDersPlanID { get; set; }
        public int FKDersPlanAnaID { get; set; }
        public DateTime GecerlilikBaslangic { get; set; }
        public DateTime GecerlilikBitis { get; set; }
        public int? FKOgrenciNotID { get; set; }
        public int? FKYazilmaID { get; set; }
        public int FKOgrenciID { get; set; }

        public virtual OGROgrenciDersPlan OGROgrenciDersPlan { get; set; }
        public virtual OGRDersPlanEtiket OGRDersPlanEtiket { get; set; }
        public virtual OGRDersPlan OGRDersPlan { get; set; }
        public virtual OGRDersPlanAna OGRDersPlanAna { get; set; }
        public virtual OGROgrenciNot OGROgrenciNot { get; set; }
        public virtual OGROgrenciYazilma OGROgrenciYazilma { get; set; }
        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
