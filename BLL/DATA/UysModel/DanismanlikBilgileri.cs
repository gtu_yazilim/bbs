namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.DanismanlikBilgileri")]
    public partial class DanismanlikBilgileri
    {
        public int ID { get; set; }

        public int FKOgrenciID { get; set; }

        [StringLength(50)]
        public string BilgisayarBilgisi1 { get; set; }

        [StringLength(50)]
        public string BilgisayarBilgisi1Seviye { get; set; }

        [StringLength(50)]
        public string BilgisayarBilgisi2 { get; set; }

        [StringLength(50)]
        public string BilgisayarBilgisi2Seviye { get; set; }

        [StringLength(50)]
        public string BilgisayarBilgisi3 { get; set; }

        [StringLength(50)]
        public string BilgisayarBilgisi3Seviye { get; set; }

        [StringLength(50)]
        public string YabanciDil1 { get; set; }

        [StringLength(50)]
        public string YabanciDil1Seviye { get; set; }

        [StringLength(50)]
        public string YabanciDil2 { get; set; }

        [StringLength(50)]
        public string YabanciDil2Seviye { get; set; }

        [StringLength(50)]
        public string YabanciDil3 { get; set; }

        [StringLength(50)]
        public string YabanciDil3Seviye { get; set; }

        [StringLength(250)]
        public string Burs1 { get; set; }

        [StringLength(250)]
        public string Burs2 { get; set; }

        [StringLength(250)]
        public string Burs3 { get; set; }

        [StringLength(250)]
        public string Kredi1 { get; set; }

        [StringLength(250)]
        public string Kredi2 { get; set; }

        [StringLength(250)]
        public string Kredi3 { get; set; }

        [StringLength(250)]
        public string Sertifika1 { get; set; }

        [StringLength(250)]
        public string Sertifika2 { get; set; }

        [StringLength(250)]
        public string Sertifika3 { get; set; }

        public DateTime? Sertifika1Tarih { get; set; }

        public DateTime? Sertifika2Tarih { get; set; }

        public DateTime? Sertifika3Tarih { get; set; }

        public DateTime? Sertifika1Tarih1 { get; set; }

        public DateTime? Sertifika2Tarih1 { get; set; }

        public DateTime? Sertifika3Tarih1 { get; set; }

        [StringLength(250)]
        public string CalistigiKurum { get; set; }

        public bool? SosyalGuvenceVarMi { get; set; }

        public int? FK_KaldinizSehirID { get; set; }

        public int? FK_KaldiginizIlceID { get; set; }

        [StringLength(50)]
        public string KaldiginizYer { get; set; }

        public int? KaldiginizYerdekiKisiSayisi { get; set; }

        [StringLength(50)]
        public string OturdugunuzEvKimeAit { get; set; }

        public bool? KronikHastalikVarMi { get; set; }

        public bool? SurekliKullanilanIlacVarMi { get; set; }

        public bool? AlerjiVarMi { get; set; }

        [StringLength(250)]
        public string AlerjiOykusu { get; set; }

        [StringLength(50)]
        public string SizeGoreAilenizinEkonomikDurumu { get; set; }

        [StringLength(250)]
        public string CalistiginizYer1 { get; set; }

        [StringLength(250)]
        public string CalistiginizYer2 { get; set; }

        [StringLength(250)]
        public string CalistiginizYer3 { get; set; }

        [StringLength(250)]
        public string CalismaTarihi1 { get; set; }

        [StringLength(250)]
        public string CalismaTarihi2 { get; set; }

        [StringLength(250)]
        public string CalismaTarihi3 { get; set; }

        public virtual DanismanlikBilgileri DanismanlikBilgileri1 { get; set; }

        public virtual DanismanlikBilgileri DanismanlikBilgileri2 { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }

        public virtual TNMIl TNMIl { get; set; }

        public virtual TNMIlce TNMIlce { get; set; }
    }
}
