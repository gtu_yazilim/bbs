namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("gen.osymSiralama")]
    public partial class osymSiralama
    {
        public int ID { get; set; }

        public double? Kod { get; set; }

        [StringLength(255)]
        public string Universite { get; set; }

        [StringLength(255)]
        public string Fakulte { get; set; }

        [StringLength(255)]
        public string Program { get; set; }

        [StringLength(10)]
        public string PuanTur { get; set; }

        public int? Kontenjan { get; set; }

        public int? Yerlesen { get; set; }

        public int? Sira { get; set; }

        public decimal? EnDusuk { get; set; }

        public decimal? EnYuksek { get; set; }

        public double? yoksis_birim_id { get; set; }

        public int? OgretimTur { get; set; }

        public int? Yil { get; set; }
    }
}
