namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.ABSPaylar")]
    public partial class ABSPaylar
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ABSPaylar()
        {
            ABSOgrenciNot = new HashSet<ABSOgrenciNot>();
            ABSPaylarOlcme = new HashSet<ABSPaylarOlcme>();
            ABSYayinlananPayNotlar = new HashSet<ABSYayinlananPayNotlar>();
            OSinavGrup = new HashSet<OSinavGrup>();
            PayRandevu = new HashSet<PayRandevu>();
        }

        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        public string Kullanici { get; set; }

        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? InKod { get; set; }

        public byte? Sira { get; set; }

        public byte? Oran { get; set; }

        public int? Yil { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? Tarih { get; set; }

        public int? EnumDonem { get; set; }

        public int? FKPersonelID { get; set; }

        public int? FKDersPlanAnaID { get; set; }

        public int? EnumCalismaTip { get; set; }

        public bool? Yayinlandi { get; set; }

        public DateTime? YayinlanmaTarihi { get; set; }

        public int? FKDersGrupID { get; set; }

        public int? FKDersPlanID { get; set; }

        [StringLength(100)]
        public string PayEtiket { get; set; }

        public int? FKAltDersPlanAnaID { get; set; }

        public int? Pay { get; set; }

        public int? Payda { get; set; }

        public bool Silindi { get; set; }

        public DateTime? SilindiTarih { get; set; }

        [StringLength(30)]
        public string SilenKullanici { get; set; }

        [StringLength(30)]
        public string SilenIP { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSOgrenciNot> ABSOgrenciNot { get; set; }

        public virtual OGRDersGrup OGRDersGrup { get; set; }

        public virtual OGRDersPlan OGRDersPlan { get; set; }

        public virtual OGRDersPlanAna OGRDersPlanAna { get; set; }

        public virtual OGRDersPlanAna OGRDersPlanAna1 { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSPaylarOlcme> ABSPaylarOlcme { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSYayinlananPayNotlar> ABSYayinlananPayNotlar { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OSinavGrup> OSinavGrup { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PayRandevu> PayRandevu { get; set; }
    }
}
