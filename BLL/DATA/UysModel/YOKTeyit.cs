namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.YOKTeyit")]
    public partial class YOKTeyit
    {
        public int ID { get; set; }

        public int Aktif { get; set; }

        public bool? AktifTeyit { get; set; }

        public int Izinli { get; set; }

        public bool? IzinliTeyit { get; set; }

        public int Cezali { get; set; }

        public bool? CezaliTeyit { get; set; }

        public int Beklemeli { get; set; }

        public bool? BeklemeliTeyit { get; set; }

        public int Pasif { get; set; }

        public bool? PasifTeyit { get; set; }

        public int FarabiGiden { get; set; }

        public bool? FarabiGidenTeyit { get; set; }

        public int FarabiGelen { get; set; }

        public bool? FarabiGelenTeyit { get; set; }

        public int MevlanaGiden { get; set; }

        public bool? MevlanaGidenTeyit { get; set; }

        public int MevlanaGelen { get; set; }

        public bool? MevlanaGelenTeyit { get; set; }

        public int ErasmusGiden { get; set; }

        public bool? ErasmusGidenTeyit { get; set; }

        public int ErasmusGelen { get; set; }

        public bool? ErasmusGelenTeyit { get; set; }

        public int OzelGiden { get; set; }

        public bool? OzelGidenTeyit { get; set; }

        public int OzelGelen { get; set; }

        public bool? OzelGelenTeyit { get; set; }

        public int DigerYurtDisiGiden { get; set; }

        public bool? DigerYurtDisiGidenTeyit { get; set; }

        public int DigerYurtDisiGelen { get; set; }

        public bool? DigerYurtDisiGelenTeyit { get; set; }

        public int Yil { get; set; }

        public int EnumDonem { get; set; }

        public bool? AktarildiMi { get; set; }

        public DateTime? AktarimZamani { get; set; }

        [StringLength(500)]
        public string AktarimMesaji { get; set; }

        public DateTime Zaman { get; set; }
    }
}
