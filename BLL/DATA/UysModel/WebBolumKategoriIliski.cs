namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.WebBolumKategoriIliski")]
    public partial class WebBolumKategoriIliski
    {
        [Key]
        [Column(Order = 0)]
        public int InKod { get; set; }

        [StringLength(2)]
        public string FakKod { get; set; }

        [StringLength(2)]
        public string BolKod { get; set; }

        public byte? OgretimSeviye { get; set; }

        public int? YetId { get; set; }

        public int? AnaKategoriNo { get; set; }

        public int? AltKategoriNo { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short Yil1 { get; set; }

        [StringLength(20)]
        public string Ekleyen { get; set; }

        public DateTime? Tarih { get; set; }

        public int? FKFakulteBirimID { get; set; }

        public int? FKBolumBirimID { get; set; }

        public int? FKProgramBirimID { get; set; }

        public virtual WebBolumKategoriIliski WebBolumKategoriIliski1 { get; set; }

        public virtual WebBolumKategoriIliski WebBolumKategoriIliski2 { get; set; }
    }
}
