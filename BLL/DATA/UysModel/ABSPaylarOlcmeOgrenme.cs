namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.ABSPaylarOlcmeOgrenme")]
    public partial class ABSPaylarOlcmeOgrenme
    {
        public int ID { get; set; }

        public int? FKPaylarOlcmeID { get; set; }

        public int? FKOgrenmeCiktiID { get; set; }

        public virtual ABSPaylarOlcme ABSPaylarOlcme { get; set; }

        public virtual DersCikti DersCikti { get; set; }
    }
}
