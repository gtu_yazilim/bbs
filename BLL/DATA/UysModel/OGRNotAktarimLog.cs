namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRNotAktarimLog")]
    public partial class OGRNotAktarimLog
    {
        public int ID { get; set; }

        public DateTime Tarih { get; set; }

        [Required]
        [StringLength(250)]
        public string BarkodNo { get; set; }

        [Required]
        [StringLength(250)]
        public string Kullanici { get; set; }

        [Required]
        [StringLength(50)]
        public string IP { get; set; }

        [Required]
        [StringLength(250)]
        public string Tip { get; set; }

        [Required]
        public string Sonuc { get; set; }
    }
}
