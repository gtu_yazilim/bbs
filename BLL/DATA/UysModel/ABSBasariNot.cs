namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.ABSBasariNot")]
    public partial class ABSBasariNot
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ABSBasariNot()
        {
            OGRHazirlik = new HashSet<OGRHazirlik>();
            OGROgrenciNot = new HashSet<OGROgrenciNot>();
        }

        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        public string Kullanici { get; set; }

        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? InKod { get; set; }

        public double? Mutlak { get; set; }

        public double? Bagil { get; set; }

        public int? Yil { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? Tarih { get; set; }

        public int? EnumDonem { get; set; }

        public int? EnumHarfMutlak { get; set; }

        public int? EnumHarfBasari { get; set; }

        public int? FKOgrenciID { get; set; }

        public int? FKDersPlanID { get; set; }

        public int? FKDersGrupID { get; set; }

        public bool ButunlemeMi { get; set; }

        public bool SilindiMi { get; set; }

        public int? FKDersPlanAnaID { get; set; }

        public int? FKDegerlendirmeID { get; set; }

        public virtual ABSDegerlendirme ABSDegerlendirme { get; set; }

        public virtual OGRDersGrup OGRDersGrup { get; set; }

        public virtual OGRDersPlan OGRDersPlan { get; set; }

        public virtual OGRDersPlanAna OGRDersPlanAna { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRHazirlik> OGRHazirlik { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGROgrenciNot> OGROgrenciNot { get; set; }
    }
}
