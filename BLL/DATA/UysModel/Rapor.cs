namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.Rapor")]
    public partial class Rapor
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int InKod { get; set; }

        public int Sure { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? AyrilisTarihi { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? DonusTarihi { get; set; }

        [StringLength(250)]
        public string Aciklama { get; set; }

        public int KullaniciKod { get; set; }

        public DateTime IslemTarihi { get; set; }

        public int ENUMRaporTuru { get; set; }

        public int FKPersonelID { get; set; }
    }
}
