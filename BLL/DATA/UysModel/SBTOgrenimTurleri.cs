namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.SBTOgrenimTurleri")]
    public partial class SBTOgrenimTurleri
    {
        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string OgrenimTuru { get; set; }
    }
}
