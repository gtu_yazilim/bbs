namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGROzelOgrenciExceldenTranskript")]
    public partial class OGROzelOgrenciExceldenTranskript
    {
        [Key]
        [Column(Order = 0)]
        public int ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FKOgrenciID { get; set; }

        [Key]
        [Column(Order = 2)]
        public DateTime TarihKayit { get; set; }

        public DateTime? TarihGuncelleme { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(50)]
        public string Kullanici { get; set; }

        [Key]
        [Column(Order = 4)]
        public string TranskriptJson { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
