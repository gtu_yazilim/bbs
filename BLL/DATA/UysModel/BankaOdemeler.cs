namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("bnk.BankaOdemeler")]
    public partial class BankaOdemeler
    {
        public int ID { get; set; }

        public DateTime ActivityDate { get; set; }

        public double Amount { get; set; }

        public double Balance { get; set; }

        [StringLength(100)]
        public string Explanation { get; set; }

        [StringLength(11)]
        public string TCNO { get; set; }

        [StringLength(11)]
        public string OgrenciNo { get; set; }

        [Required]
        [StringLength(10)]
        public string TransactionId { get; set; }

        [Required]
        [StringLength(30)]
        public string TransactionReferenceId { get; set; }

        public long RefersansID { get; set; }

        [StringLength(10)]
        public string CorrBankNum { get; set; }

        [StringLength(10)]
        public string CorrBranchNum { get; set; }

        [StringLength(50)]
        public string CorrIBAN { get; set; }

        [StringLength(30)]
        public string CorrVKN { get; set; }

        public int FKServisHareketID { get; set; }

        public bool? AktarildiMi { get; set; }

        public virtual BankaWebServisHareketi BankaWebServisHareketi { get; set; }
    }
}
