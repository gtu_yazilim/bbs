namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ytk.IslemLog")]
    public partial class IslemLog
    {
        public int ID { get; set; }

        public DateTime Tarih { get; set; }

        public int EnumIslemTipi { get; set; }

        public int? FKKullaniciID { get; set; }

        [StringLength(50)]
        public string IP { get; set; }

        [StringLength(50)]
        public string Sunucu { get; set; }

        [StringLength(50)]
        public string IslemYapan { get; set; }

        public virtual Kullanici Kullanici { get; set; }
    }
}
