namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRMufredatDersleri")]
    public partial class OGRMufredatDersleri
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OGRMufredatDersleri()
        {
           
        }
        public int ID { get; set; }

        public DateTime Zaman { get; set; }

        public string Kullanici { get; set; }

        public int FKMufredatID { get; set; }

        public int FKDersPlanAnaID { get; set; }

        public DateTime BaslangicTarihi { get; set; }

        public DateTime BitisTarihi { get; set; }

        public int? Yariyil { get; set; }

        public virtual OGRMufredat OGRMufredat { get; set; }

        public virtual OGRDersPlanAna OGRDersPlanAna { get; set; }
    }
}
