namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRGeciciOgrenciNumaralari")]
    public partial class OGRGeciciOgrenciNumaralari
    {
        public int ID { get; set; }

        [Required]
        [StringLength(11)]
        public string TCKimlikNo { get; set; }

        [StringLength(100)]
        public string AdSoyad { get; set; }

        public int FKBirimID { get; set; }

        [StringLength(2)]
        public string BankaBirimTuru { get; set; }

        public int OgrenciNo { get; set; }

        public int Yil { get; set; }

        public int Donem { get; set; }

        public DateTime Zaman { get; set; }

        public int? FKOgrenciID { get; set; }

        public double? HarcMiktari { get; set; }

        public double? Odeme { get; set; }

        public DateTime? OdemeTarihi { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public DateTime? SGZamani { get; set; }

        public int? FKOdemeID { get; set; }

        public DateTime? SonOdemeTarihi { get; set; }

        public virtual OnlineHarcOdemeleri OnlineHarcOdemeleri { get; set; }

        public virtual Birimler Birimler { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
