namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRHarcKrediUcretlendirmesiOdeme")]
    public partial class OGRHarcKrediUcretlendirmesiOdeme
    {
        public int ID { get; set; }

        public DateTime Zaman { get; set; }

        [Required]
        [StringLength(500)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public double OdemeMiktari { get; set; }

        public DateTime OdemeTarihi { get; set; }

        public int Yil { get; set; }

        public int AitYil { get; set; }

        public int EnumDonemOdeme { get; set; }

        public int EnumDonemAit { get; set; }

        public int? FKKisiID { get; set; }

        [StringLength(11)]
        public string TCNO { get; set; }

        [StringLength(50)]
        public string AdSoyad { get; set; }

        [StringLength(50)]
        public string OdemeKodu { get; set; }

        [StringLength(100)]
        public string Aciklama { get; set; }

        public DateTime SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public virtual Kisi Kisi { get; set; }
    }
}
