namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.KadroBilgisi")]
    public partial class KadroBilgisi
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public KadroBilgisi()
        {
            Hareket = new HashSet<Hareket>();
            Hareket1 = new HashSet<Hareket>();
            IzinYOKDetay = new HashSet<IzinYOKDetay>();
            IzinYOKDoluDetay = new HashSet<IzinYOKDoluDetay>();
            KadroTakasi = new HashSet<KadroTakasi>();
            KadroTakasi1 = new HashSet<KadroTakasi>();
        }

        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int KadroNo { get; set; }

        public int Derece { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? MaliyeIzinTarihi { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? YOKIzinTarihi { get; set; }

        public int TahsisHareketIslemID { get; set; }

        public int? KullaniciKod { get; set; }

        public DateTime? IslemTarihi { get; set; }

        public int ENUMKadroTipi { get; set; }

        public int ENUMKadroDurumu { get; set; }

        public int? FKPersonelBilgisiID { get; set; }

        public int? FKKadroUnvaniID { get; set; }

        public int? FKUlkeID { get; set; }

        public int? FKUniversiteID { get; set; }

        public int? FKFakulteID { get; set; }

        public int? FKBolumID { get; set; }

        public int? FKAnaBilimDaliID { get; set; }

        public int? PlakaNo { get; set; }

        public virtual Birimler Birimler { get; set; }

        public virtual Birimler Birimler1 { get; set; }

        public virtual Birimler Birimler2 { get; set; }

        public virtual Birimler Birimler3 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Hareket> Hareket { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Hareket> Hareket1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IzinYOKDetay> IzinYOKDetay { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IzinYOKDoluDetay> IzinYOKDoluDetay { get; set; }

        public virtual TNMKadroUnvani TNMKadroUnvani { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<KadroTakasi> KadroTakasi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<KadroTakasi> KadroTakasi1 { get; set; }
    }
}
