namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("basvuru.OgrenciEtkinlikBilgiFormu")]
    public partial class OgrenciEtkinlikBilgiFormu
    {
        public int ID { get; set; }

        public int FKOgrenciID { get; set; }

        public DateTime TarihKayit { get; set; }

        [Required]
        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string CepTelefonu { get; set; }

        [StringLength(50)]
        public string EvTelefonu { get; set; }

        [StringLength(150)]
        public string Email { get; set; }

        public int? IlgiliSporDaliFutbol { get; set; }

        public int? IlgiliSporDaliBasket { get; set; }

        public int? IlgiliSporDaliVoleybol { get; set; }

        public int? IlgiliSporDaliFitness { get; set; }

        [StringLength(250)]
        public string IlgiliSporDaliDigerAd { get; set; }

        public int? IlgiliSporDaliDiger { get; set; }

        public int? IlgiliEnstrumanPiyano { get; set; }

        public int? IlgiliEnstrumanKeman { get; set; }

        public int? IlgiliEnstrumanFlut { get; set; }

        public int? IlgiliEnstrumanBaglama { get; set; }

        [StringLength(250)]
        public string IlgiliEnstrumanDigerAd { get; set; }

        public int? IlgiliEnstrumanDiger { get; set; }

        public bool OgrenciKlubuOkculuk { get; set; }

        public bool OgrenciKlubuBinicilik { get; set; }

        public bool OgrenciKlubuMuzik { get; set; }

        public bool OgrenciKlubuDrama { get; set; }

        [StringLength(250)]
        public string OgrenciKlubuDigerAd { get; set; }

        public bool OgrenciKlubuDiger { get; set; }

        public DateTime? TarihGuncelleme { get; set; }

        public int? IlgiliSporDaliBowling { get; set; }

        public int? IlgiliSporDaliMasaTenisi { get; set; }

        public int? IlgiliSporDaliOkculuk { get; set; }

        public int? IlgiliSporDaliKurek { get; set; }

        public int? IlgiliSporDaliKickBoks { get; set; }

        public int? IlgiliSporDaliYuzme { get; set; }

        public int? IlgiliSporDaliSatranc { get; set; }

        public int? IlgiliSporDaliGures { get; set; }

        public int? IlgiliSporDaliJudo { get; set; }

        public int? IlgiliSporDaliKarete { get; set; }

        public int? IlgiliSporDaliTaekwondo { get; set; }

        public int? IlgiliSporDaliOryantiring { get; set; }

        public int? IlgiliSporDaliBoks { get; set; }

        public int? IlgiliSporDaliBinicilik { get; set; }

        public int? IlgiliSporDaliSquash { get; set; }

        public int? IlgiliEnstrumanGitar { get; set; }

        public int? IlgiliEnstrumanTambur { get; set; }

        public int? IlgiliEnstrumanBateri { get; set; }

        public int? IlgiliEnstrumanUd { get; set; }

        public int? IlgiliEnstrumanNey { get; set; }

        public int? IlgiliEnstrumanKemence { get; set; }

        public int? IlgiliEnstrumanKanun { get; set; }

        public int? IlgiliEnstrumanBendir { get; set; }

        public bool OgrenciKlubuDagcilik { get; set; }

        public bool OgrenciKlubuEdebiyat { get; set; }

        public bool OgrenciKlubuFotograf { get; set; }

        public bool OgrenciKlubuMunazara { get; set; }

        public bool OgrenciKlubuSinema { get; set; }

        public bool OgrenciKlubuTurkMuzigi { get; set; }

        public bool OgrenciKlubuSatranc { get; set; }

        public bool OgrenciKlubuSpor { get; set; }

        public bool OgrenciKlubuTiyatro { get; set; }

        public bool OgrenciKlubuUluslararasiOgrenciAgi { get; set; }

        public bool OgrenciKlubuBisiklet { get; set; }

        public int? IlgiliSporDaliBadminton { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
