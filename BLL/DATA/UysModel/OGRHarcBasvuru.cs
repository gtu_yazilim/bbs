namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRHarcBasvuru")]
    public partial class OGRHarcBasvuru
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(500)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? InKod { get; set; }

        [StringLength(100)]
        public string TC { get; set; }

        [StringLength(250)]
        public string AdSoyad { get; set; }

        public int? Yil { get; set; }

        public int? Ucret { get; set; }

        [StringLength(250)]
        public string GuvenlikKodu { get; set; }

        public DateTime? OdemeTarih { get; set; }

        public int? EnumDonem { get; set; }

        public int? EnumOkulTipi { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }
    }
}
