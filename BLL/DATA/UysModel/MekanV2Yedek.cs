namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("mek.MekanV2Yedek")]
    public partial class MekanV2Yedek
    {
        [Key]
        [Column(Order = 0)]
        public int ID { get; set; }

        public int? UstMekanID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FKMekanTipID { get; set; }

        public int? FKBirimID { get; set; }

        public int? FKIlID { get; set; }

        public int? FKIlceID { get; set; }

        public int? FKSorumluKisiID { get; set; }

        [StringLength(200)]
        public string MekanAd { get; set; }

        [StringLength(300)]
        public string MekanKisaAd { get; set; }

        [StringLength(500)]
        public string MekanUzunAd { get; set; }

        [StringLength(50)]
        public string MekanKod { get; set; }

        [StringLength(300)]
        public string AcikAdres { get; set; }

        [StringLength(300)]
        public string Aciklama { get; set; }

        public int? Kat { get; set; }

        public int? Alan { get; set; }

        public int? ToplamKapasite { get; set; }

        public int? SinavKapasite { get; set; }

        [Key]
        [Column(Order = 2)]
        public bool Aktif { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(50)]
        public string Kullanici { get; set; }

        [Key]
        [Column(Order = 4)]
        public DateTime Zaman { get; set; }

        public DateTime? SGZaman { get; set; }

        public int? EskiID { get; set; }

        public double? Latitude { get; set; }

        public double? Longitude { get; set; }
    }
}
