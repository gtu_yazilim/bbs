namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ytk.KullaniciTicket")]
    public partial class KullaniciTicket
    {
        [Key]
        public Guid TicketID { get; set; }

        public int? FKKullaniciID { get; set; }

        [StringLength(50)]
        public string KullaniciAdi { get; set; }

        [StringLength(200)]
        public string EPosta { get; set; }

        [Required]
        [StringLength(50)]
        public string IP { get; set; }

        public bool BeniHatirla { get; set; }

        public DateTime TarihGiris { get; set; }

        public DateTime? TarihGuncelleme { get; set; }

        public DateTime? TarihBitis { get; set; }

        public int EnumKullaniciTip { get; set; }

        public string URL { get; set; }

        public bool TOTPGerekliMi { get; set; }

        public DateTime SuperErisimBitisTarihi { get; set; }
    }
}
