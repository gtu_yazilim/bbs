namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRHazirlik")]
    public partial class OGRHazirlik
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(100)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? InKod { get; set; }

        public int? Yil { get; set; }

        [StringLength(50)]
        public string BNot { get; set; }

        public int? Butunleme { get; set; }

        public int? EnumHazirlik { get; set; }

        public int? EnumHazirlikDurum { get; set; }

        public int? FKOgrenciID { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public int? EnumHazirlikTipi { get; set; }

        public int? EnumDonem { get; set; }

        public bool Aktif { get; set; }

        public int? FKABSBasariNotID { get; set; }

        public virtual ABSBasariNot ABSBasariNot { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
