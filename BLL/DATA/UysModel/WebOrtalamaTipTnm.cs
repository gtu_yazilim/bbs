namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.WebOrtalamaTipTnm")]
    public partial class WebOrtalamaTipTnm
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int InKod { get; set; }

        public byte FormDil { get; set; }

        public byte OrtalamaTipKod { get; set; }

        [Required]
        [StringLength(50)]
        public string OrtalamaTipAd { get; set; }

        public DateTime Zaman { get; set; }
    }
}
