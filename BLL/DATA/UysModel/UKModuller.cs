namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UKModuller")]
    public partial class UKModuller
    {
        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string ModulAd { get; set; }

        [StringLength(250)]
        public string ModulYol { get; set; }

        [StringLength(50)]
        public string KisaUrl { get; set; }

        public int? UstID { get; set; }

        public bool HerkeseAcik { get; set; }

        public int Sira { get; set; }

        public bool UstMenudeGorunsun { get; set; }

        public bool? AdminMenusu { get; set; }

        [StringLength(50)]
        public string CssClass { get; set; }

        public string Aciklama { get; set; }
    }
}
