namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGREnstituSeminer")]
    public partial class OGREnstituSeminer
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? InKod { get; set; }

        public DateTime? Tarih { get; set; }

        public int? EnumSeminerDurum { get; set; }

        public int? FKOgrenciID { get; set; }

        public int? FKDanismanID { get; set; }

        public int? FKUye1 { get; set; }

        public int? FKUye2 { get; set; }

        public int? FKMekanID { get; set; }

        public TimeSpan? Saat { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public virtual Mekan Mekan { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi1 { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi2 { get; set; }
    }
}
