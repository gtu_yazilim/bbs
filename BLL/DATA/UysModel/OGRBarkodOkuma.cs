namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRBarkodOkuma")]
    public partial class OGRBarkodOkuma
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OGRBarkodOkuma()
        {
            OGROgrenciNot = new HashSet<OGROgrenciNot>();
        }

        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(500)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public int? FKABSDegerlendirme { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public bool? ButunlemeMi { get; set; }

        public virtual ABSDegerlendirme ABSDegerlendirme { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGROgrenciNot> OGROgrenciNot { get; set; }
    }
}
