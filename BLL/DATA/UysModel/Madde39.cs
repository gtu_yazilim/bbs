namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.Madde39")]
    public partial class Madde39
    {
        public int ID { get; set; }

        [StringLength(255)]
        public string KayitNo { get; set; }

        [StringLength(255)]
        public string SiraNo { get; set; }

        public int? FKPersonelID { get; set; }

        [StringLength(255)]
        public string SicilNo { get; set; }

        [StringLength(255)]
        public string Adi { get; set; }

        [StringLength(255)]
        public string Soyadi { get; set; }

        public int? FKAkademikUnvanID { get; set; }

        [StringLength(255)]
        public string KadroUnvani { get; set; }

        public int? FKKadroBirimiID { get; set; }

        [StringLength(255)]
        public string KadroBirimi { get; set; }

        public int? FKUlkeID { get; set; }

        public int? FKIlID { get; set; }

        public int? FKIlceID { get; set; }

        [StringLength(255)]
        public string UlkeSehir { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? BaslangicTarihi { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? BitisTarihi { get; set; }

        public bool? Yolluklu { get; set; }

        public bool? Yevmiyeli { get; set; }

        public bool? Maasli { get; set; }

        public bool? Izinli { get; set; }

        [StringLength(255)]
        public string Aciklama { get; set; }

        [StringLength(50)]
        public string Sure { get; set; }

        public int? FKKanunMaddeNoID { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? GorevineBaslamaDonusTarihi { get; set; }

        public int? ENUMGorevSuresi { get; set; }

        [StringLength(255)]
        public string KayitEdildigiAy { get; set; }

        public int? ENUMDurumEvetHayir { get; set; }

        public int? ENUMYurtIciYurtDisi { get; set; }

        public int? ENUMOnRapor { get; set; }

        [StringLength(255)]
        public string OnRaporNo { get; set; }

        public int? ENUMDonusRaporu { get; set; }

        [StringLength(255)]
        public string DonusRaporNo { get; set; }
    }
}
