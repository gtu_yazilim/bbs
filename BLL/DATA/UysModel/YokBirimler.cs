namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("gen.YokBirimler")]
    public partial class YokBirimler
    {
        public int ID { get; set; }

        public long YoksisID { get; set; }

        [Required]
        [StringLength(300)]
        public string BirimAdi { get; set; }

        [StringLength(300)]
        public string BirimAdiIngilizce { get; set; }

        public long? BagliOlduguYoksisID { get; set; }

        public int Aktif { get; set; }

        [StringLength(50)]
        public string BirimTuruAdi { get; set; }

        [StringLength(500)]
        public string BirimUzunAdi { get; set; }

        public int? EnumBirimTuru { get; set; }

        public int? IlKodu { get; set; }

        public int? IlceKodu { get; set; }

        [StringLength(50)]
        public string OgrenimDili { get; set; }

        public int? OgrenimSuresi { get; set; }

        [StringLength(50)]
        public string OgrenimTuru { get; set; }

        [StringLength(500)]
        public string AcikAdres { get; set; }

        public long? KlavuzKodu { get; set; }

        public int? EnumKategori { get; set; }

        public long? FKUniversiteYoksisID { get; set; }

        public int? FKUlkeID { get; set; }
    }
}
