namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.WebDersYeterlilikIliski")]
    public partial class WebDersYeterlilikIliski
    {
        [Key]
        [Column(Order = 0)]
        public int InKod { get; set; }

        public int? DersPlanID { get; set; }

        public int? AnaDersPlanID { get; set; }

        [StringLength(2)]
        public string FakKod { get; set; }

        [StringLength(2)]
        public string BolKod { get; set; }

        public byte? OgretimSeviye { get; set; }

        public int? YeterlilikID { get; set; }

        public byte? KatkiDuzeyi { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short Yil1 { get; set; }

        [StringLength(30)]
        public string Ekleyen { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? Zaman { get; set; }

        public int? FKFakulteBirimID { get; set; }

        public int? FKBolumBirimID { get; set; }

        public int? FKProgramBirimID { get; set; }
    }
}
