namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("obs.OBSIliskiKesme")]
    public partial class OBSIliskiKesme
    {
        public int ID { get; set; }

        public DateTime TarihKayit { get; set; }

        public int FKOgrenciID { get; set; }

        [Required]
        [StringLength(50)]
        public string BarkodNo { get; set; }

        public int EnumIlisikKesmeNedeni { get; set; }

        public string Adres { get; set; }

        public string IsAdresi { get; set; }

        [StringLength(50)]
        public string GSM { get; set; }

        [StringLength(50)]
        public string IsTel { get; set; }

        [StringLength(50)]
        public string EvTel { get; set; }

        [StringLength(10)]
        public string PostaKodu { get; set; }

        [StringLength(250)]
        public string EPosta { get; set; }

        public bool AktarildiMi { get; set; }

        public DateTime? TarihBarkodOkuma { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
