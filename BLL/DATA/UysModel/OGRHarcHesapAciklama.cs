namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRHarcHesapAciklama")]
    public partial class OGRHarcHesapAciklama
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(500)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? InKod { get; set; }

        [StringLength(100)]
        public string IslemKodu { get; set; }

        [StringLength(100)]
        public string SubeKodu { get; set; }

        [StringLength(250)]
        public string HesapNo { get; set; }

        [StringLength(250)]
        public string Aciklama { get; set; }

        public int? EnumHesapTuru { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public int? FKFakulteBirimID { get; set; }

        public virtual Birimler Birimler { get; set; }
    }
}
