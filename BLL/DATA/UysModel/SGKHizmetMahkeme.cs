namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.SGKHizmetMahkeme")]
    public partial class SGKHizmetMahkeme
    {
        public int ID { get; set; }

        [StringLength(11)]
        public string Tckn { get; set; }

        public int? kayitNo { get; set; }

        [StringLength(200)]
        public string mahkemeAd { get; set; }

        [StringLength(1)]
        public string sebep { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? kararTarihi { get; set; }

        public int? kararSayisi { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? kesinlesmeTarihi { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? asilDogumTarihi { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? tashihDogumTarihi { get; set; }

        [StringLength(200)]
        public string asilAd { get; set; }

        [StringLength(200)]
        public string tashihAd { get; set; }

        [StringLength(200)]
        public string asilSoyad { get; set; }

        [StringLength(200)]
        public string tashihSoyad { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? gecerliDogumTarihi { get; set; }

        public string aciklama { get; set; }

        public int? gunSayisi { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? kurumOnayTarihi { get; set; }

        [StringLength(100)]
        public string kullaniciAd { get; set; }

        [StringLength(100)]
        public string sifre { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? zaman { get; set; }

        public int? FKPersonelBilgisiID { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }
    }
}
