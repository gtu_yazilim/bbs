namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.OSinavDosya")]
    public partial class OSinavDosya
    {
        [Key]
        [Column(Order = 0)]
        public int ID { get; set; }

        public int? SinavID { get; set; }

        public int? SoruID { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(255)]
        public string DosyaKey { get; set; }

        public virtual OSinav OSinav { get; set; }
    }
}
