namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRDiplomaUnvan")]
    public partial class OGRDiplomaUnvan
    {
        public int ID { get; set; }

        [Required]
        [StringLength(500)]
        public string VerilenUnvan { get; set; }

        public DateTime TarihKayit { get; set; }

        public DateTime TarihBaslangic { get; set; }

        public DateTime TarihBitis { get; set; }

        public int FKBirimID { get; set; }

        public DateTime? TarihSilinme { get; set; }

        public bool Silindi { get; set; }

        [StringLength(500)]
        public string EkTanim { get; set; }

        public int? EnumOgretimSeviye { get; set; }

        public virtual Birimler Birimler { get; set; }
    }
}
