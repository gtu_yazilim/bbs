namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("mek.MekanFotoV2")]
    public partial class MekanFotoV2
    {
        public int ID { get; set; }

        [Column(TypeName = "image")]
        [Required]
        public byte[] Foto { get; set; }

        public int FKMekanID { get; set; }

        public int Sira { get; set; }

        public bool Aktif { get; set; }

        [Required]
        [StringLength(50)]
        public string Kullanici { get; set; }

        public DateTime Zaman { get; set; }

        public DateTime? SGZaman { get; set; }

        public virtual MekanV2 MekanV2 { get; set; }
    }
}
