namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRTNMOkulOSYMKod")]
    public partial class OGRTNMOkulOSYMKod
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(500)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? InKod { get; set; }

        [StringLength(50)]
        public string Kod { get; set; }

        public int? YokID { get; set; }

        public int? EnumOgretimTur { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public int? EnumDil { get; set; }

        public int? EnumDerece { get; set; }

        public int? FKBolumPrBirimID { get; set; }

        public virtual Birimler Birimler { get; set; }
    }
}
