namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.WebBolumTemelAlan")]
    public partial class WebBolumTemelAlan
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }

        [StringLength(3)]
        public string Kod { get; set; }

        [StringLength(128)]
        public string Tanim { get; set; }

        [StringLength(128)]
        public string TanimEng { get; set; }
    }
}
