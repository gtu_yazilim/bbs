namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGREgitimBilgileri")]
    public partial class OGREgitimBilgileri
    {
        public int ID { get; set; }

        public int FKOgrenciID { get; set; }

        public int? FKLiseIlID { get; set; }

        [StringLength(500)]
        public string LiseAd { get; set; }

        public int? LiseMezYil { get; set; }

        public double? LisansYerlesmePuan { get; set; }

        public int? EnumPuanTip { get; set; }

        public int? FKLisansUnivID { get; set; }

        [StringLength(500)]
        public string LisansFak { get; set; }

        [StringLength(500)]
        public string LisansBol { get; set; }

        public int? LisansMezYil { get; set; }

        public double? LisansOrt { get; set; }

        public int? FKYLisansUnivID { get; set; }

        [StringLength(500)]
        public string YLisansFak { get; set; }

        [StringLength(500)]
        public string YLisansBol { get; set; }

        public int? YLisansMezYil { get; set; }

        public double? YLisansOrt { get; set; }

        public int? EnumYabDilTur { get; set; }

        public double? YabDilPuan { get; set; }

        public double? LesSayisal { get; set; }

        public double? LesSozel { get; set; }

        public double? LesEA { get; set; }

        public int? EnumLUPuanTip { get; set; }

        public int? EnumYoksisPuanTip { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public DateTime? Zaman { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public virtual Birimler Birimler { get; set; }

        public virtual Birimler Birimler1 { get; set; }

        public virtual TNMIl TNMIl { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
