namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("dosya.BelgeDogrulama")]
    public partial class BelgeDogrulama
    {
        public int ID { get; set; }

        public DateTime TarihKayit { get; set; }

        [Required]
        [StringLength(50)]
        public string Kullanici { get; set; }

        [Required]
        [StringLength(50)]
        public string IP { get; set; }

        [Required]
        [StringLength(50)]
        public string DogrulamaKodu { get; set; }

        [StringLength(200)]
        public string DosyaKey { get; set; }

        public DateTime? TarihKullanilma { get; set; }

        [StringLength(250)]
        public string Dogrulama2Key { get; set; }

        [StringLength(250)]
        public string Dogrulama2Value { get; set; }

        [StringLength(50)]
        public string KullanilanYer { get; set; }

        public bool Silindi { get; set; }
    }
}
