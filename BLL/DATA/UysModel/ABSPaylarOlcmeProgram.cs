namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.ABSPaylarOlcmeProgram")]
    public partial class ABSPaylarOlcmeProgram
    {
        public int ID { get; set; }

        public int? FKPaylarOlcmeID { get; set; }

        public int? FKProgramCiktiID { get; set; }

        public virtual ABSPaylarOlcme ABSPaylarOlcme { get; set; }

        public virtual BolumYeterlilik BolumYeterlilik { get; set; }
    }
}
