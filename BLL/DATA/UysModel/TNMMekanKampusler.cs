namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("mkn.TNMMekanKampusler")]
    public partial class TNMMekanKampusler
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TNMMekanKampusler()
        {
            MekanBinalar1 = new HashSet<MekanBinalar1>();
        }

        public int ID { get; set; }

        [StringLength(100)]
        public string Ad { get; set; }

        [StringLength(256)]
        public string Adres { get; set; }

        [Column(TypeName = "image")]
        public byte[] Foto { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MekanBinalar1> MekanBinalar1 { get; set; }
    }
}
