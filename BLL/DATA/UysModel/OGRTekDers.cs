namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRTekDers")]
    public partial class OGRTekDers
    {
        public int ID { get; set; }

        public int OgretimYili { get; set; }

        public int EnumDonem { get; set; }

        public int FKDersPlanAnaID { get; set; }

        public int FKDersPlanID { get; set; }

        public int? FKHocaID { get; set; }

        public int FKOgrenciID { get; set; }

        [Required]
        public string Makina { get; set; }

        public DateTime Zaman { get; set; }

        [Required]
        public string Kullanici { get; set; }

        public string SGKullanici { get; set; }

        public DateTime? SGZaman { get; set; }

        public bool Silindi { get; set; }

        public DateTime? YKKTarih { get; set; }

        [StringLength(50)]
        public string YKKNo { get; set; }

        public int? SinavNotu { get; set; }

        public int? EnumBasariNotu { get; set; }

        public int? SinavMekanID { get; set; }

        public string SinavMekani { get; set; }

        public DateTime? SinavZamani { get; set; }

        [StringLength(50)]
        public string BarkodNo { get; set; }

        public int? EnumTekDersDurum { get; set; }

        [StringLength(250)]
        public string Aciklama { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public virtual OGRDersPlan OGRDersPlan { get; set; }

        public virtual OGRDersPlanAna OGRDersPlanAna { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }
    }
}
