namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.TNMSporBranslari")]
    public partial class TNMSporBranslari
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TNMSporBranslari()
        {
            TNMSporOzGecmisPuan = new HashSet<TNMSporOzGecmisPuan>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(32)]
        public string BransAdi { get; set; }

        public bool AntrenorlulMu { get; set; }

        [StringLength(16)]
        public string Turu { get; set; }

        public int? Yil { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TNMSporOzGecmisPuan> TNMSporOzGecmisPuan { get; set; }
    }
}
