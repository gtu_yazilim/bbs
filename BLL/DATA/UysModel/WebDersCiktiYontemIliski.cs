namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.WebDersCiktiYontemIliski")]
    public partial class WebDersCiktiYontemIliski
    {
        [Key]
        [Column(Order = 0)]
        public int InKod { get; set; }

        public int? AnaDersPlanID { get; set; }

        public byte? CiktiNo { get; set; }

        public byte? YontemTur { get; set; }

        [StringLength(2)]
        public string YontemNo { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short Yil1 { get; set; }

        [StringLength(30)]
        public string Ekleyen { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? Zaman { get; set; }
    }
}
