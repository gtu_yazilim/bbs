namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRKYKOgrenimAktarma")]
    public partial class OGRKYKOgrenimAktarma
    {
        public int ID { get; set; }

        [Required]
        [StringLength(11)]
        public string TCNO { get; set; }

        public long? KlavuzKodu { get; set; }

        public long OgrenimKrediNo { get; set; }

        public DateTime? OgrenimBaslamaTarihi { get; set; }

        public DateTime? OgrenimBitisTarihi { get; set; }

        public int? KrediBitisYili { get; set; }

        public DateTime? OgrenimIptalTarihi { get; set; }

        public int? FKOgrenciID { get; set; }

        public bool? Aktarildi { get; set; }

        [StringLength(100)]
        public string Aciklama { get; set; }
    }
}
