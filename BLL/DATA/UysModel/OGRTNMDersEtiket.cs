namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRTNMDersEtiket")]
    public partial class OGRTNMDersEtiket
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OGRTNMDersEtiket()
        {
            OGRDersPlan = new HashSet<OGRDersPlan>();
            OGRTNMSaatBilgi = new HashSet<OGRTNMSaatBilgi>();
            OGRDersPlanEtiket = new HashSet<OGRDersPlanEtiket>();
        }

        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        [Required]
        [StringLength(300)]
        public string Etiket { get; set; }

        [StringLength(300)]
        public string EtiketEng { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        [StringLength(50)]
        public string Renk { get; set; }

        public int? AKTS { get; set; }

        public int? DSaat { get; set; }

        public int? USaat { get; set; }

        public int? Kredi { get; set; }

        [StringLength(5)]
        public string BolKodAd { get; set; }

        [StringLength(5)]
        public string DersKod { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRDersPlan> OGRDersPlan { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRTNMSaatBilgi> OGRTNMSaatBilgi { get; set; }

        public virtual ICollection<OGRDersPlanEtiket> OGRDersPlanEtiket { get; set; }
    }
}
