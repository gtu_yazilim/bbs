namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.BolumYeterlilikKategoriIliski")]
    public partial class BolumYeterlilikKategoriIliski
    {
        public int ID { get; set; }

        public int? FKYeterlilikID { get; set; }

        public int FKYeterlilikAltKategoriID { get; set; }

        public int? FKProgramBirimID { get; set; }

        public int Yil { get; set; }

        [StringLength(20)]
        public string Ekleyen { get; set; }

        public DateTime? Zaman { get; set; }

        public virtual BolumYeterlilik BolumYeterlilik { get; set; }

        public virtual Birimler Birimler { get; set; }

        public virtual YeterlilikAltKategori YeterlilikAltKategori { get; set; }
    }
}
