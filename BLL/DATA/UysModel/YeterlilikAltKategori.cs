namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.YeterlilikAltKategori")]
    public partial class YeterlilikAltKategori
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public YeterlilikAltKategori()
        {
            BolumYeterlilikKategoriIliski = new HashSet<BolumYeterlilikKategoriIliski>();
            DilYeterlilikAltKategori = new HashSet<DilYeterlilikAltKategori>();
            DuzeyYeterlilik = new HashSet<DuzeyYeterlilik>();
            TemelAlanYeterlilikKategoriIliski = new HashSet<TemelAlanYeterlilikKategoriIliski>();
        }

        public int ID { get; set; }

        public int? FKAnaKategoriID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BolumYeterlilikKategoriIliski> BolumYeterlilikKategoriIliski { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DilYeterlilikAltKategori> DilYeterlilikAltKategori { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DuzeyYeterlilik> DuzeyYeterlilik { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TemelAlanYeterlilikKategoriIliski> TemelAlanYeterlilikKategoriIliski { get; set; }

        public virtual YeterlilikAnaKategori YeterlilikAnaKategori { get; set; }
    }
}
