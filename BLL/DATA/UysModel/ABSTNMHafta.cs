namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.ABSTNMHafta")]
    public partial class ABSTNMHafta
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ABSTNMHafta()
        {
            ABSDevamTakipKontrol = new HashSet<ABSDevamTakipKontrol>();
        }

        public int ID { get; set; }

        [StringLength(10)]
        public string HaftaAd { get; set; }

        public DateTime? HaftaBaslangicTarih { get; set; }

        public DateTime? HaftaBitisTarih { get; set; }

        [StringLength(50)]
        public string Aciklama { get; set; }

        [StringLength(50)]
        public string Kullanlici { get; set; }

        public int? Yil { get; set; }

        public int? Donem { get; set; }

        public int HaftaNumarasi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSDevamTakipKontrol> ABSDevamTakipKontrol { get; set; }
    }
}
