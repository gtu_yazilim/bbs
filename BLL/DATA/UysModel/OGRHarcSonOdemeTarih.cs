namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRHarcSonOdemeTarih")]
    public partial class OGRHarcSonOdemeTarih
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(500)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public int? InKod { get; set; }

        public DateTime? SonOdemeTarihi { get; set; }

        public int? Yil { get; set; }

        public int? EnumDonem { get; set; }

        public int? EnumOgretimTur { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public int? FKBolumPrBirimID { get; set; }

        public virtual Birimler Birimler { get; set; }
    }
}
