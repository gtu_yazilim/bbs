namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGREnstituBasvuruBelgeler")]
    public partial class OGREnstituBasvuruBelgeler
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public int ID { get; set; }
        public string TCKimlikNo { get; set; }
        public string DosyaAdi { get; set; }
        public string DosyaUrl { get; set; }
        public int? Yil { get; set; }
        public int? Donem { get; set; }
        public string DosyaTipi { get; set; }
        public DateTime? KayitTarihi { get; set; }
        public bool? OnayDurumu { get; set; }
        public int? basvuruSayisi { get; set; }
    }
}
