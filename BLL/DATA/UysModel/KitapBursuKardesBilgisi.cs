namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("basvuru.KitapBursuKardesBilgisi")]
    public partial class KitapBursuKardesBilgisi
    {
        public int ID { get; set; }

        public int FKKitapBursuID { get; set; }

        [StringLength(500)]
        public string AdSoyad { get; set; }

        public DateTime? TarihDogum { get; set; }

        public int EgitimDurumu { get; set; }

        public int EgitimSeviye { get; set; }

        public virtual KitapBursu KitapBursu { get; set; }
    }
}
