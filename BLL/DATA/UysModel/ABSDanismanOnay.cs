namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.ABSDanismanOnay")]
    public partial class ABSDanismanOnay
    {
        public int ID { get; set; }

        public DateTime Tarih { get; set; }

        public int FKDanismanID { get; set; }

        public int FKOgrenciID { get; set; }

        public int Yil { get; set; }

        public int Donem { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }
    }
}
