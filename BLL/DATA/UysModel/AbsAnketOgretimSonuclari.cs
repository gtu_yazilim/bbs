namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("obs.AbsAnketOgretimSonuclari")]
    public partial class AbsAnketOgretimSonuclari
    {
        public int ID { get; set; }

        public int? FK_GrupID { get; set; }

        public int? Yil { get; set; }

        public int? s1_sifir { get; set; }

        public int? s1_bir { get; set; }

        public int? s1_iki { get; set; }

        public int? s1_uc { get; set; }

        public int? s1_dort { get; set; }

        public int? s1_bes { get; set; }

        public double? s1_dersOrt { get; set; }

        public double? s1_uniOrt { get; set; }

        public double? s1_bolumOrt { get; set; }

        public double? s1_fakOrt { get; set; }

        public int? s2_sifir { get; set; }

        public int? s2_bir { get; set; }

        public int? s2_iki { get; set; }

        public int? s2_uc { get; set; }

        public int? s2_dort { get; set; }

        public int? s2_bes { get; set; }

        public double? s2_dersOrt { get; set; }

        public double? s2_uniOrt { get; set; }

        public double? s2_bolumOrt { get; set; }

        public double? s2_fakOrt { get; set; }

        public int? s3_sifir { get; set; }

        public int? s3_bir { get; set; }

        public int? s3_iki { get; set; }

        public int? s3_uc { get; set; }

        public int? s3_dort { get; set; }

        public int? s3_bes { get; set; }

        public double? s3_dersOrt { get; set; }

        public double? s3_uniOrt { get; set; }

        public double? s3_bolumOrt { get; set; }

        public double? s3_fakOrt { get; set; }

        public int? s4_sifir { get; set; }

        public int? s4_bir { get; set; }

        public int? s4_iki { get; set; }

        public int? s4_uc { get; set; }

        public int? s4_dort { get; set; }

        public int? s4_bes { get; set; }

        public double? s4_dersOrt { get; set; }

        public double? s4_uniOrt { get; set; }

        public double? s4_bolumOrt { get; set; }

        public double? s4_fakOrt { get; set; }

        public int? s5_sifir { get; set; }

        public int? s5_bir { get; set; }

        public int? s5_iki { get; set; }

        public int? s5_uc { get; set; }

        public int? s5_dort { get; set; }

        public int? s5_bes { get; set; }

        public double? s5_dersOrt { get; set; }

        public double? s5_uniOrt { get; set; }

        public double? s5_bolumOrt { get; set; }

        public double? s5_fakOrt { get; set; }

        public int? s6_sifir { get; set; }

        public int? s6_bir { get; set; }

        public int? s6_iki { get; set; }

        public int? s6_uc { get; set; }

        public int? s6_dort { get; set; }

        public int? s6_bes { get; set; }

        public double? s6_dersOrt { get; set; }

        public double? s6_uniOrt { get; set; }

        public double? s6_bolumOrt { get; set; }

        public double? s6_fakOrt { get; set; }

        public int? s7_sifir { get; set; }

        public int? s7_bir { get; set; }

        public int? s7_iki { get; set; }

        public int? s7_uc { get; set; }

        public int? s7_dort { get; set; }

        public int? s7_bes { get; set; }

        public double? s7_dersOrt { get; set; }

        public double? s7_uniOrt { get; set; }

        public double? s7_bolumOrt { get; set; }

        public double? s7_fakOrt { get; set; }

        public int? s8_sifir { get; set; }

        public int? s8_bir { get; set; }

        public int? s8_iki { get; set; }

        public int? s8_uc { get; set; }

        public int? s8_dort { get; set; }

        public int? s8_bes { get; set; }

        public double? s8_dersOrt { get; set; }

        public double? s8_uniOrt { get; set; }

        public double? s8_bolumOrt { get; set; }

        public double? s8_fakOrt { get; set; }

        public int? s9_sifir { get; set; }

        public int? s9_bir { get; set; }

        public int? s9_iki { get; set; }

        public int? s9_uc { get; set; }

        public int? s9_dort { get; set; }

        public int? s9_bes { get; set; }

        public double? s9_dersOrt { get; set; }

        public double? s9_uniOrt { get; set; }

        public double? s9_bolumOrt { get; set; }

        public double? s9_fakOrt { get; set; }

        public int? s10_sifir { get; set; }

        public int? s10_bir { get; set; }

        public int? s10_iki { get; set; }

        public int? s10_uc { get; set; }

        public int? s10_dort { get; set; }

        public int? s10_bes { get; set; }

        public double? s10_dersOrt { get; set; }

        public double? s10_uniOrt { get; set; }

        public double? s10_bolumOrt { get; set; }

        public double? s10_fakOrt { get; set; }
    }
}
