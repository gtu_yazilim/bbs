namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.Menuler")]
    public partial class Menuler
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Menuler()
        {
            YetkiKullaniciMenu = new HashSet<YetkiKullaniciMenu>();
            YetkiRolMenu = new HashSet<YetkiRolMenu>();
        }

        public int ID { get; set; }

        [StringLength(50)]
        public string ControllerAd { get; set; }

        [StringLength(50)]
        public string ActionAd { get; set; }

        [StringLength(50)]
        public string MenuAd { get; set; }

        [StringLength(50)]
        public string ParameterAd { get; set; }

        [StringLength(50)]
        public string ParemeterDeger { get; set; }

        public bool AktifMi { get; set; }

        public bool HizliErisimdeGorunsun { get; set; }

        public bool YanMenudeGorunsun { get; set; }

        public int? UstID { get; set; }

        [StringLength(50)]
        public string CssClass { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<YetkiKullaniciMenu> YetkiKullaniciMenu { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<YetkiRolMenu> YetkiRolMenu { get; set; }
    }
}
