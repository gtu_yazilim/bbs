namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("bnk.OnlineHarcOdemeIptalleri")]
    public partial class OnlineHarcOdemeIptalleri
    {
        public int ID { get; set; }

        public int FKOdemeID { get; set; }

        public double Miktar { get; set; }

        public DateTime IptalTarihi { get; set; }

        public DateTime Zaman { get; set; }
    }
}
