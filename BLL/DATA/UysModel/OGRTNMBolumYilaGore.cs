namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRTNMBolumYilaGore")]
    public partial class OGRTNMBolumYilaGore
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(500)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? InKod { get; set; }

        [StringLength(50)]
        public string Kod { get; set; }

        [StringLength(500)]
        public string Ad { get; set; }

        [StringLength(500)]
        public string AdEng { get; set; }

        [StringLength(300)]
        public string KisaAd { get; set; }

        [StringLength(300)]
        public string KisaAdEng { get; set; }

        public int? Yil { get; set; }

        [StringLength(250)]
        public string Unvan { get; set; }

        [StringLength(250)]
        public string UnvanEng { get; set; }

        [StringLength(500)]
        public string AnaBilimDali { get; set; }

        [StringLength(500)]
        public string AnaBilimDaliEng { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public int? FKFakulteBirimID { get; set; }

        public virtual Birimler Birimler { get; set; }
    }
}
