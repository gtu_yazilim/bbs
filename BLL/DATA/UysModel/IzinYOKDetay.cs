namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.IzinYOKDetay")]
    public partial class IzinYOKDetay
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        public string Kullanici { get; set; }

        public string Makina { get; set; }

        public int EKadroDerece { get; set; }

        public int YKadroDerece { get; set; }

        [StringLength(500)]
        public string Aciklama { get; set; }

        public bool Durum { get; set; }

        public int FKIzinYOKID { get; set; }

        public int FKEKadroBilgisiID { get; set; }

        public int? FKYKadroBilgisiID { get; set; }

        public int? FKPersonelBilgisiID { get; set; }

        public int? FKEKadroUnvaniID { get; set; }

        public int? FKYKadroUnvaniID { get; set; }

        public int? FKEUlkeID { get; set; }

        public int? FKEUniversiteID { get; set; }

        public int? FKEFakulteID { get; set; }

        public int? FKEBolumID { get; set; }

        public int? FKEAnaBilimDaliID { get; set; }

        public int? FKYUlkeID { get; set; }

        public int? FKYUniversiteID { get; set; }

        public int? FKYFakulteID { get; set; }

        public int? FKYBolumID { get; set; }

        public int? FKYAnaBilimDaliID { get; set; }

        public virtual Birimler Birimler { get; set; }

        public virtual Birimler Birimler1 { get; set; }

        public virtual Birimler Birimler2 { get; set; }

        public virtual Birimler Birimler3 { get; set; }

        public virtual Birimler Birimler4 { get; set; }

        public virtual Birimler Birimler5 { get; set; }

        public virtual Birimler Birimler6 { get; set; }

        public virtual Birimler Birimler7 { get; set; }

        public virtual IzinYOK IzinYOK { get; set; }

        public virtual TNMKadroUnvani TNMKadroUnvani { get; set; }

        public virtual KadroBilgisi KadroBilgisi { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }

        public virtual TNMKadroUnvani TNMKadroUnvani1 { get; set; }
    }
}
