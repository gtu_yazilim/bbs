namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("basvuru.MuafiyetSinavBatchKey")]
    public partial class MuafiyetSinavBatchKey
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MuafiyetSinavBatchKey()
        {
            MuafiyetSinavTIN = new HashSet<MuafiyetSinavTIN>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(250)]
        public string BatchKey { get; set; }

        public int BatchNumber { get; set; }

        public DateTime TarihKullanmaBitis { get; set; }

        [StringLength(750)]
        public string UrunAdi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MuafiyetSinavTIN> MuafiyetSinavTIN { get; set; }
    }
}
