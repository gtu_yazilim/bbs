namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.IdariGorev")]
    public partial class IdariGorev
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public IdariGorev()
        {
            Izin = new HashSet<Izin>();
            Izin1 = new HashSet<Izin>();
        }

        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(100)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? PerKod { get; set; }

        public DateTime? BaslangicTarihi { get; set; }

        public DateTime? BitisTarihi { get; set; }

        public short? KullaniciKod { get; set; }

        public DateTime? IslemTarihi { get; set; }

        public int? ENUMAtamaTuru { get; set; }

        public int FKPersonelBilgisiID { get; set; }

        public int? FKGorevUnvaniID { get; set; }

        public int? FKIdariGorevID { get; set; }

        public int? FKUlkeID { get; set; }

        public int? FKUniversiteID { get; set; }

        public int? FKFakulteID { get; set; }

        public int? FKBolumID { get; set; }

        public int? FKAnaBilimDaliID { get; set; }

        public int? BirimID { get; set; }

        public int? in_kod { get; set; }

        public virtual Birimler Birimler { get; set; }

        public virtual Birimler Birimler1 { get; set; }

        public virtual Birimler Birimler2 { get; set; }

        public virtual Birimler Birimler3 { get; set; }

        public virtual TNMGorevUnvani TNMGorevUnvani { get; set; }

        public virtual TNMIdariGorev TNMIdariGorev { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Izin> Izin { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Izin> Izin1 { get; set; }
    }
}
