namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.DersCiktiYontemIliski")]
    public partial class DersCiktiYontemIliski
    {
        public int ID { get; set; }

        public int? FKCiktiID { get; set; }

        public int? FKCiktiYontemID { get; set; }

        public short Yil { get; set; }

        [StringLength(30)]
        public string Ekleyen { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? Zaman { get; set; }

        public virtual DersCikti DersCikti { get; set; }

        public virtual DersCiktiYontem DersCiktiYontem { get; set; }
    }
}
