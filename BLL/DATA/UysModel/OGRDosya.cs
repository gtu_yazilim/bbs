namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRDosya")]
    public partial class OGRDosya
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OGRDosya()
        {
            OGRBelgeSayi = new HashSet<OGRBelgeSayi>();
        }

        public int ID { get; set; }

        public DateTime TarihKayit { get; set; }

        [Required]
        [StringLength(200)]
        public string DosyaKey { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRBelgeSayi> OGRBelgeSayi { get; set; }
    }
}
