namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.ABSOrtakDegerlendirmeGrup")]
    public partial class ABSOrtakDegerlendirmeGrup
    {
        [Key]
        [Column(Order = 0)]
        public int ID { get; set; }
        
        [Column(Order = 1)]
        public DateTime TarihKayit { get; set; }
        
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FKDersGrupID { get; set; }
        
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FKOrtakDegerlendirmeID { get; set; }
        
        [Column(Order = 4)]
        [StringLength(50)]
        public string Kullanici { get; set; }
        
        [Column(Order = 5)]
        public bool Silindi { get; set; }

        public virtual ABSOrtakDegerlendirme ABSOrtakDegerlendirme { get; set; }
        public virtual OGRDersGrup OGRDersGrup { get; set; }

    }
}
