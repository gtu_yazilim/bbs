namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGROgrenciOrtalama")]
    public partial class OGROgrenciOrtalama
    {
        public int ID { get; set; }

        public int FKOgrenciID { get; set; }

        public DateTime TarihKayit { get; set; }

        public DateTime TarihGuncelleme { get; set; }

        public double ToplamAKTS { get; set; }

        public double ToplamAgirlik { get; set; }

        public double GenelOrtalama { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
