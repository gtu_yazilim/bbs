namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.PersonelBilgisi")]
    public partial class PersonelBilgisi
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PersonelBilgisi()
        {
            ABSDanismanOnay = new HashSet<ABSDanismanOnay>();
            ABSDegerlendirme = new HashSet<ABSDegerlendirme>();
            ABSDevamTakipKontrol = new HashSet<ABSDevamTakipKontrol>();
            ABSDokuman = new HashSet<ABSDokuman>();
            ABSGrupCalisma = new HashSet<ABSGrupCalisma>();
            ABSMesaj = new HashSet<ABSMesaj>();
            ABSOgrenciDanismanlik = new HashSet<ABSOgrenciDanismanlik>();
            ABSOgrenciIleGorusme = new HashSet<ABSOgrenciIleGorusme>();
            ABSOgrenciyeMesaj = new HashSet<ABSOgrenciyeMesaj>();
            ABSOrtakDegerlendirme = new HashSet<ABSOrtakDegerlendirme>();
            ABSPaylar = new HashSet<ABSPaylar>();
            ABSRolYetki = new HashSet<ABSRolYetki>();
            ABSYetkiliKullanici = new HashSet<ABSYetkiliKullanici>();
            DersIcerikPuani = new HashSet<DersIcerikPuani>();
            OgrenciDersCevap = new HashSet<OgrenciDersCevap>();
            UKKullanicilar = new HashSet<UKKullanicilar>();
            DegisimKayit = new HashSet<DegisimKayit>();
            DersKoordinator = new HashSet<DersKoordinator>();
            AdresBilgisi = new HashSet<AdresBilgisi>();
            TelefonBilgisi = new HashSet<TelefonBilgisi>();
            MekanKisiV2 = new HashSet<MekanKisiV2>();
            MekanV2 = new HashSet<MekanV2>();
            BarinmaKisiler = new HashSet<BarinmaKisiler>();
            MekanKisiler = new HashSet<MekanKisiler>();
            MekanKisiler1 = new HashSet<MekanKisiler>();
            OGRDanismanGorusme = new HashSet<OGRDanismanGorusme>();
            OGRDanismanMesaj = new HashSet<OGRDanismanMesaj>();
            OGRDersGrupHoca = new HashSet<OGRDersGrupHoca>();
            OGREnstituDanisman = new HashSet<OGREnstituDanisman>();
            OGREnstituSeminer = new HashSet<OGREnstituSeminer>();
            OGREnstituSeminer1 = new HashSet<OGREnstituSeminer>();
            OGREnstituSeminer2 = new HashSet<OGREnstituSeminer>();
            OGREnstituTezIzlemeKomitesi = new HashSet<OGREnstituTezIzlemeKomitesi>();
            OGREnstituTezIzlemeKomitesi1 = new HashSet<OGREnstituTezIzlemeKomitesi>();
            OGREnstituTezIzlemeKomitesi2 = new HashSet<OGREnstituTezIzlemeKomitesi>();
            OGREnstituTOS = new HashSet<OGREnstituTOS>();
            OGREnstituYeterlik = new HashSet<OGREnstituYeterlik>();
            OGREnstituYeterlik1 = new HashSet<OGREnstituYeterlik>();
            OGREnstituYeterlik2 = new HashSet<OGREnstituYeterlik>();
            OGREnstituYeterlik3 = new HashSet<OGREnstituYeterlik>();
            OGREnstituYeterlik4 = new HashSet<OGREnstituYeterlik>();
            OGRKimlik = new HashSet<OGRKimlik>();
            OGRMazeret = new HashSet<OGRMazeret>();
            OGRTekDers = new HashSet<OGRTekDers>();
            OGRYazilmaDanismanOnay = new HashSet<OGRYazilmaDanismanOnay>();
            SinavGorevli = new HashSet<SinavGorevli>();
            AsaletTasdikiDetay = new HashSet<AsaletTasdikiDetay>();
            ASVPersonel = new HashSet<ASVPersonel>();
            ASVPersonelLog = new HashSet<ASVPersonelLog>();
            AtanmaSureleriDetay = new HashSet<AtanmaSureleriDetay>();
            BYOK = new HashSet<BYOK>();
            DisGorevlendirme = new HashSet<DisGorevlendirme>();
            EgitimBilgisi = new HashSet<EgitimBilgisi>();
            FiiliGorevYeri = new HashSet<FiiliGorevYeri>();
            Hareket = new HashSet<Hareket>();
            IdariGorev = new HashSet<IdariGorev>();
            Izin = new HashSet<Izin>();
            Izin1 = new HashSet<Izin>();
            Izin2 = new HashSet<Izin>();
            IzinKalan = new HashSet<IzinKalan>();
            IzinYOKDetay = new HashSet<IzinYOKDetay>();
            IzinYOKDoluDetay = new HashSet<IzinYOKDoluDetay>();
            KadroBilgisi = new HashSet<KadroBilgisi>();
            KurumDisiGorevlendirme = new HashSet<KurumDisiGorevlendirme>();
            LojmanBilgisi = new HashSet<LojmanBilgisi>();
            OgrenimBilgisi = new HashSet<OgrenimBilgisi>();
            Kullanici1 = new HashSet<Kullanici>();
            OGREnstituYeterlilikKomite = new HashSet<OGREnstituYeterlilikKomite>();
            OGREnstituYeterlilikKomite1 = new HashSet<OGREnstituYeterlilikKomite>();
            OGREnstituYeterlilikKomite2 = new HashSet<OGREnstituYeterlilikKomite>();
            OGREnstituYeterlilikKomite3 = new HashSet<OGREnstituYeterlilikKomite>();
            OGREnstituYeterlilikKomite4 = new HashSet<OGREnstituYeterlilikKomite>();
            Sendika = new HashSet<Sendika>();
            SGKHizmetAcikSure = new HashSet<SGKHizmetAcikSure>();
            SGKHizmetAskerlik = new HashSet<SGKHizmetAskerlik>();
            SGKHizmetBirlestirme = new HashSet<SGKHizmetBirlestirme>();
            SGKHizmetBorclanma = new HashSet<SGKHizmetBorclanma>();
            SGKHizmetCetveli = new HashSet<SGKHizmetCetveli>();
            SGKHizmetIHS = new HashSet<SGKHizmetIHS>();
            SGKHizmetKurs = new HashSet<SGKHizmetKurs>();
            SGKHizmetMahkeme = new HashSet<SGKHizmetMahkeme>();
            SGKHizmetNufus = new HashSet<SGKHizmetNufus>();
            SGKHizmetOkul = new HashSet<SGKHizmetOkul>();
            SGKHizmetTazminat = new HashSet<SGKHizmetTazminat>();
            SGKHizmetUnvan = new HashSet<SGKHizmetUnvan>();
            TNMKullanicilar = new HashSet<TNMKullanicilar>();
            YabanciDil = new HashSet<YabanciDil>();
        }

        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(100)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        [StringLength(10)]
        public string SicilNo { get; set; }

        [Required]
        [StringLength(11)]
        public string TC { get; set; }

        [StringLength(50)]
        public string KullaniciAdi { get; set; }

        [StringLength(50)]
        public string Ad { get; set; }

        [StringLength(50)]
        public string Soyad { get; set; }

        [StringLength(50)]
        public string UnvanAd { get; set; }

        [StringLength(512)]
        public string GorevAd { get; set; }

        [StringLength(100)]
        public string GorevFakAd { get; set; }

        [StringLength(100)]
        public string GorevBolAd { get; set; }

        public int? EnumPersonelTipi { get; set; }

        public int? EnumPersonelDurum { get; set; }

        public int? FK_GorevFakID { get; set; }

        public int? FK_GorevBolID { get; set; }

        public int? FK_GorevAbdID { get; set; }

        public int? FKGorevBirimID { get; set; }

        public int? FKUniversiteAtanmaSekliID { get; set; }

        public bool? ABSGirisIzni { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        [StringLength(12)]
        public string EmekliSicilNo { get; set; }

        [StringLength(20)]
        public string SSKNo { get; set; }

        public int? Sebep { get; set; }

        public int? SakatlikYuzdesi { get; set; }

        public int? ENUMSakatlik { get; set; }

        [StringLength(1)]
        public string YetkiSeviyesi { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? MemuriyetBaslamaTarihi { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? KurumaBaslamaTarihi { get; set; }

        public int? ENUMKurumdanAyrilmaNedeni { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? KurumdanAyrilmaTarihi { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? AdaylikBaslamaTarihi { get; set; }

        public int? AdaylikSuresi { get; set; }

        public int? ENUMAsalet { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? AsaletTarihi { get; set; }

        public int? AtanmaSuresiYil { get; set; }

        public int? AtanmaSuresiAy { get; set; }

        public int? AtanmaSuresiGun { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? AtanmaSureBitisTarihi { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? IlkAtanmaTarihi { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? SonAtanmaTarihi { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? TeknikHizmetlerAtanma { get; set; }

        public int? KefaletKesintisi { get; set; }

        [StringLength(20)]
        public string BankaHesapNo { get; set; }

        [StringLength(50)]
        public string VergiDairesi { get; set; }

        [StringLength(12)]
        public string VergiNo { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? EvlilikTarihi { get; set; }

        [StringLength(3)]
        public string EvlilikCuzdanSeri { get; set; }

        [StringLength(12)]
        public string EvlilikCuzdanNo { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? EvlilikCuzdanVerilisTarihi { get; set; }

        [StringLength(250)]
        public string EsAciklama { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? ProfIlkAtanmaTarihi { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? DocIlkAtanmaTarihi { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? YarDocIlkAtanmaTarihi { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? UAKDocentlikTarihi { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? DoktoraTarihi { get; set; }

        public int? PerKod { get; set; }

        public int? ENUMKategori { get; set; }

        public int? ENUMKanunTabiyeti { get; set; }

        public int? ENUMEsCalismaDurumu { get; set; }

        public int? FKUnvanID { get; set; }

        public int? FKAkademikUnvanID { get; set; }

        public int FKKisiID { get; set; }

        [StringLength(250)]
        public string Email { get; set; }

        [StringLength(100)]
        public string GorevABDAd { get; set; }

        public int? FKKadroUnvaniID { get; set; }

        public int? ENUMAtanmaSekli { get; set; }

        [StringLength(50)]
        public string UzmanlikAlani { get; set; }

        public bool? WebteFotograf { get; set; }

        public bool? WebteGoster { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSDanismanOnay> ABSDanismanOnay { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSDegerlendirme> ABSDegerlendirme { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSDevamTakipKontrol> ABSDevamTakipKontrol { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSDokuman> ABSDokuman { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSGrupCalisma> ABSGrupCalisma { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSMesaj> ABSMesaj { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSOgrenciDanismanlik> ABSOgrenciDanismanlik { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSOgrenciIleGorusme> ABSOgrenciIleGorusme { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSOgrenciyeMesaj> ABSOgrenciyeMesaj { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSOrtakDegerlendirme> ABSOrtakDegerlendirme { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSPaylar> ABSPaylar { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSRolYetki> ABSRolYetki { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSYetkiliKullanici> ABSYetkiliKullanici { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DersIcerikPuani> DersIcerikPuani { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OgrenciDersCevap> OgrenciDersCevap { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UKKullanicilar> UKKullanicilar { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DegisimKayit> DegisimKayit { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DersKoordinator> DersKoordinator { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AdresBilgisi> AdresBilgisi { get; set; }

        public virtual Birimler Birimler { get; set; }

        public virtual Birimler Birimler1 { get; set; }

        public virtual Birimler Birimler2 { get; set; }

        public virtual Kisi Kisi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TelefonBilgisi> TelefonBilgisi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MekanKisiV2> MekanKisiV2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MekanV2> MekanV2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BarinmaKisiler> BarinmaKisiler { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MekanKisiler> MekanKisiler { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MekanKisiler> MekanKisiler1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRDanismanGorusme> OGRDanismanGorusme { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRDanismanMesaj> OGRDanismanMesaj { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRDersGrupHoca> OGRDersGrupHoca { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituDanisman> OGREnstituDanisman { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituSeminer> OGREnstituSeminer { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituSeminer> OGREnstituSeminer1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituSeminer> OGREnstituSeminer2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituTezIzlemeKomitesi> OGREnstituTezIzlemeKomitesi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituTezIzlemeKomitesi> OGREnstituTezIzlemeKomitesi1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituTezIzlemeKomitesi> OGREnstituTezIzlemeKomitesi2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituTOS> OGREnstituTOS { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituYeterlik> OGREnstituYeterlik { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituYeterlik> OGREnstituYeterlik1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituYeterlik> OGREnstituYeterlik2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituYeterlik> OGREnstituYeterlik3 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituYeterlik> OGREnstituYeterlik4 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRKimlik> OGRKimlik { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRMazeret> OGRMazeret { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRTekDers> OGRTekDers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRYazilmaDanismanOnay> OGRYazilmaDanismanOnay { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SinavGorevli> SinavGorevli { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AsaletTasdikiDetay> AsaletTasdikiDetay { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ASVPersonel> ASVPersonel { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ASVPersonelLog> ASVPersonelLog { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AtanmaSureleriDetay> AtanmaSureleriDetay { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BYOK> BYOK { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DisGorevlendirme> DisGorevlendirme { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EgitimBilgisi> EgitimBilgisi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FiiliGorevYeri> FiiliGorevYeri { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Hareket> Hareket { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IdariGorev> IdariGorev { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Izin> Izin { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Izin> Izin1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Izin> Izin2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IzinKalan> IzinKalan { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IzinYOKDetay> IzinYOKDetay { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IzinYOKDoluDetay> IzinYOKDoluDetay { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<KadroBilgisi> KadroBilgisi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<KurumDisiGorevlendirme> KurumDisiGorevlendirme { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LojmanBilgisi> LojmanBilgisi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OgrenimBilgisi> OgrenimBilgisi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Kullanici> Kullanici1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituYeterlilikKomite> OGREnstituYeterlilikKomite { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituYeterlilikKomite> OGREnstituYeterlilikKomite1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituYeterlilikKomite> OGREnstituYeterlilikKomite2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituYeterlilikKomite> OGREnstituYeterlilikKomite3 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituYeterlilikKomite> OGREnstituYeterlilikKomite4 { get; set; }

        public virtual SBTAtanmaSekli SBTAtanmaSekli { get; set; }

        public virtual TNMKadroUnvani TNMKadroUnvani { get; set; }

        public virtual TNMUnvan TNMUnvan { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Sendika> Sendika { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SGKHizmetAcikSure> SGKHizmetAcikSure { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SGKHizmetAskerlik> SGKHizmetAskerlik { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SGKHizmetBirlestirme> SGKHizmetBirlestirme { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SGKHizmetBorclanma> SGKHizmetBorclanma { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SGKHizmetCetveli> SGKHizmetCetveli { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SGKHizmetIHS> SGKHizmetIHS { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SGKHizmetKurs> SGKHizmetKurs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SGKHizmetMahkeme> SGKHizmetMahkeme { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SGKHizmetNufus> SGKHizmetNufus { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SGKHizmetOkul> SGKHizmetOkul { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SGKHizmetTazminat> SGKHizmetTazminat { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SGKHizmetUnvan> SGKHizmetUnvan { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TNMKullanicilar> TNMKullanicilar { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<YabanciDil> YabanciDil { get; set; }
    }
}
