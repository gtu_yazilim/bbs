namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ytk.YetkiLog")]
    public partial class YetkiLog
    {
        public int ID { get; set; }

        public DateTime Tarih { get; set; }

        [Required]
        public string LogData { get; set; }
    }
}
