namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRIntibak")]
    public partial class OGRIntibak
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OGRIntibak()
        {
            OGRIntibakBirim = new HashSet<OGRIntibakBirim>();
            OGRIntibakDers = new HashSet<OGRIntibakDers>();
        }

        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(500)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        [StringLength(500)]
        public string Aciklama { get; set; }

        public int? KriterGirisYiliMin { get; set; }

        public int? KriterGirisYiliMax { get; set; }

        public int? EnumIntibakSart { get; set; }

        public int EnumIntibakTipi { get; set; }

        public int? EnumIntibakSonuc { get; set; }

        public int? FKDersPlanAnaID_A { get; set; }

        public int? FKDersPlanAnaID_B { get; set; }

        public int? FKDersPlanAnaID_C { get; set; }

        public bool Uygulandi { get; set; }

        public DateTime? UygulamaTarihi { get; set; }

        public int Yil { get; set; }

        public int EnumDonem { get; set; }

        public virtual OGRDersPlanAna OGRDersPlanAna { get; set; }

        public virtual OGRDersPlanAna OGRDersPlanAna1 { get; set; }

        public virtual OGRDersPlanAna OGRDersPlanAna2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRIntibakBirim> OGRIntibakBirim { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRIntibakDers> OGRIntibakDers { get; set; }
    }
}
