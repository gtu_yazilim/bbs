namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.OSinavOturum")]
    public partial class OSinavOturum
    {
        [Key]
        [Column(Order = 0)]
        public int OturumID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SinavID { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int OgrenciID { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SinavOgrenciID { get; set; }

        [Key]
        [Column(Order = 4)]
        public byte Durum { get; set; }

        public DateTime? Baslangic { get; set; }

        public DateTime? Bitis { get; set; }

        public bool? EndAuto { get; set; }

        public byte? EndType { get; set; }

        [Key]
        [Column(Order = 5)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SoruSayi { get; set; }

        [Key]
        [Column(Order = 6)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DogruSayi { get; set; }

        [Key]
        [Column(Order = 7)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int YanlisSayi { get; set; }

        public decimal? Notu { get; set; }

        public int? DUId { get; set; }

        public DateTime? DDate { get; set; }

        [Key]
        [Column(Order = 8)]
        public bool Silindi { get; set; }
    }
}
