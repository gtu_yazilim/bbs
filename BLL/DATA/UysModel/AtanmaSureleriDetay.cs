namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.AtanmaSureleriDetay")]
    public partial class AtanmaSureleriDetay
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        public string Kullanici { get; set; }

        public string Makina { get; set; }

        public DateTime? IlkAtanmaTarihi { get; set; }

        public DateTime? SonAtanmaTarihi { get; set; }

        public DateTime? AtanmaSuresiBitisTarihi { get; set; }

        public DateTime? YeniAtanmaSuresiBitisTarihi { get; set; }

        public int? FKAtanmaSureleriID { get; set; }

        public int FKPersonelBilgisiID { get; set; }

        public int FKKadroUnvaniID { get; set; }

        public int? FKFGYFakulteID { get; set; }

        public int? FKFGYBolumID { get; set; }

        public int? ENUMDetayIslem { get; set; }

        public bool Silindi { get; set; }

        public string Aciklama { get; set; }

        public virtual Birimler Birimler { get; set; }

        public virtual Birimler Birimler1 { get; set; }

        public virtual AtanmaSureleri AtanmaSureleri { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }

        public virtual TNMKadroUnvani TNMKadroUnvani { get; set; }
    }
}
