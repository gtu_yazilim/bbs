namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.Izin")]
    public partial class Izin
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        public string Kullanici { get; set; }

        public string Makina { get; set; }

        public int? AitOlduguYil { get; set; }

        public DateTime? AyrilisTarihi { get; set; }

        public DateTime? DonusTarihi { get; set; }

        public DateTime? IstemTarihi { get; set; }

        public DateTime? OnayTarihi { get; set; }

        public int? GecenYildan { get; set; }

        public int? CariYil { get; set; }

        public int? KullanilanIzinToplami { get; set; }

        [StringLength(250)]
        public string Aciklama { get; set; }

        [StringLength(250)]
        public string IzindekiAdresi { get; set; }

        public int? ENUMIzinTuru { get; set; }

        public int? FKPersonelBilgisiID { get; set; }

        public int? FKUnvanID { get; set; }

        public int? FKBirimID { get; set; }

        public int? FKIzinVerenYetkiliID { get; set; }

        public int? FKIzinVerenYetkiliUnvanID { get; set; }

        public int? FKIzinVerenYetkiliIdariGorevID { get; set; }

        public int? FKOnaylayanID { get; set; }

        public int? FKOnaylayanUnvanID { get; set; }

        public int? FKOnaylayanIdariGorevID { get; set; }

        public virtual Birimler Birimler { get; set; }

        public virtual IdariGorev IdariGorev { get; set; }

        public virtual IdariGorev IdariGorev1 { get; set; }

        public virtual TNMKadroUnvani TNMKadroUnvani { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi1 { get; set; }

        public virtual TNMKadroUnvani TNMKadroUnvani1 { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi2 { get; set; }

        public virtual TNMKadroUnvani TNMKadroUnvani2 { get; set; }
    }
}
