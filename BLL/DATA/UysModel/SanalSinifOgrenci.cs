namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.SanalSinifOgrenci")]
    public partial class SanalSinifOgrenci
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SSinifID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int OgrenciID { get; set; }

        public DateTime? KatilimIlk { get; set; }

        public DateTime? KatilimSon { get; set; }

        public int? CanliIzleme { get; set; }

        public int? TekrarIzleme { get; set; }

        [StringLength(250)]
        public string IzlemeKey { get; set; }
    }
}
