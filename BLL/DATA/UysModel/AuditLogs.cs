namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class AuditLogs
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AuditLogs()
        {
            LogMetadata = new HashSet<LogMetadata>();
        }

        [Key]
        public long AuditLogId { get; set; }

        public string UserName { get; set; }

        public DateTime EventDateUTC { get; set; }

        public int EventType { get; set; }

        [Required]
        [StringLength(512)]
        public string TypeFullName { get; set; }

        [Required]
        [StringLength(256)]
        public string RecordId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LogMetadata> LogMetadata { get; set; }
    }
}
