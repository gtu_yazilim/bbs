namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRMufredat")]
    public partial class OGRMufredat
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OGRMufredat()
        {
            OGRMufredatDersleri = new HashSet<OGRMufredatDersleri>();
            OGRKimlik = new HashSet<OGRKimlik>();
            OGRMufredatAKTS = new HashSet<OGRMufredatAKTS>();
        }
        public int ID { get; set; }

        public DateTime Zaman { get; set; }

        public string Kullanici { get; set; }

        public string MufredatAdi { get; set; }

        public int BaslangicYili { get; set; }

        public int BitisYili { get; set; }

        public int FKProgramID { get; set; }

        public virtual Birimler Birimler { get; set; }

        public virtual ICollection<OGRMufredatDersleri> OGRMufredatDersleri { get; set; }

        public virtual ICollection<OGRKimlik> OGRKimlik { get; set; }

        public virtual ICollection<OGRMufredatAKTS> OGRMufredatAKTS { get; set; }
    }
}
