namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OgrOgrenciYonetmelik")]
    public partial class OgrOgrenciYonetmelik
    {
        public int Id { get; set; }

        public string Aciklama { get; set; }

        public int BaslangicDonemNo { get; set; }

        public int BitisDonemNo { get; set; }

        public int FkOgrenciId { get; set; }

        public int FkYonetmelikId { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime GorunumBaslangic { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime GorunumBitis { get; set; }

        public string Kullanici { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }

        public virtual OGRTNMYonetmelik OGRTNMYonetmelik { get; set; }
    }
}
