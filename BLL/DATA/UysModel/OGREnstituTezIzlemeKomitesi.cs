namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGREnstituTezIzlemeKomitesi")]
    public partial class OGREnstituTezIzlemeKomitesi
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? InKod { get; set; }

        public DateTime? TarihBaslangic { get; set; }

        [StringLength(10)]
        public string YKNo { get; set; }

        public DateTime? TarihBitis { get; set; }

        [StringLength(100)]
        public string HariciDanismanAdi { get; set; }

        [StringLength(10)]
        public string DanismanSicilNo { get; set; }

        [StringLength(10)]
        public string OgrEleman1SicilNo { get; set; }

        [StringLength(10)]
        public string OgrEleman2SicilNo { get; set; }

        public int? FKDanismanID { get; set; }

        public int? FKOgrEleman1ID { get; set; }

        public int? FKOgrEleman2ID { get; set; }

        public int? FKOgrenciID { get; set; }

        [StringLength(100)]
        public string AdSoyad { get; set; }

        [StringLength(200)]
        public string Unv { get; set; }

        [StringLength(100)]
        public string AdSoyad1 { get; set; }

        [StringLength(200)]
        public string Unv1 { get; set; }

        [StringLength(100)]
        public string AdSoyad2 { get; set; }

        [StringLength(200)]
        public string Unv2 { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi1 { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi2 { get; set; }
    }
}
