namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.WebHavuzDersPlanAna")]
    public partial class WebHavuzDersPlanAna
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int InKod { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short Yil1 { get; set; }

        [Required]
        [StringLength(2)]
        public string FakKod { get; set; }

        [Required]
        [StringLength(2)]
        public string BolKod { get; set; }

        [Required]
        [StringLength(150)]
        public string HavuzDersAd { get; set; }

        [StringLength(150)]
        public string HavuzYabDersAd { get; set; }

        public byte? Zorunlu { get; set; }

        public byte DSaat { get; set; }

        public byte USaat { get; set; }

        public byte Kredi { get; set; }

        public byte? AKTS { get; set; }

        public byte? DersTip { get; set; }

        [StringLength(2)]
        public string DersYer { get; set; }

        [StringLength(7)]
        public string RenkKod { get; set; }

        [StringLength(30)]
        public string Ekleyen { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string BolKodAd { get; set; }

        [StringLength(50)]
        public string DersKod { get; set; }
    }
}
