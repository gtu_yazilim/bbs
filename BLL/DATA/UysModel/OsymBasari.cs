namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("osym.OsymBasari")]
    public partial class OsymBasari
    {
        public int ID { get; set; }

        public double? Kod { get; set; }

        [StringLength(255)]
        public string ProgramAdi { get; set; }

        public double? Kontenjan { get; set; }

        public double? Yerlesen { get; set; }

        [StringLength(255)]
        public string PuanTuru { get; set; }

        [StringLength(255)]
        public string EnKucuk { get; set; }

        public double? BasariSira { get; set; }

        [StringLength(255)]
        public string EnBuyuk { get; set; }

        public int? Yil { get; set; }

        public int? EnumUniversiteTip { get; set; }

        public int? EnumOgretimTuru { get; set; }

        public double? UnvKod { get; set; }

        public int? FKBirimID { get; set; }

        public double? TabanSira { get; set; }

        public double? TavanSira { get; set; }
    }
}
