namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("gen.TNMIlce")]
    public partial class TNMIlce
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TNMIlce()
        {
            DanismanlikBilgileri = new HashSet<DanismanlikBilgileri>();
            AdresBilgisi = new HashSet<AdresBilgisi>();
            Nufus = new HashSet<Nufus>();
            Nufus1 = new HashSet<Nufus>();
            OBSOnKayitAdres = new HashSet<OBSOnKayitAdres>();
            OgrenimBilgisi = new HashSet<OgrenimBilgisi>();
            TNMEgitimYeri = new HashSet<TNMEgitimYeri>();
            TNMOgrenimYerleri = new HashSet<TNMOgrenimYerleri>();
        }

        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? InKod { get; set; }

        [StringLength(50)]
        public string IlceKodu { get; set; }

        [Required]
        [StringLength(250)]
        public string Adi { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public int FKIlID { get; set; }

        public int IlceKod { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DanismanlikBilgileri> DanismanlikBilgileri { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AdresBilgisi> AdresBilgisi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Nufus> Nufus { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Nufus> Nufus1 { get; set; }

        public virtual TNMIl TNMIl { get; set; }

        public virtual TNMIl TNMIl1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OBSOnKayitAdres> OBSOnKayitAdres { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OgrenimBilgisi> OgrenimBilgisi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TNMEgitimYeri> TNMEgitimYeri { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TNMOgrenimYerleri> TNMOgrenimYerleri { get; set; }
    }
}
