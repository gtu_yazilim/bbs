namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("basvuru.CapYandalTercih")]
    public partial class CapYandalTercih
    {
        public int ID { get; set; }

        public int FKOgrenciID { get; set; }

        public int FKCapYandalKontenjanID { get; set; }

        public DateTime TarihKayit { get; set; }

        public bool Silindi { get; set; }

        public DateTime? TarihGuncelleme { get; set; }

        public int Sira { get; set; }

        [Required]
        [StringLength(250)]
        public string Kullanici { get; set; }

        [Required]
        [StringLength(50)]
        public string IP { get; set; }

        public double Ortalama { get; set; }

        public bool Iptal { get; set; }

        [StringLength(500)]
        public string IptalNedeni { get; set; }

        public bool Onay { get; set; }

        public DateTime? TarihOnay { get; set; }

        public int EnumKontenjanDurum { get; set; }

        public int YerlesmeSira { get; set; }

        public virtual CapYandalKontenjan CapYandalKontenjan { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
