namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.SGKHizmetBirlestirme")]
    public partial class SGKHizmetBirlestirme
    {
        public int ID { get; set; }

        [StringLength(11)]
        public string Tckn { get; set; }

        public int? kayitNo { get; set; }

        public int? sgkNevi { get; set; }

        [StringLength(50)]
        public string sgkSicilNo { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime baslamaTarihi { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? bitisTarihi { get; set; }

        public int? sure { get; set; }

        [StringLength(200)]
        public string kamuIsyeriAd { get; set; }

        [StringLength(200)]
        public string ozelIsyeriAd { get; set; }

        [StringLength(200)]
        public string bagKurMeslek { get; set; }

        public int? ulkeKod { get; set; }

        public int? bankaSandikKod { get; set; }

        [StringLength(1)]
        public string kidemTazminatOdemeDurumu { get; set; }

        public string ayrilmaNedeni { get; set; }

        [StringLength(1)]
        public string khaDurum { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? kurumOnayTarihi { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? zaman { get; set; }

        public int? FKPersonelBilgisiID { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool Durum { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }
    }
}
