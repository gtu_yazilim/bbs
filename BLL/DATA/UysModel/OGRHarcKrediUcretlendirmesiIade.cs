namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRHarcKrediUcretlendirmesiIade")]
    public partial class OGRHarcKrediUcretlendirmesiIade
    {
        public int ID { get; set; }

        public DateTime Zaman { get; set; }

        [Required]
        [StringLength(500)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public double IadeMiktari { get; set; }

        public DateTime IadeTarihi { get; set; }

        public int AitYil { get; set; }

        public int EnumDonemAit { get; set; }

        public int FKKisiID { get; set; }

        [StringLength(100)]
        public string Aciklama { get; set; }

        public DateTime SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public virtual Kisi Kisi { get; set; }
    }
}
