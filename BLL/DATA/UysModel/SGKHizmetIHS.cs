namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.SGKHizmetIHS")]
    public partial class SGKHizmetIHS
    {
        public int ID { get; set; }

        [StringLength(11)]
        public string Tckn { get; set; }

        public int? kayitNo { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? BaslamaTarihi { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? BitisTarihi { get; set; }

        public int? ihzNevi { get; set; }

        [StringLength(100)]
        public string kullaniciAd { get; set; }

        [StringLength(100)]
        public string sifre { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? zaman { get; set; }

        public int? FKPersonelBilgisiID { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }
    }
}
