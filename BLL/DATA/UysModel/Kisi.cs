namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("gen.Kisi")]
    public partial class Kisi
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Kisi()
        {
            AdresBilgisi = new HashSet<AdresBilgisi>();
            IBANBilgisi = new HashSet<IBANBilgisi>();
            Kullanici1 = new HashSet<Kullanici>();
            OGRAskerlik = new HashSet<OGRAskerlik>();
            OGRBelgeAyar = new HashSet<OGRBelgeAyar>();
            OGRHarcKrediUcretlendirmesiIade = new HashSet<OGRHarcKrediUcretlendirmesiIade>();
            OGRHarcKrediUcretlendirmesiOdeme = new HashSet<OGRHarcKrediUcretlendirmesiOdeme>();
            OGRKimlik = new HashSet<OGRKimlik>();
            OGRSaglik = new HashSet<OGRSaglik>();
            PersonelBilgisi = new HashSet<PersonelBilgisi>();
            TelefonBilgisi = new HashSet<TelefonBilgisi>();
        }

        public int ID { get; set; }

        [StringLength(100)]
        public string Ad { get; set; }

        [StringLength(100)]
        public string Soyad { get; set; }

        [StringLength(11)]
        public string TC { get; set; }

        [StringLength(250)]
        public string Foto { get; set; }

        public int? InKod { get; set; }

        [StringLength(100)]
        public string PasaportNo { get; set; }

        [StringLength(100)]
        public string TezkereNo { get; set; }

        [StringLength(100)]
        public string KampKartNo { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public DateTime? Zaman { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public int? FKNufusID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AdresBilgisi> AdresBilgisi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IBANBilgisi> IBANBilgisi { get; set; }

        public virtual Nufus Nufus { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Kullanici> Kullanici1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRAskerlik> OGRAskerlik { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRBelgeAyar> OGRBelgeAyar { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRHarcKrediUcretlendirmesiIade> OGRHarcKrediUcretlendirmesiIade { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRHarcKrediUcretlendirmesiOdeme> OGRHarcKrediUcretlendirmesiOdeme { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRKimlik> OGRKimlik { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRSaglik> OGRSaglik { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PersonelBilgisi> PersonelBilgisi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TelefonBilgisi> TelefonBilgisi { get; set; }
    }
}
