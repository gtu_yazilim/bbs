namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.YOKBildirilenOgrenciler")]
    public partial class YOKBildirilenOgrenciler
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FKOgrenciID { get; set; }

        public int FKYokAktarimID { get; set; }

        public int? EnumAktarimNedeni { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }

        public virtual YOKAktarim YOKAktarim { get; set; }
    }
}
