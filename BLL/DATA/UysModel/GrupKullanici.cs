namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ytk.GrupKullanici")]
    public partial class GrupKullanici
    {
        public int ID { get; set; }

        public int? FKYoksisID { get; set; }

        public int FKKullaniciID { get; set; }

        public int FKGrupID { get; set; }

        public virtual Grup Grup { get; set; }

        public virtual Kullanici Kullanici { get; set; }
    }
}
