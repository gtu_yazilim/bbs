namespace Sabis.Bolum.Bll.DATA.UysModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRDersGrup")]
    public partial class OGRDersGrup
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OGRDersGrup()
        {
            ABSBasariNot = new HashSet<ABSBasariNot>();
            ABSDegerlendirme = new HashSet<ABSDegerlendirme>();
            ABSDegerlendirmeDosya = new HashSet<ABSDegerlendirmeDosya>();
            ABSDevamTakipKontrol = new HashSet<ABSDevamTakipKontrol>();
            ABSGrupCalisma = new HashSet<ABSGrupCalisma>();
            ABSMesaj = new HashSet<ABSMesaj>();
            ABSOgrenciDanismanlik = new HashSet<ABSOgrenciDanismanlik>();
            ABSOgrenciNot = new HashSet<ABSOgrenciNot>();
            ABSOgrenciNotDetay = new HashSet<ABSOgrenciNotDetay>();
            ABSOrtakDegerlendirmeGrup = new HashSet<ABSOrtakDegerlendirmeGrup>();
            ABSPaylar = new HashSet<ABSPaylar>();
            ABSYayinlananBasariNotlar = new HashSet<ABSYayinlananBasariNotlar>();
            ABSYayinlananPayNotlar = new HashSet<ABSYayinlananPayNotlar>();
            OSinavGrup = new HashSet<OSinavGrup>();
            DersIcerikPuani = new HashSet<DersIcerikPuani>();
            OgrenciDersCevap = new HashSet<OgrenciDersCevap>();
            YazOkuluOnKayitDersler = new HashSet<YazOkuluOnKayitDersler>();
            ABSDokumanGrup = new HashSet<ABSDokumanGrup>();
            ABSProjeTakvim = new HashSet<ABSProjeTakvim>();
            AKRGenelOgrenmeCiktilari = new HashSet<AKRGenelOgrenmeCiktilari>();
            AKRGenelProgramCiktilari = new HashSet<AKRGenelProgramCiktilari>();
            OGRDersGrup1 = new HashSet<OGRDersGrup>();
            OGRDersGrupHoca = new HashSet<OGRDersGrupHoca>();
            OGROgrenciYazilma = new HashSet<OGROgrenciYazilma>();
            OGROgrenciYazilma1 = new HashSet<OGROgrenciYazilma>();
            OGRMazeret = new HashSet<OGRMazeret>();
            OGROgrenciNot = new HashSet<OGROgrenciNot>();
            PayRandevu = new HashSet<PayRandevu>();
        }

        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(500)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? InKod { get; set; }

        [StringLength(10)]
        public string GrupAd { get; set; }

        public int? OgretimYili { get; set; }

        public int? GrupLimit { get; set; }

        public DateTime? FinalTarihi { get; set; }

        public int? EnumEBSOnay { get; set; }

        public int? EnumDersPlanIliskiNo { get; set; }

        public int? EnumDersGrupDil { get; set; }

        public int? EnumDonem { get; set; }

        public int? FKDersPlanAnaID { get; set; }

        public int? FKDersPlanID { get; set; }

        public int? FKRedirectToDersGrupID { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public int FKBirimID { get; set; }

        public int EnumOgretimTur { get; set; }

        public bool Acik { get; set; }

        public bool Silindi { get; set; }

        public bool Uzem { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSBasariNot> ABSBasariNot { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSDegerlendirme> ABSDegerlendirme { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSDegerlendirmeDosya> ABSDegerlendirmeDosya { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSDevamTakipKontrol> ABSDevamTakipKontrol { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSGrupCalisma> ABSGrupCalisma { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSMesaj> ABSMesaj { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSOgrenciDanismanlik> ABSOgrenciDanismanlik { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSOgrenciNot> ABSOgrenciNot { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSOgrenciNotDetay> ABSOgrenciNotDetay { get; set; }

        public virtual ICollection<ABSOrtakDegerlendirmeGrup> ABSOrtakDegerlendirmeGrup  { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSPaylar> ABSPaylar { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSYayinlananBasariNotlar> ABSYayinlananBasariNotlar { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSYayinlananPayNotlar> ABSYayinlananPayNotlar { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OSinavGrup> OSinavGrup { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DersIcerikPuani> DersIcerikPuani { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OgrenciDersCevap> OgrenciDersCevap { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<YazOkuluOnKayitDersler> YazOkuluOnKayitDersler { get; set; }

        public virtual Birimler Birimler { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSDokumanGrup> ABSDokumanGrup { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSProjeTakvim> ABSProjeTakvim { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AKRGenelOgrenmeCiktilari> AKRGenelOgrenmeCiktilari { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AKRGenelProgramCiktilari> AKRGenelProgramCiktilari { get; set; }

        public virtual OGRDersPlan OGRDersPlan { get; set; }

        public virtual OGRDersPlanAna OGRDersPlanAna { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRDersGrup> OGRDersGrup1 { get; set; }

        public virtual OGRDersGrup OGRDersGrup2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRDersGrupHoca> OGRDersGrupHoca { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGROgrenciYazilma> OGROgrenciYazilma { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGROgrenciYazilma> OGROgrenciYazilma1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRMazeret> OGRMazeret { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGROgrenciNot> OGROgrenciNot { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PayRandevu> PayRandevu { get; set; }
    }
}
