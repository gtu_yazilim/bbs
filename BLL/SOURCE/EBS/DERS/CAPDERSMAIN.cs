﻿using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Bolum.Core.EBS.BIRIM;
using Sabis.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Bll.SOURCE.EBS.DERS
{
    public class CAPDERSMAIN
    {
        public static List<BirimDto> Birimler()
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                try
                {
                    return db.Birimler.Where(b => b.UstBirimID == 21398).Take(10).Select(x => new BirimDto
                    {
                        BirimID = x.ID,
                        BirimAdi = x.BirimAdi
                    }).OrderBy(y => y.BirimAdi).ToList();
                }
                catch (Exception ex)
                {

                    return null;
                }
            }
        }
        public static bool CapDersKaydet(int EnumGecisTipi, int FKAnaBirimID, int FKAltBirimID, bool Silindi, string Kullanici, DateTime TarihKayit, int Kontenjan)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                try
                {
                    if (!db.OGRCapYandalTanim.Any(x => x.FKAnaBirimID == FKAnaBirimID && x.FKAltBirimID == FKAltBirimID && !x.Silindi))
                    {
                        var ct = new OGRCapYandalTanim();
                        ct.EnumGecisTipi = EnumGecisTipi;
                        ct.FKAnaBirimID = FKAnaBirimID;
                        ct.FKAltBirimID = FKAltBirimID;
                        ct.Silindi = Silindi;
                        ct.Kullanici = Kullanici;
                        ct.TarihKayit = TarihKayit;
                        db.OGRCapYandalTanim.Add(ct);
                        db.SaveChanges();

                        if (!db.CapYandalKontenjan.Any(x => x.FKCapYandalTanimID == ct.ID && !x.Silindi))
                        {
                            var ck = new CapYandalKontenjan();
                            ck.Kullanici = Kullanici;
                            ck.TarihKayit = DateTime.Now;
                            ck.FKCapYandalTanimID = ct.ID;
                            ck.Yil = 2017;
                            ck.Donem = 1;
                            ck.Adet = Kontenjan;
                            ck.Silindi = false;
                            ck.KayitAcik = false;
                            db.CapYandalKontenjan.Add(ck);
                            db.SaveChanges();
                        }

                        return true;
                    }
                    return false;

                }
                catch (Exception ex)
                {

                    return false;
                }

            }
        }
        public static List<BolumPlanDto> BolumPlanGetir(int yil, int bolumID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                List<BolumPlanDto> liste = new List<BolumPlanDto>();
                return db.OGRDersPlan.Where(x => x.FKBirimID == bolumID && x.Yil == yil).Select(x => new BolumPlanDto
                {
                    FKDersPlanID = x.ID,
                    FKDersPlanAnaID = x.FKDersPlanAnaID,
                    DersAd = x.OGRDersPlanAna.DersAd,
                    Yariyil = x.Yariyil,
                    DersSaat = x.OGRDersPlanAna.DSaat,
                    AKTS = x.OGRDersPlanAna.ECTSKredi
                }).OrderBy(o => o.DersAd).ToList();
            }
        }

        public static List<BolumPlanDto> CapPlanGetir(int FKCapTanimID, int FKAnaBirimID, int yil)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                List<BolumPlanDto> liste = new List<BolumPlanDto>();
                var veri = db.OGRDersPlan.Where(x =>
                    x.FKBirimID == FKAnaBirimID &&
                    x.Yil == yil &&
                    x.EnumDonem != 3 &&
                    (x.Yariyil != 0 && x.Yariyil != 9) &&
                    (x.EnumEBSGosterim == null || x.EnumEBSGosterim == 1) &&
                    //x.OGRDersPlanAna.EnumDersAktif == 1 &&
                    x.EnumDersPlanKapsam == 1 &&
                    !(x.Yariyil == 7 && x.OGRDersPlanAna.EnumKokDersTipi == 3) &&
                    x.EnumDersSecimTipi == (int)Sabis.Enum.EnumDersSecimTipi.ZORUNLU
                    ).Select(x => new
                    {
                        x.ID,
                        x.FKDersPlanAnaID,
                        x.OGRDersPlanAna.DersAd,
                        x.Yariyil,
                        x.OGRDersPlanAna.DSaat,
                        x.OGRDersPlanAna.ECTSKredi
                    }).OrderBy(o => o.Yariyil).ThenBy(a => a.DersAd);
                if (veri != null)
                {
                    foreach (var item in veri.ToList())
                    {
                        //if (!db.OGRCapDersPlan.Any(x => x.FKCapTanimID == FKCapTanimID && x.FKAnaDersPlanID == item.ID && !x.Silindi))
                        //if (!db.OGRCapDersPlan.Any(x => x.FKCapTanimID == FKCapTanimID && x.FKAnaDersPlanAnaID == item.FKDersPlanAnaID && !x.Silindi))
                        if (!db.OGRCapDersPlan.Any(x => x.FKCapTanimID == FKCapTanimID && x.FKCapDersPlanAnaID == item.FKDersPlanAnaID && !x.Silindi))
                        {
                            var model = new BolumPlanDto();
                            model.FKCapTanimID = FKCapTanimID;
                            model.FKDersPlanID = item.ID;
                            model.FKDersPlanAnaID = item.FKDersPlanAnaID;
                            model.DersAd = item.DersAd;
                            model.Yariyil = item.Yariyil;
                            model.DersSaat = item.DSaat;
                            model.AKTS = item.ECTSKredi;
                            liste.Add(model);
                        }

                    }
                    //seçmeli dersleri getir
                    //var etiketDersler = (from saatBilgi in db.OGRTNMSaatBilgi
                    //                     join etiketDers in db.OGRTNMDersEtiket on saatBilgi.FKEtiketID equals etiketDers.ID
                    //                     where 
                    //                        saatBilgi.Yil == yil && 
                    //                        saatBilgi.FKBolumPrBirimID == FKAnaBirimID &&
                    //                        saatBilgi.AKTS != 0
                    //                     select new
                    //                     {
                    //                         saatBilgi.ID,
                    //                         etiketDers.Etiket,
                    //                         etiketDers.EtiketEng,
                    //                         saatBilgi.EnumDersPlanZorunlu,
                    //                         saatBilgi.FKEtiketID,
                    //                         saatBilgi.AKTS,
                    //                         saatBilgi.Yariyil,
                    //                         etiketDers.Renk,
                    //                         saatBilgi.Sayi
                    //                     }).OrderBy(m => m.Yariyil).ThenBy(m => m.Etiket).ToList();
                    //if (etiketDersler != null)
                    //{
                    //    foreach (var item in etiketDersler)
                    //    {
                    //        for (int i = 0; i < item.Sayi; i++)
                    //        {
                    //            var model = new BolumPlanDto();
                    //            model.FKCapTanimID = FKCapTanimID;
                    //            model.FKDersPlanID = item.ID;
                    //            model.FKDersPlanAnaID = item.FKEtiketID;
                    //            model.DersAd = item.Etiket;
                    //            model.Yariyil = item.Yariyil;
                    //            //model.DersSaat = 333;
                    //            model.AKTS = item.AKTS;
                    //            liste.Add(model);
                    //        }
                    //    }
                    //}


                    return liste;
                }
                else
                {
                    return null;
                }
            }
        }

        public static List<BolumPlanDto> CapPlanGetir2(int FKCapTanimID, int FKAnaBirimID, int yil)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                List<BolumPlanDto> liste = new List<BolumPlanDto>();
                var veri = db.OGRDersPlan.Where(x =>
                    x.FKBirimID == FKAnaBirimID &&
                    x.Yil == yil &&
                    x.EnumDonem != 3 &&
                    (x.Yariyil != 0 && x.Yariyil != 9) &&
                    (x.EnumEBSGosterim == null || x.EnumEBSGosterim == 1) &&
                    //x.OGRDersPlanAna.EnumDersAktif == 1 &&
                    x.EnumDersPlanKapsam == 1 &&
                    !(x.Yariyil == 7 && x.OGRDersPlanAna.EnumKokDersTipi == 3) &&
                    x.EnumDersSecimTipi == (int)Sabis.Enum.EnumDersSecimTipi.ZORUNLU
                    ).Select(x => new
                    {
                        x.ID,
                        x.FKDersPlanAnaID,
                        x.OGRDersPlanAna.DersAd,
                        x.Yariyil,
                        x.OGRDersPlanAna.DSaat,
                        x.OGRDersPlanAna.ECTSKredi
                    }).OrderBy(o => o.Yariyil).ThenBy(a => a.DersAd);
                if (veri != null)
                {
                    foreach (var item in veri.ToList())
                    {
                        if (!db.OGRCapDersPlan.Any(x => x.FKCapTanimID == FKCapTanimID && x.FKAnaDersPlanID == item.ID && !x.Silindi))            
                        {
                            var model = new BolumPlanDto();
                            model.FKCapTanimID = FKCapTanimID;
                            model.FKDersPlanID = item.ID;
                            model.FKDersPlanAnaID = item.FKDersPlanAnaID;
                            model.DersAd = item.DersAd;
                            model.Yariyil = item.Yariyil;
                            model.DersSaat = item.DSaat;
                            model.AKTS = item.ECTSKredi;
                            liste.Add(model);
                        }

                    }
                    //seçmeli dersleri getir
                    //var etiketDersler = (from saatBilgi in db.OGRTNMSaatBilgi
                    //                     join etiketDers in db.OGRTNMDersEtiket on saatBilgi.FKEtiketID equals etiketDers.ID
                    //                     where 
                    //                        saatBilgi.Yil == yil && 
                    //                        saatBilgi.FKBolumPrBirimID == FKAnaBirimID &&
                    //                        saatBilgi.AKTS != 0
                    //                     select new
                    //                     {
                    //                         saatBilgi.ID,
                    //                         etiketDers.Etiket,
                    //                         etiketDers.EtiketEng,
                    //                         saatBilgi.EnumDersPlanZorunlu,
                    //                         saatBilgi.FKEtiketID,
                    //                         saatBilgi.AKTS,
                    //                         saatBilgi.Yariyil,
                    //                         etiketDers.Renk,
                    //                         saatBilgi.Sayi
                    //                     }).OrderBy(m => m.Yariyil).ThenBy(m => m.Etiket).ToList();
                    //if (etiketDersler != null)
                    //{
                    //    foreach (var item in etiketDersler)
                    //    {
                    //        for (int i = 0; i < item.Sayi; i++)
                    //        {
                    //            var model = new BolumPlanDto();
                    //            model.FKCapTanimID = FKCapTanimID;
                    //            model.FKDersPlanID = item.ID;
                    //            model.FKDersPlanAnaID = item.FKEtiketID;
                    //            model.DersAd = item.Etiket;
                    //            model.Yariyil = item.Yariyil;
                    //            //model.DersSaat = 333;
                    //            model.AKTS = item.AKTS;
                    //            liste.Add(model);
                    //        }
                    //    }
                    //}


                    return liste;
                }
                else
                {
                    return null;
                }
            }
        }

        public static List<BolumEtiketPlanDto> CapSecmeliPlanGetir(int FKCapTanimID, int FKAnaBirimID, int yil)
        {
            //aslında dersetiketleri getiriyor.
            using (UYSv2Entities db = new UYSv2Entities())
            {
                List<BolumEtiketPlanDto> liste = new List<BolumEtiketPlanDto>();
                var veri = (from saatBilgi in db.OGRTNMSaatBilgi
                            join etiketDers in db.OGRTNMDersEtiket on saatBilgi.FKEtiketID equals etiketDers.ID
                            where saatBilgi.Yil == yil && saatBilgi.FKBolumPrBirimID == FKAnaBirimID && saatBilgi.AKTS != 0
                            select new
                            {
                                saatBilgi.ID,
                                etiketDers.Etiket,
                                etiketDers.EtiketEng,
                                saatBilgi.EnumDersPlanZorunlu,
                                saatBilgi.FKEtiketID,
                                saatBilgi.AKTS,
                                saatBilgi.Yariyil,
                                etiketDers.Renk,
                                saatBilgi.Sayi
                            }).OrderBy(m => m.Yariyil).ThenBy(m => m.Etiket);
                if (veri != null)
                {
                    foreach (var item in veri)
                    {
                        //var intibak_yapilan_secmeli_kontrol = db.OGRCapSecmeliTanim.Where(x => x.FKCapTanimID == FKCapTanimID && x.FKCapSaatBilgiID == item.ID && x.FKCapEtiketID == item.FKEtiketID && !x.Silindi);
                        var intibak_yapilan_secmeli_kontrol = db.OGRCapSecmeliTanim.Where(x => x.FKCapTanimID == FKCapTanimID && x.FKCapSaatBilgiID == item.ID && x.FKCapEtiketID == item.FKEtiketID && !x.Silindi);

                        

                        if (intibak_yapilan_secmeli_kontrol.Count() > 0)
                        {
                            if (item.Sayi.Value != intibak_yapilan_secmeli_kontrol.ToList().Sum(x => x.Adet))
                            {
                                var model = new BolumEtiketPlanDto();
                                model.FKEtiketID = item.FKEtiketID;
                                model.FKSaatBilgiID = item.ID;
                                model.Etiket = item.Etiket;
                                model.AKTS = item.AKTS * Math.Abs(item.Sayi.Value - intibak_yapilan_secmeli_kontrol.ToList().Sum(x => x.Adet));
                                model.YariYil = item.Yariyil;
                                model.Sayi = Math.Abs(item.Sayi.Value - intibak_yapilan_secmeli_kontrol.ToList().Sum(x => x.Adet));
                                liste.Add(model);
                            }
                        }
                        else
                        {
                            var model = new BolumEtiketPlanDto();
                            model.FKEtiketID = item.FKEtiketID;
                            model.FKSaatBilgiID = item.ID;
                            model.Etiket = item.Etiket;
                            model.AKTS = item.AKTS * item.Sayi.Value;
                            model.YariYil = item.Yariyil;
                            model.Sayi = item.Sayi;
                            liste.Add(model);
                        }

                    }
                    return liste;
                }
                else
                {
                    return null;
                }



                //List<BolumPlanDto> liste = new List<BolumPlanDto>();
                //var veri = db.OGRDersPlan.Where(x =>
                //    x.FKBirimID == FKAnaBirimID &&
                //    x.Yil == yil &&
                //    x.EnumDonem != 3 &&
                //    (x.Yariyil != 0 && x.Yariyil != 9) &&
                //    (x.EnumEBSGosterim == null || x.EnumEBSGosterim == 1) &&
                //    //x.OGRDersPlanAna.EnumDersAktif == 1 &&
                //    x.EnumDersPlanKapsam == 1 &&
                //    !(x.Yariyil == 7 && x.OGRDersPlanAna.EnumKokDersTipi == 3) &&
                //    x.EnumDersSecimTipi == (int)Sabis.Enum.EnumDersSecimTipi.SECMELI
                //    ).Select(x => new
                //    {
                //        x.ID,
                //        x.FKDersPlanAnaID,
                //        x.OGRDersPlanAna.DersAd,
                //        x.Yariyil,
                //        x.OGRDersPlanAna.DSaat,
                //        x.OGRDersPlanAna.ECTSKredi
                //    }).OrderBy(o => o.Yariyil).ThenBy(a => a.DersAd);
                //if (veri != null)
                //{
                //    foreach (var item in veri.ToList())
                //    {
                //        //if (!db.OGRCapDersPlan.Any(x => x.FKCapTanimID == FKCapTanimID && x.FKAnaDersPlanID == item.ID && !x.Silindi))
                //        //{
                //            var model = new BolumPlanDto();
                //            model.FKCapTanimID = FKCapTanimID;
                //            model.FKDersPlanID = item.ID;
                //            model.FKDersPlanAnaID = item.FKDersPlanAnaID;
                //            model.DersAd = item.DersAd;
                //            model.Yariyil = item.Yariyil;
                //            model.DersSaat = item.DSaat;
                //            model.AKTS = item.ECTSKredi;
                //            liste.Add(model);
                //        //}

                //    }
                //    return liste;
                //}
                //else
                //{
                //    return null;
                //}
            }
        }
        public static List<BolumEtiketPlanDto> AnaSecmeliPlanGetir(int FKCapTanimID, int FKAnaBirimID, int yil)
        {
            //aslında dersetiketleri getiriyor.
            using (UYSv2Entities db = new UYSv2Entities())
            {
                List<BolumEtiketPlanDto> liste = new List<BolumEtiketPlanDto>();
                var veri = (from saatBilgi in db.OGRTNMSaatBilgi
                            join etiketDers in db.OGRTNMDersEtiket on saatBilgi.FKEtiketID equals etiketDers.ID
                            where saatBilgi.Yil == yil && saatBilgi.FKBolumPrBirimID == FKAnaBirimID && saatBilgi.AKTS != 0
                            select new
                            {
                                saatBilgi.ID,
                                etiketDers.Etiket,
                                etiketDers.EtiketEng,
                                saatBilgi.EnumDersPlanZorunlu,
                                saatBilgi.FKEtiketID,
                                saatBilgi.AKTS,
                                saatBilgi.Yariyil,
                                etiketDers.Renk,
                                saatBilgi.Sayi
                            }).OrderBy(m => m.Yariyil).ThenBy(m => m.Etiket);
                if (veri != null)
                {
                    foreach (var item in veri)
                    {
                        //var intibak_yapilan_secmeli_kontrol = db.OGRCapSecmeliTanim.Where(x => x.FKCapTanimID == FKCapTanimID && x.FKSaatBilgiID == item.ID && x.FKAnaEtiketID == item.FKEtiketID && !x.Silindi);
                        var intibak_yapilan_secmeli_kontrol = db.OGRCapSecmeliTanim.Where(x => x.FKCapTanimID == FKCapTanimID && x.FKSaatBilgiID == item.ID && x.FKAnaEtiketID == item.FKEtiketID && !x.Silindi);

                        if (intibak_yapilan_secmeli_kontrol.Count() > 0)
                        {
                            if (item.Sayi.Value != intibak_yapilan_secmeli_kontrol.ToList().Sum(x => x.Adet))
                            {
                                var model = new BolumEtiketPlanDto();
                                model.FKEtiketID = item.FKEtiketID;
                                model.FKSaatBilgiID = item.ID;
                                model.Etiket = item.Etiket;
                                model.AKTS = item.AKTS * Math.Abs(item.Sayi.Value - intibak_yapilan_secmeli_kontrol.ToList().Sum(x => x.Adet));
                                model.YariYil = item.Yariyil;
                                model.Sayi = Math.Abs(item.Sayi.Value - intibak_yapilan_secmeli_kontrol.ToList().Sum(x => x.Adet));
                                liste.Add(model);
                            }
                        }
                        else
                        {
                            var model = new BolumEtiketPlanDto();
                            model.FKEtiketID = item.FKEtiketID;
                            model.FKSaatBilgiID = item.ID;
                            model.Etiket = item.Etiket;
                            model.AKTS = item.AKTS * item.Sayi.Value;
                            model.YariYil = item.Yariyil;
                            model.Sayi = item.Sayi;
                            liste.Add(model);
                        }

                    }
                    return liste;
                }
                else
                {
                    return null;
                }



                //List<BolumPlanDto> liste = new List<BolumPlanDto>();
                //var veri = db.OGRDersPlan.Where(x =>
                //    x.FKBirimID == FKAnaBirimID &&
                //    x.Yil == yil &&
                //    x.EnumDonem != 3 &&
                //    (x.Yariyil != 0 && x.Yariyil != 9) &&
                //    (x.EnumEBSGosterim == null || x.EnumEBSGosterim == 1) &&
                //    //x.OGRDersPlanAna.EnumDersAktif == 1 &&
                //    x.EnumDersPlanKapsam == 1 &&
                //    !(x.Yariyil == 7 && x.OGRDersPlanAna.EnumKokDersTipi == 3) &&
                //    x.EnumDersSecimTipi == (int)Sabis.Enum.EnumDersSecimTipi.SECMELI
                //    ).Select(x => new
                //    {
                //        x.ID,
                //        x.FKDersPlanAnaID,
                //        x.OGRDersPlanAna.DersAd,
                //        x.Yariyil,
                //        x.OGRDersPlanAna.DSaat,
                //        x.OGRDersPlanAna.ECTSKredi
                //    }).OrderBy(o => o.Yariyil).ThenBy(a => a.DersAd);
                //if (veri != null)
                //{
                //    foreach (var item in veri.ToList())
                //    {
                //        //if (!db.OGRCapDersPlan.Any(x => x.FKCapTanimID == FKCapTanimID && x.FKAnaDersPlanID == item.ID && !x.Silindi))
                //        //{
                //            var model = new BolumPlanDto();
                //            model.FKCapTanimID = FKCapTanimID;
                //            model.FKDersPlanID = item.ID;
                //            model.FKDersPlanAnaID = item.FKDersPlanAnaID;
                //            model.DersAd = item.DersAd;
                //            model.Yariyil = item.Yariyil;
                //            model.DersSaat = item.DSaat;
                //            model.AKTS = item.ECTSKredi;
                //            liste.Add(model);
                //        //}

                //    }
                //    return liste;
                //}
                //else
                //{
                //    return null;
                //}
            }
        }

        public static List<BolumEtiketPlanDto> CapSecmeliPlanGetir2(int FKCapTanimID, int FKAnaBirimID, int yil)
        {
            //aslında dersetiketleri getiriyor.
            using (UYSv2Entities db = new UYSv2Entities())
            {
                List<BolumEtiketPlanDto> liste = new List<BolumEtiketPlanDto>();
                var veri = (from saatBilgi in db.OGRTNMSaatBilgi
                            join etiketDers in db.OGRTNMDersEtiket on saatBilgi.FKEtiketID equals etiketDers.ID
                            where saatBilgi.Yil == yil && saatBilgi.FKBolumPrBirimID == FKAnaBirimID && saatBilgi.AKTS != 0
                            select new
                            {
                                saatBilgi.ID,
                                etiketDers.Etiket,
                                etiketDers.EtiketEng,
                                saatBilgi.EnumDersPlanZorunlu,
                                saatBilgi.FKEtiketID,
                                saatBilgi.AKTS,
                                saatBilgi.Yariyil,
                                etiketDers.Renk,
                                saatBilgi.Sayi
                            }).OrderBy(m => m.Yariyil).ThenBy(m => m.Etiket);
                if (veri != null)
                {
                    foreach (var item in veri)
                    {

                    }
                    return liste;
                }
                else
                {
                    return null;
                }



                //List<BolumPlanDto> liste = new List<BolumPlanDto>();
                //var veri = db.OGRDersPlan.Where(x =>
                //    x.FKBirimID == FKAnaBirimID &&
                //    x.Yil == yil &&
                //    x.EnumDonem != 3 &&
                //    (x.Yariyil != 0 && x.Yariyil != 9) &&
                //    (x.EnumEBSGosterim == null || x.EnumEBSGosterim == 1) &&
                //    //x.OGRDersPlanAna.EnumDersAktif == 1 &&
                //    x.EnumDersPlanKapsam == 1 &&
                //    !(x.Yariyil == 7 && x.OGRDersPlanAna.EnumKokDersTipi == 3) &&
                //    x.EnumDersSecimTipi == (int)Sabis.Enum.EnumDersSecimTipi.SECMELI
                //    ).Select(x => new
                //    {
                //        x.ID,
                //        x.FKDersPlanAnaID,
                //        x.OGRDersPlanAna.DersAd,
                //        x.Yariyil,
                //        x.OGRDersPlanAna.DSaat,
                //        x.OGRDersPlanAna.ECTSKredi
                //    }).OrderBy(o => o.Yariyil).ThenBy(a => a.DersAd);
                //if (veri != null)
                //{
                //    foreach (var item in veri.ToList())
                //    {
                //        //if (!db.OGRCapDersPlan.Any(x => x.FKCapTanimID == FKCapTanimID && x.FKAnaDersPlanID == item.ID && !x.Silindi))
                //        //{
                //            var model = new BolumPlanDto();
                //            model.FKCapTanimID = FKCapTanimID;
                //            model.FKDersPlanID = item.ID;
                //            model.FKDersPlanAnaID = item.FKDersPlanAnaID;
                //            model.DersAd = item.DersAd;
                //            model.Yariyil = item.Yariyil;
                //            model.DersSaat = item.DSaat;
                //            model.AKTS = item.ECTSKredi;
                //            liste.Add(model);
                //        //}

                //    }
                //    return liste;
                //}
                //else
                //{
                //    return null;
                //}
            }
        }

        #region EBS Plan Getir
        public static List<BolumDersPlanViewModel> BolumEtiketDersPlan(byte dil, int ProgramID, int Yil, List<BolumDersPlanViewModel> bolumDersPlanListe)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var etiketDersler = (from saatBilgi in db.OGRTNMSaatBilgi
                                     join etiketDers in db.OGRTNMDersEtiket on saatBilgi.FKEtiketID equals etiketDers.ID
                                     where saatBilgi.Yil == (Yil) && saatBilgi.FKBolumPrBirimID == ProgramID
                                     select new
                                     {
                                         saatBilgi.ID,
                                         etiketDers.Etiket,
                                         etiketDers.EtiketEng,
                                         saatBilgi.EnumDersPlanZorunlu,
                                         saatBilgi.FKEtiketID,
                                         saatBilgi.AKTS,
                                         saatBilgi.Yariyil,
                                         etiketDers.Renk,
                                         saatBilgi.Sayi
                                     }).OrderBy(m => m.Yariyil).ThenBy(m => m.Etiket).ToList();
                List<BolumDersPlanViewModel> etiketDersPlanListe = new List<BolumDersPlanViewModel>();
                int sonYariyil = -1;
                if (etiketDersler != null)
                {
                    foreach (var sonuc in etiketDersler)
                    {
                        BolumDersPlanViewModel item;
                        BolumDersPlanYariyilViewModel dersplanZorunludetay;
                        for (int i = 0; i < sonuc.Sayi; i++) // kaçtane seçmeli varsa o kadar eklensin. 
                        {
                            dersplanZorunludetay = new BolumDersPlanYariyilViewModel();

                            dersplanZorunludetay.DersKod = "";
                            dersplanZorunludetay.AKTS = (int)sonuc.AKTS;
                            dersplanZorunludetay.DersAd = dil == 1 ? sonuc.Etiket : sonuc.EtiketEng;
                            dersplanZorunludetay.YabDersAd = sonuc.EtiketEng;
                            dersplanZorunludetay.DersPlanAnaID = (int)sonuc.FKEtiketID;
                            dersplanZorunludetay.DersPlanID = sonuc.ID;
                            dersplanZorunludetay.EnumZorunluTur = (int)sonuc.EnumDersPlanZorunlu;
                            dersplanZorunludetay.Renk = sonuc.Renk;
                            if (sonYariyil != sonuc.Yariyil)
                            {
                                item = new BolumDersPlanViewModel();
                                item.Yariyil = (int)sonuc.Yariyil;
                                etiketDersPlanListe.Add(item);
                                sonYariyil = (int)sonuc.Yariyil;
                            }
                            etiketDersPlanListe.First(s => s.Yariyil == sonYariyil).YariyilZorunluDetayListe.Add(dersplanZorunludetay);
                        }


                    }
                }
                if (etiketDersPlanListe.Count > 0)
                    foreach (var yariyil in bolumDersPlanListe)
                    {
                        if (etiketDersPlanListe.FirstOrDefault(s => s.Yariyil == yariyil.Yariyil) != null)
                            bolumDersPlanListe.First(s => s.Yariyil == yariyil.Yariyil).YariyilZorunluDetayListe.AddRange(etiketDersPlanListe.FirstOrDefault(s => s.Yariyil == yariyil.Yariyil).YariyilZorunluDetayListe);

                    }
                return bolumDersPlanListe;
            }
        }

        public static List<BolumDersPlanViewModel> BolumDersPlan(byte dil, int FakulteID, int ProgramID, int Yil, byte ogrenimseviye)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var bolumDersPlan = ((from dersplan in db.OGRDersPlan
                                      join dersplanana in db.OGRDersPlanAna on dersplan.FKDersPlanAnaID equals dersplanana.ID
                                      join dersicerik in db.DersBilgi on dersplanana.ID equals dersicerik.FKDersPlanAnaID into leftderscerik
                                      from altdersicerik in leftderscerik.Where(f => f.Yil == Yil).DefaultIfEmpty()
                                      where dersplan.FKBirimID == ProgramID && dersplan.Yil == Yil && dersplan.EnumDersPlanKapsam == 1
                                      && dersplan.EnumDonem != 3 && dersplan.EnumEBSGosterim == null
                                      //&& (dersplanana.EnumKokDersTipi==1 || dersplanana.EnumKokDersTipi ==6 || dersplanana.EnumKokDersTipi ==3 || dersplanana.EnumKokDersTipi == 8 || dersplanana.EnumKokDersTipi ==9 || dersplanana.EnumKokDersTipi == 11|| dersplanana.EnumKokDersTipi == 12)
                                      && !(dersplan.Yariyil == 7 && dersplanana.EnumKokDersTipi == 3)
                                      select new
                                      {
                                          dersplan.ID,
                                          dersplan.BolKodAd,
                                          dersplan.DersKod,
                                          dersplanana.DersAd,
                                          dersplanana.YabDersAd,
                                          dersplan.EnumDersSecimTipi,
                                          dersplan.EnumDil,
                                          dersplan.EnumDil2,
                                          altdersicerik.Yil,
                                          dersplanana.DSaat,
                                          dersplanana.USaat,
                                          dersplanana.Kredi,
                                          dersplanana.ECTSKredi,
                                          dersplan.FKDersPlanAnaID,
                                          dersplan.Yariyil,
                                          dersplan.EnumDersBirimTipi,
                                          dersplanana.EnumDersTipi
                                      })
                                 .Union(from dersplan in db.OGRDersPlan
                                        join dersplanana in db.OGRDersPlanAna on dersplan.FKDersPlanAnaID equals dersplanana.ID
                                        join dersicerik in db.DersBilgi on dersplanana.ID equals dersicerik.FKDersPlanAnaID into leftderscerik
                                        from altdersicerik in leftderscerik.Where(f => f.Yil == Yil).DefaultIfEmpty()
                                        where dersplan.FKFakulteBirimID == FakulteID && dersplan.Yil == Yil && dersplan.EnumDersPlanKapsam == 1
                                        && dersplan.EnumDersSecimTipi == 2 && dersplan.EnumDersBirimTipi == 2 && dersplan.EnumDonem != 3 && dersplan.EnumEBSGosterim == null
                                        select new
                                        {
                                            dersplan.ID,
                                            dersplan.BolKodAd,
                                            dersplan.DersKod,
                                            dersplanana.DersAd,
                                            dersplanana.YabDersAd,
                                            dersplan.EnumDersSecimTipi,
                                            dersplan.EnumDil,
                                            dersplan.EnumDil2,
                                            altdersicerik.Yil,
                                            dersplanana.DSaat,
                                            dersplanana.USaat,
                                            dersplanana.Kredi,
                                            dersplanana.ECTSKredi,
                                            dersplan.FKDersPlanAnaID,
                                            dersplan.Yariyil,
                                            dersplan.EnumDersBirimTipi,
                                            dersplanana.EnumDersTipi
                                        }
                                  /*güzel sanatlar haric bitirme projesi kısıtlaması 7.yy da gösterilmeyecek                                 */
                                  )).OrderBy(m => (m.Yariyil)).ThenBy(m => m.EnumDersTipi).ThenBy(m => m.BolKodAd).ThenBy(m => m.DersKod).ThenBy(m => m.DersAd);
                // aşağıda ders ve ona bağlı yeterlilikleri listelemek için aşağıdaki foreach yapısını yazdım adurdu.24.04.2015

                //bolumDersPlan = bolumDersPlan.Union(etiketDersler).OrderBy(m=>(m.Yariyil)).ThenBy(m=>m.EnumDersTipi).ThenBy(m=>m.DersAd).ThenBy(m=>m.BolKodAd).ThenBy(m=>m.DersKod);
                List<BolumDersPlanViewModel> liste = new List<BolumDersPlanViewModel>();
                int sonYariyil = -1;
                if (bolumDersPlan != null)
                {
                    foreach (var sonuc in bolumDersPlan)
                    {
                        if (sonYariyil != (int)sonuc.Yariyil)
                        {
                            BolumDersPlanViewModel item = new BolumDersPlanViewModel();
                            item.Yariyil = (int)sonuc.Yariyil;
                            BolumDersPlanYariyilViewModel dersplanZorunludetay = new BolumDersPlanYariyilViewModel();
                            BolumDersPlanYariyilViewModel dersplanSecmelidetay = new BolumDersPlanYariyilViewModel();
                            if (sonuc.EnumDersSecimTipi == 1)
                            { // Zorunlu Ders ise 
                                dersplanZorunludetay.DersKod = sonuc.BolKodAd + " " + sonuc.DersKod;
                                if (sonuc.ECTSKredi != null) dersplanZorunludetay.AKTS = (int)sonuc.ECTSKredi;
                                dersplanZorunludetay.DersAd = dil == 1 ? sonuc.DersAd : sonuc.YabDersAd;
                                dersplanZorunludetay.YabDersAd = sonuc.YabDersAd;
                                if (sonuc.FKDersPlanAnaID != null) dersplanZorunludetay.DersPlanAnaID = (int)sonuc.FKDersPlanAnaID;
                                dersplanZorunludetay.DersPlanID = sonuc.ID;
                                if (sonuc.EnumDersTipi != null) dersplanZorunludetay.EnumDersTip = (int)sonuc.EnumDersTipi;
                                if (sonuc.DSaat != null) dersplanZorunludetay.TSaat = (int)sonuc.DSaat;
                                if (sonuc.USaat != null) dersplanZorunludetay.USaat = (int)sonuc.USaat;
                                dersplanZorunludetay.EnumDilDersIcerik = sonuc.EnumDil;
                                dersplanZorunludetay.EnumDilDersIcerik2 = sonuc.EnumDil2;
                                dersplanZorunludetay.EnumZorunluTur = sonuc.EnumDersSecimTipi;
                                dersplanZorunludetay.EnumDersSecimTip = sonuc.EnumDersSecimTipi;
                                if (sonuc.Kredi != null) dersplanZorunludetay.Kredi = (int)sonuc.Kredi;
                                item.YariyilZorunluDetayListe.Add(dersplanZorunludetay);
                            }
                            else
                            { // seçmeli ders ise. 
                                dersplanSecmelidetay.DersKod = sonuc.BolKodAd + " " + sonuc.DersKod;
                                if (sonuc.ECTSKredi != null) dersplanSecmelidetay.AKTS = (int)sonuc.ECTSKredi;
                                dersplanSecmelidetay.DersAd = dil == 1 ? sonuc.DersAd : sonuc.YabDersAd;
                                dersplanSecmelidetay.YabDersAd = sonuc.YabDersAd;
                                if (sonuc.FKDersPlanAnaID != null) dersplanSecmelidetay.DersPlanAnaID = (int)sonuc.FKDersPlanAnaID;
                                dersplanSecmelidetay.DersPlanID = sonuc.ID;
                                if (sonuc.EnumDersTipi != null) dersplanSecmelidetay.EnumDersTip = (int)sonuc.EnumDersTipi;
                                if (sonuc.DSaat != null) dersplanSecmelidetay.TSaat = (int)sonuc.DSaat;
                                if (sonuc.USaat != null) dersplanSecmelidetay.USaat = (int)sonuc.USaat;
                                dersplanSecmelidetay.EnumDilDersIcerik = sonuc.EnumDil;
                                dersplanSecmelidetay.EnumDilDersIcerik2 = sonuc.EnumDil2;
                                dersplanSecmelidetay.EnumZorunluTur = sonuc.EnumDersSecimTipi;
                                dersplanSecmelidetay.EnumDersSecimTip = sonuc.EnumDersSecimTipi;
                                if (sonuc.Kredi != null) dersplanSecmelidetay.Kredi = (int)sonuc.Kredi;
                                item.YariyilSecmeliDetayListe.Add(dersplanSecmelidetay);
                            }
                            liste.Add(item);
                        }
                        else
                        {
                            BolumDersPlanYariyilViewModel dersplanZorunludetay = new BolumDersPlanYariyilViewModel();
                            BolumDersPlanYariyilViewModel dersplanSecmelidetay = new BolumDersPlanYariyilViewModel();
                            if (sonuc.EnumDersSecimTipi == 1)
                            { // Zorunlu Ders ise 
                                dersplanZorunludetay.DersKod = sonuc.BolKodAd + " " + sonuc.DersKod;
                                if (sonuc.ECTSKredi != null) dersplanZorunludetay.AKTS = (int)sonuc.ECTSKredi;
                                dersplanZorunludetay.DersAd = dil == 1 ? sonuc.DersAd : sonuc.YabDersAd;
                                dersplanZorunludetay.YabDersAd = sonuc.YabDersAd;
                                if (sonuc.FKDersPlanAnaID != null) dersplanZorunludetay.DersPlanAnaID = (int)sonuc.FKDersPlanAnaID;
                                dersplanZorunludetay.DersPlanID = sonuc.ID;
                                if (sonuc.EnumDersTipi != null) dersplanZorunludetay.EnumDersTip = (int)sonuc.EnumDersTipi;
                                if (sonuc.DSaat != null) dersplanZorunludetay.TSaat = (int)sonuc.DSaat;
                                if (sonuc.USaat != null) dersplanZorunludetay.USaat = (int)sonuc.USaat;
                                dersplanZorunludetay.EnumDilDersIcerik = sonuc.EnumDil;
                                dersplanZorunludetay.EnumDilDersIcerik2 = sonuc.EnumDil2;
                                dersplanZorunludetay.EnumZorunluTur = sonuc.EnumDersSecimTipi;
                                dersplanZorunludetay.EnumDersSecimTip = sonuc.EnumDersSecimTipi;
                                if (sonuc.Kredi != null) dersplanZorunludetay.Kredi = (int)sonuc.Kredi;
                                liste.First(s => s.Yariyil == sonYariyil).YariyilZorunluDetayListe.Add(dersplanZorunludetay);
                            }
                            else
                            { // seçmeli ders ise. 
                                dersplanSecmelidetay.DersKod = sonuc.BolKodAd + " " + sonuc.DersKod;
                                if (sonuc.ECTSKredi != null) dersplanSecmelidetay.AKTS = (int)sonuc.ECTSKredi;
                                dersplanSecmelidetay.DersAd = dil == 1 ? sonuc.DersAd : sonuc.YabDersAd;
                                dersplanSecmelidetay.YabDersAd = sonuc.YabDersAd;
                                if (sonuc.FKDersPlanAnaID != null) dersplanSecmelidetay.DersPlanAnaID = (int)sonuc.FKDersPlanAnaID;
                                dersplanSecmelidetay.DersPlanID = sonuc.ID;
                                if (sonuc.EnumDersTipi != null) dersplanSecmelidetay.EnumDersTip = (int)sonuc.EnumDersTipi;
                                if (sonuc.DSaat != null) dersplanSecmelidetay.TSaat = (int)sonuc.DSaat;
                                if (sonuc.USaat != null) dersplanSecmelidetay.USaat = (int)sonuc.USaat;
                                dersplanSecmelidetay.EnumDilDersIcerik = sonuc.EnumDil;
                                dersplanSecmelidetay.EnumDilDersIcerik2 = sonuc.EnumDil2;
                                dersplanSecmelidetay.EnumZorunluTur = sonuc.EnumDersSecimTipi;
                                dersplanSecmelidetay.EnumDersSecimTip = sonuc.EnumDersSecimTipi;
                                if (sonuc.Kredi != null) dersplanSecmelidetay.Kredi = (int)sonuc.Kredi;
                                liste.First(s => s.Yariyil == sonYariyil).YariyilSecmeliDetayListe.Add(dersplanSecmelidetay);
                            }
                        }
                        sonYariyil = (int)sonuc.Yariyil;
                    }
                }
                liste = BolumEtiketDersPlan(dil, ProgramID, Yil, liste);
                bool yy0 = false;
                if (liste.FirstOrDefault(s => s.Yariyil == 9) != null || liste.FirstOrDefault(s => s.Yariyil == 0) != null) // 9.yy hazırlık dersleri varsa en başa getir. veya 0.yy en sona getir
                {
                    List<BolumDersPlanViewModel> yeni_liste = new List<BolumDersPlanViewModel>();
                    if (liste.FirstOrDefault(s => s.Yariyil == 9) != null)
                    {
                        yeni_liste.Add(liste.FirstOrDefault(s => s.Yariyil == 9));
                        liste.Remove(liste.FirstOrDefault(s => s.Yariyil == 9));
                    }
                    foreach (var item in liste)
                    {
                        if (item.Yariyil == 0)
                        {
                            yy0 = true;
                            continue; // 0.yy fakülte dersleri varsa en sona eklenecek. 
                        }
                        yeni_liste.Add(item);
                    }
                    if (yy0 == true) yeni_liste.Add(liste.FirstOrDefault(s => s.Yariyil == 0)); //0.yy en sona ekle
                    liste = yeni_liste;
                }



                if (ogrenimseviye == 2) /*sadece lisans bolumlerinde gelecek*/
                {
                    // üniversite ortak dersleri ekle
                    var univDersler = (from dersplan in db.OGRDersPlan
                                       join dersplanana in db.OGRDersPlanAna on dersplan.FKDersPlanAnaID equals dersplanana.ID
                                       where dersplan.Yil == Yil && dersplan.EnumOgretimTur == 1 && dersplanana.EnumDersAktif == 1 && dersplan.BolKodAd == "SAU" && dersplan.EnumDersBirimTipi == 3
                                       && dersplan.EnumEBSGosterim == null
                                       select new
                                       {
                                           dersplan.ID,
                                           dersplan.BolKodAd,
                                           dersplan.DersKod,
                                           dersplanana.DersAd,
                                           dersplanana.YabDersAd,
                                           dersplan.EnumDersSecimTipi,
                                           dersplan.EnumDersBirimTipi,
                                           dersplanana.DSaat,
                                           dersplanana.USaat,
                                           dersplanana.Kredi,
                                           dersplanana.ECTSKredi,
                                           dersplan.FKDersPlanAnaID,
                                           dersplanana.EnumDersTipi
                                       }).OrderBy(m => m.BolKodAd).ThenBy(m => m.DersKod).ThenBy(m => m.DersAd);
                    if (univDersler != null)
                    {
                        int sayac = 0;
                        foreach (var sonuc in univDersler)
                        {
                            BolumDersPlanViewModel item = new BolumDersPlanViewModel();
                            item.Yariyil = 11; // üniversite dersleri 11. yy da
                            BolumDersPlanYariyilViewModel dersplanSecmelidetay = new BolumDersPlanYariyilViewModel();
                            dersplanSecmelidetay.DersKod = sonuc.BolKodAd + " " + sonuc.DersKod;
                            if (sonuc.ECTSKredi != null) dersplanSecmelidetay.AKTS = (int)sonuc.ECTSKredi;
                            dersplanSecmelidetay.DersAd = dil == 1 ? sonuc.DersAd : sonuc.YabDersAd;
                            dersplanSecmelidetay.YabDersAd = sonuc.YabDersAd;
                            if (sonuc.FKDersPlanAnaID != null) dersplanSecmelidetay.DersPlanAnaID = (int)sonuc.FKDersPlanAnaID;
                            dersplanSecmelidetay.DersPlanID = sonuc.ID;
                            if (sonuc.EnumDersTipi != null) dersplanSecmelidetay.EnumDersTip = (int)sonuc.EnumDersTipi;
                            if (sonuc.DSaat != null) dersplanSecmelidetay.TSaat = (int)sonuc.DSaat;
                            if (sonuc.USaat != null) dersplanSecmelidetay.USaat = (int)sonuc.USaat;
                            dersplanSecmelidetay.EnumZorunluTur = (int)sonuc.EnumDersSecimTipi;
                            dersplanSecmelidetay.EnumDersSecimTip = sonuc.EnumDersSecimTipi;
                            if (sonuc.Kredi != null) dersplanSecmelidetay.Kredi = (int)sonuc.Kredi;
                            item.YariyilSecmeliDetayListe.Add(dersplanSecmelidetay);
                            if (sayac == 0)
                                liste.Add(item);
                            else
                                liste.First(s => s.Yariyil == 11).YariyilSecmeliDetayListe.Add(dersplanSecmelidetay);
                            sayac++;
                        }
                    }
                }
                return liste;
            }
        }

        #endregion
        public static List<BolumPlanDto> PlanGetirIntibak(int FKCapTanimID, int yil, int bolumID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                List<BolumPlanDto> liste = new List<BolumPlanDto>();
                //var veri = db.OGRDersPlan.Where(x => 
                //    x.FKBirimID == bolumID && 
                //    x.Yil == yil && 
                //    (x.Yariyil != 0 && x.Yariyil != 9) && 
                //    (x.EnumEBSGosterim == null || x.EnumEBSGosterim == 1) &&
                //    //x.OGRDersPlanAna.EnumDersAktif == 1 &&
                //    x.EnumDersSecimTipi == 1
                //    ).Select(x => new
                //{
                //    x.ID,
                //    x.FKDersPlanAnaID,
                //    x.OGRDersPlanAna.DersAd,
                //    x.Yariyil,
                //    x.OGRDersPlanAna.DSaat,
                //    x.OGRDersPlanAna.ECTSKredi
                //}).OrderBy(o => o.DersAd);
                var veri = db.OGRDersPlan.Where(x =>
                    x.FKBirimID == bolumID &&
                    x.Yil == yil &&
                    x.EnumDonem != 3 &&
                    (x.Yariyil != 0 && x.Yariyil != 9) &&
                    (x.EnumEBSGosterim == null || x.EnumEBSGosterim == 1) &&
                    //x.OGRDersPlanAna.EnumDersAktif == 1 &&
                    x.EnumDersPlanKapsam == 1 &&
                    !(x.Yariyil == 7 && x.OGRDersPlanAna.EnumKokDersTipi == 3) &&
                    x.EnumDersSecimTipi == (int)Sabis.Enum.EnumDersSecimTipi.ZORUNLU
                    ).Select(x => new
                    {
                        x.ID,
                        x.FKDersPlanAnaID,
                        x.OGRDersPlanAna.DersAd,
                        x.Yariyil,
                        x.OGRDersPlanAna.DSaat,
                        x.OGRDersPlanAna.ECTSKredi
                    }).OrderBy(o => o.Yariyil).ThenBy(a => a.DersAd);
                if (veri != null)
                {
                    foreach (var item in veri.ToList())
                    {
                        //if (!db.OGRCapDersPlan.Any(x => x.FKCapTanimID == FKCapTanimID && x.FKCapDersPlanID == item.ID && !x.Silindi))
                        if (!db.OGRCapDersPlan.Any(x => x.FKCapTanimID == FKCapTanimID && x.FKAnaDersPlanID == item.ID && !x.Silindi))
                        {
                            var model = new BolumPlanDto();
                            model.FKDersPlanID = item.ID;
                            model.FKDersPlanAnaID = item.FKDersPlanAnaID;
                            model.DersAd = item.DersAd;
                            model.Yariyil = item.Yariyil;
                            model.DersSaat = item.DSaat;
                            model.AKTS = item.ECTSKredi;
                            liste.Add(model);
                        }

                    }

                    //var etiketDersler = (from saatBilgi in db.OGRTNMSaatBilgi
                    //                     join etiketDers in db.OGRTNMDersEtiket on saatBilgi.FKEtiketID equals etiketDers.ID
                    //                     where
                    //                        saatBilgi.Yil == yil &&
                    //                        saatBilgi.FKBolumPrBirimID == bolumID &&
                    //                        saatBilgi.AKTS != 0
                    //                     select new
                    //                     {
                    //                         saatBilgi.ID,
                    //                         etiketDers.Etiket,
                    //                         etiketDers.EtiketEng,
                    //                         saatBilgi.EnumDersPlanZorunlu,
                    //                         saatBilgi.FKEtiketID,
                    //                         saatBilgi.AKTS,
                    //                         saatBilgi.Yariyil,
                    //                         etiketDers.Renk,
                    //                         saatBilgi.Sayi
                    //                     }).OrderBy(m => m.Yariyil).ThenBy(m => m.Etiket).ToList();
                    //if (etiketDersler != null)
                    //{
                    //    foreach (var item in etiketDersler)
                    //    {
                    //        for (int i = 0; i < item.Sayi; i++)
                    //        {
                    //            var model = new BolumPlanDto();
                    //            model.FKCapTanimID = FKCapTanimID;
                    //            model.FKDersPlanID = item.ID;
                    //            model.FKDersPlanAnaID = item.FKEtiketID;
                    //            model.DersAd = item.Etiket;
                    //            model.Yariyil = item.Yariyil;
                    //            //model.DersSaat = 333;
                    //            model.AKTS = item.AKTS;
                    //            liste.Add(model);
                    //        }
                    //    }
                    //}








                    return liste;
                }
                else
                {
                    return null;
                }
            }
        }

        public static List<BolumEtiketPlanDto> SecmeliIntibakYapilacakListe(int FKCapTanimID, int Yil, int FKAltBirimID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                if (db.OGRCapYandalTanim.Any(x => x.ID == FKCapTanimID && !x.Silindi))
                {
                    //var dersplanlar = CAPDERSMAIN.CapSecmeliPlanGetir(FKCapTanimID, FKAltBirimID, Yil).OrderBy(x => x.YariYil).ToList();
                    var dersplanlar = CAPDERSMAIN.AnaSecmeliPlanGetir(FKCapTanimID, FKAltBirimID, Yil).OrderBy(x => x.YariYil).ToList();
                    return dersplanlar;
                }
                else
                {
                    return null;
                }
            }
        }

        public static List<IntibakPlanDto> IntibakPlanGetir(int FKCapTanimID, int Yil)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                List<IntibakPlanDto> liste = new List<IntibakPlanDto>();
                var veri = db.OGRCapDersPlan.Where(x => x.FKCapTanimID == FKCapTanimID && x.Yil == Yil && !x.Silindi);
                if (veri != null)
                {
                    foreach (var item in veri)
                    {
                        var model = new IntibakPlanDto();
                        model.ID = item.ID;
                        model.FKCapTanimID = item.FKCapTanimID;
                        model.FkAnaDersPlanAnaID = item.FKAnaDersPlanAnaID;
                        model.FKCapDersPlanAnaID = item.FKCapDersPlanAnaID;
                        model.FKAnaDersPlanID = item.FKAnaDersPlanID;
                        model.FKCapDersPlanID = item.FKCapDersPlanID;
                        model.FKAnaDersPlanAnaAd = item.OGRDersPlanAna.DersAd;
                        model.FKCapDersPlanAnaAd = item.OGRDersPlanAna1.DersAd;
                        //model.Yariyil = item.OGRDersPlan.Yariyil;
                        //model.DersSaat = item.OGRDersPlanAna.DSaat;
                        //model.AKTS = item.OGRDersPlanAna.ECTSKredi;
                        model.DersSaat = item.OGRDersPlanAna1.DSaat;
                        model.Yariyil = item.OGRDersPlan1.Yariyil;
                        model.AKTS = item.OGRDersPlanAna1.ECTSKredi;
                        model.YariyilAna = item.OGRDersPlan1.Yariyil;
                        model.AKTSAna = item.OGRDersPlanAna.ECTSKredi;
                        liste.Add(model);
                    }




                    //if (db.OGRCapYandalTanim.Any(x => x.ID == FKCapTanimID))
                    //{
                    //    var FKAltBirimID = db.OGRCapYandalTanim.FirstOrDefault(x => x.ID == FKCapTanimID).FKAltBirimID;
                    //    var etiketDersler = (from saatBilgi in db.OGRTNMSaatBilgi
                    //                         join etiketDers in db.OGRTNMDersEtiket on saatBilgi.FKEtiketID equals etiketDers.ID
                    //                         where
                    //                            saatBilgi.Yil == 2016 &&
                    //                            saatBilgi.FKBolumPrBirimID == FKAltBirimID &&
                    //                            saatBilgi.AKTS != 0
                    //                         select new
                    //                         {
                    //                             saatBilgi.ID,
                    //                             etiketDers.Etiket,
                    //                             etiketDers.EtiketEng,
                    //                             saatBilgi.EnumDersPlanZorunlu,
                    //                             saatBilgi.FKEtiketID,
                    //                             saatBilgi.AKTS,
                    //                             saatBilgi.Yariyil,
                    //                             etiketDers.Renk,
                    //                             saatBilgi.Sayi
                    //                         }).OrderBy(m => m.Yariyil).ThenBy(m => m.Etiket).ToList();
                    //    if (etiketDersler != null)
                    //    {
                    //        foreach (var item in etiketDersler)
                    //        {
                    //            for (int i = 0; i < item.Sayi; i++)
                    //            {
                    //                var model = new IntibakPlanDto();
                    //                model.FKCapTanimID = FKCapTanimID;
                    //                model.FKCapDersPlanID = item.ID;
                    //                model.FKCapDersPlanAnaID = item.FKEtiketID;
                    //                model.FKCapDersPlanAnaAd = item.Etiket;
                    //                model.Yariyil = item.Yariyil;
                    //                //model.DersSaat = 333;
                    //                model.AKTS = item.AKTS;
                    //                liste.Add(model);
                    //            }
                    //        }
                    //    }
                    //}


















                    return liste.OrderBy(x => x.Yariyil).ToList();
                }
                else
                {
                    return null;
                }
            }
        }

        public static List<BolumEtiketDto> IntibakSecmeliPlanGetir(int FKCapTanimID, int Yil)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var veri = (from s in db.OGRCapSecmeliTanim
                            where
                            s.FKCapTanimID == FKCapTanimID &&
                            s.Silindi == false &&
                            //s.Yil == Yil
                            s.OGRTNMSaatBilgi.Yil == Yil
                            select new BolumEtiketDto
                            {
                                ID = s.ID,
                                FKCapTanimID = s.FKCapTanimID,
                                FKAnaEtiketID = s.FKAnaEtiketID,
                                FKCapEtiketID = s.FKCapEtiketID,
                                AnaEtiket = s.OGRTNMDersEtiket.Etiket,
                                CapEtiket = s.OGRTNMDersEtiket1.Etiket,
                                YariYil = s.OGRTNMSaatBilgi.Yariyil,
                                AKTS = s.OGRTNMSaatBilgi.AKTS,
                                Adet = s.Adet
                            }).OrderBy(x => x.YariYil).ToList();
                return veri;

                //List<BolumEtiketDto> liste = new List<BolumEtiketDto>();
                //var veri = db.OGRCapSecmeliTanim.Where(x => x.FKCapTanimID == FKCapTanimID && !x.Silindi);
                //if (veri != null)
                //{
                //    var bolumID = db.OGRCapYandalTanim.FirstOrDefault(x => x.ID == FKCapTanimID).FKAnaBirimID;
                //    foreach (var item in veri)
                //    {

                //        var aetiket = (from saatBilgi in db.OGRTNMSaatBilgi
                //                       join etiketDers in db.OGRTNMDersEtiket on saatBilgi.FKEtiketID equals etiketDers.ID
                //                       where saatBilgi.Yil == Yil && saatBilgi.FKBolumPrBirimID == bolumID && etiketDers.ID == item.FKAnaEtiketID && saatBilgi.ID == item.FKSaatBilgiID
                //                       select new
                //                       {
                //                           saatBilgi.ID,
                //                           etiketDers.Etiket,
                //                           etiketDers.EtiketEng,
                //                           saatBilgi.EnumDersPlanZorunlu,
                //                           saatBilgi.FKEtiketID,
                //                           saatBilgi.AKTS,
                //                           saatBilgi.Yariyil,
                //                           etiketDers.Renk,
                //                           saatBilgi.Sayi
                //                       }).FirstOrDefault();

                //        if (aetiket != null)
                //        {
                //            var model = new BolumEtiketDto();
                //            model.ID = item.ID;
                //            model.FKCapTanimID = item.FKCapTanimID;
                //            model.FKAnaEtiketID = item.FKAnaEtiketID;
                //            model.FKCapEtiketID = item.FKCapEtiketID;
                //            model.Adet = item.Adet;
                //            model.AnaEtiket = aetiket.Etiket;
                //            model.CapEtiket = db.OGRTNMDersEtiket.FirstOrDefault(x => x.ID == item.FKCapEtiketID).Etiket;
                //            model.YariYil = aetiket.Yariyil;
                //            model.AKTS = aetiket.AKTS;
                //            liste.Add(model);
                //        }

                //    }
                //    var test = liste;
                //    return liste;
                //}
                //return null;
            }
        }


        public static List<int> AnaBirimIDs()
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return (from br in db.OGRCapYandalTanim
                        group br.FKAnaBirimID by br.FKAnaBirimID into sg
                        orderby sg.Key
                        select sg.Key).ToList();
                //return db.OGRCapYandalTanim.Select(x => x.FKAnaBirimID).ToList();
            }
        }
        public static List<int> AltBirimIDs()
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return (from br in db.OGRCapYandalTanim
                        group br.FKAltBirimID by br.FKAltBirimID into sg
                        orderby sg.Key
                        select sg.Key).ToList();
                //return db.OGRCapYandalTanim.Select(x => x.FKAltBirimID).ToList();
            }
        }

        public static List<BolumDto> TumBolumler()
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var birimler = Sabis.Client.BirimIslem.Instance.getirBirimListe(CAPDERSMAIN.BolumListe(), false, false).OrderBy(x => x.BirimAdi).ToList();
                List<BolumDto> bolumdtoliste = new List<BolumDto>();
                foreach (var item in birimler)
                {
                    var model = new BolumDto();
                    model.BolumID = item.ID;
                    model.BolumAd = item.BirimAdi;
                    model.FKFakulteAd = Sabis.Client.BirimIslem.Instance.getirBagliFakulte(item.ID, true).BirimAdi;
                    bolumdtoliste.Add(model);
                }
                return bolumdtoliste.OrderBy(x => x.FKFakulteAd).ToList();
            }
        }
        public static List<OGRCapYandalTanimDto> AnaBirimler()
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                List<OGRCapYandalTanimDto> liste = new List<OGRCapYandalTanimDto>();

                var veri = Sabis.Client.BirimIslem.Instance.getirBirimListe(CAPDERSMAIN.AnaBirimIDs(), true, false).Select(x => new OGRCapYandalTanimDto
                {
                    UstBirimID = x.UstBirimID.Value,
                    FKAnaBirimID = x.ID,
                    FKAltBirimID = db.Birimler.FirstOrDefault(y => y.ID == x.UstBirimID).ID,
                    FKAnaBolumID = db.Birimler.FirstOrDefault(y => y.ID == x.UstBirimID).ID,
                    AnaBirimAd = x.BirimAdi,
                    AnaBolumAd = db.Birimler.FirstOrDefault(y => y.ID == db.Birimler.FirstOrDefault(z => z.ID == x.ID).UstBirimID).BirimAdi,
                }).OrderBy(o => o.BolumAd).ToList();

                foreach (var item in veri)
                {
                    var model = new OGRCapYandalTanimDto();
                    model.FKAnaBirimID = item.FKAnaBirimID;
                    model.AnaBirimAd = item.AnaBirimAd;
                    model.FKFakulteAd = Sabis.Client.BirimIslem.Instance.getirBagliFakulte(item.FKAnaBirimID, true).BirimAdi;
                    liste.Add(model);
                }
                return liste.OrderBy(x => x.FKFakulteAd).ToList();
            }
        }
        public static List<OGRCapYandalTanimDto> AltBirimler()
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                List<OGRCapYandalTanimDto> liste = new List<OGRCapYandalTanimDto>();

                var listID = CAPDERSMAIN.AltBirimIDs();
                //var listEgitimFakultesi = new int[] { 21406, 21407, 2267, 2269, 2270, 2271, 25407, 25408, 25409, 25410, 25411, 25412 };
                //listID.AddRange(listEgitimFakultesi);

                var veri = Sabis.Client.BirimIslem.Instance.getirBirimListe(listID, true, false).Select(x => new OGRCapYandalTanimDto
                {
                    FKAnaBirimID = x.ID,
                    FKAltBirimID = db.Birimler.FirstOrDefault(y => y.ID == x.UstBirimID).ID,
                    FKAnaBolumID = db.Birimler.FirstOrDefault(y => y.ID == x.UstBirimID).ID,
                    AnaBirimAd = x.BirimAdi,
                    AnaBolumAd = db.Birimler.FirstOrDefault(y => y.ID == db.Birimler.FirstOrDefault(z => z.ID == x.ID).UstBirimID).BirimAdi,
                }).OrderBy(o => o.BolumAd).ToList();

                //foreach (var item in listEgitimFakultesi)
                //{
                //    var k = veri.FirstOrDefault(x => x.FKAnaBirimID == item);
                //    k.UstBirimID = 117;
                //}

                foreach (var item in veri)
                {
                    var model = new OGRCapYandalTanimDto();
                    model.FKAnaBirimID = item.FKAnaBirimID;
                    model.AnaBirimAd = item.AnaBirimAd;
                    model.FKFakulteAd = Sabis.Client.BirimIslem.Instance.getirBagliFakulte(item.FKAnaBirimID, true).BirimAdi;
                    liste.Add(model);
                }
                return liste.OrderBy(x => x.FKFakulteAd).ToList();
            }
        }
        public static List<OGRCapYandalTanimDto> CapYandalTanimListe()
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                List<OGRCapYandalTanimDto> liste = new List<OGRCapYandalTanimDto>();
                var veri = db.OGRCapYandalTanim.Where(c => c.Silindi == false && c.EnumGecisTipi == (int)Sabis.Enum.EnumGecisProgramTipi.CAP).Select(x => new OGRCapYandalTanimDto
                {
                    ID = x.ID,
                    FKAnaBirimID = x.FKAnaBirimID,
                    AnaBirimAd = db.Birimler.FirstOrDefault(y => y.ID == x.FKAnaBirimID).BirimAdi,
                    FKAltBirimID = x.FKAltBirimID,
                    AltBirimAd = db.Birimler.FirstOrDefault(y => y.ID == x.FKAltBirimID).BirimAdi,
                    Silindi = x.Silindi,
                    Kullanici = x.Kullanici,
                    TarihKayit = x.TarihKayit,
                    Kontenjan = x.CapYandalKontenjan.FirstOrDefault().Adet

                }).ToList();

                foreach (var item in veri)
                {
                    var fakulte = Sabis.Client.BirimIslem.Instance.getirBagliFakulte(item.FKAnaBirimID, false);
                    var capFakulte = Sabis.Client.BirimIslem.Instance.getirBagliFakulte(item.FKAltBirimID, false);
                    var model = new OGRCapYandalTanimDto();
                    model.ID = item.ID;
                    model.FKAnaBirimID = item.FKAnaBirimID;
                    model.AnaBirimAd = db.Birimler.FirstOrDefault(y => y.ID == item.FKAnaBirimID).BirimAdi;
                    model.FKAltBirimID = item.FKAltBirimID;
                    model.AltBirimAd = db.Birimler.FirstOrDefault(y => y.ID == item.FKAltBirimID).BirimAdi;
                    model.FKFakulteAd = fakulte != null ? fakulte.BirimAdi : "";
                    model.FKCapFakulteAd = capFakulte != null ? capFakulte.BirimAdi : "";
                    model.Kontenjan = item.Kontenjan;
                    model.Silindi = item.Silindi;
                    model.Kullanici = item.Kullanici;
                    model.TarihKayit = item.TarihKayit;
                    liste.Add(model);
                }
                return liste;
            }
        }
        public static List<CapDersPlanDto> CapDersPlanGetir(int FKAnaBirimID, int FKAltBirimID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                //çap tanım eklenmişse
                if (db.OGRCapYandalTanim.Any(x => x.FKAnaBirimID == FKAnaBirimID && x.FKAltBirimID == FKAltBirimID && !x.Silindi))
                {
                    var FKCapTanimID = db.OGRCapYandalTanim.FirstOrDefault(x => x.FKAnaBirimID == FKAnaBirimID && x.FKAltBirimID == FKAltBirimID && !x.Silindi).ID;
                    return db.OGRCapDersPlan.Where(x => x.FKCapTanimID == FKCapTanimID && !x.Silindi).Select(y => new CapDersPlanDto
                    {
                        ID = y.ID,
                        FKCapTanimID = y.FKCapTanimID,
                        FKAnaDersPlanAnaID = y.FKAnaDersPlanAnaID,
                        FKAnaDersPlanID = y.FKAnaDersPlanID,
                        FKCapDersPlanAnaID = y.FKCapDersPlanAnaID,
                        FKCapDersPLanID = y.FKCapDersPlanID,
                        FKAnaDersPlanAnaAd = db.OGRDersPlanAna.FirstOrDefault(z => z.ID == y.FKAnaDersPlanAnaID).DersAd,
                        FKCapDersPlanAnaAd = db.OGRDersPlanAna.FirstOrDefault(z => z.ID == y.FKCapDersPlanAnaID).DersAd,
                        Silindi = y.Silindi
                    }).ToList();
                }
                return null;
            }
        }
        public static List<int> BolumListe()
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                List<int> egitimFakultesiProgramListesi = Sabis.Client.BirimIslem.Instance.getirAltBirimler(117, false, false, new List<int>(new int[] { 23, 3 })).Select(x => x.ID).ToList();
                List<int> bolumListesi = db.Birimler.Where(x => x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.Bolum && x.Aktif && x.UstBirimID != 117).Select(x => x.ID).ToList();


                return egitimFakultesiProgramListesi.Concat(bolumListesi).ToList();
            }
        }
        public static bool CapTanimSil(int FKCapTanimID, string KullaniciAdi)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                if (db.OGRCapYandalTanim.Any(x => x.ID == FKCapTanimID))
                {
                    if (db.OGRCapDersPlan.Any(x => x.FKCapTanimID == FKCapTanimID))
                    {
                        var tanimplanlar = db.OGRCapDersPlan.Where(x => x.FKCapTanimID == FKCapTanimID).ToList();
                        foreach (var item in tanimplanlar)
                        {
                            var tanimplan = db.OGRCapDersPlan.FirstOrDefault(x => x.ID == item.ID);
                            tanimplan.Silindi = true;
                            tanimplan.TarihSilinme = DateTime.Now;
                            tanimplan.Kullanici = KullaniciAdi;
                        }
                        db.SaveChanges();
                    }
                    var model = db.OGRCapYandalTanim.FirstOrDefault(x => x.ID == FKCapTanimID);
                    model.Silindi = true;
                    model.Kullanici = KullaniciAdi;
                    db.SaveChanges();

                    var capyandalkontenjan = db.CapYandalKontenjan.FirstOrDefault(x => x.FKCapYandalTanimID == FKCapTanimID && !x.Silindi);
                    capyandalkontenjan.Silindi = true;
                    db.SaveChanges();

                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool CapPlanEkle(int FKAnaBirimID, int FKAltBirimID, int FKAnaDersPlanID, int FKCapDersPlanID, string kullanici, int yil)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                if (db.OGRCapYandalTanim.Any(x => x.FKAnaBirimID == FKAnaBirimID & x.FKAltBirimID == FKAltBirimID))
                {
                    var FKAnaDersPlanAnaID = db.OGRDersPlan.FirstOrDefault(x => x.ID == FKAnaDersPlanID).FKDersPlanAnaID;
                    var FKCapDersPlanAnaID = db.OGRDersPlan.FirstOrDefault(x => x.ID == FKCapDersPlanID).FKDersPlanAnaID;
                    if (!db.OGRCapDersPlan.Any(x => x.FKAnaDersPlanAnaID == FKAnaDersPlanAnaID && x.FKCapDersPlanAnaID == FKCapDersPlanAnaID && x.FKAnaDersPlanID == FKAnaDersPlanID && x.FKCapDersPlanID == FKCapDersPlanID && !x.Silindi))
                    {
                        var model = new OGRCapDersPlan();
                        model.FKCapTanimID = db.OGRCapYandalTanim.FirstOrDefault(x => x.FKAnaBirimID == FKAnaBirimID & x.FKAltBirimID == FKAltBirimID && !x.Silindi).ID;
                        model.TarihKayit = DateTime.Now;
                        model.Kullanici = kullanici;
                        model.Yil = yil;
                        model.FKAnaDersPlanAnaID = FKAnaDersPlanAnaID.Value;
                        model.FKCapDersPlanAnaID = FKCapDersPlanAnaID.Value;
                        model.FKAnaDersPlanID = FKAnaDersPlanID;
                        model.FKCapDersPlanID = FKCapDersPlanID;
                        model.Silindi = false;
                        db.OGRCapDersPlan.Add(model);
                        db.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool CapPlanSil(int CapDersPlanID, string kullanici)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                if (db.OGRCapDersPlan.Any(x => x.ID == CapDersPlanID))
                {
                    var model = db.OGRCapDersPlan.FirstOrDefault(x => x.ID == CapDersPlanID);
                    model.Silindi = true;
                    model.TarihSilinme = DateTime.Now;
                    model.Kullanici = kullanici;
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        #region YENISEKME
        public static List<int> FakulteIDList()
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return (from br in db.Birimler
                        where
                        (br.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.Fakulte || br.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.YuksekOkul) &&
                        br.UstBirimID == 21398 &&
                        br.Aktif == true
                        select br.ID
                        ).ToList();
            }
        }
        public static List<DtoBirimler> FakulteGetir(List<int> capBirimIDler, bool capKoordinator)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                if (capKoordinator)
                {
                    return Sabis.Client.BirimIslem.Instance.getirBirimListe(capBirimIDler, false, false).OrderBy(x => x.BirimAdi).ToList();
                }
                else
                {
                    return Sabis.Client.BirimIslem.Instance.getirBirimListe(FakulteIDList(), false, false).OrderBy(x => x.BirimAdi).ToList();
                }

            }
        }
        public static List<DtoBirimler> BolumGetir(int FKFakulteID, List<int> capBirimIDler, bool capKoordinator)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                if (capKoordinator)
                {
                    //List<int> birimTipleri = new List<int>() { 3 };
                    //return Sabis.Client.BirimIslem.Instance.getirAltBirimler(FKFakulteID, false, false, birimTipleri, null, null, null).OrderBy(x => x.BirimAdi).ToList();

                    List<DtoBirimler> birimList = new List<DtoBirimler>();

                    if (FKFakulteID == 117)
                    {
                        //birimList.AddRange(new int[] { 21406, 21407, 2267, 2269, 2270, 2271, 25407, 25408, 25409, 25410, 25411, 25412 });
                        birimList.AddRange(Sabis.Client.BirimIslem.Instance.getirAltBirimler(117, false, false, new List<int>(new int[] { 23 })));
                    }

                    birimList.AddRange(Sabis.Client.BirimIslem.Instance.getirBirimListe(capBirimIDler, false, false));

                    return birimList.OrderBy(x => x.BirimAdi).ToList();

                }
                else
                {
                    List<int> birimTipleri = new List<int>() { 3, 23 };
                    return Sabis.Client.BirimIslem.Instance.getirAltBirimler(FKFakulteID, false, false, birimTipleri, null, null, null).OrderBy(x => x.BirimAdi).ToList();
                }
            }
        }
        public static List<CapProgramDto> CapProgramListe(int FKAnaBirimID, int Yil)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                List<CapProgramDto> liste = new List<CapProgramDto>();
                //var veri = db.OGRCapYandalTanim.Where(x => x.FKAnaBirimID == FKAnaBirimID && !x.Silindi);
                var veri = db.OGRCapYandalTanim.Where(x => x.FKAltBirimID == FKAnaBirimID && !x.Silindi);
                if (veri != null)
                {
                    foreach (var item in veri.ToList())
                    {
                        //var planlar = CAPDERSMAIN.CapPlanGetir2(item.ID, item.FKAltBirimID, Yil);
                        var planlar = CAPDERSMAIN.CapPlanGetir2(item.ID, item.FKAnaBirimID, Yil);
                        //var etiket = CAPDERSMAIN.CapSecmeliPlanGetir2(item.ID, item.FKAltBirimID, Yil).OrderBy(x => x.YariYil).ToList();
                        var etiket = CAPDERSMAIN.CapSecmeliPlanGetir2(item.ID, item.FKAnaBirimID, Yil).OrderBy(x => x.YariYil).ToList();

                        var model = new CapProgramDto();
                        model.FKCapTanimID = item.ID;
                        //model.FKFakulteID = Sabis.Client.BirimIslem.Instance.getirBagliFakulte(item.FKAltBirimID, true).ID;
                        model.FKFakulteID = Sabis.Client.BirimIslem.Instance.getirBagliFakulte(item.FKAnaBirimID, true).ID;
                        //model.FakulteAd = Sabis.Client.BirimIslem.Instance.getirBagliFakulte(item.FKAltBirimID, true).BirimAdi;
                        model.FakulteAd = Sabis.Client.BirimIslem.Instance.getirBagliFakulte(item.FKAnaBirimID, true).BirimAdi;
                        //model.FKBolumID = item.FKAltBirimID;
                        model.FKBolumID = item.FKAnaBirimID;
                        //model.BolumAd = Sabis.Client.BirimIslem.Instance.getirBirimbyID(item.FKAltBirimID).BirimAdi;
                        model.BolumAd = Sabis.Client.BirimIslem.Instance.getirBirimbyID(item.FKAnaBirimID).BirimAdi;
                        //model.DersAdedi = planlar.Count() + etiket.Count();

                        #region AKTS Hesaplama

                        List<BolumPlanDto> listee = new List<BolumPlanDto>();
                        var verii = db.OGRDersPlan.Where(x =>
                            x.FKBirimID == FKAnaBirimID &&
                            x.Yil == Yil &&
                            x.EnumDonem != 3 &&
                            (x.Yariyil != 0 && x.Yariyil != 9) &&
                            (x.EnumEBSGosterim == null || x.EnumEBSGosterim == 1) &&
                            //x.OGRDersPlanAna.EnumDersAktif == 1 &&
                            x.EnumDersPlanKapsam == 1 &&
                            !(x.Yariyil == 7 && x.OGRDersPlanAna.EnumKokDersTipi == 3) &&
                            x.EnumDersSecimTipi == (int)Sabis.Enum.EnumDersSecimTipi.ZORUNLU
                            ).Select(x => new
                            {
                                x.ID,
                                x.FKDersPlanAnaID,
                                x.OGRDersPlanAna.DersAd,
                                x.Yariyil,
                                x.OGRDersPlanAna.DSaat,
                                x.OGRDersPlanAna.ECTSKredi
                            }).OrderBy(o => o.Yariyil).ThenBy(a => a.DersAd);
                        if (veri != null)
                        {
                            foreach (var itemm in verii.ToList())
                            {
                                if (!db.OGRCapDersPlan.Any(x => x.FKCapTanimID == item.ID && x.FKCapDersPlanAnaID == itemm.FKDersPlanAnaID && !x.Silindi))
                                {
                                    var mod = new BolumPlanDto();
                                    mod.AKTS = itemm.ECTSKredi;
                                    listee.Add(mod);
                                }

                            }
                            
                        }
                        else
                        {
                            return null;
                        }
                        var capsecmeli_dersplanlar = CAPDERSMAIN.CapSecmeliPlanGetir(item.ID, FKAnaBirimID, Yil).OrderBy(x => x.YariYil).ToList();
                        #endregion
                        model.DersAdedi = listee.Count() + capsecmeli_dersplanlar.Sum(x => x.Sayi.Value);
                        model.AKTS = listee.Sum(x => x.AKTS) + capsecmeli_dersplanlar.Sum(x => x.AKTS);
                        //model.AKTS = planlar.Sum(x => x.AKTS) + etiket.Sum(x => x.AKTS);
                        if (db.CapYandalKontenjan.Any(x => x.FKCapYandalTanimID == item.ID))
                        {
                            model.Kontenjan = db.CapYandalKontenjan.FirstOrDefault(x => x.FKCapYandalTanimID == item.ID).Adet;
                        }
                        else
                        {
                            model.Kontenjan = 0;
                        }

                        liste.Add(model);


                    }
                    return liste;
                }
                return null;
            }
        }
        public static CapProgramDto CapProgramBilgi(int FKCapTanimID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                if (db.OGRCapYandalTanim.Any(x => x.ID == FKCapTanimID && !x.Silindi))
                {
                    var veri = db.OGRCapYandalTanim.FirstOrDefault(x => x.ID == FKCapTanimID && !x.Silindi);
                    var model = new CapProgramDto();
                    //model.FKFakulteID = Sabis.Client.BirimIslem.Instance.getirBagliFakulte(veri.FKAltBirimID, true).ID;
                    //model.FakulteAd = Sabis.Client.BirimIslem.Instance.getirBagliFakulte(veri.FKAltBirimID, true).BirimAdi;
                    //model.FKBolumID = veri.FKAltBirimID;
                    //model.BolumAd = Sabis.Client.BirimIslem.Instance.getirBirimbyID(veri.FKAltBirimID).BirimAdi;
                    model.FKFakulteID = Sabis.Client.BirimIslem.Instance.getirBagliFakulte(veri.FKAnaBirimID, true).ID;
                    model.FakulteAd = Sabis.Client.BirimIslem.Instance.getirBagliFakulte(veri.FKAnaBirimID, true).BirimAdi;
                    model.FKBolumID = veri.FKAnaBirimID;
                    model.BolumAd = Sabis.Client.BirimIslem.Instance.getirBirimbyID(veri.FKAnaBirimID).BirimAdi;

                    //model.AnaFKFakulteID = Sabis.Client.BirimIslem.Instance.getirBagliFakulte(veri.FKAnaBirimID, true).ID;
                    //model.AnaFakulteAd = Sabis.Client.BirimIslem.Instance.getirBagliFakulte(veri.FKAnaBirimID, true).BirimAdi;
                    //model.AnaFKBolumID = veri.FKAnaBirimID;
                    //model.AnaBolumAd = Sabis.Client.BirimIslem.Instance.getirBirimbyID(veri.FKAnaBirimID).BirimAdi;
                    model.AnaFKFakulteID = Sabis.Client.BirimIslem.Instance.getirBagliFakulte(veri.FKAltBirimID, true).ID;
                    model.AnaFakulteAd = Sabis.Client.BirimIslem.Instance.getirBagliFakulte(veri.FKAltBirimID, true).BirimAdi;
                    model.AnaFKBolumID = veri.FKAltBirimID;
                    model.AnaBolumAd = Sabis.Client.BirimIslem.Instance.getirBirimbyID(veri.FKAltBirimID).BirimAdi;
                    return model;
                }
                else
                {
                    return null;
                }
            }
        }
        public static List<BolumPlanDto> CapDerslerGetir(int FKCapTanimID, int Yil)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                if (db.OGRCapYandalTanim.Any(x => x.ID == FKCapTanimID && !x.Silindi))
                {
                    //var FKAnaBirimID = db.OGRCapYandalTanim.FirstOrDefault(x => x.ID == FKCapTanimID && !x.Silindi).FKAnaBirimID;
                    var FKAnaBirimID = db.OGRCapYandalTanim.FirstOrDefault(x => x.ID == FKCapTanimID && !x.Silindi).FKAltBirimID;
                    //var dersplanlar = CAPDERSMAIN.BolumPlanGetir(2016, FKAnaBirimID);
                    //var dersplanlar = CAPDERSMAIN.CapPlanGetir(FKCapTanimID, FKAnaBirimID, Yil).OrderBy(x => x.Yariyil).ToList();
                    var dersplanlar = CAPDERSMAIN.CapPlanGetir(FKCapTanimID, FKAnaBirimID, Yil).OrderBy(x => x.Yariyil).ToList();
                    return dersplanlar;
                }
                else
                {
                    return null;
                }
            }
        }
        public static List<BolumEtiketPlanDto> CapSecmeliDerslerGetir(int FKCapTanimID, int Yil)
        {
            //aslında dersetiketleri getiriyor.
            using (UYSv2Entities db = new UYSv2Entities())
            {
                if (db.OGRCapYandalTanim.Any(x => x.ID == FKCapTanimID && !x.Silindi))
                {
                    //var FKAnaBirimID = db.OGRCapYandalTanim.FirstOrDefault(x => x.ID == FKCapTanimID && !x.Silindi).FKAnaBirimID;
                    var FKAnaBirimID = db.OGRCapYandalTanim.FirstOrDefault(x => x.ID == FKCapTanimID && !x.Silindi).FKAltBirimID;
                    var dersplanlar = CAPDERSMAIN.CapSecmeliPlanGetir(FKCapTanimID, FKAnaBirimID, Yil).OrderBy(x => x.YariYil).ToList();
                    return dersplanlar;
                }
                else
                {
                    return null;
                }
            }
        }
        public static List<BolumDersPlanViewModel> EBSDersPlanGetir(int FKCapTanimID, int Yil)
        {
            var sonuc = CAPDERSMAIN.BolumDersPlan(1, 1098, 1100, 2016, 2);
            return sonuc;
        }
        public static List<IntibakPlanDto> IntibakDerslerGetir(int FKCapTanimID, int Yil)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                if (db.OGRCapYandalTanim.Any(x => x.ID == FKCapTanimID && !x.Silindi))
                {
                    return CAPDERSMAIN.IntibakPlanGetir(FKCapTanimID, Yil);
                }
                else
                {
                    return null;
                }
            }
        }
        public static List<BolumEtiketDto> IntibakSecmeliDerslerGetir(int FKCapTanimID, int Yil)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                if (db.OGRCapYandalTanim.Any(x => x.ID == FKCapTanimID && !x.Silindi))
                {
                    return CAPDERSMAIN.IntibakSecmeliPlanGetir(FKCapTanimID, Yil);
                }
                else
                {
                    return null;
                }
            }
        }
        public static IntibakOlusturDto _IntibakOlustur(int FKCapTanimID, int FKAnaDersPlanID, int Yil)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                if (db.OGRCapYandalTanim.Any(X => X.ID == FKCapTanimID && !X.Silindi))
                {
                    var model = new IntibakOlusturDto();
                    model.FKDersPlanID = FKAnaDersPlanID;
                    model.FKDersPlanAd = db.OGRDersPlan.FirstOrDefault(x => x.ID == FKAnaDersPlanID).OGRDersPlanAna.DersAd;
                    //model.IntibakYapilacakListe = CAPDERSMAIN.PlanGetirIntibak(FKCapTanimID, Yil, db.OGRCapYandalTanim.FirstOrDefault(X => X.ID == FKCapTanimID && !X.Silindi).FKAltBirimID);
                    model.IntibakYapilacakListe = CAPDERSMAIN.PlanGetirIntibak(FKCapTanimID, Yil, db.OGRCapYandalTanim.FirstOrDefault(X => X.ID == FKCapTanimID && !X.Silindi).FKAnaBirimID);
                    //model.IntibakYapilacakListe = CAPDERSMAIN.BolumPlanGetir(2016, db.OGRCapYandalTanim.FirstOrDefault(X => X.ID == FKCapTanimID && !X.Silindi).FKAltBirimID);
                    model.FKCapTanimID = FKCapTanimID;

                    return model;
                }
                else
                {
                    return null;
                }
            }
        }
        public static SecmeliIntibakOlusturDto _IntibakOlusturSecmeli(int FKCapTanimID, int FKAnaEtiketID, int Yil)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                if (db.OGRCapYandalTanim.Any(X => X.ID == FKCapTanimID && !X.Silindi))
                {
                    var captanim = db.OGRCapYandalTanim.FirstOrDefault(X => X.ID == FKCapTanimID && !X.Silindi);

                    var model = new SecmeliIntibakOlusturDto();
                    model.FKCapTanimID = FKCapTanimID;
                    model.FKAnaEtiketID = FKAnaEtiketID;
                    model.EtiketAd = db.OGRTNMDersEtiket.FirstOrDefault(x => x.ID == FKAnaEtiketID).Etiket;
                    //model.SecmeliListe = CAPDERSMAIN.SecmeliIntibakYapilacakListe(FKCapTanimID, Yil, captanim.FKAltBirimID);
                    model.SecmeliListe = CAPDERSMAIN.SecmeliIntibakYapilacakListe(FKCapTanimID, Yil, captanim.FKAnaBirimID);
                    return model;
                }
                else
                {
                    return null;
                }
            }
        }
        public static bool _IntibakKaydet(int FKCapTanimID, int FKDersPlanID, int FKCapDersPlanID, string Aciklama, string kullanici, int yil)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                if (db.OGRCapYandalTanim.Any(x => x.ID == FKCapTanimID && !x.Silindi))
                {
                    var FKAnaDersPlanAnaID = db.OGRDersPlan.FirstOrDefault(x => x.ID == FKDersPlanID).FKDersPlanAnaID;
                    var FKCapDersPlanAnaID = db.OGRDersPlan.FirstOrDefault(x => x.ID == FKCapDersPlanID).FKDersPlanAnaID;

                    if (!db.OGRCapDersPlan.Any(x => x.FKCapTanimID == FKCapTanimID && x.FKAnaDersPlanAnaID == FKAnaDersPlanAnaID && x.FKCapDersPlanAnaID == FKCapDersPlanAnaID && x.FKAnaDersPlanID == FKDersPlanID && x.FKCapDersPlanID == FKCapDersPlanID && !x.Silindi))
                    {
                        var model = new OGRCapDersPlan();
                        model.FKCapTanimID = FKCapTanimID;
                        model.TarihKayit = DateTime.Now;
                        model.Kullanici = kullanici;
                        model.Yil = yil;
                        //model.FKAnaDersPlanAnaID = FKAnaDersPlanAnaID.Value;
                        //model.FKCapDersPlanAnaID = FKCapDersPlanAnaID.Value;
                        //model.FKAnaDersPlanID = FKDersPlanID;
                        //model.FKCapDersPlanID = FKCapDersPlanID;
                        model.FKAnaDersPlanAnaID = FKCapDersPlanAnaID.Value;
                        model.FKCapDersPlanAnaID = FKAnaDersPlanAnaID.Value;
                        model.FKAnaDersPlanID = FKCapDersPlanID;
                        model.FKCapDersPlanID = FKDersPlanID;
                        model.Aciklama = Aciklama;
                        model.Silindi = false;
                        db.OGRCapDersPlan.Add(model);
                        db.SaveChanges();
                        return true;
                    }
                }
                return false;
            }
        }
        public static bool _IntibakSecmeliKaydet(int FKCapTanimID, int FKAnaEtiketID, int FKCapEtiketID, string Kullanici, int Adet, int FKSaatBilgiID, int Yil)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var anaetiketID = db.OGRTNMSaatBilgi.FirstOrDefault(x => x.ID == FKCapEtiketID).FKEtiketID.Value;

                var model = new OGRCapSecmeliTanim();
                model.FKCapTanimID = FKCapTanimID;
                model.FKAnaEtiketID = anaetiketID;
                model.FKCapEtiketID = FKAnaEtiketID;
                model.Kullanici = Kullanici;
                model.TarihKayit = DateTime.Now;
                model.Silindi = false;
                model.Adet = Adet;
                model.Yil = Yil;
                model.FKSaatBilgiID = FKCapEtiketID;
                model.FKCapSaatBilgiID = FKSaatBilgiID;
                db.OGRCapSecmeliTanim.Add(model);
                db.SaveChanges();
                return true;
                
            }
        }
        public static bool IntibakPlanSil(int ID, string kullanici)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                if (db.OGRCapDersPlan.Any(x => x.ID == ID))
                {
                    var model = db.OGRCapDersPlan.FirstOrDefault(x => x.ID == ID);
                    model.Silindi = true;
                    model.TarihSilinme = DateTime.Now;
                    model.Kullanici = kullanici;
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool IntibakSecmeliPlanSil(int ID, string kullanici)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                if (db.OGRCapSecmeliTanim.Any(x => x.ID == ID))
                {
                    var model = db.OGRCapSecmeliTanim.FirstOrDefault(x => x.ID == ID);
                    model.Silindi = true;
                    model.Kullanici = kullanici;
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static CapTanimDuzenleDto CapTanimDuzenle(int FKCapTanimID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var model = db.OGRCapYandalTanim.FirstOrDefault(x => x.ID == FKCapTanimID && !x.Silindi);

                var dto = new CapTanimDuzenleDto();
                dto.FKCapTanimID = FKCapTanimID;
                dto.AnaBolumID = model.FKAnaBirimID;
                dto.AltBolumID = model.FKAltBirimID;
                dto.AnaBolumAd = db.Birimler.FirstOrDefault(x => x.ID == model.FKAnaBirimID).BirimAdi;
                dto.AltBolumAd = db.Birimler.FirstOrDefault(x => x.ID == model.FKAltBirimID).BirimAdi;
                dto.Kontenjan = model.CapYandalKontenjan.FirstOrDefault(x => x.FKCapYandalTanimID == FKCapTanimID).Adet;
                return dto;
            }
        }
        public static bool CapDersDuzenle(int FKCapTanimID, int FKAnaBirimID, int FKAltBirimID, int Kontenjan)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                if (db.OGRCapYandalTanim.Any(x => x.ID == FKCapTanimID && x.FKAnaBirimID == FKAnaBirimID && x.FKAltBirimID == FKAltBirimID && !x.Silindi))
                {
                    var model = db.CapYandalKontenjan.FirstOrDefault(x => x.FKCapYandalTanimID == FKCapTanimID && !x.Silindi);
                    model.Adet = Kontenjan;
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool KontenjanGuncelle(int FKCapTanimID, int Kontenjan)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                if (db.OGRCapYandalTanim.Any(x => x.ID == FKCapTanimID && !x.Silindi))
                {
                    var model = db.CapYandalKontenjan.FirstOrDefault(x => x.FKCapYandalTanimID == FKCapTanimID && !x.Silindi);
                    model.Adet = Kontenjan;
                    db.SaveChanges();
                    return true;
                }
                return false;
            }
        }

        public static bool DuzenlemeIzinKontrol()
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var model = db.OGRCapIslemIzin.FirstOrDefault(x => x.Id == 1);
                return model.Izin.Value;
            }
        }
        public static bool DuzenlemeIzin(bool Deger)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var model = db.OGRCapIslemIzin.FirstOrDefault(x => x.Id == 1);
                model.Izin = Deger;
                db.SaveChanges();
                return true;
            }
        }
        #endregion


    }
}
