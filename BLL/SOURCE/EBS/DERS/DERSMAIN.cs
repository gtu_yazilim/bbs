﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Bolum.Core.EBS.DERS;

namespace Sabis.Bolum.Bll.SOURCE.EBS.DERS
{
    public class DERSMAIN
    {
        
        public static List<DersListesiVM> GetirKoordinatorDersleri(int FKPersonelID, int yil, int enumDOnem, string kullaniciAdi)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var dersIDList = db.DersKoordinator.Where(x => x.FKPersonelID == FKPersonelID && x.OGRDersPlanAna.EnumKokDersTipi != 4 && x.OGRDersPlanAna.EnumKokDersTipi != 5 && x.OGRDersPlanAna.EnumDersAktif == 1 && x.OGRDersPlanAna.OGRDersPlan.Any(y => y.Yil == yil) && !x.Silindi).Select(x => x.FKDersPlanAnaID).Distinct().ToList();

                var dersList = db.OGRDersPlanAna.Where(x => dersIDList.Contains(x.ID)).Select(x => new DersListesiVM
                {
                    ID = x.ID,
                    DersAd = x.DersAd,
                    DSaat = x.OGRDersPlanAna2.DSaat,
                    GrupSayisi = db.OGRDersGrup.Count(y => y.FKDersPlanAnaID == x.FKDersPlanAnaID && y.OgretimYili == 2017 && y.EnumDonem == 1 && y.Acik == true && y.Silindi == false)
                    //	ToplamOgrenciSayisi = OGROgrenciYazilma.Count(y => y.FKDersPlanAnaID == x.FKDersPlanAnaID && y.Yil == 2017 && y.EnumDonem == 1 && y.Iptal == false),
                }).ToList();

                var test = dersList;


                //var kDersIDList = db.DersKoordinator.Where(x => x.FKPersonelID == FKPersonelID && !x.Silindi).Select(x => x.FKDersPlanAnaID).Distinct().ToList();

                //var dersList = db.OGRDersPlan.Where(x => x.Yil == yil && kDersIDList.Contains(x.FKDersPlanAnaID.Value) && x.OGRDersPlanAna.EnumKokDersTipi != (int)Sabis.Enum.EnumKokDersTipi.UZMANLIKALANI && x.OGRDersPlanAna.EnumKokDersTipi != (int)Sabis.Enum.EnumKokDersTipi.TEZ_CALISMASI && x.OGRDersPlanAna.EnumDersAktif == (int)Sabis.Enum.EnumDersAktif.AKTIF)
                //    .GroupBy(x => x.FKDersPlanAnaID)
                //    .Select(x => new DersListesiVM
                //    {
                //        ID = x.FirstOrDefault(y => y.FKDersPlanAnaID == x.Key).OGRDersPlanAna.ID,
                //        DersAd = x.FirstOrDefault(y => y.FKDersPlanAnaID == x.Key).OGRDersPlanAna.DersAd,
                //        DSaat = x.FirstOrDefault(y => y.FKDersPlanAnaID == x.Key).OGRDersPlanAna.DSaat,
                //        DersTipi = x.Where(z => z.Yil == yil).FirstOrDefault(y => y.FKDersPlanAnaID == x.Key).EnumDersBirimTipi,
                //        GrupSayisi = db.OGRDersGrup.Count(y => y.FKDersPlanAnaID == x.Key && y.OgretimYili == yil && y.EnumDonem == enumDOnem && y.Acik == true && y.Silindi == false),
                //        ToplamOgrenciSayisi = db.OGROgrenciYazilma.Count(y => y.FKDersPlanAnaID == x.Key && y.Yil == yil && y.EnumDonem == enumDOnem && y.Iptal == false),
                //        BirimListe = x.FirstOrDefault(y => y.FKDersPlanAnaID == x.Key).OGRDersPlanAna.OGRDersGrup.Where(y => y.OgretimYili == yil && y.EnumDonem == enumDOnem && y.Acik == true && y.Silindi == false).Select(y => new DersBirimListeVM
                //        {
                //            BirimID = y.Birimler.ID,
                //            BirimAdi = y.Birimler.BirimAdi + " (" + y.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.UnvanAd + " " + y.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.Kisi.Ad + " " + y.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.Kisi.Soyad + ")",
                //            BirimDersKod = y.OGRDersPlan.BolKodAd + " " + y.OGRDersPlan.DersKod
                //        }).ToList(),
                //        //FKDersGrupHocaID = db.OGRDersGrupHoca.Any(y => y.OGRDersGrup.OgretimYili == yil && y.OGRDersGrup.EnumDonem == enumDOnem && y.OGRDersGrup.FKDersPlanAnaID == x.Key) ? db.OGRDersGrupHoca.Where(y=> y.OGRDersGrup.OgretimYili == yil && y.OGRDersGrup.EnumDonem == enumDOnem && y.OGRDersGrup.FKDersPlanAnaID == x.Key).FirstOrDefault().ID : 0,
                //        //EnumKokDersTipi = x.FirstOrDefault(y=> y.FKDersPlanAnaID == x.Key).OGRDersPlanAna.EnumKokDersTipi
                //    }).ToList();


                return dersList;
            }
        }

        #region DERS AKIŞI CRUD
        public static List<DersAkisiVM> GetirDersAkisi(int? dersPlanAnaID, int yil, int? enumDilID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var sonuc = (from derskonu in db.DersKonu
                             where derskonu.FKDersPlanAnaID == dersPlanAnaID && derskonu.Yil == yil
                             select new DersAkisiVM
                             {
                                 ID = derskonu.ID,
                                 FKDersPlanAnaID = derskonu.FKDersPlanAnaID,
                                 Yil = derskonu.Yil,
                                 DersAkisiIcerik = (from dilderskonu in db.DilDersKonu
                                                    where dilderskonu.FKDersKonuID == derskonu.ID && dilderskonu.EnumDilID == enumDilID
                                                    select new DersAkisiDilVM
                                                    {
                                                        ID = dilderskonu.ID,
                                                        FKDersKonuID = dilderskonu.FKDersKonuID,
                                                        Icerik = dilderskonu.Icerik,
                                                        OnHazirlikIcerik = dilderskonu.OnHazirlikIcerik,
                                                        EnumDilID = dilderskonu.EnumDilID
                                                    }).ToList(),
                                 HaftaNo = db.DersHaftaIliski.FirstOrDefault(x => x.ID == derskonu.FKDersHaftaIliskiID).HaftaNo
                             }).OrderBy(x => x.HaftaNo).ToList();
                return sonuc;
            }
        }
        public static bool EkleDersAkisiYeniKonu(DersAkisiVM yeniKonu, string kullaniciAdi)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    if (db.DersKonu.Any(x => x.FKDersPlanAnaID == yeniKonu.FKDersPlanAnaID && x.Yil == yeniKonu.Yil && x.DersHaftaIliski.HaftaNo == yeniKonu.HaftaNo))
                    {
                        return false;
                    }
                    DersKonu dk = new DersKonu();
                    DilDersKonu ddk = new DilDersKonu();
                    DersHaftaIliski dhi = new DersHaftaIliski();

                    //ders hafta ilişki
                    dhi.FKDersPlanAnaID = yeniKonu.FKDersPlanAnaID;
                    dhi.HaftaNo = yeniKonu.HaftaNo;
                    dhi.Ekleyen = kullaniciAdi;
                    dhi.Zaman = DateTime.Now;

                    db.DersHaftaIliski.Add(dhi);
                    db.SaveChanges();
                    //ders hafta ilişki

                    //ders konu
                    dk.FKDersPlanAnaID = yeniKonu.FKDersPlanAnaID;
                    dk.FKDersHaftaIliskiID = dhi.ID;
                    dk.Yil = yeniKonu.Yil;
                    dk.Ekleyen = kullaniciAdi;
                    dk.Zaman = DateTime.Now;

                    db.DersKonu.Add(dk);
                    db.SaveChanges();
                    //ders konu

                    //ders konu içerik
                    foreach (var item in yeniKonu.DersAkisiIcerik)
                    {
                        if (db.DilDersKonu.Any(x => x.FKDersKonuID == dk.ID && x.EnumDilID == item.EnumDilID))
                        {
                            continue;
                        }
                        ddk.FKDersKonuID = dk.ID;
                        ddk.Icerik = item.Icerik;
                        ddk.OnHazirlikIcerik = item.OnHazirlikIcerik;
                        ddk.EnumDilID = item.EnumDilID;
                        ddk.Ekleyen = kullaniciAdi;
                        ddk.Zaman = DateTime.Now;

                        db.DilDersKonu.Add(ddk);
                        db.SaveChanges();
                    }
                    //ders konu içerik
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool GuncelleDersAkisiKonu(DersAkisiDilVM konuBilgi, string kullaniciAdi)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                try
                {
                    var DK = db.DersKonu.FirstOrDefault(x => x.ID == konuBilgi.FKDersKonuID);
                    var DDK = db.DilDersKonu.FirstOrDefault(x => x.ID == konuBilgi.ID);

                    DDK.Ekleyen = kullaniciAdi;
                    DDK.Zaman = DateTime.Now;
                    DDK.FKDersKonuID = konuBilgi.FKDersKonuID;
                    DDK.EnumDilID = konuBilgi.EnumDilID;
                    DDK.Icerik = konuBilgi.Icerik;
                    DDK.OnHazirlikIcerik = konuBilgi.OnHazirlikIcerik;

                    db.SaveChanges();

                    if (DK.FKDersHaftaIliskiID.HasValue)
                    {
                        var DHI = db.DersHaftaIliski.FirstOrDefault(x => x.ID == DK.FKDersHaftaIliskiID);
                        DHI.HaftaNo = (byte)konuBilgi.HaftaNo;
                    }
                    db.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }
        public static bool SilDersAkisiKonu(int dersKonuID)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var ddk = db.DilDersKonu.Where(x => x.FKDersKonuID == dersKonuID);
                    var dk = db.DersKonu.FirstOrDefault(x => x.ID == dersKonuID);
                    var dhi = db.DersHaftaIliski.FirstOrDefault(x => x.ID == dk.FKDersHaftaIliskiID);

                    db.DersHaftaIliski.Remove(dhi);
                    db.SaveChanges();

                    foreach (var item in ddk)
                    {
                        db.DilDersKonu.Remove(item);
                    }

                    db.SaveChanges();

                    db.DersKonu.Remove(dk);
                    db.SaveChanges();

                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

        //TabloYok
        #region ÖĞRENME ÇIKTILARI CRUD
        //public static List<OgrenmeCiktilariVM> GetirOgrenmeCiktilari(int dersPlanAnaID, int fkBirimID, int Yil, int EnumDilID)
        //{
        //    using (UYSv2Entities db = new UYSv2Entities())
        //    {
        //        List<int> ciktiIDList = db.DilDersCikti.Where(x => x.DersCikti.FKDersPlanAnaID == dersPlanAnaID && x.DersCikti.Yil == Yil && x.EnumDilID == EnumDilID).Select(x => x.FKDersCiktiID.Value).Distinct().ToList();

        //        //var ciktiYontemList = db.DersCiktiYontem.Where(x => x.Yil == Yil).ToList();
        //        //var ciktiYontemIliskiList = db.DersCiktiYontemIliski.Where(x => ciktiIDList.Contains(x.FKCiktiID.Value)).ToList();

        //        List<OgrenmeCiktilariVM> sonuc = db.DilDersCikti.Where(x => x.DersCikti.FKDersPlanAnaID == dersPlanAnaID && x.DersCikti.Yil == Yil && x.EnumDilID == EnumDilID)
        //        .Select(x => new OgrenmeCiktilariVM
        //        {
        //            ID = x.DersCikti.ID,
        //            FKDersPlanAnaID = x.DersCikti.FKDersPlanAnaID,
        //            Ekleyen = x.DersCikti.Ekleyen,
        //            Yil = x.DersCikti.Yil,
        //            CiktiNo = x.DersCikti.CiktiNo,
        //            Zaman = x.DersCikti.Zaman,
        //            Icerik = x.Icerik,
        //            DilDersCiktiID = x.ID,
        //            EnumDilID = (byte)x.EnumDilID,
        //            DersCiktiYontemleri = db.DersCiktiYontemIliski.Where(z => z.FKCiktiID == x.FKDersCiktiID).Select(z => new DersCiktiYontemVM
        //            {
        //                ID = z.ID,
        //                YontemTur = z.DersCiktiYontem.YontemTur,
        //                YontemNo = z.DersCiktiYontem.YontemNo,
        //                Icerik = z.DersCiktiYontem.DilDersCiktiYontem.FirstOrDefault().Icerik,
        //                Yil = z.Yil
        //            }).ToList(),
        //            DersCiktiYeterlilikListe = db.DersCiktiYeterlilikIliski.Where(y => y.FKDersPlanAnaID == x.DersCikti.FKDersPlanAnaID && y.FKCiktiID == x.FKDersCiktiID && y.FKBirimID == fkBirimID && y.Yil == Yil).Select(y => new DersCiktiYeterlilikVM
        //            {
        //                ID = x.ID,
        //                FKDersPlanAnaID = y.FKDersPlanAnaID,
        //                FKCiktiID = y.FKCiktiID,
        //                FKBirimID = y.FKBirimID,
        //                FKYeterlilikID = y.FKYeterlilikID,
        //                YetID = y.BolumYeterlilik.YetID,
        //                Icerik = y.BolumYeterlilik.DilBolumYeterlilik.FirstOrDefault().Icerik,
        //                Ekleyen = x.Ekleyen,
        //                Yil = y.Yil,
        //                Zaman = y.Zaman
        //            }).ToList()
        //        }).ToList();
        //        return sonuc;
        //    }
        //}
        public static bool OgrenmeCiktisiGuncelle(OgrenmeCiktilariVM ogrenmeCiktisi)
        {
            try
            {
                using (UYSv2Entities db = new DATA.UysModel.UYSv2Entities())
                {
                    var birimID = db.OGRDersPlan.Where(x => x.FKDersPlanAnaID == ogrenmeCiktisi.FKDersPlanAnaID && x.Yil == ogrenmeCiktisi.Yil).Select(x => x.FKBolumPrBirimID).FirstOrDefault();

                    DersCikti DC = db.DersCikti.FirstOrDefault(x => x.ID == ogrenmeCiktisi.ID);
                    DilDersCikti DDC = db.DilDersCikti.First(x => x.ID == ogrenmeCiktisi.DilDersCiktiID);

                    //foreach (var subItem in db.DersCiktiYontemIliski.Where(x => x.FKCiktiID == DC.ID))
                    //{
                    //    db.DersCiktiYontemIliski.Remove(subItem);
                    //}
                    //db.SaveChanges();

                    //if (ogrenmeCiktisi.DersCiktiYontemleri != null)
                    //{
                    //    foreach (var subItem2 in ogrenmeCiktisi.DersCiktiYontemleri)
                    //    {
                    //        DersCiktiYontemIliski dcyi = new DersCiktiYontemIliski();
                    //        dcyi.FKCiktiID = DC.ID;
                    //        dcyi.FKCiktiYontemID = subItem2.ID;
                    //        dcyi.Yil = (short)subItem2.Yil;
                    //        dcyi.Ekleyen = ogrenmeCiktisi.Ekleyen;
                    //        dcyi.Zaman = DateTime.Now;
                    //        db.DersCiktiYontemIliski.Add(dcyi);
                    //    }
                    //    db.SaveChanges();
                    //}

                    DC.FKDersPlanAnaID = ogrenmeCiktisi.FKDersPlanAnaID;
                    DC.CiktiNo = ogrenmeCiktisi.CiktiNo;
                    DC.Yil = (short)ogrenmeCiktisi.Yil.Value;
                    DC.Ekleyen = ogrenmeCiktisi.Ekleyen;
                    DC.Zaman = DateTime.Now;


                    DDC.FKDersCiktiID = DC.ID;
                    DDC.Icerik = ogrenmeCiktisi.Icerik;
                    DDC.EnumDilID = (byte)ogrenmeCiktisi.EnumDilID;
                    DDC.Ekleyen = ogrenmeCiktisi.Ekleyen;
                    DDC.Zaman = DateTime.Now;
                    db.SaveChanges();

                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool EkleYeniOgrenmeCiktisi(OgrenmeCiktilariVM yeniOgrenmeCiktisi)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var sira = db.DersCikti.Count(x => x.FKDersPlanAnaID == yeniOgrenmeCiktisi.FKDersPlanAnaID && x.Yil == yeniOgrenmeCiktisi.Yil) + 1;
                    var birimID = db.OGRDersPlan.Where(x => x.FKDersPlanAnaID == yeniOgrenmeCiktisi.FKDersPlanAnaID && x.Yil == yeniOgrenmeCiktisi.Yil).Select(x => x.FKBolumPrBirimID).FirstOrDefault();
                    DersCikti DC = new DersCikti();
                    DilDersCikti DDC = new DilDersCikti();

                    //DC.FKProgramBirimID = birimID;
                    DC.FKDersPlanAnaID = yeniOgrenmeCiktisi.FKDersPlanAnaID;
                    DC.CiktiNo = sira;
                    DC.Yil = (short)yeniOgrenmeCiktisi.Yil.Value;
                    DC.Ekleyen = yeniOgrenmeCiktisi.Ekleyen;
                    DC.Zaman = DateTime.Now;
                    db.DersCikti.Add(DC);
                    db.SaveChanges();


                    DDC.FKDersCiktiID = DC.ID;
                    DDC.Icerik = yeniOgrenmeCiktisi.Icerik;
                    DDC.EnumDilID = 1; //tr
                    DDC.Ekleyen = yeniOgrenmeCiktisi.Ekleyen;
                    DDC.Zaman = DateTime.Now;
                    db.DilDersCikti.Add(DDC);
                    db.SaveChanges();

                    DDC.FKDersCiktiID = DC.ID;
                    DDC.Icerik = yeniOgrenmeCiktisi.Icerik;
                    DDC.EnumDilID = 2; //en
                    DDC.Ekleyen = yeniOgrenmeCiktisi.Ekleyen;
                    DDC.Zaman = DateTime.Now;
                    db.DilDersCikti.Add(DDC);
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        //TabloYok
        //public static bool SilDersOgrenmeCiktisi(int dersCiktiID)
        //{
        //    try
        //    {
        //        using (UYSv2Entities db = new UYSv2Entities())
        //        {
        //            var DersCikti = db.DersCikti.FirstOrDefault(x => x.ID == dersCiktiID);
        //            var DilDersCikti = db.DilDersCikti.Where(x => x.FKDersCiktiID == dersCiktiID);
        //            var YontemIliski = db.DersCiktiYontemIliski.Where(x => x.FKCiktiID == dersCiktiID);
        //            var YeterlilikIliski = db.DersCiktiYeterlilikIliski.Where(x => x.FKCiktiID == dersCiktiID);

        //            foreach (var item in DilDersCikti)
        //            {
        //                db.DilDersCikti.Remove(item);
        //            }
        //            foreach (var item in YontemIliski)
        //            {
        //                db.DersCiktiYontemIliski.Remove(item);
        //            }
        //            foreach (var item in YeterlilikIliski)
        //            {
        //                db.DersCiktiYeterlilikIliski.Remove(item);
        //            }
        //            db.DersCikti.Remove(DersCikti);
        //            db.SaveChanges();
        //            return true;
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }
        //}
        public static List<DersCiktiYontemVM> GetirDersCiktiYontemleri(int yil)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return db.DersCiktiYontem.Where(x => x.Yil == yil).Select(x => new DersCiktiYontemVM
                {
                    ID = x.ID,
                    YontemTur = x.YontemTur,
                    YontemNo = x.YontemNo,
                    Yil = x.Yil,
                    Icerik = x.DilDersCiktiYontem.FirstOrDefault(y => y.FKCiktiYontemID == x.ID).Icerik
                }).ToList();
            }
        }
        public static bool KaydetDersCiktiYontemIliski(int fkCiktiID, List<int> fkCiktiYontemIDList, int yontemTUr, int yil, string kullaniciAdi)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    List<DersCiktiYontemIliski> iliskiList = db.DersCiktiYontemIliski.Where(x => x.FKCiktiID == fkCiktiID && x.DersCiktiYontem.YontemTur == yontemTUr).ToList();

                    foreach (var item in iliskiList)
                    {
                        db.DersCiktiYontemIliski.Remove(item);
                    }
                    db.SaveChanges();

                    if (fkCiktiYontemIDList != null)
                    {
                        foreach (var item in fkCiktiYontemIDList)
                        {
                            DersCiktiYontemIliski yeniIliski = new DersCiktiYontemIliski();
                            yeniIliski.FKCiktiID = fkCiktiID;
                            yeniIliski.FKCiktiYontemID = item;
                            yeniIliski.Yil = (short)yil;
                            yeniIliski.Ekleyen = kullaniciAdi;
                            yeniIliski.Zaman = DateTime.Now;
                            db.DersCiktiYontemIliski.Add(yeniIliski);
                        }
                        db.SaveChanges();
                    }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

        #region DEĞERLENDİRME CRUD
        public static List<DegerlendirmePayListesiVM> GetirDegerlendirmePayListesi(int dersPlanAnaID, int yil, int EnumDonem, int? FKDersPlanID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var payListesi = (from paylar in db.ABSPaylar
                                  where paylar.FKDersPlanAnaID == dersPlanAnaID &&
                                        paylar.Silindi == false &&
                                        paylar.Yil == yil &&
                                        paylar.EnumDonem == EnumDonem
                                  select new DegerlendirmePayListesiVM
                                  {
                                      PayID = paylar.ID,
                                      EnumCalismaTip = paylar.EnumCalismaTip,
                                      Sira = paylar.Sira,
                                      Oran = paylar.Oran,
                                      NotGirilmisMi = paylar.ABSOgrenciNot.Any(x => x.Silindi == false && x.Yil == paylar.Yil && x.EnumDonem == paylar.EnumDonem),
                                      SinavOdevVarMi = db.ABSPaylarOlcme.Any(x=> x.FKPaylarID == paylar.ID && !x.Silindi.Value),
                                      EnumSeviye = db.OGRDersGrup.FirstOrDefault(x => x.FKDersPlanAnaID == paylar.FKDersPlanAnaID && x.OgretimYili == yil && x.EnumDonem == EnumDonem).Birimler.EnumSeviye,
                                      FKDersPlanID = paylar.FKDersPlanID
                                  }).OrderBy(x => x.EnumCalismaTip).ToList();

                if (FKDersPlanID.HasValue)
                {
                    payListesi = payListesi.Where(x => x.FKDersPlanID == FKDersPlanID).ToList();
                }
                else
                {
                    payListesi = payListesi.Where(x => x.FKDersPlanID == null).ToList();
                }

                return payListesi;
            }
        }


        public static List<DegerlendirmeGruplariVM> GetirDegerlendirmeGrupListesi(int dersPlanAnaID, int yil, int donem)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var grupListesi = db.ABSOrtakDegerlendirme.Where(x => x.FKDersPlanAnaID == dersPlanAnaID && x.Yil == yil && x.Donem == donem && x.Silindi == false)
                                                          .Select(x => new DegerlendirmeGruplariVM
                                                          {
                                                              ID = x.ID,
                                                              FKPersonelID = x.FKPersonelID,
                                                              FKDersPlanAnaID = x.FKDersPlanAnaID,
                                                              PersonelAdSoyadUnvan = x.PersonelBilgisi.UnvanAd + " " + x.PersonelBilgisi.Ad + " " + x.PersonelBilgisi.Soyad,
                                                              TarihKayit = x.TarihKayit,
                                                              Kullanici = x.Kullanici,
                                                              FKDersGrupHocaID = db.OGRDersGrupHoca.Where(y=> x.ABSOrtakDegerlendirmeGrup.Where(c=> c.FKOrtakDegerlendirmeID == x.ID).Select(c=> c.FKDersGrupID).ToList().Contains(y.FKDersGrupID.Value) && y.FKPersonelID.HasValue).FirstOrDefault().ID,
                                                              //FKDersGrupHocaID = db.OGRDersGrupHoca.FirstOrDefault(y=> y.FKPersonelID.HasValue && y.OGRDersGrup.ID == x.ABSOrtakDegerlendirmeGrup.FirstOrDefault().FKDersGrupID && y.OGRDersGrup.OgretimYili == yil && y.OGRDersGrup.EnumDonem == donem && y.OGRDersGrup.Acik == true).ID,
                                                              DersGrupListesi = x.ABSOrtakDegerlendirmeGrup.Where(y => y.ABSOrtakDegerlendirme.FKDersPlanAnaID == x.FKDersPlanAnaID && y.Silindi == false).Select(y => new DegerlendirmeDersGruplariVM
                                                              {
                                                                  ID = y.ID,
                                                                  FKOrtakDegerlendirmeID = y.FKOrtakDegerlendirmeID,
                                                                  FKDersGrupID = y.FKDersGrupID,
                                                                  BirimAdi = y.OGRDersGrup.Birimler.BirimAdi + " (" + y.OGRDersGrup.OGRDersGrupHoca.Select(c => c.PersonelBilgisi.UnvanAd + " " + c.PersonelBilgisi.Ad + " " + c.PersonelBilgisi.Soyad + ")").FirstOrDefault(),
                                                                  //BirimAdi = y.OGRDersGrup.Birimler.BirimAdi + " (" + String.Join(",", y.OGRDersGrup.OGRDersGrupHoca.Select(c=> c.PersonelBilgisi.UnvanAd + " " + c.PersonelBilgisi.Ad + " " + c.PersonelBilgisi.Soyad + ")").ToList()),
                                                                  TarihKayit = y.TarihKayit,
                                                                  Kullanici = y.Kullanici,
                                                                  EnumDil = y.OGRDersGrup.EnumDersGrupDil.Value
                                                              }).ToList(),
                                                              DegisiklikYapilabilirMi = !db.ABSDegerlendirme.Any(y=> y.FKDersPlanAnaID == x.FKDersPlanAnaID && y.Yil == yil && y.EnumDonem == donem && y.SilindiMi == false)
                                                          }).ToList();
                return grupListesi;
            }
        }

        public static bool EkleYeniDegerlendirmeGrup(int dersPlanAnaID, int FKPersonelID, string kullaniciAdi, int yil, int donem)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                try
                {
                    ABSOrtakDegerlendirme yeniGrup = new ABSOrtakDegerlendirme();
                    yeniGrup.FKDersPlanAnaID = dersPlanAnaID;
                    yeniGrup.TarihKayit = DateTime.Now;
                    yeniGrup.Silindi = false;
                    yeniGrup.Kullanici = kullaniciAdi;
                    yeniGrup.FKPersonelID = FKPersonelID;
                    yeniGrup.Yil = yil;
                    yeniGrup.Donem = donem;
                    db.ABSOrtakDegerlendirme.Add(yeniGrup);
                    db.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public static bool SilDegerlendirmeGrup(int DegerlendirmeGrupID, int yil, int donem)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    ABSOrtakDegerlendirme grup = db.ABSOrtakDegerlendirme.FirstOrDefault(x => x.ID == DegerlendirmeGrupID);
                    ABSOrtakDegerlendirme yeniGrup = db.ABSOrtakDegerlendirme.Where(x => x.ID != DegerlendirmeGrupID && x.FKDersPlanAnaID == grup.FKDersPlanAnaID && x.Yil == grup.Yil && x.Donem == grup.Donem && x.Silindi == false).FirstOrDefault();

                    List<ABSOrtakDegerlendirmeGrup> grupListe = db.ABSOrtakDegerlendirmeGrup.Where(x => x.FKOrtakDegerlendirmeID == DegerlendirmeGrupID).ToList();

                    foreach (var item in grupListe)
                    {
                        item.FKOrtakDegerlendirmeID = yeniGrup.ID;
                    }
                    db.SaveChanges();

                    db.ABSOrtakDegerlendirme.Remove(grup);
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool DegistirDersDegerlendirmeGrup(int OrtakGrupID, int FKOrtakDegerlendirmeID)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    ABSOrtakDegerlendirmeGrup dersGrup = db.ABSOrtakDegerlendirmeGrup.FirstOrDefault(x => x.ID == OrtakGrupID);
                    dersGrup.FKOrtakDegerlendirmeID = FKOrtakDegerlendirmeID;
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool DegistirDegerlendirmeGrupHoca(int OrtakDegerlendirmeID, int FKPersonelID)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    ABSOrtakDegerlendirme degerlendirmeGrup = db.ABSOrtakDegerlendirme.FirstOrDefault(x => x.ID == OrtakDegerlendirmeID);
                    degerlendirmeGrup.FKPersonelID = FKPersonelID;
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static List<DersGrupHocaVM> GetirDersHocaListesi(int dersPlanAnaID, int yil, int donem)
        {
            List<DersGrupHocaVM> result = new List<DersGrupHocaVM>();
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var dersGrupList = db.OGRDersGrup.Where(x => x.FKDersPlanAnaID == dersPlanAnaID && x.OgretimYili == yil && x.EnumDonem == donem);
                foreach (var item in dersGrupList)
                {
                    foreach (var hoca in item.OGRDersGrupHoca.Where(x=>x.FKPersonelID.HasValue))
                    {
                        if (!result.Any(x => x.FKPersonelID == hoca.FKPersonelID))
                        {
                            DersGrupHocaVM dgHoca = new DersGrupHocaVM();
                            dgHoca.FKPersonelID = hoca.FKPersonelID.Value;
                            dgHoca.PersonelAdSoyadUnvan = hoca.PersonelBilgisi.UnvanAd + " " + hoca.PersonelBilgisi.Kisi.Ad + " " + hoca.PersonelBilgisi.Kisi.Soyad;
                            result.Add(dgHoca);
                        }
                    }
                }
            }
            return result;
        }

        public static bool HocalariGruplaraAyir(int dersPlanAnaID, int yil, int donem, string kullaniciAdi)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var sonHalVerilenGruplar = db.ABSDegerlendirme.Where(x => x.FKDersPlanAnaID == dersPlanAnaID && x.Yil == yil && x.EnumDonem == donem && x.SilindiMi == false).Select(x => x.FKDersGrupID).ToList();
                    //önceki kayıtları sil
                    var dGrupList = db.ABSOrtakDegerlendirmeGrup.Where(x => !sonHalVerilenGruplar.Contains(x.FKDersGrupID) && x.ABSOrtakDegerlendirme.FKDersPlanAnaID == dersPlanAnaID);

                    var grupList = db.ABSOrtakDegerlendirmeGrup.Where(x => !sonHalVerilenGruplar.Contains(x.FKDersGrupID) && x.ABSOrtakDegerlendirme.FKDersPlanAnaID == dersPlanAnaID).Select(x => x.ABSOrtakDegerlendirme).ToList();

                    //var grupList = db.ABSOrtakDegerlendirme.Where(x => x.FKDersPlanAnaID == dersPlanAnaID);
                    //var dGrupList = db.ABSOrtakDegerlendirmeGrup.Where(x => grupList.Select(y => y.ID).Contains(x.FKOrtakDegerlendirmeID));

                    foreach (var item in dGrupList)
                    {
                        db.ABSOrtakDegerlendirmeGrup.Remove(item);
                    }
                    db.SaveChanges();

                    foreach (var item in grupList)
                    {
                        db.ABSOrtakDegerlendirme.Remove(item);
                    }
                    db.SaveChanges();
                    //önceki kayıtları sil


                    //hocalara göre yeni grupları oluştur
                    var dersGrupList = db.OGRDersGrup.Where(x => x.FKDersPlanAnaID == dersPlanAnaID && x.OgretimYili == yil && x.EnumDonem == donem && x.Acik == true && !sonHalVerilenGruplar.Contains(x.ID));
                    var dersGrupIDList = dersGrupList.Select(x => x.ID).ToList();
                    var dersGrupHocaList = db.OGRDersGrupHoca.Where(x => dersGrupIDList.Contains(x.FKDersGrupID.Value) && x.FKPersonelID.HasValue).ToList();
                    var dersGrupHocaIDList = dersGrupHocaList.Select(x => x.FKPersonelID).Distinct();

                    foreach (var item in dersGrupHocaIDList)
                    {
                        ABSOrtakDegerlendirme yeniDegerlendirmeGrup = new ABSOrtakDegerlendirme();
                        yeniDegerlendirmeGrup.TarihKayit = DateTime.Now;
                        yeniDegerlendirmeGrup.FKDersPlanAnaID = dersPlanAnaID;
                        yeniDegerlendirmeGrup.Kullanici = kullaniciAdi;
                        yeniDegerlendirmeGrup.FKPersonelID = item.Value;
                        yeniDegerlendirmeGrup.Silindi = false;
                        yeniDegerlendirmeGrup.Yil = yil;
                        yeniDegerlendirmeGrup.Donem = donem;
                        db.ABSOrtakDegerlendirme.Add(yeniDegerlendirmeGrup);
                        db.SaveChanges();

                        var hocaGrupIDList = dersGrupHocaList.Where(x => x.FKPersonelID == item.Value).Select(x => x.FKDersGrupID);
                        var gruplar = dersGrupList.Where(x => hocaGrupIDList.Contains(x.ID));
                        foreach (var subItem in gruplar)
                        {
                            ABSOrtakDegerlendirmeGrup yeniAltGrup = new ABSOrtakDegerlendirmeGrup();
                            yeniAltGrup.TarihKayit = DateTime.Now;
                            yeniAltGrup.FKDersGrupID = subItem.ID;
                            yeniAltGrup.FKOrtakDegerlendirmeID = yeniDegerlendirmeGrup.ID;
                            yeniAltGrup.Kullanici = kullaniciAdi;
                            yeniAltGrup.Silindi = false;
                            db.ABSOrtakDegerlendirmeGrup.Add(yeniAltGrup);
                        }
                        db.SaveChanges();
                    }
                    //hocalara göre yeni grupları oluştur
                }
                return true;
            }
            catch (Exception error)
            {
                return false;
            }
        }

        public static bool GruplariBirlestir(int dersPlanAnaID, int yil, int donem, string kullaniciAdi, int fkKullaniciID)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var sonHalVerilenGruplar = db.ABSDegerlendirme.Where(x => x.FKDersPlanAnaID == dersPlanAnaID && x.Yil == yil && x.EnumDonem == donem && x.SilindiMi == false).Select(x=> x.FKDersGrupID).ToList();

                    //önceki kayıtları sil
                    var dGrupList = db.ABSOrtakDegerlendirmeGrup.Where(x=> !sonHalVerilenGruplar.Contains(x.FKDersGrupID) && x.ABSOrtakDegerlendirme.FKDersPlanAnaID == dersPlanAnaID);

                    var grupList = db.ABSOrtakDegerlendirmeGrup.Where(x => !sonHalVerilenGruplar.Contains(x.FKDersGrupID) && x.ABSOrtakDegerlendirme.FKDersPlanAnaID == dersPlanAnaID).Select(x=> x.ABSOrtakDegerlendirme).ToList();

                    //var grupList = db.ABSOrtakDegerlendirme.Where(x => x.FKDersPlanAnaID == dersPlanAnaID);
                    //var dGrupList = db.ABSOrtakDegerlendirmeGrup.Where(x => grupList.Select(y => y.ID).Contains(x.FKOrtakDegerlendirmeID) && x.fk);

                    foreach (var item in dGrupList)
                    {
                        db.ABSOrtakDegerlendirmeGrup.Remove(item);
                    }
                    db.SaveChanges();

                    foreach (var item in grupList)
                    {
                        db.ABSOrtakDegerlendirme.Remove(item);
                    }
                    db.SaveChanges();
                    //önceki kayıtları sil

                    //değerlendirme yapılmayan grupları birleştir
                    var dersGrupList = db.OGRDersGrup.Where(x => x.FKDersPlanAnaID == dersPlanAnaID && x.OgretimYili == yil && x.EnumDonem == donem && x.Acik == true && !sonHalVerilenGruplar.Contains(x.ID));
                    var dersGrupIDList = dersGrupList.Select(x => x.ID).ToList();
                    var dersGrupHocaList = db.OGRDersGrupHoca.Where(x => dersGrupIDList.Contains(x.FKDersGrupID.Value) && x.FKPersonelID.HasValue).ToList();
                    var dersGrupHocaIDList = dersGrupHocaList.Select(x => x.FKPersonelID).Distinct();

                    ABSOrtakDegerlendirme yeniDegerlendirmeGrup = new ABSOrtakDegerlendirme();
                    yeniDegerlendirmeGrup.TarihKayit = DateTime.Now;
                    yeniDegerlendirmeGrup.FKDersPlanAnaID = dersPlanAnaID;
                    yeniDegerlendirmeGrup.Kullanici = kullaniciAdi;
                    yeniDegerlendirmeGrup.FKPersonelID = fkKullaniciID;
                    yeniDegerlendirmeGrup.Yil = yil;
                    yeniDegerlendirmeGrup.Donem = donem;
                    yeniDegerlendirmeGrup.Silindi = false;
                    db.ABSOrtakDegerlendirme.Add(yeniDegerlendirmeGrup);
                    db.SaveChanges();

                    foreach (var item in dersGrupList)
                    {
                        ABSOrtakDegerlendirmeGrup yeniAltGrup = new ABSOrtakDegerlendirmeGrup();
                        yeniAltGrup.TarihKayit = DateTime.Now;
                        yeniAltGrup.FKDersGrupID = item.ID;
                        yeniAltGrup.FKOrtakDegerlendirmeID = yeniDegerlendirmeGrup.ID;
                        yeniAltGrup.Kullanici = kullaniciAdi;
                        yeniAltGrup.Silindi = false;
                        db.ABSOrtakDegerlendirmeGrup.Add(yeniAltGrup);
                    }
                    db.SaveChanges();
                    //değerlendirme yapılmayan grupları birleştir


                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion

        #region AKTS İŞ YÜKÜ
        public static List<DersAktsVM> GetirDersAkts(byte dil, int dersID, int Yil)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return (from aktsyuk in db.DersAktsYukIliski.Where(x => x.FKDersPlanAnaID == dersID && x.Yil == Yil)
                        select new DersAktsVM
                        {
                            AktsID = aktsyuk.FKAktsID,
                            DersPlanAnaID = (int)aktsyuk.FKDersPlanAnaID,
                            AktsAd = aktsyuk.DersAktsTnm.DilDersAktsTnm.FirstOrDefault().Icerik,
                            Sayi = aktsyuk.Sayi,
                            Sure = aktsyuk.Sure,
                            AktsYukID = (int)aktsyuk.ID
                        }).ToList();
            }
        }
        public static bool EkleYeniDersAkts(DersAktsVM yeniDersAkts, string kullaniciAdi)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    DersAktsTnm yeniAkts = new DersAktsTnm();
                    yeniAkts.Yil = yeniDersAkts.Yil.Value;
                    db.DersAktsTnm.Add(yeniAkts);
                    db.SaveChanges();

                    DersAktsYukIliski yeniAktsYukIliski = new DersAktsYukIliski();
                    yeniAktsYukIliski.FKDersPlanAnaID = yeniDersAkts.DersPlanAnaID;
                    yeniAktsYukIliski.FKAktsID = yeniAkts.ID;
                    yeniAktsYukIliski.Sayi = yeniDersAkts.Sayi;
                    yeniAktsYukIliski.Sure = yeniDersAkts.Sure;
                    yeniAktsYukIliski.Yil = yeniDersAkts.Yil.Value;
                    yeniAktsYukIliski.Ekleyen = kullaniciAdi;
                    yeniAktsYukIliski.Zaman = DateTime.Now;
                    db.DersAktsYukIliski.Add(yeniAktsYukIliski);
                    db.SaveChanges();

                    DilDersAktsTnm yeniDilAkts = new DilDersAktsTnm();
                    yeniDilAkts.FKDersAktsTnmID = yeniAkts.ID;
                    yeniDilAkts.Icerik = yeniDersAkts.AktsAd;
                    yeniDilAkts.EnumDilID = yeniDersAkts.EnumDilID;
                    yeniDilAkts.Ekleyen = kullaniciAdi;
                    yeniDilAkts.Zaman = DateTime.Now;
                    db.DilDersAktsTnm.Add(yeniDilAkts);
                    db.SaveChanges();

                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool GuncelleDersAkts(List<DersAktsVM> dersAktsList, string kullaniciAdi)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    foreach (var item in dersAktsList)
                    {
                        if (item.KayitliMi)
                        {
                            var yuk = db.DersAktsYukIliski.FirstOrDefault(x => x.ID == item.AktsYukID);

                            yuk.Sure = item.Sure;
                            yuk.Sayi = item.Sayi;

                            db.SaveChanges();
                        }
                        else if (item.Sayi.HasValue && item.Sure.HasValue && item.Sayi != 0 && item.Sure != 0)
                        {
                            DersAktsYukIliski yeniAKTS = new DersAktsYukIliski();
                            yeniAKTS.FKDersPlanAnaID = item.DersPlanAnaID;
                            yeniAKTS.FKAktsID = item.AktsID;
                            yeniAKTS.Sayi = item.Sayi;
                            yeniAKTS.Sure = item.Sure;
                            yeniAKTS.Yil = (short)item.Yil;
                            yeniAKTS.Ekleyen = kullaniciAdi;
                            yeniAKTS.Zaman = DateTime.Now;

                            db.DersAktsYukIliski.Add(yeniAKTS);
                            db.SaveChanges();
                        }

                    }
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool SilAKTSIsYuku(int dersAktsTnmID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var dil = db.DilDersAktsTnm.FirstOrDefault(x => x.FKDersAktsTnmID == dersAktsTnmID);
                var yuk = db.DersAktsYukIliski.FirstOrDefault(x => x.FKAktsID == dersAktsTnmID);
                var ders = db.DersAktsTnm.FirstOrDefault(x => x.ID == dersAktsTnmID);

                db.DilDersAktsTnm.Remove(dil);
                db.SaveChanges();

                db.DersAktsYukIliski.Remove(yuk);
                db.SaveChanges();

                db.DersAktsTnm.Remove(ders);
                db.SaveChanges();
                return true;
            }
        }
        public static List<DersAktsVM> GetirAktsListesi(byte dil, int yil)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var sonuc = (from aktsTNM in db.DersAktsTnm
                             where aktsTNM.Yil == yil
                             select new DersAktsVM
                             {
                                 AktsID = (int)aktsTNM.DersAktsYukIliski.FirstOrDefault().FKAktsID,
                                 AktsNo = aktsTNM.AktsNo,
                                 AktsAd = aktsTNM.DilDersAktsTnm.FirstOrDefault(x => x.EnumDilID == dil).Icerik,
                                 AktsYukID = (int)aktsTNM.DersAktsYukIliski.FirstOrDefault().ID
                             }).ToList();
                return sonuc;
            }
        }
        #endregion

        #region DERS BİLGİLERİ
        public static DersBilgiVM GetirDersBilgi(int FKDersPlanAnaID, int yil, int EnumDilID, int? birimID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                DersBilgiVM dersBilgi = new DersBilgiVM();
                var ders = db.OGRDersPlanAna.FirstOrDefault(x => x.ID == FKDersPlanAnaID);
                var dBilgi = new DersBilgi();
                var dilDersBilgi = new DilDersBilgi();
                if (db.DersBilgi.Any(x => x.FKDersPlanAnaID == FKDersPlanAnaID && x.Yil == yil))
                {
                    dBilgi = db.DersBilgi.FirstOrDefault(x => x.FKDersPlanAnaID == FKDersPlanAnaID && x.Yil == yil);
                    dilDersBilgi = db.DilDersBilgi.FirstOrDefault(x => x.FKDersBilgiID == dBilgi.ID && x.EnumDilID == EnumDilID);
                }

                var dersKoordinator = db.DersKoordinator.Where(x => x.FKDersPlanAnaID == FKDersPlanAnaID && x.Silindi == false).FirstOrDefault();
                var personelBilgi = db.PersonelBilgisi.FirstOrDefault(x => x.ID == dersKoordinator.FKPersonelID);

                dersBilgi.FKDersPlanAnaID = ders.ID;
                dersBilgi.DersAd = ders.DersAd;
                if (!birimID.HasValue)
                {
                    dersBilgi.DersKodu = ders.OGRDersPlan.Where(x => x.Yil == yil).Select(x => x.BolKodAd + " " + x.DersKod).FirstOrDefault();
                }
                else
                {
                    dersBilgi.DersKodu = ders.OGRDersPlan.Where(x => x.Yil == yil && x.FKBirimID == birimID).Select(x => x.BolKodAd + " " + x.DersKod).FirstOrDefault();
                }

                //dersBilgi.YariYil = ders.yar
                dersBilgi.TUSaat = ders.DSaat + " + " + ders.USaat;
                dersBilgi.Kredi = ders.Kredi;
                dersBilgi.AKTS = ders.ECTSKredi.Value;
                dersBilgi.HesaplananKredi = ders.HesaplananECTS;
                dersBilgi.DersinSeviyesi = ders.EnumDersTipi;
                dersBilgi.HiyerarsikMi = ders.Hiyerarsik;
                if (dilDersBilgi != null)
                {
                    dersBilgi.DersinAmaci = dilDersBilgi.IcerikDersAmac;
                    dersBilgi.DersinIcerigi = dilDersBilgi.IcerikDers;
                    dersBilgi.OnKosulDersleri = dilDersBilgi.IcerikOnKosul;
                    dersBilgi.OnerilenSecmeliDersler = dilDersBilgi.IcerikOnerilenSecmeli;
                    dersBilgi.DersinYardimcilari = dilDersBilgi.IcerikDersYardimcilar;
                    dersBilgi.DersNotlari = dilDersBilgi.IcerikDersNot;
                    dersBilgi.DersKaynaklari = dilDersBilgi.IcerikDersKaynak;
                }
                dersBilgi.FKKoordinatorID = dersKoordinator.FKPersonelID;
                dersBilgi.KoordinatorAdSoyad = personelBilgi.UnvanAd + " " + personelBilgi.Ad + " " + personelBilgi.Soyad;
                dersBilgi.DersiVerenPersonelIDList = db.OGRDersGrupHoca.Where(o => o.OGRDersGrup.FKDersPlanAnaID == FKDersPlanAnaID && o.OGRDersGrup.OgretimYili == yil && o.FKPersonelID != null).Select(o => o.FKPersonelID).Distinct().ToList();
                dersBilgi.EnumDersPlanAnaZorunlu = ders.EnumDersPlanAnaZorunlu;
                return dersBilgi;
            }
        }
        public static bool GuncelleDersBilgi(int FKDersPlanAnaID, DersBilgiVM dersBilgileri, int EBSYil, string kullaniciAdi)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                try
                {
                    //int? dersBilgiID = db.DersBilgi.FirstOrDefault(x => x.FKDersPlanAnaID == FKDersPlanAnaID && x.Yil == EBSYil).ID;
                    if (db.DersBilgi.Any(x => x.FKDersPlanAnaID == FKDersPlanAnaID && x.Yil == EBSYil))
                    {
                        var dersBilgi = db.DersBilgi.FirstOrDefault(x => x.FKDersPlanAnaID == FKDersPlanAnaID && x.Yil == EBSYil);
                        var dilDersBilgi = db.DilDersBilgi.FirstOrDefault(x => x.FKDersBilgiID == dersBilgi.ID && x.EnumDilID == dersBilgileri.EnumDilID);
                        if (dilDersBilgi != null)
                        {
                            dilDersBilgi.IcerikDersAmac = dersBilgileri.DersinAmaci;
                            dilDersBilgi.IcerikDers = dersBilgileri.DersinIcerigi;
                            dilDersBilgi.IcerikOnKosul = dersBilgileri.OnKosulDersleri;
                            dilDersBilgi.IcerikOnerilenSecmeli = dersBilgileri.OnerilenSecmeliDersler;
                            dilDersBilgi.IcerikDersYardimcilar = dersBilgileri.DersinYardimcilari;
                            dilDersBilgi.IcerikDersNot = dersBilgileri.DersNotlari;
                            dilDersBilgi.IcerikDersKaynak = dersBilgileri.DersKaynaklari;
                            dilDersBilgi.Ekleyen = kullaniciAdi;
                            dilDersBilgi.Zaman = DateTime.Now;
                        }
                        else
                        {
                            DilDersBilgi yeniDDB = new DilDersBilgi();
                            yeniDDB.FKDersBilgiID = dersBilgi.ID;
                            yeniDDB.IcerikDersAmac = dersBilgileri.DersinAmaci;
                            yeniDDB.IcerikDers = dersBilgileri.DersinIcerigi;
                            yeniDDB.IcerikOnKosul = dersBilgileri.OnKosulDersleri;
                            yeniDDB.IcerikOnerilenSecmeli = dersBilgileri.OnerilenSecmeliDersler;
                            yeniDDB.IcerikDersYardimcilar = dersBilgileri.DersinYardimcilari;
                            yeniDDB.IcerikDersNot = dersBilgileri.DersNotlari;
                            yeniDDB.IcerikDersKaynak = dersBilgileri.DersKaynaklari;
                            yeniDDB.EnumDilID = (byte)dersBilgileri.EnumDilID;
                            yeniDDB.Ekleyen = kullaniciAdi;
                            yeniDDB.Zaman = DateTime.Now;

                            db.DilDersBilgi.Add(yeniDDB);
                        }

                        db.SaveChanges();
                    }
                    else
                    {
                        var DB = new DersBilgi();
                        var DDB1 = new DilDersBilgi();
                        var DDB2 = new DilDersBilgi();

                        DB.FKDersPlanAnaID = FKDersPlanAnaID;
                        DB.FKDilID = 1;
                        DB.Yil = (short)EBSYil;
                        DB.Ekleyen = kullaniciAdi;
                        DB.Zaman = DateTime.Now;

                        db.DersBilgi.Add(DB);
                        db.SaveChanges();

                        DDB1.FKDersBilgiID = DB.ID;
                        DDB1.EnumDilID = 1;
                        DDB1.Ekleyen = kullaniciAdi;
                        DDB1.Zaman = DateTime.Now;
                        DDB1.IcerikDersAmac = dersBilgileri.DersinAmaci;
                        DDB1.IcerikDers = dersBilgileri.DersinIcerigi;
                        DDB1.IcerikOnKosul = dersBilgileri.OnKosulDersleri;
                        DDB1.IcerikOnerilenSecmeli = dersBilgileri.OnerilenSecmeliDersler;
                        DDB1.IcerikDersYardimcilar = dersBilgileri.DersinYardimcilari;
                        DDB1.IcerikDersNot = dersBilgileri.DersNotlari;
                        DDB1.IcerikDersKaynak = dersBilgileri.DersKaynaklari;

                        DDB2.FKDersBilgiID = DB.ID;
                        DDB2.EnumDilID = 2;
                        DDB2.Ekleyen = kullaniciAdi;
                        DDB2.Zaman = DateTime.Now;

                        db.DilDersBilgi.Add(DDB1);
                        db.DilDersBilgi.Add(DDB2);
                        db.SaveChanges();
                    }
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }
        public static List<DersProgramCiktilariVM> GetirDersProgramCiktiKatkiDuzeyi(int dersPlanAnaID, int birimID, int yil, int enumDilID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return db.DersYeterlilikIliski.Where(x => x.FKDersPlanAnaID == dersPlanAnaID && x.Yil == yil && x.FKProgramBirimID == birimID && x.Silindi == false).Select(x => new DersProgramCiktilariVM
                {
                    ID = x.ID,
                    DersPlanAnaID = x.FKDersPlanAnaID.Value,
                    //FKBirimID = x.FKProgramBirimID.Value,
                    Yil = x.Yil,
                    Icerik = x.BolumYeterlilik.DilBolumYeterlilik.FirstOrDefault(y => y.EnumDilID == enumDilID).Icerik,
                    KatkiDuzeyi = (int)x.KatkiDuzeyi,
                    Sira = x.BolumYeterlilik.YetID.Value,
                    YeterlilikID = x.BolumYeterlilik.ID
                }).ToList();
            }
        }
        public static bool GuncelleDersProgramCiktiKatkiDuzeyi(List<DersProgramCiktilariVM> katkiDuzeyList, int fkDersPlanAnaID, int yil)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                try
                {
                    int birimID = katkiDuzeyList.FirstOrDefault().FKBirimID;
                    var iliskiList = db.DersYeterlilikIliski.Where(x => x.FKDersPlanAnaID == fkDersPlanAnaID && x.FKProgramBirimID == birimID && x.Yil == yil && x.Silindi == false).ToList();

                    foreach (var item in iliskiList)
                    {
                        item.Silindi = true;
                    }
                    db.SaveChanges();

                    foreach (var item in katkiDuzeyList)
                    {
                        DersYeterlilikIliski yeniDuzey = new DersYeterlilikIliski();
                        yeniDuzey.FKDersPlanAnaID = item.DersPlanAnaID;
                        yeniDuzey.FKProgramBirimID = item.FKBirimID;
                        yeniDuzey.Yil = (short)item.Yil;
                        yeniDuzey.KatkiDuzeyi = (byte)item.KatkiDuzeyi;
                        yeniDuzey.FKYeterlilikID = item.YeterlilikID;
                        yeniDuzey.Ekleyen = item.KullaniciAdi;
                        yeniDuzey.Zaman = item.Zaman;
                        yeniDuzey.Silindi = false;
                        db.DersYeterlilikIliski.Add(yeniDuzey);
                        db.SaveChanges();
                    }
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }
        public static List<DersKategoriVM> GetirDersKategoriListe(int dersPlanAnaID, int yil, int EnumDilID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return db.DilDersKategoriTnm.Where(x => x.DersKategoriTnm.Yil == yil && x.EnumDilID == EnumDilID).Select(x => new DersKategoriVM
                {
                    ID = x.FKDersKategoriTnmID.Value,
                    Yil = (short)x.DersKategoriTnm.Yil,
                    Icerik = x.Icerik,
                    EnumDilID = x.EnumDilID.Value,
                    SeciliMi = db.DersKategoriIliski.Any(y => y.FKDersPlanAnaID == dersPlanAnaID && y.FKDersKategoriID == x.FKDersKategoriTnmID)
                }).ToList();
            }
        }
        public static bool GuncelleDersKategori(int dersPlanAnaID, int dersKategoriID, int yil, string Ekleyen)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                if (db.DersKategoriIliski.Any(x => x.FKDersPlanAnaID == dersPlanAnaID))
                {
                    var iliski = db.DersKategoriIliski.FirstOrDefault(x => x.FKDersPlanAnaID == dersPlanAnaID);
                    iliski.FKDersKategoriID = dersKategoriID;
                    iliski.Yil = (short)yil;
                    iliski.Ekleyen = Ekleyen;
                    iliski.Zaman = DateTime.Now;
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    DersKategoriIliski yeniIliski = new DersKategoriIliski();
                    yeniIliski.FKDersPlanAnaID = dersPlanAnaID;
                    yeniIliski.FKDersKategoriID = dersKategoriID;
                    yeniIliski.Yil = (short)yil;
                    yeniIliski.Ekleyen = Ekleyen;
                    yeniIliski.Zaman = DateTime.Now;
                    db.DersKategoriIliski.Add(yeniIliski);
                    db.SaveChanges();
                    return true;
                }
            }
        }
        public static bool GuncelleDersKoordinator(int FKDersPlanAnaID, int FKPersonelID, string kullaniciAdi)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    if (db.DersKoordinator.FirstOrDefault(x => x.FKDersPlanAnaID == FKDersPlanAnaID) != null)
                    {
                        var dk = db.DersKoordinator.Where(x => x.FKDersPlanAnaID == FKDersPlanAnaID).OrderByDescending(x => x.ID).FirstOrDefault();
                        dk.FKPersonelID = FKPersonelID;
                    }
                    else
                    {
                        DersKoordinator dk = new DersKoordinator();
                        dk.FKDersPlanAnaID = FKDersPlanAnaID;
                        dk.FKPersonelID = FKPersonelID;
                        dk.BaslangicTarih = DateTime.Now;
                        dk.BitisTarih = DateTime.Now.AddYears(1);
                        dk.Ekleyen = kullaniciAdi;
                        dk.Zaman = DateTime.Now;
                        db.DersKoordinator.Add(dk);
                    }

                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static List<OgrenmeCiktilariVM> GetirDersOgrenmeCiktilari(int dersPlanAnaID, int Yil, int EnumDilID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return (from ogrenme in db.DersCikti
                        join dilogrenme in db.DilDersCikti on ogrenme.ID equals dilogrenme.FKDersCiktiID
                        where ogrenme.FKDersPlanAnaID == dersPlanAnaID && ogrenme.Yil == Yil && dilogrenme.EnumDilID == EnumDilID
                        select new OgrenmeCiktilariVM
                        {
                            ID = ogrenme.ID,
                            FKDersPlanAnaID = ogrenme.FKDersPlanAnaID,
                            Ekleyen = ogrenme.Ekleyen,
                            Yil = ogrenme.Yil,
                            CiktiNo = ogrenme.CiktiNo,
                            Zaman = ogrenme.Zaman,
                            Icerik = dilogrenme.Icerik,
                            DilDersCiktiID = dilogrenme.ID,
                            EnumDilID = (byte)dilogrenme.EnumDilID,
                            DersCiktiYontemleri = db.DersCiktiYontemIliski.Where(z => z.FKCiktiID == ogrenme.ID).Select(z => new DersCiktiYontemVM
                            {
                                        ID = z.ID,
                                        YontemTur = z.DersCiktiYontem.YontemTur,
                                        YontemNo = z.DersCiktiYontem.YontemNo,
                                        Icerik = z.DersCiktiYontem.DilDersCiktiYontem.FirstOrDefault().Icerik,
                                        Yil = z.Yil
                            }).ToList(),
                        }).OrderBy(x => x.CiktiNo).ToList();
            }
        }
        #endregion

        public static List<DersListesiVM> GetirBirimUniversiteOrtakDersList(int FKBirimID, int yil)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                List<int> dersIDList = db.OGRDersPlan.Where(o => o.FKBirimID == FKBirimID && o.Yil == yil &&
                                                                  o.EnumDersBirimTipi == (int)Sabis.Enum.EnumDersBirimTipi.UNIVERSITE

                                                            ).Select(x => x.FKDersPlanAnaID.Value).Distinct().ToList();

                return db.OGRDersPlanAna.Where(x => dersIDList.Contains(x.ID) && x.EnumDersAktif == (int)Sabis.Enum.EnumDersAktif.AKTIF).Select(x => new DersListesiVM
                {
                    ID = x.ID,
                    DersAd = x.DersAd
                }).ToList();
            }
        }

        public static List<Sabis.Bolum.Core.GENEL.BirimVM> GetirDersBirimListesi(int FKDersPlanAnaID, int yil, bool fakulteGoster = false)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                if (fakulteGoster)
                {
                    List<Sabis.Bolum.Core.GENEL.BirimVM> birimList = db.OGRDersPlan.Where(x => x.FKDersPlanAnaID == FKDersPlanAnaID && x.Yil == yil).Select(x => new Sabis.Bolum.Core.GENEL.BirimVM
                    {
                        ID = x.FKBirimID,
                        Ad = x.Birimler.BirimAdi,
                        EnumBirimTuru = x.Birimler.EnumBirimTuru,
                        FKDersPlanID = x.ID
                    }).ToList();

                    return birimList.Where(x => x.EnumBirimTuru != 0).Select(x => new Sabis.Bolum.Core.GENEL.BirimVM
                    {
                        ID = x.ID,
                        Ad = x.Ad + " (" + Sabis.Client.BirimIslem.Instance.getirBagliFakulte(x.ID).BirimAdi + ")",
                        FKDersPlanID = x.ID
                    }).ToList();
                }
                else
                {
                    return db.OGRDersPlan.Where(x => x.FKDersPlanAnaID == FKDersPlanAnaID && x.Yil == yil).Select(x => new Sabis.Bolum.Core.GENEL.BirimVM
                    {
                        ID = x.FKBirimID,
                        Ad = x.Birimler.BirimAdi,
                        FKDersPlanID = x.ID
                    }).ToList();
                }
            }
        }
        //TabloYok
        //public static bool KaydetDersCiktiYeterlilikIliski(int fkDersPlanAnaID, int fkCiktiID, int fkBirimID, List<int> pCiktiList, int yil, string ekleyen)
        //{
        //    using (UYSv2Entities db = new UYSv2Entities())
        //    {
        //        try
        //        {
        //            List<DersCiktiYeterlilikIliski> cList = db.DersCiktiYeterlilikIliski.Where(x => x.FKDersPlanAnaID == fkDersPlanAnaID && x.FKCiktiID == fkCiktiID && x.FKBirimID == fkBirimID && x.Yil == yil).ToList();
        //            foreach (var item in cList)
        //            {
        //                db.DersCiktiYeterlilikIliski.Remove(item);
        //            }
        //            db.SaveChanges();

        //            if (pCiktiList != null)
        //            {
        //                foreach (var item in pCiktiList)
        //                {
        //                    DersCiktiYeterlilikIliski yeniDCYI = new DersCiktiYeterlilikIliski();
        //                    yeniDCYI.FKDersPlanAnaID = fkDersPlanAnaID;
        //                    yeniDCYI.FKCiktiID = fkCiktiID;
        //                    yeniDCYI.FKBirimID = fkBirimID;
        //                    yeniDCYI.FKYeterlilikID = item;
        //                    yeniDCYI.Zaman = DateTime.Now;
        //                    yeniDCYI.Yil = yil;
        //                    yeniDCYI.Ekleyen = ekleyen;
        //                    db.DersCiktiYeterlilikIliski.Add(yeniDCYI);
        //                }
        //                db.SaveChanges();
        //            }

        //            return true;
        //        }
        //        catch (Exception)
        //        {
        //            return false;
        //        }
        //    }
        //}

        public static bool GuncelleYazOkuluDersi(int FKDersPlanAnaID, int yil, int? FakulteID, int? FKBirimID, bool durum, string kullanici, int? FKProgramID, bool dersKontrol = true)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    if (durum)
                    {
                        if (dersKontrol && db.YazOkuluDersler.Count(x => x.FKBirimID == FKBirimID && x.FKProgramID == FKProgramID && x.Yil == yil) > 10)
                        {
                            return false;
                        }
                        if (!db.YazOkuluDersler.Any(x => x.FKDersPlanAnaID == FKDersPlanAnaID && x.FKFakulteID == FakulteID && x.FKBirimID == FKBirimID))
                        {

                            YazOkuluDersler yeniDers = new YazOkuluDersler();
                            yeniDers.FKDersPlanAnaID = FKDersPlanAnaID;
                            yeniDers.FKFakulteID = FakulteID;
                            yeniDers.FKBirimID = FKBirimID;
                            yeniDers.FKProgramID = FKProgramID;
                            yeniDers.EnumDersTipi = db.OGRDersPlanAna.FirstOrDefault(x => x.ID == FKDersPlanAnaID).EnumDersTipi;
                            yeniDers.EkleyenKullanici = kullanici;
                            yeniDers.Yil = yil;
                            yeniDers.Zaman = DateTime.Now;
                            db.YazOkuluDersler.Add(yeniDers);

                            db.SaveChanges();
                        }
                    }
                    else
                    {
                        var item = db.YazOkuluDersler.FirstOrDefault(x => x.FKDersPlanAnaID == FKDersPlanAnaID && x.FKFakulteID == FakulteID && x.FKBirimID == FKBirimID);
                        db.YazOkuluDersler.Remove(item);
                        db.SaveChanges();
                    }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        

        public static List<DersListesiVM> GetirUniversiteOrtakDersListesi(int yil)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var dersPlanIDList = db.OGRDersPlan.Where(x => x.Yil == yil && x.EnumDersBirimTipi == (int)Sabis.Enum.EnumDersBirimTipi.UNIVERSITE).Select(x => x.FKDersPlanAnaID).Distinct().ToList();

                //var dpAnaIDList = db.OGRDersPlanAna.Where(x => x.EnumDersTipi == (int)Sabis.Enum.EnumDersTipi.UNIVERSITE_DERSI && x.EnumDersAktif == (int)Sabis.Enum.EnumDersAktif.AKTIF).Select(x => x.FKDersPlanAnaID).Distinct().ToList();

                //var dersIDList = dersPlanIDList.Concat(dpAnaIDList).Distinct().ToList();

                return db.OGRDersPlanAna.Where(x => dersPlanIDList.Contains(x.ID) && x.EnumDersAktif == (int)Sabis.Enum.EnumDersAktif.AKTIF).OrderBy(x => x.DersAd).Select(x => new DersListesiVM
                {
                    ID = x.ID,
                    DersAd = x.DersAd,
                    //DersKod = db.OGRDersPlan.FirstOrDefault(y => y.FKDersPlanAnaID == x.FKDersPlanAnaID.Value && y.FKBirimID == FKBirimID).BolKodAd + " " + db.OGRDersPlan.FirstOrDefault(y => y.FKDersPlanAnaID == x.FKDersPlanAnaID.Value && y.FKBirimID == FKBirimID).DersKod,
                    DSaat = x.DSaat,
                    USaat = x.USaat,
                    AKTS = x.ECTSKredi,
                    DersTipi = x.EnumDersTipi,
                    YazOkuluDersiMi = db.YazOkuluDersler.Any(y => y.FKDersPlanAnaID == x.ID && y.Yil == yil),
                    FKKoordinatorID = db.DersKoordinator.FirstOrDefault(y => y.FKDersPlanAnaID == x.ID && y.Silindi == false).FKPersonelID,
                    KoordinatorAdSoyad = db.DersKoordinator.Where(y => y.FKDersPlanAnaID == x.ID && y.Silindi == false).Select(y => y.PersonelBilgisi.UnvanAd + " " + y.PersonelBilgisi.Ad + " " + y.PersonelBilgisi.Soyad).FirstOrDefault()
                }).ToList();
            }
        }
        public static int GetirKoordinatorIDByDersPlanAnaID(int FKDersPlanAnaID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return db.DersKoordinator.FirstOrDefault(x => x.FKDersPlanAnaID == FKDersPlanAnaID && x.Silindi == false).FKPersonelID;
            }
        }
    }
}
