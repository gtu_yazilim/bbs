﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sabis.Bolum.Core.EBS.DERS;
using Sabis.Bolum.Bll.DATA.UysModel;

namespace Sabis.Bolum.Bll.SOURCE.EBS.DERS
{
    public class YAZOKULUMAIN
    {
        public static List<DersListesiVM> ListeleYazOkuluDersleri(int FKBirimID, int yil)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return db.YazOkuluDersler.Where(x => x.FKBirimID == FKBirimID && x.Yil == yil).Select(x => new DersListesiVM
                {
                    ID = x.FKDersPlanAnaID.Value,
                    DersAd = db.OGRDersPlanAna.FirstOrDefault(y => y.ID == x.FKDersPlanAnaID.Value).DersAd,
                    DersKod = db.OGRDersPlan.FirstOrDefault(y => y.FKDersPlanAnaID == x.FKDersPlanAnaID.Value && y.FKBirimID == FKBirimID).BolKodAd + " " + db.OGRDersPlan.FirstOrDefault(y => y.FKDersPlanAnaID == x.FKDersPlanAnaID.Value && y.FKBirimID == FKBirimID).DersKod,
                    DSaat = db.OGRDersPlanAna.FirstOrDefault(y => y.ID == x.FKDersPlanAnaID.Value).DSaat,
                    USaat = db.OGRDersPlanAna.FirstOrDefault(y => y.ID == x.FKDersPlanAnaID.Value).USaat,
                    AKTS = db.OGRDersPlanAna.FirstOrDefault(y => y.ID == x.FKDersPlanAnaID.Value).ECTSKredi,
                    DersTipi = db.OGRDersPlanAna.FirstOrDefault(y => y.ID == x.FKDersPlanAnaID.Value).EnumDersTipi,
                    EnumDersBirimTipi = db.OGRDersPlan.FirstOrDefault(y=> y.FKDersPlanAnaID == x.FKDersPlanAnaID && y.FKBirimID == FKBirimID).EnumDersBirimTipi
                }).ToList();
            }
        }

        public static bool KaydetYazOkuluDersleri(YazOkuluDersleriVM yazOkuluDersleri, string kullanici)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    //var dersListFiltre = ListeleYazOkuluDersleri(yazOkuluDersleri.FKBirimID, yazOkuluDersleri.Yil);
                    ////yetkiye göre filtre
                    //var yetkiliFakulteList = BIRIM.FAKULTEMAIN.GetirFakulteYetkiListesi(yazOkuluDersleri.FKPersonelID);
                    //int? fakulteID = Sabis.Client.BirimIslem.Instance.getirBagliFakulteID(yazOkuluDersleri.FKBirimID);
                    //if (fakulteID.HasValue && !yetkiliFakulteList.Any(x => x == fakulteID))
                    //{
                    //    dersListFiltre = dersListFiltre.Where(x => x.EnumDersBirimTipi == (int)Sabis.Enum.EnumDersBirimTipi.BOLUM).ToList();
                    //}
                    //var dersIDList = dersListFiltre.Select(x => x.ID).ToList();
                    ////yetkiye göre filtre

                    var dersPLan = db.OGRDersPlan.Where(x => x.FKBirimID == yazOkuluDersleri.FKBirimID && x.Yil == yazOkuluDersleri.Yil).ToList();
                    var bolumDersList = dersPLan.Where(x => yazOkuluDersleri.DersList.Contains(x.FKDersPlanAnaID.Value) && x.EnumDersBirimTipi == (int)Sabis.Enum.EnumDersBirimTipi.BOLUM).ToList();

                    if (bolumDersList.Count > 10)
                    {
                        return false;
                    }

                    var dersList = db.YazOkuluDersler.Where(x => x.FKBirimID == yazOkuluDersleri.FKBirimID && x.Yil == yazOkuluDersleri.Yil).ToList(); // && dersIDList.Contains(x.FKDersPlanAnaID.Value)

                    foreach (var ders in dersList)
                    {
                        db.YazOkuluDersler.Remove(ders);
                    }
                    db.SaveChanges();

                    foreach (var ders in yazOkuluDersleri.DersList)
                    {
                        YazOkuluDersler yeniDers = new YazOkuluDersler();
                        yeniDers.FKFakulteID = yazOkuluDersleri.FakulteID;
                        yeniDers.FKBirimID = yazOkuluDersleri.FKBirimID;
                        yeniDers.FKDersPlanAnaID = ders;
                        yeniDers.Yil = yazOkuluDersleri.Yil;
                        yeniDers.EkleyenKullanici = kullanici;
                        yeniDers.Zaman = DateTime.Now;
                        db.YazOkuluDersler.Add(yeniDers);
                    }
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception error)
            {
                return false;
            }
        }
    }
}
