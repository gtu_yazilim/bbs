﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Bolum.Core.EBS.DERS;

namespace Sabis.Bolum.Bll.SOURCE.EBS.DERS
{
    public class YANDALMAIN
    {
        public static List<YandalVM> GetirYandalListesi(int fkBirimID, int yil, int donem)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                //return db.OGRCapYandalTanim.Where(x => x.FKAnaBirimID == fkBirimID && x.EnumGecisTipi == (int)Sabis.Enum.EnumGecisProgramTipi.YANDAL && x.CapYandalKontenjan. && !x.Silindi).Select(x => new YandalVM
                //{
                //    ID = x.ID,
                //    EnumGecisTipi = x.EnumGecisTipi,
                //    BirimAdi = x.Birimler.BirimAdi,
                //    FKAnaBirimID = x.FKAnaBirimID,
                //    FKAltBirimID = x.FKAltBirimID,
                //    Silindi = x.Silindi,
                //    Kullanici = x.Kullanici,
                //    TarihKayit = x.TarihKayit,
                //    Yil = x.CapYandalKontenjan.FirstOrDefault(y=> y.FKCapYandalTanimID == x.ID).Yil,
                //    Donem = x.CapYandalKontenjan.FirstOrDefault(y => y.FKCapYandalTanimID == x.ID).Donem,
                //    Kontenjan = x.CapYandalKontenjan.FirstOrDefault(y => y.FKCapYandalTanimID == x.ID).Adet,
                //    DersAdet = x.OGRCapDersPlan.Count(y=> y.FKCapTanimID == x.ID && !y.Silindi),
                //    AKTS = x.OGRCapDersPlan.Any(y => y.FKCapTanimID == x.ID && !y.Silindi && y.OGRDersPlanAna.ECTSKredi.HasValue) ? x.OGRCapDersPlan.Where(y => y.FKCapTanimID == x.ID && !y.Silindi).Sum(y=> y.OGRDersPlanAna.ECTSKredi.Value) : 0,
                //}).ToList();

                return db.CapYandalKontenjan.Where(x => x.OGRCapYandalTanim.FKAnaBirimID == fkBirimID && x.OGRCapYandalTanim.EnumGecisTipi == (int)Sabis.Enum.EnumGecisProgramTipi.YANDAL && x.Yil == yil && x.Donem == donem && !x.Silindi && !x.OGRCapYandalTanim.Silindi).Select(x => new YandalVM
                {
                    ID = x.FKCapYandalTanimID,
                    EnumGecisTipi = x.OGRCapYandalTanim.EnumGecisTipi,
                    //BirimAdi = x.OGRCapYandalTanim.Birimler.BirimAdi,
                    BirimAdi = "",
                    FKAnaBirimID = x.OGRCapYandalTanim.FKAnaBirimID,
                    FKAltBirimID = x.OGRCapYandalTanim.FKAltBirimID,
                    Silindi = x.OGRCapYandalTanim.Silindi,
                    Kullanici = x.OGRCapYandalTanim.Kullanici,
                    TarihKayit = x.OGRCapYandalTanim.TarihKayit,
                    Yil = x.Yil,
                    Donem = x.Donem,
                    Kontenjan = x.Adet,
                    DersAdet = x.OGRCapYandalTanim.OGRCapDersPlan.Count(y => y.FKCapTanimID == x.FKCapYandalTanimID && !y.Silindi),
                    AKTS = x.OGRCapYandalTanim.OGRCapDersPlan.Any(y => y.FKCapTanimID == x.FKCapYandalTanimID && !y.Silindi && y.OGRDersPlanAna.ECTSKredi.HasValue) ? x.OGRCapYandalTanim.OGRCapDersPlan.Where(y => y.FKCapTanimID == x.FKCapYandalTanimID && !y.Silindi).Sum(y => y.OGRDersPlanAna.ECTSKredi.Value) : 0,
                }).ToList();
            }
        }

        public static bool EkleYeniYandal(YandalVM yandal)
        {
            try
            {
                
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    if (!db.CapYandalKontenjan.Any(x=> x.OGRCapYandalTanim.FKAnaBirimID == yandal.FKAnaBirimID && x.OGRCapYandalTanim.EnumGecisTipi == (int)Sabis.Enum.EnumGecisProgramTipi.YANDAL && x.Yil == yandal.Yil && x.Donem == yandal.Donem && !x.Silindi && !x.OGRCapYandalTanim.Silindi))//!db.OGRCapYandalTanim.Any(x=> x.FKAnaBirimID == yandal.FKAnaBirimID)
                    {
                        OGRCapYandalTanim yeniYandal = new OGRCapYandalTanim();
                        yeniYandal.EnumGecisTipi = yandal.EnumGecisTipi;
                        yeniYandal.FKAnaBirimID = yandal.FKAnaBirimID;
                        yeniYandal.FKAltBirimID = yandal.FKAnaBirimID;
                        yeniYandal.Silindi = false;
                        yeniYandal.Kullanici = yandal.Kullanici;
                        yeniYandal.TarihKayit = DateTime.Now;
                        //yeniYandal.FKEskiAnaBirimID = 21398;
                        //yeniYandal.FKEskiAltBirimID = 21398.ToString();
                        db.OGRCapYandalTanim.Add(yeniYandal);
                        db.SaveChanges();

                        CapYandalKontenjan yandalKontenjan = new CapYandalKontenjan();
                        yandalKontenjan.Kullanici = yandal.Kullanici;
                        yandalKontenjan.TarihKayit = DateTime.Now;
                        yandalKontenjan.FKCapYandalTanimID = yeniYandal.ID;
                        yandalKontenjan.Yil = yandal.Yil;
                        yandalKontenjan.Donem = yandal.Donem;
                        yandalKontenjan.Adet = yandal.Kontenjan;
                        yandalKontenjan.Silindi = false;
                        db.CapYandalKontenjan.Add(yandalKontenjan);
                        db.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                    
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static List<Sabis.Core.DtoBirimler> GetirYandalBirimList(int fkFakulteID)
        {
            return Sabis.Client.BirimIslem.Instance.getirAltBirimler(fkFakulteID, false, false, new List<int> { 114 }).OrderBy(x=> x.BirimAdi).ToList();
        }

        public static List<YandalDersVM> GetirYandalDersList(int fkCapYandalTanimID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return db.OGRCapDersPlan.Where(x => x.FKCapTanimID == fkCapYandalTanimID).Select(x => new YandalDersVM
                {
                    ID = x.ID,
                    DersAdi = x.OGRDersPlanAna.DersAd,
                    FKDersPlanAnaID = x.FKAnaDersPlanAnaID,
                    FKCapTanimID = x.FKCapTanimID
                }).ToList();
            }
        }

        public static bool EkleYeniYandalDers(List<YandalDersVM> yandalDersList)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    //öncekileri sil
                    int capTanimID = yandalDersList.First().FKCapTanimID.Value;
                    var dersList = db.OGRCapDersPlan.Where(x => x.FKCapTanimID == capTanimID);
                    foreach (var item in dersList)
                    {
                        item.Silindi = true;
                        item.TarihSilinme = DateTime.Now;
                    }
                    db.SaveChanges();
                    //öncekileri sil


                    foreach (var ders in yandalDersList)
                    {
                        int dpID = db.OGRDersPlan.FirstOrDefault(x => x.FKDersPlanAnaID == ders.FKDersPlanAnaID && x.Yil == ders.Yil).ID;
                        OGRCapDersPlan yeniYandalDers = new OGRCapDersPlan();
                        yeniYandalDers.FKCapTanimID = ders.FKCapTanimID.Value;
                        yeniYandalDers.TarihKayit = DateTime.Now;
                        yeniYandalDers.Kullanici = ders.Kullanici;
                        yeniYandalDers.Yil = ders.Yil;
                        yeniYandalDers.FKAnaDersPlanAnaID = ders.FKDersPlanAnaID;
                        yeniYandalDers.FKCapDersPlanAnaID = ders.FKDersPlanAnaID;
                        yeniYandalDers.FKAnaDersPlanID = dpID;
                        yeniYandalDers.FKCapDersPlanID = dpID;
                        yeniYandalDers.Silindi = false;
                        db.OGRCapDersPlan.Add(yeniYandalDers);
                    }
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static List<YandalDersVM> GetirBolumDersList(int fkCapYandalTanimID, int yil)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                int birimID = db.OGRCapYandalTanim.FirstOrDefault(x => x.ID == fkCapYandalTanimID).FKAnaBirimID;
                int bolumID = Sabis.Client.BirimIslem.Instance.getirBagliBolumID(birimID);
                
                List<int> seciliDersler = db.OGRCapDersPlan.Where(x => x.FKCapTanimID == fkCapYandalTanimID && !x.Silindi).Select(x => x.FKAnaDersPlanAnaID).ToList();

                return db.OGRDersPlan.Where(x => x.FKBirimID == bolumID && x.Yil == yil).Select(x => new YandalDersVM
                {
                    ID = x.ID,
                    FKDersPlanAnaID = x.FKDersPlanAnaID.Value,
                    DersAdi = x.OGRDersPlanAna.DersAd,
                    AKTS = x.OGRDersPlanAna.ECTSKredi.Value,
                    Yil = x.Yil.Value,
                    Silindi = false,
                    Secili = seciliDersler.Any(y=> y == x.FKDersPlanAnaID)
                }).OrderByDescending(x=> x.Secili).ThenBy(x=> x.DersAdi).ToList();
            }
        }

        public static bool GuncelleKontenjan(int fkTanimID, int adet)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var kontenjan = db.CapYandalKontenjan.FirstOrDefault(x => x.FKCapYandalTanimID == fkTanimID);
                    kontenjan.Adet = adet;
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
