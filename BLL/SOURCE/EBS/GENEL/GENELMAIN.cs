﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Bolum.Core.ENUM.EBS;


namespace Sabis.Bolum.Bll.SOURCE.EBS.GENEL
{
    public class GENELMAIN
    {
        public static bool DersKoordinatoruMu(int FKDersPlanAnaID, int FKPersonelID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                string kullaniciAdi = db.PersonelBilgisi.FirstOrDefault(x => x.ID == FKPersonelID).KullaniciAdi;
                return db.DersKoordinator.Any(x => x.FKDersPlanAnaID == FKDersPlanAnaID && x.FKPersonelID == FKPersonelID && x.Silindi == false) || db.OGRDersPlanAna.Any(x => x.ID == FKDersPlanAnaID && x.Koordinator == kullaniciAdi);
            }
        }
        public static bool DersKoordinatorluguVarMi(int FKPersonelID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return db.DersKoordinator.Any(x => x.FKPersonelID == FKPersonelID && x.Silindi == false);
            }
        }
        public static bool OrtakDersKoordinatoruMu(int FKPersonelID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return db.Yetkililer.Any(x => x.FKPersonelID == FKPersonelID && x.YetkiGrupID == 1);
            }
        }
        public static bool KoordinatorMu(int FKDersPlanAnaID, int PersonelID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                int pID = db.DersKoordinator.Where(x => x.FKDersPlanAnaID == FKDersPlanAnaID && x.Silindi == false).OrderByDescending(x => x.ID).Select(x => x.FKPersonelID).FirstOrDefault();
                bool sonuc = pID == PersonelID;
                return sonuc;
            }  
        }
        public static bool BolumKoordinatoruMu(int FKPersonelID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                if (db.IdariGorev.Any(x => x.FKPersonelBilgisiID == FKPersonelID && (x.FKGorevUnvaniID ==  10 || x.FKGorevUnvaniID == 12) && x.FKBolumID.HasValue && x.BitisTarihi > DateTime.Now) || db.Yetkililer.Any(x => x.FKPersonelID == FKPersonelID && x.YetkiGrupID == 3 && x.YetkiBitis > DateTime.Now))
                {
                    return true;
                }
                return false;
            }
        }
        public static string GetirDersAdi(int dersPlanAnaID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return db.OGRDersPlanAna.FirstOrDefault(x => x.ID == dersPlanAnaID).DersAd;
            }
        }
        public static int? GetirDersPlanAnaDersTipi(int FKDersPlanAnaID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return db.OGRDersPlanAna.FirstOrDefault(x => x.ID == FKDersPlanAnaID).EnumDersTipi;
            }
        }
        public static List<ABSPaylar> GetirPaylarByDersPlanAnaID(int dersPlanAnaID, int yil, int enumDonem)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return db.ABSPaylar.Where(x => !x.Silindi && x.FKDersPlanAnaID == dersPlanAnaID && x.Yil == yil && x.EnumDonem == enumDonem).ToList();
            }
        }

        public static bool DegerlendirmeGrupKoordinatorluguVarMi(int FKDersPlanAnaID, int FKPersonelID, int yil, int donem)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return db.ABSOrtakDegerlendirme.Any(x => x.FKDersPlanAnaID == FKDersPlanAnaID && x.FKPersonelID == FKPersonelID && x.Yil == yil && x.Donem == donem);
            }
        }

        public static bool DegerlendirmeKoordinatoruMu(int FKDersGrupHocaID, int FKPersonelID, int yil, int donem)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                int DersGrupID = db.OGRDersGrupHoca.FirstOrDefault(x => x.ID == FKDersGrupHocaID).FKDersGrupID.Value;
                if (db.ABSOrtakDegerlendirmeGrup.Any(x=> x.FKDersGrupID == DersGrupID))
                {
                    return db.ABSOrtakDegerlendirmeGrup.FirstOrDefault(x => x.FKDersGrupID == DersGrupID).ABSOrtakDegerlendirme.FKPersonelID == FKPersonelID;
                }
                return false;
            }
        }

        public static bool DegerlendirmeGruplariOlusturulmusMu(int FKDersGrupHocaID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                OGRDersGrup dersGrup = db.OGRDersGrupHoca.FirstOrDefault(x => x.ID == FKDersGrupHocaID).OGRDersGrup;
                return db.ABSOrtakDegerlendirmeGrup.Any(x => x.FKDersGrupID == dersGrup.ID && x.Silindi == false && x.ABSOrtakDegerlendirme.Silindi == false);
            }
        }
    }
}
