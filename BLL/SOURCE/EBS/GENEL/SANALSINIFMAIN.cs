﻿using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Bolum.Core.GENEL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Bll.SOURCE.EBS.GENEL
{
    public class SANALSINIFMAIN
    {
        public static List<DersGrupBilgi> GetirDersGrupBilgi(int FKDersPlanAnaID, int yil, int donem)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var sorgu = (from g in db.OGRDersGrup
                             join h in db.OGRDersGrupHoca on g.ID equals h.FKDersGrupID
                             where
                             g.FKDersPlanAnaID == FKDersPlanAnaID &&
                             g.Acik == true &&
                             g.Silindi == false &&
                             g.OgretimYili == yil &&
                             g.EnumDonem == donem
                             select new DersGrupBilgi {
                                 FKBirimID = g.FKBirimID,
                                 FKDersPlanID = g.FKDersPlanID,
                                 FKDersGrupID = g.ID,
                                 BirimAd = g.Birimler.BirimAdi,
                                 DersAd = g.OGRDersPlan.OGRDersPlanAna.DersAd,
                                 GrupAd = g.GrupAd,
                                 HocaAd = h.PersonelBilgisi.UnvanAd +" "+ h.PersonelBilgisi.Kisi.Ad +" "+ h.PersonelBilgisi.Kisi.Soyad
                             }).ToList();
                return sorgu;
            }
        }

        public static List<SanalSinifRapor> GetirRapor(int FKDersGrupID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var sorgu = (from sg in db.SanalSinifGrup
                             join ss in db.SanalSinif on sg.SSinifID equals ss.ID
                             where
                                 sg.FKDersGrupID == FKDersGrupID &&
                                 ss.Silindi == false &&
                                 ss.Bitis < DateTime.Now
                             select new SanalSinifRapor
                             {
                                 ID = ss.ID,
                                 RoomID = ss.SSinifKey,
                                 PID = ss.SunucuID,
                                 Ad = ss.Ad,
                                 Adres = ss.Adres,
                                 Hafta = ss.Hafta,
                                 AktifSure = ss.AktifSure
                             }).OrderBy(x => x.Hafta).ThenBy(x => x.Ad).ToList();
                return sorgu;
            }
        }
    }
}
