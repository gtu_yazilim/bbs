﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Bolum.Core.EBS.BIRIM;

namespace Sabis.Bolum.Bll.SOURCE.EBS.BIRIM
{
    public class FAKULTEMAIN
    {
        public static bool FakulteKoordinatorluguVarMi(int FKPersonelID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return db.Yetkililer.Any(x => x.FKPersonelID == FKPersonelID && x.YetkiGrupID == (int)Sabis.Bolum.Core.ENUM.EBS.EnumYetkiGrup.FakulteKoordinator) || db.IdariGorev.Any(x=> x.FKPersonelBilgisiID == FKPersonelID && x.FKGorevUnvaniID == 4 && x.BitisTarihi > DateTime.Now);
            }
        }
        public static bool FakulteKoordinatoruMu(int FKPersonelID, int FKFakulteID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                int fakulteID = Sabis.Client.BirimIslem.Instance.getirBagliFakulteID(FKFakulteID);
                return db.Yetkililer.Any(x => x.FKPersonelID == FKPersonelID && x.FKFakulteBirimID == fakulteID && x.YetkiGrupID == (int)Sabis.Bolum.Core.ENUM.EBS.EnumYetkiGrup.FakulteKoordinator);
            }
        }

        public static List<int> GetirFakulteYetkiListesi(int fkPersonelID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var idariYetkiList = db.IdariGorev.Where(x => x.FKPersonelBilgisiID == fkPersonelID && x.FKGorevUnvaniID == 4 && x.BitisTarihi > DateTime.Now).Select(x => x.FKFakulteID.Value).Distinct().ToList();
                var yetkiList = db.Yetkililer.Where(x => x.FKPersonelID == fkPersonelID && x.YetkiGrupID == (int)Sabis.Bolum.Core.ENUM.EBS.EnumYetkiGrup.FakulteKoordinator).Select(x => x.FKFakulteBirimID.Value).Distinct().ToList();
                return yetkiList.Concat(idariYetkiList).ToList();
            }
        }

        public static int DekanligiVarMi(int fkPersonelID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                if (db.IdariGorev.Any(x => x.FKPersonelBilgisiID == fkPersonelID && x.FKGorevUnvaniID == 3 && x.BitisTarihi >= DateTime.Now))
                {
                    return db.IdariGorev.FirstOrDefault(x => x.FKPersonelBilgisiID == fkPersonelID && x.FKGorevUnvaniID == 3 && x.BitisTarihi >= DateTime.Now).FKFakulteID.Value;
                }
                else
                {
                    return 0;
                }
            }
        }

        public static int DekanYardimciligiVarMi(int fkPersonelID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                if (db.IdariGorev.Any(x => x.FKPersonelBilgisiID == fkPersonelID && x.FKGorevUnvaniID == 4 && x.BitisTarihi >= DateTime.Now))
                {
                    return db.IdariGorev.FirstOrDefault(x => x.FKPersonelBilgisiID == fkPersonelID && x.FKGorevUnvaniID == 4 && x.BitisTarihi >= DateTime.Now).FKFakulteID.Value;
                }
                else
                {
                    return 0;
                }
            }
        }

        public static List<KoordinatorDersListesiVM> GetirFakulteDersListesi(int fakulteID, int yil)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                //var kDersIDList = db.DersKoordinator.Where(x => x.FKPersonelID == koordinatorID).Select(x => x.FKDersPlanAnaID).Distinct().ToList();
                //var fakulteOrtakDersList = (from DP in db.OGRDersPlan
                //                            join DPANA in db.OGRDersPlanAna on DP.FKDersPlanAnaID equals DPANA.ID
                //                            where DP.Yil == yil && DP.FKFakulteBirimID == fakulteID && DP.EnumOgretimTur == (int)Sabis.Enum.EnumOgretimTur.OGRETIM_1 && DP.EnumDonem != (int)Sabis.Enum.EnumDonem.YAZ && DP.EnumDersPlanKapsam == (int)Sabis.Enum.EnumDersPlanKapsam.GENEL && DP.EnumEBSGosterim == null &&
                //                            (DP.EnumDersPlanWebGosterim == (int)Sabis.Enum.EnumDersPlanWebGosterim.NORMAL ||
                //                             DP.EnumDersPlanWebGosterim == (int)Sabis.Enum.EnumDersPlanWebGosterim.STAJ ||
                //                             DP.EnumDersPlanWebGosterim == (int)Sabis.Enum.EnumDersPlanWebGosterim.BITIRME ||
                //                             DP.EnumDersPlanWebGosterim == (int)Sabis.Enum.EnumDersPlanWebGosterim.TASARIM ||
                //                             DP.EnumDersPlanWebGosterim == (int)Sabis.Enum.EnumDersPlanWebGosterim.FAALIYETCALISMASI ||
                //                             DP.EnumDersPlanWebGosterim == (int)Sabis.Enum.EnumDersPlanWebGosterim.ISYERI_UYGULAMASI) &&
                //                            DP.EnumDersPlanZorunlu == (int)Sabis.Enum.EnumDersPlanZorunlu.FAKULTE_SOSYAL_SECMELI &&
                //                            DPANA.EnumDersTipi == (int)Sabis.Enum.EnumDersTipi.FAKULTE_DERSI
                //                            select DP.FKDersPlanAnaID).Distinct().ToList();

                string fakulteKod = Convert.ToString(fakulteID);
                List<int> fakulteOrtakDersList = new List<int>();
                List<int> fakulteDersList1 = db.OGRDersPlan.Where(x=> x.FKBirimID == fakulteID && (x.EnumDersBirimTipi == (int)Sabis.Enum.EnumDersBirimTipi.FAKULTE || x.OGRDersPlanAna.EnumDersTipi == (int)Sabis.Enum.EnumDersTipi.FAKULTE_DERSI)).Select(x=> x.FKDersPlanAnaID.Value).ToList();
                List<int> fakutleDersList2 = db.OGRDersPlanAna.Where(x=> x.DersYerFakulteKodu == fakulteKod).Where(x=> x.EnumDersTipi == (int)Sabis.Enum.EnumDersTipi.FAKULTE_DERSI && x.EnumDersAktif == (int)Sabis.Enum.EnumDersAktif.AKTIF).Select(x=> x.ID).ToList();

                fakulteOrtakDersList = fakulteDersList1.Concat(fakutleDersList2).Distinct().ToList();

                List<KoordinatorDersListesiVM> koordinatorListe = db.OGRDersPlanAna.Where(x => fakulteOrtakDersList.Contains(x.ID)).Select(x => new KoordinatorDersListesiVM
                {
                    FKDersPlanAnaID = x.ID,
                    DersAdi = x.DersAd,
                    YazOkuluDersiMi = db.YazOkuluDersler.Any(y => y.FKDersPlanAnaID == x.ID && y.FKFakulteID == fakulteID && y.Yil == yil),
                    ID = db.DersKoordinator.Any(y => y.FKDersPlanAnaID == x.ID && y.Silindi == false) ? (int?)db.DersKoordinator.FirstOrDefault(y=> y.FKDersPlanAnaID == x.ID && y.Silindi == false).ID : null,
                    FKPersonelID = db.DersKoordinator.Any(y => y.FKDersPlanAnaID == x.ID && y.Silindi == false) ? (int?)db.DersKoordinator.FirstOrDefault(y => y.FKDersPlanAnaID == x.ID && y.Silindi == false).FKPersonelID : null,
                    PersonelAdSoyadUnvan = db.DersKoordinator.Any(y => y.FKDersPlanAnaID == x.ID && y.Silindi == false) ? db.DersKoordinator.Where(y => y.FKDersPlanAnaID == x.ID && y.Silindi == false).Select(y=> y.PersonelBilgisi.UnvanAd + " " + y.PersonelBilgisi.Ad + " " + y.PersonelBilgisi.Soyad).FirstOrDefault() : "",
                    FakulteID = fakulteID,
                    FKDersBilgiID = db.DersKoordinator.Any(y => y.FKDersPlanAnaID == x.ID && y.Silindi == false) ? db.DersKoordinator.FirstOrDefault(y => y.FKDersPlanAnaID == x.ID && y.Silindi == false).FKDersBilgiID : null,
                    Ekleyen = db.DersKoordinator.Any(y => y.FKDersPlanAnaID == x.ID && y.Silindi == false) ? db.DersKoordinator.FirstOrDefault(y => y.FKDersPlanAnaID == x.ID && y.Silindi == false).Ekleyen : "",
                    Zaman = db.DersKoordinator.Any(y => y.FKDersPlanAnaID == x.ID && y.Silindi == false) ? db.DersKoordinator.FirstOrDefault(y => y.FKDersPlanAnaID == x.ID && y.Silindi == false).Zaman : DateTime.Now
                }).ToList();

                //foreach (var item in fakulteOrtakDersList)
                //{
                //    KoordinatorDersListesiVM yeniKoordinator = new KoordinatorDersListesiVM();
                //    var dpAna = db.OGRDersPlanAna.FirstOrDefault(x => x.ID == item);

                //    yeniKoordinator.FKDersPlanAnaID = item;
                //    yeniKoordinator.DersAdi = dpAna.DersAd;
                //    //yeniKoordinator.EnumDersBirimTipi = dPlan.EnumDersBirimTipi;
                //    //yeniKoordinator.EnumDersSecimTipi = dPlan.EnumDersSecimTipi;

                //    yeniKoordinator.YazOkuluDersiMi = db.YazOkuluDersler.Any(x => x.FKDersPlanAnaID == item && x.FKFakulteID == fakulteID && x.Yil == yil);
                    

                //    var koordinatorBilgi = db.DersKoordinator.FirstOrDefault(x => x.FKDersPlanAnaID == item);

                //    if (koordinatorBilgi != null)
                //    {
                //        yeniKoordinator.ID = koordinatorBilgi.ID;
                //        yeniKoordinator.FKPersonelID = koordinatorBilgi.FKPersonelID;
                //        if (koordinatorBilgi.PersonelBilgisi.Ad != null)
                //        {
                //            yeniKoordinator.PersonelAdSoyadUnvan = koordinatorBilgi.PersonelBilgisi.UnvanAd + " " + koordinatorBilgi.PersonelBilgisi.Ad + " " + koordinatorBilgi.PersonelBilgisi.Soyad;
                //        }
                //        else
                //        {
                //            yeniKoordinator.PersonelAdSoyadUnvan = "";
                //        }

                //        yeniKoordinator.FakulteID = fakulteID;
                //        yeniKoordinator.FKDersBilgiID = koordinatorBilgi.FKDersBilgiID;
                //        yeniKoordinator.Ekleyen = koordinatorBilgi.Ekleyen;
                //        yeniKoordinator.Zaman = koordinatorBilgi.Zaman;
                //    }

                //    koordinatorListe.Add(yeniKoordinator);

                //}
                return koordinatorListe.OrderBy(x=> x.DersAdi).ToList();
            }
        }

        public static List<KoordinatorDersListesiVM> GetirFakulteBolumDersListesi(int bolumID, int yil, int? personelID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                int fakulteID = Sabis.Client.BirimIslem.Instance.getirBagliFakulteID(bolumID);

                //var fakulteDersIDList = db.OGRDersPlan.Where(x => x.FKFakulteBirimID == fakulteID && x.Yil == yil)
                //                                      .Where(x=> x.FKBirimID == bolumID)
                //                                      .Select(x => x.FKDersPlanAnaID).ToList();

                var fakulteDersIDList = new List<int?>();

                if (personelID.HasValue && personelID == 1908) // ahmet zengin
                {
                    fakulteDersIDList = db.OGRDersPlan.Where(x => x.FKFakulteBirimID == fakulteID && x.Yil == yil)
                                                      .Where(x => x.FKBirimID == bolumID)
                                                      .Select(x => x.FKDersPlanAnaID).ToList();
                }
                else
                {
                    fakulteDersIDList = db.OGRDersPlan.Where(x => x.FKFakulteBirimID == fakulteID && x.Yil == yil)
                                                      .Where(x => x.FKBirimID == bolumID && x.DersKod != "500")
                                                      .Select(x => x.FKDersPlanAnaID).ToList();
                }

                var fakulteDersList = db.OGRDersPlanAna.Where(x => fakulteDersIDList.Contains(x.ID) && x.EnumDersAktif == (int)Sabis.Enum.EnumDersAktif.AKTIF).ToList();

                List<KoordinatorDersListesiVM> koordinatorListe = new List<KoordinatorDersListesiVM>();

                foreach (var item in fakulteDersList)
                {

                    KoordinatorDersListesiVM yeniKoordinator = new KoordinatorDersListesiVM();


                    yeniKoordinator.FKDersPlanAnaID = item.ID;
                    yeniKoordinator.DersAdi = item.DersAd;

                    var koordinatorBilgi = db.DersKoordinator.FirstOrDefault(x => x.FKDersPlanAnaID == item.ID);

                    if (koordinatorBilgi != null)
                    {
                        yeniKoordinator.ID = koordinatorBilgi.ID;
                        yeniKoordinator.FKPersonelID = koordinatorBilgi.FKPersonelID;
                        if (koordinatorBilgi.PersonelBilgisi.Ad != null)
                        {
                            yeniKoordinator.PersonelAdSoyadUnvan = koordinatorBilgi.PersonelBilgisi.UnvanAd + " " + koordinatorBilgi.PersonelBilgisi.Ad + " " + koordinatorBilgi.PersonelBilgisi.Soyad;
                        }
                        else
                        {
                            yeniKoordinator.PersonelAdSoyadUnvan = "";
                        }
                        yeniKoordinator.FakulteID = fakulteID;
                        yeniKoordinator.BolumID = bolumID;
                        yeniKoordinator.FKDersBilgiID = koordinatorBilgi.FKDersBilgiID;
                        yeniKoordinator.Ekleyen = koordinatorBilgi.Ekleyen;
                        yeniKoordinator.Zaman = koordinatorBilgi.Zaman;
                    }

                    koordinatorListe.Add(yeniKoordinator);

                }
                return koordinatorListe;


            }
        }

        public static bool AdaMYOmu(int FKDersPlanAnaID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return db.OGRDersPlanAna.Any(x => x.FKDersPlanAnaID == FKDersPlanAnaID && x.DersYerFakulteKodu == "339");
            }
        }

        //public static List<KoordinatorDersListesiVM> GetirFakulteDersListesi(int fkBirimID, int yil, int EnumDonem, int? personelID, int? FKProgramID, bool yetkiFiltre = false)
        //{

        //    using (UYSv2Entities db = new UYSv2Entities())
        //    {
        //        var koordinatorList = db.DersKoordinator.ToList();
        //        var birimDersList = db.OGRDersPlan.Where(x => x.EnumDersBirimTipi == (int)Sabis.Enum.EnumDersBirimTipi.FAKULTE && x.FKFakulteBirimID == fkBirimID && x.Yil == yil && x.OGRDersPlanAna.EnumDersAktif == (int)Sabis.Enum.EnumDersAktif.AKTIF && !x.OgrenciPlanaEklenmesin).ToList();
        //        var yazOkuluDersList = db.YazOkuluDersler.Where(x => x.FKBirimID == fkBirimID).ToList();

        //        List<KoordinatorDersListesiVM> dersList = new List<KoordinatorDersListesiVM>();

        //        foreach (var dersPlan in birimDersList)
        //        {
        //            KoordinatorDersListesiVM yeniDers = new KoordinatorDersListesiVM();
        //            var personel = koordinatorList.Any(y => y.FKDersPlanAnaID == dersPlan.FKDersPlanAnaID && !y.Silindi) ? koordinatorList.FirstOrDefault(y => y.FKDersPlanAnaID == dersPlan.FKDersPlanAnaID && !y.Silindi).PersonelBilgisi : new PersonelBilgisi();
        //            var ders = dersPlan.OGRDersPlanAna;

        //            yeniDers.ID = ders.ID;
        //            yeniDers.FKPersonelID = personel.ID;
        //            yeniDers.PersonelAdSoyadUnvan = personel.UnvanAd + " " + personel.Ad + " " + personel.Soyad;
        //            yeniDers.FKDersBilgiID = koordinatorList.FirstOrDefault(y => y.FKDersPlanAnaID == ders.ID).FKDersBilgiID;
        //            yeniDers.FKDersPlanAnaID = ders.ID;
        //            yeniDers.FKDersPlanID = ders.ID;
        //            yeniDers.DersAdi = ders.DersAd;
        //            yeniDers.Ekleyen = "";
        //            yeniDers.Zaman = dersPlan.Zaman;
        //            yeniDers.Yariyil = dersPlan.Yariyil;
        //            yeniDers.YazOkuluDersiMi = yazOkuluDersList.Any(y => y.FKDersPlanAnaID == ders.ID);
        //            yeniDers.EnumDersBirimTipi = dersPlan.EnumDersBirimTipi;
        //            yeniDers.EnumDersSecimTipi = dersPlan.EnumDersSecimTipi;
        //            yeniDers.Yil = dersPlan.Yil;
        //            yeniDers.Donem = dersPlan.EnumDonem;

        //            dersList.Add(yeniDers);
        //        }

        //        var birimIDList = birimDersList.Select(x => x.FKDersPlanAnaID).Distinct().ToList();

        //        if (yetkiFiltre)
        //        {
        //            var yetkiliFakulteList = FAKULTEMAIN.GetirFakulteYetkiListesi(personelID.Value);
        //            int? fakulteID = Sabis.Client.BirimIslem.Instance.getirBagliFakulteID(fkBirimID);
        //            if (fakulteID.HasValue && !yetkiliFakulteList.Any(x => x == fakulteID))
        //            {
        //                dersList = dersList.Where(x => x.EnumDersBirimTipi == (int)Sabis.Enum.EnumDersBirimTipi.BOLUM).ToList();
        //            }
        //        }

        //        return dersList;
        //    }
        //}

    }
}
