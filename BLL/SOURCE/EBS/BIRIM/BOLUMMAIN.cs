﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Bolum.Core.EBS.BIRIM;
using Sabis.Bolum.Core.EBS.DERS;
using Sabis.Bolum.Bll.SOURCE.ABS.DERS;
using Sabis.Bolum.Core.ABS.GENEL;
using Sabis.Bolum.Core.ABS.DERS.SINAV;

namespace Sabis.Bolum.Bll.SOURCE.EBS.BIRIM
{
    public class BOLUMMAIN
    {

        //public static bool BolumKoordinatoruMu(int FKPersonelID, int FKDersPlanAnaID)
        //{
        //    using (UYSv2Entities db = new UYSv2Entities())
        //    {
        //        var ders = db.OGRDersPlanAna.FirstOrDefault(x => x.ID == FKDersPlanAnaID);
        //        var dersBolumID = Sabis.Client.BirimIslem.Instance.getirBagliBolumID(ders.f)
        //    }
        //}

        #region BÖLÜM BİLGİ CRUD
        public static List<BolumBilgiVM> GetirBolumBilgi(int? birimID, int yil, int EnumDilID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var ustBirimID = db.Birimler.FirstOrDefault(x => x.ID == birimID).UstBirimID;
                var bolumListe = (from bolumIcerik in db.DilBolumBilgi
                                  join bolumBilgi in db.BolumBilgi on bolumIcerik.FKBolumBilgiID equals bolumBilgi.ID
                                  where bolumBilgi.Yil == yil && bolumIcerik.EnumDilID == EnumDilID && bolumBilgi.FKProgramBirimID.HasValue
                                  select new BolumBilgiVM
                                  {
                                      FKDilBolumBilgiID = bolumIcerik.ID,
                                      BolumID = bolumBilgi.FKProgramBirimID,
                                      ProgramID = bolumBilgi.FKProgramBirimID,
                                      EnumBolumBilgiTur = bolumIcerik.EnumBolumBilgiTur,
                                      Icerik = bolumIcerik.Icerik,
                                      EnumDilID = bolumIcerik.EnumDilID,

                                  }).ToList();

                var list = new List<BolumBilgiVM>();

                if (birimID == 465)
                {
                    list = bolumListe.Select(x => new BolumBilgiVM
                    {
                        FKDilBolumBilgiID = x.FKDilBolumBilgiID,
                        BolumID = Sabis.Client.BirimIslem.Instance.getirBagliBolumID(x.BolumID.Value),
                        ProgramID = x.ProgramID,
                        EnumBolumBilgiTur = x.EnumBolumBilgiTur,
                        Icerik = x.Icerik,
                        EnumDilID = x.EnumDilID,
                    })
                    .Where(x => x.BolumID != x.ProgramID ? x.ProgramID == ustBirimID : x.BolumID == ustBirimID)
                    .ToList();
                }
                else
                {
                    list = bolumListe.Select(x => new BolumBilgiVM
                    {
                        FKDilBolumBilgiID = x.FKDilBolumBilgiID,
                        BolumID = Sabis.Client.BirimIslem.Instance.getirBagliBolumID(x.BolumID.Value),
                        ProgramID = x.ProgramID,
                        EnumBolumBilgiTur = x.EnumBolumBilgiTur,
                        Icerik = x.Icerik,
                        EnumDilID = x.EnumDilID,
                    })
                    .Where(x => x.BolumID != x.ProgramID ? x.ProgramID == birimID : x.BolumID == birimID)
                    .ToList();
                }

                return list;
            }
        }

        //public static List<GeriBildirimSinav> _SinavGeriBildirimleri(int FKDersPlanID,int yil, int enumdonem)
        //{
        //    using (UYSv2Entities db = new UYSv2Entities())
        //    {
        //        var sorgu = (from yazilma in db.OGROgrenciYazilma
        //                     join sg in db.OSinavGrup on yazilma.FKDersGrupID equals sg.FKDersGrupID
        //                     join gb in db.ABSGeriBildirim on new { SINAVID = sg.OSinav.ID, OGRID = yazilma.FKOgrenciID.Value } equals new { SINAVID = gb.AktiviteID, OGRID = gb.OgrenciID }
        //                     where
        //                      yazilma.FKDersPlanID == FKDersPlanID &&
        //                      yazilma.Yil == yil &&
        //                      yazilma.EnumDonem == enumdonem &&
        //                      yazilma.Iptal == false &&
        //                      yazilma.OGRKimlik.EnumOgrDurum == 2
        //                     select new GeriBildirimSinav
        //                     {
        //                         SinavID = sg.FKSinavID,
        //                         FKDersGrupID = yazilma.FKDersGrupID.Value,
        //                         SinavAd = sg.OSinav.Ad,
        //                         GrupAd = yazilma.OGRDersGrup.EnumOgretimTur.ToString() + " - " + yazilma.OGRDersGrup.GrupAd,
        //                         HocaAd = yazilma.OGRDersGrup.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.Ad + " " + yazilma.OGRDersGrup.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.Soyad + " (" + yazilma.OGRDersGrup.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.KullaniciAdi + ")",
        //                         BildirimBaslik = gb.Baslik,
        //                         BildirimIcerik = gb.Icerik,
        //                         BildirimTarih = gb.TarihKayit,
        //                         BildirimDurum = gb.EnumGeriBildirimDurum,
        //                         FKOgrenciID = gb.OgrenciID,
        //                         OgrenciAd = yazilma.OGRKimlik.Kisi.Ad,
        //                         OgrenciSoyad = yazilma.OGRKimlik.Kisi.Soyad,
        //                         OgrenciNumara = yazilma.OGRKimlik.Numara
        //                     }).OrderBy(x => x.BildirimDurum).ThenByDescending(x => x.BildirimTarih).ToList();
        //        return sorgu;
        //    }
        //}

        public static bool GuncelleBolumBilgi(int FKDilBolumBilgiID, string Icerik)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    DilBolumBilgi bolumBilgi = db.DilBolumBilgi.FirstOrDefault(x => x.ID == FKDilBolumBilgiID);
                    bolumBilgi.Icerik = Icerik;
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static List<BolumYeterlilikAltKategoriVM> GetirYeterlilikKategoriListe(int EnumDilID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return db.DilYeterlilikAltKategori.Where(x => x.EnumDilID == EnumDilID).Select(x => new BolumYeterlilikAltKategoriVM
                {
                    ID = x.ID,
                    FKAnaKategoriID = x.YeterlilikAltKategori.FKAnaKategoriID,
                    FKYeterlilikAltKategoriID = x.FKYeterlilikAltKategoriID,
                    IcerikAna = x.YeterlilikAltKategori.YeterlilikAnaKategori.DilYeterlilikAnaKategori.FirstOrDefault(y => x.EnumDilID == EnumDilID).Icerik,
                    IcerikAlt = x.Icerik
                }).ToList();
            }
        }
        public static List<KoordinatorDersListesiVM> GetirBolumDersListesi(int fkBirimID, int yil, int EnumDonem, int? personelID, int? FKProgramID, bool yetkiFiltre = false)
        {

            using (UYSv2Entities db = new UYSv2Entities())
            {
                var koordinatorList = db.DersKoordinator.ToList();
                var birimDersList = db.OGRDersPlan.Where(x => x.FKBirimID == fkBirimID && x.Yil == yil && x.OGRDersPlanAna.EnumDersAktif == (int)Sabis.Enum.EnumDersAktif.AKTIF).ToList();
                var yazOkuluDersList = db.YazOkuluDersler.Where(x => x.FKBirimID == fkBirimID && x.Yil == yil).ToList();

                List<KoordinatorDersListesiVM> dersList = new List<KoordinatorDersListesiVM>();

                foreach (var dersPlan in birimDersList)
                {
                    if (dersPlan.BolKodAd == "TIP" && dersPlan.Yariyil == 0) { continue; }
                    KoordinatorDersListesiVM yeniDers = new KoordinatorDersListesiVM();
                    var personel = koordinatorList.Any(y => y.FKDersPlanAnaID == dersPlan.FKDersPlanAnaID && !y.Silindi) ? koordinatorList.FirstOrDefault(y => y.FKDersPlanAnaID == dersPlan.FKDersPlanAnaID && !y.Silindi).PersonelBilgisi : new PersonelBilgisi();
                    var ders = dersPlan.OGRDersPlanAna;

                    yeniDers.ID = ders.ID;
                    yeniDers.FKPersonelID = personel.ID;
                    yeniDers.PersonelAdSoyadUnvan = personel.UnvanAd + " " + personel.Ad + " " + personel.Soyad;
                    yeniDers.FKDersBilgiID = koordinatorList.Any(y => y.FKDersPlanAnaID == ders.ID) ? koordinatorList.FirstOrDefault(y => y.FKDersPlanAnaID == ders.ID).FKDersBilgiID : 0;
                    yeniDers.FKDersPlanAnaID = ders.ID;
                    yeniDers.FKDersPlanID = ders.ID;
                    yeniDers.DersAdi = ders.DersAd;
                    yeniDers.Ekleyen = "";
                    yeniDers.Zaman = dersPlan.Zaman;
                    yeniDers.Yariyil = dersPlan.Yariyil;
                    yeniDers.YazOkuluDersiMi = yazOkuluDersList.Any(y => y.FKDersPlanAnaID == ders.ID);
                    yeniDers.EnumDersBirimTipi = dersPlan.EnumDersBirimTipi;
                    yeniDers.EnumDersSecimTipi = dersPlan.EnumDersSecimTipi;
                    yeniDers.Yil = dersPlan.Yil;
                    yeniDers.Donem = dersPlan.EnumDonem;

                    dersList.Add(yeniDers);
                }

                var birimIDList = birimDersList.Select(x => x.FKDersPlanAnaID).Distinct().ToList();

                if (yetkiFiltre)
                {
                    var yetkiliFakulteList = FAKULTEMAIN.GetirFakulteYetkiListesi(personelID.Value);
                    int? fakulteID = Sabis.Client.BirimIslem.Instance.getirBagliFakulteID(fkBirimID);
                    if (fakulteID.HasValue && !yetkiliFakulteList.Any(x => x == fakulteID))
                    {
                        dersList = dersList.Where(x => x.EnumDersBirimTipi == (int)Sabis.Enum.EnumDersBirimTipi.BOLUM).ToList();
                    }
                }

                return dersList;
            }
            


            //int fkBolumID = FKBirimID.Value;
            //using (UYSv2Entities db = new UYSv2Entities())
            //{
            //    var liste = new List<int?>();

            //    //if (personelID.HasValue && personelID == 1908) // ahmet zengin
            //    //{
            //    //    liste = db.OGRDersPlan.Where(x => (x.Yil == yil && x.FKBirimID == FKBirimID && x.OGRDersPlanAna.EnumDersAktif == (int)Sabis.Enum.EnumDersAktif.AKTIF)).Select(x => x.FKDersPlanAnaID).Distinct().ToList();
            //    //}
            //    //else
            //    //{
            //    //    liste = db.OGRDersPlan.Where(x => (x.Yil == yil && x.FKBirimID == FKBirimID && x.OGRDersPlanAna.EnumDersAktif == (int)Sabis.Enum.EnumDersAktif.AKTIF) && x.EnumDersBirimTipi != (int)Sabis.Enum.EnumDersBirimTipi.UNIVERSITE && x.OGRDersPlanAna.EnumDersTipi != (int)Sabis.Enum.EnumDersTipi.UNIVERSITE_DERSI && x.EnumDersBirimTipi != (int)Sabis.Enum.EnumDersBirimTipi.FAKULTE && x.OGRDersPlanAna.EnumDersTipi != (int)Sabis.Enum.EnumDersTipi.FAKULTE_DERSI).Where(x => x.DersKod != "500").Select(x => x.FKDersPlanAnaID).Distinct().ToList();
            //    //}

            //    liste = db.OGRDersPlan.Where(x => (x.Yil == yil & x.FKBirimID == FKBirimID && x.OGRDersPlanAna.EnumDersAktif == (int)Sabis.Enum.EnumDersAktif.AKTIF)).Select(x => x.FKDersPlanAnaID).Distinct().ToList();

            //    List<KoordinatorDersListesiVM> koordinatorListe = new List<KoordinatorDersListesiVM>();



            //    foreach (var item in liste)
            //    {
            //        KoordinatorDersListesiVM yeniKoordinator = new KoordinatorDersListesiVM();

            //        var dpAna = db.OGRDersPlanAna.FirstOrDefault(x => x.ID == item.Value);
            //        var dPlan = db.OGRDersPlan.FirstOrDefault(x=> x.FKDersPlanAnaID == item.Value &&  x.FKBirimID == FKBirimID);

            //        yeniKoordinator.FKDersPlanAnaID = item.Value;
            //        yeniKoordinator.DersAdi = dpAna.DersAd;
            //        yeniKoordinator.EnumDersBirimTipi = dPlan.EnumDersBirimTipi;
            //        yeniKoordinator.EnumDersSecimTipi = dPlan.EnumDersSecimTipi;
            //        yeniKoordinator.FKDersPlanID = dPlan.ID;
            //        yeniKoordinator.Donem = dPlan.EnumDonem;
            //        yeniKoordinator.Yil = dPlan.Yil;

            //        if (FKProgramID.HasValue)
            //        {
            //            yeniKoordinator.YazOkuluDersiMi = db.YazOkuluDersler.Any(x => x.FKDersPlanAnaID == item.Value && x.FKBirimID == fkBolumID && (x.FKProgramID == FKProgramID || x.FKProgramID == null) && x.Yil == yil);
            //        }
            //        else
            //        {
            //            yeniKoordinator.YazOkuluDersiMi = db.YazOkuluDersler.Any(x => x.FKDersPlanAnaID == item.Value && (x.FKBirimID == fkBolumID || x.FKProgramID == fkBolumID) && x.Yil == yil);
            //        }

            //        if (db.OGRDersPlan.FirstOrDefault(x => x.FKDersPlanAnaID == item.Value && x.Yil == yil && x.FKBirimID == FKBirimID).Yariyil != null)
            //        {
            //            yeniKoordinator.Yariyil = db.OGRDersPlan.FirstOrDefault(x => x.FKDersPlanAnaID == item.Value && x.Yil == yil && x.FKBirimID == FKBirimID).Yariyil.Value;
            //        }


            //        var koordinatorBilgi = db.DersKoordinator.Where(x => x.FKDersPlanAnaID == item).OrderByDescending(x=> x.ID).FirstOrDefault();


            //        if (koordinatorBilgi != null)
            //        {
            //            yeniKoordinator.ID = koordinatorBilgi.ID;
            //            yeniKoordinator.FKPersonelID = koordinatorBilgi.FKPersonelID;
            //            if (koordinatorBilgi.PersonelBilgisi.Ad != null)
            //            {
            //                yeniKoordinator.PersonelAdSoyadUnvan = koordinatorBilgi.PersonelBilgisi.UnvanAd + " " + koordinatorBilgi.PersonelBilgisi.Ad + " " + koordinatorBilgi.PersonelBilgisi.Soyad;
            //            }
            //            else
            //            {
            //                yeniKoordinator.PersonelAdSoyadUnvan = "";
            //            }

            //            yeniKoordinator.FKDersBilgiID = koordinatorBilgi.FKDersBilgiID;
            //            //yeniKoordinator.FKDersPlanAnaID = koordinatorBilgi.FKDersPlanAnaID;
            //            //yeniKoordinator.DersAdi = koordinatorBilgi.OGRDersPlanAna.DersAd;
            //            yeniKoordinator.Ekleyen = koordinatorBilgi.Ekleyen;
            //            yeniKoordinator.Zaman = koordinatorBilgi.Zaman;
            //            //yeniKoordinator.Yariyil = koordinatorBilgi
            //        }

            //        koordinatorListe.Add(yeniKoordinator);
            //        var test = koordinatorListe.ToList();
            //    }
            //    return koordinatorListe.OrderBy(x=> x.Yariyil).ToList();
            //}
        }

        public static List<KoordinatorDersListesiVM> GetirBolumDersListesiDonemBazli(int? FKBirimID, int yil, int EnumDonem, int? personelID, int? FKProgramID)
        {
            int fkBolumID = FKBirimID.Value;
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var liste = new List<int?>();

                //liste = db.OGRDersPlan.Where(x => (x.Yil == yil & x.EnumDonem == EnumDonem & x.FKBirimID == FKBirimID && x.OGRDersPlanAna.EnumDersAktif == (int)Sabis.Enum.EnumDersAktif.AKTIF)).Select(x => x.FKDersPlanAnaID).Distinct().ToList();
                liste = db.OGRDersPlan.Where(x => (x.Yil == yil & x.FKBirimID == FKBirimID && x.OGRDersPlanAna.EnumDersAktif == (int)Sabis.Enum.EnumDersAktif.AKTIF)).Select(x => x.FKDersPlanAnaID).Distinct().ToList();
                

                List<KoordinatorDersListesiVM> koordinatorListe = new List<KoordinatorDersListesiVM>();

                foreach (var item in liste)
                {
                    KoordinatorDersListesiVM yeniKoordinator = new KoordinatorDersListesiVM();

                    var dpAna = db.OGRDersPlanAna.FirstOrDefault(x => x.ID == item.Value);
                    var dPlan = db.OGRDersPlan.FirstOrDefault(x => x.FKDersPlanAnaID == item.Value && x.FKBirimID == FKBirimID);


                    yeniKoordinator.FKDersPlanAnaID = item.Value;
                    yeniKoordinator.DersAdi = dpAna.DersAd;
                    yeniKoordinator.EnumDersBirimTipi = dPlan.EnumDersBirimTipi;
                    yeniKoordinator.EnumDersSecimTipi = dPlan.EnumDersSecimTipi;
                    yeniKoordinator.FKDersPlanID = dPlan.FKDersPlanID;
                    yeniKoordinator.Donem = dPlan.EnumDonem;
                    yeniKoordinator.Yil = dPlan.Yil;
                    yeniKoordinator.GrupSayisi = db.OGRDersGrup.Where(x => 
                        x.FKDersPlanAnaID == dpAna.ID && 
                        x.OgretimYili == yil && 
                        x.EnumDonem == EnumDonem && 
                        x.Acik == true && 
                        x.Silindi == false
                        ).ToList().Count();

                    if (FKProgramID.HasValue)
                    {
                        yeniKoordinator.YazOkuluDersiMi = db.YazOkuluDersler.Any(x => x.FKDersPlanAnaID == item.Value && x.FKBirimID == fkBolumID && (x.FKProgramID == FKProgramID || x.FKProgramID == null) && x.Yil == yil);
                    }
                    else
                    {
                        yeniKoordinator.YazOkuluDersiMi = db.YazOkuluDersler.Any(x => x.FKDersPlanAnaID == item.Value && (x.FKBirimID == fkBolumID || x.FKProgramID == fkBolumID) && x.Yil == yil);
                    }

                    if (db.OGRDersPlan.FirstOrDefault(x => x.FKDersPlanAnaID == item.Value && x.Yil == yil && x.FKBirimID == FKBirimID).Yariyil != null)
                    {
                        yeniKoordinator.Yariyil = db.OGRDersPlan.FirstOrDefault(x => x.FKDersPlanAnaID == item.Value && x.Yil == yil && x.FKBirimID == FKBirimID).Yariyil.Value;
                    }


                    var koordinatorBilgi = db.DersKoordinator.Where(x => x.FKDersPlanAnaID == item).OrderByDescending(x => x.ID).FirstOrDefault();


                    if (koordinatorBilgi != null)
                    {
                        yeniKoordinator.ID = koordinatorBilgi.ID;
                        yeniKoordinator.FKPersonelID = koordinatorBilgi.FKPersonelID;
                        if (koordinatorBilgi.PersonelBilgisi.Ad != null)
                        {
                            yeniKoordinator.PersonelAdSoyadUnvan = koordinatorBilgi.PersonelBilgisi.UnvanAd + " " + koordinatorBilgi.PersonelBilgisi.Ad + " " + koordinatorBilgi.PersonelBilgisi.Soyad;
                        }
                        else
                        {
                            yeniKoordinator.PersonelAdSoyadUnvan = "";
                        }

                        yeniKoordinator.FKDersBilgiID = koordinatorBilgi.FKDersBilgiID;
                        //yeniKoordinator.FKDersPlanAnaID = koordinatorBilgi.FKDersPlanAnaID;
                        //yeniKoordinator.DersAdi = koordinatorBilgi.OGRDersPlanAna.DersAd;
                        yeniKoordinator.Ekleyen = koordinatorBilgi.Ekleyen;
                        yeniKoordinator.Zaman = koordinatorBilgi.Zaman;
                        //yeniKoordinator.Yariyil = koordinatorBilgi
                    }

                    koordinatorListe.Add(yeniKoordinator);
                }
                var test = koordinatorListe.ToList();
                return koordinatorListe.OrderBy(x => x.Yariyil).ToList();
                //return koordinatorListe.Where(x => x.Donem == 2).OrderBy(x => x.Yariyil).ToList();
            }
        }
        public static GeriBildirimVM GeriBildirimDetay(int FKDersPlanID, int m)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var sorgu = (from dp in db.OGRDersPlan
                             where
                             dp.ID == FKDersPlanID
                             select new GeriBildirimVM
                             {
                                 FKDersPlanID = dp.ID,
                                 FKBirimID = dp.Birimler.ID,
                                 DersAd = dp.OGRDersPlanAna.DersAd,
                                 BirimAd = dp.Birimler.BirimAdi,
                                 ModulTip = m
                             }).FirstOrDefault();
                return sorgu;
            }
        }
        public static List<KoordinatorDersListesiVM> DersListesiGetir(int? FKBirimID, int yil, int EnumDonem, int? personelID, int? FKProgramID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var liste = new List<int?>();
                liste = db.OGRDersPlan.Where(x => (x.Yil == yil & x.EnumDonem == EnumDonem && x.FKBirimID == FKBirimID && x.OGRDersPlanAna.EnumDersAktif == (int)Sabis.Enum.EnumDersAktif.AKTIF)).Select(x => x.FKDersPlanAnaID).Distinct().ToList();
                List<KoordinatorDersListesiVM> koordinatorListe = new List<KoordinatorDersListesiVM>();
                foreach (var item in liste)
                {
                    KoordinatorDersListesiVM yeniKoordinator = new KoordinatorDersListesiVM();

                    var dpAna = db.OGRDersPlanAna.FirstOrDefault(x => x.ID == item.Value);
                    var dPlan = db.OGRDersPlan.FirstOrDefault(x => x.FKDersPlanAnaID == item.Value && x.FKBirimID == FKBirimID);


                    yeniKoordinator.FKDersPlanAnaID = item.Value;
                    yeniKoordinator.DersAdi = dpAna.DersAd;
                    yeniKoordinator.EnumDersBirimTipi = dPlan.EnumDersBirimTipi;
                    yeniKoordinator.EnumDersSecimTipi = dPlan.EnumDersSecimTipi;
                    yeniKoordinator.FKDersPlanID = dPlan.ID;
                    yeniKoordinator.Donem = dPlan.EnumDonem;
                    yeniKoordinator.Yil = dPlan.Yil;
                    yeniKoordinator.GrupSayisi = db.OGRDersGrup.Where(x =>
                        x.FKDersPlanAnaID == dpAna.ID &&
                        x.OgretimYili == yil &&
                        x.EnumDonem == EnumDonem &&
                        x.Acik == true &&
                        x.Silindi == false
                        ).ToList().Count();

                    //var sinavgb = (from dp in db.OGRDersPlan
                    //               join sg in db.OSinavGrup on dp.ID equals sg.FKDersPlanID
                    //               join gb in db.ABSGeriBildirim on sg.FKSinavID equals gb.AktiviteID
                    //               where
                    //               dp.ID == yeniKoordinator.FKDersPlanID &&
                    //               gb.EnumModulTipi == 0
                    //               select new
                    //               {
                    //                   ANADERSID = dp.OGRDersPlanAna.ID,
                    //                   DERSID = dp.ID,
                    //                   gb.Baslik,
                    //                   gb.EnumGeriBildirimDurum
                    //               }).Distinct();
                    var sinavgb = (from yazilma in db.OGROgrenciYazilma
                                   join sg in db.OSinavGrup on yazilma.FKDersGrupID equals sg.FKDersGrupID
                                   join gb in db.ABSGeriBildirim on new { SINAVID = sg.OSinav.ID, OGRID = yazilma.FKOgrenciID.Value } equals new { SINAVID = gb.AktiviteID, OGRID = gb.OgrenciID }
                                   where
                                    yazilma.FKDersPlanID == yeniKoordinator.FKDersPlanID &&
                                    yazilma.Yil == yil &&
                                    yazilma.EnumDonem == EnumDonem &&
                                    yazilma.Iptal == false &&
                                    yazilma.OGRKimlik.EnumOgrDurum == 2 &&
                                    gb.EnumModulTipi == 0
                                   select new GeriBildirimSinav
                                   {
                                       SinavID = sg.FKSinavID,
                                       FKDersGrupID = yazilma.FKDersGrupID.Value,
                                       SinavAd = sg.OSinav.Ad,
                                       GrupAd = yazilma.OGRDersGrup.EnumOgretimTur.ToString() + " - " + yazilma.OGRDersGrup.GrupAd,
                                       HocaAd = yazilma.OGRDersGrup.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.Ad + " " + yazilma.OGRDersGrup.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.Soyad + " (" + yazilma.OGRDersGrup.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.KullaniciAdi + ")",
                                       BildirimBaslik = gb.Baslik,
                                       BildirimIcerik = gb.Icerik,
                                       BildirimTarih = gb.TarihKayit,
                                       BildirimDurum = gb.EnumGeriBildirimDurum,
                                       FKOgrenciID = gb.OgrenciID,
                                       OgrenciAd = yazilma.OGRKimlik.Kisi.Ad,
                                       OgrenciSoyad = yazilma.OGRKimlik.Kisi.Soyad,
                                       OgrenciNumara = yazilma.OGRKimlik.Numara
                                   }).OrderBy(x => x.BildirimDurum).ThenByDescending(x => x.BildirimTarih).ToList();

                    //var odevgb = (from dp in db.OGRDersPlan
                    //              join gr in db.OGRDersGrup on dp.ID equals gr.FKDersPlanID
                    //              join sg in db.OOdevGrup on gr.ID equals sg.FKOGRDersGrupID
                    //              join gb in db.ABSGeriBildirim on sg.FKOOdevID equals gb.AktiviteID
                    //              where
                    //              dp.ID == yeniKoordinator.FKDersPlanID &&
                    //              gb.EnumModulTipi == 1
                    //              select new
                    //              {
                    //                  ANADERSID = dp.OGRDersPlanAna.ID,
                    //                  DERSID = dp.ID,
                    //                  gb.Baslik,
                    //                  gb.EnumGeriBildirimDurum
                    //              }).Distinct();
                    var odevgb = (from yazilma in db.OGROgrenciYazilma
                                 join sg in db.OOdevGrup on yazilma.FKDersGrupID equals sg.FKOGRDersGrupID
                                 join gb in db.ABSGeriBildirim on new { SINAVID = sg.OOdev.ID, OGRID = yazilma.FKOgrenciID.Value } equals new { SINAVID = gb.AktiviteID, OGRID = gb.OgrenciID }
                                 where
                                  yazilma.FKDersPlanID == yeniKoordinator.FKDersPlanID &&
                                  yazilma.Yil == yil &&
                                  yazilma.EnumDonem == EnumDonem &&
                                  yazilma.Iptal == false &&
                                  yazilma.OGRKimlik.EnumOgrDurum == 2 &&
                                  gb.EnumModulTipi == 1
                                 select new GeriBildirimOdev
                                 {
                                     OdevID = sg.FKOOdevID,
                                     FKDersGrupID = yazilma.FKDersGrupID.Value,
                                     OdevAd = sg.OOdev.Ad,
                                     OdevAciklama = sg.OOdev.Aciklama,
                                     GrupAd = yazilma.OGRDersGrup.EnumOgretimTur.ToString() + " - " + yazilma.OGRDersGrup.GrupAd,
                                     HocaAd = yazilma.OGRDersGrup.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.Ad + " " + yazilma.OGRDersGrup.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.Soyad + " (" + yazilma.OGRDersGrup.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.KullaniciAdi + ")",
                                     BildirimBaslik = gb.Baslik,
                                     BildirimIcerik = gb.Icerik,
                                     BildirimTarih = gb.TarihKayit,
                                     BildirimDurum = gb.EnumGeriBildirimDurum,
                                     FKOgrenciID = gb.OgrenciID,
                                     OgrenciAd = yazilma.OGRKimlik.Kisi.Ad,
                                     OgrenciSoyad = yazilma.OGRKimlik.Kisi.Soyad,
                                     OgrenciNumara = yazilma.OGRKimlik.Numara
                                 }).OrderBy(x => x.BildirimDurum).ThenByDescending(x => x.BildirimTarih).ToList();

                    var sanalsinifgb = (from yazilma in db.OGROgrenciYazilma
                                 join sg in db.SanalSinifGrup on yazilma.FKDersGrupID equals sg.FKDersGrupID
                                 join gb in db.ABSGeriBildirim on new { SSINIFID = sg.SanalSinif.ID, OGRID = yazilma.FKOgrenciID.Value } equals new { SSINIFID = gb.AktiviteID, OGRID = gb.OgrenciID }
                                 where
                                  yazilma.FKDersPlanID == yeniKoordinator.FKDersPlanID &&
                                  //yazilma.FKDersPlanID == 422711 &&
                                  yazilma.Yil == yil &&
                                  yazilma.EnumDonem == EnumDonem &&
                                  yazilma.Iptal == false &&
                                  yazilma.OGRKimlik.EnumOgrDurum == 2 &&
                                  gb.EnumModulTipi == 2
                                 select new GeriBildirimSanalSinif
                                 {
                                     SanalSinifID = sg.SSinifID,
                                     FKDersGrupID = yazilma.FKDersGrupID.Value,
                                     SanalSinifAd = sg.SanalSinif.Ad,
                                     GrupAd = yazilma.OGRDersGrup.EnumOgretimTur.ToString() + " - " + yazilma.OGRDersGrup.GrupAd,
                                     HocaAd = yazilma.OGRDersGrup.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.Ad + " " + yazilma.OGRDersGrup.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.Soyad + " (" + yazilma.OGRDersGrup.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.KullaniciAdi + ")",
                                     BildirimBaslik = gb.Baslik,
                                     BildirimIcerik = gb.Icerik,
                                     BildirimTarih = gb.TarihKayit,
                                     BildirimDurum = gb.EnumGeriBildirimDurum,
                                     FKOgrenciID = gb.OgrenciID,
                                     OgrenciAd = yazilma.OGRKimlik.Kisi.Ad,
                                     OgrenciSoyad = yazilma.OGRKimlik.Kisi.Soyad,
                                     OgrenciNumara = yazilma.OGRKimlik.Numara
                                 }).OrderBy(x => x.BildirimDurum).ThenByDescending(x => x.BildirimTarih).ToList();

                    yeniKoordinator.YeniGeriBildirimSayisi = sinavgb.Where(x => x.BildirimDurum == 0).ToList().Count + odevgb.Where(x => x.BildirimDurum == 0).ToList().Count + sanalsinifgb.Where(x => x.BildirimDurum == 0).ToList().Count;
                    yeniKoordinator.CevaplananGeriBildirimSayisi = sinavgb.Where(x => x.BildirimDurum == 2).ToList().Count + odevgb.Where(x => x.BildirimDurum == 2).ToList().Count + sanalsinifgb.Where(x => x.BildirimDurum == 2).ToList().Count;

                    yeniKoordinator.Sinav_YeniBildirimSayi = sinavgb.Where(x => x.BildirimDurum == 0).ToList().Count;
                    yeniKoordinator.Sinav_CevaplananBildirimSayi = sinavgb.Where(x => x.BildirimDurum == 2).ToList().Count;

                    yeniKoordinator.Odev_YeniBildirimSayi = odevgb.Where(x => x.BildirimDurum == 0).ToList().Count;
                    yeniKoordinator.Odev_CevaplananBildirimSayi = odevgb.Where(x => x.BildirimDurum == 2).ToList().Count;

                    yeniKoordinator.SanalSinif_YeniBildirimSayi = sanalsinifgb.Where(x => x.BildirimDurum == 0).ToList().Count;
                    yeniKoordinator.SanalSinif_CevaplananBildirimSayi = sanalsinifgb.Where(x => x.BildirimDurum == 2).ToList().Count;

                    var koordinatorBilgi = db.DersKoordinator.Where(x => x.FKDersPlanAnaID == item).OrderByDescending(x => x.ID).FirstOrDefault();
                    if (koordinatorBilgi != null)
                    {
                        yeniKoordinator.ID = koordinatorBilgi.ID;
                        yeniKoordinator.FKPersonelID = koordinatorBilgi.FKPersonelID;
                        if (koordinatorBilgi.PersonelBilgisi.Ad != null)
                        {
                            yeniKoordinator.PersonelAdSoyadUnvan = koordinatorBilgi.PersonelBilgisi.UnvanAd + " " + koordinatorBilgi.PersonelBilgisi.Ad + " " + koordinatorBilgi.PersonelBilgisi.Soyad;
                        }
                        else
                        {
                            yeniKoordinator.PersonelAdSoyadUnvan = "";
                        }

                        yeniKoordinator.FKDersBilgiID = koordinatorBilgi.FKDersBilgiID;
                        //yeniKoordinator.FKDersPlanAnaID = koordinatorBilgi.FKDersPlanAnaID;
                        //yeniKoordinator.DersAdi = koordinatorBilgi.OGRDersPlanAna.DersAd;
                        yeniKoordinator.Ekleyen = koordinatorBilgi.Ekleyen;
                        yeniKoordinator.Zaman = koordinatorBilgi.Zaman;
                        //yeniKoordinator.Yariyil = koordinatorBilgi
                    }

                    koordinatorListe.Add(yeniKoordinator);
                }
                return koordinatorListe.OrderBy(x => x.DersAdi).ToList();
            }
        }
        public static List<KoordinatorDersListesiVM> DersListesiGetir1(int? FKBirimID, int yil, int EnumDonem, int? personelID, int? FKProgramID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var liste = new List<int?>();
                liste = db.OGRDersGrup.Where(x => (
                    x.OgretimYili == yil && 
                    x.EnumDonem == EnumDonem && 
                    x.FKBirimID == FKBirimID && 
                    x.OGRDersPlanAna.EnumDersAktif == (int)Sabis.Enum.EnumDersAktif.AKTIF)
                    ).Select(x => x.FKDersPlanAnaID).Distinct().ToList();

                List<KoordinatorDersListesiVM> koordinatorListe = new List<KoordinatorDersListesiVM>();
                foreach (var item in liste)
                {
                    KoordinatorDersListesiVM yeniKoordinator = new KoordinatorDersListesiVM();

                    var dpAna = db.OGRDersPlanAna.FirstOrDefault(x => x.ID == item.Value);
                    var dPlan = db.OGRDersGrup.FirstOrDefault(x => x.FKDersPlanAnaID == item.Value && x.FKBirimID == FKBirimID && x.OgretimYili == yil && x.EnumDonem == EnumDonem && x.Acik == true && x.Silindi == false).OGRDersPlan;
                    //var dPlan = db.OGRDersPlan.FirstOrDefault(x => x.FKDersPlanAnaID == item.Value && x.FKBirimID == FKBirimID);


                    yeniKoordinator.FKDersPlanAnaID = item.Value;
                    yeniKoordinator.DersAdi = dpAna.DersAd;
                    yeniKoordinator.EnumDersBirimTipi = dPlan.EnumDersBirimTipi;
                    yeniKoordinator.EnumDersSecimTipi = dPlan.EnumDersSecimTipi;
                    yeniKoordinator.FKDersPlanID = dPlan.ID;
                    yeniKoordinator.Donem = dPlan.EnumDonem;
                    yeniKoordinator.Yil = dPlan.Yil;
                    yeniKoordinator.GrupSayisi = db.OGRDersGrup.Where(x =>
                        x.FKDersPlanAnaID == dpAna.ID &&
                        x.OgretimYili == yil &&
                        x.EnumDonem == EnumDonem &&
                        x.Acik == true &&
                        x.Silindi == false
                        ).ToList().Count();

                    var sinavgb = (from yazilma in db.OGROgrenciYazilma
                                   join sg in db.OSinavGrup on yazilma.FKDersGrupID equals sg.FKDersGrupID
                                   join gb in db.ABSGeriBildirim on new { SINAVID = sg.OSinav.ID, OGRID = yazilma.FKOgrenciID.Value } equals new { SINAVID = gb.AktiviteID, OGRID = gb.OgrenciID }
                                   where
                                    yazilma.FKDersPlanID == yeniKoordinator.FKDersPlanID &&
                                    yazilma.Yil == yil &&
                                    yazilma.EnumDonem == EnumDonem &&
                                    yazilma.Iptal == false &&
                                    yazilma.OGRKimlik.EnumOgrDurum == 2 &&
                                    gb.EnumModulTipi == 0
                                   select new GeriBildirimSinav
                                   {
                                       SinavID = sg.FKSinavID,
                                       FKDersGrupID = yazilma.FKDersGrupID.Value,
                                       SinavAd = sg.OSinav.Ad,
                                       GrupAd = yazilma.OGRDersGrup.EnumOgretimTur.ToString() + " - " + yazilma.OGRDersGrup.GrupAd,
                                       HocaAd = yazilma.OGRDersGrup.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.Ad + " " + yazilma.OGRDersGrup.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.Soyad + " (" + yazilma.OGRDersGrup.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.KullaniciAdi + ")",
                                       BildirimBaslik = gb.Baslik,
                                       BildirimIcerik = gb.Icerik,
                                       BildirimTarih = gb.TarihKayit,
                                       BildirimDurum = gb.EnumGeriBildirimDurum,
                                       FKOgrenciID = gb.OgrenciID,
                                       OgrenciAd = yazilma.OGRKimlik.Kisi.Ad,
                                       OgrenciSoyad = yazilma.OGRKimlik.Kisi.Soyad,
                                       OgrenciNumara = yazilma.OGRKimlik.Numara
                                   }).OrderBy(x => x.BildirimDurum).ThenByDescending(x => x.BildirimTarih).ToList();

                    //var odevgb = (from dp in db.OGRDersPlan
                    //              join gr in db.OGRDersGrup on dp.ID equals gr.FKDersPlanID
                    //              join sg in db.OOdevGrup on gr.ID equals sg.FKOGRDersGrupID
                    //              join gb in db.ABSGeriBildirim on sg.FKOOdevID equals gb.AktiviteID
                    //              where
                    //              dp.ID == yeniKoordinator.FKDersPlanID &&
                    //              gb.EnumModulTipi == 1
                    //              select new
                    //              {
                    //                  ANADERSID = dp.OGRDersPlanAna.ID,
                    //                  DERSID = dp.ID,
                    //                  gb.Baslik,
                    //                  gb.EnumGeriBildirimDurum
                    //              }).Distinct();
                    var odevgb = (from yazilma in db.OGROgrenciYazilma
                                  join sg in db.OOdevGrup on yazilma.FKDersGrupID equals sg.FKOGRDersGrupID
                                  join gb in db.ABSGeriBildirim on new { SINAVID = sg.OOdev.ID, OGRID = yazilma.FKOgrenciID.Value } equals new { SINAVID = gb.AktiviteID, OGRID = gb.OgrenciID }
                                  where
                                   yazilma.FKDersPlanID == yeniKoordinator.FKDersPlanID &&
                                   yazilma.Yil == yil &&
                                   yazilma.EnumDonem == EnumDonem &&
                                   yazilma.Iptal == false &&
                                   yazilma.OGRKimlik.EnumOgrDurum == 2 &&
                                   gb.EnumModulTipi == 1
                                  select new GeriBildirimOdev
                                  {
                                      OdevID = sg.FKOOdevID,
                                      FKDersGrupID = yazilma.FKDersGrupID.Value,
                                      OdevAd = sg.OOdev.Ad,
                                      OdevAciklama = sg.OOdev.Aciklama,
                                      GrupAd = yazilma.OGRDersGrup.EnumOgretimTur.ToString() + " - " + yazilma.OGRDersGrup.GrupAd,
                                      HocaAd = yazilma.OGRDersGrup.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.Ad + " " + yazilma.OGRDersGrup.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.Soyad + " (" + yazilma.OGRDersGrup.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.KullaniciAdi + ")",
                                      BildirimBaslik = gb.Baslik,
                                      BildirimIcerik = gb.Icerik,
                                      BildirimTarih = gb.TarihKayit,
                                      BildirimDurum = gb.EnumGeriBildirimDurum,
                                      FKOgrenciID = gb.OgrenciID,
                                      OgrenciAd = yazilma.OGRKimlik.Kisi.Ad,
                                      OgrenciSoyad = yazilma.OGRKimlik.Kisi.Soyad,
                                      OgrenciNumara = yazilma.OGRKimlik.Numara
                                  }).OrderBy(x => x.BildirimDurum).ThenByDescending(x => x.BildirimTarih).ToList();

                    var sanalsinifgb = (from yazilma in db.OGROgrenciYazilma
                                        join sg in db.SanalSinifGrup on yazilma.FKDersGrupID equals sg.FKDersGrupID
                                        join gb in db.ABSGeriBildirim on new { SSINIFID = sg.SanalSinif.ID, OGRID = yazilma.FKOgrenciID.Value } equals new { SSINIFID = gb.AktiviteID, OGRID = gb.OgrenciID }
                                        where
                                         yazilma.FKDersPlanID == yeniKoordinator.FKDersPlanID &&
                                         //yazilma.FKDersPlanID == 422711 &&
                                         yazilma.Yil == yil &&
                                         yazilma.EnumDonem == EnumDonem &&
                                         yazilma.Iptal == false &&
                                         yazilma.OGRKimlik.EnumOgrDurum == 2 &&
                                         gb.EnumModulTipi == 2
                                        select new GeriBildirimSanalSinif
                                        {
                                            SanalSinifID = sg.SSinifID,
                                            FKDersGrupID = yazilma.FKDersGrupID.Value,
                                            SanalSinifAd = sg.SanalSinif.Ad,
                                            GrupAd = yazilma.OGRDersGrup.EnumOgretimTur.ToString() + " - " + yazilma.OGRDersGrup.GrupAd,
                                            HocaAd = yazilma.OGRDersGrup.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.Ad + " " + yazilma.OGRDersGrup.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.Soyad + " (" + yazilma.OGRDersGrup.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.KullaniciAdi + ")",
                                            BildirimBaslik = gb.Baslik,
                                            BildirimIcerik = gb.Icerik,
                                            BildirimTarih = gb.TarihKayit,
                                            BildirimDurum = gb.EnumGeriBildirimDurum,
                                            FKOgrenciID = gb.OgrenciID,
                                            OgrenciAd = yazilma.OGRKimlik.Kisi.Ad,
                                            OgrenciSoyad = yazilma.OGRKimlik.Kisi.Soyad,
                                            OgrenciNumara = yazilma.OGRKimlik.Numara
                                        }).OrderBy(x => x.BildirimDurum).ThenByDescending(x => x.BildirimTarih).ToList();

                    yeniKoordinator.YeniGeriBildirimSayisi = sinavgb.Where(x => x.BildirimDurum == 0).ToList().Count + odevgb.Where(x => x.BildirimDurum == 0).ToList().Count + sanalsinifgb.Where(x => x.BildirimDurum == 0).ToList().Count;
                    yeniKoordinator.CevaplananGeriBildirimSayisi = sinavgb.Where(x => x.BildirimDurum == 2).ToList().Count + odevgb.Where(x => x.BildirimDurum == 2).ToList().Count + sanalsinifgb.Where(x => x.BildirimDurum == 2).ToList().Count;

                    yeniKoordinator.Sinav_YeniBildirimSayi = sinavgb.Where(x => x.BildirimDurum == 0).ToList().Count;
                    yeniKoordinator.Sinav_CevaplananBildirimSayi = sinavgb.Where(x => x.BildirimDurum == 2).ToList().Count;

                    yeniKoordinator.Odev_YeniBildirimSayi = odevgb.Where(x => x.BildirimDurum == 0).ToList().Count;
                    yeniKoordinator.Odev_CevaplananBildirimSayi = odevgb.Where(x => x.BildirimDurum == 2).ToList().Count;

                    yeniKoordinator.SanalSinif_YeniBildirimSayi = sanalsinifgb.Where(x => x.BildirimDurum == 0).ToList().Count;
                    yeniKoordinator.SanalSinif_CevaplananBildirimSayi = sanalsinifgb.Where(x => x.BildirimDurum == 2).ToList().Count;

                    var koordinatorBilgi = db.DersKoordinator.Where(x => x.FKDersPlanAnaID == item).OrderByDescending(x => x.ID).FirstOrDefault();
                    if (koordinatorBilgi != null)
                    {
                        yeniKoordinator.ID = koordinatorBilgi.ID;
                        yeniKoordinator.FKPersonelID = koordinatorBilgi.FKPersonelID;
                        if (koordinatorBilgi.PersonelBilgisi.Ad != null)
                        {
                            yeniKoordinator.PersonelAdSoyadUnvan = koordinatorBilgi.PersonelBilgisi.UnvanAd + " " + koordinatorBilgi.PersonelBilgisi.Ad + " " + koordinatorBilgi.PersonelBilgisi.Soyad;
                        }
                        else
                        {
                            yeniKoordinator.PersonelAdSoyadUnvan = "";
                        }

                        yeniKoordinator.FKDersBilgiID = koordinatorBilgi.FKDersBilgiID;
                        //yeniKoordinator.FKDersPlanAnaID = koordinatorBilgi.FKDersPlanAnaID;
                        //yeniKoordinator.DersAdi = koordinatorBilgi.OGRDersPlanAna.DersAd;
                        yeniKoordinator.Ekleyen = koordinatorBilgi.Ekleyen;
                        yeniKoordinator.Zaman = koordinatorBilgi.Zaman;
                        //yeniKoordinator.Yariyil = koordinatorBilgi
                    }

                    koordinatorListe.Add(yeniKoordinator);
                }
                return koordinatorListe.OrderBy(x => x.DersAdi).ToList();
            }
        }
        public static List<GeriBildirimSinav> _SinavGeriBildirimleri(int FKDersPlanID, int yil, int EnumDonem)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var sorgu = (from yazilma in db.OGROgrenciYazilma
                             join sg in db.OSinavGrup on yazilma.FKDersGrupID equals sg.FKDersGrupID
                             join gb in db.ABSGeriBildirim on new { SINAVID = sg.OSinav.ID, OGRID = yazilma.FKOgrenciID.Value } equals new { SINAVID = gb.AktiviteID, OGRID = gb.OgrenciID }
                             where
                              yazilma.FKDersPlanID == FKDersPlanID &&
                              yazilma.Yil == yil &&
                              yazilma.EnumDonem == EnumDonem &&
                              yazilma.Iptal == false &&
                              yazilma.OGRKimlik.EnumOgrDurum == 2 &&
                              gb.EnumModulTipi == 0
                             select new GeriBildirimSinav
                             {
                                 SinavID = sg.FKSinavID,
                                 FKDersGrupID = yazilma.FKDersGrupID.Value,
                                 SinavAd = sg.OSinav.Ad,
                                 GrupAd = yazilma.OGRDersGrup.EnumOgretimTur.ToString() + " - " + yazilma.OGRDersGrup.GrupAd,
                                 HocaAd = yazilma.OGRDersGrup.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.Ad + " " + yazilma.OGRDersGrup.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.Soyad + " (" + yazilma.OGRDersGrup.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.KullaniciAdi + ")",
                                 BildirimBaslik = gb.Baslik,
                                 BildirimIcerik = gb.Icerik,
                                 BildirimTarih = gb.TarihKayit,
                                 BildirimDurum = gb.EnumGeriBildirimDurum,
                                 FKOgrenciID = gb.OgrenciID,
                                 OgrenciAd = yazilma.OGRKimlik.Kisi.Ad,
                                 OgrenciSoyad = yazilma.OGRKimlik.Kisi.Soyad,
                                 OgrenciNumara = yazilma.OGRKimlik.Numara
                             }).OrderBy(x => x.BildirimDurum).ThenByDescending(x => x.BildirimTarih).ToList();

                //var sorgu = (from dp in db.OGRDersPlan
                //             join gr in db.OGRDersGrup on dp.ID equals gr.FKDersPlanID
                //             join sg in db.OSinavGrup on gr.ID equals sg.FKDersGrupID
                //             join gb in db.ABSGeriBildirim on sg.FKSinavID equals gb.AktiviteID
                //             join s in db.OSinav on gb.AktiviteID equals s.ID
                //             join ogrkim in db.OGRKimlik on gb.OgrenciID equals ogrkim.ID
                //             where
                //             dp.ID == 420630 &&
                //             gb.EnumModulTipi == 0
                //             select new GeriBildirimSinav
                //             {
                //                 SinavID = sg.FKSinavID,
                //                 SinavAd = s.Ad,
                //                 BildirimBaslik = gb.Baslik,
                //                 BildirimIcerik = gb.Icerik,
                //                 BildirimTarih = gb.TarihKayit,
                //                 BildirimDurum = gb.EnumGeriBildirimDurum,
                //                 FKOgrenciID = gb.OgrenciID,
                //                 OgrenciAd = ogrkim.Kisi.Ad,
                //                 OgrenciSoyad = ogrkim.Kisi.Soyad,
                //                 OgrenciNumara = ogrkim.Numara

                //             }).Distinct().OrderBy(x => x.BildirimDurum).ToList();
                return sorgu;
            }
        }
        public static List<GeriBildirimOdev> _OdevGeriBildirimleri(int FKDersPlanID, int yil, int EnumDonem)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var sorgu = (from yazilma in db.OGROgrenciYazilma
                                 join sg in db.OOdevGrup on yazilma.FKDersGrupID equals sg.FKOGRDersGrupID
                                 join gb in db.ABSGeriBildirim on new { SINAVID = sg.OOdev.ID, OGRID = yazilma.FKOgrenciID.Value } equals new { SINAVID = gb.AktiviteID, OGRID = gb.OgrenciID }
                                 where
                                  yazilma.FKDersPlanID == FKDersPlanID &&
                                  yazilma.Yil == yil &&
                                  yazilma.EnumDonem == EnumDonem &&
                                  yazilma.Iptal == false &&
                                  yazilma.OGRKimlik.EnumOgrDurum == 2 &&
                                  gb.EnumModulTipi == 1
                                 select new GeriBildirimOdev
                                 {
                                     OdevID = sg.FKOOdevID,                                  
                                     FKDersGrupID = yazilma.FKDersGrupID.Value,
                                     OdevAd = sg.OOdev.Ad,
                                     OdevAciklama = sg.OOdev.Aciklama,
                                     GrupAd = yazilma.OGRDersGrup.EnumOgretimTur.ToString() + " - " + yazilma.OGRDersGrup.GrupAd,
                                     HocaAd = yazilma.OGRDersGrup.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.Ad + " " + yazilma.OGRDersGrup.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.Soyad + " (" + yazilma.OGRDersGrup.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.KullaniciAdi + ")",
                                     BildirimBaslik = gb.Baslik,
                                     BildirimIcerik = gb.Icerik,
                                     BildirimTarih = gb.TarihKayit,
                                     BildirimDurum = gb.EnumGeriBildirimDurum,
                                     FKOgrenciID = gb.OgrenciID,
                                     OgrenciAd = yazilma.OGRKimlik.Kisi.Ad,
                                     OgrenciSoyad = yazilma.OGRKimlik.Kisi.Soyad,
                                     OgrenciNumara = yazilma.OGRKimlik.Numara
                                 }).OrderBy(x => x.BildirimDurum).ThenByDescending(x => x.BildirimTarih).ToList();

                    return sorgu;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public static List<GeriBildirimSanalSinif> _SanalSinifGeriBildirimleri(int FKDersPlanID, int yil, int EnumDonem)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var sorgu = (from yazilma in db.OGROgrenciYazilma
                                 join sg in db.SanalSinifGrup on yazilma.FKDersGrupID equals sg.FKDersGrupID
                                 join gb in db.ABSGeriBildirim on new { SSINIFID = sg.SanalSinif.ID, OGRID = yazilma.FKOgrenciID.Value } equals new { SSINIFID = gb.AktiviteID, OGRID = gb.OgrenciID }
                                 where
                                  yazilma.FKDersPlanID == FKDersPlanID &&
                                  //yazilma.FKDersPlanID == 422711 &&
                                  yazilma.Yil == yil &&
                                  yazilma.EnumDonem == EnumDonem &&
                                  yazilma.Iptal == false &&
                                  yazilma.OGRKimlik.EnumOgrDurum == 2 &&
                                  gb.EnumModulTipi == 2
                                 select new GeriBildirimSanalSinif
                                 {
                                     SanalSinifID = sg.SSinifID,
                                     FKDersGrupID = yazilma.FKDersGrupID.Value,
                                     SanalSinifAd = sg.SanalSinif.Ad,
                                     GrupAd = yazilma.OGRDersGrup.EnumOgretimTur.ToString() + " - " + yazilma.OGRDersGrup.GrupAd,
                                     HocaAd = yazilma.OGRDersGrup.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.Ad + " " + yazilma.OGRDersGrup.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.Soyad + " (" + yazilma.OGRDersGrup.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.KullaniciAdi + ")",
                                     BildirimBaslik = gb.Baslik,
                                     BildirimIcerik = gb.Icerik,
                                     BildirimTarih = gb.TarihKayit,
                                     BildirimDurum = gb.EnumGeriBildirimDurum,
                                     FKOgrenciID = gb.OgrenciID,
                                     OgrenciAd = yazilma.OGRKimlik.Kisi.Ad,
                                     OgrenciSoyad = yazilma.OGRKimlik.Kisi.Soyad,
                                     OgrenciNumara = yazilma.OGRKimlik.Numara
                                 }).OrderBy(x => x.BildirimDurum).ThenByDescending(x => x.BildirimTarih).ToList();

                    return sorgu;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public static GeriBildirimSinav SinavGeriBildirimGetir(int SinavID, int FKOgrenciID, int FKDersGrupID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                //MesajClient.Test("1612.10507");

                //List<Sabis.Mesaj.Core.Istek.Mesaj> mesaj = new List<Sabis.Mesaj.Core.Istek.Mesaj>();
                //Sabis.Mesaj.Core.Istek.Mesaj sMesaj = new Sabis.Mesaj.Core.Istek.Mesaj();
                //sMesaj.DBKaydet = false;
                //string aliciKAdi = Sabis.Core.Utils.OgrenciNo.OgrenciNoKullaniciAdiCevir("1612.10507");
                //sMesaj.Alici = new Sabis.Mesaj.Core.Istek.Alici { KullaniciAdi = aliciKAdi };
                //sMesaj.EnumMesajTipi = Sabis.Mesaj.Core.MesajEnum.EnumMesajTipi.DersDuyuru;
                //string baslikMesaj = string.Format("{0} dersi için yeni bir duyurunuz var", "DersAd");
                //string icerikEposta = string.Format("{0}<br/>{1}<br/><br/>{2}", "Konu", "Mesaj", "Gönderen Ad Soyad");
                ////sMesaj.EPosta = new EPosta { Baslik = "SABİS - Ders Duyurusu", Icerik = icerikEposta, ReplyTo = "b161210507" + "@sakarya.edu.tr" };
                //sMesaj.Gonderen = new Gonderen { IPAdresi = "", KullaniciAdi = "b161210507" };
                ////sMesaj.MobilBildirim = new MobilBildirim { Baslik = baslikMesaj, Icerik = baslikMesaj };
                ////sMesaj.FacebookBildirim = new FacebookBildirim { Baslik = baslikMesaj };
                //mesaj.Add(sMesaj);
                //MesajClient.Gonder(mesaj);

                //var test = mesaj;




                var ogrenci = (from ogr in db.OGRKimlik
                               where
                               ogr.ID == FKOgrenciID
                               select new
                               {
                                   ogr.Kisi.Ad,
                                   ogr.Kisi.Soyad,
                                   ogr.Numara
                               }).FirstOrDefault();

                var gbildirim = (from gb in db.ABSGeriBildirim
                                 where
                                 gb.AktiviteID == SinavID &&
                                 gb.OgrenciID == FKOgrenciID &&
                                 gb.EnumModulTipi == 0
                                 select new
                                 {
                                     gb.ID,
                                     gb.AktiviteID,
                                     gb.OgrenciID,
                                     gb.Baslik,
                                     gb.Icerik,
                                     gb.DosyaKey,
                                     gb.Cevap,
                                     gb.TarihKayit,
                                     gb.EnumGeriBildirimDurum
                                 }).FirstOrDefault();


                var oturum = (from o in db.OSinavOgrenci
                              where
                              o.SinavID == SinavID &&
                              o.OgrenciID == FKOgrenciID
                              select new
                              {
                                  o.EID,
                                  o.OturumDurum,
                                  o.TarihOturumBaslangic,
                                  o.TarihOturumBitis,
                                  o.TarihMazeretOturumBaslangic,
                                  o.TarihMazeretOturumBitis,
                                  o.SoruSayisi,
                                  o.DogruSayisi,
                                  o.YanlisSayisi,
                                  o.MazeretDogruSayisi,
                                  o.MazeretYanlisSayisi,
                                  o.Notu,
                                  o.MazeretNotu
                              }).FirstOrDefault();

                //var oturumdetaylari = SINAVMAIN.OturumDetay(oturum.EID);

                var model = new GeriBildirimSinav();
                model.GeriBildirimID = gbildirim.ID;
                model.SinavID = gbildirim.AktiviteID;
                model.FKDersGrupID = FKDersGrupID;
                model.FKOgrenciID = gbildirim.OgrenciID;
                model.OgrenciAd = ogrenci.Ad;
                model.OgrenciSoyad = ogrenci.Soyad;
                model.OgrenciNumara = ogrenci.Numara;
                model.BildirimBaslik = gbildirim.Baslik;
                model.BildirimIcerik = gbildirim.Icerik;
                model.BildirimDosya = gbildirim.DosyaKey;
                model.BildirimCevap = gbildirim.Cevap;
                model.BildirimTarih = gbildirim.TarihKayit;
                model.BildirimDurum = gbildirim.EnumGeriBildirimDurum;
                if (oturum != null)
                {
                    model.OturumDurum = oturum.OturumDurum;
                    model.OturumunDurumu = Sabis.Enum.Utils.GetEnumDescription((SinavDurum)oturum.OturumDurum);
                    model.TarihOturumBaslangic = oturum.TarihOturumBaslangic;
                    model.TarihOturumBitis = oturum.TarihOturumBitis;
                    model.TarihMazeretOturumBaslangic = oturum.TarihMazeretOturumBaslangic;
                    model.TarihMazeretOturumBitis = oturum.TarihMazeretOturumBitis;
                    model.SoruSayisi = oturum.SoruSayisi;
                    model.DogruSayisi = oturum.DogruSayisi;
                    model.YanlisSayisi = oturum.YanlisSayisi;
                    model.BosSayisi = oturum.SoruSayisi - (oturum.DogruSayisi + oturum.YanlisSayisi);
                    model.MazeretDogruSayisi = oturum.MazeretDogruSayisi;
                    model.MazeretYanlisSayisi = oturum.MazeretYanlisSayisi;
                    model.MazeretBosSayisi = oturum.SoruSayisi - (oturum.MazeretDogruSayisi + oturum.MazeretYanlisSayisi);
                    model.Notu = oturum.Notu;
                    model.MazeretNotu = oturum.MazeretNotu;
                    var oturumdetaylari = SINAVMAIN.OturumDetay(oturum.EID);
                    model.oturumdetay = oturumdetaylari;
                }

                return model;
            }
        }
        public static GeriBildirimOdev OdevGeriBildirimGetir(int OdevID, int FKOgrenciID, int FKDersGrupID)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var ogrenci = (from ogr in db.OGRKimlik
                                   where
                                   ogr.ID == FKOgrenciID
                                   select new
                                   {
                                       ogr.Kisi.Ad,
                                       ogr.Kisi.Soyad,
                                       ogr.Numara
                                   }).FirstOrDefault();

                    var gbildirim = (from gb in db.ABSGeriBildirim
                                     where
                                     gb.AktiviteID == OdevID &&
                                     gb.OgrenciID == FKOgrenciID &&
                                     gb.EnumModulTipi == 1
                                     select new
                                     {
                                         gb.ID,
                                         gb.AktiviteID,
                                         gb.OgrenciID,
                                         gb.Baslik,
                                         gb.Icerik,
                                         gb.DosyaKey,
                                         gb.Cevap,
                                         gb.TarihKayit,
                                         gb.EnumGeriBildirimDurum
                                     }).FirstOrDefault();

                    var odevbilgi = db.OOdev.FirstOrDefault(x => x.ID == OdevID);
                    var odevogrenci = db.OOdevOgrenci.FirstOrDefault(x => x.FKOOdevID == OdevID && x.FKOgrenciID == FKOgrenciID);

                    var ogrnot = db.ABSOgrenciNot.FirstOrDefault(x => x.FKAbsPaylarID == odevbilgi.FKAbsPaylarID && x.FKOgrenciID == FKOgrenciID && x.Silindi == false);

                    var model = new GeriBildirimOdev();
                    model.GeriBildirimID = gbildirim.ID;
                    model.OdevID = gbildirim.AktiviteID;
                    model.OdevAd = odevbilgi.Ad;
                    model.OdevAciklama = odevbilgi.Aciklama;
                    model.BaslangicTarihi = odevbilgi.Baslangic;
                    model.BitisTarihi = odevbilgi.Bitis;
                    model.OdevOnline = odevbilgi.OnlineOdevMi;
                    model.OdevGenel = odevbilgi.GenelOdevMi;
                    model.OdevDosya = odevbilgi.OdevDosyaKey;
                    model.FKDersGrupID = FKDersGrupID;
                    model.FKOgrenciID = gbildirim.OgrenciID;
                    model.OgrenciAd = ogrenci.Ad;
                    model.OgrenciSoyad = ogrenci.Soyad;
                    model.OgrenciNumara = ogrenci.Numara;
                    model.OdevYuklemeTarih = odevogrenci.YuklemeTarihi;
                    model.OdevOgrenciDosya = odevogrenci.DosyaKey;
                    model.OdevDosyaTuru = odevogrenci.DosyaTuru;
                    model.BildirimBaslik = gbildirim.Baslik;
                    model.BildirimIcerik = gbildirim.Icerik;
                    model.BildirimDosya = gbildirim.DosyaKey;
                    model.BildirimCevap = gbildirim.Cevap;
                    model.BildirimTarih = gbildirim.TarihKayit;
                    model.BildirimDurum = gbildirim.EnumGeriBildirimDurum;
                    if (ogrnot != null)
                    {
                        model.OdevNotu = ogrnot.NotDeger;
                    }
                    else
                    {
                        model.OdevNotu = -1;
                    }
                   
                    model.OdevNotuAciklamasi = odevogrenci.Aciklama;
                    
                    return model;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public static GeriBildirimSanalSinif SanalSinifGeriBildirimGetir(int SanalSinifID, int FKOgrenciID, int FKDersGrupID)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var ogrenci = (from ogr in db.OGRKimlik
                                   where
                                   ogr.ID == FKOgrenciID
                                   select new
                                   {
                                       ogr.Kisi.Ad,
                                       ogr.Kisi.Soyad,
                                       ogr.Numara
                                   }).FirstOrDefault();

                    var gbildirim = (from gb in db.ABSGeriBildirim
                                     where
                                     gb.AktiviteID == SanalSinifID &&
                                     gb.OgrenciID == FKOgrenciID &&
                                     gb.EnumModulTipi == 2
                                     select new
                                     {
                                         gb.ID,
                                         gb.AktiviteID,
                                         gb.OgrenciID,
                                         gb.Baslik,
                                         gb.Icerik,
                                         gb.DosyaKey,
                                         gb.Cevap,
                                         gb.TarihKayit,
                                         gb.EnumGeriBildirimDurum
                                     }).FirstOrDefault();

                    var sanalsinifbilgi = db.SanalSinif.FirstOrDefault(x => x.ID == SanalSinifID);
                    var sanalsinifogrenci = db.SanalSinifOgrenci.FirstOrDefault(x => x.SSinifID == SanalSinifID && x.OgrenciID == FKOgrenciID);

                    var model = new GeriBildirimSanalSinif();
                    model.GeriBildirimID = gbildirim.ID;
                    model.SanalSinifID = gbildirim.AktiviteID;
                    model.SunucuID = sanalsinifbilgi.SunucuID;
                    model.RoomID = sanalsinifbilgi.SSinifKey;
                    model.AktifSure = sanalsinifbilgi.AktifSure;
                    model.SanalSinifBaslangic = sanalsinifbilgi.Baslangic;
                    model.SanalSinifBitis = sanalsinifbilgi.Bitis;
                    //if (sanalsinifogrenci.IzlemeKey != null)
                    //{
                    //    model.OgrenciSanalSinifAdres = sanalsinifogrenci.IzlemeKey;
                    //}
                    //else
                    //{
                    //    model.OgrenciSanalSinifAdres = sanalsinifbilgi.Adres;
                    //}
                    model.OgrenciSanalSinifAdres = "";

                    model.SanalSinifAd = sanalsinifbilgi.Ad;
                    model.FKDersGrupID = FKDersGrupID;
                    model.FKOgrenciID = gbildirim.OgrenciID;
                    model.OgrenciAd = ogrenci.Ad;
                    model.OgrenciSoyad = ogrenci.Soyad;
                    model.OgrenciNumara = ogrenci.Numara;                 
                    model.BildirimBaslik = gbildirim.Baslik;
                    model.BildirimIcerik = gbildirim.Icerik;
                    model.BildirimDosya = gbildirim.DosyaKey;
                    model.BildirimCevap = gbildirim.Cevap;
                    model.BildirimTarih = gbildirim.TarihKayit;
                    model.BildirimDurum = gbildirim.EnumGeriBildirimDurum;

                    // detay açılınca bildirim okundu olarak işaretleniyor.
                    var gbildirimguncelle = db.ABSGeriBildirim.FirstOrDefault(x => x.ID == gbildirim.ID);
                    gbildirimguncelle.EnumGeriBildirimDurum = 1;
                    db.SaveChanges();



                    return model;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        #endregion
        
        #region BÖLÜM KOORDİNATÖR CRUD
        public static List<BolumKoordinatorVM> GetirBolumKoordinatorListesi(int fakulteID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                List<BolumKoordinatorVM> bolumKoordinatorleri = new List<BolumKoordinatorVM>();

                var idariGorevList = db.IdariGorev.Where(x => (x.FKGorevUnvaniID == 10 || x.FKGorevUnvaniID == 13) && x.FKFakulteID.Value == fakulteID && x.BitisTarihi > DateTime.Now).Select(x => new BolumKoordinatorVM
                {
                    ID = x.ID,
                    YetkiGrupID = x.FKGorevUnvaniID.Value,
                    AdSoyad = x.PersonelBilgisi.UnvanAd + " " + x.PersonelBilgisi.Ad + " " + x.PersonelBilgisi.Soyad,
                    FKPersonelID = x.FKPersonelBilgisiID,
                    YetkiBaslangicTarihi = x.BaslangicTarihi,
                    YetkiBitisTarihi = x.BitisTarihi,
                    Zaman = x.Zaman,
                    FakulteID = x.FKFakulteID,
                    BolumID = x.FKBolumID,
                    FKProgramBirimID = x.BirimID,
                    IdariMi = true
                }).ToList();

                var yetkiList = db.Yetkililer.Where(x => (x.YetkiGrupID == (int)Sabis.Bolum.Core.ENUM.EBS.EnumYetkiGrup.BolumKoordinator || x.YetkiGrupID == (int)Sabis.Bolum.Core.ENUM.EBS.EnumYetkiGrup.FakulteKoordinator) && x.FKFakulteBirimID == fakulteID && x.YetkiBitis > DateTime.Now).Select(x => new BolumKoordinatorVM
                {
                    ID = x.ID,
                    YetkiGrupID = x.YetkiGrupID,
                    AdSoyad = db.PersonelBilgisi.Where(y => y.ID == x.FKPersonelID).Select(y => y.UnvanAd + " " + y.Ad + " " + y.Soyad).FirstOrDefault(),
                    FKPersonelID = x.FKPersonelID,
                    YetkiBaslangicTarihi = x.YetkiBaslangic,
                    YetkiBitisTarihi = x.YetkiBitis,
                    Zaman = x.SGZaman,
                    FakulteID = x.FKFakulteBirimID,
                    BolumID = x.FKBolumBirimID,
                    FKProgramBirimID = x.FKProgramBirimID,
                    IdariMi = false
                }).ToList();

                bolumKoordinatorleri = yetkiList.Concat(idariGorevList).ToList();
                return bolumKoordinatorleri;
            }
        }
        public static bool GuncelleBolumKoordinator(int? id, int FKPersonelID, int? BolumID, string kullanici)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    if (db.Yetkililer.Any(x => x.FKBolumBirimID == BolumID.Value)) // yetki kaydı varsa
                    {
                        var koordinatorBilgi = db.Yetkililer.FirstOrDefault(x => x.FKBolumBirimID == BolumID.Value);

                        koordinatorBilgi.FKPersonelID = FKPersonelID;
                        koordinatorBilgi.FKBolumBirimID = BolumID;
                        koordinatorBilgi.FKFakulteBirimID = Sabis.Client.BirimIslem.Instance.getirBagliFakulteID(BolumID.Value);
                        koordinatorBilgi.SGKullanici = kullanici;
                        koordinatorBilgi.SGZaman = DateTime.Now;
                        db.SaveChanges();
                    }
                    else
                    {
                        Yetkililer yeniYetki = new Yetkililer();
                        yeniYetki.YetkiGrupID = 3;
                        yeniYetki.FKPersonelID = FKPersonelID;
                        yeniYetki.YetkiBaslangic = DateTime.Now;
                        yeniYetki.YetkiBitis = DateTime.Now.AddYears(1);
                        yeniYetki.SGKullanici = kullanici;
                        yeniYetki.SGZaman = DateTime.Now;
                        yeniYetki.FKFakulteBirimID = Sabis.Client.BirimIslem.Instance.getirBagliFakulteID(BolumID.Value);
                        yeniYetki.FKBolumBirimID = BolumID;

                        db.Yetkililer.Add(yeniYetki);
                        db.SaveChanges();
                    }

                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool EkleBolumKoordinator(int FakulteID, int? BolumID, int? ProgramID, int? AnaBilimDaliID, int FKPersonelID, string ekleyen)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    Yetkililer yeniYetki = new Yetkililer();
                    yeniYetki.FKFakulteBirimID = FakulteID;
                    yeniYetki.FKBolumBirimID = BolumID;
                    yeniYetki.FKProgramBirimID = ProgramID;
                    yeniYetki.FKAnaBilimDaliID = AnaBilimDaliID;
                    yeniYetki.YetkiGrupID = BolumID == 0 ? (int)Sabis.Bolum.Core.ENUM.EBS.EnumYetkiGrup.FakulteKoordinator : (int)Sabis.Bolum.Core.ENUM.EBS.EnumYetkiGrup.BolumKoordinator;
                    yeniYetki.YetkiBaslangic = DateTime.Now;
                    yeniYetki.YetkiBitis = DateTime.Now.AddYears(1);
                    yeniYetki.FKPersonelID = FKPersonelID;
                    yeniYetki.SGKullanici = ekleyen;
                    yeniYetki.SGZaman = DateTime.Now;
                    db.Yetkililer.Add(yeniYetki);
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool SilBolumKoordinator(int id)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var item = db.Yetkililer.FirstOrDefault(x => x.ID == id);
                    db.Yetkililer.Remove(item);
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static string GetirBolumVekilBilgi(int BolumID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                if (db.Yetkililer.Any(x => x.FKBolumBirimID == BolumID && x.YetkiGrupID == 3))
                {
                    var vekilID = db.Yetkililer.FirstOrDefault(x => x.FKBolumBirimID == BolumID && x.YetkiGrupID == 3).FKPersonelID;
                    return db.PersonelBilgisi.Where(x => x.ID == vekilID).Select(x => x.Ad + " " + x.Soyad).FirstOrDefault();
                }
                else
                {
                    return "Bulunamadı";
                }
            }
        }
        public static bool BolumVekiliMi(int FKPersonelID, int BolumID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return db.Yetkililer.Any(x => x.FKPersonelID == FKPersonelID && x.FKBolumBirimID == BolumID);
            }
        }
        #endregion

        #region BÖLÜM PROGRAM ÇIKTILARI CRUD
        public static List<BolumProgramCiktilariVM> GetirBolumProgramCiktilari(int FKBirimID, int yil, int enumDilID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var birim = db.Birimler.FirstOrDefault(x => x.ID == FKBirimID);
                //if (birim.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.)
                //{

                //}

                if (birim.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.Fakulte || birim.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.Universite)
                {
                    return new List<BolumProgramCiktilariVM>();
                }

                int birimID = Sabis.Client.BirimIslem.Instance.getirBagliBolumID(FKBirimID);
                if (birim.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.AraProgram)
                {
                    birimID = birim.ID;
                }
                
                List<BolumProgramCiktilariVM> sonuc = (from yeterlilik in db.BolumYeterlilik
                                                       join dilyeterlilik in db.DilBolumYeterlilik on yeterlilik.ID equals dilyeterlilik.FKBolumYeterlilikID
                                                       where yeterlilik.FKProgramBirimID == FKBirimID && yeterlilik.Yil == yil && dilyeterlilik.EnumDilID == enumDilID && yeterlilik.FKProgramBirimID.HasValue //yeterlilik.FKProgramBirimID == birimID && 
                                                       select new BolumProgramCiktilariVM
                                                       {
                                                           ID = yeterlilik.ID,
                                                           DilBolumYeterlilikID = dilyeterlilik.ID,
                                                           EnumDilID = enumDilID,
                                                           YetID = yeterlilik.YetID,
                                                           Icerik = dilyeterlilik.Icerik,
                                                           Yil = yeterlilik.Yil,
                                                           Ekleyen = yeterlilik.Ekleyen,
                                                           Zaman = yeterlilik.Zaman,
                                                           BolumYeterlilikKategoriList = db.BolumYeterlilikKategoriIliski.Where(x => x.FKYeterlilikID == yeterlilik.ID).Select(x => new BolumYeterlilikAltKategoriVM
                                                           {
                                                               FKAnaKategoriID = x.YeterlilikAltKategori.FKAnaKategoriID,
                                                               FKYeterlilikAltKategoriID = x.FKYeterlilikAltKategoriID,
                                                               ID = x.FKYeterlilikID.Value,
                                                           }).ToList(),
                                                           BirimID = yeterlilik.FKProgramBirimID,
                                                           ProgramID = yeterlilik.ProgramID
                                                       }
                               ).OrderBy(x => x.YetID).ToList();

                List<BolumProgramCiktilariVM> list = new List<BolumProgramCiktilariVM>();

                if (birim.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.AraProgram)
                {
                    list = sonuc.Select(x => new BolumProgramCiktilariVM
                    {
                        ID = x.ID,
                        DilBolumYeterlilikID = x.DilBolumYeterlilikID,
                        EnumDilID = x.EnumDilID,
                        YetID = x.YetID,
                        Icerik = x.Icerik,
                        Yil = x.Yil,
                        Ekleyen = x.Ekleyen,
                        Zaman = x.Zaman,
                        BolumYeterlilikKategoriList = x.BolumYeterlilikKategoriList,
                        BirimID = x.BirimID,
                        ProgramID = x.ProgramID
                    })
                .Where(x => x.ProgramID.HasValue ? x.ProgramID == FKBirimID : x.BirimID.Value == birimID && x.Yil == yil)
                .ToList();
                }
                else
                {
                    list = sonuc.Select(x => new BolumProgramCiktilariVM
                    {
                        ID = x.ID,
                        DilBolumYeterlilikID = x.DilBolumYeterlilikID,
                        EnumDilID = x.EnumDilID,
                        YetID = x.YetID,
                        Icerik = x.Icerik,
                        Yil = x.Yil,
                        Ekleyen = x.Ekleyen,
                        Zaman = x.Zaman,
                        BolumYeterlilikKategoriList = x.BolumYeterlilikKategoriList,
                        BirimID = x.BirimID,
                        ProgramID = x.ProgramID
                    })
                .Where(x => x.ProgramID.HasValue ? x.ProgramID == FKBirimID : Sabis.Client.BirimIslem.Instance.getirBagliBolumID(x.BirimID.Value) == birimID && x.Yil == yil)
                .ToList();
                }

                return list;
            }
        }
        public static bool GuncelleProgramCiktisi(BolumProgramCiktilariVM programCiktisi, int yil, string ekleyen)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    BolumYeterlilik BY = db.BolumYeterlilik.FirstOrDefault(x => x.ID == programCiktisi.ID);
                    DilBolumYeterlilik DBY = db.DilBolumYeterlilik.FirstOrDefault(x => x.ID == programCiktisi.DilBolumYeterlilikID);

                    //BY.FKProgramBirimID = programCiktisi.BirimID;
                    BY.Yil = programCiktisi.Yil.Value;
                    BY.Ekleyen = programCiktisi.Ekleyen;
                    BY.YetID = programCiktisi.YetID;
                    BY.Zaman = DateTime.Now;
                    BY.Ekleyen = programCiktisi.Ekleyen;

                    DBY.FKBolumYeterlilikID = BY.ID;
                    DBY.Icerik = programCiktisi.Icerik;
                    DBY.EnumDilID = (byte)programCiktisi.EnumDilID;
                    DBY.Ekleyen = programCiktisi.Ekleyen;
                    DBY.Zaman = DateTime.Now;

                    foreach (var item in db.BolumYeterlilikKategoriIliski.Where(x => x.FKYeterlilikID == BY.ID))
                    {
                        db.BolumYeterlilikKategoriIliski.Remove(item);
                    }
                    db.SaveChanges();

                    if (programCiktisi.BolumYeterlilikKategoriList != null)
                    {
                        foreach (var subItem in programCiktisi.BolumYeterlilikKategoriList)
                        {
                            BolumYeterlilikKategoriIliski byki = new BolumYeterlilikKategoriIliski();
                            byki.FKYeterlilikID = BY.ID;
                            byki.FKYeterlilikAltKategoriID = subItem.FKYeterlilikAltKategoriID.Value;
                            byki.FKProgramBirimID = subItem.FKProgramBirimID;
                            byki.Yil = yil;
                            byki.Ekleyen = ekleyen;
                            byki.Zaman = DateTime.Now;
                            db.BolumYeterlilikKategoriIliski.Add(byki);
                        }
                    }

                    db.SaveChanges();

                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool EkleYeniProgramCiktisi(BolumProgramCiktilariVM yeniProgramCiktisi)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    BolumYeterlilik BY = new BolumYeterlilik();
                    DilBolumYeterlilik DBY = new DilBolumYeterlilik();

                    //int yetID = db.BolumYeterlilik.Count(x => x.FKProgramBirimID == yeniProgramCiktisi.BirimID && x.Yil == yeniProgramCiktisi.Yil)+1;
                    int yetID = db.DilBolumYeterlilik.Count(x => x.BolumYeterlilik.FKProgramBirimID == yeniProgramCiktisi.BirimID && x.BolumYeterlilik.Yil == yeniProgramCiktisi.Yil && x.EnumDilID == yeniProgramCiktisi.EnumDilID) + 1;

                    BY.FKProgramBirimID = yeniProgramCiktisi.BirimID;
                    BY.Yil = yeniProgramCiktisi.Yil.Value;
                    BY.Ekleyen = yeniProgramCiktisi.Ekleyen;
                    BY.Zaman = DateTime.Now;
                    BY.YetID = yetID;
                    db.BolumYeterlilik.Add(BY);
                    db.SaveChanges();

                    DBY.FKBolumYeterlilikID = BY.ID;
                    DBY.Icerik = yeniProgramCiktisi.Icerik;
                    DBY.EnumDilID = 1;
                    DBY.Ekleyen = yeniProgramCiktisi.Ekleyen;
                    DBY.Zaman = DateTime.Now;
                    db.DilBolumYeterlilik.Add(DBY);
                    db.SaveChanges();

                    DBY.FKBolumYeterlilikID = BY.ID;
                    DBY.Icerik = yeniProgramCiktisi.Icerik;
                    DBY.EnumDilID = 2;
                    DBY.Ekleyen = yeniProgramCiktisi.Ekleyen;
                    DBY.Zaman = DateTime.Now;
                    db.DilBolumYeterlilik.Add(DBY);
                    db.SaveChanges();

                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool SilDersProgramCiktisi(int bolumYeterlilikID)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var BolumYeterlilik = db.BolumYeterlilik.FirstOrDefault(x => x.ID == bolumYeterlilikID);
                    var DilBolumYeterlilik = db.DilBolumYeterlilik.Where(x => x.FKBolumYeterlilikID == bolumYeterlilikID);
                    var BolumYeterlilikIliski = db.BolumYeterlilikKategoriIliski.Where(x => x.FKYeterlilikID == BolumYeterlilik.ID);

                    foreach (var item in BolumYeterlilikIliski)
                    {
                        db.BolumYeterlilikKategoriIliski.Remove(item);
                    }
                    db.SaveChanges();

                    foreach (var item in DilBolumYeterlilik)
                    {
                        db.DilBolumYeterlilik.Remove(item);
                    }
                    db.SaveChanges();

                    db.BolumYeterlilik.Remove(BolumYeterlilik);
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static GeriBildirimVM GeriBildirimDetay(int FKDersPlanID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var sorgu = (from dp in db.OGRDersPlan
                             where
                             dp.ID == FKDersPlanID
                             select new GeriBildirimVM
                             {
                                 FKDersPlanID = dp.ID,
                                 FKBirimID = dp.Birimler.ID,
                                 DersAd = dp.OGRDersPlanAna.DersAd,
                                 BirimAd = dp.Birimler.BirimAdi
                             }).FirstOrDefault();
                

                return sorgu;
            }
        }
        #endregion
    }
}
