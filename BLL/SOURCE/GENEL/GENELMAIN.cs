﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Bolum.Core.GENEL;
//using System.Net.Http;

using System.Net;
using System.Net.Mail;

namespace Sabis.Bolum.Bll.SOURCE.GENEL
{
    public class GENELMAIN
    {
        public static List<BirimVM> GetirBirimAgaci(int? ustBirimID, bool idariDahil = false)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                if (!ustBirimID.HasValue)
                {
                    ustBirimID = 21398;
                }

                List<BirimVM> sorgu = db.Birimler
                    .Where(x => x.UstBirimID == ustBirimID)
                    .Select(p => new BirimVM
                    {
                        ID = p.ID,
                        Ad = p.BirimAdi,
                        EnumBirimTuru = p.EnumBirimTuru,
                        BirimAdiUzun = p.BirimUzunAdi
                    }).OrderBy(x => x.Ad).ToList();

                if (!idariDahil)
                {
                    sorgu = sorgu.Where(x =>
                        x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.Anabilimdali ||
                        x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.AnaSanatDali ||
                        x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.BilimDali ||
                        x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.BilimselHazirlikProg ||
                        x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.Bolum ||
                        x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.CAPLisansProg ||
                        x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.DGSLisansProg ||
                        x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.DoktoraAnaBilimDali ||
                        x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.DoktoraProgrami ||
                        x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.Enstitu ||
                        x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.Fakulte ||
                        x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.LisansProgrami ||
                        x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.MeslekYuksekOkulu ||
                        x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.OnLisansProgrami ||
                        x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.Program ||
                        x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.SanatDali ||
                        x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.SanattaYeterlilikAnaBilimDali ||
                        x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.TezsizYLisansProg ||
                        x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.Universite ||
                        x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.VakifMeslekYuksekOkulu ||
                        x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.YandalLisansProg ||
                        x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.YDilHazirlikProg ||
                        x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.YuksekLisansProgrami ||
                        x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.YuksekOkul
                    ).ToList();
                }
                return sorgu;
            }
        }

        public static int GetirBirimIDByFKDersPlanAnaID(int FKDersPlanAnaID, int yil)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return db.OGRDersPlan.Where(x => x.FKDersPlanAnaID == FKDersPlanAnaID && x.Yil == yil).Select(x => x.FKBolumPrBirimID).FirstOrDefault().Value;
            }
        }

        public static bool MailGonder(string hata, string kullaniciAdi, string IP, int? dersGrupHocaID)
        {
            try
            {



                //var fromAddress = new MailAddress("hakki@sakarya.edu.tr", "ABS");
                //var toAddress = new MailAddress("hakki@sakarya.edu.tr", "İsmail Hakkı Şen");
                //const string fromPassword = "ismail92192";
                //const string subject = "ABS Otomatik Hata Bilgilendirme";
                //string body = hata;

                //var smtp = new SmtpClient
                //{
                //    Host = "smtp.gmail.com",
                //    Port = 587,
                //    EnableSsl = true,
                //    DeliveryMethod = SmtpDeliveryMethod.Network,
                //    UseDefaultCredentials = false,
                //    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                //};
                //using (var message = new MailMessage(fromAddress, toAddress)
                //{
                //    Subject = subject,
                //    Body = body
                //})
                //{
                //    smtp.Send(message);
                //}

                SmtpClient SmtpServer = new SmtpClient("smtp.live.com");
                var mail = new MailMessage();
                mail.From = new MailAddress("hakki@outlook.com");
                mail.To.Add("hakki@sakarya.edu.tr");
                mail.Subject = "ABS Otomatik Hata Bilgilendirme";
                mail.IsBodyHtml = true;
                string htmlBody;
                htmlBody = hata + "<br />" + kullaniciAdi + "<br />" + IP;
                if (dersGrupHocaID.HasValue)
                {
                    htmlBody += "<br />" + dersGrupHocaID;
                }
                mail.Body = htmlBody;
                SmtpServer.Port = 587;
                SmtpServer.UseDefaultCredentials = false;
                SmtpServer.Credentials = new System.Net.NetworkCredential("hakki@outlook.com", "ismail92192");
                SmtpServer.EnableSsl = true;
                //SmtpServer.Send(mail);


                //                Sabis.Mesaj.Core.Istek.Mesaj yeniMesaj = new Sabis.Mesaj.Core.Istek.Mesaj();


                //                yeniMesaj.Alici.KullaniciAdi = "hakki@sakarya.edu.tr";
                //                yeniMesaj.EnumMesajTipi = Sabis.Mesaj.Core.MesajEnum.EnumMesajTipi.Uyari;
                //                yeniMesaj.EPosta = new Sabis.Mesaj.Core.Istek.EPosta { Baslik = "ABS Otomatik Hata Bildirim", Icerik = hata, ReplyTo = "hakki@sakarya.edu.tr" };
                //                yeniMesaj.Gonderen = new Sabis.Mesaj.Core.Istek.Gonderen { IPAdresi = IP, KullaniciAdi = kullaniciAdi };

                //                string mesajApiBaseURL = "http://mesaj.local.apollo.gtu.edu.tr/";

                //#if DEBUG
                //                mesajApiBaseURL = "http://mesaj.apollo.gtu.edu.tr/";
                //#endif
                //                using (var client = new HttpClient())
                //                {
                //                    client.BaseAddress = new Uri(mesajApiBaseURL);
                //                    client.DefaultRequestHeaders.Accept.Clear();
                //                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                //                    HttpResponseMessage response = client.PostAsJsonAsync("api/Mesaj/Gonder", yeniMesaj).Result;
                //                    if (response.IsSuccessStatusCode)
                //                    {
                //                        return true;
                //                    }
                //                }
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
