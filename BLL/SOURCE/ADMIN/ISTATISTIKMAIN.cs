﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Bolum.Core.ADMIN;

namespace Sabis.Bolum.Bll.SOURCE.ADMIN
{
    public class ISTATISTIKMAIN
    {
        public static IstatistikVM GetirIstatistikler(int yil, int donem)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                IstatistikVM istatistik = new IstatistikVM();

                istatistik.SonHalSayisi = db.ABSDegerlendirme.Count(x => x.Yil == yil && x.EnumDonem == donem && x.EnumSonHal == (int)Sabis.Enum.EnumSonHal.EVET);
                istatistik.GrupSayisi = db.OGRDersGrup.Count(x => x.OgretimYili == yil && x.EnumDonem == donem && x.Acik == true);
                istatistik.SonHalOran =  (istatistik.SonHalSayisi / istatistik.GrupSayisi) * 100;

                istatistik.OgrenciSayisi = db.OGRKimlik.Count(x => x.EnumOgrDurum == (int)Sabis.Enum.EnumOgrDurum.NORMAL);
                istatistik.YazilmaSayisi = db.OGROgrenciYazilma.Where(x => x.Yil == yil && x.EnumDonem == donem && x.Iptal == false).GroupBy(x => x.FKOgrenciID).Count();
                istatistik.YazilmaOran = (istatistik.YazilmaSayisi / istatistik.OgrenciSayisi) * 100;

                return istatistik;
            }
        }

        public static List<DokumanRaporVM> GetirDokumanRapor(int yil, int donem)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var dokumanList = db.ABSDokumanGrup.Where(x => x.ABSDokuman.Yil == yil && x.ABSDokuman.EnumDonem == donem && x.Silindi == false && x.ABSDokuman.Silindi == false); //.GroupBy(x=> x.FKOGRDersGrupID).ToList();

                //List<DokumanRaporVM> rList = new List<DokumanRaporVM>();

                //foreach (var item in dokumanList)
                //{
                //    DokumanRaporVM yeniDokuman = new DokumanRaporVM();
                //    yeniDokuman.DersAd == dokumanList.FirstOrDefault
                //}

                var list = dokumanList
                .GroupBy(x => x.FKOGRDersGrupID)
                .Select(x => new DokumanRaporVM
                {
                    DersAd = x.FirstOrDefault(y => y.FKOGRDersGrupID == x.Key).ABSDokuman.OGRDersPlanAna.DersAd,
                    DokumanSayisi = x.Count(y => y.FKOGRDersGrupID == x.Key && y.Silindi == false),
                    HocaAdSoyadUnvan = x.FirstOrDefault(y => y.FKOGRDersGrupID == x.Key).OGRDersGrup.OGRDersGrupHoca.FirstOrDefault().FKPersonelID.HasValue ? x.FirstOrDefault(y => y.FKOGRDersGrupID == x.Key).OGRDersGrup.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.UnvanAd + " " + x.FirstOrDefault(y => y.FKOGRDersGrupID == x.Key).OGRDersGrup.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.Kisi.Ad + " " + x.FirstOrDefault(y => y.FKOGRDersGrupID == x.Key).OGRDersGrup.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.Soyad : "",
                    DersGrupAd = x.FirstOrDefault(y=> y.FKOGRDersGrupID == x.Key).OGRDersGrup.GrupAd,
                    FKDersPlanAnaID = x.FirstOrDefault(y=> y.FKOGRDersGrupID == x.Key).OGRDersGrup.FKDersPlanAnaID.Value
                }).ToList();

                return list;
            }
        }
    }
}
