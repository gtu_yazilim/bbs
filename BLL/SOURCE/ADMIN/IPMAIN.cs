﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Bolum.Core.ADMIN;

namespace Sabis.Bolum.Bll.SOURCE.ADMIN
{
    public class IPMAIN
    {
        public static List<IPYetkiVM> GetirIPYetkiListesi()
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return db.ABSIPYetki.Select(x => new IPYetkiVM
                {
                    Aciklama = x.Aciklama,
                    AdSoyad = x.AdSoyad,
                    BirimBolum = x.BirimBolum,
                    IPAdresi = x.IPAdresi,
                    IpYetkiID = x.ID,
                    KullaniciAdi = x.KullaniciAdi,
                    SimuleYetkisiVarMi = x.SimuleYetkisiVarMi.HasValue ? x.SimuleYetkisiVarMi.Value : false,
                    YetkiVar = x.YetkiVar,
                    YetkiVeren = x.YetkiVeren,
                }).OrderByDescending(x=>x.IpYetkiID).ToList();
            }
        }
        public static bool EkleIPYetki(IPYetkiVM ipYetki)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                try
                {
                    ABSIPYetki ipYetkisi = new ABSIPYetki();
                    ipYetkisi.Aciklama = ipYetki.Aciklama;
                    ipYetkisi.AdSoyad = ipYetki.AdSoyad;
                    ipYetkisi.BirimBolum = ipYetki.BirimBolum;
                    ipYetkisi.IPAdresi = ipYetki.IPAdresi;
                    ipYetkisi.KullaniciAdi = ipYetki.KullaniciAdi;
                    ipYetkisi.SimuleYetkisiVarMi = ipYetki.SimuleYetkisiVarMi;
                    ipYetkisi.SonHalKaldirmaVarMi = ipYetki.SonHalKaldirmaVarMi;
                    ipYetkisi.Tarih = DateTime.Now;
                    ipYetkisi.YetkiVar = ipYetki.YetkiVar;
                    ipYetkisi.YetkiVeren = ipYetki.YetkiVeren;
                    db.ABSIPYetki.Add(ipYetkisi);
                    db.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }
        public static void GuncelleIPYetki(IPYetkiVM ipYetki)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                try
                {
                    ABSIPYetki ipYetkisi = db.ABSIPYetki.Find(ipYetki.IpYetkiID);
                    ipYetkisi.Aciklama = ipYetki.Aciklama;
                    ipYetkisi.AdSoyad = ipYetki.AdSoyad;
                    ipYetkisi.BirimBolum = ipYetki.BirimBolum;
                    ipYetkisi.IPAdresi = ipYetki.IPAdresi;
                    ipYetkisi.KullaniciAdi = ipYetki.KullaniciAdi;
                    ipYetkisi.SimuleYetkisiVarMi = ipYetki.SimuleYetkisiVarMi;
                    ipYetkisi.SonHalKaldirmaVarMi = ipYetki.SonHalKaldirmaVarMi;
                    ipYetkisi.Tarih = DateTime.Now;
                    ipYetkisi.YetkiVar = ipYetki.YetkiVar;
                    ipYetkisi.YetkiVeren = ipYetki.YetkiVeren;
                    db.SaveChanges();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        public static IPYetkiVM GetirIPYetki(int ID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return db.ABSIPYetki.Where(x => x.ID == ID).Select(x => new IPYetkiVM
                {
                    Aciklama = x.Aciklama,
                    AdSoyad = x.AdSoyad,
                    BirimBolum = x.BirimBolum,
                    IPAdresi = x.IPAdresi,
                    IpYetkiID = x.ID,
                    KullaniciAdi = x.KullaniciAdi,
                    SimuleYetkisiVarMi = x.SimuleYetkisiVarMi.HasValue ? x.SimuleYetkisiVarMi.Value : false,
                    YetkiVar = x.YetkiVar,
                    YetkiVeren = x.YetkiVeren,
                }).FirstOrDefault();
            }
        }
        public static bool SilIpYetki(int ID)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    ABSIPYetki orjinal = db.ABSIPYetki.Find(ID);
                    db.ABSIPYetki.Remove(orjinal);
                    db.SaveChanges();
                    return true;
                } 
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool KontrolIPYetki(string kullanciAdi, string IP)
        {
            bool result = false;
            using (UYSv2Entities db = new UYSv2Entities())
            {
                if (!string.IsNullOrEmpty(kullanciAdi) && !string.IsNullOrEmpty(IP))
                {
                    result = db.ABSIPYetki.Where(x => (x.IPAdresi == IP || x.KullaniciAdi == kullanciAdi) && x.YetkiVar).Any();
                }
            }
            return result;
        }
    }
}
