﻿using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Bolum.Core.Hoca;
using Sabis.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Bll.SOURCE.HOCA
{
    public class HocaBilgileri
    {
        public static List<Sabis.Bolum.Core.Hoca.DtoPersonelBilgisi> ListeleBolumHocalari(int personelID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var idariGorevler = db.IdariGorev.Where(a => a.FKPersonelBilgisiID == personelID && a.FKBolumID != null).ToList();
                List<int> birimler = new List<int>();
                foreach(IdariGorev ig in idariGorevler)
                {
                    var birim = Sabis.Client.BirimIslem.Instance.getirAltBirimlerID(ig.FKBolumID, true);
                    birimler.AddRange(birim);
                }
                if (idariGorevler.Count >0)
                {
                    return db.PersonelBilgisi.Where(a => birimler.Contains((int)a.FK_GorevBolID) && a.EnumPersonelDurum == (int)Sabis.Enum.EnumPersonelDurum.AKTIF).Select(b => new DtoPersonelBilgisi()
                    {
                        ID = b.ID,
                        Ad = b.Ad,
                        Soyad = b.Soyad,
                        Unvan = b.UnvanAd,
                        TC = b.TC,
                        KullaniciAdi = b.KullaniciAdi,
                        BolumID = b.FK_GorevBolID.Value,
                        FakulteID = b.FK_GorevFakID.Value
                    }).ToList();
                }
                else
                    return null;
            }

        }

        public static DtoHocaDersBilgileri HocaDersList(int personelID, int yil, int enumDonem)
        {
            DtoHocaDersBilgileri dersList = new DtoHocaDersBilgileri();
            using (UYSv2Entities db = new UYSv2Entities())
            {
                List<Sabis.Bolum.Core.ABS.DERS.HOME.HocaDersListesiViewModelv2> dersler = db.OGRDersGrupHoca.Where(a => a.FKPersonelID == personelID && a.OGRDersGrup.OgretimYili == yil && a.OGRDersGrup.EnumDonem == enumDonem && a.OGRDersGrup.Acik == true).Select(b => new Core.ABS.DERS.HOME.HocaDersListesiViewModelv2()
                {
                    DPAnaID = b.OGRDersGrup.OGRDersPlan.FKDersPlanAnaID.Value,
                    DersKod = b.OGRDersGrup.OGRDersPlan.BolKodAd + b.OGRDersGrup.OGRDersPlan.DersKod,
                    GorunenDersAd = b.OGRDersGrup.EnumDersGrupDil.Value == (int)Sabis.Enum.EnumDil.INGILIZCE ? b.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.YabDersAd : b.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.DersAd,
                    DersAd = b.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.DersAd,
                    DersYabAd = b.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.YabDersAd,
                    //EnumOgretimTur = grup.OGRDersPlan.EnumOgretimTur ?? grup.EnumOgretimTur, 
                    EnumOgretimTur = b.OGRDersGrup.EnumOgretimTur,
                    Donem = b.OGRDersGrup.EnumDonem.Value,
                    BolumKod = b.OGRDersGrup.OGRDersPlan.BolKodAd,
                    OgrenciSayisi = b.OGRDersGrup.OGROgrenciYazilma.Where(x => x.Iptal == false && x.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL && x.OGROgrenciNot.EnumOgrenciBasariNot != (int)EnumOgrenciBasariNot.W).Count(),
                    //OgrenciSayisiButunleme = grup.OGROgrenciYazilma.Where(x => x.Iptal == false && (x.ButunlemeMi.HasValue && x.ButunlemeMi.Value) && x.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL && x.OGROgrenciNot.EnumOgrenciBasariNot != (int)EnumOgrenciBasariNot.W).Count(),
                    GrupAd = b.OGRDersGrup.GrupAd == "?" ? "A" : b.OGRDersGrup.GrupAd,
                    DersGrupID = b.OGRDersGrup.ID,
                    Yil = b.OGRDersGrup.OgretimYili.Value,
                    Kredi = b.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.Kredi.HasValue ? b.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.Kredi.Value : 0,
                    ECTSKredi = b.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.ECTSKredi.HasValue ? b.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.ECTSKredi.Value : 0,
                    EnumDersPlanZorunlu = b.OGRDersGrup.OGRDersPlan.EnumDersPlanZorunlu,
                    FakulteID = b.OGRDersGrup.OGRDersPlan.FKFakulteBirimID,
                    ProgramID = b.OGRDersGrup.OGRDersPlan.FKBirimID,
                    DSaat = b.OGRDersGrup.OGRDersPlanAna.DSaat,
                    USaat = b.OGRDersGrup.OGRDersPlanAna.USaat,
                    YariYil = b.OGRDersGrup.OGRDersPlan.Yariyil,
                    EnumKokDersTipi = b.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.EnumKokDersTipi,
                    BolumAd = b.OGRDersGrup.Birimler.BirimAdi,
                    //ButunlemeMi = grup.OGROgrenciYazilma.Any(x => x.Iptal == false && (x.ButunlemeMi.HasValue && x.ButunlemeMi.Value == true) && x.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL),
                    EnumSeviye = b.OGRDersGrup.OGRDersPlan.EnumOgretimSeviye,
                    Seviye = b.OGRDersGrup.OGRDersPlan.EnumOgretimSeviye == (int)EnumOgretimSeviye.LISANS ? "Lisans" : (b.OGRDersGrup.OGRDersPlan.EnumOgretimSeviye == (int)EnumOgretimSeviye.YUKSEK_LISANS ? "Yüksek Lisans" : "Doktora" ),
                    //Hiyerarsik = grup.OGRDersPlan.OGRDersPlanAna.Hiyerarsik,
                    //KokDPAnaID = grup.OGRDersPlan.OGRDersPlanAna.FKHiyerarsikKokID,
                    //DersDegTempList = db.ABSDegerlendirme.Where(deg => deg.FKDersGrupID == grup.ID && deg.SilindiMi == false).OrderByDescending(x => x.ID).Select(deg => new DersDegTemp { ID = deg.ID, Butunleme = deg.ButunlemeMi, EnumSonHal = deg.EnumSonHal }).ToList(),
                    DersEtiketleri = b.OGRDersGrup.OGRDersPlan.OGRDersPlanEtiket.Select(a => a.OGRTNMDersEtiket.Etiket).ToList(),
                    DersSecimTipi = b.OGRDersGrup.OGRDersPlan.EnumDersSecimTipi == (int)EnumDersSecimTipi.ZORUNLU ? "Zorunlu" : "Seçmeli"
                    //Mufredatlar = db.OGRMufredatDersleri.Where(a => a.FKDersPlanAnaID == b.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.ID && birimler.Contains(a.OGRMufredat.FKProgramID)).OrderBy(a => a.FKMufredatID).Distinct().Select(a => a.OGRMufredat.MufredatAdi).ToList(),
                }).ToList();

                dersList.DersListesi = dersler;
                var personel = db.PersonelBilgisi.Find(personelID);
                dersList.AdSoyad = personel.Ad + " " + personel.Soyad;
                dersList.PersonelID = personelID;
                dersList.UnvanAd = personel.UnvanAd;
             
                return dersList;
            }
        }

        public static DtoHocaDersOgrenciBilgileri DersiAlanOgrenciListesiByGrupHocaID(int dersGrupHocaId)
        {
            DtoHocaDersOgrenciBilgileri dersiAlanOgrenciListesi = new DtoHocaDersOgrenciBilgileri();
            using (UYSv2Entities db = new UYSv2Entities())
            {
                OGRDersGrupHoca dgh = db.OGRDersGrupHoca.Where(a => a.ID==dersGrupHocaId).First();

                var personel = db.PersonelBilgisi.Find(dgh.FKPersonelID);
                dersiAlanOgrenciListesi.AdSoyad = personel.Ad + " " + personel.Soyad;
                dersiAlanOgrenciListesi.PersonelID = personel.ID;
                dersiAlanOgrenciListesi.Unvan = personel.UnvanAd;

                var dersGrup = db.OGRDersGrup.Find(dgh.FKDersGrupID);
                var dersPlan = dersGrup.OGRDersPlan;
                var dersPlanAna = dersGrup.OGRDersPlanAna;
                dersiAlanOgrenciListesi.DersKod = dersGrup.EnumDersGrupDil == (int)EnumDil.TURKCE ? dersPlan.BolKodAd + " " + dersPlan.DersKod : dersPlan.BolKodYabAd + " " + dersPlan.DersKod;
                dersiAlanOgrenciListesi.DersAd = dersGrup.EnumDersGrupDil == (int)EnumDil.TURKCE ? dersPlanAna.DersAd : dersPlanAna.YabDersAd;
                dersiAlanOgrenciListesi.GrupAd = dersGrup.GrupAd;
                dersiAlanOgrenciListesi.DersGrupID = dersGrup.ID;
                dersiAlanOgrenciListesi.DersGrupHocaID = dgh.ID;

                return dersiAlanOgrenciListesi;
            }
        }

        public static DtoHocaDersOgrenciBilgileri DersiAlanOgrenciListesi(int dersGrupID, int personelID)
        {
            DtoHocaDersOgrenciBilgileri dersiAlanOgrenciListesi = new DtoHocaDersOgrenciBilgileri();
            using (UYSv2Entities db = new UYSv2Entities())
            {
                List<Sabis.Bolum.Core.dto.Ogrenci.OgrenciBilgiDto> ogrenciListesi = db.OGROgrenciYazilma.Where(a => a.FKDersGrupID == dersGrupID && a.Iptal == false).Select(b => new Core.dto.Ogrenci.OgrenciBilgiDto()
                {
                    Ad = b.OGRKimlik.Kisi.Ad,
                    Soyad = b.OGRKimlik.Kisi.Soyad,
                    Numara = b.OGRKimlik.Numara,
                    ID = b.FKOgrenciID.Value,
                    KisiID = b.OGRKimlik.FKKisiID,
                    KullaniciAdi = b.OGRKimlik.Kullanici1.Count > 0 ? b.OGRKimlik.Kullanici1.FirstOrDefault().KullaniciAdi : " ",
                    BirimAdi = b.OGRKimlik.Birimler.BirimAdi

                }).ToList();

                foreach(var o in ogrenciListesi)
                {
                    o.Telefon = Sabis.Bolum.Bll.SOURCE.ABS.OGRENCI.OGRENCIMAIN.GetirOgrenciTelefon(o.KisiID);
                }

                OGRDersGrupHoca dgh = db.OGRDersGrupHoca.Where(a => a.FKDersGrupID == dersGrupID && a.FKPersonelID == personelID).First();

                dersiAlanOgrenciListesi.OgrenciListesi = ogrenciListesi;
                var personel = db.PersonelBilgisi.Find(personelID);
                dersiAlanOgrenciListesi.AdSoyad = personel.Ad + " " + personel.Soyad;
                dersiAlanOgrenciListesi.PersonelID = personelID;
                dersiAlanOgrenciListesi.Unvan = personel.UnvanAd;

                var dersGrup = db.OGRDersGrup.Find(dersGrupID);
                var dersPlan = dersGrup.OGRDersPlan;
                var dersPlanAna = dersGrup.OGRDersPlanAna;
                dersiAlanOgrenciListesi.DersKod = dersGrup.EnumDersGrupDil == (int)EnumDil.TURKCE ? dersPlan.BolKodAd + " " + dersPlan.DersKod : dersPlan.BolKodYabAd + " " + dersPlan.DersKod;
                dersiAlanOgrenciListesi.DersAd = dersGrup.EnumDersGrupDil == (int)EnumDil.TURKCE ? dersPlanAna.DersAd : dersPlanAna.YabDersAd;
                dersiAlanOgrenciListesi.GrupAd = dersGrup.GrupAd;
                dersiAlanOgrenciListesi.DersGrupID = dersGrup.ID;
                dersiAlanOgrenciListesi.DersGrupHocaID = dgh.ID;

                return dersiAlanOgrenciListesi;
            }
        }
    }
}
