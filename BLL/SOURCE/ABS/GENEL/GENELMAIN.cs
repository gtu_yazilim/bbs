﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Enum;


namespace Sabis.Bolum.Bll.SOURCE.ABS.GENEL
{
    public class GENELMAIN
    {
        public static bool SonHalVerildiMi(int FKDersGrupID, bool butunlemeMi)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return db.ABSDegerlendirme.Any(d => d.FKDersGrupID == FKDersGrupID && d.SilindiMi == false && d.EnumSonHal == (int)Sabis.Enum.EnumSonHal.EVET && d.ButunlemeMi == butunlemeMi);
            }
        }

        public static bool SonHalVerildiMiByDersPlanAnaID(int fkDersPlanAnaID, int yil, int donem)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return db.ABSDegerlendirme.Any(x => x.FKDersPlanAnaID == fkDersPlanAnaID && x.Yil == yil && x.EnumDonem == donem && x.SilindiMi == false && x.EnumSonHal == (int)EnumSonHal.EVET);
            }
        }
        public static bool DersGrupHocaMi(int FKDersGrupHocaID, int FKPersonelID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return db.OGRDersGrupHoca.Any(x => x.ID == FKDersGrupHocaID && x.FKPersonelID == FKPersonelID);
            }
        }
        public static EnumAkademikTarih AkademikTarihDonustur(EnumCalismaTip enumCalismaTipi)
        {
            EnumAkademikTarih akademikTarih;
            switch (enumCalismaTipi)
            {
                case EnumCalismaTip.AraSinav:
                    akademikTarih = EnumAkademikTarih.YIL_ICI;
                    break;
                case EnumCalismaTip.KisaSinav:
                    akademikTarih = EnumAkademikTarih.YIL_ICI;
                    break;
                case EnumCalismaTip.Odev:
                    akademikTarih = EnumAkademikTarih.YIL_ICI;
                    break;
                case EnumCalismaTip.SozluSinav:
                    akademikTarih = EnumAkademikTarih.YIL_ICI;
                    break;
                case EnumCalismaTip.ProjeTasarim:
                    akademikTarih = EnumAkademikTarih.YIL_ICI;
                    break;
                case EnumCalismaTip.PerformansGoreviUygulama:
                    akademikTarih = EnumAkademikTarih.YIL_ICI;
                    break;
                case EnumCalismaTip.PerformanGoreviLabaratuvar:
                    akademikTarih = EnumAkademikTarih.YIL_ICI;
                    break;
                case EnumCalismaTip.PerformansGoreviAtolye:
                    akademikTarih = EnumAkademikTarih.YIL_ICI;
                    break;
                case EnumCalismaTip.PerformansGoreviSeminer:
                    akademikTarih = EnumAkademikTarih.YIL_ICI;
                    break;
                case EnumCalismaTip.PerformansGoreviAraziCalismasi:
                    akademikTarih = EnumAkademikTarih.YIL_ICI;
                    break;
                case EnumCalismaTip.YilIciOrtalamasi:
                    akademikTarih = EnumAkademikTarih.YIL_ICI;
                    break;
                case EnumCalismaTip.YilIcininBasariya:
                    akademikTarih = EnumAkademikTarih.YIL_ICI;
                    break;
                case EnumCalismaTip.Final:
                    akademikTarih = EnumAkademikTarih.YIL_SONU;
                    break;
                case EnumCalismaTip.Butunleme:
                    akademikTarih = EnumAkademikTarih.BUTUNLEME_SINAVI;
                    break;
                case EnumCalismaTip.AltDersinYiliciKatkisi:
                    akademikTarih = EnumAkademikTarih.YIL_ICI;
                    break;
                case EnumCalismaTip.AltDersinFinalKatkisi:
                    akademikTarih = EnumAkademikTarih.FINAL_SINAVI;
                    break;
                default:
                    akademikTarih = EnumAkademikTarih.YIL_ICI;
                    break;

            }
            return akademikTarih;
        }
        public static bool KontrolIPYetki(string kullanciAdi, string IP)
        {
            bool result = false;
            using (UYSv2Entities db = new UYSv2Entities())
            {
                if (!string.IsNullOrEmpty(kullanciAdi) && !string.IsNullOrEmpty(IP))
                {
                    result = db.ABSIPYetki.Where(x => (x.IPAdresi == IP || x.KullaniciAdi == kullanciAdi) && x.YetkiVar).Any();
                }
            }
            return result;
        }
        public static List<IdariGorev> GetirIdariGorevList(int fkPersonelID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return db.IdariGorev.Where(x => x.FKPersonelBilgisiID == fkPersonelID && (x.FKGorevUnvaniID == 10 || x.FKGorevUnvaniID == 12 || x.FKGorevUnvaniID == 4) && x.BitisTarihi > DateTime.Now).ToList();
            }
        }
        public static List<Yetkililer> GetirYetkiList(int fkPersonelID, int yetkiGrupID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return db.Yetkililer.Where(x => x.FKPersonelID == fkPersonelID && x.YetkiGrupID == yetkiGrupID && x.YetkiBitis > DateTime.Now).ToList();
            }
        }
        public static List<Sabis.Bolum.Core.ABS.GENEL.BirimVM> BirimGetir(int ustBirimID, int? FKPersonelID, int? EnumBirimTuru, bool yetkiKontrol = true, int? yetkiGrupID = 3, int? EnumOgretimSeviye = null, int? enumOgretimTur = null, bool birimGrup = false)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var birim = db.Birimler.FirstOrDefault(x => x.ID == ustBirimID);
                bool fakulteMi = false;
                if (birim.EnumBirimTuru != (int)Sabis.Enum.EnumBirimTuru.Universite)
                {
                    fakulteMi = Sabis.Client.BirimIslem.Instance.getirBagliFakulte(birim.ID).EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.Fakulte;
                }
                //var kullaniciAdiGetir = db.PersonelBilgisi.FirstOrDefault(x => x.ID == FKPersonelID).KullaniciAdi;
                //var yetkiList = db.Yetkililer.Where(x => x.KullaniciAdi == kullaniciAdiGetir);
                var idariYetkiList = GetirIdariGorevList(FKPersonelID.Value);
                var yetkiList = GetirYetkiList(FKPersonelID.Value, yetkiGrupID.Value);
                //var idariYetkliList = db.IdariGorev.Where(x => x.FKPersonelBilgisiID == FKPersonelID && (x.FKGorevUnvaniID == 10 || x.FKGorevUnvaniID == 12 || x.FKGorevUnvaniID == 4) && x.BitisTarihi > DateTime.Now).ToList();
                //var yetkiList = db.Yetkililer.Where(x => x.FKPersonelID == FKPersonelID && x.YetkiGrupID == yetkiGrupID && x.YetkiBitis > DateTime.Now).ToList();
                var birimList = db.Birimler.Where(x => x.UstBirimID == ustBirimID && x.Aktif == true)
                    .Where(x => x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.Anabilimdali ||
                                x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.AnaSanatDali ||
                                x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.AraProgram ||
                                x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.BilimDali ||
                                x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.Bolum ||
                                x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.CAPLisansProg ||
                                x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.DGSLisansProg ||
                                x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.DoktoraAnaBilimDali ||
                                x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.DoktoraProgrami ||
                                x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.Enstitu ||
                                x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.Fakulte ||
                                x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.LisansProgrami ||
                                x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.MeslekYuksekOkulu ||
                                x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.OnLisansProgrami ||
                                x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.Program ||
                                x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.SanatDali ||
                                x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.SanattaYeterlilikAnaBilimDali ||
                                x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.TezsizYLisansProg ||
                                x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.Universite ||
                                x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.VakifMeslekYuksekOkulu ||
                                x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.YandalLisansProg ||
                                x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.YDilHazirlikProg ||
                                x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.YuksekLisansAnaBilimDali ||
                                x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.YuksekLisansProgrami ||
                                x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.YuksekOkul
                    )
                    .Select(x => new Sabis.Bolum.Core.ABS.GENEL.BirimVM
                    {
                        ID = x.ID,
                        Ad = x.BirimAdi,
                        EnumBirimTuru = x.EnumBirimTuru,
                        BirimAdiUzun = x.BirimUzunAdi,
                        UstBirimID = x.UstBirimID,
                        EnumOgretimTur = x.EnumOgrenimTuru
                    }).ToList();

                if (EnumBirimTuru.HasValue)
                {
                    if (EnumBirimTuru == 0)
                    {
                        birimList = birimList.Where(x => x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.Fakulte || x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.Enstitu || x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.YuksekOkul || x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.MeslekYuksekOkulu).ToList();
                    }
                    else
                    {
                        birimList = birimList.Where(x => x.EnumBirimTuru == EnumBirimTuru).ToList();
                    }
                }

                if (enumOgretimTur.HasValue)
                {
                    birimList = birimList.Where(x => x.EnumOgretimTur == enumOgretimTur).ToList();
                }

                bool araProgramVarMi = birimList.Any(x => x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.AraProgram);

                if (fakulteMi && birim.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.Bolum && birimGrup && !araProgramVarMi)
                {
                    birimList.Clear();
                }

                if (yetkiKontrol && birimList.Any())
                {
                    if ((yetkiList.Any(x => x.YetkiGrupID == 2) || idariYetkiList.Any(x=> x.FKGorevUnvaniID == 4)) && birimList.FirstOrDefault().EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.Fakulte)
                    {
                        birimList = birimList.Where(x => yetkiList.Select(y => y.FKFakulteBirimID).Contains(x.ID) ||
                                                    yetkiList.Select(y => y.FKBolumBirimID).Contains(x.ID) ||
                                                    yetkiList.Select(y => y.FKProgramBirimID).Contains(x.ID) ||
                                                    yetkiList.Select(y => y.FKAnaBilimDaliID).Contains(x.ID) ||
                                                    idariYetkiList.Select(y => y.FKFakulteID).Contains(x.ID) ||
                                                    idariYetkiList.Select(y => y.FKBolumID).Contains(x.ID) ||
                                                    idariYetkiList.Select(y => y.BirimID).Contains(x.ID) ||
                                                    idariYetkiList.Select(y => y.BirimID).Contains(x.UstBirimID)
                                                    ).ToList();
                    }
                    else if ((yetkiList.Any(x => x.YetkiGrupID == 2) || idariYetkiList.Any(x => x.FKGorevUnvaniID == 4)) && birimList.FirstOrDefault().EnumBirimTuru != (int)Sabis.Enum.EnumBirimTuru.Bolum)
                    {
                        birimList = birimList.Where(x => yetkiList.Select(y => y.FKFakulteBirimID).Contains(x.ID) ||
                                                    yetkiList.Select(y => y.FKBolumBirimID).Contains(x.ID) ||
                                                    yetkiList.Select(y => y.FKProgramBirimID).Contains(x.ID) ||
                                                    yetkiList.Select(y => y.FKAnaBilimDaliID).Contains(x.ID) ||
                                                    idariYetkiList.Select(y => y.FKFakulteID).Contains(x.ID) ||
                                                    idariYetkiList.Select(y => y.FKBolumID).Contains(x.ID) ||
                                                    idariYetkiList.Select(y => y.BirimID).Contains(x.ID) ||
                                                    idariYetkiList.Select(y => y.BirimID).Contains(x.UstBirimID)
                                                    ).ToList();
                    }

                    if (!yetkiList.Any(x => x.YetkiGrupID == 2) || idariYetkiList.Any(x => x.FKGorevUnvaniID == 4))
                    {
                        var xx = yetkiList;
                        var aa = birimList;
                        birimList = birimList.Where(x => yetkiList.Select(y => y.FKFakulteBirimID).Contains(x.ID) ||
                                                    yetkiList.Select(y => y.FKBolumBirimID).Contains(x.ID) ||
                                                    yetkiList.Select(y => y.FKProgramBirimID).Contains(x.ID) ||
                                                    yetkiList.Select(y => y.FKAnaBilimDaliID).Contains(x.ID) ||
                                                    idariYetkiList.Select(y => y.FKFakulteID).Contains(x.ID) ||
                                                    idariYetkiList.Select(y => y.FKBolumID).Contains(x.ID) ||
                                                    idariYetkiList.Select(y => y.BirimID).Contains(x.ID) ||
                                                    idariYetkiList.Select(y => y.BirimID).Contains(x.UstBirimID)
                                                    ).ToList();
                        var test = birimList;
                    }

                    //switch (EnumBirimTuru)
                    //{
                    //    case 0:
                    //        birimList = birimList.Where(x => yetkiList.Select(y => y.FKFakulteBirimID).Contains(x.ID) || idariYetkliList.Select(y => y.FKFakulteID).Contains(x.ID)).ToList();
                    //        break;
                    //    case (int)Sabis.Enum.EnumBirimTuru.Fakulte:
                    //        birimList = birimList.Where(x => yetkiList.Select(y => y.FKFakulteBirimID).Contains(x.ID) || idariYetkliList.Select(y=>y.FKFakulteID).Contains(x.ID)).ToList();
                    //        break;
                    //    case (int)Sabis.Enum.EnumBirimTuru.Bolum:
                    //        birimList = birimList.Where(x => yetkiList.Select(y => y.FKBolumBirimID).Contains(x.ID) || idariYetkliList.Select(y=>y.FKBolumID).Contains(x.ID)).ToList();
                    //        break;
                    //    case (int)Sabis.Enum.EnumBirimTuru.Program:
                    //        birimList = birimList.Where(x => yetkiList.Select(y => y.FKProgramBirimID).Contains(x.ID) || idariYetkliList.Select(y=>y.BirimID).Contains(x.ID)).ToList();
                    //        break;
                    //    default:
                    //        break;

                    //}
                }

                return birimList.OrderBy(x => x.Ad).ToList();
            }
        }
        public static int GrupIDToHocaID(int dersGrupID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return db.OGRDersGrupHoca.FirstOrDefault(x => x.FKDersGrupID == dersGrupID).ID;
            }
        }
    }
}
