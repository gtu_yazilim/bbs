﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sabis.Bolum.Core.ABS.ANKET;
using Sabis.Enum;
using Sabis.Bolum.Bll.DATA.UysModel;

namespace Sabis.Bolum.Bll.SOURCE.ABS.ANKET
{
    public class AnketDegerlendirme
    {
        public static void UzemDokumanAktar()
        {

        }

        //TabloYok
        //public static GenelDegerlendirme GenelDegerlendirmeSonuc(int yil, int donem)
        //{
        //    GenelDegerlendirme result = new GenelDegerlendirme();

        //    result.BirimSonucList = new Dictionary<int, BirimSonuc>();

        //    var sonuc = GenelDegerlendirmeSonuc2(yil, donem);
        //    foreach (var item in sonuc)
        //    {
        //        DegerlendirmeSonuc personelBilgi;
        //        var fakulte = Sabis.Client.BirimIslem.Instance.getirBagliFakulte(item.BirimID);
        //        if (fakulte == null)
        //        {
        //            fakulte = Sabis.Client.BirimIslem.Instance.getirUstBirim(item.BirimID);
        //        }

        //        BirimSonuc fakulteSonuc;
        //        if (!result.BirimSonucList.TryGetValue(fakulte.ID, out fakulteSonuc))
        //        {
        //            fakulteSonuc = new BirimSonuc();
        //            fakulteSonuc.BirimID = fakulte.ID;
        //            fakulteSonuc.BirimAd = fakulte.BirimAdi;
        //            fakulteSonuc.BirimSonucList = new Dictionary<int, BirimSonuc>();
        //            result.BirimSonucList.Add(fakulte.ID, fakulteSonuc);
        //        }
        //        BirimSonuc birimSonuc;
        //        if (!fakulteSonuc.BirimSonucList.TryGetValue(item.BirimID, out birimSonuc))
        //        {
        //            birimSonuc = new BirimSonuc();
        //            birimSonuc.BirimID = item.BirimID;
        //            birimSonuc.BirimAd = item.BirimAd;
        //            birimSonuc.PersonelList = new List<DegerlendirmeSonuc>();
        //            fakulteSonuc.BirimSonucList.Add(item.BirimID, birimSonuc);
        //        }

        //        birimSonuc.PersonelList.Add(item);
        //    }

        //    foreach (var fakulteItem in result.BirimSonucList)
        //    {
        //        foreach (var bolumItem in fakulteItem.Value.BirimSonucList)
        //        {
        //            bolumItem.Value.PersonelList = bolumItem.Value.PersonelList.OrderByDescending(x => x.GenelToplam).ToList();
        //        }
        //    }

        //    result.BirimSonucList = result.BirimSonucList.OrderBy(x => x.Value.BirimAd).ToDictionary(x => x.Key, x => x.Value);


        //    return result;
        //}

        //TabloYok
        //public static DegerlendirmeSonuc GetirDegerlendirmeSonuc(int yil, int donem, int personelID)
        //{
        //    DegerlendirmeSonuc result = new DegerlendirmeSonuc();

        //    DATA.UysModel.UYSv2Entities db = new DATA.UysModel.UYSv2Entities();

        //    var sorgu = db.PersonelBilgisi.AsNoTracking().Where(x => x.ID == personelID && x.FK_GorevBolID.HasValue).Select(x => new { Ad = x.Kisi.Ad, Soyad = x.Kisi.Soyad, Unvan = x.UnvanAd, ID = x.ID, GorevBolumID = x.FK_GorevBolID.Value, KullaniciAdi = x.Kullanici1.FirstOrDefault().KullaniciAdi }).AsQueryable();

        //    var personel = sorgu.FirstOrDefault();

        //    if (personel == null)
        //    {
        //        throw new Exception("Personel bulunamadı!");
        //    }

        //    result.AdSoyadUnvan = string.Format("{0} {1} {2}", personel.Unvan, personel.Ad, personel.Soyad);
        //    result.BirimAd = Sabis.Client.BirimIslem.Instance.BirimDict[personel.GorevBolumID].BirimAdi;
        //    result.PersonelID = personel.ID;
        //    result.KullaniciAdi = personel.KullaniciAdi;
        //    result.DersDegerlendirmeList = GetirDersDegerlendirmeList(ref db, yil, donem, personelID);
        //    result.PersonelDegerlendirme = GetirPersonelDegerlendirme(ref db, yil, donem, personelID);
        //    result.GenelToplam = result.DersDegerlendirmeList.Any() ? ((0.5) * result.DersDegerlendirmeList.Average(x => x.SoruCevapToplam)) + ((0.3) * result.DersDegerlendirmeList.Average(x => x.DokumanToplam)) + (0.2) * result.PersonelDegerlendirme.GenelToplam : 0;
        //    db.Dispose();
        //    db = null;
        //    return result;
        //}

        //TabloYok
        //private static PersonelDegerlendirme GetirPersonelDegerlendirme(ref DATA.UysModel.UYSv2Entities db, int yil, int donem, int personelID)
        //{
        //    PersonelDegerlendirme result = new PersonelDegerlendirme();

        //    var sorgu = db.OgretimUyesiCevap.AsNoTracking().Where(x => x.Yil == yil && x.Donem == donem && x.Puan.HasValue && x.HedefPersonelID == personelID)
        //        .GroupBy(x => x.SoruID)
        //        .ToList();

        //    result.SoruCevapList = new List<SoruDegerlendirme>();
        //    foreach (var item in sorgu)
        //    {
        //        SoruDegerlendirme soru = new SoruDegerlendirme();
        //        soru.SoruNo = item.Key;
        //        soru.SoruMetin = lazyPersonelSoruDict.Value[item.Key];
        //        soru.Puan = item.Average(y => y.Puan.Value * 1.0) * 1.0 / 0.2;
        //        soru.Adet = item.Count();
        //        result.SoruCevapList.Add(soru);
        //    }
        //    result.GenelToplam = result.SoruCevapList.Sum(x => x.Puan);
        //    return result;
        //}

            //TabloYok
        //private static List<DersOgrenciDegerlendirme> GetirDersDegerlendirmeList(ref DATA.UysModel.UYSv2Entities db, int yil, int donem, int personelID)
        //{
        //    List<DersOgrenciDegerlendirme> result = new List<DersOgrenciDegerlendirme>();

        //    var sorguOgrenciDers = db.OgrenciDersCevap.AsNoTracking()
        //        .Where(x => x.OGRDersGrup.OgretimYili == yil && x.OGRDersGrup.EnumDonem == donem && x.FKPersonelID == personelID &&
        //         (x.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.EnumKokDersTipi == (int)EnumKokDersTipi.NORMAL) && (x.OGRDersGrup.Birimler.EnumSeviye == (int)EnumOgretimSeviye.LISANS || x.OGRDersGrup.Birimler.EnumSeviye == (int)EnumOgretimSeviye.ONLISANS))
        //        .GroupBy(x => new { FKDersPlanAnaID = x.OGRDersGrup.OGRDersPlan.FKDersPlanAnaID.Value, DersAd = x.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.DersAd, FKPersonelID = x.FKPersonelID, SoruID = x.SoruID })
        //        .Select(x => new { x.Key.FKPersonelID, x.Key.FKDersPlanAnaID, x.Key.DersAd, x.Key.SoruID, Puan = x.Average(y => y.Puan * 1.0) * 1.0 / 0.4, Adet = x.Count() })
        //        .GroupBy(x => new { x.FKPersonelID, x.FKDersPlanAnaID, x.DersAd }).ToList();

        //    foreach (var dersItem in sorguOgrenciDers)
        //    {
        //        var ders = result.FirstOrDefault(x => x.DersPlanAnaID == dersItem.Key.FKDersPlanAnaID);
        //        bool yeniMi = false;
        //        if (ders == null)
        //        {
        //            ders = new DersOgrenciDegerlendirme();
        //            ders.DersPlanAnaID = dersItem.Key.FKDersPlanAnaID;
        //            ders.DersAd = dersItem.Key.DersAd;
        //            yeniMi = true;
        //            ders.DokumanList = new List<DersDokuman>();
        //        }
        //        ders.SoruCevapList = new List<SoruDegerlendirme>();
        //        foreach (var soruItem in dersItem)
        //        {
        //            SoruDegerlendirme soru = new SoruDegerlendirme();
        //            soru.Adet = soruItem.Adet;
        //            soru.Puan = Math.Round(soruItem.Puan, 2, MidpointRounding.AwayFromZero);
        //            soru.SoruNo = soruItem.SoruID;
        //            soru.SoruMetin = lazyOgrenciSoruDict.Value[soru.SoruNo];
        //            ders.SoruCevapList.Add(soru);
        //        }
        //        ders.SoruCevapList = ders.SoruCevapList.OrderBy(x => x.SoruNo).ToList();
        //        if (yeniMi)
        //        {
        //            result.Add(ders);
        //        }
        //    }

        //    var dokumanSorgu = db.DersIcerikPuani.AsNoTracking()
        //        .Where(x => x.FKPersonelID == personelID && x.OGRDersGrup.OgretimYili == yil && x.OGRDersGrup.EnumDonem == donem && x.OGRDersGrup.OGRDersPlan.FKDersPlanAnaID.HasValue &&
        //         (x.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.EnumKokDersTipi == (int)EnumKokDersTipi.NORMAL) && (x.OGRDersGrup.Birimler.EnumSeviye == (int)EnumOgretimSeviye.LISANS || x.OGRDersGrup.Birimler.EnumSeviye == (int)EnumOgretimSeviye.ONLISANS))
        //        .GroupBy(x => new { FKDersPlanAnaID = x.OGRDersGrup.OGRDersPlan.FKDersPlanAnaID.Value, DersAd = x.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.DersAd }).ToList();
        //    foreach (var dersItem in dokumanSorgu)
        //    {
        //        var ders = result.FirstOrDefault(x => x.DersPlanAnaID == dersItem.Key.FKDersPlanAnaID);
        //        bool yeniMi = false;
        //        if (ders == null)
        //        {
        //            ders = new DersOgrenciDegerlendirme();
        //            ders.DersPlanAnaID = dersItem.Key.FKDersPlanAnaID;
        //            ders.DersAd = dersItem.Key.DersAd;
        //            yeniMi = true;
        //            ders.SoruCevapList = new List<SoruDegerlendirme>();
        //        }
        //        ders.DokumanList = new List<DersDokuman>();
        //        foreach (var dokumanItem in dersItem)
        //        {
        //            DersDokuman dokuman = new DersDokuman();
        //            dokuman.Adet = dokumanItem.Adet;
        //            dokuman.Puan = dokumanItem.Puan;
        //            dokuman.DokumanTipi = ""/*dokumanItem.SoruID*/;
        //            ders.DokumanList.Add(dokuman);
        //        }
        //        if (yeniMi)
        //        {
        //            result.Add(ders);
        //        }
        //    }

        //    foreach (var item in result)
        //    {
        //        double dokumanPuan = item.DokumanList.Sum(x => x.Puan);
        //        dokumanPuan = dokumanPuan > 100 ? 100 : dokumanPuan;
        //        item.DokumanToplam = dokumanPuan;
        //        item.SoruCevapToplam = item.SoruCevapList.Sum(x => x.Puan);
        //    }

        //    result = result.OrderBy(x => x.DersAd).ToList();
        //    return result;
        //}

        //TabloYok
        //private static Dictionary<int, Tuple<double, IEnumerable<int>>> GetirDersDegerlendirmeList(ref DATA.UysModel.UYSv2Entities db, int yil, int donem)
        //{
        //    var onsorgu = (from x in db.OgrenciDersCevap.AsNoTracking().Where(x => x.OGRDersGrup.OgretimYili == yil && x.OGRDersGrup.EnumDonem == donem &&
        //        (x.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.EnumKokDersTipi == (int)EnumKokDersTipi.NORMAL) && (x.OGRDersGrup.Birimler.EnumSeviye == (int)EnumOgretimSeviye.LISANS || x.OGRDersGrup.Birimler.EnumSeviye == (int)EnumOgretimSeviye.ONLISANS)
        //        )//.OrderBy(x => x.OGRDersGrup.Birimler.BirimAdi)
        //                   join dg in db.OGRDersGrup on x.FKDersGrupID equals dg.ID
        //                   join dp in db.OGRDersPlan on dg.FKDersPlanID equals dp.ID
        //                   select new { FKDersPlanAnaID = dp.FKDersPlanAnaID.Value, FKPersonelID = x.FKPersonelID, SoruID = x.SoruID, Puan = x.Puan }).AsQueryable();



        //    var sorgu = onsorgu

        //        .GroupBy(x => new { FKDersPlanAnaID = x.FKDersPlanAnaID, FKPersonelID = x.FKPersonelID, SoruID = x.SoruID })
        //        .Select(x => new { x.Key.FKPersonelID, x.Key.FKDersPlanAnaID, x.Key.SoruID, Puan = x.Average(y => y.Puan * 1.0) * 1.0 / 0.4, Adet = x.Count() })
        //        .GroupBy(x => new { x.FKPersonelID, x.FKDersPlanAnaID })
        //        .Select(x => new { x.Key.FKPersonelID, x.Key.FKDersPlanAnaID, Toplam = x.Sum(y => y.Puan) })
        //        .GroupBy(x => new { x.FKPersonelID })
        //        .Select(x => new { x.Key.FKPersonelID, Toplam = x.Sum(y => y.Toplam * 1.0), DPAnaList = x.Select(y => y.FKDersPlanAnaID) }).AsQueryable();

        //    var result = sorgu.ToDictionary(x => x.FKPersonelID, y => new Tuple<double, IEnumerable<int>>(y.Toplam, y.DPAnaList));

        //    return result;
        //}

        private static Dictionary<int, Tuple<double, IEnumerable<int>>> GetirDokumanDegerlendirmeList(ref UYSv2Entities db, int yil, int donem)
        {
            Dictionary<int, Tuple<double, IEnumerable<int>>> result;
            if (yil == 2015 || (yil == 2016 && donem == 1))
            {
                var sorgu = db.DersIcerikPuani.AsNoTracking().Where(x => x.OGRDersGrup.OgretimYili == yil && x.OGRDersGrup.EnumDonem == donem && x.OGRDersGrup.OGRDersPlan.FKDersPlanAnaID.HasValue && (x.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.EnumKokDersTipi == (int)EnumKokDersTipi.NORMAL) && (x.OGRDersGrup.Birimler.EnumSeviye == (int)EnumOgretimSeviye.LISANS || x.OGRDersGrup.Birimler.EnumSeviye == (int)EnumOgretimSeviye.ONLISANS))
                   .OrderBy(x => x.OGRDersGrup.Birimler.BirimAdi)
                   .GroupBy(x => new { FKPersonelID = x.FKPersonelID, FKDersPlanAnaID = x.OGRDersGrup.OGRDersPlan.FKDersPlanAnaID.Value })
                   .Select(x => new { FKPersonelID = x.Key.FKPersonelID, FKDersPlanAnaID = x.Key.FKDersPlanAnaID, Toplam = x.Sum(y => y.Puan) })
                   .Select(x => new { x.FKPersonelID, x.FKDersPlanAnaID, Toplam = x.Toplam > 100 ? 100 : x.Toplam })
                   .GroupBy(x => x.FKPersonelID)
                   .Select(x => new { FKPersonelID = x.Key, Toplam = x.Sum(y => y.Toplam * 1.0), DPAnaList = x.Select(y => y.FKDersPlanAnaID) })
                   .AsQueryable();
                result = sorgu.ToDictionary(x => x.FKPersonelID, y => new Tuple<double, IEnumerable<int>>(y.Toplam, y.DPAnaList));
            }
            else
            {
                var sorgu = db.ABSDokumanGrup.Where(x => !x.Silindi && x.OGRDersGrup.OGRDersPlanAna.EnumKokDersTipi == (int)EnumKokDersTipi.NORMAL && (x.OGRDersGrup.Birimler.EnumSeviye == (int)EnumOgretimSeviye.LISANS || x.OGRDersGrup.Birimler.EnumSeviye == (int)EnumOgretimSeviye.ONLISANS))
           .GroupBy(x => new { FKPersonelID = x.ABSDokuman.FKPersonelID, FKDersPlanAnaID = x.ABSDokuman.FKDersPlanAnaID })
           .Select(x => new { FKPersonelID = x.Key.FKPersonelID, FKDersPlanAnaID = x.Key.FKDersPlanAnaID, Toplam = x.Count() * 5 })
           .Select(x => new { x.FKPersonelID, x.FKDersPlanAnaID, Toplam = x.Toplam > 100 ? 100 : x.Toplam })
              .GroupBy(x => x.FKPersonelID)
              .Select(x => new { FKPersonelID = x.Key, Toplam = x.Sum(y => y.Toplam * 1.0), DPAnaList = x.Select(y => y.FKDersPlanAnaID) })
              .AsQueryable();
                result = sorgu.ToDictionary(x => x.FKPersonelID, y => new Tuple<double, IEnumerable<int>>(y.Toplam, y.DPAnaList));
            }

            return result;
        }

        //TabloYok
        //private static Dictionary<int, double> GetirPersonelDegerlendirmeList(ref UYSv2Entities db, int yil, int donem)
        //{
        //    var sorgu = db.OgretimUyesiCevap.AsNoTracking().Where(x => x.Yil == yil && x.Donem == donem && x.Puan.HasValue)
        //        .OrderBy(x => x.PersonelBilgisi.Ad)
        //        .GroupBy(x => new { PersonelID = x.HedefPersonelID, SoruID = x.SoruID })
        //        .Select(x => new { x.Key.PersonelID, x.Key.SoruID, Puan = x.Average(y => y.Puan * 1.0) * 1.0 / 0.2 })
        //        .GroupBy(x => x.PersonelID)
        //        .Select(x => new { PersonelID = x.Key, Toplam = x.Sum(y => y.Puan.Value) }).AsQueryable();

        //    var result = sorgu.ToDictionary(x => x.PersonelID, y => y.Toplam);

        //    return result;
        //}

        #region SoruDict
        private static readonly Lazy<Dictionary<int, string>> lazyPersonelSoruDict = new Lazy<Dictionary<int, string>>(() => PersonelSoruDictDoldur());

        private static Dictionary<int, string> PersonelSoruDictDoldur()
        {
            Dictionary<int, string> result = new Dictionary<int, string>();
            result.Add(1, "Öğretim üyeleri ile iletişimi");
            result.Add(2, "Bölüm toplantılarına devamlılığı (mazeret bildirimi hariç)");
            result.Add(3, "Karşılaşılan sorunlarda yapıcı katkısı");
            result.Add(4, "Verilen görevleri zamanında tamamlama");
            result.Add(5, "Erişilebilirlik (Ders, toplantı, görevlendirme vb. dışında)");
            return result;
        }

        private static readonly Lazy<Dictionary<int, string>> lazyOgrenciSoruDict = new Lazy<Dictionary<int, string>>(() => OgrenciSoruDictDoldur());

        private static Dictionary<int, string> OgrenciSoruDictDoldur()
        {
            Dictionary<int, string> result = new Dictionary<int, string>();
            result.Add(1, "Öğretim elemanının derse düzenli ve zamanında gelmesi");
            result.Add(2, "Öğretim elemanının derse hazırlıklı gelmesi");
            result.Add(3, "Öğretim elemanının, dersin işlenişinde ve dersle ilişkili sorulara cevap vermede yetkinliği");
            result.Add(4, "Öğretim elemanının, derste farklı düşüncelere ve yorumlara yer vererek derse katılımı özendirmesi");
            result.Add(5, "Öğretim elemanının öğrencilerle iletişimi");
            result.Add(6, "Öğretim elemanının ders sürecinde öğretim teknolojilerini (projeksiyon, görsel materyal vb.) etkin kullanımı");
            result.Add(7, "Öğretim elemanının, ders süresini etkin kullanımı");
            result.Add(8, "Öğretim elemanının hazırladığı ödevlerin/sınavların, ders içeriğine uygunluğu");
            result.Add(9, "Öğretim elemanının, ödevleri/sınavları objektif bir şekilde değerlendirmesi");
            result.Add(10, "Keşke bu öğretim elemanından bir ders daha alabilsem");
            return result;
        }
        #endregion

        //TabloYok
        //public static List<DegerlendirmeSonuc> GenelDegerlendirmeSonuc2(int yil, int donem)
        //{
        //    List<DegerlendirmeSonuc> result = new List<DegerlendirmeSonuc>();
        //    DATA.UysModel.UYSv2Entities db = new DATA.UysModel.UYSv2Entities();
        //    List<int> personelKadroFiltre = new List<int> { 1, 2, 3 };
        //    var dokuman = GetirDokumanDegerlendirmeList(ref db, yil, donem);//.Select(x => new { PersonelID = x.Key, Puan = x.Value * 0.3 });
        //    var ders = GetirDersDegerlendirmeList(ref db, yil, donem);//.Select(x => new { PersonelID = x.Key, Puan = x.Value * 0.5 });
        //    var personel = GetirPersonelDegerlendirmeList(ref db, yil, donem);

        //    Dictionary<int, int> personelDersAdetDict = new Dictionary<int, int>();

        //    var tumPersonelList = dokuman.Select(x => x.Key).ToList();
        //    tumPersonelList.AddRange(dokuman.Select(x => x.Key));
        //    tumPersonelList.AddRange(personel.Select(x => x.Key));
        //    tumPersonelList.AddRange(ders.Select(x => x.Key));
        //    tumPersonelList = tumPersonelList.Distinct().ToList();

        //    foreach (var personelID in tumPersonelList)
        //    {
        //        Tuple<double, IEnumerable<int>> dokumanTuple;
        //        dokuman.TryGetValue(personelID, out dokumanTuple);

        //        Tuple<double, IEnumerable<int>> dersTuple;
        //        ders.TryGetValue(personelID, out dersTuple);

        //        List<int> dersList = new List<int>();
        //        if (dersTuple != null)
        //        {
        //            dersList.AddRange(dersTuple.Item2.ToList());
        //        }
        //        if (dokumanTuple != null)
        //        {
        //            dersList.AddRange(dokumanTuple.Item2.ToList());
        //        }
        //        dersList = dersList.Distinct().ToList();
        //        personelDersAdetDict.Add(personelID, dersList.Count);
        //    }

        //    List<DegerlendirmeSonuc> sonuc = new List<DegerlendirmeSonuc>();
        //    foreach (var item in tumPersonelList)
        //    {
        //        var personelID = item;
        //        var dersAdet = 0;
        //        personelDersAdetDict.TryGetValue(personelID, out dersAdet);
        //        Tuple<double, IEnumerable<int>> dersTuple;
        //        double puan = 0;
        //        if (dokuman.TryGetValue(personelID, out dersTuple))
        //        {
        //            puan += (dersTuple.Item1 / (double)dersAdet) * (0.3);
        //        }

        //        if (ders.TryGetValue(personelID, out dersTuple))
        //        {
        //            puan += (dersTuple.Item1 / (double)dersAdet) * (0.5);
        //        }

        //        double pPuan = 0;
        //        if (personel.TryGetValue(personelID, out pPuan))
        //        {
        //            puan += pPuan * 0.2;
        //        }
        //        sonuc.Add(new DegerlendirmeSonuc() { PersonelID = personelID, GenelToplam = puan });
        //    }

        //    //var sonuc = dokuman.Union(ders).Union(personel).GroupBy(x => x.PersonelID).Select(x => new DegerlendirmeSonuc { PersonelID = x.Key, GenelToplam = x.Sum(y => y.Puan) });
        //    var personelDict = db.PersonelBilgisi.Where(x => x.FK_GorevBolID.HasValue && x.FKAkademikUnvanID.HasValue && x.EnumPersonelDurum == (int)EnumPersonelDurum.AKTIF).Select(x => new DegerlendirmeSonuc { PersonelID = x.ID, AdSoyadUnvan = x.UnvanAd + " " + x.Kisi.Ad + " " + x.Kisi.Soyad, BirimID = x.FK_GorevBolID.Value, KullaniciAdi = x.Kullanici1.FirstOrDefault().KullaniciAdi, FKAkademikUnvanID = x.FKAkademikUnvanID }).ToDictionary(x => x.PersonelID, y => y);

        //    foreach (var item in sonuc)
        //    {
        //        DegerlendirmeSonuc personelBilgi;
        //        if (personelDict.TryGetValue(item.PersonelID, out personelBilgi))
        //        {
        //            if (!personelKadroFiltre.Contains(personelBilgi.FKAkademikUnvanID.Value))
        //            {
        //                continue;
        //            }

        //            item.GenelToplam = Math.Round(item.GenelToplam, 2, MidpointRounding.AwayFromZero);
        //            item.AdSoyadUnvan = personelBilgi.AdSoyadUnvan;
        //            item.BirimID = personelBilgi.BirimID;
        //            item.KullaniciAdi = personelBilgi.KullaniciAdi;
        //            var birim = Sabis.Client.BirimIslem.Instance.BirimDict[personelBilgi.BirimID];
        //            item.BirimAd = birim.BirimAdi;

        //            result.Add(item);
        //        }
        //    }

        //    db.Dispose();
        //    db = null;
        //    return result;
        //}
    }
}
