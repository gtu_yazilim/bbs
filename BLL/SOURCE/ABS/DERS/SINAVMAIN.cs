﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Bolum.Core.ABS.DERS.SINAV;
using Sabis.Enum;
using Sabis.Bolum.Core.ENUM.ABS;
using EntityFramework.BulkInsert.Extensions;
using Sabis.Bolum.Core.ABS.DERS.OSINAV;
using Sabis.Bolum.Core.dto.Ogrenci;
using System.Reflection;
using System.ComponentModel;
using AutoMapper;
using Sabis.Bolum.Bll.DATA;

namespace Sabis.Bolum.Bll.SOURCE.ABS.DERS
{
    public class SINAVMAIN
    {
        public static List<PaylarOlcmeVM> GetirPaySinavSoruListesi(int FKDersGrupHocaID, int FKPaylarID, int Yil, int Donem, int enumDil)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var sonuc = (from soru in db.ABSPaylarOlcme
                             join onlinesoru in db.OSinavSoru on soru.FKOSinavSoruID equals onlinesoru.SoruID into tbl
                             from onlinesoru in tbl.DefaultIfEmpty()
                                 //where soru.FKPaylarID == FKPaylarID && soru.FKABSOrtakDegerlendirmeID == fkOrtakDegerlendirmeID && soru.Silindi == false || (soru.Silindi == false && onlinesoru.Silindi == false)
                                 //where soru.FKPaylarID == FKPaylarID && soru.FKABSOrtakDegerlendirmeID == fkOrtakDegerlendirmeID && soru.Silindi == false && (onlinesoru.Silindi == false || onlinesoru.Silindi == null)
                             where soru.FKPaylarID == FKPaylarID && soru.Silindi == false
                             select new PaylarOlcmeVM
                             {

                                 ID = soru.ID,
                                 FKPaylarID = soru.FKPaylarID,
                                 SoruNo = soru.SoruNo,
                                 SoruPuan = soru.SoruPuan,
                                 FKKoordinatorID = soru.FKKoordinatorID,
                                 Zaman = soru.Zaman,
                                 EnumSinavTuru = soru.EnumSinavTuru,
                                 EnumSoruTipi = soru.EnumSoruTipi,
                                 ProgramCiktilari = (from pcikti in db.ABSPaylarOlcmeProgram
                                                     join cikti in db.BolumYeterlilik on pcikti.FKProgramCiktiID equals cikti.ID
                                                     join dcikti in db.DilBolumYeterlilik on cikti.ID equals dcikti.FKBolumYeterlilikID
                                                     where pcikti.FKPaylarOlcmeID == soru.ID && dcikti.EnumDilID == enumDil
                                                     select new PaylarOlcmeProgramCiktilariVM
                                                     {
                                                         ID = pcikti.ID,
                                                         FKPaylarOlcmeID = pcikti.FKPaylarOlcmeID,
                                                         Sira = cikti.YetID,
                                                         FKProgramCiktiID = pcikti.FKProgramCiktiID,
                                                         Icerik = dcikti.Icerik
                                                     }).OrderBy(x => x.Sira).ToList(),
                                 OgrenmeCiktilari = (from pcikti in db.ABSPaylarOlcmeOgrenme
                                                     join cikti in db.DersCikti on pcikti.FKOgrenmeCiktiID equals cikti.ID
                                                     where pcikti.FKPaylarOlcmeID == soru.ID
                                                     select new PaylarOlcmeOgrenmeCiktilariVM
                                                     {
                                                         ID = pcikti.ID,
                                                         FKPaylarOlcmeID = pcikti.FKPaylarOlcmeID,
                                                         Sira = cikti.CiktiNo,
                                                         FKOgrenmeCiktiID = pcikti.FKOgrenmeCiktiID,
                                                         Icerik = db.DilDersCikti.FirstOrDefault(d => d.FKDersCiktiID == cikti.ID && d.EnumDilID == enumDil).Icerik
                                                     }).OrderBy(x => x.Sira).ToList()
                             ,
                                 OSinavSoruID = onlinesoru == null ? 0 : onlinesoru.SoruID
                             }).OrderBy(x => x.SoruNo).ToList();
                return sonuc;
            }
        }
        public static List<PaylarOlcmeVM> ListelePaySorulari(int payID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                //var soruList = db.ABSPaylarOlcme.Where(x => x.Silindi == false && x.FKPaylarID == payID).ToList();
                //var testSoruList = soruList.Where(x => x.EnumSoruTipi == (int)EnumSoruTipi.TEST).ToList();
                //var klasikSoruList = soruList.Where(x => x.EnumSoruTipi == (int)EnumSoruTipi.KLASIK).ToList();
                //var odevSoruList = soruList.Where(x => x.EnumSoruTipi == (int)EnumSoruTipi.UYGULAMA_ODEV).ToList();

                //foreach (var item in klasikSoruList)
                //{
                //    item.SoruNo = Convert.ToByte(item.SoruNo);
                //}

                //return testSoruList.Concat(klasikSoruList).Concat(odevSoruList).Where(x => x.FKPaylarID == payID)
                //.Select(x => new PaylarOlcmeVM
                //{
                //    FKPaylarID = x.FKPaylarID,
                //    SoruNo = x.SoruNo,
                //    SoruPuan = x.SoruPuan,
                //    FKKoordinatorID = x.FKKoordinatorID,
                //    EnumSoruTipi = x.EnumSoruTipi,
                //    EnumSinavTuru = x.EnumSinavTuru
                //})
                //.ToList();
                return db.ABSPaylarOlcme.Where(x => x.Silindi == false && x.FKPaylarID == payID).Select(x => new PaylarOlcmeVM
                {
                    FKPaylarID = x.FKPaylarID,
                    SoruNo = x.SoruNo,
                    SoruPuan = x.SoruPuan,
                    FKKoordinatorID = x.FKKoordinatorID,
                    EnumSoruTipi = x.EnumSoruTipi,
                    EnumSinavTuru = x.EnumSinavTuru
                }).ToList();
            }
        }
        public static bool NotGirilmisMi(int FKPaylarID, int yil, int enumDonem, int FKDersGrupID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                //var ortakDegerlendirmeID = db.ABSOrtakDegerlendirmeGrup.FirstOrDefault(x => x.FKDersGrupID == FKDersGrupID && x.ABSOrtakDegerlendirme.Yil == yil && x.ABSOrtakDegerlendirme.Donem == enumDonem).FKOrtakDegerlendirmeID;
                //var grupIDList = db.ABSOrtakDegerlendirmeGrup.Where(x => x.FKOrtakDegerlendirmeID == ortakDegerlendirmeID).Select(x => x.FKDersGrupID).ToList();
                return db.ABSOgrenciNot.Any(x => x.FKAbsPaylarID == FKPaylarID && x.Yil == yil && x.EnumDonem == enumDonem && x.NotDeger != 130 && x.Silindi == false); //  && grupIDList.Contains(x.FKDersGrupID.Value && x.FKDersGrupID == FKDersGrupID sınavlar gruplara göre oluşturulursa grupid eklenecek
            }
        }

        public static int SinavBilgi_Pay(int SinavID)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var sorgu = (from s in db.OSinav
                                 join sg in db.OSinavGrup on s.ID equals sg.FKSinavID
                                 where
                                 sg.Silindi == false &&
                                 s.ID == SinavID
                                 select sg.PayID).FirstOrDefault();
                    return sorgu;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        public static bool SoruNotGirilmisMi(int FKPaylarID, int FKDersGrupID, int yil, int enumDonem)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                //var ortakDegerlendirmeID = db.ABSOrtakDegerlendirmeGrup.FirstOrDefault(x => x.FKDersGrupID == FKDersGrupID && x.ABSOrtakDegerlendirme.Yil == yil && x.ABSOrtakDegerlendirme.Donem == enumDonem).FKOrtakDegerlendirmeID;
                //var grupIDList = db.ABSOrtakDegerlendirmeGrup.Where(x => x.FKOrtakDegerlendirmeID == ortakDegerlendirmeID).Select(x => x.FKDersGrupID).ToList();
                var soruList = db.ABSPaylarOlcme.Where(x => x.Silindi == false && x.FKPaylarID == FKPaylarID).Select(x => x.ID).ToList(); // && x.FKABSOrtakDegerlendirmeID == ortakDegerlendirmeID
                return db.ABSOgrenciNotDetay.Any(x => x.FKABSPaylarID == FKPaylarID && soruList.Contains(x.FKAbsPaylarOlcmeID.Value) && x.Yil == yil && x.EnumDonem == enumDonem && x.Silindi == false);
            }
        }
        public static void SinavOlustur(int soruSayisi, int FKPaylarID, int FKKoordinatorID, int yil, int enumDonem, int FKDersGrupID, int enumSinavTuru, int? birimID, string kullanici, string IP, int? klasikSoruSayisi, int OnlineAktiviteTip)
        {
            //if (enumSinavTuru != (int)EnumSinavTuru.ODEV && NotGirilmisMi(FKPaylarID, yil, enumDonem, FKDersGrupID))
            //{
            //    return false;
            //}

            //int ortakDegerlendirmeID = new Int32();
            OGRDersGrup dersGrup = new OGRDersGrup();
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var paybilgi = db.ABSPaylar.FirstOrDefault(x => !x.Silindi && x.ID == FKPaylarID);
                dersGrup = db.OGRDersGrup.FirstOrDefault(x => x.ID == FKDersGrupID);
                //ortakDegerlendirmeID = db.ABSOrtakDegerlendirmeGrup.FirstOrDefault(x => x.FKDersGrupID == dersGrup.ID && x.ABSOrtakDegerlendirme.Yil == yil && x.ABSOrtakDegerlendirme.Donem == enumDonem).FKOrtakDegerlendirmeID;

                //int oncekiSoruSayisi = db.ABSPaylarOlcme.Count(x => x.Silindi == false && x.FKPaylarID == FKPaylarID && x.FKABSOrtakDegerlendirmeID == ortakDegerlendirmeID);
                int oncekiSoruSayisi = db.ABSPaylarOlcme.Count(x => x.Silindi == false && x.FKPaylarID == FKPaylarID);
                double ortalamaPuan = 0;

                if (enumSinavTuru == (int)EnumSinavTuru.KARMA)
                {
                    ortalamaPuan = (double)100 / (double)(soruSayisi + klasikSoruSayisi.Value + oncekiSoruSayisi);
                }
                else
                {
                    ortalamaPuan = (double)100 / (double)(soruSayisi + oncekiSoruSayisi);
                }

                db.ABSPaylarOlcme.Where(x => x.Silindi == false && x.FKPaylarID == FKPaylarID)
                                 .ToList()
                                 .ForEach(x => x.SoruPuan = (byte)ortalamaPuan);
                db.SaveChanges();

                int soruTip = 2;
                //if (EnumSinavTuru == (int)Sabis.Bolum.Core.ENUM.ABS.EnumSinavTuru.TEST)
                //{
                //    soruTip = (int)EnumSoruTipi.TEST;
                //}
                if (enumSinavTuru == (int)EnumSinavTuru.KLASIK)
                {
                    soruTip = (int)EnumSoruTipi.KLASIK;
                }
                if (enumSinavTuru == (int)EnumSinavTuru.ODEV)
                {
                    soruTip = (int)EnumSoruTipi.UYGULAMA_ODEV;
                }
                if (enumSinavTuru == (int)EnumSinavTuru.ONLINESINAV)
                {
                    soruTip = (int)EnumSoruTipi.ONLINESINAV;
                    if (OnlineAktiviteTip == 2)
                    {
                        SINAVMAIN.SinavOlusturOnlineGenel(OnlineAktiviteTip, soruSayisi, kullanici, FKKoordinatorID, FKDersGrupID, FKPaylarID, yil, enumDonem);
                    }
                    else if (OnlineAktiviteTip == 1)
                    {
                        SINAVMAIN.SinavOlusturOnlineSinif(OnlineAktiviteTip, soruSayisi, kullanici, FKKoordinatorID, FKDersGrupID, FKPaylarID, yil, enumDonem);
                    }

                }

                for (int i = 1 + oncekiSoruSayisi; i <= soruSayisi + oncekiSoruSayisi; i++)
                {
                    ABSPaylarOlcme ap = new ABSPaylarOlcme();
                    ap.FKPaylarID = FKPaylarID;
                    //ap.FKABSOrtakDegerlendirmeID = ortakDegerlendirmeID;
                    ap.FKKoordinatorID = FKKoordinatorID;
                    ap.SoruNo = (byte)i;
                    ap.SoruPuan = (double)ortalamaPuan;
                    ap.Zaman = DateTime.Now;
                    ap.EnumSinavTuru = enumSinavTuru;
                    ap.EnumSoruTipi = soruTip;
                    ap.Silindi = false;

                    db.ABSPaylarOlcme.Add(ap);
                }
                db.SaveChanges();

                if (enumSinavTuru == (int)EnumSinavTuru.KARMA)
                {
                    for (int i = 1; i <= klasikSoruSayisi; i++)
                    {
                        ABSPaylarOlcme ap = new ABSPaylarOlcme();
                        ap.FKPaylarID = FKPaylarID;
                        //ap.FKABSOrtakDegerlendirmeID = ortakDegerlendirmeID;
                        ap.FKKoordinatorID = FKKoordinatorID;
                        ap.SoruNo = Convert.ToByte(i + soruSayisi);
                        ap.SoruPuan = (byte)ortalamaPuan;
                        ap.Zaman = DateTime.Now;
                        ap.EnumSinavTuru = enumSinavTuru;
                        ap.EnumSoruTipi = (int)EnumSoruTipi.KLASIK;
                        ap.Silindi = false;

                        db.ABSPaylarOlcme.Add(ap);
                    }
                    db.SaveChanges();
                }

            }
            //if (enumSinavTuru != (int)EnumSinavTuru.ONLINESINAV)
            //{
            using (KagitOkumaEntities db = new KagitOkumaEntities())
            {

                Sinav yeniSinav = new Sinav();
                yeniSinav.FKDersPlanAnaID = dersGrup.FKDersPlanAnaID.Value;
                yeniSinav.FKDersPlanID = dersGrup.FKDersPlanID;
                yeniSinav.FKABSPaylarID = FKPaylarID;
                yeniSinav.FKDersGrupID = FKDersGrupID;
                //yeniSinav.FKABSOrtakDegerlendirmeID = ortakDegerlendirmeID;
                yeniSinav.EnumSinavTuru = enumSinavTuru;
                if (enumSinavTuru == (int)EnumSinavTuru.KARMA)
                {
                    yeniSinav.KlasikSoruSayisi = klasikSoruSayisi;
                }
                yeniSinav.SoruSayisi = soruSayisi;
                yeniSinav.Silindi = false;
                yeniSinav.OlusturanKullanici = kullanici;
                yeniSinav.OlusturanIP = IP;
                yeniSinav.OlusturmaTarih = DateTime.Now;
                yeniSinav.Okunabilme = "0";

                if (!db.Sinav.Any(x=> x.FKABSPaylarID == FKPaylarID && !x.Silindi))
                {
                    db.Sinav.Add(yeniSinav);
                    db.SaveChanges();
                }

                
            }
            //}

        }
        public static bool SinavOlusturOnlineGenel(int OnlineAktiviteTip, int soruSayisi, string kullanici, int FKKoordinatorID, int FKDersGrupID, int FKPaylarID, int Yil, int Donem)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    if (OnlineAktiviteTip == 2)
                    {
                        var paybilgi = db.ABSPaylar.FirstOrDefault(x => !x.Silindi && x.ID == FKPaylarID);
                        var dersGrup = db.OGRDersGrup.FirstOrDefault(x => x.ID == FKDersGrupID);

                        var osinav = new OSinav();
                        osinav.Ad = paybilgi.Sira + ". " + Sabis.Enum.Utils.GetEnumDescription((Sabis.Enum.EnumCalismaTip)paybilgi.EnumCalismaTip);
                        //osinav.Ad = "Bu Bir Test Sınavıdır.";
                        osinav.EklemeTuru = OnlineAktiviteTip;
                        osinav.Aciklama = null;
                        osinav.Baslangic = DateTime.Now;
                        osinav.Bitis = DateTime.Now;
                        osinav.Sure = 30;
                        osinav.SoruSayisi = soruSayisi;
                        osinav.Yayinlandi = 0;
                        osinav.Ekleyen = kullanici;
                        osinav.EkleyenID = FKKoordinatorID;
                        osinav.EklemeTarihi = DateTime.Now;
                        osinav.Guncelleyen = kullanici;
                        osinav.GuncelleyenID = FKKoordinatorID;
                        osinav.GuncellemeTarihi = DateTime.Now;
                        osinav.Silindi = false;
                        osinav.MazeretSinav = false;

                        db.OSinav.Add(osinav);
                        db.SaveChanges();

                        var tumgruplar = db.OGRDersGrup.Where(x => x.FKDersPlanAnaID == dersGrup.FKDersPlanAnaID && x.OgretimYili == Yil && x.EnumDonem == Donem && x.Silindi == false && x.Acik == true).ToList();
                        foreach (var item in tumgruplar)
                        {
                            var osinavgrup = new OSinavGrup();
                            osinavgrup.FKSinavID = osinav.ID;
                            osinavgrup.FKDersPlanAnaID = item.FKDersPlanAnaID.Value;
                            osinavgrup.FKDersPlanID = item.FKDersPlanID.Value;
                            osinavgrup.FKDersGrupID = item.ID;
                            osinavgrup.PayID = FKPaylarID;
                            osinavgrup.Silindi = false;
                            db.OSinavGrup.Add(osinavgrup);
                        }
                        db.SaveChanges();

                    }
                    return true;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public static bool SinavOlusturOnlineSinif(int OnlineAktiviteTip, int soruSayisi, string kullanici, int FKKoordinatorID, int FKDersGrupID, int FKPaylarID, int Yil, int Donem)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var paybilgi = db.ABSPaylar.FirstOrDefault(x => !x.Silindi && x.ID == FKPaylarID);
                    var dersGrup = db.OGRDersGrup.FirstOrDefault(x => x.ID == FKDersGrupID);
                    var tumgruplar = db.OGRDersGrup.Where(x => x.FKDersPlanAnaID == dersGrup.FKDersPlanAnaID && x.OgretimYili == Yil && x.EnumDonem == Donem && x.Silindi == false && x.Acik == true).ToList();
                    foreach (var item in tumgruplar)
                    {
                        var osinav = new OSinav();
                        osinav.Ad = paybilgi.Sira + ". " + Sabis.Enum.Utils.GetEnumDescription((Sabis.Enum.EnumCalismaTip)paybilgi.EnumCalismaTip);
                        //osinav.Ad = "Bu Bir Test Sınavıdır.(Grup)";
                        osinav.EklemeTuru = OnlineAktiviteTip;
                        osinav.Aciklama = null;
                        osinav.Baslangic = DateTime.Now;
                        osinav.Bitis = DateTime.Now;
                        osinav.Sure = 30;
                        osinav.SoruSayisi = soruSayisi;
                        osinav.Yayinlandi = 0;
                        osinav.Ekleyen = kullanici;
                        osinav.EkleyenID = FKKoordinatorID;
                        osinav.EklemeTarihi = DateTime.Now;
                        osinav.Guncelleyen = kullanici;
                        osinav.GuncelleyenID = FKKoordinatorID;
                        osinav.GuncellemeTarihi = DateTime.Now;
                        osinav.Silindi = false;
                        osinav.MazeretSinav = false;
                        db.OSinav.Add(osinav);
                        db.SaveChanges();

                        var osinavgrup = new OSinavGrup();
                        osinavgrup.FKSinavID = osinav.ID;
                        osinavgrup.FKDersPlanAnaID = item.FKDersPlanAnaID.Value;
                        osinavgrup.FKDersPlanID = item.FKDersPlanID.Value;
                        osinavgrup.FKDersGrupID = item.ID;
                        osinavgrup.PayID = FKPaylarID;
                        osinavgrup.Silindi = false;
                        db.OSinavGrup.Add(osinavgrup);

                        db.SaveChanges();
                    }
                    db.SaveChanges();


                    return true;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public static OSinavVM SinavBilgi(int FKDersGrupHocaID, int FKPayID)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var sorgu = (from p in db.ABSPaylar
                                 join sg in db.OSinavGrup on p.ID equals sg.PayID
                                 join gh in db.OGRDersGrupHoca on sg.FKDersGrupID equals gh.FKDersGrupID
                                 join s in db.OSinav on sg.FKSinavID equals s.ID
                                 where
                                 p.ID == FKPayID &&
                                 gh.ID == FKDersGrupHocaID &&
                                 s.Silindi == false &&
                                 sg.Silindi == false
                                 select new OSinavVM
                                 {
                                     SinavID = s.ID,
                                     Ad = s.Ad,
                                     EklemeTuru = s.EklemeTuru,
                                     Aciklama = s.Aciklama,
                                     Baslangic = s.Baslangic,
                                     Bitis = s.Bitis,
                                     Sure = s.Sure,
                                     SoruSayisi = s.SoruSayisi,
                                     Yayinlandi = s.Yayinlandi,
                                     Mazeret = s.MazeretSinav,
                                     MazeretBaslangic = s.TarihMazeretBaslangic,
                                     MazeretBitis = s.TarihMazeretBitis,
                                     PayID = FKPayID,
                                     HocaID = FKDersGrupHocaID
                                 }).FirstOrDefault();
                    return sorgu;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public static string SinavAd(int sinavid)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    return db.OSinav.FirstOrDefault(x => x.ID == sinavid).Ad;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        public static List<GrupDto> Gruplar(int FKDersGrupHocaID, int yil, int donem)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    //koordinatörlük kontrol
                    var gruphoca = db.OGRDersGrupHoca.FirstOrDefault(x => x.ID == FKDersGrupHocaID);
                    var anadersid = db.OGRDersGrup.FirstOrDefault(x => x.ID == gruphoca.FKDersGrupID).FKDersPlanAnaID;
                    int pID = db.DersKoordinator.Where(x => x.FKDersPlanAnaID == anadersid && x.Silindi == false).OrderByDescending(x => x.ID).Select(x => x.FKPersonelID).FirstOrDefault();
                    bool sonuc = pID == gruphoca.FKPersonelID;
                    if (sonuc)
                    {
                        //var tumDersGruplari = db.OGRDersGrup.Where(
                        //x => x.FKDersPlanAnaID == anadersid &&
                        //x.OgretimYili == yil &&
                        //x.EnumDonem == donem).Select(x => new GrupDto { value = x.ID, text = x.GrupAd + " - (" + x.EnumOgretimTur + ". Öğretim)" }).ToList();
                        //return tumDersGruplari;

                        var tumDersGruplari = (from g in db.OGRDersGrup
                                               join b in db.Birimler on g.FKBirimID equals b.ID
                                               where
                                               g.FKDersPlanAnaID == anadersid &&
                                               g.OgretimYili == yil &&
                                               g.EnumDonem == donem &&
                                               g.Acik == true &&
                                               g.Silindi == false
                                               select new GrupDto
                                               {
                                                   value = g.ID,
                                                   text = g.GrupAd + " - (" + g.EnumOgretimTur + ". Öğretim) " + b.BirimAdi
                                               }).ToList();
                        return tumDersGruplari;
                    }
                    else
                    {
                        //var hocaninGruplari = (from h in db.OGRDersGrupHoca
                        //                       join g in db.OGRDersGrup on h.FKDersGrupID equals g.ID
                        //                       where
                        //                       h.FKPersonelID == gruphoca.FKPersonelID &&
                        //                       g.FKDersPlanAnaID == anadersid &&
                        //                       g.OgretimYili == yil &&
                        //                       g.EnumDonem == donem
                        //                       select new GrupDto
                        //                       {
                        //                           value = g.ID,
                        //                           text = g.GrupAd + " - (" + g.EnumOgretimTur + ". Öğretim)"
                        //                       }).ToList();
                        var hocaninGruplari = (from h in db.OGRDersGrupHoca
                                               join g in db.OGRDersGrup on h.FKDersGrupID equals g.ID
                                               join b in db.Birimler on g.FKBirimID equals b.ID
                                               where
                                               h.FKPersonelID == gruphoca.FKPersonelID &&
                                               g.FKDersPlanAnaID == anadersid &&
                                               g.OgretimYili == yil &&
                                               g.EnumDonem == donem &&
                                               g.Silindi == false &&
                                               g.Acik == true
                                               select new GrupDto
                                               {
                                                   value = g.ID,
                                                   text = g.GrupAd + " - (" + g.EnumOgretimTur + ". Öğretim) " + b.BirimAdi
                                               }).ToList();
                        return hocaninGruplari;
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public static List<SinavOturumDto> Oturumlar(int sinavid, int? grupid, string ogrencino)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    if (grupid != null)
                    {
                        //var sonuc = (from yazilma in db.OGROgrenciYazilma
                        //             join kullanici in db.Kullanici on yazilma.FKOgrenciID equals kullanici.FKOgrenciID
                        //             join sogrenci in db.OSinavOgrenci on yazilma.ID equals sogrenci.DersYazilmaID into tbl1
                        //             from sogrenci in tbl1.Where(x => x.SinavID == sinavid).DefaultIfEmpty()
                        //             join gbildirim in db.ABSGeriBildirim on new { OGRID = sogrenci.OgrenciID, SINAVID = sogrenci.SinavID } equals new { OGRID = gbildirim.OgrenciID, SINAVID = gbildirim.AktiviteID } into tbl2
                        //             from gbildirim in tbl2.Where(x => x.AktiviteID == sinavid && x.EnumGeriBildirimDurum != 2).DefaultIfEmpty()
                        //             where
                        //             yazilma.FKDersGrupID == grupid &&
                        //             yazilma.Iptal == false &&
                        //             yazilma.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL &&
                        //             yazilma.EnumDonem == yazilma.OGRDersGrup.EnumDonem

                        //             orderby yazilma.OGRKimlik.Numara
                        //             select new SinavOturumDto
                        //             {
                        //                 EID = sogrenci == null ? 0 : sogrenci.EID,
                        //                 YazilmaID = yazilma.ID,
                        //                 SinavID = sogrenci == null ? 0 : sogrenci.SinavID,
                        //                 OgrenciNumara = yazilma.OGRKimlik.Numara,
                        //                 OgrenciAd = yazilma.OGRKimlik.Kisi.Ad,
                        //                 OgrenciSoyad = yazilma.OGRKimlik.Kisi.Soyad,
                        //                 OgrenciID = yazilma.OGRKimlik.ID,
                        //                 OgrenciNo = kullanici.KullaniciAdi,
                        //                 KisiID = yazilma.OGRKimlik.FKKisiID,
                        //                 OturumDurum = sogrenci == null ? 0 : sogrenci.OturumDurum,
                        //                 Notu = sogrenci == null ? 0 : sogrenci.SonNot,
                        //                 OturumTarih = sogrenci == null ? null : sogrenci.TarihKatilimIlk,
                        //                 OturumMesaj = sogrenci == null ? "Giriş Yapmadı" : "Sınava Katıldı",
                        //                 DogruSayisi = sogrenci == null ? 0 : sogrenci.DogruSayisi,
                        //                 YanlisSayisi = sogrenci == null ? 0 : sogrenci.YanlisSayisi,
                        //                 BosSayisi = sogrenci == null ? 0 : sogrenci.SoruSayisi - (sogrenci.DogruSayisi + sogrenci.YanlisSayisi),
                        //                 MazeretDogruSayisi = sogrenci == null ? 0 : sogrenci.MazeretDogruSayisi,
                        //                 MazeretYanlisSayisi = sogrenci == null ? 0 : sogrenci.MazeretYanlisSayisi,
                        //                 MazeretBosSayisi = sogrenci == null ? 0 : sogrenci.SoruSayisi - (sogrenci.MazeretDogruSayisi + sogrenci.MazeretYanlisSayisi),
                        //                 TarihMazeretOturumBaslangic = sogrenci == null ? null : sogrenci.TarihMazeretOturumBaslangic,
                        //                 TarihMazeretOturumBitis = sogrenci == null ? null : sogrenci.TarihMazeretOturumBitis,
                        //                 GeriBildirimID = gbildirim == null ? 0 : gbildirim.ID
                        //             });
                        var sonuc = (from yazilma in db.OGROgrenciYazilma
                                     join kullanici in db.Kullanici on yazilma.FKOgrenciID equals kullanici.FKOgrenciID
                                     join sogrenci in db.OSinavOgrenci on yazilma.ID equals sogrenci.DersYazilmaID into tbl1
                                     from sogrenci in tbl1.Where(x => x.SinavID == sinavid).DefaultIfEmpty()
                                     join gbildirim in db.ABSGeriBildirim on yazilma.FKOgrenciID equals gbildirim.OgrenciID into tbl2
                                     from gbildirim in tbl2.Where(x => x.AktiviteID == sinavid && x.EnumGeriBildirimDurum != 2).DefaultIfEmpty()
                                     where
                                     yazilma.FKDersGrupID == grupid &&
                                     yazilma.Iptal == false &&
                                     yazilma.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL &&
                                     yazilma.EnumDonem == yazilma.OGRDersGrup.EnumDonem

                                     orderby yazilma.OGRKimlik.Numara
                                     select new SinavOturumDto
                                     {
                                         EID = sogrenci == null ? 0 : sogrenci.EID,
                                         YazilmaID = yazilma.ID,
                                         SinavID = sogrenci == null ? 0 : sogrenci.SinavID,
                                         OgrenciNumara = yazilma.OGRKimlik.Numara,
                                         OgrenciAd = yazilma.OGRKimlik.Kisi.Ad,
                                         OgrenciSoyad = yazilma.OGRKimlik.Kisi.Soyad,
                                         OgrenciID = yazilma.OGRKimlik.ID,
                                         OgrenciNo = kullanici.KullaniciAdi,
                                         KisiID = yazilma.OGRKimlik.FKKisiID,
                                         OturumDurum = sogrenci == null ? 0 : sogrenci.OturumDurum,
                                         Notu = sogrenci == null ? 0 : sogrenci.SonNot,
                                         OturumTarih = sogrenci == null ? null : sogrenci.TarihKatilimIlk,
                                         OturumMesaj = sogrenci == null ? "Giriş Yapmadı" : "Sınava Katıldı",
                                         DogruSayisi = sogrenci == null ? 0 : sogrenci.DogruSayisi,
                                         YanlisSayisi = sogrenci == null ? 0 : sogrenci.YanlisSayisi,
                                         BosSayisi = sogrenci == null ? 0 : sogrenci.SoruSayisi - (sogrenci.DogruSayisi + sogrenci.YanlisSayisi),
                                         MazeretDogruSayisi = sogrenci == null ? 0 : sogrenci.MazeretDogruSayisi,
                                         MazeretYanlisSayisi = sogrenci == null ? 0 : sogrenci.MazeretYanlisSayisi,
                                         MazeretBosSayisi = sogrenci == null ? 0 : sogrenci.SoruSayisi - (sogrenci.MazeretDogruSayisi + sogrenci.MazeretYanlisSayisi),

                                         TarihOturumBaslangic = sogrenci == null ? null : sogrenci.TarihOturumBaslangic,
                                         TarihOturumBitis = sogrenci == null ? null : sogrenci.TarihOturumBitis,

                                         TarihMazeretOturumBaslangic = sogrenci == null ? null : sogrenci.TarihMazeretOturumBaslangic,
                                         TarihMazeretOturumBitis = sogrenci == null ? null : sogrenci.TarihMazeretOturumBitis,
                                         GeriBildirimID = gbildirim == null ? 0 : gbildirim.ID
                                     });



                        if (ogrencino != null)
                        {
                            var ogrsonuc = sonuc.Where(x => x.OgrenciNo == ogrencino).ToList();
                            return ogrsonuc;
                        }
                        else
                        {
                            return sonuc.ToList();
                        }
                    }
                    else
                    {
                        var sonuc = (from yazilma in db.OGROgrenciYazilma
                                     join kullanici in db.Kullanici on yazilma.FKOgrenciID equals kullanici.FKOgrenciID
                                     join sogrenci in db.OSinavOgrenci on yazilma.ID equals sogrenci.DersYazilmaID into tbl1
                                     from sogrenci in tbl1.Where(x => x.SinavID == sinavid).DefaultIfEmpty()
                                     join gbildirim in db.ABSGeriBildirim on new { OGRID = sogrenci.OgrenciID, SINAVID = sogrenci.SinavID } equals new { OGRID = gbildirim.OgrenciID, SINAVID = gbildirim.AktiviteID } into tbl2
                                     from gbildirim in tbl2.Where(x => x.AktiviteID == sinavid && x.EnumGeriBildirimDurum != 2).DefaultIfEmpty()
                                     where
                                     sogrenci.SinavID == sinavid &&
                                     yazilma.Iptal == false &&
                                     yazilma.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL &&
                                     yazilma.EnumDonem == yazilma.OGRDersGrup.EnumDonem

                                     orderby yazilma.OGRKimlik.Numara
                                     select new SinavOturumDto
                                     {
                                         EID = sogrenci == null ? 0 : sogrenci.EID,
                                         SinavID = sogrenci == null ? 0 : sogrenci.SinavID,
                                         OgrenciNumara = yazilma.OGRKimlik.Numara,
                                         OgrenciAd = yazilma.OGRKimlik.Kisi.Ad,
                                         OgrenciSoyad = yazilma.OGRKimlik.Kisi.Soyad,
                                         OgrenciID = yazilma.OGRKimlik.ID,
                                         OgrenciNo = kullanici.KullaniciAdi,
                                         KisiID = yazilma.OGRKimlik.FKKisiID,
                                         OturumDurum = sogrenci == null ? 0 : sogrenci.OturumDurum,
                                         Notu = sogrenci == null ? 0 : sogrenci.SonNot,
                                         OturumTarih = sogrenci == null ? null : sogrenci.TarihKatilimIlk,
                                         OturumMesaj = sogrenci == null ? "Giriş Yapmadı" : "Sınava Katıldı",
                                         DogruSayisi = sogrenci == null ? 0 : sogrenci.DogruSayisi,
                                         YanlisSayisi = sogrenci == null ? 0 : sogrenci.YanlisSayisi,
                                         BosSayisi = sogrenci == null ? 0 : sogrenci.SoruSayisi - (sogrenci.DogruSayisi + sogrenci.YanlisSayisi),
                                         MazeretDogruSayisi = sogrenci == null ? 0 : sogrenci.MazeretDogruSayisi,
                                         MazeretYanlisSayisi = sogrenci == null ? 0 : sogrenci.MazeretYanlisSayisi,
                                         MazeretBosSayisi = sogrenci == null ? 0 : sogrenci.SoruSayisi - (sogrenci.MazeretDogruSayisi + sogrenci.MazeretYanlisSayisi),
                                         TarihMazeretOturumBaslangic = sogrenci == null ? null : sogrenci.TarihMazeretOturumBaslangic,
                                         TarihMazeretOturumBitis = sogrenci == null ? null : sogrenci.TarihMazeretOturumBitis,
                                         GeriBildirimID = gbildirim == null ? 0 : gbildirim.ID
                                     });



                        if (ogrencino != null)
                        {
                            var ogrsonuc = sonuc.Where(x => x.OgrenciNo == ogrencino).ToList();
                            return ogrsonuc;
                        }
                        else
                        {
                            return sonuc.ToList();
                        }
                    }



                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public static List<SinavOturumDto> TumSorunBildirenOturumlar(int sinavid)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var sonuc = (from gbildirim in db.ABSGeriBildirim
                                 join kullanici in db.Kullanici on gbildirim.OgrenciID equals kullanici.FKOgrenciID
                                 join kisi in db.Kisi on kullanici.FKKisiID equals kisi.ID
                                 join ogr in db.OGRKimlik on kullanici.FKOgrenciID equals ogr.ID
                                 join sogrenci in db.OSinavOgrenci on gbildirim.OgrenciID equals sogrenci.OgrenciID into tbl1
                                 from sogrenci in tbl1.Where(x => x.SinavID == sinavid).DefaultIfEmpty()
                                 where
                                 gbildirim.AktiviteID == sinavid &&
                                 gbildirim.EnumModulTipi == 0 &&
                                 gbildirim.EnumGeriBildirimDurum != 2
                                 select new SinavOturumDto
                                 {
                                     EID = sogrenci == null ? 0 : sogrenci.EID,
                                     SinavID = sogrenci == null ? 0 : sogrenci.SinavID,
                                     OgrenciNumara = ogr.Numara,
                                     OgrenciAd = kisi.Ad,
                                     OgrenciSoyad = kisi.Soyad,
                                     OgrenciID = ogr.ID,
                                     OgrenciNo = kullanici.KullaniciAdi,
                                     KisiID = kisi.ID,
                                     OturumDurum = sogrenci == null ? 0 : sogrenci.OturumDurum,
                                     Notu = sogrenci == null ? 0 : sogrenci.SonNot,
                                     OturumTarih = sogrenci == null ? null : sogrenci.TarihKatilimIlk,
                                     OturumMesaj = sogrenci == null ? "Giriş Yapmadı" : "Sınava Katıldı",
                                     DogruSayisi = sogrenci == null ? 0 : sogrenci.DogruSayisi,
                                     YanlisSayisi = sogrenci == null ? 0 : sogrenci.YanlisSayisi,
                                     BosSayisi = sogrenci == null ? 0 : sogrenci.SoruSayisi - (sogrenci.DogruSayisi + sogrenci.YanlisSayisi),
                                     MazeretDogruSayisi = sogrenci == null ? 0 : sogrenci.MazeretDogruSayisi,
                                     MazeretYanlisSayisi = sogrenci == null ? 0 : sogrenci.MazeretYanlisSayisi,
                                     MazeretBosSayisi = sogrenci == null ? 0 : sogrenci.SoruSayisi - (sogrenci.MazeretDogruSayisi + sogrenci.MazeretYanlisSayisi),
                                     TarihMazeretOturumBaslangic = sogrenci == null ? null : sogrenci.TarihMazeretOturumBaslangic,
                                     TarihMazeretOturumBitis = sogrenci == null ? null : sogrenci.TarihMazeretOturumBitis,
                                     GeriBildirimID = gbildirim == null ? 0 : gbildirim.ID

                                 });
                    return sonuc.ToList();
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public static GeriBildirimDto SorunBildirimi(int bid)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var geribildirim = db.ABSGeriBildirim.FirstOrDefault(x => x.ID == bid);
                    var model = new GeriBildirimDto();
                    if (geribildirim != null)
                    {
                        geribildirim.EnumGeriBildirimDurum = (int)EnumGeriBildirimDurum.OKUNDU;
                        db.SaveChanges();

                        model.ID = geribildirim.ID;
                        model.TarihKayit = geribildirim.TarihKayit;
                        model.OgrenciID = geribildirim.OgrenciID;
                        model.AktiviteID = geribildirim.AktiviteID;
                        model.GrupID = geribildirim.GrupID;
                        model.EnumModulTipi = geribildirim.EnumModulTipi;
                        model.Baslik = geribildirim.Baslik;
                        model.Icerik = geribildirim.Icerik;
                        model.EnumGeriBildirimDurum = geribildirim.EnumGeriBildirimDurum;
                        model.DosyaKey = geribildirim.DosyaKey;

                        model.GecmisBildirimler = db.ABSGeriBildirim.Where(x => x.AktiviteID == geribildirim.AktiviteID && x.OgrenciID == geribildirim.OgrenciID && x.EnumGeriBildirimDurum == 2).Select(y => new GeriBildirimDto
                        {
                            AktiviteID = y.AktiviteID,
                            Baslik = y.Baslik,
                            Cevap = y.Cevap,
                            DosyaKey = y.DosyaKey,
                            EnumGeriBildirimDurum = y.EnumGeriBildirimDurum,
                            EnumModulTipi = y.EnumModulTipi,
                            Icerik = y.Icerik,
                            ID = y.ID,
                            TarihKayit = y.TarihKayit

                        }).ToList();

                        return model;
                    }
                    return model;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public static List<OSinavOnizleme> SinavOnizleme(int sinavID, bool mazeret)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var sorular = db.OSinavSoru.Where(s => s.SinavID == sinavID && s.Silindi == false && s.MazeretSoru == mazeret).Select(t =>
                    new OSinavOnizleme()
                    {
                        SinavID = t.SinavID,
                        SoruID = t.SoruID,
                        SoruNo = t.SoruNo,
                        Baslik = t.Baslik,
                        DogruCevap = t.DogruCevap,
                        Cevap1 = t.Cevap1,
                        Cevap2 = t.Cevap2,
                        Cevap3 = t.Cevap3,
                        Cevap4 = t.Cevap4,
                        Cevap5 = t.Cevap5,
                        SoruResim = db.OSinavDosya.FirstOrDefault(x => x.SoruID == t.SoruID).DosyaKey,
                        Cevap1Resim = t.Cevap1DosyaKey,
                        Cevap2Resim = t.Cevap2DosyaKey,
                        Cevap3Resim = t.Cevap3DosyaKey,
                        Cevap4Resim = t.Cevap4DosyaKey,
                        Cevap5Resim = t.Cevap5DosyaKey
                    }).OrderBy(x => Guid.NewGuid()).ToList();
                    var test = sorular;
                    return sorular;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public static bool BildirimKaydet(int ID, string Cevap)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var model = db.ABSGeriBildirim.FirstOrDefault(x => x.ID == ID);
                    if (model != null)
                    {
                        if (model.EnumGeriBildirimDurum != 2)
                        {
                            model.EnumGeriBildirimDurum = 2;
                        }
                        model.Cevap = Cevap;
                        db.SaveChanges();

                        return true;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public static List<OturumDetayDto> OturumDetay(int eid)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var sorgu = (from so in db.OSinavOgrenci
                                 join sr in db.OSinavSoru on so.SinavID equals sr.SinavID
                                 join cv in db.OSinavCevap on new { EID = so.EID, SORUID = sr.SoruID } equals new { EID = cv.EID, SORUID = cv.SoruID }
                                 where
                                 so.EID == eid
                                 select new OturumDetayDto
                                 {
                                     SinavID = so.SinavID,
                                     SoruNo = sr.SoruNo,
                                     EID = so.EID,
                                     Notu = so.Notu,
                                     MazeretNotu = so.MazeretNotu,
                                     Baslik = sr.Baslik,
                                     CevapDogru = cv.CevapDogru,
                                     CevapDogru1 = cv.CevapDogru == true ? 1 : cv.Cevap == null ? -1 : 0,
                                     OgrenciCevap = cv.Cevap == 1 ? sr.Cevap1 :
                                                    cv.Cevap == 2 ? sr.Cevap2 :
                                                    cv.Cevap == 3 ? sr.Cevap3 :
                                                    cv.Cevap == 4 ? sr.Cevap4 :
                                                    cv.Cevap == 5 ? sr.Cevap5 : "Boş Bırakıldı",
                                     DogruCevap = sr.DogruCevap == 1 ? sr.Cevap1 :
                                                  sr.DogruCevap == 2 ? sr.Cevap2 :
                                                  sr.DogruCevap == 3 ? sr.Cevap3 :
                                                  sr.DogruCevap == 4 ? sr.Cevap4 :
                                                  sr.DogruCevap == 5 ? sr.Cevap5 : "Boş Bırakıldı",
                                     MazeretSoru = sr.MazeretSoru,
                                     MazeretCevap = sr.MazeretSoru,
                                     TarihOturumBaslangic = so.TarihOturumBaslangic,
                                     TarihOturumBitis = so.TarihOturumBitis,
                                     TarihMazeretOturumBaslangic = so.TarihMazeretOturumBaslangic,
                                     TarihMazeretOturumBitis = so.TarihMazeretOturumBitis,
                                     SoruIptal = sr.Iptal
                                 }).OrderBy(x => x.SoruNo).ToList();
                    return sorgu;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public static bool OturumTemizle(int EID, bool mazeret)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var model = db.OSinavOgrenci.FirstOrDefault(x => x.EID == EID);

                    if (mazeret == true)
                    {
                        model.MazeretNotu = null;
                        model.Durum = 0;
                        model.OturumDurum = 3;

                        model.TarihMazeretOturumBaslangic = null;
                        model.TarihMazeretOturumBitis = null;
                        model.MazeretDogruSayisi = 0;
                        model.MazeretYanlisSayisi = 0;
                        db.SaveChanges();

                        var cevaplar = db.OSinavCevap.Where(x => x.EID == EID && x.MazeretSoru == true);
                        foreach (var cevap in cevaplar)
                        {
                            db.OSinavCevap.Remove(cevap);
                        }
                        db.SaveChanges();
                    }
                    else if (mazeret == false)
                    {
                        model.Notu = null;
                        model.SonNot = null;
                        model.Durum = 0;
                        model.OturumDurum = 0;
                        model.TarihKatilimIlk = null;
                        model.TarihKatilimSon = null;
                        model.TarihOturumBaslangic = null;
                        model.TarihOturumBitis = null;
                        model.DogruSayisi = 0;
                        model.YanlisSayisi = 0;
                        db.SaveChanges();

                        var cevaplar = db.OSinavCevap.Where(x => x.EID == EID && x.MazeretSoru == false);
                        foreach (var cevap in cevaplar)
                        {
                            db.OSinavCevap.Remove(cevap);
                        }
                        db.SaveChanges();
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        public static bool SinavNotYenidenHesapla(int sinavid)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var liste = (from so in db.OSinavOgrenci
                                 join sr in db.OSinavSoru on so.SinavID equals sr.SinavID
                                 join cv in db.OSinavCevap on new { EID = so.EID, SORUID = sr.SoruID } equals new { EID = cv.EID, SORUID = cv.SoruID }
                                 where
                                 so.SinavID == sinavid &&
                                 so.Durum == 2 &&
                                 so.OturumDurum == 2
                                 select new
                                 {
                                     OgrenciID = so.OgrenciID,
                                     SinavID = so.SinavID,
                                     SoruID = sr.SoruID,
                                     SoruNo = sr.SoruNo,
                                     DogruCevap = sr.DogruCevap,
                                     OgrenciCevapID = cv.CevapID,
                                     OgrencininVerdigiCevap = cv.Cevap,
                                     MazeretSoru = sr.MazeretSoru,
                                     Iptal = sr.Iptal
                                 }).ToList();

                    foreach (var item in liste)
                    {
                        var sinavcevap = db.OSinavCevap.FirstOrDefault(x => x.CevapID == item.OgrenciCevapID);
                        if (item.DogruCevap == item.OgrencininVerdigiCevap)
                        {
                            sinavcevap.CevapDogru = true;
                        }
                        else if (item.DogruCevap != item.OgrencininVerdigiCevap)
                        {
                            sinavcevap.CevapDogru = false;
                        }
                        if (item.Iptal == true)
                        {
                            sinavcevap.CevapDogru = true;
                        }
                    }
                    db.SaveChanges();

                    //OSinavOgrenci tablosunda doğru yanlış sayılarını guncelle
                    //db.OturumCevapDurumGetir(sinavid);
                    db.OturumCevapDurumEsitle(sinavid);

                    //not güncelle
                    var sinavogrenci = db.OSinavOgrenci.Where(x => x.SinavID == sinavid && x.Durum == 2 && x.OturumDurum == 2).ToList();

                    foreach (var item in sinavogrenci)
                    {
                        var enrollmentNormal = db.OSinavOgrenci.FirstOrDefault(x => x.EID == item.EID);
                        //var notNormal = 0;
                        decimal notNormal = 0.0M;
                        var cevappuanlarNormal = (from c in db.OSinavCevap
                                                  join s in db.OSinavSoru on c.SoruID equals s.SoruID
                                                  join p in db.ABSPaylarOlcme on s.OlcmeID equals p.ID
                                                  where
                                                  (c.CevapDogru == true || c.Iptal == true) &&
                                                  c.EID == item.EID &&
                                                  c.MazeretSoru == false &&
                                                  c.OSinavSoru.Silindi == false &&
                                                  s.MazeretSoru == false &&
                                                  p.Silindi == false
                                                  select new
                                                  {
                                                      c.SoruID,
                                                      c.CevapDogru,
                                                      p.SoruNo,
                                                      p.SoruPuan
                                                  });

                        foreach (var puanekle in cevappuanlarNormal)
                        {
                            //notNormal = notNormal + Convert.ToInt32(puanekle.SoruPuan);

                            notNormal = notNormal + Convert.ToDecimal(puanekle.SoruPuan);
                        }
                        enrollmentNormal.Notu = Convert.ToDouble(notNormal);
                        enrollmentNormal.SonNot = Convert.ToDouble(notNormal);
                        //enrollmentNormal.Notu = notNormal;
                        //enrollmentNormal.SonNot = notNormal;
                        db.SaveChanges();

                        db.OturumCevapDurumEsitle(sinavid);
                    }
                    SinavNotYenidenHesaplaMazeret(sinavid);
                    return true;
                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        public static bool SinavNotYenidenHesaplaMazeret(int sinavid)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var liste = (from so in db.OSinavOgrenci
                                 join sr in db.OSinavSoru on so.SinavID equals sr.SinavID
                                 join cv in db.OSinavCevap on new { EID = so.EID, SORUID = sr.SoruID } equals new { EID = cv.EID, SORUID = cv.SoruID }
                                 where
                                 so.SinavID == sinavid &&
                                 so.OturumDurum == (int)SinavDurum.MazeretSinavTamamlandi &&
                                 sr.MazeretSoru == true
                                 select new
                                 {
                                     SinavID = so.SinavID,
                                     SoruID = sr.SoruID,
                                     SoruNo = sr.SoruNo,
                                     DogruCevap = sr.DogruCevap,
                                     OgrenciCevapID = cv.CevapID,
                                     OgrencininVerdigiCevap = cv.Cevap,
                                     MazeretSoru = sr.MazeretSoru,
                                     Iptal = sr.Iptal
                                 }).ToList();
                    foreach (var item in liste)
                    {
                        if (item.DogruCevap == item.OgrencininVerdigiCevap)
                        {
                            var sinavcevap = db.OSinavCevap.FirstOrDefault(x => x.CevapID == item.OgrenciCevapID);
                            sinavcevap.CevapDogru = true;
                        }
                        else if (item.DogruCevap != item.OgrencininVerdigiCevap)
                        {
                            var sinavcevap = db.OSinavCevap.FirstOrDefault(x => x.CevapID == item.OgrenciCevapID);
                            sinavcevap.CevapDogru = false;
                        }
                        if (item.Iptal == true)
                        {
                            var sinavcevap = db.OSinavCevap.FirstOrDefault(x => x.CevapID == item.OgrenciCevapID);
                            sinavcevap.CevapDogru = true;
                        }
                    }
                    db.SaveChanges();

                    var sinavogrenci = db.OSinavOgrenci.Where(x => x.SinavID == sinavid && x.OturumDurum == (int)SinavDurum.MazeretSinavTamamlandi).ToList();
                    foreach (var item in sinavogrenci)
                    {
                        var enrollmentMazeret = db.OSinavOgrenci.FirstOrDefault(x => x.EID == item.EID);
                        //var notMazeret = 0;
                        decimal notMazeret = 0.0M;
                        var cevappuanlarMazeret = (from c in db.OSinavCevap
                                                   join s in db.OSinavSoru on c.SoruID equals s.SoruID
                                                   join p in db.ABSPaylarOlcme on s.OlcmeID equals p.ID
                                                   where
                                                   (c.CevapDogru == true || c.Iptal == true) &&
                                                   c.EID == item.EID &&
                                                   c.MazeretSoru == true &&
                                                   c.OSinavSoru.Silindi == false &&
                                                   s.MazeretSoru == true
                                                   select new
                                                   {
                                                       c.SoruID,
                                                       c.CevapDogru,
                                                       p.SoruNo,
                                                       p.SoruPuan
                                                   });
                        foreach (var puanekle in cevappuanlarMazeret)
                        {
                            //notMazeret = notMazeret + Convert.ToInt32(puanekle.SoruPuan);
                            notMazeret = notMazeret + Convert.ToDecimal(puanekle.SoruPuan);
                        }
                        if (enrollmentMazeret.TarihMazeretOturumBaslangic != null && enrollmentMazeret.TarihMazeretOturumBitis != null)
                        {
                            enrollmentMazeret.MazeretNotu = Convert.ToDouble(notMazeret);
                            enrollmentMazeret.SonNot = Convert.ToDouble(notMazeret);
                        }
                        db.SaveChanges();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public static bool TamamlanmayanOturumBitir()
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var tamamlanmayanoturumlar = (from so in db.OSinavOgrenci
                                                  join si in db.OSinav on so.SinavID equals si.ID
                                                  where
                                                  so.Durum == 1 &&
                                                  so.OturumDurum == 1
                                                  select new
                                                  {
                                                      si.ID,
                                                      so.EID,
                                                      so.OgrenciID,
                                                      so.Durum,
                                                      so.OturumDurum,
                                                      so.TarihOturumBaslangic,
                                                      si.Sure,
                                                      so.DogruSayisi,
                                                      so.YanlisSayisi,
                                                      so.Notu,
                                                      so.SonNot
                                                  }).ToList();
                    foreach (var item in tamamlanmayanoturumlar)
                    {
                        if (item.TarihOturumBaslangic != null)
                        {
                            if (item.TarihOturumBaslangic.Value.AddMinutes(item.Sure + 10) < DateTime.Now)
                            {
                                var enrollment = db.OSinavOgrenci.FirstOrDefault(x => x.EID == item.EID);
                                enrollment.Durum = 2;
                                enrollment.OturumDurum = 2;
                                db.SaveChanges();




                                var enrollmentNormal = db.OSinavOgrenci.FirstOrDefault(x => x.EID == enrollment.EID);
                                decimal notNormal = 0.0M;
                                var cevappuanlarNormal = (from c in db.OSinavCevap
                                                          join s in db.OSinavSoru on c.SoruID equals s.SoruID
                                                          join p in db.ABSPaylarOlcme on s.OlcmeID equals p.ID
                                                          where
                                                          (c.CevapDogru == true || c.Iptal == true) &&
                                                          c.EID == enrollment.EID &&
                                                          c.MazeretSoru == false &&
                                                          c.OSinavSoru.Silindi == false &&
                                                          s.MazeretSoru == false
                                                          select new
                                                          {
                                                              c.SoruID,
                                                              c.CevapDogru,
                                                              p.SoruNo,
                                                              p.SoruPuan
                                                          });
                                foreach (var puanekle in cevappuanlarNormal)
                                {
                                    notNormal = notNormal + Convert.ToDecimal(puanekle.SoruPuan);
                                }
                                enrollmentNormal.Notu = Convert.ToDouble(notNormal);
                                enrollmentNormal.NotAciklama = "Sistem tarafından tamamlandı :" + Convert.ToDouble(notNormal);
                                enrollmentNormal.TarihOturumBitis = DateTime.Now;
                                enrollmentNormal.SonNot = Convert.ToDouble(notNormal);
                                db.SaveChanges();

                                var enrollmentMazeret = db.OSinavOgrenci.FirstOrDefault(x => x.EID == item.EID);
                                decimal notMazeret = 0.0M;
                                var cevappuanlarMazeret = (from c in db.OSinavCevap
                                                           join s in db.OSinavSoru on c.SoruID equals s.SoruID
                                                           join p in db.ABSPaylarOlcme on s.OlcmeID equals p.ID
                                                           where
                                                           (c.CevapDogru == true || c.Iptal == true) &&
                                                           c.EID == item.EID &&
                                                           c.MazeretSoru == true &&
                                                           c.OSinavSoru.Silindi == false &&
                                                           s.MazeretSoru == true
                                                           select new
                                                           {
                                                               c.SoruID,
                                                               c.CevapDogru,
                                                               p.SoruNo,
                                                               p.SoruPuan
                                                           });
                                foreach (var puanekle in cevappuanlarMazeret)
                                {
                                    notMazeret = notMazeret + Convert.ToDecimal(puanekle.SoruPuan);
                                }

                                if (enrollmentMazeret.TarihMazeretOturumBaslangic != null)
                                {
                                    enrollmentMazeret.MazeretNotu = Convert.ToDouble(notMazeret);
                                    enrollmentMazeret.TarihMazeretOturumBitis = DateTime.Now;
                                    enrollmentMazeret.SonNot = Convert.ToDouble(notMazeret);
                                }
                                db.SaveChanges();










                            }
                        }
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public static bool TamamlanmayanOturumBitirMazeret()
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var tamamlanmayanoturumlar = (from so in db.OSinavOgrenci
                                                  join si in db.OSinav on so.SinavID equals si.ID
                                                  where
                                                  so.OturumDurum == (int)SinavDurum.MazeretSinavDevamEdiyor
                                                  select new
                                                  {
                                                      si.ID,
                                                      so.EID,
                                                      so.OgrenciID,
                                                      so.Durum,
                                                      so.OturumDurum,
                                                      so.TarihMazeretOturumBaslangic,
                                                      si.Sure,
                                                      so.DogruSayisi,
                                                      so.YanlisSayisi,
                                                      so.Notu,
                                                      so.SonNot
                                                  }).ToList();
                    foreach (var item in tamamlanmayanoturumlar)
                    {
                        if (item.TarihMazeretOturumBaslangic != null)
                        {
                            if (item.TarihMazeretOturumBaslangic.Value.AddMinutes(item.Sure + 10) < DateTime.Now)
                            {
                                var enrollment = db.OSinavOgrenci.FirstOrDefault(x => x.EID == item.EID);
                                enrollment.Durum = 2;
                                enrollment.OturumDurum = (int)SinavDurum.MazeretSinavTamamlandi;
                                db.SaveChanges();

                                var enrollmentMazeret = db.OSinavOgrenci.FirstOrDefault(x => x.EID == item.EID);
                                decimal notMazeret = 0.0M;
                                var cevappuanlarMazeret = (from c in db.OSinavCevap
                                                           join s in db.OSinavSoru on c.SoruID equals s.SoruID
                                                           join p in db.ABSPaylarOlcme on s.OlcmeID equals p.ID
                                                           where
                                                           (c.CevapDogru == true || c.Iptal == true) &&
                                                           c.EID == item.EID &&
                                                           c.MazeretSoru == true &&
                                                           c.OSinavSoru.Silindi == false &&
                                                           s.MazeretSoru == true
                                                           select new
                                                           {
                                                               c.SoruID,
                                                               c.CevapDogru,
                                                               p.SoruNo,
                                                               p.SoruPuan
                                                           });
                                foreach (var puanekle in cevappuanlarMazeret)
                                {
                                    notMazeret = notMazeret + Convert.ToDecimal(puanekle.SoruPuan);
                                }

                                if (enrollmentMazeret.TarihMazeretOturumBaslangic != null)
                                {
                                    enrollmentMazeret.MazeretNotu = Convert.ToDouble(notMazeret);
                                    enrollmentMazeret.TarihMazeretOturumBitis = DateTime.Now;
                                    enrollmentMazeret.SonNot = Convert.ToDouble(notMazeret);
                                }
                                db.SaveChanges();
                            }
                        }
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public static bool MazeretHakVer(int sinavid, int ogrenciID, int EID, int yazilmaid, int yil, int donem)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    if (EID == 0)
                    {
                        var sinavbilgi = db.OSinav.FirstOrDefault(x => x.ID == sinavid);
                        //var yazilmabilgi = YazilmaKontrol(sinavid, ogrenciID, yil, donem);
                        var osinavogrenci = new OSinavOgrenci();
                        osinavogrenci.SinavID = sinavid;
                        osinavogrenci.DersYazilmaID = yazilmaid;
                        osinavogrenci.SoruSayisi = sinavbilgi.SoruSayisi;
                        osinavogrenci.Durum = 0;
                        osinavogrenci.OgrenciID = ogrenciID;
                        osinavogrenci.TarihKatilimIlk = DateTime.Now;
                        //osinavogrenci.MazeretIzin = true;
                        osinavogrenci.OturumDurum = 3;
                        db.OSinavOgrenci.Add(osinavogrenci);
                        db.SaveChanges();
                        return true;
                    }
                    else
                    {
                        var model = db.OSinavOgrenci.FirstOrDefault(x => x.EID == EID);
                        model.OturumDurum = 3;
                        db.SaveChanges();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        public static bool MazeretHakSil(int sinavid, int ogrenciID, int EID, int yazilmaid, int yil, int donem)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var model = db.OSinavOgrenci.FirstOrDefault(x => x.EID == EID);
                    model.OturumDurum = model.Durum;
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        public static bool MazeretHakVer2(int sinavid, int ogrenciID, int FKDersGrupID, int yil, int donem)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var kayitkontrol = db.OSinavOgrenci.Any(x => x.SinavID == sinavid && x.OgrenciID == ogrenciID);
                    if (kayitkontrol)
                    {
                        //var model = db.OSinavOgrenci.FirstOrDefault(x => x.SinavID == sinavid && x.OgrenciID == ogrenciID);
                        //model.OturumDurum = 3;
                        //db.SaveChanges();
                        return true;
                    }
                    else
                    {
                        var yazilmaid = db.OGROgrenciYazilma.FirstOrDefault(
                            x => x.FKDersGrupID == FKDersGrupID &&
                            x.FKOgrenciID == ogrenciID &&
                            x.Yil == yil &&
                            x.EnumDonem == donem &&
                            x.Iptal == false
                            ).ID;

                        var sinavbilgi = db.OSinav.FirstOrDefault(x => x.ID == sinavid);

                        //var osinavogrenci = new OSinavOgrenci();
                        //osinavogrenci.SinavID = sinavid;
                        //osinavogrenci.DersYazilmaID = yazilmaid;
                        //osinavogrenci.SoruSayisi = sinavbilgi.SoruSayisi;
                        //osinavogrenci.Durum = 0;
                        //osinavogrenci.OgrenciID = ogrenciID;
                        ////osinavogrenci.TarihKatilimIlk = DateTime.Now;
                        //osinavogrenci.OturumDurum = 3;
                        //db.OSinavOgrenci.Add(osinavogrenci);
                        //db.SaveChanges();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public static bool YenidenDegerlendir(int PAYID, int GRUPID, string IP)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                //var sonuc = db.YenidenDegerlendir(PAYID, GRUPID);
                var ogrenciListe = from yazilma in db.OGROgrenciYazilma
                                   join ogrenci in db.OGRKimlik on yazilma.FKOgrenciID equals ogrenci.ID
                                   join kisi in db.Kisi on ogrenci.FKKisiID equals kisi.ID
                                   join dersgrup in db.OGRDersGrup on yazilma.FKDersGrupID equals dersgrup.ID
                                   join sg in db.OSinavGrup on yazilma.FKDersGrupID equals sg.FKDersGrupID
                                   join s in db.OSinav on sg.FKSinavID equals s.ID
                                   join so in db.OSinavOgrenci on new { YAZILMA = yazilma.ID, SINAVID = s.ID } equals new { YAZILMA = so.DersYazilmaID, SINAVID = so.SinavID } into tbl1
                                   from so in tbl1.DefaultIfEmpty()
                                   where
                                   yazilma.FKDersGrupID == GRUPID &&
                                   sg.PayID == PAYID &&
                                   yazilma.Iptal == false &&
                                   yazilma.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL &&
                                   s.Silindi == false &&
                                   sg.Silindi == false
                                   select new
                                   {
                                       Numara = ogrenci.Numara,
                                       Ad = kisi.Ad,
                                       Soyad = kisi.Soyad,
                                       OgrenciID = ogrenci.ID,
                                       DersGrupID = yazilma.FKDersGrupID.Value,
                                       Yil = dersgrup.OgretimYili,
                                       EnumDonem = dersgrup.EnumDonem,
                                       FKDersPlanID = dersgrup.FKDersPlanID,
                                       FKDersPlanAnaID = dersgrup.FKDersPlanAnaID,
                                       YazilmaID = yazilma.ID,
                                       SinavID = s.ID,
                                       KullaniciAdi = ogrenci.Kullanici1.FirstOrDefault().KullaniciAdi,
                                       PayID = PAYID,
                                       SinavDurum = so == null ? 0 : so.OturumDurum,
                                       SonNot = so == null ? 120 : so.SonNot == null ? 120 : so.SonNot

                                   };
                //not kaydet (normal+mazeret)
                foreach (var item in ogrenciListe)
                {
                    #region HicGirmeyen,Enroll Olusup Girmeyen,Normal Tamamlayan,Mazeret Hakkı Verildiği Halde Mazerete Girmeyen

                    if (item.SinavDurum == (int)SinavDurumNotAktarim.Girmedi || item.SinavDurum == (int)SinavDurumNotAktarim.Katildi || (item.SinavDurum == (int)SinavDurumNotAktarim.MazeretHakVerildi && item.SonNot != null))
                    {
                        var ogrnot = db.ABSOgrenciNot.Where(x => x.FKAbsPaylarID == item.PayID && x.FKOgrenciID == item.OgrenciID && x.Silindi == false).ToList();
                        if (ogrnot.Count > 0)
                        {
                            foreach (var onot in ogrnot)
                            {
                                var notsil = db.ABSOgrenciNot.FirstOrDefault(x => x.ID == onot.ID && x.Silindi == false);
                                notsil.Silindi = true;
                            }
                        }
                        ABSOgrenciNot ogrYeniNot = new ABSOgrenciNot();
                        ogrYeniNot.Zaman = DateTime.Now;
                        ogrYeniNot.Kullanici = "ONLINESINAV";
                        ogrYeniNot.Makina = IP;
                        ogrYeniNot.AktarilanVeriMi = false;
                        ogrYeniNot.Yil = item.Yil.Value;
                        ogrYeniNot.EnumDonem = item.EnumDonem.Value;
                        ogrYeniNot.FKOgrenciID = item.OgrenciID;
                        ogrYeniNot.FKDersPlanID = item.FKDersPlanID;
                        ogrYeniNot.FKDersPlanAnaID = item.FKDersPlanAnaID.Value;
                        ogrYeniNot.FKDersGrupID = item.DersGrupID;
                        ogrYeniNot.FKAbsPaylarID = item.PayID;
                        ogrYeniNot.NotDeger = Convert.ToDouble(item.SonNot);
                        ogrYeniNot.Notu = Convert.ToInt32(item.SonNot);
                        ogrYeniNot.UZEMID = null;
                        ogrYeniNot.Silindi = false;
                        db.ABSOgrenciNot.Add(ogrYeniNot);
                    }

                    #endregion
                    #region Mazeret Sınavına Girenler
                    if (item.SinavDurum == (int)SinavDurumNotAktarim.MazereteKatildi)
                    {
                        var ogrnot = db.ABSOgrenciNot.Where(x => x.FKAbsPaylarID == item.PayID && x.FKOgrenciID == item.OgrenciID && x.Silindi == false).ToList();
                        if (ogrnot.Count > 0)
                        {
                            foreach (var onot in ogrnot)
                            {
                                var notsil = db.ABSOgrenciNot.FirstOrDefault(x => x.ID == onot.ID);
                                notsil.Silindi = true;
                            }
                        }
                        ABSOgrenciNot ogrYeniNot = new ABSOgrenciNot();
                        ogrYeniNot.Zaman = DateTime.Now;
                        ogrYeniNot.Kullanici = "ONLINESINAV";
                        ogrYeniNot.Makina = IP;
                        ogrYeniNot.AktarilanVeriMi = false;
                        ogrYeniNot.Yil = item.Yil.Value;
                        ogrYeniNot.EnumDonem = item.EnumDonem.Value;
                        ogrYeniNot.FKOgrenciID = item.OgrenciID;
                        ogrYeniNot.FKDersPlanID = item.FKDersPlanID;
                        ogrYeniNot.FKDersPlanAnaID = item.FKDersPlanAnaID.Value;
                        ogrYeniNot.FKDersGrupID = item.DersGrupID;
                        ogrYeniNot.FKAbsPaylarID = item.PayID;
                        ogrYeniNot.NotDeger = Convert.ToDouble(item.SonNot);
                        ogrYeniNot.Notu = Convert.ToInt32(item.SonNot);
                        ogrYeniNot.UZEMID = null;
                        ogrYeniNot.Silindi = false;
                        db.ABSOgrenciNot.Add(ogrYeniNot);
                    }
                    #endregion
                }
                db.SaveChanges();
                // notdetay kaydet (normal+mazeret)
                foreach (var item in ogrenciListe)
                {
                    #region Normal

                    if (item.SinavDurum == (int)SinavDurumNotAktarim.Girmedi || item.SinavDurum == (int)SinavDurumNotAktarim.Katildi || (item.SinavDurum == (int)SinavDurumNotAktarim.MazeretHakVerildi && item.SonNot != null))
                    {
                        var ogrnot = db.ABSOgrenciNot.Where(x => x.FKAbsPaylarID == item.PayID && x.FKOgrenciID == item.OgrenciID && x.Silindi == false).FirstOrDefault();

                        var ogrnotdetay = db.ABSOgrenciNotDetay.Where(x => x.FKABSPaylarID == item.PayID && x.FKOgrenciID == item.OgrenciID && x.Silindi == false).ToList();
                        if (ogrnotdetay.Count > 0)
                        {
                            foreach (var notdetay in ogrnotdetay)
                            {
                                var ndetay = db.ABSOgrenciNotDetay.FirstOrDefault(x => x.ID == notdetay.ID && x.Silindi == false);
                                ndetay.Silindi = true;
                            }
                        }
                        var ogrencininCevaplari = (from sr in db.OSinavSoru
                                                   join sc in db.OSinavCevap on sr.SoruID equals sc.SoruID
                                                   join olcme in db.ABSPaylarOlcme on sr.OlcmeID equals olcme.ID
                                                   where
                                                   sc.SinavID == item.SinavID &&
                                                   sc.OgrenciID == item.OgrenciID &&
                                                   sr.MazeretSoru == false
                                                   select new
                                                   {
                                                       Zaman = DateTime.Now,
                                                       Kullanici = "ONLINESINAV",
                                                       Makina = IP,
                                                       AktarilanVeri = false,
                                                       SoruNo = olcme.SoruNo,
                                                       Notu = sc.CevapDogru == true ? olcme.SoruPuan : 0,
                                                       FKOgrenciNotID = ogrnot.ID,
                                                       FKAbsPaylarID = item.PayID,
                                                       FKOgrenciID = item.OgrenciID,
                                                       FKDersGrupID = item.DersGrupID,
                                                       PaylarOlcmeID = olcme.ID,
                                                       Yil = item.Yil,
                                                       EnumDonem = item.EnumDonem,
                                                       Silindi = false
                                                   }).OrderBy(x => x.SoruNo).ToList();
                        //sınava girmeyen öğrenciye bir tane gr basılıyor.
                        if (ogrencininCevaplari.Count == 0)
                        {
                            ABSOgrenciNotDetay ogrYeniNotDetay = new ABSOgrenciNotDetay();
                            ogrYeniNotDetay.Zaman = DateTime.Now;
                            ogrYeniNotDetay.Kullanici = "ONLINESINAV";
                            ogrYeniNotDetay.Makina = IP;
                            ogrYeniNotDetay.AktarilanVeriMi = false;
                            ogrYeniNotDetay.SoruNo = 1;
                            ogrYeniNotDetay.Notu = 120;
                            ogrYeniNotDetay.FKOgrenciNotID = ogrnot.ID;
                            ogrYeniNotDetay.FKABSPaylarID = item.PayID;
                            ogrYeniNotDetay.FKOgrenciID = item.OgrenciID;
                            ogrYeniNotDetay.FKDersGrupID = item.DersGrupID;
                            ogrYeniNotDetay.FKAbsPaylarOlcmeID = db.ABSPaylarOlcme.FirstOrDefault(x => x.FKPaylarID == item.PayID && x.SoruNo == 1 && x.Silindi == false).ID;
                            ogrYeniNotDetay.Yil = item.Yil;
                            ogrYeniNotDetay.EnumDonem = item.EnumDonem;
                            ogrYeniNotDetay.Silindi = false;
                            db.ABSOgrenciNotDetay.Add(ogrYeniNotDetay);
                        }
                        else
                        {
                            foreach (var ogrcvp in ogrencininCevaplari)
                            {
                                ABSOgrenciNotDetay ogrYeniNotDetay = new ABSOgrenciNotDetay();
                                ogrYeniNotDetay.Zaman = DateTime.Now;
                                ogrYeniNotDetay.Kullanici = "ONLINESINAV";
                                ogrYeniNotDetay.Makina = IP;
                                ogrYeniNotDetay.AktarilanVeriMi = false;
                                ogrYeniNotDetay.SoruNo = ogrcvp.SoruNo;
                                ogrYeniNotDetay.Notu = ogrcvp.Notu;
                                ogrYeniNotDetay.FKOgrenciNotID = ogrnot.ID;
                                ogrYeniNotDetay.FKABSPaylarID = item.PayID;
                                ogrYeniNotDetay.FKOgrenciID = item.OgrenciID;
                                ogrYeniNotDetay.FKDersGrupID = item.DersGrupID;
                                ogrYeniNotDetay.FKAbsPaylarOlcmeID = ogrcvp.PaylarOlcmeID;
                                ogrYeniNotDetay.Yil = item.Yil;
                                ogrYeniNotDetay.EnumDonem = item.EnumDonem;
                                ogrYeniNotDetay.Silindi = false;
                                db.ABSOgrenciNotDetay.Add(ogrYeniNotDetay);

                            }
                            //db.SaveChanges();
                        }
                    }
                    #endregion
                    #region Mazeret
                    if (item.SinavDurum == (int)SinavDurumNotAktarim.MazereteKatildi)
                    {
                        var ogrnot = db.ABSOgrenciNot.Where(x => x.FKAbsPaylarID == item.PayID && x.FKOgrenciID == item.OgrenciID && x.Silindi == false).FirstOrDefault();
                        var ogrnotdetay = db.ABSOgrenciNotDetay.Where(x => x.FKABSPaylarID == item.PayID && x.FKOgrenciID == item.OgrenciID && x.Silindi == false).ToList();
                        if (ogrnotdetay.Count > 0)
                        {
                            foreach (var notdetay in ogrnotdetay)
                            {
                                var ndetay = db.ABSOgrenciNotDetay.FirstOrDefault(x => x.ID == notdetay.ID && x.Silindi == false);
                                ndetay.Silindi = true;
                            }
                        }

                        var ogrencininCevaplari = (from sr in db.OSinavSoru
                                                   join sc in db.OSinavCevap on sr.SoruID equals sc.SoruID
                                                   join olcme in db.ABSPaylarOlcme on sr.OlcmeID equals olcme.ID
                                                   where
                                                   sc.SinavID == item.SinavID &&
                                                   sc.OgrenciID == item.OgrenciID &&
                                                   sr.MazeretSoru == true
                                                   select new
                                                   {
                                                       Zaman = DateTime.Now,
                                                       Kullanici = "ONLINESINAV",
                                                       Makina = IP,
                                                       AktarilanVeri = false,
                                                       SoruNo = olcme.SoruNo,
                                                       Notu = sc.CevapDogru == true ? olcme.SoruPuan : 0,
                                                       FKOgrenciNotID = ogrnot.ID,
                                                       FKAbsPaylarID = item.PayID,
                                                       FKOgrenciID = item.OgrenciID,
                                                       FKDersGrupID = item.DersGrupID,
                                                       PaylarOlcmeID = olcme.ID,
                                                       Yil = item.Yil,
                                                       EnumDonem = item.EnumDonem,
                                                       Silindi = false
                                                   }).OrderBy(x => x.SoruNo).ToList();
                        if (ogrencininCevaplari.Count == 0)
                        {
                            ABSOgrenciNotDetay ogrYeniNotDetay = new ABSOgrenciNotDetay();
                            ogrYeniNotDetay.Zaman = DateTime.Now;
                            ogrYeniNotDetay.Kullanici = "ONLINESINAV";
                            ogrYeniNotDetay.Makina = IP;
                            ogrYeniNotDetay.AktarilanVeriMi = false;
                            ogrYeniNotDetay.SoruNo = 1;
                            ogrYeniNotDetay.Notu = 120;
                            ogrYeniNotDetay.FKOgrenciNotID = ogrnot.ID;
                            ogrYeniNotDetay.FKABSPaylarID = item.PayID;
                            ogrYeniNotDetay.FKOgrenciID = item.OgrenciID;
                            ogrYeniNotDetay.FKDersGrupID = item.DersGrupID;
                            ogrYeniNotDetay.FKAbsPaylarOlcmeID = db.ABSPaylarOlcme.FirstOrDefault(x => x.FKPaylarID == item.PayID && x.SoruNo == 1 && x.Silindi == false).ID;
                            ogrYeniNotDetay.Yil = item.Yil;
                            ogrYeniNotDetay.EnumDonem = item.EnumDonem;
                            ogrYeniNotDetay.Silindi = false;
                            db.ABSOgrenciNotDetay.Add(ogrYeniNotDetay);
                        }
                        else
                        {
                            foreach (var ogrcvp in ogrencininCevaplari)
                            {
                                ABSOgrenciNotDetay ogrYeniNotDetay = new ABSOgrenciNotDetay();
                                ogrYeniNotDetay.Zaman = DateTime.Now;
                                ogrYeniNotDetay.Kullanici = "ONLINESINAV";
                                ogrYeniNotDetay.Makina = IP;
                                ogrYeniNotDetay.AktarilanVeriMi = false;
                                ogrYeniNotDetay.SoruNo = ogrcvp.SoruNo;
                                ogrYeniNotDetay.Notu = ogrcvp.Notu;
                                ogrYeniNotDetay.FKOgrenciNotID = ogrnot.ID;
                                ogrYeniNotDetay.FKABSPaylarID = item.PayID;
                                ogrYeniNotDetay.FKOgrenciID = item.OgrenciID;
                                ogrYeniNotDetay.FKDersGrupID = item.DersGrupID;
                                ogrYeniNotDetay.FKAbsPaylarOlcmeID = ogrcvp.PaylarOlcmeID;
                                ogrYeniNotDetay.Yil = item.Yil;
                                ogrYeniNotDetay.EnumDonem = item.EnumDonem;
                                ogrYeniNotDetay.Silindi = false;
                                db.ABSOgrenciNotDetay.Add(ogrYeniNotDetay);
                            }
                        }
                    }
                    #endregion
                }
                db.SaveChanges();
                return true;
            }
        }

        public static bool MazeretSinavi(int sinavid)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    return db.OSinav.FirstOrDefault(x => x.ID == sinavid).MazeretSinav;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public static YazilmaKontrolDto YazilmaKontrol(int sinavID, int ogrenciID, int yil, int donem)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var dersplanid = db.OSinavGrup.FirstOrDefault(s => s.FKSinavID == sinavID && s.Silindi == false).FKDersPlanID;
                    var yazilmakontrol = (from y in db.OGROgrenciYazilma
                                          where
                                          y.FKOgrenciID == ogrenciID &&
                                          y.FKDersPlanID == dersplanid &&
                                          y.Yil == yil &&
                                          y.EnumDonem == donem &&
                                          y.Iptal == false
                                          select new YazilmaKontrolDto
                                          {
                                              YazilmaID = y.ID,
                                              FKDersGrupID = y.FKDersGrupID.Value,
                                              FKDersPlanAnaID = y.FKDersPlanAnaID.Value,
                                              FKDersPlanID = y.FKDersPlanID.Value
                                          }).FirstOrDefault();
                    return yazilmakontrol;
                }
            }
            catch (Exception)
            {

                return null;
            }
        }
        public static int GetirOnlineSinavTuru(int hocaid, int p)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    //var sinavbul = db.OSinav.FirstOrDefault(x => x.ID == sinavid);
                    //return sinavbul.EklemeTuru;
                    var grupid = db.OGRDersGrupHoca.FirstOrDefault(x => x.ID == hocaid).FKDersGrupID;
                    var sinavgetir = db.OSinavGrup.FirstOrDefault(x => x.PayID == p && x.FKDersGrupID == grupid && x.Silindi == false);
                    if (sinavgetir != null)
                    {
                        var sinavbul = db.OSinav.FirstOrDefault(x => x.ID == sinavgetir.FKSinavID && x.Silindi == false);
                        if (sinavbul != null)
                        {
                            return sinavbul.EklemeTuru;
                        }

                    }
                    return 0;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public static List<PaylarOlcmeVM> GetirPaySinavSoruListesi2(int sinavid, int FKDersGrupHocaID, int FKPaylarID, int Yil, int Donem)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var dersGrup = db.OGRDersGrupHoca.FirstOrDefault(x => x.ID == FKDersGrupHocaID).OGRDersGrup;

                if (sinavid != 0)
                {
                    var sonuc2 = (from soru in db.ABSPaylarOlcme
                                  join ss in db.OSinavSoru on soru.ID equals ss.OlcmeID into fg
                                  from fgi in fg.Where(f => f.SinavID == sinavid && f.MazeretSoru == false && f.Silindi == false).DefaultIfEmpty()
                                  where
                                  soru.FKPaylarID == FKPaylarID &&
                                  soru.Silindi == false
                                  select new PaylarOlcmeVM
                                  {
                                      ID = soru.ID,
                                      FKPaylarID = soru.FKPaylarID,
                                      SoruNo = soru.SoruNo,
                                      SoruPuan = soru.SoruPuan,
                                      FKKoordinatorID = soru.FKKoordinatorID,
                                      Zaman = soru.Zaman,
                                      EnumSinavTuru = soru.EnumSinavTuru,
                                      EnumSoruTipi = soru.EnumSoruTipi,
                                      OSinavSoruID = fgi == null ? 0 : fgi.SoruID,
                                      OSinavSoruBaslik = fgi.Baslik,
                                      SoruIptal = fgi == null ? false : fgi.Iptal
                                      //po.SoruNo,
                                      //aa = fgi == null ? 0 : fgi.SoruID,
                                      //fgi.Baslik
                                  }).ToList();

                    return sonuc2;
                }
                return new List<PaylarOlcmeVM>();
            }
        }
        public static List<PaylarOlcmeVM> GetirPayMazeretSinavSoruListesi(int sinavid, int FKDersGrupHocaID, int FKPaylarID, int Yil, int Donem)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    //var dersGrup = db.OGRDersGrupHoca.FirstOrDefault(x => x.ID == FKDersGrupHocaID).OGRDersGrup;
                    //var sinavid = 0;
                    //var sgrup = db.OSinavGrup.Where(x => x.PayID == FKPaylarID && x.FKDersGrupID == dersGrup.ID).FirstOrDefault();
                    //if (sgrup != null)
                    //{
                    //    sinavid = sgrup.FKSinavID;
                    //}

                    if (sinavid != 0)
                    {

                        var sonuc2 = (from soru in db.ABSPaylarOlcme
                                      join ss in db.OSinavSoru on soru.ID equals ss.OlcmeID into fg
                                      from fgi in fg.Where(f => f.SinavID == sinavid && f.MazeretSoru == true).DefaultIfEmpty()
                                      where
                                      soru.FKPaylarID == FKPaylarID &&
                                      soru.Silindi == false

                                      select new PaylarOlcmeVM
                                      {
                                          ID = soru.ID,
                                          FKPaylarID = soru.FKPaylarID,
                                          SoruNo = soru.SoruNo,
                                          SoruPuan = soru.SoruPuan,
                                          FKKoordinatorID = soru.FKKoordinatorID,
                                          Zaman = soru.Zaman,
                                          EnumSinavTuru = soru.EnumSinavTuru,
                                          EnumSoruTipi = soru.EnumSoruTipi,
                                          OSinavSoruID = fgi == null ? 0 : fgi.SoruID,
                                          OSinavSoruBaslik = fgi.Baslik,
                                          SoruIptal = fgi == null ? false : fgi.Iptal,
                                          //po.SoruNo,
                                          //aa = fgi == null ? 0 : fgi.SoruID,
                                          //fgi.Baslik
                                      }).ToList();

                        return sonuc2;
                    }
                    return null;

                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public static bool SinavIslemSinavKaydet(OSinavVM model)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var oSinav = db.OSinav.FirstOrDefault(x => x.ID == model.SinavID);
                    if (oSinav != null)
                    {
                        oSinav.Aciklama = model.Aciklama;
                        oSinav.Baslangic = model.Baslangic.Value;
                        oSinav.Bitis = model.Bitis.Value;
                        oSinav.Sure = model.Sure.Value;
                        //oSinav.Yayinlandi = 0;
                        oSinav.Silindi = false;
                        oSinav.MazeretSinav = model.Mazeret;
                        if (model.Mazeret == false)
                        {
                            oSinav.TarihMazeretBaslangic = null;
                            oSinav.TarihMazeretBitis = null;
                        }
                        else
                        {
                            oSinav.TarihMazeretBaslangic = model.MazeretBaslangic;
                            oSinav.TarihMazeretBitis = model.MazeretBitis;
                        }
                        db.SaveChanges();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public static bool SinavYayinla(int sinavid)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var sinav = db.OSinav.FirstOrDefault(x => x.ID == sinavid);
                    var sorusayisi = sinav.SoruSayisi;
                    var sinavsorulari = db.OSinavSoru.Where(x => x.SinavID == sinavid && x.Silindi == false && x.MazeretSoru == false).ToList().Count;
                    if (sorusayisi == sinavsorulari && sinav.Baslangic != sinav.Bitis)
                    //if (sinav.Baslangic != sinav.Bitis)
                    {
                        var sonuc = db.OSinav.FirstOrDefault(x => x.ID == sinavid);
                        sonuc.Yayinlandi = 1;
                        db.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public static bool SinavYayindanKaldir(int sinavid)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var sonuc = db.OSinav.FirstOrDefault(x => x.ID == sinavid);
                    sonuc.Yayinlandi = 0;
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public static bool YeniOlcmeSoruEkle(int PayID, int GrupID, int KoordinatorID)
        {
            int ortakDegerlendirmeID = new Int32();
            OGRDersGrup dersGrup = new OGRDersGrup();
            using (UYSv2Entities db = new UYSv2Entities())
            {
                dersGrup = db.OGRDersGrup.FirstOrDefault(x => x.ID == GrupID);
                //ortakDegerlendirmeID = db.ABSOrtakDegerlendirmeGrup.FirstOrDefault(x => x.FKDersGrupID == dersGrup.ID && x.ABSOrtakDegerlendirme.Yil == 2016 && x.ABSOrtakDegerlendirme.Donem == 2).FKOrtakDegerlendirmeID;
                //int oncekiSoruSayisi = db.ABSPaylarOlcme.Count(x => x.Silindi == false && x.FKPaylarID == PayID && x.FKABSOrtakDegerlendirmeID == ortakDegerlendirmeID);
                int oncekiSoruSayisi = db.ABSPaylarOlcme.Count(x => x.Silindi == false && x.FKPaylarID == PayID);
                double ortalamaPuan = 0;
                ortalamaPuan = 100 / (1 + oncekiSoruSayisi);
                db.ABSPaylarOlcme.Where(x => x.FKPaylarID == PayID && x.Silindi == false)
                                     .ToList()
                                     .ForEach(x => x.SoruPuan = (byte)ortalamaPuan);
                db.SaveChanges();
                int soruTip = (int)EnumSoruTipi.ONLINESINAV;

                ABSPaylarOlcme ap = new ABSPaylarOlcme();
                ap.FKPaylarID = PayID;
                //ap.FKABSOrtakDegerlendirmeID = ortakDegerlendirmeID;
                ap.FKKoordinatorID = KoordinatorID;
                ap.SoruNo = Convert.ToByte(oncekiSoruSayisi + 1);
                ap.SoruPuan = (byte)ortalamaPuan;
                ap.Zaman = DateTime.Now;
                ap.EnumSinavTuru = 5;
                ap.EnumSoruTipi = soruTip;
                ap.Silindi = false;
                db.ABSPaylarOlcme.Add(ap);
                db.SaveChanges();

                var sinavid = db.OSinavGrup.FirstOrDefault(x => x.PayID == PayID && x.FKDersGrupID == dersGrup.ID && !x.Silindi.Value).FKSinavID;
                var sinav = db.OSinav.FirstOrDefault(x => x.ID == sinavid);
                sinav.SoruSayisi = sinav.SoruSayisi + 1;
                db.SaveChanges();

                return true;
            }
        }
        #region Online Sınav

        public static int OlcmedenPayGetir(int OlcmeID)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var sonuc = db.ABSPaylarOlcme.FirstOrDefault(x => x.ID == OlcmeID).FKPaylarID;
                    return sonuc.Value;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public static bool OnlineSinavOlustur(string Ad, string Aciklama, DateTime Baslangic, DateTime Bitis, int Sure, int SoruSayisi, bool Yayinlandi, int yil, int enumDonem, int FKPaylarID, int FKKoordinatorID, int FKDersGrupID, int dersGrupHocaID, int sinavTuru, string Ekleyen, int EkleyenID, bool Mazeret, DateTime MazeretBaslangic, DateTime MazeretBitis, int MazeretSure, int EklemeTuru)
        {
            int ortakDegerlendirmeID = new Int32();
            OGRDersGrup dersGrup = new OGRDersGrup();
            using (UYSv2Entities db = new UYSv2Entities())
            {

                dersGrup = db.OGRDersGrup.FirstOrDefault(x => x.ID == FKDersGrupID);
                var anadersid = db.OGRDersGrup.FirstOrDefault(x => x.ID == FKDersGrupID && x.OgretimYili == yil && x.EnumDonem == enumDonem && x.Silindi == false).FKDersPlanAnaID;
                var personelid = db.OGRDersGrupHoca.FirstOrDefault(x => x.ID == dersGrupHocaID).FKPersonelID;
                ortakDegerlendirmeID = db.ABSOrtakDegerlendirmeGrup.FirstOrDefault(x => x.FKDersGrupID == dersGrup.ID && x.ABSOrtakDegerlendirme.Yil == yil && x.ABSOrtakDegerlendirme.Donem == enumDonem).FKOrtakDegerlendirmeID;
                int oncekiSoruSayisi = db.ABSPaylarOlcme.Count(x => x.FKPaylarID == FKPaylarID && x.FKABSOrtakDegerlendirmeID == ortakDegerlendirmeID && x.Silindi == false);
                double ortalamaPuan = 0;
                ortalamaPuan = 100 / (SoruSayisi + oncekiSoruSayisi);
                db.ABSPaylarOlcme.Where(x => x.FKPaylarID == FKPaylarID && x.Silindi == false)
                                     .ToList()
                                     .ForEach(x => x.SoruPuan = (byte)ortalamaPuan);
                db.SaveChanges();

                int soruTip = (int)EnumSoruTipi.ONLINESINAV;

                var sinav = new OSinav();
                sinav.Ad = Ad;
                sinav.Aciklama = Aciklama;
                sinav.Baslangic = Baslangic;
                sinav.Bitis = Bitis;
                sinav.Sure = Sure;
                sinav.SoruSayisi = SoruSayisi;
                sinav.Yayinlandi = (byte)(Yayinlandi ? 1 : 0);
                if (Mazeret == true)
                {
                    sinav.MazeretSinav = Mazeret;
                    sinav.TarihMazeretBaslangic = MazeretBaslangic;
                    sinav.TarihMazeretBitis = MazeretBitis;
                }
                sinav.Ekleyen = Ekleyen;
                sinav.EkleyenID = EkleyenID;
                sinav.EklemeTarihi = DateTime.Now;
                sinav.Guncelleyen = Ekleyen;
                sinav.GuncelleyenID = EkleyenID;
                sinav.GuncellemeTarihi = DateTime.Now;
                sinav.Silindi = false;
                sinav.EklemeTuru = EklemeTuru;
                db.OSinav.Add(sinav);
                db.SaveChanges();

                //if (degerlendirmegruplarinaekle)
                //{
                //var ortakdegerlendirmegruplar = (from od in db.ABSOrtakDegerlendirme
                //                                 join odg in db.ABSOrtakDegerlendirmeGrup on od.ID equals odg.FKOrtakDegerlendirmeID
                //                                 join g in db.OGRDersGrup on odg.FKDersGrupID equals g.ID
                //                                 join br in db.Birimler on g.FKBirimID equals br.ID
                //                                 where
                //                                 od.FKPersonelID == personelid &&
                //                                 od.FKDersPlanAnaID == anadersid

                //                                 select new DegerlendirmeGrup
                //                                 {
                //                                     GrupAd = g.GrupAd,
                //                                     GrupID = odg.FKDersGrupID,
                //                                     OgretimTuru = g.EnumOgretimTur,
                //                                     BirimAd = br.BirimAdi
                //                                 }).OrderBy(x => x.BirimAd).ThenBy(x => x.GrupAd).ThenBy(x => x.OgretimTuru).ToList();
                //foreach (var gp in ortakdegerlendirmegruplar)
                //{
                //    var model = new OSinavGrup();
                //    model.FKSinavID = sinav.ID;
                //    model.FKDersPlanAnaID = anadersid.Value;
                //    model.FKDersPlanID = dersGrup.FKDersPlanID.Value;
                //    model.FKDersGrupID = gp.GrupID;
                //    model.PayID = FKPaylarID;
                //    db.OSinavGrup.Add(model);
                //}
                //db.SaveChanges();

                //}
                //if (tumgruplaraekle)
                //{
                // artık değerlendirme koordinatörü yok ders koordinatörü var ve sınav eklediğinde bütün gruplara eklenecek.
                var tumDersGruplari = db.OGRDersGrup.Where(x => x.FKDersPlanAnaID == anadersid && x.OgretimYili == yil && x.EnumDonem == enumDonem).Select(x => x.ID).ToList();
                foreach (var grup in tumDersGruplari)
                {
                    var model = new OSinavGrup();
                    model.ID = sinav.ID;
                    model.FKDersPlanAnaID = anadersid.Value;
                    model.FKDersPlanID = dersGrup.FKDersPlanID.Value;
                    model.FKDersGrupID = grup;
                    model.PayID = FKPaylarID;
                    model.Silindi = false;
                    db.OSinavGrup.Add(model);
                }
                db.SaveChanges();
                //}

                for (int i = 1 + oncekiSoruSayisi; i <= SoruSayisi + oncekiSoruSayisi; i++)
                {
                    ABSPaylarOlcme ap = new ABSPaylarOlcme();
                    ap.FKPaylarID = FKPaylarID;
                    ap.FKABSOrtakDegerlendirmeID = ortakDegerlendirmeID;
                    ap.FKKoordinatorID = FKKoordinatorID;
                    ap.SoruNo = (byte)i;
                    ap.SoruPuan = (byte)ortalamaPuan;
                    ap.Zaman = DateTime.Now;
                    ap.EnumSinavTuru = sinavTuru;
                    ap.EnumSoruTipi = soruTip;
                    ap.Silindi = false;
                    db.ABSPaylarOlcme.Add(ap);
                }
                db.SaveChanges();
            }
            return true;
        }

        public static bool OnlineSinavDuzenle(int SinavID, string Baslangic, string Bitis, int Sure, int FKPaylarID, int FKKoordinatorID, int FKDersGrupID, int dersGrupHocaID, bool DegerlendirmeGruplarinaEkle, int Yil, int Donem)
        {
            OGRDersGrup dersGrup = new OGRDersGrup();
            using (UYSv2Entities db = new UYSv2Entities())
            {
                dersGrup = db.OGRDersGrup.FirstOrDefault(x => x.ID == FKDersGrupID);
                var anadersid = db.OGRDersGrup.FirstOrDefault(x => x.ID == FKDersGrupID && x.OgretimYili == Yil && x.EnumDonem == Donem && x.Silindi == false).FKDersPlanAnaID;
                var personelid = db.OGRDersGrupHoca.FirstOrDefault(x => x.ID == dersGrupHocaID).FKPersonelID;

                var model = db.OSinav.Where(s => s.ID == SinavID).FirstOrDefault();
                model.Baslangic = Convert.ToDateTime(Baslangic);
                model.Bitis = Convert.ToDateTime(Bitis);
                model.Sure = Sure;
                db.SaveChanges();

                //if (DegerlendirmeGruplarinaEkle)
                //{
                var ortakdegerlendirmegruplar = (from od in db.ABSOrtakDegerlendirme
                                                 join odg in db.ABSOrtakDegerlendirmeGrup on od.ID equals odg.FKOrtakDegerlendirmeID
                                                 join g in db.OGRDersGrup on odg.FKDersGrupID equals g.ID
                                                 join br in db.Birimler on g.FKBirimID equals br.ID
                                                 where
                                                 od.FKPersonelID == personelid &&
                                                 od.FKDersPlanAnaID == anadersid

                                                 select new DegerlendirmeGrup
                                                 {
                                                     GrupAd = g.GrupAd,
                                                     GrupID = odg.FKDersGrupID,
                                                     OgretimTuru = g.EnumOgretimTur,
                                                     BirimAd = br.BirimAdi
                                                 }).OrderBy(x => x.BirimAd).ThenBy(x => x.GrupAd).ThenBy(x => x.OgretimTuru).ToList();
                foreach (var gp in ortakdegerlendirmegruplar)
                {
                    var grupkontrol = db.OSinavGrup.FirstOrDefault(g => g.FKDersPlanAnaID == anadersid.Value && g.FKDersPlanID == dersGrup.FKDersPlanID.Value && g.FKDersGrupID == gp.GrupID && g.PayID == FKPaylarID && g.Silindi == false);
                    if (grupkontrol == null)
                    {
                        var sg = new OSinavGrup();
                        sg.FKSinavID = SinavID;
                        sg.FKDersPlanAnaID = anadersid.Value;
                        sg.FKDersPlanID = dersGrup.FKDersPlanID.Value;
                        sg.FKDersGrupID = gp.GrupID;
                        sg.PayID = FKPaylarID;
                        db.OSinavGrup.Add(sg);
                    }

                }
                db.SaveChanges();

                //}
                return true;
            }
        }
        public static GrupBilgiVM HocaGrupYetki(int FKPaylarID, int FKKoordinatorID, int FKDersGrupID, int dersGrupHocaID, int yil, int donem)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var personelid = db.OGRDersGrupHoca.FirstOrDefault(x => x.ID == dersGrupHocaID).FKPersonelID;
                var anadersid = db.OGRDersGrup.FirstOrDefault(x => x.ID == FKDersGrupID).FKDersPlanAnaID;

                var model = new GrupBilgiVM();

                var ortakdegerlendirmegruplar = (from od in db.ABSOrtakDegerlendirme
                                                 join odg in db.ABSOrtakDegerlendirmeGrup on od.ID equals odg.FKOrtakDegerlendirmeID
                                                 join g in db.OGRDersGrup on odg.FKDersGrupID equals g.ID
                                                 join gh in db.OGRDersGrupHoca on g.ID equals gh.FKDersGrupID
                                                 join per in db.PersonelBilgisi on gh.FKPersonelID equals per.ID
                                                 join ki in db.Kisi on per.FKKisiID equals ki.ID
                                                 join br in db.Birimler on g.FKBirimID equals br.ID
                                                 where
                                                 od.FKPersonelID == personelid &&
                                                 od.FKDersPlanAnaID == anadersid &&
                                                 g.Acik == true &&
                                                 g.Silindi == false &&
                                                 g.OgretimYili == yil &&
                                                 g.EnumDonem == donem

                                                 select new DegerlendirmeGrup
                                                 {
                                                     GrupAd = g.GrupAd,
                                                     GrupID = odg.FKDersGrupID,
                                                     OgretimTuru = g.EnumOgretimTur,
                                                     BirimAd = br.BirimAdi,
                                                     HocaAd = per.UnvanAd + " " + ki.Ad + " " + ki.Soyad,
                                                 }).OrderBy(x => x.BirimAd).ThenBy(x => x.GrupAd).ThenBy(x => x.OgretimTuru).ToList();

                model.GruplarDegerlendirme = ortakdegerlendirmegruplar;


                var koordinator = Sabis.Bolum.Bll.SOURCE.EBS.GENEL.GENELMAIN.DersKoordinatoruMu(anadersid.Value, personelid.Value);
                if (koordinator)
                {
                    var tumgruplar = (from g in db.OGRDersGrup
                                      join br in db.Birimler on g.FKBirimID equals br.ID
                                      where
                                      g.FKDersPlanAnaID == 29275 &&
                                      g.OgretimYili == 2016 &&
                                      g.EnumDonem == 2 &&
                                      g.Silindi == false
                                      select new KoordinatorGrup
                                      {
                                          GrupAd = g.GrupAd,
                                          GrupID = g.ID,
                                          OgretimTuru = g.EnumOgretimTur,
                                          BirimAd = br.BirimAdi
                                      }).OrderBy(x => x.BirimAd).ThenBy(x => x.GrupAd).ThenBy(x => x.OgretimTuru).ToList();
                    model.GruplarKoordinator = tumgruplar;
                }
                return model;
            }
            return null;
        }
        public static OSinavVM OnlineSinavGetir(int FKDersGrupHocaID, int PayID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var grupid = db.OGRDersGrupHoca.FirstOrDefault(x => x.ID == FKDersGrupHocaID).FKDersGrupID;
                var sorgu = (from sg in db.OSinavGrup
                             join s in db.OSinav on sg.FKSinavID equals s.ID
                             where
                             sg.PayID == PayID &&
                             sg.FKDersGrupID == grupid &&
                             sg.Silindi == false
                             select new OSinavVM
                             {
                                 SinavID = s.ID,
                                 Ad = s.Ad,
                                 Baslangic = s.Baslangic,
                                 Bitis = s.Bitis,
                                 Sure = s.Sure,
                                 SoruSayisi = s.SoruSayisi,
                                 //Yayinlandi = s.Yayinlandi,
                                 EklemeTuru = s.EklemeTuru,
                                 Yayinlandi = 1,
                                 PayID = sg.PayID,
                                 Mazeret = s.MazeretSinav,
                                 MazeretBaslangic = s.TarihMazeretBaslangic,
                                 MazeretBitis = s.TarihMazeretBitis
                             }).FirstOrDefault();

                return sorgu;
            }
        }
        public static OSinavSoruEkle OnlineSinavSoruGetir(int soruid, int payID, int gHocaID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var soru = (from s in db.OSinavSoru
                            join dosya in db.OSinavDosya on s.SoruID equals dosya.SoruID into tbl1
                            from dosya in tbl1.DefaultIfEmpty()
                            where s.SoruID == soruid
                            select new
                            {
                                s.SinavID,
                                s.SoruID,
                                s.SoruNo,
                                s.Baslik,
                                s.Cevap1,
                                s.Cevap2,
                                s.Cevap3,
                                s.Cevap4,
                                s.Cevap5,
                                s.DogruCevap,
                                s.Cevap1DosyaKey,
                                s.Cevap2DosyaKey,
                                s.Cevap3DosyaKey,
                                s.Cevap4DosyaKey,
                                s.Cevap5DosyaKey,
                                dosya.DosyaKey,
                                SoruDosyaID = dosya == null ? 0 : dosya.ID,
                                s.Iptal
                            }).FirstOrDefault();
                if (soru != null)
                {
                    var model = new OSinavSoruEkle();
                    model.SinavID = soru.SinavID;
                    model.SoruID = soru.SoruID;
                    model.SoruNo = soru.SoruNo;
                    model.Baslik = soru.Baslik;
                    model.SoruDosyaID = soru.SoruDosyaID;
                    model.SoruDosyaKey = soru.DosyaKey;
                    model.Cevap1 = soru.Cevap1;
                    model.Cevap2 = soru.Cevap2;
                    model.Cevap3 = soru.Cevap3;
                    model.Cevap4 = soru.Cevap4;
                    model.Cevap5 = soru.Cevap5;
                    model.DogruCevap = soru.DogruCevap;
                    model.Cevap1DosyaKey = soru.Cevap1DosyaKey;
                    model.Cevap2DosyaKey = soru.Cevap2DosyaKey;
                    model.Cevap3DosyaKey = soru.Cevap3DosyaKey;
                    model.Cevap4DosyaKey = soru.Cevap4DosyaKey;
                    model.Cevap5DosyaKey = soru.Cevap5DosyaKey;
                    model.PayID = payID;
                    model.GrupHocaID = gHocaID;
                    model.Iptal = soru.Iptal;
                    return model;
                }
                //var soru = db.OSinavSoru.FirstOrDefault(s => s.SoruID == soruid);
                //if (soru != null)
                //{
                //    var model = new OSinavSoruEkle();
                //    model.SinavID = soru.SinavID;
                //    model.SoruID = soru.SoruID;
                //    model.SoruNo = soru.SoruNo;
                //    model.Baslik = soru.Baslik;
                //    model.Cevap1 = soru.Cevap1;
                //    model.Cevap2 = soru.Cevap2;
                //    model.Cevap3 = soru.Cevap3;
                //    model.Cevap4 = soru.Cevap4;
                //    model.Cevap5 = soru.Cevap5;
                //    model.DogruCevap = soru.DogruCevap;
                //    model.PayID = payID;
                //    model.GrupHocaID = gHocaID;
                //    return model;
                //}
                return null;
            }
        }

        public static bool OSoruSil(int soruID)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var sorugrup = db.OsinavSoruGrup.FirstOrDefault(x => x.FKSoruID == soruID);
                    if (sorugrup != null)
                    {
                        db.OsinavSoruGrup.Remove(sorugrup);
                        db.SaveChanges();
                    }
                    var soru = db.OSinavSoru.FirstOrDefault(x => x.SoruID == soruID);
                    db.OSinavSoru.Remove(soru);
                    db.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public static bool OnlineSinavSoruEkle(OSinavSoruEkle model, string KullaniciAdi, int KullaniciID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var soru = new OSinavSoru();
                soru.SinavID = model.SinavID;
                soru.SoruNo = model.PayOlcmeSoruNo.Value;
                soru.Baslik = model.Baslik;
                soru.Cevap1 = model.Cevap1;
                soru.Cevap2 = model.Cevap2;
                soru.Cevap3 = model.Cevap3;
                soru.Cevap4 = model.Cevap4;
                soru.Cevap5 = model.Cevap5;
                soru.DogruCevap = model.DogruCevap;
                soru.Silindi = false;
                soru.Iptal = false;
                soru.Ekleyen = KullaniciAdi;
                soru.EkleyenID = KullaniciID;
                soru.EklemeTarihi = DateTime.Now;
                soru.Guncelleyen = KullaniciAdi;
                soru.GuncelleyenID = KullaniciID;
                soru.GuncellemeTarihi = DateTime.Now;
                db.OSinavSoru.Add(soru);
                db.SaveChanges();

                if (model.SoruDosyaKey != null)
                {
                    var dosya = new OSinavDosya();
                    dosya.SinavID = model.SinavID;
                    dosya.SoruID = soru.SoruID;
                    dosya.DosyaKey = model.SoruDosyaKey;
                    db.OSinavDosya.Add(dosya);
                    db.SaveChanges();
                }

                var PayOlcme = db.ABSPaylarOlcme.Where(p => p.ID == model.PaylarOlcmeID && p.Silindi == false).FirstOrDefault();
                PayOlcme.FKOSinavSoruID = soru.SoruID;
                db.SaveChanges();
                return true;
            }
        }

        public static bool OSoruEkle(OSinavSoruEkle model, string KullaniciAdi, int KullaniciID)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var grupid = db.OGRDersGrupHoca.FirstOrDefault(x => x.ID == model.GrupHocaID).FKDersGrupID;

                    //var soru = new OSinavSoru();
                    //soru.SinavID = model.SinavID;
                    //soru.OlcmeID = model.PaylarOlcmeID;
                    //soru.SoruNo = model.SoruNo;
                    //soru.Baslik = model.Baslik;
                    //soru.Cevap1 = model.Cevap1;
                    //soru.Cevap2 = model.Cevap2;
                    //soru.Cevap3 = model.Cevap3;
                    //soru.Cevap4 = model.Cevap4;
                    //soru.Cevap5 = model.Cevap5;
                    //soru.DogruCevap = model.DogruCevap;
                    //soru.Silindi = false;
                    //soru.Iptal = false;
                    //soru.Ekleyen = KullaniciAdi;
                    //soru.EkleyenID = KullaniciID;
                    //soru.EklemeTarihi = DateTime.Now;
                    //soru.Guncelleyen = KullaniciAdi;
                    //soru.GuncelleyenID = KullaniciID;
                    //soru.GuncellemeTarihi = DateTime.Now;
                    //db.OSinavSoru.Add(soru);
                    //db.SaveChanges();

                    //grup sınav ise
                    if (model.SinavTur == 1)
                    {
                        #region Tüm Gruplarıma Ekle
                        if (model.TumGruplarimaEkle)
                        {
                            var dgHOCA = db.OGRDersGrupHoca.FirstOrDefault(x => x.ID == model.GrupHocaID);
                            var gid = dgHOCA.FKDersGrupID;
                            var dpa = db.OGRDersGrup.FirstOrDefault(x => x.ID == gid).FKDersPlanAnaID;
                            var hocaninGruplari = db.OGRDersGrupHoca.Where(x => x.FKPersonelID == dgHOCA.FKPersonelID && x.OGRDersGrup.FKDersPlanAnaID == dpa && x.OGRDersGrup.OgretimYili == 2016 && x.OGRDersGrup.EnumDonem == 2).Select(x => x.OGRDersGrup.ID).ToList();

                            foreach (var grup in hocaninGruplari)
                            {
                                var sinavid = db.OSinavGrup.FirstOrDefault(x => x.FKDersGrupID == grup && x.PayID == model.PayID && x.Silindi == false).FKSinavID;

                                var tekrarkontrol = db.OSinavSoru.Any(x => x.SinavID == sinavid && x.OlcmeID == model.PaylarOlcmeID && x.Silindi == false);
                                if (!tekrarkontrol)
                                {
                                    var soruu = new OSinavSoru();
                                    soruu.SinavID = sinavid;
                                    soruu.OlcmeID = model.PaylarOlcmeID;
                                    soruu.SoruNo = model.SoruNo;
                                    soruu.Baslik = model.Baslik;
                                    soruu.Cevap1 = model.Cevap1;
                                    soruu.Cevap2 = model.Cevap2;
                                    soruu.Cevap3 = model.Cevap3;
                                    soruu.Cevap4 = model.Cevap4;
                                    soruu.Cevap5 = model.Cevap5;
                                    soruu.DogruCevap = model.DogruCevap;
                                    soruu.Cevap1DosyaKey = model.Cevap1DosyaKey;
                                    soruu.Cevap2DosyaKey = model.Cevap2DosyaKey;
                                    soruu.Cevap3DosyaKey = model.Cevap3DosyaKey;
                                    soruu.Cevap4DosyaKey = model.Cevap4DosyaKey;
                                    soruu.Cevap5DosyaKey = model.Cevap5DosyaKey;
                                    soruu.Silindi = false;
                                    soruu.Iptal = false;
                                    soruu.Ekleyen = KullaniciAdi;
                                    soruu.EkleyenID = KullaniciID;
                                    soruu.EklemeTarihi = DateTime.Now;
                                    soruu.Guncelleyen = KullaniciAdi;
                                    soruu.GuncelleyenID = KullaniciID;
                                    soruu.GuncellemeTarihi = DateTime.Now;
                                    soruu.MazeretSoru = model.MazeretSoru;
                                    db.OSinavSoru.Add(soruu);
                                    db.SaveChanges();


                                    var sorugrup = new OsinavSoruGrup();
                                    sorugrup.FKSinavID = sinavid;
                                    sorugrup.FKGrupID = grup;
                                    sorugrup.FKSoruID = soruu.SoruID;
                                    db.OsinavSoruGrup.Add(sorugrup);
                                    db.SaveChanges();
                                }

                            }
                        }
                        #endregion
                        else
                        {
                            if (model.MazeretSoru == false)
                            {
                                var tekrarkontrol = db.OSinavSoru.Any(x => x.SinavID == model.SinavID && x.OlcmeID == model.PaylarOlcmeID && x.Silindi == false && x.MazeretSoru == false);
                                if (!tekrarkontrol)
                                {
                                    var soru = new OSinavSoru();
                                    soru.SinavID = model.SinavID;
                                    soru.OlcmeID = model.PaylarOlcmeID;
                                    soru.SoruNo = model.SoruNo;
                                    soru.Baslik = model.Baslik;
                                    soru.Cevap1 = model.Cevap1;
                                    soru.Cevap2 = model.Cevap2;
                                    soru.Cevap3 = model.Cevap3;
                                    soru.Cevap4 = model.Cevap4;
                                    soru.Cevap5 = model.Cevap5;
                                    soru.DogruCevap = model.DogruCevap;
                                    soru.Cevap1DosyaKey = model.Cevap1DosyaKey;
                                    soru.Cevap2DosyaKey = model.Cevap2DosyaKey;
                                    soru.Cevap3DosyaKey = model.Cevap3DosyaKey;
                                    soru.Cevap4DosyaKey = model.Cevap4DosyaKey;
                                    soru.Cevap5DosyaKey = model.Cevap5DosyaKey;
                                    soru.Silindi = false;
                                    soru.Iptal = false;
                                    soru.Ekleyen = KullaniciAdi;
                                    soru.EkleyenID = KullaniciID;
                                    soru.EklemeTarihi = DateTime.Now;
                                    soru.Guncelleyen = KullaniciAdi;
                                    soru.GuncelleyenID = KullaniciID;
                                    soru.GuncellemeTarihi = DateTime.Now;
                                    soru.MazeretSoru = model.MazeretSoru;
                                    db.OSinavSoru.Add(soru);
                                    db.SaveChanges();

                                    var sorugrup = new OsinavSoruGrup();
                                    sorugrup.FKSinavID = soru.SinavID;
                                    sorugrup.FKGrupID = grupid;
                                    sorugrup.FKSoruID = soru.SoruID;
                                    db.OsinavSoruGrup.Add(sorugrup);
                                    db.SaveChanges();

                                    if (model.SoruDosyaKey != null)
                                    {
                                        var dosya = new OSinavDosya();
                                        dosya.SinavID = model.SinavID;
                                        dosya.SoruID = soru.SoruID;
                                        dosya.DosyaKey = model.SoruDosyaKey;
                                        db.OSinavDosya.Add(dosya);
                                        db.SaveChanges();
                                    }
                                }
                            }
                            else if (model.MazeretSoru == true)
                            {
                                var tekrarkontrol = db.OSinavSoru.Any(x => x.SinavID == model.SinavID && x.OlcmeID == model.PaylarOlcmeID && x.Silindi == false && x.MazeretSoru == true);
                                if (!tekrarkontrol)
                                {
                                    var soru = new OSinavSoru();
                                    soru.SinavID = model.SinavID;
                                    soru.OlcmeID = model.PaylarOlcmeID;
                                    soru.SoruNo = model.SoruNo;
                                    soru.Baslik = model.Baslik;
                                    soru.Cevap1 = model.Cevap1;
                                    soru.Cevap2 = model.Cevap2;
                                    soru.Cevap3 = model.Cevap3;
                                    soru.Cevap4 = model.Cevap4;
                                    soru.Cevap5 = model.Cevap5;
                                    soru.DogruCevap = model.DogruCevap;
                                    soru.Cevap1DosyaKey = model.Cevap1DosyaKey;
                                    soru.Cevap2DosyaKey = model.Cevap2DosyaKey;
                                    soru.Cevap3DosyaKey = model.Cevap3DosyaKey;
                                    soru.Cevap4DosyaKey = model.Cevap4DosyaKey;
                                    soru.Cevap5DosyaKey = model.Cevap5DosyaKey;
                                    soru.Silindi = false;
                                    soru.Iptal = false;
                                    soru.Ekleyen = KullaniciAdi;
                                    soru.EkleyenID = KullaniciID;
                                    soru.EklemeTarihi = DateTime.Now;
                                    soru.Guncelleyen = KullaniciAdi;
                                    soru.GuncelleyenID = KullaniciID;
                                    soru.GuncellemeTarihi = DateTime.Now;
                                    soru.MazeretSoru = model.MazeretSoru;
                                    db.OSinavSoru.Add(soru);
                                    db.SaveChanges();

                                    var sorugrup = new OsinavSoruGrup();
                                    sorugrup.FKSinavID = soru.SinavID;
                                    sorugrup.FKGrupID = grupid;
                                    sorugrup.FKSoruID = soru.SoruID;
                                    db.OsinavSoruGrup.Add(sorugrup);
                                    db.SaveChanges();

                                    if (model.SoruDosyaKey != null)
                                    {
                                        var dosya = new OSinavDosya();
                                        dosya.SinavID = model.SinavID;
                                        dosya.SoruID = soru.SoruID;
                                        dosya.DosyaKey = model.SoruDosyaKey;
                                        db.OSinavDosya.Add(dosya);
                                        db.SaveChanges();
                                    }
                                }
                            }
                            

                        }
                    }
                    //genel sınav ise
                    if (model.SinavTur == 2)
                    {
                        if (model.MazeretSoru == false)
                        {
                            var tekrarkontrol = db.OSinavSoru.Any(x => x.SinavID == model.SinavID && x.OlcmeID == model.PaylarOlcmeID && x.Silindi == false && x.MazeretSoru == false);
                            if (!tekrarkontrol)
                            {
                                var soru = new OSinavSoru();
                                soru.SinavID = model.SinavID;
                                soru.OlcmeID = model.PaylarOlcmeID;
                                soru.SoruNo = model.SoruNo;
                                soru.Baslik = model.Baslik;
                                soru.Cevap1 = model.Cevap1;
                                soru.Cevap2 = model.Cevap2;
                                soru.Cevap3 = model.Cevap3;
                                soru.Cevap4 = model.Cevap4;
                                soru.Cevap5 = model.Cevap5;
                                soru.DogruCevap = model.DogruCevap;
                                soru.Cevap1DosyaKey = model.Cevap1DosyaKey;
                                soru.Cevap2DosyaKey = model.Cevap2DosyaKey;
                                soru.Cevap3DosyaKey = model.Cevap3DosyaKey;
                                soru.Cevap4DosyaKey = model.Cevap4DosyaKey;
                                soru.Cevap5DosyaKey = model.Cevap5DosyaKey;
                                soru.Silindi = false;
                                soru.Iptal = false;
                                soru.Ekleyen = KullaniciAdi;
                                soru.EkleyenID = KullaniciID;
                                soru.EklemeTarihi = DateTime.Now;
                                soru.Guncelleyen = KullaniciAdi;
                                soru.GuncelleyenID = KullaniciID;
                                soru.GuncellemeTarihi = DateTime.Now;
                                soru.MazeretSoru = model.MazeretSoru;
                                db.OSinavSoru.Add(soru);
                                db.SaveChanges();

                                var sorugrup = new OsinavSoruGrup();
                                sorugrup.FKSinavID = soru.SinavID;
                                sorugrup.FKSoruID = soru.SoruID;
                                db.OsinavSoruGrup.Add(sorugrup);
                                db.SaveChanges();

                                if (model.SoruDosyaKey != null)
                                {
                                    var dosya = new OSinavDosya();
                                    dosya.SinavID = model.SinavID;
                                    dosya.SoruID = soru.SoruID;
                                    dosya.DosyaKey = model.SoruDosyaKey;
                                    db.OSinavDosya.Add(dosya);
                                    db.SaveChanges();
                                }
                            }
                        }
                        else if (model.MazeretSoru == true)
                        {
                            var tekrarkontrol = db.OSinavSoru.Any(x => x.SinavID == model.SinavID && x.OlcmeID == model.PaylarOlcmeID && x.Silindi == false && x.MazeretSoru == true);
                            if (!tekrarkontrol)
                            {
                                var soru = new OSinavSoru();
                                soru.SinavID = model.SinavID;
                                soru.OlcmeID = model.PaylarOlcmeID;
                                soru.SoruNo = model.SoruNo;
                                soru.Baslik = model.Baslik;
                                soru.Cevap1 = model.Cevap1;
                                soru.Cevap2 = model.Cevap2;
                                soru.Cevap3 = model.Cevap3;
                                soru.Cevap4 = model.Cevap4;
                                soru.Cevap5 = model.Cevap5;
                                soru.DogruCevap = model.DogruCevap;
                                soru.Cevap1DosyaKey = model.Cevap1DosyaKey;
                                soru.Cevap2DosyaKey = model.Cevap2DosyaKey;
                                soru.Cevap3DosyaKey = model.Cevap3DosyaKey;
                                soru.Cevap4DosyaKey = model.Cevap4DosyaKey;
                                soru.Cevap5DosyaKey = model.Cevap5DosyaKey;
                                soru.Silindi = false;
                                soru.Iptal = false;
                                soru.Ekleyen = KullaniciAdi;
                                soru.EkleyenID = KullaniciID;
                                soru.EklemeTarihi = DateTime.Now;
                                soru.Guncelleyen = KullaniciAdi;
                                soru.GuncelleyenID = KullaniciID;
                                soru.GuncellemeTarihi = DateTime.Now;
                                soru.MazeretSoru = model.MazeretSoru;
                                db.OSinavSoru.Add(soru);
                                db.SaveChanges();

                                var sorugrup = new OsinavSoruGrup();
                                sorugrup.FKSinavID = soru.SinavID;
                                sorugrup.FKSoruID = soru.SoruID;
                                db.OsinavSoruGrup.Add(sorugrup);
                                db.SaveChanges();

                                if (model.SoruDosyaKey != null)
                                {
                                    var dosya = new OSinavDosya();
                                    dosya.SinavID = model.SinavID;
                                    dosya.SoruID = soru.SoruID;
                                    dosya.DosyaKey = model.SoruDosyaKey;
                                    db.OSinavDosya.Add(dosya);
                                    db.SaveChanges();
                                }
                            }
                        }


                    }

                    return true;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public static bool OSoruDuzenle(OSinavSoruEkle model, string KullaniciAdi, int KullaniciID)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var soru = db.OSinavSoru.FirstOrDefault(x => x.SoruID == model.SoruID);
                    soru.Baslik = model.Baslik;
                    soru.Cevap1 = model.Cevap1;
                    soru.Cevap2 = model.Cevap2;
                    soru.Cevap3 = model.Cevap3;
                    soru.Cevap4 = model.Cevap4;
                    soru.Cevap5 = model.Cevap5;
                    soru.DogruCevap = model.DogruCevap;
                    soru.Cevap1DosyaKey = model.Cevap1DosyaKey;
                    soru.Cevap2DosyaKey = model.Cevap2DosyaKey;
                    soru.Cevap3DosyaKey = model.Cevap3DosyaKey;
                    soru.Cevap4DosyaKey = model.Cevap4DosyaKey;
                    soru.Cevap5DosyaKey = model.Cevap5DosyaKey;
                    soru.Guncelleyen = KullaniciAdi;
                    soru.GuncelleyenID = KullaniciID;
                    soru.GuncellemeTarihi = DateTime.Now;
                    db.SaveChanges();

                    if (model.SoruDosyaKey != null)
                    {
                        var dosya = new OSinavDosya();
                        dosya.SinavID = soru.SinavID;
                        dosya.SoruID = soru.SoruID;
                        dosya.DosyaKey = model.SoruDosyaKey;
                        db.OSinavDosya.Add(dosya);
                        db.SaveChanges();
                    }

                    if (model.Iptal)
                    {
                        var sr = db.OSinavSoru.FirstOrDefault(x => x.SoruID == model.SoruID);
                        sr.Iptal = true;
                        db.SaveChanges();

                        var cevaplariptal = db.OSinavCevap.Where(x => x.SoruID == model.SoruID);
                        foreach (var cvpiptal in cevaplariptal)
                        {
                            var cvp = db.OSinavCevap.FirstOrDefault(x => x.CevapID == cvpiptal.CevapID);
                            cvp.Iptal = true;
                        }
                        db.SaveChanges();

                    }
                    if (!model.Iptal)
                    {
                        var sr = db.OSinavSoru.FirstOrDefault(x => x.SoruID == model.SoruID);
                        if (sr.Iptal == true)
                        {
                            sr.Iptal = false;
                            db.SaveChanges();

                            var cevaplariptal = db.OSinavCevap.Where(x => x.SoruID == model.SoruID);
                            foreach (var cvpiptal in cevaplariptal)
                            {
                                var cvp = db.OSinavCevap.FirstOrDefault(x => x.CevapID == cvpiptal.CevapID);
                                cvp.Iptal = false;
                            }
                            db.SaveChanges();
                        }
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public static bool OnlineSinavSoruDuzenle(OSinavSoruEkle model, string KullaniciAdi, int KullaniciID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var soru = db.OSinavSoru.FirstOrDefault(s => s.SoruID == model.SoruID);
                soru.Baslik = model.Baslik;
                soru.Cevap1 = model.Cevap1;
                soru.Cevap2 = model.Cevap2;
                soru.Cevap3 = model.Cevap3;
                soru.Cevap4 = model.Cevap4;
                soru.Cevap5 = model.Cevap5;
                soru.DogruCevap = model.DogruCevap;
                soru.Guncelleyen = KullaniciAdi;
                soru.GuncelleyenID = KullaniciID;
                soru.GuncellemeTarihi = DateTime.Now;
                db.SaveChanges();

                if (model.SoruDosyaKey != null)
                {
                    var dosya = new OSinavDosya();
                    dosya.SinavID = model.SinavID;
                    dosya.SoruID = soru.SoruID;
                    dosya.DosyaKey = model.SoruDosyaKey;
                    db.OSinavDosya.Add(dosya);
                    db.SaveChanges();
                }
                return true;
            }
        }

        //public static OSinavSoruEkle OnlineSinavSoruSil(int id, int PayID, int SoruID)
        //{
        //    try
        //    {
        //        using (UYSv2Entities db = new UYSv2Entities())
        //        {
        //            var soru = db.OSinavSoru.FirstOrDefault(x => x.SoruID == SoruID);
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        throw;
        //    }
        //}
        public static int SoruNoBul(int payid, int ortakdegid)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var query = from s in db.ABSPaylarOlcme
                            where
                            s.FKPaylarID == payid &&
                            s.FKABSOrtakDegerlendirmeID == ortakdegid
                            select s.SoruNo.Value;
                if (query.Count() > 0)
                {
                    return query.Max() + 1;
                }
                return 1;
            }
        }
        public static OSinavSoruDurum OnlineSinavSoruSil(int SoruID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var sorgu = (from so in db.OSinavSoru
                             join sc in db.OSinavCevap on so.SoruID equals sc.SoruID into tbl1
                             from sc in tbl1.DefaultIfEmpty()
                             select new OSinavSoruDurum
                             {
                                 SoruID = so.SoruID,
                                 Kullanimda = sc == null ? true : false
                             }
                            ).FirstOrDefault();
                return sorgu;
            }
        }
        public static bool OnlineSinavSoruSilOnay(int SoruID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var payid = 0;
                var degid = 0;

                var payolcme = db.ABSPaylarOlcme.FirstOrDefault(s => s.FKOSinavSoruID == SoruID);
                payid = payolcme.FKPaylarID.Value;
                degid = payolcme.FKABSOrtakDegerlendirmeID.Value;
                payolcme.Silindi = true;
                db.SaveChanges();

                //artık soru silinebilir.
                var soru = db.OSinavSoru.FirstOrDefault(s => s.SoruID == SoruID);
                soru.Silindi = true;
                db.SaveChanges();

                var payolcme2 = db.ABSPaylarOlcme.Where(p => p.FKABSOrtakDegerlendirmeID == degid && p.FKPaylarID == payid && p.Silindi == false).ToList();
                int i = 1;
                foreach (var polcme in payolcme2)
                {
                    var p = db.ABSPaylarOlcme.FirstOrDefault(po => po.ID == polcme.ID);
                    p.SoruNo = Convert.ToByte(i);
                    i++;
                }
                db.SaveChanges();
                return true;
            }
        }

        public static List<OgrenciSinav> OgrenciSinavListe(int FKDersGrupID, int FKOgrenciID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var sorgu = (from sg in db.OSinavGrup
                             join s in db.OSinav on sg.FKSinavID equals s.ID
                             join so in db.OSinavOgrenci on new { SINAVID = s.ID, OGRENCIID = FKOgrenciID } equals new { SINAVID = so.SinavID, OGRENCIID = so.OgrenciID } into tbl
                             from so in tbl.DefaultIfEmpty()
                             where
                             s.Yayinlandi == 1 &&
                             s.Silindi == false &&
                             sg.Silindi == false &&
                             sg.FKDersGrupID == FKDersGrupID
                             select new OgrenciSinav
                             {
                                 SinavID = s.ID,
                                 SinavAd = s.Ad,
                                 BaslangicTarih = s.Baslangic,
                                 BitisTarih = s.Bitis,
                                 TarihKayit = s.GuncellemeTarihi.HasValue ? s.GuncellemeTarihi.Value : s.EklemeTarihi,
                                 TarihMazeretBaslangic = s.TarihMazeretBaslangic,
                                 TarihMazeretBitis = s.TarihMazeretBitis,
                                 SinavSure = s.Sure,
                                 DersGrupID = sg.FKDersGrupID,
                                 DersAd = sg.OGRDersPlanAna.DersAd,
                                 //Durum = so == null ? SinavDurum.GirisYapilmadi : so.Durum == 1 ? SinavDurum.DevamEdiyor : so.Durum == 0 ? SinavDurum.GirisYapilmadi : SinavDurum.Tamamlandi,
                                 //Sonuc = so == null ? "Giriş Yapılmadı" : so.Durum == 1 ? "Devam Ediyor" : so.Durum == 0 ? "Giriş Yapılmadı" : "Tamamlandı :" + so.Notu.ToString()
                                 Durum = so == null ? SinavDurum.GirisYapilmadi : (SinavDurum)so.OturumDurum
                             }).ToList();

                foreach (var s in sorgu)
                {
                    s.Sonuc = Sabis.Enum.Utils.GetEnumDescription((SinavDurum)s.Durum);
                }

                return sorgu;
            }
        }
        public static List<OgrenciSinav> OgrenciSinavListe(int ogrenciID, int yil, int donem)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var sorgu = (from sg in db.OSinavGrup.Where(x => x.Silindi == false)
                             join s in db.OSinav.Where(x => !x.Silindi) on sg.FKSinavID equals s.ID
                             join so in db.OSinavOgrenci on new { SINAVID = s.ID, OGRENCIID = ogrenciID } equals new { SINAVID = so.SinavID, OGRENCIID = so.OgrenciID } into tbl
                             join yazilma in db.OGROgrenciYazilma.Where(x => x.FKOgrenciID == ogrenciID && !x.Iptal && x.Yil == yil && x.EnumDonem == donem) on sg.FKDersGrupID equals yazilma.FKDersGrupID
                             from so in tbl.DefaultIfEmpty()
                             select new OgrenciSinav
                             {
                                 SinavID = s.ID,
                                 SinavAd = s.Ad,
                                 BaslangicTarih = s.Baslangic,
                                 BitisTarih = s.Bitis,
                                 TarihKayit = s.GuncellemeTarihi.HasValue ? s.GuncellemeTarihi.Value : s.EklemeTarihi,
                                 TarihMazeretBaslangic = s.TarihMazeretBaslangic,
                                 TarihMazeretBitis = s.TarihMazeretBitis,
                                 SinavSure = s.Sure,
                                 DersGrupID = sg.FKDersGrupID,
                                 DersAd = sg.OGRDersPlanAna.DersAd,
                                 //Durum = so == null ? SinavDurum.GirisYapilmadi : so.Durum == 1 ? SinavDurum.DevamEdiyor : so.Durum == 0 ? SinavDurum.GirisYapilmadi : SinavDurum.Tamamlandi,
                                 //Sonuc = so == null ? "Giriş Yapılmadı" : so.Durum == 1 ? "Devam Ediyor" : so.Durum == 0 ? "Giriş Yapılmadı" : "Tamamlandı "
                                 Durum = so == null ? SinavDurum.GirisYapilmadi : (SinavDurum)so.OturumDurum,
                                 Yayinlandi = s.Yayinlandi == 1 ? true : false
                             }).ToList();
                foreach (var s in sorgu)
                {
                    s.Sonuc = Sabis.Enum.Utils.GetEnumDescription((SinavDurum)s.Durum);
                }
                return sorgu;
            }
        }

        //public static List<OSinavRapor_Result> OgrenciSinavRapor(int dersgruphocaid, int payid)
        //{
        //    using (UYSv2Entities db = new UYSv2Entities())
        //    {
        //        var sorgu = db.OSinavRapor(dersgruphocaid, payid).ToList();
        //        return sorgu;
        //    }
        //}

        public static bool SoruResimSil(int id)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var soruresim = db.OSinavDosya.FirstOrDefault(d => d.ID == id);
                    db.OSinavDosya.Remove(soruresim);
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {

                return false;
            }

        }

        public static bool SoruCevapResimSil(int soruid, int cevapno)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var model = db.OSinavSoru.FirstOrDefault(x => x.SoruID == soruid);
                    if (cevapno == 1)
                    {
                        model.Cevap1DosyaKey = null;
                    }
                    else if (cevapno == 2)
                    {
                        model.Cevap2DosyaKey = null;
                    }
                    else if (cevapno == 3)
                    {
                        model.Cevap3DosyaKey = null;
                    }
                    else if (cevapno == 4)
                    {
                        model.Cevap4DosyaKey = null;
                    }
                    else if (cevapno == 5)
                    {
                        model.Cevap5DosyaKey = null;
                    }
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public static bool SinavHakVer(int id)
        {
            try
            {
                //using (UYSv2Entities db = new UYSv2Entities())
                //{
                //    // 1 cevaplar silindi yapılacak.
                //    var cevaplar = db.OSinavCevap.Where(x => x.OturumID == id).ToList();
                //    foreach (var cevap in cevaplar)
                //    {
                //        var cvp = db.OSinavCevap.FirstOrDefault(x => x.CevapID == cevap.CevapID);
                //        if (cvp != null)
                //        {
                //            cvp.Silindi = true;
                //        }                
                //    }
                //    db.SaveChanges();

                //    // 2 oturum silindi yapılacak.
                //    var oturum = db.OSinavOturum.FirstOrDefault(x => x.OturumID == id);
                //    if (oturum != null)
                //    {
                //        oturum.Silindi = true;
                //    }                
                //    db.SaveChanges();

                //    // 3 sinavogrenci silindi yapılacak.
                //    var sinavogr = db.OSinavOgrenci.FirstOrDefault(x => x.OturumId == id);
                //    if (sinavogr != null)
                //    {
                //        sinavogr.Notu = null;
                //        sinavogr.OturumId = null;
                //    }  
                //    db.SaveChanges();

                //    return true;
                //}
                return true;
            }
            catch (Exception ex)
            {

                return false;
            }
        }
        #endregion
        public static bool SinavSil(int FKPaylarID, int FKDersGrupID, int FKKoordinatorID, int yil, int enumDonem, string kullanici, string IP, int? birimID)
        {
            //if (SoruNotGirilmisMi(FKPaylarID, FKDersGrupID, yil, enumDonem) || NotGirilmisMi(FKPaylarID, yil, enumDonem, FKDersGrupID))
            //{
            //    return false;
            //}
            OGRDersPlanAna dersPlanAna = new OGRDersPlanAna();
            OGRDersPlan dersPlan = new OGRDersPlan();
            try
            {
                //int ortakDegerlendirmeID = new Int32();
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    //ortakDegerlendirmeID = db.ABSOrtakDegerlendirmeGrup.FirstOrDefault(x => x.FKDersGrupID == FKDersGrupID && x.ABSOrtakDegerlendirme.Yil == yil && x.ABSOrtakDegerlendirme.Donem == enumDonem).FKOrtakDegerlendirmeID;

                    dersPlanAna = db.OGRDersPlanAna.FirstOrDefault(x => x.ID == db.ABSPaylar.FirstOrDefault(y => !y.Silindi && y.ID == FKPaylarID).FKDersPlanAnaID);
                    dersPlan = db.OGRDersPlan.FirstOrDefault(x => x.FKDersPlanAnaID == dersPlanAna.ID && x.Yil == yil);
                    //var soruListe = db.ABSPaylarOlcme.Where(x => x.FKPaylarID == FKPaylarID && x.FKABSOrtakDegerlendirmeID == ortakDegerlendirmeID && x.Silindi == false).ToList();
                    var soruListe = db.ABSPaylarOlcme.Where(x => x.FKPaylarID == FKPaylarID && x.Silindi == false).ToList();

                    foreach (var item in soruListe)
                    {
                        foreach (var subItem1 in db.ABSPaylarOlcmeOgrenme.Where(x => x.FKPaylarOlcmeID == item.ID))
                        {
                            db.ABSPaylarOlcmeOgrenme.Remove(subItem1);
                        }
                        db.SaveChanges();

                        foreach (var subItem2 in db.ABSPaylarOlcmeProgram.Where(x => x.FKPaylarOlcmeID == item.ID))
                        {
                            db.ABSPaylarOlcmeProgram.Remove(subItem2);
                        }
                        db.SaveChanges();

                        item.Silindi = true;
                        item.SilindiTarih = DateTime.Now;
                        item.SGKullanici = kullanici;
                        item.SGIP = IP;

                        db.SaveChanges();
                    }
                    //onlinesınav tablosunu da temizle
                    //önce gruplar
                    var onlinesinavgruplar = db.OSinavGrup.Where(g => g.FKDersPlanAnaID == dersPlanAna.ID && g.PayID == FKPaylarID && g.Silindi == false).ToList();
                    if (onlinesinavgruplar != null && onlinesinavgruplar.Count() > 0)
                    {
                        var sinavid = onlinesinavgruplar.FirstOrDefault().FKSinavID;
                        //db.OSinavGrup.RemoveRange(onlinesinavgruplar);
                        //db.SaveChanges();
                        foreach (var sg in onlinesinavgruplar)
                        {
                            var silinecekgrup = db.OSinavGrup.FirstOrDefault(x => x.ID == sg.ID);
                            silinecekgrup.Silindi = true;
                        }
                        db.SaveChanges();

                        var sorular = db.OSinavSoru.Where(s => s.SinavID == sinavid).ToList();
                        if (sorular != null && sorular.Count() > 0)
                        {
                            foreach (var soru in sorular)
                            {
                                var s = db.OSinavSoru.FirstOrDefault(so => so.SoruID == soru.SoruID);
                                s.Silindi = true;
                            }
                            db.SaveChanges();
                        }
                        var cevaplar = db.OSinavCevap.Where(x => x.SinavID == sinavid).ToList();
                        if (cevaplar != null && cevaplar.Count() > 0)
                        {
                            foreach (var cevap in cevaplar)
                            {
                                var c = db.OSinavCevap.FirstOrDefault(x => x.CevapID == cevap.CevapID);
                                c.Silindi = true;
                            }
                            db.SaveChanges();
                        }
                        var enrollments = db.OSinavOgrenci.Where(x => x.SinavID == sinavid);
                        if (enrollments != null && enrollments.Count() > 0)
                        {
                            foreach (var enrollment in enrollments)
                            {
                                var e = db.OSinavOgrenci.FirstOrDefault(x => x.EID == enrollment.EID);
                                e.Silindi = true;
                            }
                            db.SaveChanges();
                        }

                        var osinav = db.OSinav.FirstOrDefault(s => s.ID == sinavid);
                        osinav.Silindi = true;
                        db.SaveChanges();
                    }
                }

                using (KagitOkumaEntities db2 = new KagitOkumaEntities())
                {

                    //if (db2.Sinav.Any(x => x.FKDersPlanAnaID == dersPlanAna.ID && x.FKDersPlanID == dersPlan.ID && x.FKABSPaylarID == FKPaylarID && x.FKABSOrtakDegerlendirmeID == ortakDegerlendirmeID && x.Silindi == false))
                    if (db2.Sinav.Any(x => x.FKDersPlanAnaID == dersPlanAna.ID && x.FKDersPlanID == dersPlan.ID && x.FKABSPaylarID == FKPaylarID && x.Silindi == false))
                    {
                        //var sinav = db2.Sinav.FirstOrDefault(x => x.FKDersPlanAnaID == dersPlanAna.ID && x.FKDersPlanID == dersPlan.ID && x.FKABSPaylarID == FKPaylarID && x.FKABSOrtakDegerlendirmeID == ortakDegerlendirmeID && x.Silindi == false);
                        var sinav = db2.Sinav.FirstOrDefault(x => x.FKABSPaylarID == FKPaylarID && x.Silindi == false);
                        sinav.Silindi = true;
                        sinav.SilindiTarih = DateTime.Now;
                        sinav.SilenKullanici = kullanici;
                        sinav.SilenIP = IP;

                        if (db2.SinavKagit.Any(x => x.FKSinavID == sinav.ID))
                        {
                            var sinavKagit = db2.SinavKagit.Where(x => x.FKSinavID == sinav.ID);

                            foreach (var item in sinavKagit)
                            {
                                item.Silindi = true;
                                item.SilindiTarih = DateTime.Now;
                                item.SilenKullanici = kullanici;
                                item.SilenIP = IP;
                            }
                        }
                    }
                    db2.SaveChanges();

                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool KaydetProgramCiktilari(List<int> programCiktilari, int FKPaylarOlcmeID)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    foreach (var item in programCiktilari)
                    {
                        if (!db.ABSPaylarOlcmeProgram.Any(x => x.FKPaylarOlcmeID == FKPaylarOlcmeID && x.FKProgramCiktiID == item))
                        {
                            ABSPaylarOlcmeProgram yeniProgramCikti = new ABSPaylarOlcmeProgram();
                            yeniProgramCikti.FKPaylarOlcmeID = FKPaylarOlcmeID;
                            yeniProgramCikti.FKProgramCiktiID = item;
                            db.ABSPaylarOlcmeProgram.Add(yeniProgramCikti);
                            db.SaveChanges();
                        }
                    }
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool KaydetOgrenmeCiktilari(List<int> ogrenmeCiktilari, int FKPaylarOlcmeID)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    foreach (var item in ogrenmeCiktilari)
                    {
                        if (!db.ABSPaylarOlcmeOgrenme.Any(x => x.FKPaylarOlcmeID == FKPaylarOlcmeID && x.FKOgrenmeCiktiID == item))
                        {
                            ABSPaylarOlcmeOgrenme yeniOgrenmeCikti = new ABSPaylarOlcmeOgrenme();
                            yeniOgrenmeCikti.FKPaylarOlcmeID = FKPaylarOlcmeID;
                            yeniOgrenmeCikti.FKOgrenmeCiktiID = item;
                            db.ABSPaylarOlcmeOgrenme.Add(yeniOgrenmeCikti);
                            db.SaveChanges();
                        }
                    }
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool OgrenmeCiktiSil(int ID)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    db.ABSPaylarOlcmeOgrenme.Remove(db.ABSPaylarOlcmeOgrenme.Find(ID));
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool ProgramCiktiSil(int ID)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    db.ABSPaylarOlcmeProgram.Remove(db.ABSPaylarOlcmeProgram.Find(ID));
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool SoruPuanGuncelle(List<PaylarOlcmeVM> puanList)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    foreach (var item in puanList)
                    {
                        ABSPaylarOlcme soru = db.ABSPaylarOlcme.FirstOrDefault(x => x.ID == item.ID && x.Silindi == false);
                        soru.SoruPuan = (double)item.SoruPuan;
                        db.SaveChanges();
                    }
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        #region SINAV NOT İŞLEMLERİ
        public static Dictionary<int, double> GetirMutlakOrtalamaListe(int fkDersGrupID, IQueryable<ABSPaylar> payList, List<int> ogrIDList, IQueryable<ABSOgrenciNot> notList)
        {
            byte yiliciOran = payList.FirstOrDefault(x => x.EnumCalismaTip == 11).Oran.Value;
            byte yilsonuOran = payList.FirstOrDefault(x => x.EnumCalismaTip == 12).Oran.Value;

            var yiliciNotList = notList.Where(y => y.ABSPaylar.EnumCalismaTip != 12 && y.ABSPaylar.EnumCalismaTip != 11);
            var yilsonuNotList = notList.Where(y => y.ABSPaylar.EnumCalismaTip == 12);

            bool yiliciDurum = false;
            bool yilsonuDurum = false;

            if (yiliciNotList != null && yiliciNotList.Count() > 0)
            {
                yiliciDurum = true;
            }
            if (yilsonuNotList != null && yilsonuNotList.Count() > 0)
            {
                yilsonuDurum = true;
            }

            Dictionary<int, double> dictMutlakOrtalamaList = new Dictionary<int, double>();
            foreach (var ogrenciID in ogrIDList)
            {
                double yiliciOrt = yiliciDurum ? (yiliciNotList.Where(x => x.FKOgrenciID == ogrenciID).Sum(x => (x.NotDeger * x.ABSPaylar.Oran.Value) / 100) * yiliciOran) / 100 : 0;
                double yilsonuOrt = yilsonuDurum ? (yilsonuNotList.FirstOrDefault(x => x.FKOgrenciID == ogrenciID).NotDeger * yilsonuOran) / 100 : 0;
                dictMutlakOrtalamaList.Add(ogrenciID, yiliciOrt + yilsonuOrt);

            }
            return dictMutlakOrtalamaList;
        }
        public static List<SinavOgrenciListeVM> ListeleSinavOgrenciNotlari(int dersGrupID, int payID)
        {
            UYSv2Entities db = new UYSv2Entities();
            KagitOkumaEntities db2 = new KagitOkumaEntities();
            var sinav = db2.Sinav.FirstOrDefault(x => x.FKABSPaylarID == payID && x.Silindi == false);
            List<string> kagitList = new List<string>();
            if (sinav != null)
            {
                kagitList = db2.SinavKagit.Where(x => x.FKSinavID == sinav.ID && x.OgrenciGorebilir == true).Select(x => x.OgrenciNo).ToList();
            }

            List<SinavOgrenciListeVM> liste = new List<SinavOgrenciListeVM>();
            //int ortakDegerlendirmeID = db.ABSOrtakDegerlendirmeGrup.FirstOrDefault(x => x.FKDersGrupID == dersGrupID).FKOrtakDegerlendirmeID;
            ABSPaylar pay = db.ABSPaylar.FirstOrDefault(d => !d.Silindi && d.ID == payID);
            List<ABSPaylarOlcme> soruListe = db.ABSPaylarOlcme.Where(x => x.FKPaylarID == pay.ID && x.Silindi == false).ToList();


            List<OOdevOgrenci> odevDokumanList = new List<OOdevOgrenci>();
            bool odevMi = false;
            int? odevID = null;
            if (pay.EnumCalismaTip == (int)EnumCalismaTip.Odev && db.OOdevGrup.Any(x => x.FKOGRDersGrupID == dersGrupID && x.OOdev.FKAbsPaylarID == payID && x.OOdev.Silindi == false))
            {
                OOdev odev = db.OOdevGrup.FirstOrDefault(x => x.FKOGRDersGrupID == dersGrupID && x.OOdev.FKAbsPaylarID == payID && x.OOdev.Silindi == false).OOdev;
                //soruListe = soruListe.Where(x => x.FKOOdevID == odev.ID).ToList();

                odevDokumanList = db.OOdevOgrenci.Where(x => x.FKOOdevID == odev.ID).ToList();
                odevMi = true;
                odevID = odev.ID;
            }

            int soruSayi = soruListe.Count();

            var sorgu = db.OGROgrenciYazilma.Where(x => x.FKDersGrupID == dersGrupID && !x.Iptal && x.OGRKimlik.EnumOgrDurum == (int)Sabis.Enum.EnumOgrDurum.NORMAL).Select(x => new
            {
                Numara = x.OGRKimlik.Numara,
                Ad = x.OGRKimlik.Kisi.Ad,
                Soyad = x.OGRKimlik.Kisi.Soyad,
                OgrenciID = x.FKOgrenciID.Value,
                DersPlanAnaID = x.FKDersPlanAnaID.Value,
                DersGrupID = x.FKDersGrupID.Value,
                DersPlanID = x.OGRDersGrup.FKDersPlanID.Value,
                YazilmaID = x.ID,
                KullaniciAdi = x.OGRKimlik.Kullanici1.FirstOrDefault().KullaniciAdi,
                ButunlemeMi = x.ButunlemeMi
            });

            if (pay.EnumCalismaTip == (int)Sabis.Enum.EnumCalismaTip.Butunleme)
            {
                sorgu = sorgu.Where(x => x.ButunlemeMi == true);
            }

            var ogrIDList = sorgu.Select(x => x.OgrenciID);
            var notList = db.ABSOgrenciNot.Where(x => x.FKAbsPaylarID == payID && ogrIDList.Contains(x.FKOgrenciID) && x.Silindi == false);
            var notDetayList = db.ABSOgrenciNotDetay.Where(x => x.FKABSPaylarID == payID && ogrIDList.Contains(x.FKOgrenciID.Value) && x.Silindi == false);


            liste = sorgu.Select(x => new SinavOgrenciListeVM
            {
                SoruSayisi = soruSayi,
                PayID = payID,
                Ad = x.Ad,
                Soyad = x.Soyad,
                DersGrupID = x.DersGrupID,
                DersPlanID = x.DersPlanID,
                OgrenciID = x.OgrenciID,
                DersPlanAnaID = x.DersPlanAnaID,
                Numara = x.Numara,
                KullaniciAdi = x.KullaniciAdi,
                MutlakNot = notList.FirstOrDefault(y => y.FKOgrenciID == x.OgrenciID && y.FKAbsPaylarID == payID).NotDeger,
                OdevMi = odevMi,
                OgrenciNotListesi = notDetayList.Where(y => y.FKOgrenciID == x.OgrenciID)
                                                .Select(y => new SinavNotGirisSoruListeVM
                                                {
                                                    Kullanici = y.Kullanici,
                                                    Makina = y.Makina,
                                                    Notu = y.Notu,
                                                    SoruNo = y.SoruNo,
                                                    Zaman = y.Zaman,
                                                    FKOgrenciNotID = y.FKOgrenciNotID.Value,
                                                    FKABSPaylarID = y.FKABSPaylarID,
                                                    SoruSayisi = soruSayi,
                                                    //EnumSoruTip = y.ABSPaylarOlcme.EnumSoruTipi.Value,
                                                    //EnumSinavTip = y.ABSPaylarOlcme.EnumSinavTuru
                                                }).ToList(),
                //SinavKagidiGorebilirMi = kagitList.Count(y => y == x.KullaniciAdi) > 0,
                OdevAciklama = odevMi ? db.OOdevOgrenci.Where(y => y.FKOOdevID == odevID && y.FKOgrenciID == x.OgrenciID && !y.Silindi).OrderByDescending(y => y.ID).FirstOrDefault().Aciklama : "",
                OdevDosyaAdi = odevMi ? db.OOdevOgrenci.Where(y => y.FKOOdevID == odevID && y.FKOgrenciID == x.OgrenciID && !y.Silindi).OrderByDescending(y => y.ID).FirstOrDefault().DosyaAdi : "",
                OdevDosyaKey = odevMi ? db.OOdevOgrenci.Where(y => y.FKOOdevID == odevID && y.FKOgrenciID == x.OgrenciID && !y.Silindi).OrderByDescending(y => y.ID).FirstOrDefault().DosyaKey : "",
            }).ToList();

            return liste.OrderBy(d => d.Numara).ToList();
        }

        //public static List<SinavOgrenciListeVM> ListeleSinavOgrenciNotlari(int dersGrupID, int payID)
        //{
        //    UYSv2Entities db = new UYSv2Entities();
        //    KagitOkumaEntities db2 = new KagitOkumaEntities();

        //    var dersGrup = db.OGRDersGrup.FirstOrDefault(x => x.ID == dersGrupID);
        //    var payList = db.ABSPaylar.Where(x => x.FKDersPlanAnaID == dersGrup.FKDersPlanAnaID);
        //    var pay = payList.FirstOrDefault(x => x.ID == payID);


        //    var yazilmaList = db.OGROgrenciYazilma.Where(x => x.FKDersGrupID == dersGrupID && !x.Iptal).Select(x => new
        //    {
        //        Numara = x.OGRKimlik.Numara,
        //        Ad = x.OGRKimlik.Kisi.Ad,
        //        Soyad = x.OGRKimlik.Kisi.Soyad,
        //        OgrenciID = x.FKOgrenciID.Value,
        //        DersPlanAnaID = x.FKDersPlanAnaID.Value,
        //        DersGrupID = x.FKDersGrupID.Value,
        //        DersPlanID = x.OGRDersGrup.FKDersPlanID.Value,
        //        YazilmaID = x.ID,
        //        KullaniciAdi = x.OGRKimlik.Kullanici1.FirstOrDefault().KullaniciAdi,
        //        ButunlemeMi = x.ButunlemeMi
        //    });

        //    if (pay.EnumCalismaTip == (int)Sabis.Enum.EnumCalismaTip.Butunleme)
        //    {
        //        yazilmaList = yazilmaList.Where(x => x.ButunlemeMi == true);   
        //    }

        //    var ogrIDList = yazilmaList.Select(x => x.OgrenciID).ToList();
        //    var notList = db.ABSOgrenciNot.Where(x => x.FKDersGrupID == dersGrupID && !x.Silindi);
        //    var notDetayList = db.ABSOgrenciNotDetay.Where(x => x.FKABSPaylarID == payID && x.FKDersGrupID == dersGrupID && !x.Silindi);

        //    byte finalOran = payList.FirstOrDefault(l => l.EnumCalismaTip == 12).Oran.Value;

        //    bool odevMi = false;
        //    bool kagitlarGorunebilirMi = false;

        //    List<ABSPaylarOlcme> soruListe = db.ABSPaylarOlcme.Where(x => x.FKPaylarID == payID && x.Silindi == false).ToList();
        //    int soruSayi = soruListe.Count();
        //    var sinav = db2.Sinav.FirstOrDefault(x => x.FKABSPaylarID == payID && x.Silindi == false);
        //    List<string> kagitList = new List<string>();
        //    if (sinav != null)
        //    {
        //        kagitList = db2.SinavKagit.Where(x => x.FKSinavID == sinav.ID && x.OgrenciGorebilir == true).Select(x => x.OgrenciNo).ToList();
        //        kagitlarGorunebilirMi = kagitList.Any();
        //    }

        //    int? odevID = null;
        //    List<OOdevOgrenci> odevDokumanList = new List<OOdevOgrenci>();
        //    if (pay.EnumCalismaTip == 2 && db.OOdevGrup.Any(x => x.FKOGRDersGrupID == dersGrupID && x.OOdev.FKAbsPaylarID == payID && x.OOdev.Silindi == false))
        //    {
        //        OOdev odev = db.OOdevGrup.FirstOrDefault(x => x.FKOGRDersGrupID == dersGrupID && x.OOdev.FKAbsPaylarID == payID && x.OOdev.Silindi == false).OOdev;
        //        odevDokumanList = db.OOdevOgrenci.Where(x => x.FKOOdevID == odev.ID).ToList();
        //        odevMi = true;
        //        odevID = odev.ID;
        //    }

        //    var dictMutlakOrtalama = GetirMutlakOrtalamaListe(dersGrupID, payList, ogrIDList, notList);
        //    List<SinavOgrenciListeVM> liste = yazilmaList.Select(x => new SinavOgrenciListeVM
        //    {
        //        SoruSayisi = soruSayi,
        //        PayID = payID,
        //        Ad = x.Ad,
        //        Soyad = x.Soyad,
        //        DersGrupID = x.DersGrupID,
        //        DersPlanID = x.DersPlanID,
        //        OgrenciID = x.OgrenciID,
        //        DersPlanAnaID = x.DersPlanAnaID,
        //        Numara = x.Numara,
        //        KullaniciAdi = x.KullaniciAdi,
        //        MutlakNot = notList.FirstOrDefault(y => y.FKOgrenciID == x.OgrenciID && y.FKAbsPaylarID == payID).NotDeger,
        //        MutlakOrtalama = 0,//dictMutlakOrtalama.FirstOrDefault(b=> b.OgrenciID == x.OgrenciID).MutlakOrtalama,//dictMutlakOrtalama[x.OgrenciID]
        //        FinalOran = finalOran,
        //        OdevMi = odevMi,
        //        OgrenciNotListesi = notDetayList.Where(y => y.FKOgrenciID == x.OgrenciID)
        //                                                        .Select(y => new SinavNotGirisSoruListeVM
        //                                                        {
        //                                                            Kullanici = y.Kullanici,
        //                                                            Makina = y.Makina,
        //                                                            Notu = y.Notu,
        //                                                            SoruNo = y.SoruNo,
        //                                                            Zaman = y.Zaman,
        //                                                            FKOgrenciNotID = y.FKOgrenciNotID.Value,
        //                                                            FKABSPaylarID = y.FKABSPaylarID,
        //                                                            SoruSayisi = soruSayi,
        //                                                    //EnumSoruTip = y.ABSPaylarOlcme.EnumSoruTipi.Value,
        //                                                    //EnumSinavTip = y.ABSPaylarOlcme.EnumSinavTuru
        //                                                }).ToList(),
        //        SinavKagidiGorebilirMi = kagitlarGorunebilirMi, //kagitList.Any(y => y == x.KullaniciAdi) < kişi bazlı
        //        OdevAciklama = odevMi ? db.OOdevOgrenci.Where(y => y.FKOOdevID == odevID && y.FKOgrenciID == x.OgrenciID).OrderByDescending(y => y.ID).FirstOrDefault().Aciklama : "",
        //        OdevDosyaAdi = odevMi ? db.OOdevOgrenci.Where(y => y.FKOOdevID == odevID && y.FKOgrenciID == x.OgrenciID).OrderByDescending(y => y.ID).FirstOrDefault().DosyaAdi : "",
        //        OdevDosyaKey = odevMi ? db.OOdevOgrenci.Where(y => y.FKOOdevID == odevID && y.FKOgrenciID == x.OgrenciID).OrderByDescending(y => y.ID).FirstOrDefault().DosyaKey : "",
        //    }).ToList();

        //    foreach (var ogr in liste)
        //    {
        //        ogr.MutlakOrtalama = dictMutlakOrtalama[ogr.OgrenciID];
        //    }

        //    return liste.OrderBy(d => d.Numara).ToList();
        //}

        public static void OdevAciklamaKaydet(List<Sabis.Bolum.Core.ABS.DERS.ODEV.OdevAciklamaVM> aciklamaListe)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                if (aciklamaListe.Any(x => x.Aciklama != null))
                {
                    int payID = aciklamaListe.First().FKPayID;
                    int odevID = db.OOdev.FirstOrDefault(x => x.FKAbsPaylarID == payID && x.Silindi == false).ID;

                    List<int> ogrList = aciklamaListe.Select(x => x.FKOgrenciID).ToList();
                    List<OOdevOgrenci> odevOgrenciListe = db.OOdevOgrenci.Where(x => x.FKOOdevID == odevID && ogrList.Contains(x.FKOgrenciID) && x.Silindi == false).ToList();
                    foreach (var item in aciklamaListe.Where(x => x.Aciklama != null))
                    {
                        OOdevOgrenci ogrOdev = odevOgrenciListe.Where(x => x.FKOgrenciID == item.FKOgrenciID).OrderByDescending(x => x.YuklemeTarihi).FirstOrDefault();
                        ogrOdev.Aciklama = item.Aciklama;
                    }
                    db.SaveChanges();
                }
            }
        }

        public static void SinavNotKaydet(List<SinavNotGirisSoruListeVM> ogrenciNotList, List<Sabis.Bolum.Core.ABS.DERS.ODEV.OdevAciklamaVM> aciklamaListe, int FKDersPlanAnaID, int FKDersPlanID, int FKDersGrupID, int Yil, int EnumDonem, string IP = "", string kullanici = "", int? birimID = null, int enumSinavTuru = 0, bool yeniNotGiris = false)
        {
            if (aciklamaListe != null)
            {
                OdevAciklamaKaydet(aciklamaListe);
            }
            Guid yeniGuid = Guid.NewGuid();

            List<int> payIDList = ogrenciNotList.Select(x => x.FKABSPaylarID.Value).Distinct().ToList();
            int payID = (int)ogrenciNotList.Take(1).FirstOrDefault().FKABSPaylarID;
            ogrenciNotList = ogrenciNotList.Where(x => x.FKOgrenciID != 0).OrderBy(x => x.FKOgrenciID).ThenBy(x => x.SoruNo).ToList();
            List<int> ogrIDList = ogrenciNotList.Select(x => x.FKOgrenciID).ToList();

            using (UYSv2Entities db = new UYSv2Entities())
            {
                OGRDersGrup dersGrup = db.OGRDersGrup.FirstOrDefault(x => x.ID == FKDersGrupID);

                bool sonHalVerildiMi = db.ABSDegerlendirme.Any(x => x.FKDersGrupID == FKDersGrupID && x.SilindiMi == false && x.EnumSonHal == (int)EnumSonHal.EVET);
                bool detayNotlarSilindiMi = NOT.NOTMAIN.NotDetaylariniSilindiYap(ogrIDList, payIDList, IP, kullanici);
                bool notlarSilindiMi = NOT.NOTMAIN.NotlariSilindiYap(ogrIDList, payIDList, IP, kullanici);

                if (!sonHalVerildiMi || notlarSilindiMi || detayNotlarSilindiMi) // || !ortakDegerlendirmeGrupVarMi
                {
                    ogrenciNotList = ogrenciNotList.Where(x => x.Notu != null).ToList();
                    var ogrGrupNotList = ogrenciNotList.Where(x => x.Notu != null).GroupBy(x => x.FKOgrenciID).ToList();
                    var sinavSoruList = db.ABSPaylarOlcme.Where(x => x.FKPaylarID == payID && x.Silindi == false);

                    foreach (var item in ogrGrupNotList)
                    {
                        var notList = ogrenciNotList.Where(x => x.FKOgrenciID == item.Key).ToList();

                        ABSOgrenciNot ogrYeniNot = new ABSOgrenciNot();
                        ogrYeniNot.Zaman = DateTime.Now;
                        ogrYeniNot.Kullanici = kullanici;
                        ogrYeniNot.Makina = IP;
                        ogrYeniNot.AktarilanVeriMi = false;
                        ogrYeniNot.Yil = dersGrup.OgretimYili.Value;
                        ogrYeniNot.EnumDonem = dersGrup.EnumDonem.Value;
                        ogrYeniNot.FKOgrenciID = item.Key;
                        ogrYeniNot.FKDersPlanID = FKDersPlanID;
                        ogrYeniNot.FKDersPlanAnaID = FKDersPlanAnaID;
                        ogrYeniNot.FKDersGrupID = FKDersGrupID;
                        ogrYeniNot.FKAbsPaylarID = payID;
                        ogrYeniNot.GUID = yeniGuid;

                        ogrYeniNot.NotDeger = notList.Sum(x => x.Notu.Value);

                        if (notList.Any(x => x.Notu == 120))
                        {
                            ogrYeniNot.NotDeger = 120;
                        }
                        if (notList.Any(x => x.Notu == 130))
                        {
                            ogrYeniNot.NotDeger = 130;
                        }

                        ogrYeniNot.UZEMID = null;
                        ogrYeniNot.Silindi = false;
                        db.ABSOgrenciNot.Add(ogrYeniNot);
                        db.SaveChanges();

                        List<ABSOgrenciNotDetay> notDetayList = new List<ABSOgrenciNotDetay>();

                        foreach (var subItem in notList)
                        {
                            ABSOgrenciNotDetay ogrYeniNotDetay = new ABSOgrenciNotDetay();
                            ogrYeniNotDetay.Zaman = DateTime.Now;
                            ogrYeniNotDetay.Kullanici = kullanici;
                            ogrYeniNotDetay.Makina = IP;
                            ogrYeniNotDetay.AktarilanVeriMi = false;
                            ogrYeniNotDetay.SoruNo = subItem.SoruNo;
                            ogrYeniNotDetay.Notu = subItem.Notu;
                            ogrYeniNotDetay.FKOgrenciNotID = ogrYeniNot.ID;
                            ogrYeniNotDetay.FKABSPaylarID = payID;
                            ogrYeniNotDetay.FKOgrenciID = subItem.FKOgrenciID;
                            ogrYeniNotDetay.FKDersGrupID = FKDersGrupID;
                            ogrYeniNotDetay.FKAbsPaylarOlcmeID = sinavSoruList.FirstOrDefault(y => y.SoruNo == subItem.SoruNo).ID;
                            ogrYeniNotDetay.Yil = dersGrup.OgretimYili;
                            ogrYeniNotDetay.EnumDonem = dersGrup.EnumDonem;
                            ogrYeniNotDetay.Silindi = false;
                            ogrYeniNotDetay.GUID = yeniGuid;
                            notDetayList.Add(ogrYeniNotDetay);
                        }

                        EntityFramework.Utilities.EFBatchOperation.For(db, db.ABSOgrenciNotDetay).InsertAll(notDetayList);
                    }

                    #region degerlendirmeleri silindi yap
                    var degerlendirilmisler = db.ABSDegerlendirme.Where(d => d.FKDersGrupID.Value == FKDersGrupID && d.SilindiMi == false && d.EnumSonHal != (int)EnumSonHal.EVET).Select(x => x.OrtakDegerlendirmeGUID).Distinct().ToList();
                    foreach (var item in degerlendirilmisler)
                    {
                        var ortakGruplar = db.ABSDegerlendirme.Where(x => x.OrtakDegerlendirmeGUID == item).ToList();
                        foreach (var grupDegerlendirme in ortakGruplar)
                        {
                            grupDegerlendirme.SilindiMi = true;
                        }
                    }
                    db.SaveChanges();
                    #endregion
                }
            }
        }
        #endregion

        public static SinavDetaylariVM GetirSinavDetayi(int FKPaylarID)
        {
            KagitOkumaEntities db2 = new KagitOkumaEntities();
            using (UYSv2Entities db = new UYSv2Entities())
            {
                SinavDetaylariVM detay = new SinavDetaylariVM();
                if (db2.Sinav.Any(x => x.FKABSPaylarID == FKPaylarID && x.Silindi == false))
                {
                    var sinav = db2.Sinav.FirstOrDefault(x => x.FKABSPaylarID == FKPaylarID && x.Silindi == false);

                    detay.FKSinavID = sinav.ID;

                    detay.EnumSinavTuru = sinav.EnumSinavTuru;
                    var test = db.ABSPaylarOlcme.FirstOrDefault(x => x.FKPaylarID == FKPaylarID && x.Silindi == false);
                    if (db.ABSPaylarOlcme.Any(x => x.FKPaylarID == FKPaylarID && x.Silindi == false))
                    {
                        if (db.ABSPaylarOlcme.FirstOrDefault(x => x.FKPaylarID == FKPaylarID && x.Silindi == false).EnumSinavTuru == (int)EnumSinavTuru.KARMA)
                        {
                            detay.SoruSayisi = db.ABSPaylarOlcme.Count(x => x.FKPaylarID == FKPaylarID && x.EnumSoruTipi == (int)EnumSoruTipi.TEST && x.Silindi == false);
                        }
                        else
                        {
                            detay.SoruSayisi = db.ABSPaylarOlcme.Count(x => x.FKPaylarID == FKPaylarID && x.Silindi == false);
                        }
                    }

                    detay.SinavOlusturulmusMu = true;
                }
                else
                {
                    detay.SinavOlusturulmusMu = false;
                }

                detay.EnumCalismaTip = db.ABSPaylar.FirstOrDefault(x => !x.Silindi && x.ID == FKPaylarID).EnumCalismaTip.Value;
                if (detay.FKSinavID.HasValue)
                {
                    detay.NotlarAktarilabilirMi = !db2.SinavKagit.Any(x => x.FKSinavID == detay.FKSinavID && x.EnumKagitOkumaDurumu == (int)Core.ENUM.ABS.EnumKagitOkumaDurumu.BEKLEMEDE && x.Silindi == false);
                    detay.OkunmayanKagitSayisi = db2.SinavKagit.Count(x => x.FKSinavID == detay.FKSinavID && x.EnumKagitOkumaDurumu == (int)Core.ENUM.ABS.EnumKagitOkumaDurumu.BEKLEMEDE && x.Silindi == false);
                }
                return detay;
            }
        }
        public static bool GuncelleSinavDurum(int FKSinavID, int Durum)
        {
            using (KagitOkumaEntities db = new KagitOkumaEntities())
            {
                try
                {
                    var sinav = db.Sinav.FirstOrDefault(x => x.ID == FKSinavID);
                    sinav.Okunabilme = Convert.ToString(Durum);
                    db.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public static List<SinavDokumanVM> GetirSinavDokumanListesi(int fkPaylarID)
        {
            using (KagitOkumaEntities db = new KagitOkumaEntities())
            {
                Sinav sinav = db.Sinav.FirstOrDefault(x => x.FKABSPaylarID == fkPaylarID && x.Silindi == false);

                //var config = new MapperConfiguration(cfg => cfg.CreateMap<List<SinavDokuman>, List<SinavDokumanVM>>());
                //var mapper = new Mapper(config);

                //List<SinavDokumanVM> dokumanList = mapper.Map<SinavDokumanVM>(db.SinavDokuman.Where(x => x.FKSinavID == sinav.ID && x.Silindi == false))

                if (sinav != null)
                {
                    return db.SinavDokuman.Where(x => x.FKSinavID == sinav.ID && x.Silindi == false).Select(x => new SinavDokumanVM
                    {
                        ID = x.ID,
                        FKSinavID = x.FKSinavID,
                        DosyaAdi = x.DosyaAdi,
                        DosyaKey = x.DosyaKey,
                        EnumDokumanTuru = x.EnumDokumanTuru,
                        SGTarihi = x.SGTarihi,
                        SGKullanici = x.SGKullanici,
                        SGIP = x.SGIP
                    }).ToList();
                }
                else
                {
                    return new List<SinavDokumanVM>();
                }
            }
        }

        public static void KaydetSinavDokuman(List<SinavDokumanVM> sinavKagitListesi, int enumDokumanTuru, int FKPaylarID, int yil, int FKDersGrupID, string kullanici, string IP)
        {
            using (KagitOkumaEntities db = new KagitOkumaEntities())
            {
                Sinav sinav = db.Sinav.FirstOrDefault(x => x.FKABSPaylarID == FKPaylarID && x.Silindi == false);
                foreach (var dokuman in sinavKagitListesi)
                {
                    SinavDokuman yeniDokuman = new SinavDokuman();
                    yeniDokuman.FKSinavID = sinav.ID;
                    yeniDokuman.DosyaAdi = dokuman.DosyaAdi;
                    yeniDokuman.DosyaKey = dokuman.DosyaKey;
                    yeniDokuman.EnumDokumanTuru = enumDokumanTuru;
                    yeniDokuman.SGTarihi = DateTime.Now;
                    yeniDokuman.SGIP = IP;
                    yeniDokuman.SGKullanici = kullanici;
                    yeniDokuman.Silindi = false;
                    db.SinavDokuman.Add(yeniDokuman);
                }
                db.SaveChanges();
            }
        }

        public static void SilSinavDokuman(int fkSinavDokumanID, string kullanici, string IP)
        {
            using (KagitOkumaEntities db = new KagitOkumaEntities())
            {
                SinavDokuman dokuman = db.SinavDokuman.FirstOrDefault(x => x.ID == fkSinavDokumanID);
                dokuman.Silindi = true;
                dokuman.SGIP = IP;
                dokuman.SGKullanici = kullanici;
                dokuman.SilindiTarih = DateTime.Now;
                db.SaveChanges();
            }
        }

        public static SinavKagidiVM KaydetSinavKagidi(List<SinavKagidiVM> sinavKagitListesi, int FKPaylarID, int yil, int FKDersGrupID)
        {
            try
            {
                OGRDersPlanAna dersPlanAna = new OGRDersPlanAna();
                OGRDersPlan dersPlan = new OGRDersPlan();
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    dersPlanAna = db.OGRDersPlanAna.FirstOrDefault(x => x.ID == db.ABSPaylar.FirstOrDefault(y => !y.Silindi && y.ID == FKPaylarID).FKDersPlanAnaID);
                    dersPlan = db.OGRDersPlan.FirstOrDefault(x => x.FKDersPlanAnaID == dersPlanAna.ID && x.Yil == yil);
                }
                using (KagitOkumaEntities db = new KagitOkumaEntities())
                {

                    var sinav = db.Sinav.FirstOrDefault(x => x.FKABSPaylarID == FKPaylarID && x.Silindi == false);
                    foreach (var item in sinavKagitListesi)
                    {
                        SinavKagit yeniKagit = new SinavKagit();
                        yeniKagit.Kullanici = item.Kullanici;
                        yeniKagit.IP = item.IP;
                        yeniKagit.TarihKayit = item.TarihKayit;
                        yeniKagit.FKSinavID = sinav.ID;
                        yeniKagit.FKDersGrupID = FKDersGrupID;
                        yeniKagit.DosyaKey = item.DosyaKey;
                        yeniKagit.EnumKagitOkumaDurumu = item.EnumKagitOkumaDurumu.Value;
                        yeniKagit.Silindi = false;
                        yeniKagit.DosyaAdi = item.DosyaAdi;

                        db.SinavKagit.Add(yeniKagit);
                    }
                    db.SaveChanges();
                    return sinavKagitListesi.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                return new SinavKagidiVM();
            }
        }
        public static bool SilSinavKagidi(List<SinavKagidiVM> sinavKagitListesi, string kullanici, string IP)
        {
            using (KagitOkumaEntities db = new KagitOkumaEntities())
            {
                try
                {
                    foreach (var item in sinavKagitListesi)
                    {
                        var kagit = db.SinavKagit.FirstOrDefault(x => x.ID == item.ID);
                        kagit.Silindi = true;
                        kagit.SilindiTarih = DateTime.Now;
                        kagit.SilenKullanici = kullanici;
                        kagit.SilenIP = IP;

                    }
                    db.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }


        public static List<SinavKagidiVM> GetirSinavKagitListe(int FKPaylarID, int FKDersGrupID, int FKDersPlanAnaID, int FKDersPlanID, int yil, int enumDonem, string kullaniciAdi, int fkPersonelID)
        {
            UYSv2Entities db1 = new UYSv2Entities();
            KagitOkumaEntities db2 = new KagitOkumaEntities();

            var sinav = db2.Sinav.FirstOrDefault(x => x.FKABSPaylarID == FKPaylarID && x.Silindi == false);

            if (sinav == null)
            {
                return new List<SinavKagidiVM>();
            }

            var dersGrupIDList = db1.OGRDersGrupHoca.Where(x => x.FKPersonelID == fkPersonelID && x.OGRDersGrup.FKDersPlanAnaID == FKDersPlanAnaID && x.OGRDersGrup.OgretimYili == yil && x.OGRDersGrup.Acik == true && x.OGRDersGrup.Silindi == false).Select(x => x.FKDersGrupID).ToList();
            var tumDersGrupIDList = db1.OGRDersGrup.Where(x => x.FKDersPlanAnaID == FKDersPlanAnaID && x.OgretimYili == yil && x.EnumDonem == enumDonem && x.Silindi == false && x.Acik == true).Select(x => x.ID).ToList();

            var ogrList = db1.OGROgrenciYazilma.Where(x => tumDersGrupIDList.Contains(x.FKDersGrupID.Value) && x.Iptal == false);

            var ogrNoList = ogrList.Where(x => x.FKDersGrupID == FKDersGrupID).Select(x => x.OGRKimlik.Numara).Distinct().ToList();
            var ogrTumNoList = ogrList.Select(x => x.OGRKimlik.Numara).ToList();

            List<int> ogrIDs = ogrList.Select(x => x.FKOgrenciID.Value).Distinct().ToList();
            //Dictionary<string, string> dictOgrList = db1.Kullanici.Where(x => ogrIDs.Contains(x.FKOgrenciID.Value)).Select(x => new KeyValuePair<string, string>(x.OGRKimlik.Numara, x.OGRKimlik.Kisi.Ad + " " + x.OGRKimlik.Kisi.Soyad)).ToDictionary(x => x.Key, x => x.Value);

            Dictionary<string, string> dictOgrList = db1.Kullanici.Where(x => ogrIDs.Contains(x.FKOgrenciID.Value)).Select(x => new { x.KullaniciAdi, AdSoyad = x.OGRKimlik.Kisi.Ad + " " + x.OGRKimlik.Kisi.Soyad }).ToDictionary(x => x.KullaniciAdi, x => x.AdSoyad);


            ABSTestCevapAnahtari cevapAnahtari = new ABSTestCevapAnahtari();
            if (sinav.EnumSinavTuru == (int)EnumSinavTuru.TEST || sinav.EnumSinavTuru == (int)EnumSinavTuru.KARMA)
            {
                cevapAnahtari = db1.ABSTestCevapAnahtari.FirstOrDefault(x => x.FKSinavID == sinav.ID);
            }

            var ogrIDList = ogrList.Where(x => dersGrupIDList.Contains(x.FKDersGrupID) && x.Iptal == false).Select(x => x.FKOgrenciID).ToList();
            var ogrTumIDList = ogrList.Select(x => x.FKOgrenciID).ToList();

            var ogrKullaniciList = db1.Kullanici.Where(k => ogrIDList.Contains(k.FKOgrenciID)).Select(k => k.KullaniciAdi.ToLower()).ToList();
            var ogrTumKullaniciList = db1.Kullanici.Where(x => ogrTumIDList.Contains(x.FKOgrenciID)).Select(x => x.KullaniciAdi.ToLower()).ToList();


            var ogrGrupIDList = db1.OGROgrenciYazilma.Where(x => x.FKDersGrupID == FKDersGrupID && x.Iptal == false).Select(x => x.FKOgrenciID).ToList();
            var ogrGrupKullaniciList = db1.Kullanici.Where(k => ogrGrupIDList.Contains(k.FKOgrenciID)).Select(k => k.KullaniciAdi.ToLower()).ToList();

            var kagitListe = db2.SinavKagit.Where(x => x.FKSinavID == sinav.ID && x.Silindi == false);

            if (kagitListe.Count() == 0)
            {
                return new List<SinavKagidiVM>();
            }

            var sinavKagitList = kagitListe.Where(x => (x.EnumKagitOkumaDurumu == (int)EnumKagitOkumaDurumu.OKUNDU) && ogrGrupKullaniciList.Contains(x.OgrenciNo) || x.EnumKagitOkumaDurumu != (int)EnumKagitOkumaDurumu.OKUNDU && dersGrupIDList.Contains(x.FKDersGrupID) || !ogrTumKullaniciList.Contains(x.OgrenciNo)).ToList();

            var digerGrupKullaniciList = ogrKullaniciList.Where(x => !ogrGrupKullaniciList.Contains(x));

            var hataliOgrenciList2 = kagitListe.Where(x => !ogrTumKullaniciList.Contains(x.OgrenciNo)).Select(x => x.OgrenciNo).ToList();

            var hataliOgrenciList3 = kagitListe.Where(x => x.KitapcikTuru == "*").Select(x => x.OgrenciNo).ToList();

            var hataliOgrenciList4 = new List<string>();

            if (cevapAnahtari != null)
            {
                if (String.IsNullOrEmpty(cevapAnahtari.CevapA))
                {
                    var tempList = kagitListe.Where(x => x.KitapcikTuru == "A").Select(x => x.OgrenciNo);
                    hataliOgrenciList4.AddRange(tempList);
                }
                if (String.IsNullOrEmpty(cevapAnahtari.CevapB))
                {
                    var tempList = kagitListe.Where(x => x.KitapcikTuru == "B").Select(x => x.OgrenciNo);
                    hataliOgrenciList4.AddRange(tempList);
                }
                if (String.IsNullOrEmpty(cevapAnahtari.CevapC))
                {
                    var tempList = kagitListe.Where(x => x.KitapcikTuru == "C").Select(x => x.OgrenciNo);
                    hataliOgrenciList4.AddRange(tempList);
                }
                if (String.IsNullOrEmpty(cevapAnahtari.CevapD))
                {
                    var tempList = kagitListe.Where(x => x.KitapcikTuru == "D").Select(x => x.OgrenciNo);
                    hataliOgrenciList4.AddRange(tempList);
                }
            }

            var sinavTestPuanList = GetirSinavPuanListesi(FKPaylarID, (int)EnumSoruTipi.TEST);
            var testSoruSayisi = sinavTestPuanList.Count();

            var hataliOgrenciNoList = hataliOgrenciList2.Concat(hataliOgrenciList3).Concat(hataliOgrenciList4);

            using (KagitOkumaEntities db = new KagitOkumaEntities())
            {

                if (sinav != null)
                {

                    List<SinavKagidiVM> kagitList = sinavKagitList.Select(x => new SinavKagidiVM //kullaniciList.Contains(x.OgrenciNo)
                    {
                        ID = x.ID,
                        Kullanici = x.Kullanici,
                        IP = x.IP,
                        TarihKayit = x.TarihKayit,
                        DosyaAdi = x.DosyaAdi,
                        FKSinavID = x.FKSinavID,
                        DosyaKey = x.DosyaKey,
                        EnumKagitOkumaDurumu = x.EnumKagitOkumaDurumu,
                        HataDetay = x.HataDetay,
                        OgrenciNo = x.OgrenciNo,
                        OgrenciAdSoyad = x.OgrenciNo != null && x.OgrenciNo.Length > 1 && dictOgrList.ContainsKey(x.OgrenciNo) ? dictOgrList[x.OgrenciNo] : "",
                        KagitGrubu = x.KitapcikTuru,
                        Sonuclar = x.Sonuclar
                    }).OrderBy(x => x.DosyaAdi).ToList();

                    foreach (var item in kagitList.Where(x => hataliOgrenciNoList.Contains(x.OgrenciNo) && x.EnumKagitOkumaDurumu != (int)EnumKagitOkumaDurumu.BEKLEMEDE))
                    {
                        item.EnumKagitOkumaDurumu = (int)EnumKagitOkumaDurumu.HATA;
                        item.HataDetay = "Hatalı bilgi";
                    }

                    if (kagitList.Any(x => x.EnumKagitOkumaDurumu == 1))
                    {
                        GuncelleSinavDurum(sinav.ID, 1);
                    }

                    return kagitList;
                }
                else
                {
                    return new List<SinavKagidiVM>();
                }
            }
        }

        public static bool GetirSinavKagitDurum(int fkPaylarID, int fkDersGrupID)
        {
            UYSv2Entities db1 = new UYSv2Entities();
            KagitOkumaEntities db2 = new KagitOkumaEntities();

            var ogrList = db1.OGROgrenciYazilma.Where(x => x.FKDersGrupID == fkDersGrupID && x.Iptal == false).Select(x => x.FKOgrenciID);
            var sinav = db2.Sinav.FirstOrDefault(x => x.FKABSPaylarID == fkPaylarID && x.Silindi == false);
            var ogrKullaniciList = db1.Kullanici.Where(x => ogrList.Contains(x.FKOgrenciID)).Select(x => x.KullaniciAdi).ToList();

            //db1.Dispose();
            //db2.Dispose();

            return sinav != null ? db2.SinavKagit.Any(x => x.FKSinavID == sinav.ID && ogrKullaniciList.Contains(x.OgrenciNo) && x.OgrenciGorebilir) : false;

        }
        public static bool GuncelleSinavKagitDurum(int FKPaylarID, int FKDersGrupHocaID, bool durum, int? FKOgrenciID)
        {
            try
            {
                UYSv2Entities db1 = new UYSv2Entities();
                KagitOkumaEntities db2 = new KagitOkumaEntities();

                var fkDersGrupID = db1.OGRDersGrupHoca.FirstOrDefault(x => x.ID == FKDersGrupHocaID).FKDersGrupID;
                var ogrList = db1.OGROgrenciYazilma.Where(x => x.FKDersGrupID == fkDersGrupID && x.Iptal == false).Select(x => x.FKOgrenciID);

                if (FKOgrenciID.HasValue)
                {
                    ogrList = ogrList.Where(x => x == FKOgrenciID);
                }

                var ogrKullaniciList = db1.Kullanici.Where(x => ogrList.Contains(x.FKOgrenciID)).Select(x => x.KullaniciAdi).ToList();

                var sinav = db2.Sinav.FirstOrDefault(x => x.FKABSPaylarID == FKPaylarID && x.Silindi == false);
                var kagitListe = db2.SinavKagit.Where(x => x.FKSinavID == sinav.ID && ogrKullaniciList.Contains(x.OgrenciNo));

                foreach (var item in kagitListe)
                {
                    item.OgrenciGorebilir = durum;
                }
                db2.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static int GetirSinavID(int FKPaylarID, int FKDersGrupID, int FKDersPlanID, int FKDersPlanAnaID)
        {
            using (KagitOkumaEntities db = new KagitOkumaEntities())
            {
                if (db.Sinav.Any(x => x.FKABSPaylarID == FKPaylarID && x.Silindi == false))
                {
                    return db.Sinav.FirstOrDefault(x => x.FKABSPaylarID == FKPaylarID && x.Silindi == false).ID;
                }
                else
                {
                    return 0;
                }
            }
        }

        public static List<CevapAnahtariVM> GetirCevapAnahtariListe(int FKSinavID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return db.ABSTestCevapAnahtari.Where(x => x.FKSinavID == FKSinavID).Select(x => new CevapAnahtariVM
                {
                    ID = x.ID,
                    FKSinavID = x.FKSinavID,
                    CevapA = x.CevapA,
                    SiraB = x.SiraB,
                    CevapB = x.CevapB,
                    SiraC = x.SiraC,
                    CevapC = x.CevapC,
                    SiraD = x.SiraD,
                    CevapD = x.CevapD
                }).ToList();
            }
        }

        public static CevapAnahtari2VM GetirSiraliCevapAnahtari(int FKSinavID, int FKABSPaylarID)
        {
            try
            {
                CevapAnahtari2VM yeniCevapAnahtari = new CevapAnahtari2VM();
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var cevapAnahtari = db.ABSTestCevapAnahtari.FirstOrDefault(x => x.FKSinavID == FKSinavID);
                    var soruList = db.ABSPaylarOlcme.Where(x => x.FKPaylarID == FKABSPaylarID && x.Silindi == false).ToList();

                    if (!String.IsNullOrEmpty(cevapAnahtari.CevapA))
                    {
                        yeniCevapAnahtari.CevapA = cevapAnahtari.CevapA.Split(',').ToList();
                        yeniCevapAnahtari.PuanA = soruList.Select(x => x.SoruPuan.Value).ToList();
                    }

                    if (!String.IsNullOrEmpty(cevapAnahtari.SiraB))
                    {
                        yeniCevapAnahtari.SiraB = cevapAnahtari.SiraB.Split(',').ToList();
                        yeniCevapAnahtari.CevapB = cevapAnahtari.CevapB.Split(',').ToList();
                        List<double> tempList = new List<double>();
                        foreach (var item in yeniCevapAnahtari.SiraB)
                        {
                            tempList.Add(soruList.FirstOrDefault(x => x.SoruNo == Convert.ToByte(item)).SoruPuan.Value);
                        }
                        foreach (var item in soruList.Where(y => y.EnumSoruTipi == (int)EnumSoruTipi.KLASIK))
                        {
                            tempList.Add(item.SoruPuan.Value);
                        }
                        yeniCevapAnahtari.PuanB = tempList;
                    }


                    if (!String.IsNullOrEmpty(cevapAnahtari.SiraC))
                    {
                        yeniCevapAnahtari.SiraC = cevapAnahtari.SiraC.Split(',').ToList();
                        yeniCevapAnahtari.CevapC = cevapAnahtari.CevapC.Split(',').ToList();
                        List<double> tempList = new List<double>();
                        foreach (var item in yeniCevapAnahtari.SiraC)
                        {
                            tempList.Add(soruList.FirstOrDefault(x => x.SoruNo == Convert.ToByte(item)).SoruPuan.Value);
                        }
                        foreach (var item in soruList.Where(y => y.EnumSoruTipi == (int)EnumSoruTipi.KLASIK))
                        {
                            tempList.Add(item.SoruPuan.Value);
                        }
                        yeniCevapAnahtari.PuanC = tempList;
                    }


                    if (!String.IsNullOrEmpty(cevapAnahtari.SiraD))
                    {
                        yeniCevapAnahtari.SiraD = cevapAnahtari.SiraD.Split(',').ToList();
                        yeniCevapAnahtari.CevapD = cevapAnahtari.CevapD.Split(',').ToList();
                        List<double> tempList = new List<double>();
                        foreach (var item in yeniCevapAnahtari.SiraD)
                        {
                            tempList.Add(soruList.FirstOrDefault(x => x.SoruNo == Convert.ToByte(item)).SoruPuan.Value);
                        }
                        foreach (var item in soruList.Where(y => y.EnumSoruTipi == (int)EnumSoruTipi.KLASIK))
                        {
                            tempList.Add(item.SoruPuan.Value);
                        }
                        yeniCevapAnahtari.PuanD = tempList;
                    }
                }
                return yeniCevapAnahtari;
            }
            catch (Exception error)
            {

                throw;
            }
        }

        public static bool EkleCevapAnahtari(int FKSinavID)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    ABSTestCevapAnahtari yeniCevapAnahtari = new ABSTestCevapAnahtari();
                    yeniCevapAnahtari.FKSinavID = FKSinavID;
                    db.ABSTestCevapAnahtari.Add(yeniCevapAnahtari);
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool SilCevapAnahtari(int FKSinavID)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    db.ABSTestCevapAnahtari.Remove(db.ABSTestCevapAnahtari.FirstOrDefault(x => x.FKSinavID == FKSinavID));
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool KaydetCevapAnahtari(CevapAnahtariVM cevapAnahtari)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                try
                {
                    ABSTestCevapAnahtari cvp = db.ABSTestCevapAnahtari.FirstOrDefault(x => x.FKSinavID == cevapAnahtari.FKSinavID);
                    cvp.CevapA = cevapAnahtari.CevapA;
                    cvp.SiraB = cevapAnahtari.SiraB;
                    cvp.CevapB = cevapAnahtari.CevapB;
                    cvp.SiraC = cevapAnahtari.SiraC;
                    cvp.CevapC = cevapAnahtari.CevapC;
                    cvp.SiraD = cevapAnahtari.SiraD;
                    cvp.CevapD = cevapAnahtari.CevapD;

                    db.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public static void SinavDegerlendir(int FKSinavID, int FKPaylarID, int FKDersGrupHocaID, int yil, int enumDonem, string kullanici, string IP, int? FKSinavKagitID, bool grupNotlariSilinsinMi = true)
        {
            UYSv2Entities db1 = new UYSv2Entities();
            using (KagitOkumaEntities db = new KagitOkumaEntities())
            {
                if (db.Sinav.FirstOrDefault(x => x.ID == FKSinavID).EnumSinavTuru == (int)EnumSinavTuru.KLASIK)
                {
                    KlasikSinavDegerlendir(FKSinavID, FKPaylarID, FKDersGrupHocaID, yil, enumDonem, kullanici, IP, null, FKSinavKagitID, false, 0);
                    //try
                    //{
                    //    KlasikSinavDegerlendir(FKSinavID, FKPaylarID, FKDersGrupHocaID, yil, enumDonem, kullanici, IP, null, FKSinavKagitID, false, 0);
                    //    return true;
                    //}
                    //catch (Exception)
                    //{
                    //    return false;
                    //}
                }
                else if (db.Sinav.FirstOrDefault(x => x.ID == FKSinavID).EnumSinavTuru == (int)EnumSinavTuru.KARMA)
                {
                    List<SinavNotGirisSoruListeVM> testSonuc = TestSinavDegerlendir(FKSinavID, FKPaylarID, FKDersGrupHocaID, yil, enumDonem, kullanici, IP, FKSinavKagitID, true);
                    List<SinavNotGirisSoruListeVM> karmaSonuc = KlasikSinavDegerlendir(FKSinavID, FKPaylarID, FKDersGrupHocaID, yil, enumDonem, kullanici, IP, testSonuc, FKSinavKagitID, true, testSonuc.Count());
                    List<SinavNotGirisSoruListeVM> birlesikSonuc = testSonuc.Concat(karmaSonuc).ToList();

                    var dersGrupHoca = db1.OGRDersGrupHoca.FirstOrDefault(x => x.ID == FKDersGrupHocaID);
                    var dersGrup = db1.OGRDersGrup.FirstOrDefault(x => x.ID == dersGrupHoca.FKDersGrupID);

                    SinavNotKaydet(birlesikSonuc, new List<Sabis.Bolum.Core.ABS.DERS.ODEV.OdevAciklamaVM>(), dersGrup.FKDersPlanAnaID.Value, dersGrup.FKDersPlanID.Value, dersGrup.ID, yil, enumDonem, IP, kullanici, null, (int)EnumSinavTuru.KARMA);
                    //try
                    //{
                    //    List<SinavNotGirisSoruListeVM> testSonuc = TestSinavDegerlendir(FKSinavID, FKPaylarID, FKDersGrupHocaID, yil, enumDonem, kullanici, IP, FKSinavKagitID, true);
                    //    List<SinavNotGirisSoruListeVM> karmaSonuc = KlasikSinavDegerlendir(FKSinavID, FKPaylarID, FKDersGrupHocaID, yil, enumDonem, kullanici, IP, testSonuc, FKSinavKagitID, true, testSonuc.Count());
                    //    List<SinavNotGirisSoruListeVM> birlesikSonuc = testSonuc.Concat(karmaSonuc).ToList();

                    //    var dersGrupHoca = db1.OGRDersGrupHoca.FirstOrDefault(x => x.ID == FKDersGrupHocaID);
                    //    var dersGrup = db1.OGRDersGrup.FirstOrDefault(x => x.ID == dersGrupHoca.FKDersGrupID);

                    //    SinavNotKaydet(birlesikSonuc, new List<Sabis.Bolum.Core.ABS.DERS.ODEV.OdevAciklamaVM>(), dersGrup.FKDersPlanAnaID.Value, dersGrup.FKDersPlanID.Value, dersGrup.ID, yil, enumDonem, IP, kullanici, null, (int)EnumSinavTuru.KARMA);
                    //    return true;
                    //}
                    //catch (Exception)
                    //{
                    //    return false;
                    //}
                }
                else
                {
                    TestSinavDegerlendir(FKSinavID, FKPaylarID, FKDersGrupHocaID, yil, enumDonem, kullanici, IP, FKSinavKagitID);
                    //try
                    //{
                    //    TestSinavDegerlendir(FKSinavID, FKPaylarID, FKDersGrupHocaID, yil, enumDonem, kullanici, IP, FKSinavKagitID);
                    //    return true;
                    //}
                    //catch (Exception)
                    //{
                    //    return false;
                    //}
                }
            }
        }

        public static List<SinavNotGirisSoruListeVM> KlasikSinavDegerlendir(int FKSinavID, int FKPaylarID, int FKDersGrupHocaID, int yil, int enumDonem, string kullanici, string IP, List<SinavNotGirisSoruListeVM> karmaTestSonuc, int? FKSinavKagitID, bool karma = false, int soruSira = 0)
        {

            UYSv2Entities db1 = new UYSv2Entities();
            KagitOkumaEntities db2 = new KagitOkumaEntities();
            var dersGrupHoca = db1.OGRDersGrupHoca.FirstOrDefault(x => x.ID == FKDersGrupHocaID);
            var dersGrup = db1.OGRDersGrup.FirstOrDefault(x => x.ID == dersGrupHoca.FKDersGrupID);
            var yazilmaList = db1.OGROgrenciYazilma.Where(x => x.FKDersGrupID == dersGrup.ID && x.Yil == yil && x.EnumDonem == enumDonem && x.Iptal == false);
            var sinavDetay = GetirSinavDetayi(FKPaylarID);
            var yazilanOgrNoList = yazilmaList.Select(x => x.OGRKimlik.Numara).ToList();
            var yazilanKullaniciNoList = yazilanOgrNoList.Select(x => OGRENCI.OGRENCIMAIN.GetirKullaniciAdi(x));
            var tumKagitListe = db2.SinavKagit.Where(x => x.FKSinavID == FKSinavID && yazilanKullaniciNoList.Contains(x.OgrenciNo) && x.Silindi == false && x.EnumKagitOkumaDurumu == (int)EnumKagitOkumaDurumu.OKUNDU && x.Sonuclar.Length > 0).ToList();
            var sonucListe = tumKagitListe.Where(x => x.FKSinavID == FKSinavID && yazilanKullaniciNoList.Contains(x.OgrenciNo) && x.Silindi == false && x.EnumKagitOkumaDurumu == (int)EnumKagitOkumaDurumu.OKUNDU && x.Sonuclar.Length > 0).ToList();

            tumKagitListe = tumKagitListe.Where(x => x.Sonuclar.Replace(",", "").Length > 0).ToList();

            if (FKSinavKagitID.HasValue)
            {
                var ogrNo = sonucListe.FirstOrDefault(x => x.ID == FKSinavKagitID).OgrenciNo;
                sonucListe = sonucListe.Where(x => x.OgrenciNo == ogrNo).ToList();
            }

            sonucListe = sonucListe.Where(x => x.Sonuclar.Replace(",", "").Length > 0).OrderBy(s => s.DosyaAdi).GroupBy(x => x.OgrenciNo).Select(x => new SinavKagit
            {
                ID = x.FirstOrDefault(y => y.OgrenciNo == x.Key).ID,
                Kullanici = x.FirstOrDefault(y => y.OgrenciNo == x.Key).Kullanici,
                IP = x.FirstOrDefault(y => y.OgrenciNo == x.Key).IP,
                TarihKayit = x.FirstOrDefault(y => y.OgrenciNo == x.Key).TarihKayit,
                TarihOkuma = x.FirstOrDefault(y => y.OgrenciNo == x.Key).TarihOkuma,
                FKSinavID = x.FirstOrDefault(y => y.OgrenciNo == x.Key).FKSinavID,
                FKDersGrupID = x.FirstOrDefault(y => y.OgrenciNo == x.Key).FKDersGrupID,
                DosyaKey = x.FirstOrDefault(y => y.OgrenciNo == x.Key).DosyaKey,
                DosyaAdi = x.FirstOrDefault(y => y.OgrenciNo == x.Key).DosyaAdi,
                HataDetay = x.FirstOrDefault(y => y.OgrenciNo == x.Key).HataDetay,
                OgrenciNo = x.FirstOrDefault(y => y.OgrenciNo == x.Key).OgrenciNo.ToLower(),
                KitapcikTuru = x.FirstOrDefault(y => y.OgrenciNo == x.Key).KitapcikTuru,
                Sonuclar = x.FirstOrDefault(y => y.OgrenciNo == x.Key).Sonuclar
            }).ToList();

            var puanList = db1.ABSPaylarOlcme.Where(x => x.FKPaylarID == FKPaylarID && x.Silindi == false).ToList();
            var testPuanList = puanList.Where(x => x.EnumSoruTipi == (int)EnumSoruTipi.TEST);
            var klasikPuanList = puanList.Where(x => x.EnumSoruTipi == (int)EnumSoruTipi.KLASIK);

            var ogrenciListe = db1.OGRKimlik.Where(x => yazilanOgrNoList.Contains(x.Numara) && x.EnumOgrDurum == (int)EnumOgrDurum.NORMAL).ToList();

            db1.Dispose();
            //db2.Dispose();

            List<SinavNotGirisSoruListeVM> sinavNotListesi = new List<SinavNotGirisSoruListeVM>();
            //List<int> hataliOGRIDList 
            List<string> cevaplar = new List<string>(10);
            foreach (var item in sonucListe)
            {
                cevaplar.Clear();
                for (int i = 0; i < 10; i++)
                {
                    cevaplar.Add("");
                }

                foreach (var element in tumKagitListe.Where(b => b.OgrenciNo == item.OgrenciNo))
                {
                    if (karma)
                    {
                        var tempList = element.Sonuclar.Split(',').ToList();
                        for (int i = 0; i < klasikPuanList.Count(); i++)
                        {
                            if (tempList[i + testPuanList.Count()] == "")
                            {
                                cevaplar[i] = "0";
                            }
                            if (tempList[i + testPuanList.Count()] != "")
                            {
                                cevaplar[i] = tempList[i + testPuanList.Count()].ToString();
                            }
                        }
                    }
                    else
                    {
                        var tempList = element.Sonuclar.Split(',').ToList();
                        for (int i = 0; i < tempList.Count; i++)
                        {
                            if (tempList[i] == "" && cevaplar[i] == "")
                            {
                                cevaplar[i] = "0";
                            }
                            if (tempList[i] != "")
                            {
                                cevaplar[i] = tempList[i].ToString();
                            }
                        }
                    }
                }

                if (ogrenciListe.FirstOrDefault(x => x.Numara.ToLower() == OGRENCI.OGRENCIMAIN.GetirOgrenciNo(item.OgrenciNo).ToLower()) != null)
                {
                    int klasikSoruSira = 1;
                    if (karma)
                    {
                        klasikSoruSira += testPuanList.Count();
                    }

                    foreach (var cevap in cevaplar.Where(x => x != ""))
                    {
                        var kSoru = klasikPuanList.FirstOrDefault(x => x.SoruNo == klasikSoruSira);
                        if (item.EnumKagitOkumaDurumu == (int)EnumKagitOkumaDurumu.HATA)
                        {
                            continue;
                        }

                        int ogrID = ogrenciListe.FirstOrDefault(x => x.Numara.ToLower() == OGRENCI.OGRENCIMAIN.GetirOgrenciNo(item.OgrenciNo).ToLower()).ID;
                        SinavNotGirisSoruListeVM yeniNot = new SinavNotGirisSoruListeVM();
                        if (ogrID == 212396)
                        {
                            string a = "";
                        }
                        yeniNot.Zaman = DateTime.Now;
                        yeniNot.Kullanici = kullanici;
                        yeniNot.Makina = IP;
                        yeniNot.AktarilanVeriMi = false;

                        yeniNot.SoruNo = klasikSoruSira;
                        //yeniNot.SoruNo = item.;
                        //}
                        if (cevap.Length > 0 && !string.IsNullOrEmpty(cevap))
                        {
                            if (Convert.ToByte(cevap) > 100)
                            {
                                item.EnumKagitOkumaDurumu = (int)EnumKagitOkumaDurumu.HATA;
                                item.HataDetay = "Hatalı soru puanı";
                                db2.SaveChanges();
                                continue;
                            }
                            yeniNot.Notu = Convert.ToDouble(cevap);
                        }
                        else
                        {
                            yeniNot.Notu = 0;
                        }

                        yeniNot.FKABSPaylarOlcmeID = kSoru.ID;
                        yeniNot.FKABSPaylarID = FKPaylarID;
                        yeniNot.FKOgrenciID = ogrID;
                        yeniNot.EnumSoruTip = kSoru.EnumSoruTipi.Value;
                        sinavNotListesi.Add(yeniNot);
                        klasikSoruSira++;
                    }
                }
            }

            if (karma)
            {
                return sinavNotListesi;
            }
            else
            {
                SinavNotKaydet(sinavNotListesi, new List<Sabis.Bolum.Core.ABS.DERS.ODEV.OdevAciklamaVM>(), dersGrup.FKDersPlanAnaID.Value, dersGrup.FKDersPlanID.Value, dersGrup.ID, yil, enumDonem, IP, kullanici);
                return sinavNotListesi;
            }
        }

        public static List<ABSPaylarOlcme> GetirSinavPuanListesi(int FKPaylarID, int EnumSoruTipi)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return db.ABSPaylarOlcme.Where(x => x.FKPaylarID == FKPaylarID && x.EnumSoruTipi == EnumSoruTipi && x.Silindi == false).ToList();
            }
        }

        public static List<SinavNotGirisSoruListeVM> TestSinavDegerlendir(int FKSinavID, int FKPaylarID, int FKDersGrupHocaID, int yil, int enumDonem, string kullanici, string IP, int? FKSinavKagitID, bool karma = false)
        {
            UYSv2Entities db1 = new UYSv2Entities();
            KagitOkumaEntities db2 = new KagitOkumaEntities();

            var dersGrupHoca = db1.OGRDersGrupHoca.FirstOrDefault(x => x.ID == FKDersGrupHocaID);
            var dersGrup = db1.OGRDersGrup.FirstOrDefault(x => x.ID == dersGrupHoca.FKDersGrupID);
            var yazilmaList = db1.OGROgrenciYazilma.Where(x => x.FKDersGrupID == dersGrup.ID && x.Yil == yil && x.EnumDonem == enumDonem && x.Iptal == false);

            var sinavDetay = GetirSinavDetayi(FKPaylarID);

            var sinavGereksinimleri = GetirSiraliCevapAnahtari(FKSinavID, FKPaylarID);


            var yazilanOgrNoList = yazilmaList.Select(x => x.OGRKimlik.Numara.ToLower()).ToList();

            var yazilanKullaniciNoList = yazilanOgrNoList.Select(x => OGRENCI.OGRENCIMAIN.GetirKullaniciAdi(x).ToLower());

            var sonucListe = db2.SinavKagit.Where(x => x.FKSinavID == FKSinavID && yazilanKullaniciNoList.Contains(x.OgrenciNo) && x.Silindi == false && x.EnumKagitOkumaDurumu == (int)EnumKagitOkumaDurumu.OKUNDU && x.Sonuclar.Length > 0).ToList();

            if (FKSinavKagitID.HasValue)
            {
                sonucListe = sonucListe.Where(x => x.ID == FKSinavKagitID).ToList();
            }

            sonucListe = sonucListe.OrderBy(s => s.DosyaAdi).GroupBy(x => x.OgrenciNo).Select(x => new SinavKagit
            {
                ID = x.FirstOrDefault(y => y.OgrenciNo == x.Key).ID,
                Kullanici = x.FirstOrDefault(y => y.OgrenciNo == x.Key).Kullanici,
                IP = x.FirstOrDefault(y => y.OgrenciNo == x.Key).IP,
                TarihKayit = x.FirstOrDefault(y => y.OgrenciNo == x.Key).TarihKayit,
                TarihOkuma = x.FirstOrDefault(y => y.OgrenciNo == x.Key).TarihOkuma,
                FKSinavID = x.FirstOrDefault(y => y.OgrenciNo == x.Key).FKSinavID,
                FKDersGrupID = x.FirstOrDefault(y => y.OgrenciNo == x.Key).FKDersGrupID,
                DosyaKey = x.FirstOrDefault(y => y.OgrenciNo == x.Key).DosyaKey,
                DosyaAdi = x.FirstOrDefault(y => y.OgrenciNo == x.Key).DosyaAdi,
                HataDetay = x.FirstOrDefault(y => y.OgrenciNo == x.Key).HataDetay,
                OgrenciNo = x.FirstOrDefault(y => y.OgrenciNo == x.Key).OgrenciNo,
                KitapcikTuru = x.FirstOrDefault(y => y.OgrenciNo == x.Key).KitapcikTuru,
                Sonuclar = x.FirstOrDefault(y => y.OgrenciNo == x.Key).Sonuclar
            }).ToList();

            //.Where(x => yazilanOgrNoList.Contains(Sabis.Core.Utils.OgrenciNo.KullaniciAdiOgrenciNoCevir(x.OgrenciNo))).ToList();

            var puanList = db1.ABSPaylarOlcme.Where(x => x.FKPaylarID == FKPaylarID && x.EnumSoruTipi == (int)EnumSoruTipi.TEST).ToList();
            List<ABSPaylarOlcme> siraliPuanListe = new List<ABSPaylarOlcme>();

            var ogrenciListe = db1.OGRKimlik.Where(x => yazilanOgrNoList.Contains(x.Numara.ToLower()) && x.EnumOgrDurum == (int)EnumOgrDurum.NORMAL).ToList();

            db1.Dispose();
            db2.Dispose();

            List<SinavNotGirisSoruListeVM> sinavNotListesi = new List<SinavNotGirisSoruListeVM>();
            foreach (var item in sonucListe)
            {
                List<string> cevaplar = item.Sonuclar.ToString().Split(',').Take(puanList.Count()).ToList();
                List<string> aktifCevapAnahtari = new List<string>();
                List<double> aktifPuanListe = new List<double>();

                if (item.KitapcikTuru == "A") { aktifCevapAnahtari = sinavGereksinimleri.CevapA; aktifPuanListe = sinavGereksinimleri.PuanA; }
                if (item.KitapcikTuru == "B") { aktifCevapAnahtari = sinavGereksinimleri.CevapB; aktifPuanListe = sinavGereksinimleri.PuanB; }
                if (item.KitapcikTuru == "C") { aktifCevapAnahtari = sinavGereksinimleri.CevapC; aktifPuanListe = sinavGereksinimleri.PuanC; }
                if (item.KitapcikTuru == "D") { aktifCevapAnahtari = sinavGereksinimleri.CevapD; aktifPuanListe = sinavGereksinimleri.PuanD; }
                if (item.KitapcikTuru == "*") { continue; }

                string numara = OGRENCI.OGRENCIMAIN.GetirOgrenciNo(item.OgrenciNo).ToLower();

                if (ogrenciListe.FirstOrDefault(x => x.Numara.ToLower() == numara) != null)
                {
                    int soruSira = 0;

                    foreach (var cevap in cevaplar)
                    {
                        var kSoru = puanList.FirstOrDefault(x => x.SoruNo == soruSira + 1);
                        SinavNotGirisSoruListeVM yeniNot = new SinavNotGirisSoruListeVM();
                        yeniNot.Zaman = DateTime.Now;
                        yeniNot.Kullanici = kullanici;
                        yeniNot.Makina = IP;
                        yeniNot.AktarilanVeriMi = false;
                        yeniNot.SoruNo = soruSira + 1;

                        if (aktifCevapAnahtari.Count() <= soruSira)
                        {
                            continue;
                        }

                        if (cevap == aktifCevapAnahtari.ElementAt(soruSira))
                        {
                            yeniNot.Notu = aktifPuanListe.ElementAt(soruSira);
                        }
                        else if (aktifCevapAnahtari.ElementAt(soruSira) == "X")
                        {
                            yeniNot.Notu = aktifPuanListe.ElementAt(soruSira);
                        }
                        else
                        {
                            yeniNot.Notu = 0;
                        }

                        yeniNot.FKABSPaylarOlcmeID = kSoru.ID;
                        yeniNot.FKABSPaylarID = FKPaylarID;
                        yeniNot.FKOgrenciID = ogrenciListe.FirstOrDefault(x => x.Numara.ToLower() == numara).ID;
                        yeniNot.EnumSoruTip = kSoru.EnumSoruTipi.Value;
                        sinavNotListesi.Add(yeniNot);
                        soruSira++;
                    }
                }
            }

            if (karma)
            {
                return sinavNotListesi;
            }
            else
            {
                SinavNotKaydet(sinavNotListesi, new List<Sabis.Bolum.Core.ABS.DERS.ODEV.OdevAciklamaVM>(), dersGrup.FKDersPlanAnaID.Value, dersGrup.FKDersPlanID.Value, dersGrup.ID, yil, enumDonem, IP, kullanici);
                return sinavNotListesi;
            }

        }

        public static bool SinavDegerlendirilebilirMi(int FKSinavID)
        {
            using (KagitOkumaEntities db = new KagitOkumaEntities())
            {
                return db.SinavKagit.Any(x => x.FKSinavID == FKSinavID && x.Silindi == false && x.EnumKagitOkumaDurumu != (int)Sabis.Bolum.Core.ENUM.ABS.EnumKagitOkumaDurumu.BEKLEMEDE);
            }
        }

        public static List<string> GetirOgrenciSinavKagidi(string ogrNo, int FKPaylarID, int FKDersGrupID, int FKDersPlanID, int FKDersPlanAnaID)
        {
            string kullaniciAdi = OGRENCI.OGRENCIMAIN.GetirKullaniciAdi(ogrNo);
            using (KagitOkumaEntities db = new KagitOkumaEntities())
            {
                return db.SinavKagit.Where(x => x.OgrenciNo == kullaniciAdi && x.Sinav.FKABSPaylarID == FKPaylarID && x.Silindi == false).OrderBy(x => x.DosyaAdi).Select(x => x.DosyaKey).ToList();
            }
        }

        public static List<string> GetirOgrenciSinavKagitListeByOgrenciID(int FKOgrenciID, int FKABSPaylarID) // web api
        {
            UYSv2Entities db1 = new UYSv2Entities();
            KagitOkumaEntities db2 = new KagitOkumaEntities();

            string kullaniciAdi = db1.Kullanici.FirstOrDefault(x => x.FKOgrenciID == FKOgrenciID).KullaniciAdi;
            var sinav = db2.Sinav.FirstOrDefault(x => x.FKABSPaylarID == FKABSPaylarID && x.Silindi == false);

            return db2.SinavKagit.Where(x => x.OgrenciNo == kullaniciAdi && x.FKSinavID == sinav.ID && x.Silindi == false && x.OgrenciGorebilir == true).Select(x => x.DosyaKey).ToList();

        }

        public static List<SinavKagidiObisVM> GetirOgrenciSinavKagitListeByDersGrupID(int DersGrupID, int OgrenciID)
        {
            List<SinavKagidiObisVM> result = new List<SinavKagidiObisVM>();
            using (UYSv2Entities db1 = new UYSv2Entities())
            {
                using (KagitOkumaEntities db2 = new KagitOkumaEntities())
                {
                    var grup = db1.OGRDersGrup.FirstOrDefault(x => x.ID == DersGrupID);
                    var grupPayList = db1.ABSPaylar.Where(x => x.FKDersPlanAnaID == grup.FKDersPlanAnaID && x.Yil == grup.OgretimYili && x.EnumDonem == grup.EnumDonem).Select(x => new { ID = x.ID, EnumCalismaTip = x.EnumCalismaTip.Value, Sira = x.Sira.Value }).ToList();

                    string kullaniciAdi = db1.Kullanici.FirstOrDefault(x => x.FKOgrenciID == OgrenciID).KullaniciAdi;
                    var payList = grupPayList.Select(x => x.ID).ToList();
                    var sorgu = db2.SinavKagit.Where(x => x.OgrenciNo == kullaniciAdi && !x.Sinav.Silindi && payList.Contains(x.Sinav.FKABSPaylarID) && x.Silindi == false && x.OgrenciGorebilir == true).Select(x => new
                    {
                        PayID = x.Sinav.FKABSPaylarID,
                        DosyaKey = x.DosyaKey,
                        OgrenciCevap = x.Sonuclar,
                        SinavID = x.FKSinavID,
                        KitapcikTuru = x.KitapcikTuru
                    }).ToList();

                    foreach (var item in sorgu)
                    {
                        var pay = grupPayList.First(x => x.ID == item.PayID);
                        SinavKagidiObisVM kagit = new SinavKagidiObisVM()
                        {
                            OgrenciCevap = item.OgrenciCevap,
                            DosyaKey = item.DosyaKey,
                            PayID = item.PayID,
                            CalismaTip = (EnumCalismaTip)pay.EnumCalismaTip,
                            Sira = pay.Sira
                        };

                        var cevapAnahtari = db1.ABSTestCevapAnahtari.FirstOrDefault(x => x.FKSinavID == item.SinavID);
                        if (cevapAnahtari != null)
                        {
                            switch (item.KitapcikTuru)
                            {
                                case "A":
                                    kagit.CevapAnahtari = cevapAnahtari.CevapA;
                                    break;
                                case "B":
                                    kagit.CevapAnahtari = cevapAnahtari.CevapB;
                                    break;
                                case "C":
                                    kagit.CevapAnahtari = cevapAnahtari.CevapC;
                                    break;
                                case "D":
                                    kagit.CevapAnahtari = cevapAnahtari.CevapD;
                                    break;
                                default:
                                    break;
                            }
                        }

                        result.Add(kagit);
                    }
                }
            }
            return result;
        }

        public static SinavKagidiVM GetirOgrenciSinavKagitListesi(int FKSinavKagitID, string kullaniciAdi, int FKPaylarID, int FKDersGrupID, int FKDersPlanID, int FKDersPlanAnaID)
        {
            UYSv2Entities db1 = new UYSv2Entities();
            using (KagitOkumaEntities db = new KagitOkumaEntities())
            {
                SinavKagidiVM kagit = db.SinavKagit.Where(x => x.ID == FKSinavKagitID)
                    .Select(x => new SinavKagidiVM
                    {
                        ID = x.ID,
                        OgrenciNo = x.OgrenciNo,
                        DosyaKey = x.DosyaKey,
                        DosyaAdi = x.DosyaAdi,
                        FKSinavID = x.FKSinavID,
                        Sonuclar = x.Sonuclar,
                        EnumKagitOkumaDurumu = x.EnumKagitOkumaDurumu,
                        KagitGrubu = x.KitapcikTuru
                    }).ToList().FirstOrDefault();



                if (kagit.OgrenciNo != "" && kagit.OgrenciNo != "*")
                {
                    try
                    {
                        string numara = OGRENCI.OGRENCIMAIN.GetirOgrenciNo(kagit.OgrenciNo);
                        kagit.OgrenciAdSoyad = db1.OGRKimlik.Where(y => y.Numara == numara).Select(y => y.Kisi.Ad + " " + y.Kisi.Soyad).FirstOrDefault();
                    }
                    catch (Exception)
                    {
                    }
                }

                return kagit;

            }
        }

        public static bool GuncelleOgrenciSinavKagitSonucu(SinavKagidiVM sonuclar)
        {
            try
            {
                using (KagitOkumaEntities db = new KagitOkumaEntities())
                {
                    SinavKagit kagit = db.SinavKagit.FirstOrDefault(x => x.ID == sonuclar.ID);
                    kagit.OgrenciNo = sonuclar.OgrenciNo;
                    if (sonuclar.KagitGrubu != null)
                    {
                        kagit.KitapcikTuru = sonuclar.KagitGrubu.ToUpper();
                    }
                    if (sonuclar.Sonuclar != null)
                    {
                        kagit.Sonuclar = sonuclar.Sonuclar;
                    }
                    kagit.EnumKagitOkumaDurumu = (int)EnumKagitOkumaDurumu.OKUNDU;
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static int GetirDersGrupHocaID(int FKDersPlanAnaID, int yil, int donem)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                if (db.OGRDersGrupHoca.Any(x => x.OGRDersGrup.FKDersPlanAnaID == FKDersPlanAnaID && x.OGRDersGrup.OgretimYili == yil && x.OGRDersGrup.EnumDonem == donem && x.FKPersonelID.HasValue))
                {
                    return db.OGRDersGrupHoca.FirstOrDefault(x => x.OGRDersGrup.FKDersPlanAnaID == FKDersPlanAnaID && x.OGRDersGrup.OgretimYili == yil && x.OGRDersGrup.EnumDonem == donem && x.FKPersonelID.HasValue).ID;
                }
                else
                {
                    return 0;
                }

            }
        }

        public static bool SilSinavKagitlari(int FKSinavID, int FKDersGrupID, string kullaniciAdi)
        {
            try
            {
                using (KagitOkumaEntities db = new KagitOkumaEntities())
                {
                    var sinavKagitListe = db.SinavKagit.Where(x => x.FKSinavID == FKSinavID && x.Kullanici == kullaniciAdi);
                    foreach (var item in sinavKagitListe)
                    {
                        db.SinavKagit.Remove(item);
                    }
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool OptikSonuclariDegerlendirilebilirMi(int fkSinavID)
        {
            try
            {
                UYSv2Entities db1 = new UYSv2Entities();
                using (KagitOkumaEntities db = new KagitOkumaEntities())
                {
                    var sinav = db.Sinav.FirstOrDefault(x => x.ID == fkSinavID);
                    if (sinav.EnumSinavTuru == (int)EnumSinavTuru.KLASIK || sinav.EnumSinavTuru == (int)EnumSinavTuru.ODEV)
                    {
                        return true;
                    }
                    else
                    {
                        return db1.ABSTestCevapAnahtari.Any(x => x.FKSinavID == sinav.ID);
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static int GetirToplamOptikKagitSayisi()
        {
            using (KagitOkumaEntities db = new KagitOkumaEntities())
            {
                return db.SinavKagit.Count(x => x.EnumKagitOkumaDurumu == 1 && x.Silindi == false);
            }
        }

        public static SinavIstatistikleriVM GetirSinavIstatistik(int fkAbsPaylarID, int fkDersGrupID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                SinavIstatistikleriVM istatistik = new SinavIstatistikleriVM();
                IstatistikVM genelIstatistik = new IstatistikVM();
                IstatistikVM grupIstatistik = new IstatistikVM();


                var dersGrup = db.OGRDersGrup.FirstOrDefault(x => x.ID == fkDersGrupID);
                var ogrList = db.OGROgrenciYazilma.Where(x => x.FKDersPlanAnaID == dersGrup.FKDersPlanAnaID && x.Yil == dersGrup.OgretimYili && x.EnumDonem == dersGrup.EnumDonem && !x.Iptal);
                var grupOGRList = ogrList.Where(x => x.FKDersGrupID == fkDersGrupID);


                var notList = db.ABSOgrenciNot.Where(x => x.FKAbsPaylarID == fkAbsPaylarID && x.NotDeger != 120 && x.NotDeger != 130 && !x.Silindi).ToList();
                var grupNotList = notList.Where(x => x.FKDersGrupID == fkDersGrupID).ToList();


                if (grupNotList.Any())
                {
                    genelIstatistik.AzamiNot = notList.Max(x => x.NotDeger);
                    genelIstatistik.AsgariNot = notList.Min(x => x.NotDeger);
                    genelIstatistik.OrtalamaNot = notList.Average(x => x.NotDeger);
                    genelIstatistik.SifirAlanSayisi = notList.Count(x => x.NotDeger == 0);
                    genelIstatistik.YuzAlanSayisi = notList.Count(x => x.NotDeger == 100);
                    genelIstatistik.SinavaKatilan = notList.Count(x => x.NotDeger != 120 && x.NotDeger != 130);
                    genelIstatistik.SinavaKatilmayan = notList.Count(x => x.NotDeger == 120 || x.NotDeger == 130);
                    genelIstatistik.OgrenciSayisi = ogrList.Count();

                    grupIstatistik.AzamiNot = grupNotList.Max(x => x.NotDeger);
                    grupIstatistik.AsgariNot = grupNotList.Min(x => x.NotDeger);
                    grupIstatistik.OrtalamaNot = grupNotList.Average(x => x.NotDeger);
                    grupIstatistik.SifirAlanSayisi = grupNotList.Count(x => x.NotDeger == 0);
                    grupIstatistik.YuzAlanSayisi = grupNotList.Count(x => x.NotDeger == 100);
                    grupIstatistik.SinavaKatilan = grupNotList.Count(x => x.NotDeger != 120 && x.NotDeger != 130);
                    grupIstatistik.SinavaKatilmayan = grupNotList.Count(x => x.NotDeger == 120 || x.NotDeger == 130);
                    grupIstatistik.OgrenciSayisi = grupOGRList.Count();

                    istatistik.GenelIstatistikler = genelIstatistik;
                    istatistik.GrupIstatistikleri = grupIstatistik;
                }

                return istatistik;
            }
        }
    }
}
