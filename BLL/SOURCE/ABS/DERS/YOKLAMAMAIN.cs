﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Bolum.Core.ABS.DERS.DERSDETAY;
using Sabis.Enum;

namespace Sabis.Bolum.Bll.SOURCE.ABS.DERS
{
    public class YOKLAMAMAIN
    {
        public static List<YoklamaVM> GetirYoklamaListe(int dersGrupHocaID, DateTime dersTarihi)
        {
            List<YoklamaVM> yoklamaListesi = new List<YoklamaVM>();
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var liste = (from yazilma in db.OGROgrenciYazilma
                             join dersGrup in db.OGRDersGrup on yazilma.FKDersGrupID equals dersGrup.ID
                             join dersGrupHoca in db.OGRDersGrupHoca on dersGrup.ID equals dersGrupHoca.FKDersGrupID
                             where yazilma.Iptal == false && dersGrupHoca.ID == dersGrupHocaID && yazilma.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL
                             select new
                             {
                                 OgrenciID = yazilma.FKOgrenciID.Value,
                                 yazilma.OGRKimlik.Kisi.Ad,
                                 yazilma.OGRKimlik.Kisi.Soyad,
                                 yazilma.OGRKimlik.Numara,
                                 YoklamaListesi = yazilma.OGRKimlik.ABSDevamTakipV2.Where(d => d.FK_OgrenciID == yazilma.FKOgrenciID && !d.Silindi).ToList(),
                                 ProgramListesi = dersGrupHoca.OGRDersProgramiv2.Where(d => d.FKDersGrupHocaID == dersGrupHocaID && d.DersTarihSaat.Year == dersTarihi.Year && d.DersTarihSaat.Month == dersTarihi.Month && d.DersTarihSaat.Day == dersTarihi.Day).ToList()
                             }).ToList();

                foreach (var item in liste)
                {
                    YoklamaVM yoklama = new YoklamaVM();
                    yoklama.Ad = item.Ad;
                    yoklama.Soyad = item.Soyad;
                    yoklama.OgrenciID = item.OgrenciID;
                    yoklama.Numara = item.Numara;

                    foreach (var program in item.ProgramListesi)
                    {
                        YoklamaDetayVM yd = new YoklamaDetayVM();

                        if (item.YoklamaListesi.Any(d => d.FK_DersProgramV2ID == program.ID))
                        {
                            var yoklamaItem = item.YoklamaListesi.First(d => d.FK_DersProgramV2ID == program.ID);
                            yoklama.ListYoklamaDetay.Add(new YoklamaDetayVM { DevamTakipID = yoklamaItem.ID, FKOgrenciID = item.OgrenciID, FKDersProgramV2ID = program.ID, YoklamaTarihi = yoklamaItem.YoklamaTarihi, Secili = true, DersSaati = program.DersTarihSaat.ToString("HH:mm") });
                        }
                        else
                        {
                            yoklama.ListYoklamaDetay.Add(new YoklamaDetayVM { DevamTakipID = null, FKOgrenciID = item.OgrenciID, FKDersProgramV2ID = program.ID, Secili = false, DersSaati = program.DersTarihSaat.ToString("HH:mm") });

                        }
                    }
                    yoklamaListesi.Add(yoklama);
                }
            }
            return yoklamaListesi.OrderBy(d => d.Numara).ToList();
        }

        public static bool KaydetYoklama(List<YoklamaVM> liste)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    foreach (var item in liste)
                    {
                        foreach (var yoklama in item.ListYoklamaDetay)
                        {
                            if (yoklama.Secili == true && yoklama.DevamTakipID.HasValue == false)
                            {
                                ABSDevamTakipV2 y = new ABSDevamTakipV2();
                                y.FK_DersProgramV2ID = yoklama.FKDersProgramV2ID;
                                y.FK_OgrenciID = yoklama.FKOgrenciID;
                                y.Yil = yoklama.Yil;
                                y.Donem = yoklama.Donem;
                                y.YoklamaTarihi = DateTime.Now;
                                y.Kullanici = yoklama.Kullanici;
                                db.ABSDevamTakipV2.Add(y);
                                db.SaveChanges();
                            }
                            else if (yoklama.Secili == false && yoklama.DevamTakipID.HasValue == true)
                            {
                                var silinecekKayit = db.ABSDevamTakipV2.FirstOrDefault(x => x.ID == yoklama.DevamTakipID.Value);
                                if (silinecekKayit != null)
                                {
                                    silinecekKayit.Silindi = true;
                                    db.SaveChanges();
                                }
                            }

                        }
                    }
                }
                return true;
            }
            catch (Exception error)
            {
                return false;
            }
        }

        public static List<DevamsizOgrenciVM> GetirDevamsizOgrenciListesi(int dersGrupID)
        {
            List<DevamsizOgrenciVM> liste = new List<DevamsizOgrenciVM>();
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var dersGrup = db.OGRDersGrup.FirstOrDefault(d => d.ID == dersGrupID);
                var sorgu = from pay in db.ABSPaylar
                            where pay.EnumCalismaTip == (int)Sabis.Enum.EnumCalismaTip.Final && pay.FKDersPlanAnaID == dersGrup.FKDersPlanAnaID && pay.FKDersPlanID == dersGrup.FKDersPlanID && pay.Yil == dersGrup.OgretimYili && pay.EnumDonem == dersGrup.EnumDonem
                            select new
                            {
                                pay.ID,
                            };

                if (!sorgu.Any())
                {
                    if (dersGrup.OGRDersPlan.OGRDersPlanAna.EnumKokDersTipi == (int)EnumKokDersTipi.UZMANLIKALANI)
                    {
                        sorgu = from pay in db.ABSPaylar
                                where pay.EnumCalismaTip == (int)Sabis.Enum.EnumCalismaTip.Final && pay.FKDersPlanAnaID == dersGrup.FKDersPlanAnaID && pay.FKDersPlanID == dersGrup.FKDersPlanID && (pay.Yil == dersGrup.OgretimYili && (pay.OGRDersPlanAna.EnumDersSureTip == (int)EnumDersSureTip.YILLIK || (pay.OGRDersPlanAna.EnumDersSureTip == (int)EnumDersSureTip.DONEMLIK && pay.EnumDonem == dersGrup.EnumDonem)))
                                select new
                                {
                                    pay.ID,
                                };
                    }
                    else
                    {
                        sorgu = from pay in db.ABSPaylar
                                where pay.EnumCalismaTip == (int)Sabis.Enum.EnumCalismaTip.Final && pay.FKDersPlanAnaID == dersGrup.FKDersPlanAnaID && pay.FKDersPlanID == null && (pay.Yil == dersGrup.OgretimYili && (pay.OGRDersPlanAna.EnumDersSureTip == (int)EnumDersSureTip.YILLIK || (pay.OGRDersPlanAna.EnumDersSureTip == (int)EnumDersSureTip.DONEMLIK && pay.EnumDonem == dersGrup.EnumDonem)))
                                select new
                                {
                                    pay.ID,
                                };
                    }
                }
                List<int> payidlist = sorgu.Select(d => d.ID).ToList();

                string yilStr = dersGrup.OgretimYili.Value.ToString();
                liste = (from yazilma in db.OGROgrenciYazilma
                         where yazilma.FKDersGrupID == dersGrupID && yazilma.Iptal == false && yazilma.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL &&
                            (yazilma.Yil == dersGrup.OgretimYili && (yazilma.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.EnumDersSureTip == (int)EnumDersSureTip.YILLIK || (yazilma.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.EnumDersSureTip == (int)EnumDersSureTip.DONEMLIK && yazilma.EnumDonem == dersGrup.EnumDonem)))
                         group yazilma by new
                         {
                             Notu = db.ABSOgrenciNot.OrderByDescending(d => d.ID).Where(d => d.Silindi == false && d.FKOgrenciID == yazilma.FKOgrenciID && payidlist.Contains(d.FKAbsPaylarID.Value) && d.FKDersGrupID == yazilma.FKDersGrupID).FirstOrDefault(),
                             Pay = db.ABSPaylar.Where(d => !d.Silindi && payidlist.Contains(d.ID)).FirstOrDefault().ID,
                             yazilma.FKOgrenciID,
                             YazilmaID = yazilma.ID,
                             yazilma.FKDersPlanID,
                             yazilma.OGRKimlik.Numara,
                             yazilma.OGRKimlik.Kisi.Ad,
                             yazilma.OGRKimlik.Kisi.Soyad,
                             yazilma.FKDersPlanAnaID,
                             yazilma.FKDersGrupID,

                         } into g
                         select new DevamsizOgrenciVM
                         {
                             Numara = g.Key.Numara,
                             Ad = g.Key.Ad,
                             Soyad = g.Key.Soyad,
                             OgrenciID = g.Key.FKOgrenciID,
                             PayID = g.Key.Pay,
                             DersPlanAnaID = g.Key.FKDersPlanAnaID,
                             DersGrupID = g.Key.FKDersGrupID,
                             DersPlanID = g.Key.FKDersPlanID,
                             Notu = g.Key.Notu.NotDeger,
                             Devamsiz = g.Key.Notu.NotDeger == 130 ? true : false,
                             Aktif = ((g.Key.Notu == null || g.Key.Notu.NotDeger != 130) ? true : false),
                             //Notlar = db.ABSOgrenciNot.Where(x => x.FKOgrenciID == yazilma.FKOgrenciID && x.EnumDonem == dersGrup.EnumDonem && x.Yil == yilStr).OrderBy(d => d.ID),
                             YazilmaID = g.Key.YazilmaID,
                         }).ToList();
            }
            return liste;

        }

        private class DevamListeDto
        {
            public int OgrenciID { get; set; }
            public DateTime DersTarihSaat { get; set; }
            public int DersProgramID { get; set; }
        }
        public static List<KatilimOraniVM> ListeleKatilimOraniV2(int dersGrupHocaID)
        {
            List<KatilimOraniVM> katilimOrani = new List<KatilimOraniVM>();
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var liste = (from yazilma in db.OGROgrenciYazilma
                             join dersGrup in db.OGRDersGrup on yazilma.FKDersGrupID equals dersGrup.ID
                             join dersGrupHoca in db.OGRDersGrupHoca on dersGrup.ID equals dersGrupHoca.FKDersGrupID
                             where yazilma.Iptal == false && dersGrupHoca.ID == dersGrupHocaID && yazilma.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL && yazilma.FKOgrenciID.HasValue
                             select new
                             {
                                 OgrenciID = yazilma.FKOgrenciID.Value,
                                 yazilma.OGRKimlik.Kisi.Ad,
                                 yazilma.OGRKimlik.Kisi.Soyad,
                                 yazilma.OGRKimlik.Numara,
                                 //DersProgrami = dersGrupHoca.OGRDersProgramiv2.ToList(),
                                 //DevamTakip = yazilma.OGRKimlik.ABSDevamTakipV2.Where(d => d.FK_OgrenciID == yazilma.FKOgrenciID).ToList(),
                             }).ToList();

                var dersProgrami = db.OGRDersProgramiv2
                    .Where(x => x.FKDersGrupHocaID == dersGrupHocaID)
                    .GroupBy(x => new { x.DersTarihSaat.Year, x.DersTarihSaat.Month, x.DersTarihSaat.Day })
                    .Select(x => new { Yil = x.Key.Year, Ay = x.Key.Month, Gun = x.Key.Day, DersProgListe = x.Select(y => new { DersProgramID = y.ID, y.DersTarihSaat }) })
                    .ToList();

                var devamListesi = db.ABSDevamTakipV2
                    .Where(x => x.OGRDersProgramiv2.FKDersGrupHocaID == dersGrupHocaID && !x.Silindi)
                    .Select(x => new DevamListeDto
                    {
                        OgrenciID = x.FK_OgrenciID, /*Ad = x.OGRKimlik.Kisi.Ad, Soyad = x.OGRKimlik.Kisi.Soyad, x.OGRKimlik.Numara, */
                        DersTarihSaat = x.OGRDersProgramiv2.DersTarihSaat,
                        DersProgramID = x.FK_DersProgramV2ID
                    })
                    .GroupBy(x => x.OgrenciID)
                    .ToDictionary(x => x.Key, y => y.ToList());



                foreach (var item in liste)
                {
                    KatilimOraniVM katilim = new KatilimOraniVM();
                    katilim.AdSoyad = item.Ad + " " + item.Soyad;
                    katilim.OgrenciID = item.OgrenciID;
                    katilim.Numara = item.Numara;

                    //var sorgu = (from dersprogrami in item.DersProgrami
                    //             join dvm in item.DevamTakip on dersprogrami.ID equals dvm.FK_DersProgramV2ID into d
                    //             from devamtakip in d.DefaultIfEmpty()
                    //             orderby dersprogrami.DersTarihSaat
                    //             group dersprogrami by new { dersprogrami.DersTarihSaat.Year, dersprogrami.DersTarihSaat.Month, dersprogrami.DersTarihSaat.Day } into g
                    //             select new { Tarih = (g.Key.Day + "/" + g.Key.Month + "/" + g.Key.Year), Geldigi = g.Count(d => d.ABSDevamTakipV2.Where(x => x.FK_OgrenciID == item.OgrenciID).Count() > 0), DersSaati = g.Count() }).ToList();
                    List<DevamListeDto> ogrenciDevam = null;
                    double toplamDersSaati = 0;// sorgu.Sum(d => d.DersSaati);
                    double geldigiDersSaati = 0;// sorgu.Sum(d => d.Geldigi);

                    if (devamListesi.TryGetValue(item.OgrenciID, out ogrenciDevam))
                    {

                    }

                    foreach (var dp in dersProgrami)
                    {
                        KatilimOraniHaftaVM ko = new KatilimOraniHaftaVM();
                        ko.KatilimSaati = 0;
                        ko.Tarih = new DateTime(dp.Yil, dp.Ay, dp.Gun);
                        foreach (var programGun in dp.DersProgListe)
                        {
                            if (ogrenciDevam != null && ogrenciDevam.Any(x => x.DersProgramID == programGun.DersProgramID))
                            {
                                ko.KatilimSaati++;
                                geldigiDersSaati++;
                            }
                        }
                        ko.ToplamDersSaati = dp.DersProgListe.Count();
                        toplamDersSaati += ko.ToplamDersSaati;
                        katilim.KatilimOraniHaftaVM.Add(ko);
                    }
                    double oran = 0;
                    if (toplamDersSaati != 0)
                    {
                        oran = (geldigiDersSaati / toplamDersSaati) * 100d;
                    }
                    katilim.Orani = oran;
                    katilimOrani.Add(katilim);
                }
            }
            return katilimOrani.OrderBy(d => d.Numara).ToList();
        }

        public static void KaydetYoklamaV3(int dersGrupHocaID, DateTime dersTarihi, Dictionary<int, bool> devamListe)
        {
            var liste = GetirYoklamaListe(dersGrupHocaID, dersTarihi);

            using (UYSv2Entities db = new UYSv2Entities())
            {
                foreach (var item in liste)
                {
                    foreach (var yoklama in item.ListYoklamaDetay)
                    {
                        if (devamListe[item.OgrenciID] == true && yoklama.DevamTakipID.HasValue == false)
                        {
                            ABSDevamTakipV2 y = new ABSDevamTakipV2();
                            y.FK_DersProgramV2ID = yoklama.FKDersProgramV2ID;
                            y.FK_OgrenciID = yoklama.FKOgrenciID;
                            y.Yil = yoklama.Yil;
                            y.Donem = yoklama.Donem;
                            y.YoklamaTarihi = dersTarihi;
                            y.Kullanici = yoklama.Kullanici;
                            db.ABSDevamTakipV2.Add(y);
                            db.SaveChanges();
                        }
                        else if (devamListe[item.OgrenciID] == false && yoklama.DevamTakipID.HasValue == true)
                        {
                            var silinecekKayit = db.ABSDevamTakipV2.FirstOrDefault(x => x.ID == yoklama.DevamTakipID.Value);
                            if (silinecekKayit != null)
                            {
                                silinecekKayit.Silindi = true;
                                db.SaveChanges();
                            }
                        }
                    }
                }
            }
        }
    }
}
