﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Bolum.Core.ABS.DERS;
using Sabis.Enum;

namespace Sabis.Bolum.Bll.SOURCE.ABS.DERS
{
    public class DOKUMANMAIN
    {
        public static List<DokumanVM> GetirDokumanListesi(int FKDersGrupHocaID, int FKPersonelID, int yil, int donem, bool tumListe = false)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var dgHOCA = db.OGRDersGrupHoca.FirstOrDefault(x => x.ID == FKDersGrupHocaID);


                List<ABSDokuman> tumIcerik = db.ABSDokuman.Where(x => x.Silindi == false && (x.FKDersPlanAnaID == dgHOCA.OGRDersGrup.FKDersPlanAnaID || x.ABSDokumanGrup.Any(y => y.FKOGRDersGrupID == dgHOCA.FKDersGrupID && y.ABSDokuman.Silindi == false) && (x.FKPersonelID == FKPersonelID || x.DigerHocalarGorebilir == true) && x.Silindi == false && x.Yil == yil && x.EnumDonem == donem)).ToList();


                var dokumanListe = tumIcerik.Select(x => new DokumanVM
                {
                    ID = x.ID,
                    DosyaAdi = x.DosyaAdi,
                    DosyaKey = x.DosyaKey,
                    Aciklama = x.Aciklama,
                    DosyaTuru = x.DosyaTuru,
                    EnumIcerikTipi = x.EnumIcerikTipi,
                    Yil = x.Yil,
                    EnumDonem = x.EnumDonem,
                    Hafta = x.Hafta.Value,
                    DigerHocalarGorebilir = x.DigerHocalarGorebilir,
                    EBSGosterilsin = x.EBSGosterilsin,
                    FKDersPlanAnaID = x.FKDersPlanAnaID,
                    FKDersGrupIDList = db.ABSDokumanGrup.Where(y => y.FKABSDokumanID == x.ID && y.Silindi == false).Select(y => y.FKOGRDersGrupID).ToList(),
                    FKPersonelID = x.FKPersonelID,
                    EkleyenAdSoyadUnvan = x.PersonelBilgisi.UnvanAd + " " + x.PersonelBilgisi.Ad + " " + x.PersonelBilgisi.Soyad,
                    SGKullanici = x.SGKullanici,
                    SGIP = x.SGIP,
                    SGTarih = x.SGTarih,
                    Silindi = x.Silindi
                }).ToList();

                dokumanListe = dokumanListe.Where(x => x.FKPersonelID == FKPersonelID || (x.FKPersonelID != FKPersonelID && x.DigerHocalarGorebilir)).ToList();

                if (!tumListe)
                {
                    dokumanListe = dokumanListe.Where(x => x.FKDersGrupIDList.Contains(dgHOCA.OGRDersGrup.ID)).ToList();
                }
                var test = dokumanListe.Where(x => x.Yil == yil && x.EnumDonem == donem).OrderBy(x => x.Hafta).ToList();
                return dokumanListe.Where(x=> x.Yil == yil && x.EnumDonem == donem).OrderBy(x => x.Hafta).ToList();
            }
        }

        public static List<DokumanVM> GetirDokumanListesiByDersGrupID(int fkDersGrupID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return db.ABSDokumanGrup.Where(x => x.FKOGRDersGrupID == fkDersGrupID && x.Silindi == false).Select(x => new DokumanVM
                {
                    DosyaAdi = x.ABSDokuman.DosyaAdi,
                    DosyaKey = x.ABSDokuman.DosyaKey,
                    Hafta = x.ABSDokuman.Hafta.Value,
                    Aciklama = x.ABSDokuman.Aciklama,
                    EkleyenAdSoyadUnvan = x.ABSDokuman.PersonelBilgisi.UnvanAd + " " + x.ABSDokuman.PersonelBilgisi.Ad + " " + x.ABSDokuman.PersonelBilgisi.Soyad,
                    DosyaTuru = x.ABSDokuman.DosyaTuru,
                    DosyaBoyutu = x.ABSDokuman.DosyaBoyutu.Value,
                    SGTarih = x.ABSDokuman.SGTarih,
                }).OrderBy(x => x.Hafta).ToList();
            }
        }

        public static List<DokumanVM> GetirDokumanListesiByDersGrupID(int DersGrupID, int PersonelID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return db.ABSDokumanGrup.Where(x => x.FKOGRDersGrupID == DersGrupID && x.ABSDokuman.FKPersonelID == PersonelID && x.ABSDokuman.Silindi == false && x.Silindi == false).Select(x => new DokumanVM
                {
                    DosyaAdi = x.ABSDokuman.DosyaAdi,
                    DosyaKey = x.ABSDokuman.DosyaKey,
                    Hafta = x.ABSDokuman.Hafta,
                    Aciklama = x.ABSDokuman.Aciklama,
                    EkleyenAdSoyadUnvan = x.ABSDokuman.PersonelBilgisi.UnvanAd + " " + x.ABSDokuman.PersonelBilgisi.Ad + " " + x.ABSDokuman.PersonelBilgisi.Soyad,
                    DosyaTuru = x.ABSDokuman.DosyaTuru,
                    DosyaBoyutu = x.ABSDokuman.DosyaBoyutu.HasValue ? x.ABSDokuman.DosyaBoyutu.Value : 0,
                    SGTarih = x.ABSDokuman.SGTarih,
                    DigerHocalarGorebilir = x.ABSDokuman.DigerHocalarGorebilir,
                    Yil = x.ABSDokuman.Yil,
                    EnumDonem = x.ABSDokuman.EnumDonem,
                    DosyaAdres = x.ABSDokuman.DosyaAdi,
                    EnumIcerikTipi = x.ABSDokuman.EnumIcerikTipi
                }).OrderBy(x => x.Hafta).ToList();
            }
        }

        public static bool Kaydet(DokumanVM dokuman, string kullaniciAdi, string IP)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                try
                {
                    if (dokuman.ID.HasValue)
                    {
                        ABSDokuman dok = db.ABSDokuman.First(x => x.ID == dokuman.ID);
                        List<ABSDokumanGrup> dokGrup = db.ABSDokumanGrup.Where(x => x.FKABSDokumanID == dokuman.ID).ToList();

                        if (dokuman.DosyaKey == null) //yeni dosya seçilmediyse eskisinin özelliklerini yeniden eklemek gerekiyor.
                        {
                            dokuman.DosyaKey = dok.DosyaKey;
                            dokuman.DosyaBoyutu = dok.DosyaBoyutu.Value;
                            dokuman.DosyaTuru = dok.DosyaTuru;
                        }

                        foreach (var item in dokGrup)
                        {
                            item.Silindi = true;
                            item.SGTarih = DateTime.Now;
                            item.SGKullanici = kullaniciAdi;
                            item.SGIP = IP;
                        }
                        db.SaveChanges();

                        dok.Silindi = true;
                        dok.SGIP = IP;
                        dok.SGKullanici = kullaniciAdi;
                        dok.SGTarih = DateTime.Now;
                        dok.Hafta = dokuman.Hafta;
                        db.SaveChanges();
                    }

                    var dgHOCA = db.OGRDersGrupHoca.FirstOrDefault(x => x.ID == dokuman.FKDersGrupHocaID);

                    var hocaninGruplari = db.OGRDersGrupHoca.Where(x => x.FKPersonelID == dokuman.FKPersonelID && x.OGRDersGrup.FKDersPlanAnaID == dokuman.FKDersPlanAnaID && x.OGRDersGrup.OgretimYili == dokuman.Yil && x.OGRDersGrup.EnumDonem == dokuman.EnumDonem).Select(x => x.OGRDersGrup.ID).ToList();
                    var tumDersGruplari = db.OGRDersGrup.Where(x => x.FKDersPlanAnaID == dokuman.FKDersPlanAnaID && x.OgretimYili == dokuman.Yil && x.EnumDonem == dokuman.EnumDonem).Select(x => x.ID).ToList();

                    ABSDokuman yeniDokuman = new ABSDokuman();
                    yeniDokuman.DosyaAdi = dokuman.DosyaAdi;
                    yeniDokuman.DosyaKey = dokuman.DosyaKey;
                    yeniDokuman.Aciklama = dokuman.Aciklama;
                    yeniDokuman.DosyaTuru = dokuman.DosyaTuru;
                    yeniDokuman.DosyaBoyutu = dokuman.DosyaBoyutu;
                    yeniDokuman.EnumIcerikTipi = dokuman.EnumIcerikTipi;
                    yeniDokuman.Yil = dokuman.Yil;
                    yeniDokuman.EnumDonem = dokuman.EnumDonem;
                    yeniDokuman.Hafta = dokuman.Hafta;
                    yeniDokuman.DigerHocalarGorebilir = dokuman.DigerHocalarGorebilir;
                    yeniDokuman.EBSGosterilsin = dokuman.EBSGosterilsin;
                    yeniDokuman.FKDersPlanAnaID = dgHOCA.OGRDersGrup.FKDersPlanAnaID.Value;
                    //yeniDokuman.DersGrupList = Convert.ToString(dgHOCA.OGRDersGrup.ID);
                    yeniDokuman.FKPersonelID = dokuman.FKPersonelID;
                    yeniDokuman.SGKullanici = kullaniciAdi;
                    yeniDokuman.SGIP = IP;
                    yeniDokuman.SGTarih = DateTime.Now;
                    yeniDokuman.Silindi = false;
                    yeniDokuman.TumGruplaraEkle = dokuman.TumGruplaraEkle;
                    yeniDokuman.TumGruplarimaEkle = dokuman.DigerGruplaraEkle;
                    db.ABSDokuman.Add(yeniDokuman);
                    db.SaveChanges();

                    if (dokuman.TumGruplaraEkle)
                    {
                        foreach (var item in tumDersGruplari)
                        {
                            ABSDokumanGrup yeniGrup = new ABSDokumanGrup();
                            yeniGrup.FKABSDokumanID = yeniDokuman.ID;
                            yeniGrup.FKOGRDersGrupID = item;
                            yeniGrup.SGTarih = DateTime.Now;
                            yeniGrup.SGKullanici = kullaniciAdi;
                            yeniGrup.SGIP = IP;
                            yeniGrup.Silindi = false;
                            db.ABSDokumanGrup.Add(yeniGrup);
                        }
                        db.SaveChanges();
                    }
                    else if(dokuman.DigerGruplaraEkle)
                    {
                        foreach (var item in hocaninGruplari)
                        {
                            ABSDokumanGrup yeniGrup = new ABSDokumanGrup();
                            yeniGrup.FKABSDokumanID = yeniDokuman.ID;
                            yeniGrup.FKOGRDersGrupID = item;
                            yeniGrup.SGTarih = DateTime.Now;
                            yeniGrup.SGKullanici = kullaniciAdi;
                            yeniGrup.SGIP = IP;
                            yeniGrup.Silindi = false;
                            db.ABSDokumanGrup.Add(yeniGrup);
                        }
                        db.SaveChanges();
                    }
                     
                    else
                    {
                        ABSDokumanGrup yeniDokumanGrup = new ABSDokumanGrup();
                        yeniDokumanGrup.FKABSDokumanID = yeniDokuman.ID;
                        yeniDokumanGrup.FKOGRDersGrupID = dgHOCA.OGRDersGrup.ID;
                        yeniDokumanGrup.SGTarih = DateTime.Now;
                        yeniDokumanGrup.SGKullanici = kullaniciAdi;
                        yeniDokumanGrup.SGIP = IP;
                        yeniDokumanGrup.Silindi = false;
                        db.ABSDokumanGrup.Add(yeniDokumanGrup);
                        db.SaveChanges();
                    }

                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }

        public static bool Sil(int dokumanID, string kullaniciAdi, string IP)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var dokuman = db.ABSDokuman.FirstOrDefault(x => x.ID == dokumanID);
                    var dokumanGrupList = db.ABSDokumanGrup.Where(x => x.FKABSDokumanID == dokuman.ID);

                    foreach (var item in dokumanGrupList)
                    {
                        item.Silindi = true;
                        item.SGTarih = DateTime.Now;
                        item.SGKullanici = kullaniciAdi;
                        item.SGIP = IP;
                    }
                    db.SaveChanges();

                    dokuman.Silindi = true;
                    dokuman.SGTarih = DateTime.Now;
                    dokuman.SGKullanici = kullaniciAdi;
                    dokuman.SGIP = IP;

                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool GrupEkle(int DokumanID, int fkDersGrupID, string kullaniciAdi, string IP)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    ABSDokumanGrup yeniDokumanGrup = new ABSDokumanGrup();
                    yeniDokumanGrup.FKABSDokumanID = DokumanID;
                    yeniDokumanGrup.FKOGRDersGrupID = fkDersGrupID;
                    yeniDokumanGrup.SGTarih = DateTime.Now;
                    yeniDokumanGrup.SGKullanici = kullaniciAdi;
                    yeniDokumanGrup.SGIP = IP;
                    db.ABSDokumanGrup.Add(yeniDokumanGrup);
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool GrupSil(int DokumanID, int fkDersGrupID, string kullaniciAdi, string IP)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var dokumanGrup = db.ABSDokumanGrup.FirstOrDefault(x => x.FKABSDokumanID == DokumanID && x.FKOGRDersGrupID == fkDersGrupID && x.Silindi == false);
                    dokumanGrup.Silindi = true;
                    dokumanGrup.SGKullanici = kullaniciAdi;
                    dokumanGrup.SGIP = IP;
                    dokumanGrup.SGTarih = DateTime.Now;
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static List<DokumanVM> GetirEBSDokumanListesi(int FKDersPlanAnaID, int yil)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                List<int> dokumanGrupList = db.ABSDokumanGrup.Where(x => x.ABSDokuman.FKDersPlanAnaID == FKDersPlanAnaID && x.ABSDokuman.EBSGosterilsin == true && x.ABSDokuman.Yil == yil && x.ABSDokuman.Silindi == false).Select(x=> x.FKABSDokumanID).Distinct().ToList();

                return db.ABSDokuman.Where(x=> dokumanGrupList.Contains(x.ID)).Select(x => new DokumanVM
                {
                    ID = x.ID,
                    DosyaAdi = x.DosyaAdi,
                    DosyaKey = x.DosyaKey,
                    Aciklama = x.Aciklama,
                    DosyaTuru = x.DosyaTuru,
                    EnumIcerikTipi = x.EnumIcerikTipi,
                    Yil = x.Yil,
                    EnumDonem = x.EnumDonem,
                    Hafta = x.Hafta.Value,
                    DigerHocalarGorebilir = x.DigerHocalarGorebilir,
                    EBSGosterilsin = x.EBSGosterilsin,
                    FKDersPlanAnaID = x.FKDersPlanAnaID,
                    FKDersGrupIDList = db.ABSDokumanGrup.Where(y => y.FKABSDokumanID == x.ID && y.Silindi == false).Select(y => y.FKOGRDersGrupID).ToList(),
                    FKPersonelID = x.FKPersonelID,
                    EkleyenAdSoyadUnvan = x.PersonelBilgisi.UnvanAd + " " + x.PersonelBilgisi.Ad + " " + x.PersonelBilgisi.Soyad,
                    SGKullanici = x.SGKullanici,
                    SGIP = x.SGIP,
                    SGTarih = x.SGTarih,
                    Silindi = x.Silindi
                }).ToList();
            }
        }

        public static DokumanVM GetirDokuman(int dokumanID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var dokuman = db.ABSDokuman.FirstOrDefault(x => x.ID == dokumanID);
                DokumanVM d = new DokumanVM();

                d.ID = dokuman.ID;
                d.DosyaAdi = dokuman.DosyaAdi;
                d.DosyaKey = dokuman.DosyaKey;
                d.Aciklama = dokuman.Aciklama;
                d.DosyaTuru = dokuman.DosyaTuru;
                d.EnumIcerikTipi = dokuman.EnumIcerikTipi;
                d.Yil = dokuman.Yil;
                d.EnumDonem = dokuman.EnumDonem;
                d.Hafta = dokuman.Hafta.Value;
                d.DigerHocalarGorebilir = dokuman.DigerHocalarGorebilir;
                d.EBSGosterilsin = dokuman.EBSGosterilsin;
                d.FKDersPlanAnaID = dokuman.FKDersPlanAnaID;
                d.FKDersGrupIDList = db.ABSDokumanGrup.Where(y => y.FKABSDokumanID == dokumanID && y.Silindi == false).Select(y => y.FKOGRDersGrupID).ToList();
                d.FKPersonelID = dokuman.FKPersonelID;
                d.EkleyenAdSoyadUnvan = dokuman.PersonelBilgisi.UnvanAd + " " + dokuman.PersonelBilgisi.Ad + " " + dokuman.PersonelBilgisi.Soyad;
                d.SGKullanici = dokuman.SGKullanici;
                d.SGIP = dokuman.SGIP;
                d.SGTarih = dokuman.SGTarih;
                d.Silindi = dokuman.Silindi;
                d.TumGruplaraEkle = dokuman.TumGruplaraEkle.Value;
                d.DigerGruplaraEkle = dokuman.TumGruplarimaEkle.Value;

                return d;
            }
        }
    }
}
