﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Bolum.Core.ABS.DERS.DERSDETAY;

namespace Sabis.Bolum.Bll.SOURCE.ABS.DERS
{
    public class RANDEVUMAIN
    {
        public static List<RandevuVM> GetirRandevuListesi(int FKDersGrupID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return db.PayRandevu.Where(x => x.FKDersGrupID == FKDersGrupID).Select(x => new RandevuVM
                {
                    ID = x.ID,
                    FKDersGrupID = x.FKDersGrupID,
                    FKPayID = x.FKPayID,
                    FKRandevuID = x.FKRandevuID,
                    EnumCalismaTipi = x.ABSPaylar.EnumCalismaTip.Value
                }).ToList();
            }
        }
        public static bool EkleYeniRandevu(int FKPayID, int FKDersGrupID, int FKRandevuID)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    PayRandevu yeniRandevu = new PayRandevu();
                    yeniRandevu.FKDersGrupID = FKDersGrupID;
                    yeniRandevu.FKPayID = FKPayID;
                    yeniRandevu.FKRandevuID = FKRandevuID;
                    db.PayRandevu.Add(yeniRandevu);
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
