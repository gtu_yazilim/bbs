﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Bolum.Core.ABS.DERS.OSINAV;
using Sabis.Enum;
using Sabis.Bolum.Core.ABS.DERS.DERSDETAY;
using System.Net;
using System.Web;
using Sabis.Bolum.Core.ABS.DERS.SANALSINIF.Models;


namespace Sabis.Bolum.Bll.SOURCE.ABS.DERS
{
        public class OSINAV
        {
            public static bool SinavEkle(OSinav s)
            {
                try
                {
                    UYSv2Entities db = new UYSv2Entities();
                    db.OSinav.Add(s);
                    db.SaveChanges();
                    return true;
                }
                catch (Exception)
                {

                    return false;
                }
            }
            public static bool SinavDuzenle(OSinavVM sinav)
            {
                try
                {
                    UYSv2Entities db = new UYSv2Entities();
                    var model = db.OSinav.Where(s => s.ID == sinav.SinavID).FirstOrDefault();
                    model.Aciklama = sinav.Aciklama;
                    model.Baslangic = sinav.Baslangic.Value;
                    model.Bitis = sinav.Bitis.Value;
                    model.Sure = sinav.Sure.Value;
                    model.SoruSayisi = sinav.SoruSayisi.Value;
                    model.Yayinlandi = 1;
                    model.Guncelleyen = sinav.Guncelleyen;
                    model.GuncelleyenID = sinav.GuncelleyenID;
                    model.GuncellemeTarihi = DateTime.Now;
                    db.SaveChanges();
                    return true;
                }
                catch (Exception)
                {

                    return false;
                }
            }

            public static bool SinavYayinla(int sinavid)
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var sinav = db.OSinav.Find(sinavid);
                    if (sinav == null)
                    {
                        return false;
                    }
                    else
                    {
                        sinav.Yayinlandi = 1;
                        db.SaveChanges();
                    }
                    return true;
                }
            }
            public static bool SinavAskiyaAl(int sinavid)
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var sinav = db.OSinav.Find(sinavid);
                    if (sinav == null)
                    {
                        return false;
                    }
                    else
                    {
                        sinav.Yayinlandi = 2;
                        db.SaveChanges();
                    }
                    return true;
                }
            }
            public static bool SinavYenidenHesapla(int sinavid)
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    try
                    {
                        db.OSinavYenidenHesapla(sinavid);
                        return true;
                    }
                    catch (Exception)
                    {

                        return false;
                    }

                }
            }
            public static List<SinavOturumDto> SinavOturumlari(int grupid, int sinavid)
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                //var sorgu = (from sg in db.OSinavGrup
                //             join s in db.OSinav on sg.SinavID equals s.ID
                //             join se in db.OSinavOgrenci on s.SinavID equals se.SinavID
                //             join so in db.OSinavOturum on se.OturumId equals so.OturumID
                //             join og in db.OGROgrenciYazilma on se.OgrenciID equals og.FKOgrenciID
                //             where
                //                 sg.FKDersGrupID == grupid &&
                //                 s.SinavID == sinavid
                //             select new SinavOturumDto
                //             {
                //                 SinavID = s.SinavID,
                //                 GrupID = sg.FKDersGrupID,
                //                 EID = se.EID,
                //                 OturumID = so.OturumID,
                //                 OgrenciID = se.OgrenciID,
                //                 OgrenciAd = og.OGRKimlik.Kisi.Ad,
                //                 OgrenciSoyad = og.OGRKimlik.Kisi.Soyad,
                //                 OgrenciNumara = og.OGRKimlik.Numara,
                //                 OgrenciKullaniciAdi = og.OGRKimlik.KullaniciAdi,
                //                 FotografAdres = og.OGRKimlik.Kisi.Foto,
                //                 Notu = so.Notu
                //             }).Distinct();
                //var sonuc = sorgu.ToList();
                //return sonuc;
                return null;
                }
            }
            public static bool OgrenciCevapTemizle(int sinavid, int oturumid, int ogrenciid)
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    //var sorgu = db.OSinavCevap.Where(c => c.SinavID == sinavid && c.OturumID == oturumid && c.OgrenciID == ogrenciid).ToList();
                    //db.OSinavCevap.RemoveRange(sorgu);
                    //db.SaveChanges();
                    return true;
                }
            }
            public static bool OgrenciOturumTemizle(int sinavid, int oturumid, int ogrenciid)
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    //var sorgu = db.OSinavOturum.Where(c => c.SinavID == sinavid && c.OturumID == oturumid && c.OgrenciID == ogrenciid).FirstOrDefault();
                    //db.OSinavOturum.Remove(sorgu);
                    //db.SaveChanges();
                    return true;
                }
            }
            public static bool OgrenciEnrollmentTemizle(int sinavid, int oturumid, int ogrenciid)
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    //var sorgu = db.OSinavOgrenci.Where(c => c.SinavID == sinavid && c.OturumId == oturumid && c.OgrenciID == ogrenciid).FirstOrDefault();
                    //db.OSinavOgrenci.Remove(sorgu);
                    //db.SaveChanges();
                    return true;
                }
            }
            public static bool SoruEkle(OSinavSoru soru)
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    try
                    {
                        db.OSinavSoru.Add(soru);
                        db.SaveChanges();
                        return true;
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                    return false;
                }
            }
        public static bool OnlineSoruEkle(OSinavSoruEkle model, string KullaniciAdi, int KullaniciID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                try
                {
                    var s = new OSinavSoru();
                    s.SinavID = model.SinavID;
                    s.SoruNo = model.PayOlcmeSoruNo.Value;
                    s.Baslik = model.Baslik;
                    s.Cevap1 = model.Cevap1;
                    s.Cevap2 = model.Cevap2;
                    s.Cevap3 = model.Cevap3;
                    s.Cevap4 = model.Cevap4;
                    s.Cevap5 = model.Cevap5;
                    s.DogruCevap = model.DogruCevap;
                    s.Silindi = false;
                    s.Iptal = false;
                    s.Ekleyen = KullaniciAdi;
                    s.EkleyenID = KullaniciID;
                    s.EklemeTarihi = DateTime.Now;
                    s.Guncelleyen = KullaniciAdi;
                    s.GuncelleyenID = KullaniciID;
                    s.GuncellemeTarihi = DateTime.Now;

                    db.OSinavSoru.Add(s);
                    db.SaveChanges();

                    var PayOlcme = db.ABSPaylarOlcme.Where(p => p.Silindi == false && p.ID == model.PaylarOlcmeID).FirstOrDefault();
                    PayOlcme.FKOSinavSoruID = s.SoruID;
                    db.SaveChanges();
                    return true;
                }
                catch (Exception)
                {

                    throw;
                }
                return false;
            }
        }
        public static bool SoruDuzenle(OSinavSoruDuzenle model, int guncelleyenid, string guncelleyen)
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var soru = db.OSinavSoru.FirstOrDefault(s => s.SoruID == model.SoruID);
                    if (soru != null)
                    {
                        soru.Baslik = model.Baslik;
                        soru.Cevap1 = model.Cevap1;
                        soru.Cevap2 = model.Cevap2;
                        soru.Cevap3 = model.Cevap3;
                        soru.Cevap4 = model.Cevap4;
                        soru.Cevap5 = model.Cevap5;
                        soru.DogruCevap = model.DogruCevap;
                        soru.Guncelleyen = guncelleyen;
                        soru.GuncelleyenID = guncelleyenid;
                        soru.GuncellemeTarihi = DateTime.Now;
                        soru.Silindi = false;
                        soru.Iptal = false;

                        db.SaveChanges();
                        return true;
                    }
                }
                return false;

            }
            public static void SinavGrupEkle(OSinavVM vm, int? sinavID)
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var dersID = db.OGRDersGrup.Where(g => g.ID == vm.GrupID).FirstOrDefault().FKDersPlanID;
                    var anadersID = db.OGRDersPlan.Where(d => d.ID == dersID).FirstOrDefault().FKDersPlanAnaID;

                var gruplar = vm.hocagrups.Where(x => x.Selected && !x.SelectedOld).Select(x =>
                              new OSinavGrup()
                              {
                                  FKSinavID = sinavID.Value,
                                  FKDersPlanAnaID = anadersID.Value,
                                  FKDersPlanID = dersID.Value,
                                  FKDersGrupID = x.FKDersGrupID.Value,
                                  PayID = vm.PayID
                                  //,
                                  //Koordinator = x.KoordinatorEkledi
                              }
                               ).ToList();
                if (gruplar.Count() > 0)
                    {
                        db.OSinavGrup.AddRange(gruplar);
                        db.SaveChanges();
                    }
                }
            }
            public static List<OSinavListe> SinavPayBilgi(int hocaid, string kullaniciadi, string ipadres)
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var grupID = db.OGRDersGrupHoca.Where(h => h.ID == hocaid).FirstOrDefault().FKDersGrupID;
                    var paylar = Sabis.Bolum.Bll.SOURCE.ABS.DERS.PAYMAIN.GetirPayBilgileriv2(hocaid, kullaniciadi, ipadres).Where(p => p.EnumCalismaTipi == (int)Sabis.Enum.EnumCalismaTip.KisaSinav || p.EnumCalismaTipi == (int)Sabis.Enum.EnumCalismaTip.AraSinav).ToList();
                    var sorgu = (from p in paylar
                                 join g in db.OSinavGrup on new { PAYID = p.PayID.Value, GRUPID = grupID.Value } equals new { PAYID = g.PayID, GRUPID = g.FKDersGrupID } into gl
                                 from ge in gl.Where(gr => gr.FKDersGrupID == grupID).DefaultIfEmpty()
                                 select new OSinavListe
                                 {
                                     paybilgi = p,
                                     SinavID = ge == null ? 0 : ge.FKSinavID,
                                     FKDersPlanAnaID = ge == null ? 0 : ge.FKDersPlanAnaID,
                                     FKDersPlanID = ge == null ? 0 : ge.FKDersPlanID,
                                     FKDersGrupID = ge == null ? 0 : ge.FKDersGrupID,
                                     PayID = ge == null ? 0 : ge.PayID
                                 }).ToList();
                    return sorgu;
                }
            }
            public static string PayAdiGetir(int? payID)
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var pay = db.ABSPaylar.Where(p => !p.Silindi && p.ID == payID).FirstOrDefault();
                    return pay.Sira + ". " + Sabis.Enum.Utils.GetEnumDescription((Sabis.Enum.EnumCalismaTip)pay.EnumCalismaTip);
                }
            }
            public static List<OSinavHocaGruplar> HocayaAitSiniflar(int GroupID, int payid, int yil, int donem)
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var cid = db.OGRDersGrup.Where(g => g.ID == GroupID).FirstOrDefault().FKDersPlanID;
                    var mcid = db.OGRDersPlan.Where(c => c.ID == cid).FirstOrDefault().FKDersPlanAnaID;
                    var personelid = db.OGRDersGrupHoca.Where(h => h.FKDersGrupID == GroupID).FirstOrDefault().FKPersonelID;
                    var pid = payid;

                    var sorgu = (from grup in db.OGRDersGrup
                                 join anaders in db.OGRDersPlanAna on grup.FKDersPlanAnaID equals anaders.ID
                                 join gruphoca in db.OGRDersGrupHoca on grup.ID equals gruphoca.FKDersGrupID
                                 join sg in db.OSinavGrup on grup.ID equals sg.FKDersGrupID into tbl1
                                 from sg in tbl1.Where(f => f.PayID == pid).DefaultIfEmpty()
                                 where
                                     anaders.ID == mcid &&
                                     gruphoca.FKPersonelID == personelid &&
                                     grup.OgretimYili == yil &&
                                     grup.EnumDonem == donem &&
                                     grup.Acik == true &&
                                     grup.Silindi == false
                                 select new OSinavHocaGruplar
                                 {
                                     FKDersGrupID = gruphoca.FKDersGrupID,
                                     FKPersonelID = gruphoca.FKPersonelID,
                                     GrupAd = grup.GrupAd,
                                     OgretimTuru = grup.EnumOgretimTur,
                                     Selected = sg.FKDersGrupID == null ? false : true,
                                     SelectedOld = sg.FKDersGrupID == null ? false : true
                                 }).ToList();
                    return sorgu;
                }
            }
            public static List<OSinavHocaGruplar2> HocayaAitSiniflar2( int GroupID, int payid, int yil, int donem)
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {

                

                    var cid = db.OGRDersGrup.Where(g => g.ID == GroupID).FirstOrDefault().FKDersPlanID;
                    var mcid = db.OGRDersPlan.Where(c => c.ID == cid).FirstOrDefault().FKDersPlanAnaID;
                    var personelid = db.OGRDersGrupHoca.Where(h => h.FKDersGrupID == GroupID).FirstOrDefault().FKPersonelID;
                    var pid = payid;

                    var sorgu = (from grup in db.OGRDersGrup
                                 join anaders in db.OGRDersPlanAna on grup.FKDersPlanAnaID equals anaders.ID
                                 join gruphoca in db.OGRDersGrupHoca on grup.ID equals gruphoca.FKDersGrupID
                                 join sg in db.OSinavGrup on grup.ID equals sg.FKDersGrupID into tbl1
                                 from sg in tbl1.Where(f => f.PayID == pid).DefaultIfEmpty()
                                 where
                                     anaders.ID == mcid &&
                                     gruphoca.FKPersonelID == personelid &&
                                     grup.OgretimYili == yil &&
                                     grup.EnumDonem == donem &&
                                     grup.Acik == true &&
                                     grup.Silindi == false
                                 select new OSinavHocaGruplar2
                                 {
                                     SinavID = sg == null ? 0 : sg.FKSinavID,
                                     FKDersGrupID = gruphoca.FKDersGrupID,
                                     FKPersonelID = gruphoca.FKPersonelID,
                                     GrupAd = grup.GrupAd,
                                     OgretimTuru = grup.EnumOgretimTur,
                                     Selected = sg.FKDersGrupID == null ? false : true,
                                     SelectedOld = sg.FKDersGrupID == null ? false : true
                                 }).ToList();
                    return sorgu;
                }
            }        
            public static int SinavGrupPayID(int sinavID)
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    return db.OSinavGrup.Where(sg => sg.FKSinavID == sinavID).FirstOrDefault().PayID;
                }
            }
            public static OSinav SinavGetir(int sinavID)
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    return db.OSinav.Where(os => os.ID == sinavID).FirstOrDefault();
                }
            }
            public static OSinavSoru SoruGetir(int soruID)
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    return db.OSinavSoru.Include("OSinav").Where(s => s.SoruID == soruID).FirstOrDefault();
                }
            }
            public static string SoruBaslikGetir(int sinavID, int soruNo)
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    return db.OSinav.Where(s => s.ID == sinavID).FirstOrDefault().Ad + " - " + soruNo + ". Soru";
                }
            }
            public static List<OSinavSoruItem> SinavSorulari(int sinavID, int hocaID)
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var sorgu = (from sr in db.OSinavSoru
                                 where sr.SinavID == sinavID && sr.Silindi == false
                                 select new OSinavSoruItem()
                                 {
                                     SinavID = sr.SinavID,
                                     HocaID = hocaID,
                                     SoruID = sr.SoruID,
                                     SoruNo = sr.SoruNo,
                                     Baslik = sr.Baslik,
                                     Cevap1 = sr.Cevap1,
                                     Cevap2 = sr.Cevap2,
                                     Cevap3 = sr.Cevap3,
                                     Cevap4 = sr.Cevap4,
                                     Cevap5 = sr.Cevap5,
                                     DogruCevap = sr.DogruCevap,
                                     Silindi = sr.Silindi,
                                     Iptal = sr.Iptal
                                 });
                    return sorgu.ToList();
                }
            }
            public static int SoruNoBul(int sinavid)
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var query = from s in db.OSinavSoru
                                where
                                s.SinavID == sinavid &&
                                !s.Silindi
                                select s.SoruNo;
                    if (query.Count() > 0)
                    {
                        return query.Max() + 1;
                    }
                    return 1;
                }
            }

        #region Ortak Değerlendirme
        public static List<HocaGrup> HocaninGruplari(int hocaid, int payid)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var personelid = db.OGRDersGrupHoca.FirstOrDefault(h => h.ID == hocaid).FKPersonelID;

                var sorgu1 = (from ortakdeg in db.ABSOrtakDegerlendirme
                             join ortakdeg_grup in db.ABSOrtakDegerlendirmeGrup on ortakdeg.ID equals ortakdeg_grup.FKOrtakDegerlendirmeID
                             join grup in db.OGRDersGrup on ortakdeg_grup.FKDersGrupID equals grup.ID
                             where
                             ortakdeg.FKPersonelID == personelid &&
                             ortakdeg.Yil == 2016 &&
                             ortakdeg.Donem == 2 &&
                             ortakdeg.Silindi == false
                             select new
                             {
                                 FKDersGrupID = ortakdeg_grup.FKDersGrupID,
                                 FKPersonelID = ortakdeg.FKPersonelID,
                                 GrupAd = grup.GrupAd,
                                 OgretimTuru = grup.EnumOgretimTur
                             }).ToList();
                if (sorgu1.Count() > 0)
                {
                    var sorgu2 = (from q in sorgu1
                                  join sg in db.OSinavGrup on q.FKDersGrupID equals sg.FKDersGrupID into tbl1
                                  from sg in tbl1.Where(p => p.PayID == payid).DefaultIfEmpty()
                                  select new HocaGrup
                                  {
                                      FKDersGrupID = q.FKDersGrupID,
                                      FKPersonelID = q.FKPersonelID,
                                      GrupAd = q.GrupAd,
                                      OgretimTuru = q.OgretimTuru,
                                      Selected = sg == null ? false : true,
                                      SelectedOld = sg == null ? false : true,
                                      KoordinatorEkledi = true,
                                  }).ToList();
                    return sorgu2;
                }

























                //var dergerlendirmegrubu_yetki_kontrol = DegerlendirmeGrup(hocaid);
                //if (dergerlendirmegrubu_yetki_kontrol.Where(g => g.FKOrtakDegerlendirmeGrupID != 0 && g.FKPersonelID == personelid).Count() > 0)
                //{
                //    // genel değerlendirici
                //    var gruplar = dergerlendirmegrubu_yetki_kontrol;
                //    var sorgu = (from g in gruplar
                //                 join sg in db.OSinavGrup on g.FKOrtakDegerlendirmeGrupID equals sg.FKDersGrupID into tbl1
                //                 from sg in tbl1.Where(p => p.PayID == payid).DefaultIfEmpty()
                //                 select new HocaGrup
                //                 {
                //                     FKDersGrupID = g == null ? 0 : g.FKOrtakDegerlendirmeGrupID,
                //                     GrupAd = g.GrupAd,
                //                     OgretimTuru = g.OgretimTur,
                //                     Selected = sg == null ? false : true,
                //                     SelectedOld = sg == null ? false : true,
                //                     KoordinatorEkledi = true

                //                 }).ToList();
                //    return sorgu;
                //}
                //else
                //{
                //    // sadece kendi sınıflarını görecek
                //    var bilgi = (from hocagrup in db.OGRDersGrupHoca
                //                 join grup in db.OGRDersGrup on hocagrup.FKDersGrupID equals grup.ID
                //                 join dpa in db.OGRDersPlanAna on grup.FKDersPlanAnaID equals dpa.ID
                //                 where
                //                 hocagrup.ID == hocaid
                //                 select new
                //                 {
                //                     HocaID = hocagrup.ID,
                //                     GrupID = grup.ID,
                //                     AnadersID = dpa.ID,
                //                     PersonelID = hocagrup.FKPersonelID
                //                 }).FirstOrDefault(); ;

                //    var sorgu = (from grup in db.OGRDersGrup
                //                 join anaders in db.OGRDersPlanAna on grup.FKDersPlanAnaID equals anaders.ID
                //                 join gruphoca in db.OGRDersGrupHoca on grup.ID equals gruphoca.FKDersGrupID
                //                 join sg in db.OSinavGrup on grup.ID equals sg.FKDersGrupID into tbl1
                //                 from sg in tbl1.Where(f => f.PayID == payid).DefaultIfEmpty()
                //                 where
                //                     anaders.ID == bilgi.AnadersID &&
                //                     gruphoca.FKPersonelID == bilgi.PersonelID &&
                //                     grup.OgretimYili == 2016 &&
                //                     grup.EnumDonem == 2 &&
                //                     grup.Acik == true &&
                //                     grup.Silindi == false
                //                 select new HocaGrup
                //                 {
                //                     SinavID = sg == null ? 0 : sg.SinavID,
                //                     FKDersGrupID = gruphoca.FKDersGrupID,
                //                     FKPersonelID = gruphoca.FKPersonelID,
                //                     GrupAd = grup.GrupAd,
                //                     OgretimTuru = grup.EnumOgretimTur,
                //                     Selected = sg.FKDersGrupID == null ? false : true,
                //                     SelectedOld = sg.FKDersGrupID == null ? false : true,
                //                     KoordinatorEkledi = false
                //                 }).ToList();
                //    return sorgu;
                //}
                return null;
            }
        }
        public static List<DegerlendirmeGruplariDto> DegerlendirmeGrup(int FKDersGrupHocaID)
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                var sorgu = (from hocagrup in db.OGRDersGrupHoca
                            join grup in db.OGRDersGrup on hocagrup.FKDersGrupID equals grup.ID
                            join ortakdeg in db.ABSOrtakDegerlendirme on grup.FKDersPlanAnaID equals ortakdeg.FKDersPlanAnaID
                            join ortakdeg_grup in db.ABSOrtakDegerlendirmeGrup on new { ORTAKDEGERLENDIRMEID = ortakdeg.ID, KULLANICI = ortakdeg.Kullanici } equals new { ORTAKDEGERLENDIRMEID = ortakdeg_grup.FKOrtakDegerlendirmeID, KULLANICI = ortakdeg_grup.Kullanici } into tbl1
                            from ortakdeg_grup in tbl1.DefaultIfEmpty()
                            join grup2 in db.OGRDersGrup on ortakdeg_grup.FKDersGrupID equals grup2.ID
                            where
                            hocagrup.ID == FKDersGrupHocaID &&
                            //ortakdeg.FKPersonelID == 727 &&
                            grup.OgretimYili == 2016 &&
                            grup.EnumDonem == 2 &&
                            grup.Acik == true &&
                            grup.Silindi == false
                            select new DegerlendirmeGruplariDto
                            {
                                 FKPersonelID = ortakdeg.FKPersonelID,
                                 FKHocaGrupID = hocagrup.ID,
                                 FKDersPlanAnaID = grup.FKDersPlanAnaID,
                                 OrtakDegerlendirmeID = ortakdeg == null ? 0 : ortakdeg.ID,
                                 FKOrtakDegerlendirmeGrupID = ortakdeg_grup == null ? 0 : ortakdeg_grup.FKDersGrupID,
                                 FKGrupID = grup.ID,
                                 GrupAd = grup2.GrupAd,
                                 OgretimTur = grup2.EnumOgretimTur
                            });

                return sorgu.ToList();
                }
            }
            #endregion
    }
}
