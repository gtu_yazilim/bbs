﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Bolum.Core.ABS.DERS.HOME;
using Sabis.Bolum.Core.ABS.DERS.DERSDETAY;
using Sabis.Enum;


namespace Sabis.Bolum.Bll.SOURCE.ABS.DERS
{
    public class DERSMAIN : IDers
    {
        public static List<HocaDersListesiViewModelv2> ListelePersonelDersleri(int personelID, int Yil, int Donem)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var hocaDersSorgu = (from hoca in db.OGRDersGrupHoca
                                     join grup in db.OGRDersGrup on hoca.FKDersGrupID equals grup.ID
                                     join plan in db.OGRDersPlan on grup.FKDersPlanID equals plan.ID
                                     join ana in db.OGRDersPlanAna on plan.FKDersPlanAnaID equals ana.ID
                                     join prg in db.Birimler on plan.FKBolumPrBirimID equals prg.ID into p
                                     from program in p.DefaultIfEmpty()
                                     join blm in db.Birimler on program.UstBirimID equals blm.ID into b
                                     from bolum in b.DefaultIfEmpty()
                                     join fklt in db.Birimler on plan.FKFakulteBirimID equals fklt.ID into f
                                     from fakulte in f.DefaultIfEmpty()
                                     where hoca.FKPersonelID == personelID && grup.OgretimYili == Yil && grup.EnumDonem == Donem && grup.Acik == true && grup.Silindi == false
                                     select new HocaDersListesiViewModelv2
                                     {
                                         DersGrupHocaID = hoca.ID,
                                         DersKod = plan.BolKodAd + plan.DersKod,
                                         DersAd = grup.EnumDersGrupDil.Value == (int)Sabis.Enum.EnumDil.INGILIZCE ? grup.OGRDersPlanAna.YabDersAd : grup.OGRDersPlanAna.DersAd,
                                         EnumOgretimTur = plan.EnumOgretimTur,
                                         Donem = Donem,
                                         BolumKod = plan.BolKodAd,
                                         OgrenciSayisi = grup.OGROgrenciYazilma.Where(x => x.Iptal == false && x.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL && x.Yil == Yil && x.EnumDonem == Donem).Count(),
                                         OgrenciSayisiButunleme = grup.OGROgrenciYazilma.Where(x => x.Iptal == false && x.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL && x.Yil == Yil && x.EnumDonem == Donem && (x.ButunlemeMi.HasValue && x.ButunlemeMi.Value)).Count(),
                                         GrupAd = grup.GrupAd,
                                         DersGrupID = grup.ID,
                                         Yil = Yil,
                                         Kredi = ana.Kredi.HasValue ? ana.Kredi.Value : 0,
                                         EnumDersPlanZorunlu = plan.EnumDersPlanZorunlu,
                                         FakulteID = plan.FKFakulteBirimID,
                                         FakulteAd = fakulte.BirimAdi,
                                         BolumAd = bolum.BirimAdi,
                                         ProgramAd = program.BirimAdi,
                                         ProgramID = plan.FKBolumPrBirimID,
                                         DSaat = grup.OGRDersPlanAna.DSaat,
                                         USaat = grup.OGRDersPlanAna.USaat,
                                         YariYil = plan.Yariyil,
                                         ButunlemeMi = grup.OGROgrenciYazilma.Any(x => x.Iptal == false && x.Yil == Yil && x.EnumDonem == Donem && (x.ButunlemeMi.HasValue && x.ButunlemeMi.Value == true)),
                                         EnumSeviye = plan.EnumOgretimSeviye,
                                     }).OrderBy(d => d.EnumOgretimTur).ThenBy(d => d.DersKod).ThenBy(d => d.EnumSeviye).Where(d => d.OgrenciSayisi > 0).ToList();

                return hocaDersSorgu;
            }
        }
        public List<UstDersViewModel> ListelePersonelDersleriv2(int personelID, int Yil, int Donem)
        {
            List<UstDersViewModel> hiyerarsikList = DersListesiGetir(personelID, Yil, Donem);
            return hiyerarsikList;
        }
        public List<UstDersViewModel> ListeleAcilanPersonelDersleri(int personelID, int Yil, int Donem)
        {
            //List<UstDersViewModel> hiyerarsikList = AcilanDersListesiGetir(personelID, Yil, Donem);
            List<UstDersViewModel> hiyerarsikList = BolumeAcilanDersListesiGetir(personelID, Yil, Donem);
            return hiyerarsikList;
        }
        public List<AcilanDerslerExcelVM> AcilanDerslerExcelFormat (List<UstDersViewModel> ustDersViewModel)
        {
            List<AcilanDerslerExcelVM> acilanDerslerList = AcilanDersleriExcelFormatindaGetir(ustDersViewModel);
            return acilanDerslerList;
        }
        public static int GetirToplamOgrenciSayi(int dersPlanAnaID, int yil, int donem, bool butunlemeMi, int grupID = 0)
        {
            int result = 0;
            UYSv2Entities db = new UYSv2Entities();
            var sorgu = db.OGROgrenciYazilma.Where(x => x.FKDersGrupID.HasValue && x.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL && ((x.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.EnumDersSureTip == (int)EnumDersSureTip.DONEMLIK && x.OGRDersGrup.OgretimYili == yil && x.OGRDersGrup.EnumDonem == donem) || (x.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.EnumDersSureTip == (int)EnumDersSureTip.YILLIK && x.OGRDersGrup.OgretimYili == yil)) && x.Iptal == false);

            if (butunlemeMi)
            {
                sorgu = sorgu.Where(x => x.ButunlemeMi == true);
            }

            if (grupID > 0)
            {
                sorgu = sorgu.Where(x => x.FKDersGrupID.Value == grupID);
            }
            else
            {
                var altGrupList = GetirAltDersGrupIDList(ref db, dersPlanAnaID, yil, donem);

                if (altGrupList.Any())
                {
                    sorgu = sorgu.Where(x => altGrupList.Contains(x.FKDersGrupID.Value));
                }
            }
            result = sorgu.Select(x => x.FKOgrenciID.Value).Distinct().Count();
            db.Dispose();
            db = null;
            return result;
        }
        private static List<UstDersViewModel> DersListesiGetir(int personelID, int Yil, int Donem)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var kadi = db.PersonelBilgisi.Find(personelID).KullaniciAdi;
                var sorgu = (from hoca in db.OGRDersGrupHoca
                             join grup in db.OGRDersGrup on hoca.FKDersGrupID equals grup.ID
                             where grup.Acik == true && grup.Silindi == false && (hoca.FKPersonelID == personelID || hoca.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.OGRDersPlanAna4.Koordinator == kadi) &&
                                   ((grup.OGRDersPlan.OGRDersPlanAna.EnumDersSureTip == (int)Sabis.Enum.EnumDersSureTip.DONEMLIK && grup.OgretimYili == Yil && grup.EnumDonem == Donem) || (grup.OGRDersPlan.OGRDersPlanAna.EnumDersSureTip == (int)Sabis.Enum.EnumDersSureTip.YILLIK && grup.OgretimYili == Yil))
                             orderby grup.GrupAd
                             select new HocaDersListesiViewModelv2
                             {
                                 DPAnaID = grup.OGRDersPlan.FKDersPlanAnaID.Value,
                                 DPAnaUstID = grup.OGRDersPlan.OGRDersPlanAna.FKDersPlanAnaUstID.Value,
                                 DersGrupHocaID = hoca.ID,
                                 DersKod = grup.OGRDersPlan.BolKodAd + grup.OGRDersPlan.DersKod,
                                 DersAd = grup.EnumDersGrupDil.Value == (int)Sabis.Enum.EnumDil.INGILIZCE ? grup.OGRDersPlan.OGRDersPlanAna.YabDersAd : grup.OGRDersPlan.OGRDersPlanAna.DersAd,
                                 //EnumOgretimTur = grup.OGRDersPlan.EnumOgretimTur ?? grup.EnumOgretimTur, 
                                 EnumOgretimTur = grup.EnumOgretimTur,
                                 Donem = Donem,
                                 BolumKod = grup.OGRDersPlan.BolKodAd,
                                 OgrenciSayisi = grup.OGROgrenciYazilma.Where(x => x.Iptal == false && x.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL && x.OGROgrenciNot.EnumOgrenciBasariNot != (int)EnumOgrenciBasariNot.W).Count(),
                                 OgrenciSayisiButunleme = grup.OGROgrenciYazilma.Where(x => x.Iptal == false && (x.ButunlemeMi.HasValue && x.ButunlemeMi.Value) && x.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL && x.OGROgrenciNot.EnumOgrenciBasariNot != (int)EnumOgrenciBasariNot.W).Count(),
                                 GrupAd = grup.GrupAd == "?" ? "A" : grup.GrupAd,
                                 DersGrupID = grup.ID,
                                 Yil = Yil,
                                 Kredi = grup.OGRDersPlan.OGRDersPlanAna.Kredi.HasValue ? grup.OGRDersPlan.OGRDersPlanAna.Kredi.Value : 0,
                                 EnumDersPlanZorunlu = grup.OGRDersPlan.EnumDersPlanZorunlu,
                                 FakulteID = grup.OGRDersPlan.FKFakulteBirimID,
                                 ProgramID = grup.OGRDersPlan.FKBirimID,
                                 DSaat = grup.OGRDersPlanAna.DSaat,
                                 USaat = grup.OGRDersPlanAna.USaat,
                                 YariYil = grup.OGRDersPlan.Yariyil,
                                 EnumKokDersTipi = grup.OGRDersPlan.OGRDersPlanAna.EnumKokDersTipi,
                                 ButunlemeMi = grup.OGROgrenciYazilma.Any(x => x.Iptal == false && (x.ButunlemeMi.HasValue && x.ButunlemeMi.Value == true) && x.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL),
                                 EnumSeviye = grup.OGRDersPlan.EnumOgretimSeviye,
                                 Seviye = grup.OGRDersPlan.EnumOgretimSeviye == (int)EnumOgretimSeviye.LISANS ? "Lisans" : (grup.OGRDersPlan.EnumOgretimSeviye == (int)EnumOgretimSeviye.YUKSEK_LISANS ? "Yüksek Lisans" : "Doktora"),
                                 Hiyerarsik = grup.OGRDersPlan.OGRDersPlanAna.Hiyerarsik,
                                 KokDPAnaID = grup.OGRDersPlan.OGRDersPlanAna.FKHiyerarsikKokID,
                                 DersDegTempList = db.ABSDegerlendirme.Where(deg => deg.FKDersGrupID == grup.ID && deg.SilindiMi == false).OrderByDescending(x => x.ID).Select(deg => new DersDegTemp { ID = deg.ID, Butunleme = deg.ButunlemeMi, EnumSonHal = deg.EnumSonHal }).ToList()
                             }).OrderBy(d => d.EnumOgretimTur).ThenBy(d => d.DersKod).ThenBy(d => d.EnumSeviye)
                                                 .Where(d => d.OgrenciSayisi > 0);
#if DEBUG
                var sql = sorgu.ToString();
#endif

                var hocaDersSorgu = sorgu.ToList();

                foreach (var item in hocaDersSorgu)
                {
                    if (item.ProgramID.HasValue && Sabis.Client.BirimIslem.Instance.getirBagliBolum(item.ProgramID.Value) != null)
                    {
                        item.ProgramAd = Sabis.Client.BirimIslem.Instance.BirimDict[item.ProgramID.Value].BirimAdi;
                        item.BolumAd = Sabis.Client.BirimIslem.Instance.getirBagliBolum(item.ProgramID.Value).BirimAdi;
                    }
                    if (item.FakulteID.HasValue)
                    {
                        item.FakulteAd = Sabis.Client.BirimIslem.Instance.getirBagliFakulte(item.FakulteID.Value).BirimAdi;
                    }

                    var degerlendirme = item.DersDegTempList.FirstOrDefault(x => x.Butunleme == item.ButunlemeMi);
                    if (degerlendirme == null)
                    {
                        item.EnumSonHal = (int)EnumSonHal.HAYIR;
                    }
                    else if (degerlendirme.EnumSonHal == (int)EnumSonHal.EVET)
                    {
                        item.EnumSonHal = (int)EnumSonHal.EVET;
                    }
                    else
                    {
                        item.EnumSonHal = (int)EnumSonHal.HAYIR;
                    }
                }

                Dictionary<int, UstDersViewModel> anaDict = new Dictionary<int, UstDersViewModel>();
                List<int> kokDersIDList = new List<int>();
                foreach (var item in hocaDersSorgu)
                {
                    GetirDersPlanAnaDict(item.DPAnaID, item.KokDPAnaID, ref anaDict, ref kokDersIDList);
                }

                anaDict = DersAgacTemizle(ref anaDict, hocaDersSorgu.Select(x => x.DPAnaID).ToList());

                List<UstDersViewModel> hiyerarsikList = new List<UstDersViewModel>();
                foreach (var item in kokDersIDList)
                {
                    UstDersViewModel kokDers = null;
                    anaDict.TryGetValue(item, out kokDers);
                    if (kokDers != null)
                    {
                        if (kokDers.Hiyerarsik)
                        {
                            kokDers.AltDersList = GetirDersHiyerarsik(ref anaDict, kokDers.DersPlanAnaID);
                        }
                        hiyerarsikList.Add(kokDers);
                    }
                }

                hiyerarsikList = HiyerarsikGrupDoldur(hiyerarsikList, ref hocaDersSorgu);
                return hiyerarsikList;
            }

        }
        private static List<AcilanDerslerExcelVM> AcilanDersleriExcelFormatindaGetir(List<UstDersViewModel> ustDersViewModel)
        {
            List<AcilanDerslerExcelVM> acilanDersler = new List<AcilanDerslerExcelVM>();
            foreach (var ders in ustDersViewModel)
            {
                if (ders.DersGrupList != null)
                {
                    var subList = ders.DersGrupList.Select(a => new AcilanDerslerExcelVM()
                    {
                        AKTSKredi = a.ECTSKredi,
                        DersAdi = a.DersAd,
                        DersKodu = a.DersKod,
                        DUSaat = a.DSaat + " / " + a.USaat,
                        Etiketler = String.Join(",", a.DersEtiketleri.ToArray()),
                        GrupAdi = a.GrupAd,
                        GTUKredi = a.Kredi,
                        HocaAdi = String.Join(",", a.HocaListesi.ToArray()),
                        Mufredatlar = String.Join(",", a.Mufredatlar.ToArray()),
                        OgrenciSayisi = a.OgrenciSayisi,
                        SecimTipi = a.DersSecimTipi,
                        Sira = a.Sira
                    }).ToList();
                    acilanDersler.AddRange(subList);
                }
            }

            return acilanDersler;
        }
        private static List<UstDersViewModel> BolumeAcilanDersListesiGetir(int personelID, int Yil, int Donem)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var idariGorevler = db.IdariGorev.Where(a => a.FKPersonelBilgisiID == personelID && a.FKBolumID!=null).ToList();

                if (idariGorevler.Count>0)
                {
                    List<int> birimler = new List<int>();
                    foreach(IdariGorev ig in idariGorevler)
                    {
                        var birim = Sabis.Client.BirimIslem.Instance.getirAltBirimlerID(ig.FKBolumID);
                        birimler.AddRange(birim);
                    }
                    //var birimler = Sabis.Client.BirimIslem.Instance.getirAltBirimlerID(idariGorev.FKBolumID);
                    var sorgu = (from grup in db.OGRDersGrup
                                 join dgh in db.OGRDersGrupHoca on grup.ID equals dgh.FKDersGrupID into temp
                                 from t in temp.DefaultIfEmpty()
                                 where grup.Acik == true && grup.Silindi == false &&
                                 birimler.Contains(grup.FKBirimID) &&
                                   ((grup.OGRDersPlan.OGRDersPlanAna.EnumDersSureTip == (int)Sabis.Enum.EnumDersSureTip.DONEMLIK && grup.OgretimYili == Yil && grup.EnumDonem == Donem) || (grup.OGRDersPlan.OGRDersPlanAna.EnumDersSureTip == (int)Sabis.Enum.EnumDersSureTip.YILLIK && grup.OgretimYili == Yil))
                                 orderby grup.GrupAd
                                 select new HocaDersListesiViewModelv2
                                 {
                                     DPAnaID = grup.OGRDersPlan.FKDersPlanAnaID.Value,
                                     DPAnaUstID = grup.OGRDersPlan.OGRDersPlanAna.FKDersPlanAnaUstID.Value,
                                     DersKod = grup.OGRDersPlan.BolKodAd + grup.OGRDersPlan.DersKod,
                                     GorunenDersAd = grup.EnumDersGrupDil.Value == (int)Sabis.Enum.EnumDil.INGILIZCE ? grup.OGRDersPlan.OGRDersPlanAna.YabDersAd : grup.OGRDersPlan.OGRDersPlanAna.DersAd,
                                     DersAd = grup.OGRDersPlan.OGRDersPlanAna.DersAd,
                                     DersYabAd = grup.OGRDersPlan.OGRDersPlanAna.YabDersAd,
                                     //EnumOgretimTur = grup.OGRDersPlan.EnumOgretimTur ?? grup.EnumOgretimTur, 
                                     EnumOgretimTur = grup.EnumOgretimTur,
                                     Donem = Donem,
                                     BolumKod = grup.OGRDersPlan.BolKodAd,
                                     OgrenciSayisi = grup.OGROgrenciYazilma.Where(x => x.Iptal == false && x.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL && x.OGROgrenciNot.EnumOgrenciBasariNot != (int)EnumOgrenciBasariNot.W).Count(),
                                     OgrenciSayisiButunleme = grup.OGROgrenciYazilma.Where(x => x.Iptal == false && (x.ButunlemeMi.HasValue && x.ButunlemeMi.Value) && x.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL && x.OGROgrenciNot.EnumOgrenciBasariNot != (int)EnumOgrenciBasariNot.W).Count(),
                                     GrupAd = grup.GrupAd == "?" ? "A" : grup.GrupAd,
                                     DersGrupID = grup.ID,
                                     Yil = Yil,
                                     Kredi = grup.OGRDersPlan.OGRDersPlanAna.Kredi.HasValue ? grup.OGRDersPlan.OGRDersPlanAna.Kredi.Value : 0,
                                     ECTSKredi = grup.OGRDersPlan.OGRDersPlanAna.ECTSKredi.HasValue ? grup.OGRDersPlan.OGRDersPlanAna.ECTSKredi.Value : 0,
                                     EnumDersPlanZorunlu = grup.OGRDersPlan.EnumDersPlanZorunlu,
                                     FakulteID = grup.OGRDersPlan.FKFakulteBirimID,
                                     ProgramID = grup.OGRDersPlan.FKBirimID,
                                     DSaat = grup.OGRDersPlanAna.DSaat,
                                     USaat = grup.OGRDersPlanAna.USaat,
                                     YariYil = grup.OGRDersPlan.Yariyil,
                                     EnumKokDersTipi = grup.OGRDersPlan.OGRDersPlanAna.EnumKokDersTipi,
                                     ButunlemeMi = grup.OGROgrenciYazilma.Any(x => x.Iptal == false && (x.ButunlemeMi.HasValue && x.ButunlemeMi.Value == true) && x.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL),
                                     EnumSeviye = grup.OGRDersPlan.EnumOgretimSeviye,
                                     Seviye = grup.OGRDersPlan.EnumOgretimSeviye == (int)EnumOgretimSeviye.LISANS ? "Lisans" : (grup.OGRDersPlan.EnumOgretimSeviye == (int)EnumOgretimSeviye.YUKSEK_LISANS ? "Yüksek Lisans" : "Doktora"),
                                     Hiyerarsik = grup.OGRDersPlan.OGRDersPlanAna.Hiyerarsik,
                                     KokDPAnaID = grup.OGRDersPlan.OGRDersPlanAna.FKHiyerarsikKokID,
                                     DersDegTempList = db.ABSDegerlendirme.Where(deg => deg.FKDersGrupID == grup.ID && deg.SilindiMi == false).OrderByDescending(x => x.ID).Select(deg => new DersDegTemp { ID = deg.ID, Butunleme = deg.ButunlemeMi, EnumSonHal = deg.EnumSonHal }).ToList(),
                                     DersEtiketleri = grup.OGRDersPlan.OGRDersPlanEtiket.Select(a => a.OGRTNMDersEtiket.Etiket).ToList(),
                                     DersSecimTipi = grup.OGRDersPlan.EnumDersSecimTipi == (int)EnumDersSecimTipi.ZORUNLU ? "Zorunlu" : "Seçmeli",
                                     Mufredatlar = db.OGRMufredatDersleri.Where(a => a.FKDersPlanAnaID == grup.OGRDersPlan.OGRDersPlanAna.ID && birimler.Contains(a.OGRMufredat.FKProgramID)).OrderBy(a => a.FKMufredatID).Distinct().Select(a => a.OGRMufredat.MufredatAdi).ToList(),
                                     HocaListesi = grup.OGRDersGrupHoca.Select(a => a.PersonelBilgisi.Ad + " " + a.PersonelBilgisi.Soyad).ToList()
                                 }).OrderBy(d => d.EnumSeviye).ThenBy(d => d.EnumOgretimTur).ThenBy(d => d.ProgramID).ThenBy(d => d.DersKod);


#if DEBUG
                    var sql = sorgu.ToString();
#endif

                    var hocaDersSorgu = sorgu.ToList();

                    foreach (var item in hocaDersSorgu)
                    {
                        if (item.ProgramID.HasValue && Sabis.Client.BirimIslem.Instance.getirBagliBolum(item.ProgramID.Value) != null)
                        {
                            item.ProgramAd = Sabis.Client.BirimIslem.Instance.BirimDict[item.ProgramID.Value].BirimAdi;
                            item.BolumAd = Sabis.Client.BirimIslem.Instance.getirBagliBolum(item.ProgramID.Value).BirimAdi;
                        }
                        if (item.FakulteID.HasValue)
                        {
                            item.FakulteAd = Sabis.Client.BirimIslem.Instance.getirBagliFakulte(item.FakulteID.Value).BirimAdi;
                        }

                        var degerlendirme = item.DersDegTempList.FirstOrDefault(x => x.Butunleme == item.ButunlemeMi);
                        if (degerlendirme == null)
                        {
                            item.EnumSonHal = (int)EnumSonHal.HAYIR;
                        }
                        else if (degerlendirme.EnumSonHal == (int)EnumSonHal.EVET)
                        {
                            item.EnumSonHal = (int)EnumSonHal.EVET;
                        }
                        else
                        {
                            item.EnumSonHal = (int)EnumSonHal.HAYIR;
                        }
                    }

                    Dictionary<int, UstDersViewModel> anaDict = new Dictionary<int, UstDersViewModel>();
                    List<int> kokDersIDList = new List<int>();
                    foreach (var item in hocaDersSorgu)
                    {
                        GetirDersPlanAnaDict(item.DPAnaID, item.KokDPAnaID, ref anaDict, ref kokDersIDList);
                    }

                    anaDict = DersAgacTemizle(ref anaDict, hocaDersSorgu.Select(x => x.DPAnaID).ToList());

                    List<UstDersViewModel> hiyerarsikList = new List<UstDersViewModel>();
                    foreach (var item in kokDersIDList)
                    {
                        UstDersViewModel kokDers = null;
                        anaDict.TryGetValue(item, out kokDers);
                        if (kokDers != null)
                        {
                            if (kokDers.Hiyerarsik)
                            {
                                kokDers.AltDersList = GetirDersHiyerarsik(ref anaDict, kokDers.DersPlanAnaID);
                            }
                            hiyerarsikList.Add(kokDers);
                        }
                    }

                    hiyerarsikList = HiyerarsikGrupDoldur(hiyerarsikList, ref hocaDersSorgu);
                    int sira = 1;
                    if (hiyerarsikList.Count() > 0)
                    {
                        foreach (var hd in hiyerarsikList)
                        {
                            foreach (var ders in hd.DersGrupList)
                            {
                                ders.Sira = sira;
                                sira++;
                            }
                        }
                    }
                   
                    return hiyerarsikList;
                }
                else
                {
                    return new List<UstDersViewModel>();
                }
                
               
            }

        }
        private static List<UstDersViewModel> AcilanDersListesiGetir(int personelID, int Yil, int Donem)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var kadi = db.PersonelBilgisi.Find(personelID).KullaniciAdi;
                var sorgu = (from hoca in db.OGRDersGrupHoca
                             join grup in db.OGRDersGrup on hoca.FKDersGrupID equals grup.ID
                             where grup.Acik == true && grup.Silindi == false && (hoca.FKPersonelID == personelID || hoca.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.OGRDersPlanAna4.Koordinator == kadi) &&
                                   ((grup.OGRDersPlan.OGRDersPlanAna.EnumDersSureTip == (int)Sabis.Enum.EnumDersSureTip.DONEMLIK && grup.OgretimYili == Yil && grup.EnumDonem == Donem) || (grup.OGRDersPlan.OGRDersPlanAna.EnumDersSureTip == (int)Sabis.Enum.EnumDersSureTip.YILLIK && grup.OgretimYili == Yil))
                             orderby grup.GrupAd
                             select new HocaDersListesiViewModelv2
                             {
                                 DPAnaID = grup.OGRDersPlan.FKDersPlanAnaID.Value,
                                 DPAnaUstID = grup.OGRDersPlan.OGRDersPlanAna.FKDersPlanAnaUstID.Value,
                                 DersGrupHocaID = hoca.ID,
                                 DersKod = grup.OGRDersPlan.BolKodAd + grup.OGRDersPlan.DersKod,
                                 GorunenDersAd = grup.EnumDersGrupDil.Value == (int)Sabis.Enum.EnumDil.INGILIZCE ? grup.OGRDersPlan.OGRDersPlanAna.YabDersAd : grup.OGRDersPlan.OGRDersPlanAna.DersAd,
                                 DersAd = grup.OGRDersPlan.OGRDersPlanAna.DersAd,
                                 DersYabAd = grup.OGRDersPlan.OGRDersPlanAna.YabDersAd,
                                 //EnumOgretimTur = grup.OGRDersPlan.EnumOgretimTur ?? grup.EnumOgretimTur, 
                                 EnumOgretimTur = grup.EnumOgretimTur,
                                 Donem = Donem,
                                 BolumKod = grup.OGRDersPlan.BolKodAd,
                                 OgrenciSayisi = grup.OGROgrenciYazilma.Where(x => x.Iptal == false && x.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL && x.OGROgrenciNot.EnumOgrenciBasariNot != (int)EnumOgrenciBasariNot.W).Count(),
                                 OgrenciSayisiButunleme = grup.OGROgrenciYazilma.Where(x => x.Iptal == false && (x.ButunlemeMi.HasValue && x.ButunlemeMi.Value) && x.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL && x.OGROgrenciNot.EnumOgrenciBasariNot != (int)EnumOgrenciBasariNot.W).Count(),
                                 GrupAd = grup.GrupAd == "?" ? "A" : grup.GrupAd,
                                 DersGrupID = grup.ID,
                                 Yil = Yil,
                                 Kredi = grup.OGRDersPlan.OGRDersPlanAna.Kredi.HasValue ? grup.OGRDersPlan.OGRDersPlanAna.Kredi.Value : 0,
                                 ECTSKredi = grup.OGRDersPlan.OGRDersPlanAna.ECTSKredi.HasValue ? grup.OGRDersPlan.OGRDersPlanAna.ECTSKredi.Value : 0,
                                 EnumDersPlanZorunlu = grup.OGRDersPlan.EnumDersPlanZorunlu,
                                 FakulteID = grup.OGRDersPlan.FKFakulteBirimID,
                                 ProgramID = grup.OGRDersPlan.FKBirimID,
                                 DSaat = grup.OGRDersPlanAna.DSaat,
                                 USaat = grup.OGRDersPlanAna.USaat,
                                 YariYil = grup.OGRDersPlan.Yariyil,
                                 EnumKokDersTipi = grup.OGRDersPlan.OGRDersPlanAna.EnumKokDersTipi,
                                 ButunlemeMi = grup.OGROgrenciYazilma.Any(x => x.Iptal == false && (x.ButunlemeMi.HasValue && x.ButunlemeMi.Value == true) && x.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL),
                                 EnumSeviye = grup.OGRDersPlan.EnumOgretimSeviye,
                                 Hiyerarsik = grup.OGRDersPlan.OGRDersPlanAna.Hiyerarsik,
                                 KokDPAnaID = grup.OGRDersPlan.OGRDersPlanAna.FKHiyerarsikKokID,
                                 DersDegTempList = db.ABSDegerlendirme.Where(deg => deg.FKDersGrupID == grup.ID && deg.SilindiMi == false).OrderByDescending(x => x.ID).Select(deg => new DersDegTemp { ID = deg.ID, Butunleme = deg.ButunlemeMi, EnumSonHal = deg.EnumSonHal }).ToList(),
                                 DersEtiketleri = grup.OGRDersPlan.OGRDersPlanEtiket.Select(a => a.OGRTNMDersEtiket.Etiket).ToList(),
                                 DersSecimTipi = grup.OGRDersPlan.EnumDersSecimTipi == (int)EnumDersSecimTipi.ZORUNLU ? "Zorunlu" : "Seçmeli",
                                 Mufredatlar = db.OGRMufredatDersleri.Where(a=>a.FKDersPlanAnaID==grup.OGRDersPlan.OGRDersPlanAna.ID).OrderBy(a=>a.FKMufredatID).Select(a=>a.OGRMufredat.MufredatAdi).ToList()
                                 
                             }).OrderBy(d => d.EnumSeviye).ThenBy(d => d.EnumOgretimTur).ThenBy(d=>d.ProgramID).ThenBy(d => d.DersKod);
#if DEBUG
                var sql = sorgu.ToString();
#endif

                var hocaDersSorgu = sorgu.ToList();

                foreach (var item in hocaDersSorgu)
                {
                    if (item.ProgramID.HasValue && Sabis.Client.BirimIslem.Instance.getirBagliBolum(item.ProgramID.Value) != null)
                    {
                        item.ProgramAd = Sabis.Client.BirimIslem.Instance.BirimDict[item.ProgramID.Value].BirimAdi;
                        item.BolumAd = Sabis.Client.BirimIslem.Instance.getirBagliBolum(item.ProgramID.Value).BirimAdi;
                    }
                    if (item.FakulteID.HasValue)
                    {
                        item.FakulteAd = Sabis.Client.BirimIslem.Instance.getirBagliFakulte(item.FakulteID.Value).BirimAdi;
                    }

                    var degerlendirme = item.DersDegTempList.FirstOrDefault(x => x.Butunleme == item.ButunlemeMi);
                    if (degerlendirme == null)
                    {
                        item.EnumSonHal = (int)EnumSonHal.HAYIR;
                    }
                    else if (degerlendirme.EnumSonHal == (int)EnumSonHal.EVET)
                    {
                        item.EnumSonHal = (int)EnumSonHal.EVET;
                    }
                    else
                    {
                        item.EnumSonHal = (int)EnumSonHal.HAYIR;
                    }
                }

                Dictionary<int, UstDersViewModel> anaDict = new Dictionary<int, UstDersViewModel>();
                List<int> kokDersIDList = new List<int>();
                foreach (var item in hocaDersSorgu)
                {
                    GetirDersPlanAnaDict(item.DPAnaID, item.KokDPAnaID, ref anaDict, ref kokDersIDList);
                }

                anaDict = DersAgacTemizle(ref anaDict, hocaDersSorgu.Select(x => x.DPAnaID).ToList());

                List<UstDersViewModel> hiyerarsikList = new List<UstDersViewModel>();
                foreach (var item in kokDersIDList)
                {
                    UstDersViewModel kokDers = null;
                    anaDict.TryGetValue(item, out kokDers);
                    if (kokDers != null)
                    {
                        if (kokDers.Hiyerarsik)
                        {
                            kokDers.AltDersList = GetirDersHiyerarsik(ref anaDict, kokDers.DersPlanAnaID);
                        }
                        hiyerarsikList.Add(kokDers);
                    }
                }

                hiyerarsikList = HiyerarsikGrupDoldur(hiyerarsikList, ref hocaDersSorgu);
                return hiyerarsikList;
            }

        }
        public static UstDersViewModel ListeleAnaDersDetay(int personelID, int Yil, int Donem, int dersPlanAnaID)
        {
            List<UstDersViewModel> hiyerarsikList = DersListesiGetir(personelID, Yil, Donem);
            UstDersViewModel result = AnaFiltre(hiyerarsikList, dersPlanAnaID);
            return result;
        }
        private static UstDersViewModel AnaFiltre(List<UstDersViewModel> hiyerarsikList, int dersPlanAnaID)
        {
            UstDersViewModel result = null;
            foreach (var item in hiyerarsikList)
            {
                if (item.DersPlanAnaID == dersPlanAnaID)
                {
                    result = item;
                    break;
                }
                else if (item.AltDersList != null)
                {
                    result = AnaFiltre(item.AltDersList, dersPlanAnaID);
                    if (result != null)
                    {
                        break;
                    }
                }
            }
            return result;
        }
        private static Dictionary<int, UstDersViewModel> DersAgacTemizle(ref Dictionary<int, UstDersViewModel> anaDict, List<int> altDPAnaIDList)
        {
            Dictionary<int, UstDersViewModel> result = new Dictionary<int, UstDersViewModel>();
            foreach (var item in altDPAnaIDList)
            {
                int? ustID = item;
                int sayac = 0;
                while (ustID.HasValue)
                {
                    UstDersViewModel ana = null;
                    anaDict.TryGetValue(ustID.Value, out ana);
                    if (ana != null)
                    {
                        ustID = ana.FKUstDersPlanAnaID;
                        if (!result.ContainsKey(ana.DersPlanAnaID))
                        {
                            result.Add(ana.DersPlanAnaID, ana);
                        }
                    }
                    //Kendi kendine işaret eden dersler olursa döngüyü kırmak için güvenlik
                    sayac++;
                    if (sayac > 100)
                    {
                        break;
                    }
                }
            }
            return result;
        }

        private static List<UstDersViewModel> HiyerarsikGrupDoldur(List<UstDersViewModel> hiyerarsikList, ref List<HocaDersListesiViewModelv2> hocaDersSorgu)
        {
            List<UstDersViewModel> result = new List<UstDersViewModel>();
            foreach (var item in hiyerarsikList)
            {
                result.Add(item);
                if (item.AltDersList == null)
                {
                    item.DersGrupList = hocaDersSorgu.Where(x => x.DPAnaID == item.DersPlanAnaID).OrderBy(d => d.EnumOgretimTur).ThenBy(d => d.GrupAd).ThenBy(d => d.DersKod).ThenBy(d => d.EnumSeviye).ToList();
                }
                else
                {
                    item.AltDersList = HiyerarsikGrupDoldur(item.AltDersList, ref hocaDersSorgu);
                }
            }
            return result;
        }

        private static List<UstDersViewModel> GetirDersHiyerarsik(ref Dictionary<int, UstDersViewModel> anaDict, int dpAnaID)
        {
            List<UstDersViewModel> result = null;
            var altDersler = anaDict.Where(x => x.Value.FKUstDersPlanAnaID.HasValue && x.Value.FKUstDersPlanAnaID.Value == dpAnaID).Select(x => x.Value).ToList();
            if (altDersler != null && altDersler.Any())
            {
                result = new List<UstDersViewModel>();
                foreach (var item in altDersler)
                {
                    item.AltDersList = GetirDersHiyerarsik(ref anaDict, item.DersPlanAnaID);
                    result.Add(item);
                }
            }
            return result;
        }

        private static void GetirDersPlanAnaDict(int dersPlanAnaID, int? kokAnaID, ref Dictionary<int, UstDersViewModel> dict, ref List<int> kokDersIDList)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                List<UstDersViewModel> tmp = null;
                var query = db.OGRDersPlanAna.OrderBy(x => x.ID).AsQueryable();
                if (kokAnaID.HasValue)
                {
                    query = query.Where(x => x.FKHiyerarsikKokID == kokAnaID || x.ID == kokAnaID.Value);
                }
                else
                {
                    query = query.Where(x => x.ID == dersPlanAnaID);
                }

                tmp = query.Select(x => new UstDersViewModel { DersPlanAnaID = x.ID, FKUstDersPlanAnaID = x.FKDersPlanAnaUstID, DersAd = x.DersAd, KokDersPlanAnaID = x.FKHiyerarsikKokID, Hiyerarsik = x.Hiyerarsik }).ToList();

                foreach (var item in tmp)
                {
                    if (!dict.ContainsKey(item.DersPlanAnaID))
                    {
                        dict.Add(item.DersPlanAnaID, item);
                        if (!item.FKUstDersPlanAnaID.HasValue)
                        {
                            kokDersIDList.Add(item.DersPlanAnaID);
                        }
                    }
                }
            }
        }

        public static DersDetayVM GetirDersDetay(int DersGrupHocaID, int? FKPersonelID = null, int? haftaNo = null)
        {
            DersDetayVM dersDetay = new DersDetayVM();
            using (UYSv2Entities db = new UYSv2Entities())
            {
                dersDetay = db.OGRDersGrupHoca.Where(x => x.ID == DersGrupHocaID).Select(x => new DersDetayVM
                {
                    DersGrupHocaID = x.ID,
                    DersGrupID = x.FKDersGrupID.Value,
                    DersPlanAnaID = x.OGRDersGrup.FKDersPlanAnaID.Value,
                    DersPlanID = x.OGRDersGrup.FKDersPlanID.Value,
                    BolumKod = x.OGRDersGrup.OGRDersPlan.BolKodAd,
                    DersKod = x.OGRDersGrup.OGRDersPlan.DersKod,
                    DersAd = x.OGRDersGrup.OGRDersPlanAna.DersAd,
                    Kredi = x.OGRDersGrup.OGRDersPlanAna.Kredi.Value,
                    GrupAd = x.OGRDersGrup.GrupAd == "?" ? "A" : x.OGRDersGrup.GrupAd,
                    Yil = x.OGRDersGrup.OgretimYili.Value,
                    Yariyil = x.OGRDersGrup.OGRDersPlan.Yariyil,
                    Donem = x.OGRDersGrup.EnumDonem.Value,
                    FakulteAd = "",
                    BolumAd = "",
                    FakulteID = 0,
                    BolumID = 0,
                    ProgramID = x.OGRDersGrup.OGRDersPlan.FKBirimID,
                    ProgramAd = x.OGRDersGrup.Birimler.BirimAdi,
                    OgrenciSayisi = x.OGRDersGrup.OGROgrenciYazilma.Count(y => !y.Iptal && y.OGRKimlik.EnumOgrDurum == (int)Sabis.Enum.EnumOgrDurum.NORMAL && y.OGROgrenciNot.EnumOgrenciBasariNot != (int)EnumOgrenciBasariNot.W),
                    EnumDersPlanZorunlu = x.OGRDersGrup.OGRDersPlan.EnumDersPlanZorunlu,
                    EnumOgretimSeviye = x.OGRDersGrup.Birimler.EnumSeviye,
                    EnumDersPlanOrtalama = x.OGRDersGrup.OGRDersPlan.EnumDersPlanOrtalama,
                    EnumOgretimTur = x.OGRDersGrup.EnumOgretimTur,
                    EnumKokDersTipi = x.OGRDersGrup.OGRDersPlanAna.EnumKokDersTipi,
                    HiyerarsikKokID = x.OGRDersGrup.OGRDersPlanAna.FKHiyerarsikKokID,
                    DersSaati = x.OGRDersGrup.OGRDersPlanAna.DSaat,
                    UygulamaSaati = x.OGRDersGrup.OGRDersPlanAna.USaat,
                    FinalTarihi = DateTime.Now,
                    UzemMi = x.OGRDersGrup.OGRDersPlan.UzemMi,
                    PersonelID = x.FKPersonelID,
                    //ButunlemeMiOrijinal = x.OGRDersGrup.OGROgrenciYazilma.Where(y => !y.Iptal && y.OGRKimlik.EnumOgrDurum == (int)Sabis.Enum.EnumOgrDurum.NORMAL && y.ButunlemeMi.HasValue && y.ButunlemeMi.Value).Any(),
                    ButunlemeMiOrijinal = x.OGRDersGrup.OGROgrenciYazilma.Any(y => y.Iptal == false && (y.ButunlemeMi.HasValue && y.ButunlemeMi.Value == true) && y.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL && y.OGROgrenciNot.EnumOgrenciBasariNot != (int)EnumOgrenciBasariNot.W),
                    Koordinator = "",
                    FKKoordinatorID = 0,
                    KoordinatorAdSoyad = "",
                    DersProgramiv2ID = haftaNo.HasValue ? x.OGRDersProgramiv2.FirstOrDefault(y => y.YilinHaftasi == haftaNo).ID : 0,
                    EnumDersSureTip = x.OGRDersGrup.OGRDersPlanAna.EnumDersSureTip,
                    DersGrupBirimID = x.OGRDersGrup.FKBirimID,
                    SinifAdi = "",
                    EnumDegerlendirmeTip =  x.OGRDersGrup.OGRDersPlan.EnumDegerlendirmeTipi, // (int?)EnumDegerlendirmeTipi.BASARI_NOT,
                    SonHalVerildiMi = x.OGRDersGrup.ABSDegerlendirme.Any(y => y.FKDersGrupID == x.FKDersGrupID && !y.SilindiMi && y.EnumSonHal == (int)Sabis.Enum.EnumSonHal.EVET)
                }).FirstOrDefault();
                //result = dersDetay.FirstOrDefault();

                var fakulte = Sabis.Client.BirimIslem.Instance.getirBagliFakulte(dersDetay.ProgramID.Value);
                var bolum = Sabis.Client.BirimIslem.Instance.getirBagliBolum(dersDetay.ProgramID.Value);
                var koordinator = db.DersKoordinator.FirstOrDefault(x => x.FKDersPlanAnaID == dersDetay.DersPlanAnaID && !x.Silindi);

                if (fakulte != null)
                {
                    dersDetay.FakulteID = fakulte.ID;
                    dersDetay.FakulteAd = fakulte.BirimAdi;
                }
                if (bolum != null)
                {
                    dersDetay.BolumID = bolum.ID;
                    dersDetay.BolumAd = bolum.BirimAdi;
                }
                if (koordinator != null)
                {
                    dersDetay.Koordinator = koordinator.PersonelBilgisi.KullaniciAdi;
                    dersDetay.FKKoordinatorID = koordinator.FKPersonelID;
                    dersDetay.KoordinatorAdSoyad = koordinator.PersonelBilgisi.UnvanAd + " " + koordinator.PersonelBilgisi.Ad + " " + koordinator.PersonelBilgisi.Soyad;
                }
                
                dersDetay.SinifAdi = dersDetay.BolumAd + " " + dersDetay.DersKod + " - " + dersDetay.DersAd + " - " + dersDetay.EnumOgretimTur + ". Öğretim" + " - " + dersDetay.GrupAd + " Grubu" + " (" + dersDetay.OgrenciSayisi + ")";
                
            }
            return dersDetay;
        }

        public static List<DersSinavTarihleriVM> GetirDersSinavTarihleri(int DersGrupHocaID, int yil, int donem, int UniversiteID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                List<DersSinavTarihleriVM> listTarihAraligi = new List<DersSinavTarihleriVM>();
                var dersBilgi = db.OGRDersGrupHoca.Where(d => d.ID == DersGrupHocaID && d.FKDersGrupID.HasValue).Select(x => new { DersGrupID = x.FKDersGrupID.Value, FKBolumPrBirimID = x.OGRDersGrup.OGRDersPlan.FKBolumPrBirimID, FKFakulteBirimID = x.OGRDersGrup.OGRDersPlan.FKFakulteBirimID, EnumDersSureTip = x.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.EnumDersSureTip, Yil = x.OGRDersGrup.OgretimYili.Value, Donem = x.OGRDersGrup.EnumDonem }).FirstOrDefault();
                if (dersBilgi == null)
                {
                    return new List<DersSinavTarihleriVM>();
                }

                //var payList = PayBL.ListelePay(dersBilgi.DersGrupID);
                var payList = PAYMAIN.GetirPayBilgileri(dersBilgi.DersGrupID);

                foreach (var item in payList)
                {
                    DersSinavTarihleriVM ta = new DersSinavTarihleriVM();
                    int BirimID = UniversiteID;
                    //önce program birim id sine bakılıyor yoksa fakulte birimid si
                    if (dersBilgi.FKBolumPrBirimID.HasValue)
                    {
                        BirimID = dersBilgi.FKBolumPrBirimID.Value;
                    }
                    else if (dersBilgi.FKFakulteBirimID.HasValue)
                    {
                        BirimID = dersBilgi.FKFakulteBirimID.Value;
                    }
                    EnumAkademikTarih akademikTarih = GENEL.GENELMAIN.AkademikTarihDonustur((EnumCalismaTip)item.EnumCalismaTipi);

                    Sabis.Core.DTO.AkademikTakvim tarihKontrol = new Sabis.Core.DTO.AkademikTakvim();
                    try
                    {
                        int donemDeger = donem;
                        if (dersBilgi.EnumDersSureTip != (int)EnumDersSureTip.YILLIK)
                        {
                            donemDeger = dersBilgi.Donem.Value;
                        }
                        tarihKontrol = Sabis.Client.AkademikTakvimIslem.GetirTarih(akademikTarih, dersBilgi.Yil, donemDeger, BirimID);
                    }
                    catch (Exception)
                    {
                        tarihKontrol = null;
                    }

                    if (tarihKontrol != null)
                    {
                        ta.TarihBaslangic = tarihKontrol.TarihBaslangic;
                        ta.TarihBitis = tarihKontrol.TarihBitis;
                        ta.EnumAkademikTarih = akademikTarih;
                        if (!listTarihAraligi.Any(x => x.EnumAkademikTarih == ta.EnumAkademikTarih))
                        {
                            listTarihAraligi.Add(ta);
                        }
                    }
                }
                return listTarihAraligi;
            }
        }

        public static List<int> GetirAltDersGrupIDList(ref UYSv2Entities db, int dersPlanAnaID, int yil, int donem)
        {
            List<int> result = new List<int>();
            var grupIDList = db.OGRDersGrup.Where(x => x.OGRDersPlan.FKDersPlanAnaID == dersPlanAnaID && ((x.OGRDersPlan.OGRDersPlanAna.EnumDersSureTip == (int)Sabis.Enum.EnumDersSureTip.DONEMLIK && x.OgretimYili == yil && x.EnumDonem == donem) || (x.OGRDersPlan.OGRDersPlanAna.EnumDersSureTip == (int)Sabis.Enum.EnumDersSureTip.YILLIK && x.OgretimYili == yil))).Select(x => x.ID).ToList();
            result.AddRange(grupIDList);
            var altDersList = db.OGRDersPlanAna.Where(x => x.FKDersPlanAnaUstID == dersPlanAnaID);
            foreach (var item in altDersList)
            {
                var grupIDAltList = GetirAltDersGrupIDList(ref db, item.ID, yil, donem);
                result.AddRange(grupIDAltList);
            }
            return result.Distinct().ToList();
        }

        public static List<PayBilgileriVM> ListelePaylarVeNotBilgileriv2(int DersGrupHocaID, string kullanici, string IP)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var DersGrupHoca = db.OGRDersGrupHoca.FirstOrDefault(d => d.ID == DersGrupHocaID);
                int dersGrupID = DersGrupHoca.FKDersGrupID.Value;

                var payList = PAYMAIN.GetirPayBilgileri(dersGrupID);

                int butSayi = db.OGROgrenciYazilma.Count(x => x.FKDersGrupID == dersGrupID && x.Iptal != true && x.ButunlemeMi == true);
                bool butVarMi = butSayi > 0;
                var butPay = payList.FirstOrDefault(x => x.EnumCalismaTipi == (int)Sabis.Enum.EnumCalismaTip.Butunleme);
                if (butPay == null)
                {
                    if (butVarMi)
                    {
                        var final = payList.FirstOrDefault(x => x.EnumCalismaTipi == (int)Sabis.Enum.EnumCalismaTip.Final);
                        //Bütünleme payı yok, ekle
                        if (final == null)
                        {
                            throw new Exception("Bütünleme için baz alınacak final oranı bulunamadı!");
                        }
                        ABSPaylar butunlemePayi = new ABSPaylar();
                        butunlemePayi.FKDersPlanAnaID = final.DersPlanAnaID;
                        butunlemePayi.FKDersPlanID = final.DersPlanID;
                        butunlemePayi.EnumCalismaTip = (int)Sabis.Enum.EnumCalismaTip.Butunleme;
                        butunlemePayi.Oran = Convert.ToByte(final.KatkiOrani);
                        butunlemePayi.Sira = 1;
                        butunlemePayi.Yil = final.Yil;
                        butunlemePayi.EnumDonem = final.Donem;
                        butunlemePayi.Pay = final.KatKiOraniPay;
                        butunlemePayi.Payda = final.KatKiOraniPayda;

                        db.ABSPaylar.Add(butunlemePayi);
                        db.SaveChanges();
                        PayBilgileriVM butModel = new PayBilgileriVM();
                        butModel.EnumCalismaTipi = butunlemePayi.EnumCalismaTip;
                        butModel.Donem = butunlemePayi.EnumDonem;
                        //butModel.FKDersGrupID = butunlemePayi.FKDersGrupID;
                        butModel.DersPlanAnaID = butunlemePayi.FKDersPlanAnaID;
                        butModel.DersPlanID = butunlemePayi.FKDersPlanID;
                        //butModel.FKPersonelID = butunlemePayi.FKPersonelID;
                        butModel.PayID = butunlemePayi.ID;
                        butModel.KatkiOrani = butunlemePayi.Oran;
                        butModel.SiraNo = butunlemePayi.Sira;
                        butModel.Yil = butunlemePayi.Yil.Value;
                        butModel.ToplamOgrenciSayisi = butSayi;
                        payList.Add(butModel);
                    }
                }
                //Bütünleme payı var ancak bütünlemeye kalan öğrenci yok.
                else if (!butVarMi)
                {
                    payList.Remove(butPay);
                }
                else
                {
                    butPay.ToplamOgrenciSayisi = butSayi;
                }

                //IP yetki kontrolü
                bool yetkiVarMi = ADMIN.IPMAIN.KontrolIPYetki(kullanici, IP);
                bool sonHalVerildi = db.ABSDegerlendirme.Any(d => d.FKDersGrupID == dersGrupID && d.SilindiMi == false && d.ButunlemeMi == butVarMi && d.EnumSonHal == (int)EnumSonHal.EVET);

                foreach (var item in payList)
                {
                    int FakulteID = 0;
                    int BirimID = 21398;
                    //önce program birim id sine bakılıyor yoksa fakulte birimid si
                    if (DersGrupHoca.OGRDersGrup.OGRDersPlan.FKBolumPrBirimID.HasValue)
                    {
                        FakulteID = Sabis.Client.BirimIslem.Instance.getirBagliFakulte(DersGrupHoca.OGRDersGrup.OGRDersPlan.FKBolumPrBirimID.Value).ID;
                        BirimID = DersGrupHoca.OGRDersGrup.OGRDersPlan.FKBolumPrBirimID.Value;
                    }
                    else if (DersGrupHoca.OGRDersGrup.OGRDersPlan.FKFakulteBirimID.HasValue)
                    {
                        BirimID = DersGrupHoca.OGRDersGrup.OGRDersPlan.FKFakulteBirimID.Value;
                    }

                    if (yetkiVarMi)
                    {
                        item.TarihAraligiUygunMu = yetkiVarMi;
                    }
                    else
                    {
                        EnumAkademikTarih akademikTarih = GENEL.GENELMAIN.AkademikTarihDonustur((EnumCalismaTip)item.EnumCalismaTipi);
                        bool tarihKontrol = false;

                        try
                        {
                            tarihKontrol = Sabis.Client.AkademikTakvimIslem.TarihKontrolu(akademikTarih, DersGrupHoca.OGRDersGrup.OgretimYili.Value, DersGrupHoca.OGRDersGrup.EnumDonem.Value, BirimID);
                        }
                        catch (Exception)
                        {
                            tarihKontrol = false;
                        }
                        item.TarihAraligiUygunMu = tarihKontrol;
                    }


                    if (butVarMi)
                    {
                        if (item.EnumCalismaTipi == (int)EnumCalismaTip.Butunleme && item.TarihAraligiUygunMu == true)
                        {
                            item.TarihAraligiUygunMu = true;
                        }
                        else
                        {
                            item.TarihAraligiUygunMu = false;
                        }
                    }

                    //Hazırlık derslrinin not girişleri aktif kalsın.
                    if (item.EnumDersTipi == 20 || item.EnumDersTipi == 21 || item.EnumDersTipi == 22 || item.EnumDersTipi == 29 || item.EnumDersTipi == 30 || item.EnumDersTipi == 31 || item.EnumDersTipi == 40)
                    {
                        item.TarihAraligiUygunMu = true;
                    }

                    //adamyo tarih aralığı kapat
                    if (yetkiVarMi == true && FakulteID == 339)
                    {
                        item.TarihAraligiUygunMu = true;
                    }
                }
                return payList;
            }

        }

        public static int GetirDersHaftaSaat(int DersGrupHocaID, int yil, int ay, int gun)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return db.OGRDersProgramiv2.Count(x => x.FKDersGrupHocaID == DersGrupHocaID && x.DersYil == yil && x.DersAy == ay && x.DersGun == gun);
            }
        }

        public static UstDersViewModel GetirDersPlanAna(int dpAnaID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return db.OGRDersPlanAna.Where(x => x.ID == dpAnaID).Select(x => new UstDersViewModel { DersPlanAnaID = x.ID, FKUstDersPlanAnaID = x.FKDersPlanAnaUstID, DersAd = x.DersAd, Hiyerarsik = x.Hiyerarsik, KokDersPlanAnaID = x.FKHiyerarsikKokID }).First();
            }
        }
    }
}
