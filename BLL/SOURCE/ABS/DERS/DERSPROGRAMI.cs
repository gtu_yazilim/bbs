﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Bolum.Core.ABS.DERS;
using Sabis.Bolum.Core.dto.Ders;
using Sabis.Enum;

namespace Sabis.Bolum.Bll.SOURCE.ABS.DERS
{
    public class DERSPROGRAMI
    {
        private class DersProgramTemp
        {
            public string DersAd { get; set; }
            public string DersKod { get; set; }
            public DateTime DersTarihSaat { get; set; }
            public int? EnumOgretimTuru { get; set; }
            public int FKBirimID { get; set; }
            public int FKDersGrupHocaID { get; set; }
            public int FKDersGrupID { get; set; }
            public int FKDersPlanAnaID { get; set; }
            public int? FKMekanv2ID { get; set; }
            public int ID { get; set; }
            public string MekanAd { get; internal set; }
            public double? MekanKoordinatLat { get; internal set; }
            public double? MekanKoordinatLon { get; internal set; }
            public string MekanUzunAd { get; internal set; }
            public int OgrenciSayisi { get; internal set; }
        }

        private static List<DersProgramTemp> GetirDersProgramiData(int personelID, int yil, int donem, DateTime baslangic, DateTime bitis)
        {
            List<DersProgramTemp> result = null;
            using (UYSv2Entities db = new UYSv2Entities())
            {
                result = (from veri in db.OGRDersProgramiv2
                          where veri.OGRDersGrupHoca.FKPersonelID == personelID && ((veri.OGRDersGrupHoca.OGRDersGrup.OGRDersPlan.EnumDersSureTip == (int)Sabis.Enum.EnumDersSureTip.DONEMLIK && veri.OGRDersGrupHoca.OGRDersGrup.OgretimYili == yil && veri.OGRDersGrupHoca.OGRDersGrup.EnumDonem == donem) || (veri.OGRDersGrupHoca.OGRDersGrup.OGRDersPlan.EnumDersSureTip == (int)Sabis.Enum.EnumDersSureTip.YILLIK && veri.OGRDersGrupHoca.OGRDersGrup.OgretimYili == yil))
                          && veri.DersTarihSaat >= baslangic && veri.DersTarihSaat <= bitis
                          orderby veri.DersTarihSaat
                          select new DersProgramTemp
                          {
                              ID = veri.ID,
                              DersTarihSaat = veri.DersTarihSaat,
                              FKDersGrupHocaID = veri.FKDersGrupHocaID,
                              FKDersGrupID = veri.OGRDersGrupHoca.FKDersGrupID.Value,
                              FKMekanv2ID = veri.FKMekanv2ID,
                              EnumOgretimTuru = veri.OGRDersGrupHoca.OGRDersGrup.EnumOgretimTur,
                              FKBirimID = veri.OGRDersGrupHoca.OGRDersGrup.FKBirimID,
                              DersKod = veri.OGRDersGrupHoca.OGRDersGrup.OGRDersPlan.BolKodAd + " " + veri.OGRDersGrupHoca.OGRDersGrup.OGRDersPlan.DersKod + " ",
                              DersAd = veri.OGRDersGrupHoca.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.DersAd,
                              FKDersPlanAnaID = veri.OGRDersGrupHoca.OGRDersGrup.OGRDersPlan.FKDersPlanAnaID.Value,
                              MekanAd = veri.MekanV2.MekanAd,
                              MekanUzunAd = veri.MekanV2.MekanUzunAd,
                              MekanKoordinatLat = veri.MekanV2.Latitude,
                              MekanKoordinatLon = veri.MekanV2.Longitude,
                              OgrenciSayisi = veri.OGRDersGrupHoca.OGRDersGrup.OGROgrenciYazilma.Count(y=> y.FKDersGrupID == veri.OGRDersGrupHoca.FKDersGrupID && !y.Iptal)
                          }).Where(x=> x.OgrenciSayisi != 0).ToList();
            }
            return result;
        }

        private static List<DersProgramiDto> ProgramBirlestir(List<DersProgramTemp> data)
        {
            List<DersProgramiDto> result = new List<DersProgramiDto>();
            Dictionary<int, int> temp = new Dictionary<int, int>();
            foreach (var item in data)
            {
                if (temp.ContainsKey(item.ID))
                {
                    continue;
                }

                DersProgramiDto dersProgram = new DersProgramiDto();
                dersProgram.ID = item.ID;
                if (item.FKMekanv2ID.HasValue)
                {
                    dersProgram.Mekan = new Core.dto.Mekan.MekanDto();
                    dersProgram.Mekan.Ad = item.MekanAd;
                    dersProgram.Mekan.UzunAd = item.MekanUzunAd;
                    dersProgram.Mekan.ID = item.FKMekanv2ID.Value;
                    if (item.MekanKoordinatLat.HasValue && item.MekanKoordinatLon.HasValue)
                    {
                        dersProgram.Mekan.Koordinat = new Core.dto.Mekan.GeoCoordinate(item.MekanKoordinatLat.Value, item.MekanKoordinatLon.Value);
                    }
                    dersProgram.Mekan.ID = item.FKMekanv2ID.Value;
                }
                dersProgram.Baslangic = item.DersTarihSaat;
                dersProgram.Bitis = item.DersTarihSaat.AddHours(1);
                dersProgram.DersBilgisi = new DersBilgisiDto()
                {
                    DersGrupID = item.FKDersGrupID,
                    DersGrupHocaID = item.FKDersGrupHocaID,
                    FKBirimID = item.FKBirimID,
                    DersAd = item.DersAd,
                    DersKod = item.DersKod,
                    DersPlanAnaID = item.FKDersPlanAnaID,
                    EnumOgretimTur = (EnumOgretimTur)item.EnumOgretimTuru
                };
                dersProgram.ParcaList = new List<DersProgramParcaDto>();
                dersProgram.ParcaList.Add(new DersProgramParcaDto() { DersProgramID = item.ID, Tarih = item.DersTarihSaat, Saat = item.DersTarihSaat.Hour });
                temp.Add(dersProgram.ID, dersProgram.DersBilgisi.DersGrupID);
                var parcalar = data.Where(x => x.FKDersGrupID == item.FKDersGrupID && x.DersTarihSaat > item.DersTarihSaat);
                foreach (var parca in parcalar)
                {
                    if (parca.DersTarihSaat == dersProgram.Bitis)
                    {
                        if (temp.Any(x=> x.Key == parca.ID))
                        {
                            continue;
                        }
                        temp.Add(parca.ID, parca.FKDersGrupID);
                        dersProgram.Bitis = dersProgram.Bitis.AddHours(1);
                        dersProgram.ParcaList.Add(new DersProgramParcaDto() { DersProgramID = parca.ID, Tarih = parca.DersTarihSaat, Saat = parca.DersTarihSaat.Hour });
                    }
                    else
                    {
                        break;
                    }
                }
                result.Add(dersProgram);
            }
            temp = null;
            return result;
        }

        public static List<DersProgramiDto> GetirOgretimElemaniDersProgramiv2(int personelID, int yil, int donem)
        {
            List<DersProgramiDto> result = null;
            var sorgu = GetirDersProgramiData(personelID, yil, donem, DateTime.MinValue, DateTime.MaxValue);
            result = ProgramBirlestir(sorgu);
            return result;
        }

        public static List<DersProgramiDto> GetirOgretimElemaniDersProgramiAralik(int personelID, int yil, int donem, DateTime baslangic, DateTime bitis)
        {
            List<DersProgramiDto> result = null;
            var sorgu = GetirDersProgramiData(personelID, yil, donem, baslangic, bitis);
            result = ProgramBirlestir(sorgu);
            return result;
        }


        public static List<DersProgramiDto> GetirGrupDersProgramiv2(int personelID, int dersGrupID)
        {
            List<DersProgramiDto> result = null;
            List<DersProgramTemp> data = null;
            using (UYSv2Entities db = new UYSv2Entities())
            {
                data = (from veri in db.OGRDersProgramiv2
                        where veri.OGRDersGrupHoca.FKPersonelID == personelID && veri.OGRDersGrupHoca.FKDersGrupID == dersGrupID && veri.OGRDersGrupHoca.OGRDersGrup.Acik == true
                        orderby veri.DersTarihSaat
                        select new DersProgramTemp
                        {
                            ID = veri.ID,
                            DersTarihSaat = veri.DersTarihSaat,
                            FKDersGrupHocaID = veri.FKDersGrupHocaID,
                            FKMekanv2ID = veri.FKMekanv2ID,
                            EnumOgretimTuru = veri.OGRDersGrupHoca.OGRDersGrup.EnumOgretimTur,
                            FKBirimID = veri.OGRDersGrupHoca.OGRDersGrup.FKBirimID,
                            DersKod = veri.OGRDersGrupHoca.OGRDersGrup.OGRDersPlan.BolKodAd + " " + veri.OGRDersGrupHoca.OGRDersGrup.OGRDersPlan.DersKod + " ",
                            DersAd = veri.OGRDersGrupHoca.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.DersAd,
                            FKDersPlanAnaID = veri.OGRDersGrupHoca.OGRDersGrup.OGRDersPlan.FKDersPlanAnaID.Value,
                            MekanAd = veri.MekanV2.MekanAd,
                            MekanUzunAd = veri.MekanV2.MekanUzunAd,
                            MekanKoordinatLat = veri.MekanV2.Latitude,
                            MekanKoordinatLon = veri.MekanV2.Longitude,
                        }).ToList();
            }
            result = ProgramBirlestir(data);
            return result;
        }

        public static object GetirGrupDersProgramiv2Aralik(int personelID, int dersGrupID, DateTime baslangic, DateTime bitis)
        {
            List<DersProgramiDto> result = null;
            List<DersProgramTemp> data = null;
            using (UYSv2Entities db = new UYSv2Entities())
            {
                data = (from veri in db.OGRDersProgramiv2
                        where veri.OGRDersGrupHoca.FKPersonelID == personelID && veri.OGRDersGrupHoca.FKDersGrupID == dersGrupID && veri.OGRDersGrupHoca.OGRDersGrup.Acik == true && veri.DersTarihSaat > baslangic && veri.DersTarihSaat < bitis
                        orderby veri.DersTarihSaat
                        select new DersProgramTemp
                        {
                            ID = veri.ID,
                            DersTarihSaat = veri.DersTarihSaat,
                            FKDersGrupHocaID = veri.FKDersGrupHocaID,
                            FKMekanv2ID = veri.FKMekanv2ID,
                            EnumOgretimTuru = veri.OGRDersGrupHoca.OGRDersGrup.EnumOgretimTur,
                            FKBirimID = veri.OGRDersGrupHoca.OGRDersGrup.FKBirimID,
                            DersKod = veri.OGRDersGrupHoca.OGRDersGrup.OGRDersPlan.BolKodAd + " " + veri.OGRDersGrupHoca.OGRDersGrup.OGRDersPlan.DersKod + " ",
                            DersAd = veri.OGRDersGrupHoca.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.DersAd,
                            FKDersPlanAnaID = veri.OGRDersGrupHoca.OGRDersGrup.OGRDersPlan.FKDersPlanAnaID.Value,
                            MekanAd = veri.MekanV2.MekanAd,
                            MekanUzunAd = veri.MekanV2.MekanUzunAd,
                            MekanKoordinatLat = veri.MekanV2.Latitude,
                            MekanKoordinatLon = veri.MekanV2.Longitude,
                        }).ToList();
            }
            result = ProgramBirlestir(data);
            return result;
        }

    }
}
