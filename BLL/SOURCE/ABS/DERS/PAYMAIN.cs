﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sabis.Bolum.Core.ABS.DERS.DERSDETAY;
using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Enum;


namespace Sabis.Bolum.Bll.SOURCE.ABS.DERS
{
    public class PAYMAIN
    {
        public static List<PayBilgileriVM> GetirPayBilgileri(int dersGrupID, bool yilIciGoster = false)
        {
            List<PayBilgileriVM> result = new List<PayBilgileriVM>();
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var grup = db.OGRDersGrup
                    .Where(x => x.ID == dersGrupID && x.FKDersPlanID.HasValue && x.OGRDersPlan.FKDersPlanAnaID.HasValue)
                    .Select(x => new
                    {
                        Yil = x.OgretimYili,
                        Donem = x.EnumDonem,
                        ID = x.ID,
                        DersPlanID = x.FKDersPlanID.Value,
                        DersPlanAnaID = x.OGRDersPlan.FKDersPlanAnaID.Value,
                        ToplamOgrenciSayisi = x.OGROgrenciYazilma.Count(d => d.Iptal == false && d.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL),
                        EnumKokDersTipi = x.OGRDersPlan.OGRDersPlanAna.EnumKokDersTipi
                    }).First();
                var grupHoca = db.OGRDersGrupHoca.FirstOrDefault(x => x.FKDersGrupID == grup.ID && x.FKPersonelID.HasValue).PersonelBilgisi.KullaniciAdi;


                var sorguBase = db.ABSPaylar
                    .Where(pay =>
                                   !pay.Silindi &&
                                   pay.FKDersPlanAnaID == grup.DersPlanAnaID &&
                                    pay.Yil == grup.Yil &&
                                    ((pay.EnumDonem == grup.Donem && pay.OGRDersPlanAna.EnumDersSureTip == (int)EnumDersSureTip.DONEMLIK) || pay.OGRDersPlanAna.EnumDersSureTip == (int)EnumDersSureTip.YILLIK))
                    .OrderBy(x => x.EnumCalismaTip)
                    .ThenBy(x => x.Sira)
                    .AsQueryable();

                var notDetayList = db.ABSOgrenciNotDetay.Where(x => sorguBase.Select(y => y.ID).ToList().Contains(x.FKABSPaylarID.Value) && x.Kullanici == grupHoca && x.Silindi == false);

#if DEBUG
                var test = sorguBase.ToList();
#endif
                if (!yilIciGoster)
                {
                    sorguBase = sorguBase.Where(pay =>
                    pay.EnumCalismaTip != (int)Sabis.Enum.EnumCalismaTip.YilIcininBasariya);
                }

                if (grup.EnumKokDersTipi == (int)EnumKokDersTipi.UZMANLIKALANI)
                {
                    sorguBase = sorguBase.Where(x => x.FKDersPlanID.HasValue && x.FKDersPlanID.Value == grup.DersPlanID);
                }
                else if (!sorguBase.Any(x => x.FKDersPlanID == grup.DersPlanID) && grup.EnumKokDersTipi != (int)EnumKokDersTipi.UZMANLIKALANI)
                {
                    sorguBase = sorguBase.Where(x => !x.FKDersPlanID.HasValue);
                }
                else if (sorguBase.Any(x => x.FKDersPlanID.HasValue))
                {
                    sorguBase = sorguBase.Where(x => x.FKDersPlanID == grup.DersPlanID);
                }

                result = sorguBase.Select(pay =>
                               new PayBilgileriVM
                               {
                                   PayID = pay.ID,
                                   EnumCalismaTipi = pay.EnumCalismaTip,
                                   SiraNo = pay.Sira,
                                   Donem = grup.Donem,
                                   KatkiOrani = pay.Oran,
                                   KatKiOraniPay = pay.Pay,
                                   KatKiOraniPayda = pay.Payda,
                                   DersPlanID = pay.FKDersPlanID,
                                   DersGrupID = pay.FKDersGrupID,
                                   DersPlanAnaID = pay.FKDersPlanAnaID,
                                   EnumDersTipi = pay.OGRDersPlanAna.EnumDersTipi,
                                   NotuGirilenOgrenciSayisi = pay.ABSOgrenciNot.Count(d => d.FKDersGrupID == dersGrupID && d.FKAbsPaylarID == pay.ID && d.Silindi == false &&
                                    db.OGROgrenciYazilma.Any(yaz => yaz.Iptal != true && yaz.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL && yaz.FKOgrenciID == d.FKOgrenciID && yaz.FKDersGrupID == d.FKDersGrupID)),
                                   Yil = pay.Yil.Value,
                                   ToplamOgrenciSayisi = grup.ToplamOgrenciSayisi,
                                   YayinlanmaDurumu = (pay.ABSYayinlananPayNotlar.Where(d => ((d.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.EnumDersSureTip == (int)EnumDersSureTip.DONEMLIK && d.EnumDonem == grup.Donem) || d.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.EnumDersSureTip == (int)EnumDersSureTip.YILLIK) && d.Yil == grup.Yil && d.FKDersGrupID == dersGrupID).Count() > 0 ? true : false),
                                   DersPlanAnaUstID = pay.OGRDersPlan.OGRDersPlanAna.FKDersPlanAnaUstID,
                                   NotGirilmisMi = pay.ABSOgrenciNot.Any(v=> v.Silindi == false),
                                   EskiNotGirisiAktif = (pay.EnumCalismaTip == (int)EnumCalismaTip.Odev || pay.EnumCalismaTip == (int)EnumCalismaTip.KisaSinav) && pay.ABSOgrenciNot.Any(v=> v.Silindi == false) && !notDetayList.Any(y => y.FKABSPaylarID == pay.ID) ? true : false
                               }).OrderBy(x => x.EnumCalismaTipi).ThenBy(x => x.SiraNo).ToList();
            }
            return result;
        }
        public static List<PayBilgileriVM> GetirPayBilgileriByDPAnaID(int dersPlanAnaID, int yil, int donem, bool yilIciGoster = false)
        {
            List<PayBilgileriVM> result = new List<PayBilgileriVM>();
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var sorguBase = db.ABSPaylar
                    .Where(pay =>
                                   pay.FKDersPlanAnaID == dersPlanAnaID &&
                                    pay.Yil == yil &&
                                    ((pay.EnumDonem == donem && pay.OGRDersPlanAna.EnumDersSureTip == (int)EnumDersSureTip.DONEMLIK) || pay.OGRDersPlanAna.EnumDersSureTip == (int)EnumDersSureTip.YILLIK))
                    .OrderBy(x => x.EnumCalismaTip)
                    .ThenBy(x => x.Sira)
                    .AsQueryable();

                if (!yilIciGoster)
                {
                    sorguBase = sorguBase.Where(pay =>
                    pay.EnumCalismaTip != (int)Sabis.Enum.EnumCalismaTip.YilIcininBasariya);
                }

                result = sorguBase.Select(pay =>
                               new PayBilgileriVM
                               {
                                   PayID = pay.ID,
                                   EnumCalismaTipi = pay.EnumCalismaTip,
                                   SiraNo = pay.Sira,
                                   //Donem = donem,
                                   KatkiOrani = pay.Oran,
                                   //KatKiOraniPay = pay.Pay,
                                   //KatKiOraniPayda = pay.Payda,
                                   //DersPlanID = pay.FKDersPlanID,
                                   //DersGrupID = pay.FKDersGrupID,
                                   //DersPlanAnaID = pay.FKDersPlanAnaID,
                                   //EnumDersTipi = pay.OGRDersPlanAna.EnumDersTipi,
                                   //Yil = pay.Yil.Value,
                                   //ToplamOgrenciSayisi = grup.ToplamOgrenciSayisi,
                                   //YayinlanmaDurumu = (pay.ABSYayinlananPayNotlar.Where(d => ((d.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.EnumDersSureTip == (int)EnumDersSureTip.DONEMLIK && d.EnumDonem == grup.Donem) || d.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.EnumDersSureTip == (int)EnumDersSureTip.YILLIK) && d.Yil == grup.Yil && d.FKDersGrupID == dersGrupID).Count() > 0 ? true : false),
                                   //DersPlanAnaUstID = pay.OGRDersPlan.OGRDersPlanAna.FKDersPlanAnaUstID,
                                   //NotGirilmisMi = pay.ABSOgrenciNot.Any()
                               }).OrderBy(x => x.EnumCalismaTipi).ThenBy(x => x.SiraNo).ToList();
            }
            return result;
        }
        public static List<PayBilgileriVM> GetirPayBilgileriv2(int DersGrupHocaID, string kullanici, string IP)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var DersGrupHoca = db.OGRDersGrupHoca.FirstOrDefault(d => d.ID == DersGrupHocaID);
                int dersGrupID = DersGrupHoca.FKDersGrupID.Value;

                var payList = GetirPayBilgileri(dersGrupID);

                int butSayi = db.OGROgrenciYazilma.Count(x => x.FKDersGrupID == dersGrupID && x.Iptal != true && x.ButunlemeMi == true);
                bool butVarMi = butSayi > 0;


                //RMZN edit : Pay sistemi devre dışı bırakılıyor..
                //var butPay = payList.FirstOrDefault(x => x.EnumCalismaTipi == (int)Sabis.Enum.EnumCalismaTip.Butunleme);
                //if (butPay == null)
                //{
                //    if (butVarMi)
                //    {
                //        var final = payList.FirstOrDefault(x => x.EnumCalismaTipi == (int)Sabis.Enum.EnumCalismaTip.Final);
                //        //Bütünleme payı yok, ekle
                //        if (final == null)
                //        {
                //            throw new Exception("Bütünleme için baz alınacak final oranı bulunamadı!");
                //        }

                //        ABSPaylar butunlemePayi = new ABSPaylar();
                //        butunlemePayi.FKDersPlanAnaID = final.DersPlanAnaID;
                //        butunlemePayi.FKDersPlanID = final.DersPlanID;
                //        butunlemePayi.EnumCalismaTip = (int)Sabis.Enum.EnumCalismaTip.Butunleme;
                //        butunlemePayi.Oran = Convert.ToByte(final.KatkiOrani);
                //        butunlemePayi.Sira = 1;
                //        butunlemePayi.Yil = final.Yil;
                //        butunlemePayi.EnumDonem = final.Donem;
                //        butunlemePayi.Pay = final.KatKiOraniPay;
                //        butunlemePayi.Payda = final.KatKiOraniPayda;

                //        db.ABSPaylar.Add(butunlemePayi);
                //        db.SaveChanges();
                //        PayBilgileriVM butModel = new PayBilgileriVM();
                //        butModel.EnumCalismaTipi = butunlemePayi.EnumCalismaTip;
                //        butModel.Donem = butunlemePayi.EnumDonem;
                //        //butModel.FKDersGrupID = butunlemePayi.FKDersGrupID;
                //        butModel.DersPlanAnaID = butunlemePayi.FKDersPlanAnaID;
                //        butModel.DersPlanID = butunlemePayi.FKDersPlanID;
                //        //butModel.FKPersonelID = butunlemePayi.FKPersonelID;
                //        butModel.PayID = butunlemePayi.ID;
                //        butModel.KatkiOrani = butunlemePayi.Oran;
                //        butModel.SiraNo = butunlemePayi.Sira;
                //        butModel.Yil = butunlemePayi.Yil.Value;
                //        butModel.ToplamOgrenciSayisi = butSayi;
                //        payList.Add(butModel);
                //    }
                //}
                ////Bütünleme payı var ancak bütünlemeye kalan öğrenci yok.
                //else if (!butVarMi)
                //{
                //    payList.Remove(butPay);
                //}
                //else
                //{
                //    butPay.ToplamOgrenciSayisi = butSayi;
                //}

                //IP yetki kontrolü
                bool yetkiVarMi = GENEL.GENELMAIN.KontrolIPYetki(kullanici, IP);
                bool sonHalVerildi = db.ABSDegerlendirme.Any(d => d.FKDersGrupID == dersGrupID && d.SilindiMi == false && d.ButunlemeMi == butVarMi && d.EnumSonHal == (int)EnumSonHal.EVET);

                foreach (var item in payList)
                {
                    int FakulteID = 0;
                    int BirimID = 21398;
                    //önce program birim id sine bakılıyor yoksa fakulte birimid si
                    if (DersGrupHoca.OGRDersGrup.OGRDersPlan.FKBolumPrBirimID.HasValue)
                    {
                        FakulteID = Sabis.Client.BirimIslem.Instance.getirBagliFakulte(DersGrupHoca.OGRDersGrup.OGRDersPlan.FKBolumPrBirimID.Value).ID;
                        BirimID = DersGrupHoca.OGRDersGrup.OGRDersPlan.FKBolumPrBirimID.Value;
                    }
                    else if (DersGrupHoca.OGRDersGrup.OGRDersPlan.FKFakulteBirimID.HasValue)
                    {
                        BirimID = DersGrupHoca.OGRDersGrup.OGRDersPlan.FKFakulteBirimID.Value;
                    }

                    if (yetkiVarMi)
                    {
                        item.TarihAraligiUygunMu = yetkiVarMi;
                    }
                    else
                    {
                        EnumAkademikTarih akademikTarih = GENEL.GENELMAIN.AkademikTarihDonustur((EnumCalismaTip)item.EnumCalismaTipi);

                        bool tarihKontrol = false;

                        try
                        {
                            tarihKontrol = Sabis.Client.AkademikTakvimIslem.TarihKontrolu(akademikTarih, DersGrupHoca.OGRDersGrup.OgretimYili.Value, DersGrupHoca.OGRDersGrup.EnumDonem.Value, BirimID);
                        }
                        catch (Exception)
                        {
                            tarihKontrol = false;
                        }
                        item.TarihAraligiUygunMu = tarihKontrol;
                    }


                    if (butVarMi)
                    {
                        if (item.EnumCalismaTipi == (int)EnumCalismaTip.Butunleme && item.TarihAraligiUygunMu == true)
                        {
                            item.TarihAraligiUygunMu = true;
                        }
                        else
                        {
                            item.TarihAraligiUygunMu = false;
                        }
                    }

                    //Hazırlık derslrinin not girişleri aktif kalsın.
                    if (item.EnumDersTipi == 20 || item.EnumDersTipi == 21 || item.EnumDersTipi == 22 || item.EnumDersTipi == 29 || item.EnumDersTipi == 30 || item.EnumDersTipi == 31 || item.EnumDersTipi == 40)
                    {
                        item.TarihAraligiUygunMu = true;
                    }

                    //adamyo tarih aralığı kapat
                    if (yetkiVarMi == true && FakulteID == 339)
                    {
                        item.TarihAraligiUygunMu = true;
                    }
                }
                return payList;
            }
        }

        public static void YazOkuluPayAktar(int dersGrupHocaID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var dersHoca = db.OGRDersGrupHoca.FirstOrDefault(x => x.ID == dersGrupHocaID);
                var dersGrup = db.OGRDersGrup.FirstOrDefault(x => x.ID == dersHoca.FKDersGrupID);
                var dersPlan = db.OGRDersPlan.FirstOrDefault(x => x.FKDersPlanAnaID == dersGrup.FKDersPlanAnaID && x.Yil == dersGrup.OgretimYili);
                //var dersPlan = db.OGRDersPlan.FirstOrDefault(x => x.FKDersPlanAnaID == dersGrup.FKDersPlanAnaID && x.FKBirimID == dersGrup.FKBirimID);
                int donem = (dersPlan.Yariyil % 2) == 0 ? 2 : 1;

                var payList = db.ABSPaylar.Where(x => !x.Silindi && x.Yil == dersGrup.OgretimYili && x.EnumDonem == donem && x.FKDersPlanAnaID == dersGrup.FKDersPlanAnaID);
                var mevcutPayList = db.ABSPaylar.Where(x => !x.Silindi && x.Yil == dersGrup.OgretimYili && x.EnumDonem == (int)Sabis.Enum.EnumDonem.YAZ && x.FKDersPlanAnaID == dersGrup.FKDersPlanAnaID);

                if (mevcutPayList.Count() == 0 && dersGrup.EnumDonem == (int)Sabis.Enum.EnumDonem.YAZ)
                {
                    foreach (var pay in payList)
                    {
                        ABSPaylar yeniPay = new ABSPaylar();
                        yeniPay.Zaman = DateTime.Now;
                        yeniPay.Kullanici = "ABS_YAZOKULU";
                        yeniPay.Makina = "ABS";
                        yeniPay.AktarilanVeriMi = true;
                        yeniPay.Sira = pay.Sira;
                        yeniPay.Oran = pay.Oran;
                        yeniPay.Yil = pay.Yil;
                        yeniPay.Tarih = DateTime.Now;
                        yeniPay.EnumDonem = (int)Sabis.Enum.EnumDonem.YAZ;
                        yeniPay.FKDersPlanAnaID = pay.FKDersPlanAnaID;
                        yeniPay.EnumCalismaTip = pay.EnumCalismaTip;
                        yeniPay.Pay = pay.Pay;
                        yeniPay.Payda = pay.Pay;
                        yeniPay.FKDersGrupID = pay.FKDersGrupID;
                        yeniPay.FKDersPlanID = pay.FKDersPlanID;
                        yeniPay.FKPersonelID = pay.FKPersonelID;
                        db.ABSPaylar.Add(yeniPay);
                    }
                    db.SaveChanges();
                }

            }
        }

        public static List<PayBilgileriVM> SinavGetirPayBilgileriv2(int DersGrupHocaID, string kullanici, string IP)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var DersGrupHoca = db.OGRDersGrupHoca.FirstOrDefault(d => d.ID == DersGrupHocaID);
                int dersGrupID = DersGrupHoca.FKDersGrupID.Value;

                var payList = GetirPayBilgileri(dersGrupID).Where(p => p.EnumCalismaTipi == (int)Sabis.Enum.EnumCalismaTip.KisaSinav || p.EnumCalismaTipi == (int)Sabis.Enum.EnumCalismaTip.AraSinav || p.EnumCalismaTipi == (int)Sabis.Enum.EnumCalismaTip.Final).ToList();






                int butSayi = db.OGROgrenciYazilma.Count(x => x.FKDersGrupID == dersGrupID && x.Iptal != true && x.ButunlemeMi == true);
                bool butVarMi = butSayi > 0;
                var butPay = payList.FirstOrDefault(x => x.EnumCalismaTipi == (int)Sabis.Enum.EnumCalismaTip.Butunleme);
                if (butPay == null)
                {
                    if (butVarMi)
                    {
                        var final = payList.FirstOrDefault(x => x.EnumCalismaTipi == (int)Sabis.Enum.EnumCalismaTip.Final);
                        //Bütünleme payı yok, ekle
                        if (final == null)
                        {
                            throw new Exception("Bütünleme için baz alınacak final oranı bulunamadı!");
                        }

                        ABSPaylar butunlemePayi = new ABSPaylar();
                        butunlemePayi.FKDersPlanAnaID = final.DersPlanAnaID;
                        butunlemePayi.FKDersPlanID = final.DersPlanID;
                        butunlemePayi.EnumCalismaTip = (int)Sabis.Enum.EnumCalismaTip.Butunleme;
                        butunlemePayi.Oran = Convert.ToByte(final.KatkiOrani);
                        butunlemePayi.Sira = 1;
                        butunlemePayi.Yil = final.Yil;
                        butunlemePayi.EnumDonem = final.Donem;
                        butunlemePayi.Pay = final.KatKiOraniPay;
                        butunlemePayi.Payda = final.KatKiOraniPayda;

                        db.ABSPaylar.Add(butunlemePayi);
                        db.SaveChanges();
                        PayBilgileriVM butModel = new PayBilgileriVM();
                        butModel.EnumCalismaTipi = butunlemePayi.EnumCalismaTip;
                        butModel.Donem = butunlemePayi.EnumDonem;
                        //butModel.FKDersGrupID = butunlemePayi.FKDersGrupID;
                        butModel.DersPlanAnaID = butunlemePayi.FKDersPlanAnaID;
                        butModel.DersPlanID = butunlemePayi.FKDersPlanID;
                        //butModel.FKPersonelID = butunlemePayi.FKPersonelID;
                        butModel.PayID = butunlemePayi.ID;
                        butModel.KatkiOrani = butunlemePayi.Oran;
                        butModel.SiraNo = butunlemePayi.Sira;
                        butModel.Yil = butunlemePayi.Yil.Value;
                        butModel.ToplamOgrenciSayisi = butSayi;
                        payList.Add(butModel);
                    }
                }
                //Bütünleme payı var ancak bütünlemeye kalan öğrenci yok.
                else if (!butVarMi)
                {
                    payList.Remove(butPay);
                }
                else
                {
                    butPay.ToplamOgrenciSayisi = butSayi;
                }

                //IP yetki kontrolü
                bool yetkiVarMi = GENEL.GENELMAIN.KontrolIPYetki(kullanici, IP);
                bool sonHalVerildi = db.ABSDegerlendirme.Any(d => d.FKDersGrupID == dersGrupID && d.SilindiMi == false && d.ButunlemeMi == butVarMi && d.EnumSonHal == (int)EnumSonHal.EVET);

                foreach (var item in payList)
                {
                    int FakulteID = 0;
                    int BirimID = 21398;
                    //önce program birim id sine bakılıyor yoksa fakulte birimid si
                    if (DersGrupHoca.OGRDersGrup.OGRDersPlan.FKBolumPrBirimID.HasValue)
                    {
                        FakulteID = Sabis.Client.BirimIslem.Instance.getirBagliFakulte(DersGrupHoca.OGRDersGrup.OGRDersPlan.FKBolumPrBirimID.Value).ID;
                        BirimID = DersGrupHoca.OGRDersGrup.OGRDersPlan.FKBolumPrBirimID.Value;
                    }
                    else if (DersGrupHoca.OGRDersGrup.OGRDersPlan.FKFakulteBirimID.HasValue)
                    {
                        BirimID = DersGrupHoca.OGRDersGrup.OGRDersPlan.FKFakulteBirimID.Value;
                    }

                    if (yetkiVarMi)
                    {
                        item.TarihAraligiUygunMu = yetkiVarMi;
                    }
                    else
                    {
                        EnumAkademikTarih akademikTarih = GENEL.GENELMAIN.AkademikTarihDonustur((EnumCalismaTip)item.EnumCalismaTipi);

                        bool tarihKontrol = false;

                        try
                        {
                            tarihKontrol = Sabis.Client.AkademikTakvimIslem.TarihKontrolu(akademikTarih, DersGrupHoca.OGRDersGrup.OgretimYili.Value, DersGrupHoca.OGRDersGrup.EnumDonem.Value, BirimID);
                        }
                        catch (Exception)
                        {
                            tarihKontrol = false;
                        }
                        item.TarihAraligiUygunMu = tarihKontrol;
                    }


                    if (butVarMi)
                    {
                        if (item.EnumCalismaTipi == (int)EnumCalismaTip.Butunleme && item.TarihAraligiUygunMu == true)
                        {
                            item.TarihAraligiUygunMu = true;
                        }
                        else
                        {
                            item.TarihAraligiUygunMu = false;
                        }
                    }

                    //Hazırlık derslrinin not girişleri aktif kalsın.
                    if (item.EnumDersTipi == 20 || item.EnumDersTipi == 21 || item.EnumDersTipi == 22 || item.EnumDersTipi == 29 || item.EnumDersTipi == 30 || item.EnumDersTipi == 31 || item.EnumDersTipi == 40)
                    {
                        item.TarihAraligiUygunMu = true;
                    }

                    //adamyo tarih aralığı kapat
                    if (yetkiVarMi == true && FakulteID == 339)
                    {
                        item.TarihAraligiUygunMu = true;
                    }
                }
                return payList;
            }

        }

        public static ABSPaylar GetirPayBilgi(int FKPaylarID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return db.ABSPaylar.FirstOrDefault(x => !x.Silindi && x.ID == FKPaylarID);
            }
        }

        public static bool EklePay(PayBilgileriVM pay)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    //if (db.ABSOgrenciNot.Any(x => x.FKDersPlanAnaID == pay.DersPlanAnaID && !x.Silindi && x.Yil == pay.Yil))
                    //{
                    //    return false;
                    //}

                    var dpAna = db.OGRDersPlanAna.FirstOrDefault(x => x.ID == pay.DersPlanAnaID);

                    var grup = db.OGRDersGrup
                        .Where(x => x.ID == pay.DersGrupID && x.FKDersPlanID.HasValue && x.OGRDersPlan.FKDersPlanAnaID.HasValue)
                        .Select(x => new {
                            Yil = x.OgretimYili,
                            Donem = x.EnumDonem,
                            ID = x.ID,
                            DersPlanID = x.FKDersPlanID.Value,
                            DersPlanAnaID = x.OGRDersPlan.FKDersPlanAnaID.Value,
                            ToplamOgrenciSayisi = x.OGROgrenciYazilma.Count(d => d.Iptal == false && d.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL),
                            EnumKokDersTipi = x.OGRDersPlan.OGRDersPlanAna.EnumKokDersTipi
                        }).FirstOrDefault();

                    List<ABSPaylar> payList = db.ABSPaylar.Where(x => !x.Silindi && x.FKDersPlanAnaID == pay.DersPlanAnaID && x.Yil == pay.Yil).ToList();

                    if (grup != null && grup.EnumKokDersTipi == (int)EnumKokDersTipi.UZMANLIKALANI)
                    {
                        payList = payList.Where(x => x.FKDersPlanID.HasValue && x.FKDersPlanID.Value == grup.DersPlanID).ToList();
                    }
                    else if (pay.DersPlanID.HasValue)
                    {
                        payList = payList.Where(x => x.FKDersPlanID.HasValue && x.FKDersPlanID.Value == pay.DersPlanID).ToList();
                    }
                    else if (grup == null || (!payList.Any(x => x.FKDersPlanID == grup.DersPlanID) && grup.EnumKokDersTipi != (int)EnumKokDersTipi.UZMANLIKALANI))
                    {
                        payList = payList.Where(x => !x.FKDersPlanID.HasValue && !x.FKDersGrupID.HasValue).ToList();
                    }

                   
                    foreach (var element in System.Enum.GetValues(typeof(EnumDonem)))
                    {
                        int donem = (int)element;
                        if (donem == 0 || (donem != 1 && (dpAna.Hiyerarsik || dpAna.EnumDersSureTip == (int)Sabis.Enum.EnumDersSureTip.YILLIK))) { continue; }
                        byte siraNo = (byte)(payList.Where(x=> x.EnumDonem == donem).Count(x => x.EnumCalismaTip == pay.EnumCalismaTipi) + 1);
                        if (pay.PayID.HasValue)
                        {
                            ABSPaylar absPay = db.ABSPaylar.FirstOrDefault(d => !d.Silindi && d.ID == pay.PayID.Value);
                            absPay.EnumCalismaTip = pay.EnumCalismaTipi;
                            absPay.EnumDonem = pay.Donem;
                            absPay.FKDersPlanAnaID = pay.DersPlanAnaID;

                            absPay.FKDersGrupID = pay.DersGrupID;
                            absPay.FKDersPlanID = pay.DersPlanID;

                            if (grup.EnumKokDersTipi != (int)EnumKokDersTipi.UZMANLIKALANI)
                            {
                                absPay.FKDersGrupID = null;
                                absPay.FKDersPlanID = null;
                            }

                            absPay.FKPersonelID = pay.PersonelID;
                            absPay.Oran = (byte)pay.KatkiOrani;
                            absPay.Zaman = pay.Zaman;
                            absPay.Kullanici = pay.KullaniciAdi;
                            absPay.Makina = pay.IP;
                            if (pay.SiraNo.HasValue)
                            {
                                absPay.Sira = (byte)pay.SiraNo;
                            }
                            absPay.Tarih = DateTime.Now;
                            absPay.Yil = pay.Yil;

                            if (pay.EnumCalismaTipi == (int)EnumCalismaTip.Final)
                            {
                                var yilIcininBasariya = db.ABSPaylar.FirstOrDefault(d => !d.Silindi && d.FKDersGrupID == pay.DersGrupID && d.EnumCalismaTip == (int)EnumCalismaTip.YilIcininBasariya);
                                if (yilIcininBasariya != null)
                                {
                                    byte oran = Convert.ToByte(100 - pay.KatkiOrani);
                                    yilIcininBasariya.Oran = oran;
                                }
                            }
                            db.SaveChanges();

                        }
                        else
                        {
                            ABSPaylar yeniPay = new ABSPaylar();

                            yeniPay.EnumCalismaTip = pay.EnumCalismaTipi;
                            yeniPay.EnumDonem = donem;
                            yeniPay.FKDersGrupID = pay.DersGrupID;
                            yeniPay.FKDersPlanAnaID = pay.DersPlanAnaID;
                            yeniPay.FKDersPlanID = pay.DersPlanID;
                            yeniPay.FKPersonelID = pay.PersonelID;
                            yeniPay.Oran = (byte)pay.KatkiOrani;
                            yeniPay.Sira = siraNo;
                            yeniPay.Tarih = DateTime.Now;
                            yeniPay.Yil = pay.Yil;
                            yeniPay.Zaman = DateTime.Now;
                            yeniPay.Yayinlandi = false;
                            yeniPay.Kullanici = pay.KullaniciAdi;
                            yeniPay.Makina = pay.IP;
                            db.ABSPaylar.Add(yeniPay);

                            if (yeniPay.EnumCalismaTip == (int)EnumCalismaTip.Final && !payList.Any(d => !d.Silindi && d.EnumCalismaTip == (int)EnumCalismaTip.YilIcininBasariya && d.FKDersPlanAnaID == pay.DersPlanAnaID && d.Yil == pay.Yil && d.EnumDonem == donem))
                            {
                                byte oran = Convert.ToByte(100 - pay.KatkiOrani);
                                ABSPaylar absPayYilIci = new ABSPaylar();
                                absPayYilIci.EnumCalismaTip = (int)EnumCalismaTip.YilIcininBasariya;
                                absPayYilIci.EnumDonem = donem;
                                absPayYilIci.FKDersGrupID = pay.DersGrupID;
                                absPayYilIci.FKDersPlanAnaID = pay.DersPlanAnaID;
                                absPayYilIci.FKDersPlanID = pay.DersPlanID;
                                absPayYilIci.FKPersonelID = pay.PersonelID;
                                absPayYilIci.Oran = oran;
                                absPayYilIci.Sira = 1;
                                absPayYilIci.Tarih = DateTime.Now;
                                absPayYilIci.Yil = pay.Yil;
                                absPayYilIci.Zaman = DateTime.Now;
                                absPayYilIci.Yayinlandi = false;
                                absPayYilIci.Kullanici = pay.KullaniciAdi;
                                absPayYilIci.Makina = pay.IP;
                                db.ABSPaylar.Add(absPayYilIci);
                            }
                            db.SaveChanges();
                        }

                        
                    }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        public static bool GuncellePay(List<PayVM> payListesi)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                try
                {
                    var id = payListesi.FirstOrDefault().ID;
                    
                    ABSPaylar pay = db.ABSPaylar.FirstOrDefault(x => !x.Silindi && x.ID == id);
                    var dpAna = db.OGRDersPlanAna.FirstOrDefault(x => x.ID == pay.FKDersPlanAnaID);
                    var payList = db.ABSPaylar.Where(x => x.FKDersPlanAnaID == pay.FKDersPlanAnaID && !x.Silindi && x.Yil == pay.Yil).ToList();

                    if (pay.FKDersPlanID.HasValue)
                    {
                        payList = payList.Where(x => x.FKDersPlanID == pay.FKDersPlanID).ToList();
                    }

                    foreach (var item in payListesi)
                    {
                        var payItem = payList.FirstOrDefault(x => x.ID == item.ID);
                        foreach (var element in System.Enum.GetValues(typeof(EnumDonem)))
                        {
                            int donem = (int)element;
                            if (donem == 0 || (donem != 1 && (dpAna.Hiyerarsik || dpAna.EnumDersSureTip == (int)Sabis.Enum.EnumDersSureTip.YILLIK))) { continue; }

                            var payDuzenle = payList.FirstOrDefault(x=> x.EnumCalismaTip == payItem.EnumCalismaTip && x.EnumDonem == donem && x.Sira == payItem.Sira);

                            if (payDuzenle == null)
                            {
                                continue;
                            }

                            if (item.Oran.HasValue)
                            {
                                payDuzenle.Oran = item.Oran;
                                payDuzenle.Pay = null;
                                payDuzenle.Payda = null;
                            }
                            else
                            {
                                if (item.Pay.HasValue)
                                {
                                    payDuzenle.Pay = item.Pay;
                                }

                                if (item.Payda.HasValue)
                                {
                                    payDuzenle.Payda = item.Payda;
                                }
                            }
                            
                            
                        }
                    }
                    db.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }
        public static bool SilPay(int payID, string kullanici, string IP)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var pay = db.ABSPaylar.FirstOrDefault(x => x.ID == payID);

                    List<ABSPaylar> payYilList = db.ABSPaylar.Where(x => !x.Silindi && x.FKDersPlanAnaID == pay.FKDersPlanAnaID && x.Yil == pay.Yil && x.EnumCalismaTip == pay.EnumCalismaTip).ToList();
                    List<int> payYilIDList = payYilList.Select(x => x.ID).ToList();

                    if (db.ABSOgrenciNot.Any(x=> !x.Silindi && payYilIDList.Contains(x.FKAbsPaylarID.Value)))
                    {
                        return false;
                    }

                    foreach (var element in System.Enum.GetValues(typeof(EnumDonem)))
                    {
                        int donem = (int)element;
                        if (donem == 0 || !payYilList.Any(x=> x.EnumDonem == donem)) {  continue; }

                        List<ABSPaylar> silinecekPayList = payYilList.Where(x => x.EnumDonem == donem && x.Sira == pay.Sira).ToList();

                        foreach (var silinecekPay in silinecekPayList)
                        {
                            silinecekPay.Silindi = true;
                            silinecekPay.SilindiTarih = DateTime.Now;
                            silinecekPay.SilenKullanici = kullanici;
                            silinecekPay.SilenIP = IP;
                        }
                    }
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public static List<PayHiyerarsikVM> ListelePayHiyerarsik(int dersPlanAnaID, int yil, int donem)
        {
            List<PayHiyerarsikVM> result = null;
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var sorgu = (from p in db.ABSPaylar
                             where 
                             p.FKDersPlanAnaID == dersPlanAnaID &&
                             p.Silindi == false &&
                             ((p.OGRDersPlanAna.EnumDersSureTip == (int)EnumDersSureTip.DONEMLIK && p.Yil == yil && p.EnumDonem == donem) || (p.OGRDersPlanAna.EnumDersSureTip == (int)EnumDersSureTip.YILLIK && p.Yil == yil)) && 
                             p.EnumCalismaTip.HasValue && 
                             (p.EnumCalismaTip != (int)EnumCalismaTip.AltDersinFinalKatkisi && p.EnumCalismaTip != (int)EnumCalismaTip.AltDersinYiliciKatkisi && p.EnumCalismaTip != (int)EnumCalismaTip.AltDersinKatkisi)
                             select new PayHiyerarsikVM
                             {
                                 EnumCalismaTip = p.EnumCalismaTip,
                                 EnumDonem = p.EnumDonem,
                                 FKDersGrupID = p.FKDersGrupID,
                                 FKDersPlanAnaID = p.FKDersPlanAnaID,
                                 FKDersPlanID = p.FKDersPlanID,
                                 FKPersonelID = p.FKPersonelID,
                                 ID = p.ID,
                                 Oran = p.Oran,
                                 Sira = p.Sira,
                                 Yil = p.Yil,
                                 FKAltDersPlanAnaID = p.FKAltDersPlanAnaID,
                                 YayinlanmaDurumu = (p.ABSYayinlananPayNotlar.Where(d => d.EnumDonem == donem && d.Yil == yil && d.FKDersPlanAnaID == dersPlanAnaID).Count() > 0 ? true : false),
                             });
#if DEBUG
                var sql = sorgu.OrderBy(x => x.EnumCalismaTip).ThenBy(x => x.Sira).ToString();
#endif
                result = sorgu.ToList();
            }
            int toplamOgrenci = DERSMAIN.GetirToplamOgrenciSayi(dersPlanAnaID, yil, donem, false);
            int butunlemeToplamOgrenci = DERSMAIN.GetirToplamOgrenciSayi(dersPlanAnaID, yil, donem, true);
            foreach (var item in result)
            {
                item.PayNotAdet = NOT.NOTMAIN.GetirPayNotAdet(item.ID.Value, yil, donem);
                item.PayNotAdet.Toplam = toplamOgrenci;
                if (item.EnumCalismaTip == (int)Sabis.Enum.EnumCalismaTip.Butunleme)
                {
                    item.PayNotAdet.Toplam = butunlemeToplamOgrenci;
                }
                
            }
            return result;
        }

        public static bool YayinlaPay(int payID, int dersGrupID, string kullanici, bool mesajGonder)
        {
            bool result = false;
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var pay = db.ABSPaylar.First(x => !x.Silindi && x.ID == payID);
                    var yayinBilgisi = db.ABSYayinlananPayNotlar.FirstOrDefault(d => d.FKDersGrupID == dersGrupID && d.FKABSPayID == payID && d.Yayinlandi == true);
                    if (yayinBilgisi == null)
                    {
                        ABSYayinlananPayNotlar yayinlananNotPay = new ABSYayinlananPayNotlar();
                        yayinlananNotPay.FKDersGrupID = dersGrupID;
                        yayinlananNotPay.FKABSPayID = pay.ID;
                        yayinlananNotPay.YayinlanmaTarihi = DateTime.Now;
                        yayinlananNotPay.Yayinlandi = true;
                        yayinlananNotPay.EnumDonem = pay.EnumDonem;
                        yayinlananNotPay.Yil = pay.Yil;
                        yayinlananNotPay.FKDersPlanAnaID = pay.FKDersPlanAnaID;
                        db.ABSYayinlananPayNotlar.Add(yayinlananNotPay);
                        db.SaveChanges();
                    }
                }
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }

            if (mesajGonder)
            {
                try
                {
                    OgrencilereMesajGonder(dersGrupID, payID, kullanici);
                }
                catch (Exception)
                {
                }
            }

            return result;
        }

        private static void OgrencilereMesajGonder(int dersGrupID, int payID, string kullaniciAdi)
        {
            List<Sabis.Mesaj.Core.Istek.Mesaj> mesajList = new List<Sabis.Mesaj.Core.Istek.Mesaj>();
            string dersAd = string.Empty;
            using (UYSv2Entities db = new UYSv2Entities())
            {
                dersAd = db.OGRDersGrup.Find(dersGrupID).OGRDersPlan.OGRDersPlanAna.DersAd;
            }
            var veri = Sabis.Bolum.Bll.SOURCE.ABS.NOT.NOTMAIN.GetirOgrenciNotlari(dersGrupID, payID.ToString());
            foreach (var item in veri)
            {
                Sabis.Mesaj.Core.Istek.Mesaj mesaj = new Sabis.Mesaj.Core.Istek.Mesaj();
                mesaj.DBKaydet = false;
                string aliciKAdi = OGRENCI.OGRENCIMAIN.GetirKullaniciAdi(item.Numara);
                mesaj.Alici = new Sabis.Mesaj.Core.Istek.Alici { KullaniciAdi = aliciKAdi };
                mesaj.EnumMesajTipi = Sabis.Mesaj.Core.MesajEnum.EnumMesajTipi.NotAciklama;
                string baslik = "SABİS - Not Bildirim";
                string icerik = string.Empty;
                foreach (var notItem in item.OgrenciNotListesi)
                {
                    icerik = string.Format("{0} dersi {1}. {2} notunuz: {3}", dersAd, notItem.Sira, Sabis.Enum.Utils.GetEnumDescription((EnumCalismaTip)notItem.EnumCalismaTip), notItem.Notu);
                }
                mesaj.EPosta = new Sabis.Mesaj.Core.Istek.EPosta { Baslik = "SABİS - Not bildirim", Icerik = icerik };
                mesaj.Gonderen = new Sabis.Mesaj.Core.Istek.Gonderen { IPAdresi = Sabis.Core.Utils.Guvenlik.IPAdres, KullaniciAdi = kullaniciAdi };
                mesaj.MobilBildirim = new Sabis.Mesaj.Core.Istek.MobilBildirim { Baslik = baslik, Icerik = icerik };
                mesaj.FacebookBildirim = new Sabis.Mesaj.Core.Istek.FacebookBildirim { Baslik = icerik };
                mesajList.Add(mesaj);
            }
            Mesaj.MesajClient.Gonder(mesajList);
        }

        public static bool YayindanKaldirPay(int payID, int dersGrupID)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    foreach (var item in db.ABSYayinlananPayNotlar.Where(d => d.FKABSPayID == payID && d.FKDersGrupID == dersGrupID))
                    {
                        db.ABSYayinlananPayNotlar.Remove(item);
                    }
                    db.SaveChanges();

                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static void PayYayinlaDersplanAnaUstDers(PayVM pay)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {

                    var yayinBilgisi = db.ABSYayinlananPayNotlar.FirstOrDefault(d => d.FKDersGrupID == pay.FKDersPlanAnaID && d.FKABSPayID == pay.ID.Value && d.Yayinlandi == true);
                    if (yayinBilgisi == null)
                    {
                        ABSYayinlananPayNotlar yayinlananNotPay = new ABSYayinlananPayNotlar();
                        yayinlananNotPay.FKDersGrupID = pay.FKDersGrupID;
                        yayinlananNotPay.FKABSPayID = pay.ID.Value;
                        yayinlananNotPay.YayinlanmaTarihi = DateTime.Now;
                        yayinlananNotPay.Yayinlandi = true;
                        yayinlananNotPay.EnumDonem = pay.EnumDonem;
                        yayinlananNotPay.Yil = pay.Yil;
                        yayinlananNotPay.FKDersPlanAnaID = pay.FKDersPlanAnaID;
                        db.ABSYayinlananPayNotlar.Add(yayinlananNotPay);
                        db.SaveChanges();
                    }

                }
            }
            catch (Exception ex)
            {
                throw new Exception("Pay yayınlama işleminde hata oluştu. " + ex.Message); ;
            }
        }
        public static bool PayYayinlaDersplanAnaAltDersVeGruplari(PayVM pay)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    if (pay.FKDersPlanAnaID.HasValue)
                    {

                        //var grupListesi = db.OGRDersGrup.Where(d => d.OGRDersPlan.FKDersPlanAnaID == pay.FKDersPlanAnaID && d.OgretimYili == pay.Yil && d.EnumDonem == pay.EnumDonem).ToList();

                        var altDersListesi = db.OGRDersPlanAna.Where(x => x.FKHiyerarsikKokID == pay.FKDersPlanAnaID).Select(x => x.ID).ToList();
                        var grupListesi = db.OGRDersGrup.Where(x => altDersListesi.Contains(x.FKDersPlanAnaID.Value) && x.OgretimYili == pay.Yil && x.EnumDonem == pay.EnumDonem && x.Acik == true).ToList();

                        foreach (var item in grupListesi)
                        {
                            var yayinBilgisi = db.ABSYayinlananPayNotlar.FirstOrDefault(d => d.FKDersGrupID == pay.FKDersGrupID && d.FKABSPayID == pay.ID.Value && d.Yayinlandi == true);
                            if (yayinBilgisi == null)
                            {
                                ABSYayinlananPayNotlar yayinlananNotPay = new ABSYayinlananPayNotlar();
                                yayinlananNotPay.FKDersGrupID = item.ID;
                                yayinlananNotPay.FKABSPayID = pay.ID.Value;
                                yayinlananNotPay.YayinlanmaTarihi = DateTime.Now;
                                yayinlananNotPay.Yayinlandi = true;
                                yayinlananNotPay.EnumDonem = pay.EnumDonem;
                                yayinlananNotPay.Yil = pay.Yil;
                                yayinlananNotPay.FKDersPlanAnaID = pay.FKDersPlanAnaID;
                                db.ABSYayinlananPayNotlar.Add(yayinlananNotPay);
                            }
                            else
                            {
                                yayinBilgisi.Yayinlandi = true;
                            }
                        }
                        db.SaveChanges();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
                //throw new Exception("Pay yayınlama işleminde hata oluştu. " + ex.Message); ;
            }
        }
        public static bool PayYayindanKaldirDersPlanAnaAltDersVeGruplari(PayVM pay)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    if (pay.FKDersPlanAnaID.HasValue)
                    {
                        var altDersListesi = db.OGRDersPlanAna.Where(x => x.FKHiyerarsikKokID == pay.FKDersPlanAnaID).Select(x => x.ID).ToList();
                        var grupListesi = db.OGRDersGrup.Where(x => altDersListesi.Contains(x.FKDersPlanAnaID.Value) && x.OgretimYili == pay.Yil && x.EnumDonem == pay.EnumDonem && x.Acik == true).ToList();
                        var yayinlananGrupListesi = db.ABSYayinlananPayNotlar.Where(x => grupListesi.Select(y => y.ID).ToList().Contains(x.FKDersGrupID.Value)).ToList();

                        foreach (var item in yayinlananGrupListesi)
                        {
                            item.Yayinlandi = false;
                        }
                        db.SaveChanges();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static void PayYayindanKaldirDersPlanAnaUstDers(PayVM pay)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {

                    foreach (var item in db.ABSYayinlananPayNotlar.Where(d => d.FKABSPayID == pay.ID && d.FKDersPlanAnaID == pay.FKDersPlanAnaID))
                    {
                        db.ABSYayinlananPayNotlar.Remove(item);
                    }
                    db.SaveChanges();

                }
            }
            catch (Exception ex)
            {

                throw new Exception("Pay yayınlama işleminde hata oluştu. " + ex.Message); ;
            }
        }

        public static Dictionary<int, PayBilgileriVM> PayGetir(List<int> payIDList)
        {
            try
            {
                Dictionary<int, PayBilgileriVM> result = null;
                using (UYSv2Entities db = new UYSv2Entities())
                {

                    result = db.ABSPaylar.Where(x => !x.Silindi && payIDList.Contains(x.ID)).Select(x => new PayBilgileriVM() { PayID = x.ID, DersPlanAnaID = x.FKDersPlanAnaID, DersGrupID = x.FKDersGrupID, DersPlanID = x.FKDersPlanID, Donem = x.EnumDonem, EnumCalismaTipi = x.EnumCalismaTip, SiraNo = x.Sira, Yil = x.Yil.Value }).ToDictionary(x => x.PayID.Value, y => y);
                }
                return result;
            }
            catch (Exception ex)
            {

                throw new Exception("Pay getirme işleminde hata oluştu. " + ex.Message); ;
            }
        }

        public static PayNotAdetVM GetirPayNotAdet(int payID, int yil, int donem, int grupID = 0)
        {
            PayNotAdetVM result = new PayNotAdetVM();
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var pay = db.ABSPaylar.FirstOrDefault(x => !x.Silindi && x.ID == payID);
                int dersPlanAnaID = pay.FKDersPlanAnaID.Value;
                if (pay == null)
                {
                    throw new Exception("Geçersiz pay ID!");
                }
                result.PayID = pay.ID;

                //Ders grubu seçilerek geldiyse gruba göre
                if (grupID > 0)
                {
                    var grup = db.OGRDersGrup.First(x => x.ID == grupID);
                    result.Girilen = db.ABSOgrenciNot.Where(x => x.FKAbsPaylarID == payID && x.FKDersGrupID == grupID && x.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL && x.Silindi == false).Count();
                    result.Toplam = db.OGROgrenciYazilma.Count(x => x.FKDersGrupID == grupID && x.Iptal == false && x.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL);
                }
                //Hiyerarşik bir ders seçilerek geldiyse
                else
                {
                    result.Girilen = db.ABSOgrenciNot.Where(x => x.FKAbsPaylarID == payID && x.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL && x.Silindi == false).Select(x => x.FKOgrenciID).Distinct().Count();
                }

                return result;
            }

        }

        public static bool KontrolPayDurumuDogrumu(int FKDersGrupID, int yil, int donem)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var dersGrup = db.OGRDersGrup.FirstOrDefault(x => x.ID == FKDersGrupID);
                int payAdet = db.ABSPaylar.Count(x => !x.Silindi && x.FKDersPlanAnaID == dersGrup.FKDersPlanAnaID && x.Yil == yil && x.EnumDonem == donem &&
                                                    x.EnumCalismaTip != (int)EnumCalismaTip.YilIcininBasariya &&
                                                    x.EnumCalismaTip != (int)EnumCalismaTip.Final &&
                                                    x.EnumCalismaTip != (int)EnumCalismaTip.Butunleme);
                if ((dersGrup.Birimler.EnumSeviye == (int)EnumOgretimSeviye.ONLISANS || dersGrup.Birimler.EnumSeviye == (int)EnumOgretimSeviye.LISANS) && payAdet < 4)
                {
                    return false;
                }
                return true;
            }
        }

        public static HiyerarsikDersListesiVM GetirHiyerarsikDersPaylistesi(int fKDersPlanAnaID, int yil, int donem)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
               return db.OGRDersPlanAna.Where(x => x.ID == fKDersPlanAnaID).Select(x => new HiyerarsikDersListesiVM
                {
                    DersPlanAnaID = x.ID,
                    DersAd = x.DersAd,
                    PayListesi = x.ABSPaylar.Select(y => new PayHiyerarsikVM
                    {
                        EnumCalismaTip = y.EnumCalismaTip,
                        EnumDonem = y.EnumDonem,
                        FKDersGrupID = y.FKDersGrupID,
                        FKDersPlanAnaID = y.FKDersPlanAnaID,
                        FKDersPlanID = y.FKDersPlanID,
                        FKPersonelID = y.FKPersonelID,
                        ID = y.ID,
                        Oran = y.Oran,
                        Sira = y.Sira,
                        Yil = y.Yil,
                        FKAltDersPlanAnaID = y.FKAltDersPlanAnaID,
                        YayinlanmaDurumu = (y.ABSYayinlananPayNotlar.Where(d => d.EnumDonem == donem && d.Yil == yil && d.FKDersPlanAnaID == x.ID).Count() > 0 ? true : false),
                    }).ToList(),
                    AltDersListesi = db.OGRDersPlanAna.Where(y=> y.FKHiyerarsikKokID == x.ID).Select(y=> new AltDersListesi
                    {
                        DersPlanAnaID = y.ID,
                        DersAd = y.DersAd
                    }).ToList()
                }).FirstOrDefault();
            }
        }

        public static List<HiyerarsikPayDersListVM> GetirHiyerarsikPayList(int fkDersPlanAnaID, int yil, int donem)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var payList = db.OGRDersPlanAna.Where(x=> x.FKHiyerarsikKokID == fkDersPlanAnaID || x.ID == fkDersPlanAnaID && x.EnumDersAktif == (int)Sabis.Enum.EnumDersAktif.AKTIF).Select(x=> new HiyerarsikPayDersListVM
                {
                    DersPlanAnaID = x.ID,
                    DersAd = x.DersAd,
                    UstDersID = x.FKDersPlanAnaUstID,
                    Paylar = x.ABSPaylar.Where(y=> y.FKDersPlanAnaID == x.ID && !y.Silindi && y.Yil == yil && y.EnumDonem == donem).Select(y=> new PayVM
                    {
                        ID = y.ID,
                        EnumCalismaTip = y.EnumCalismaTip,
                        Sira = y.Sira,
                        Oran = y.Oran,
                        Yil = y.Yil,
                        EnumDonem = y.EnumDonem,
                        FKDersPlanAnaID = y.FKDersPlanAnaID,
                        AltDersPlanAnaID = y.FKAltDersPlanAnaID,
                        Pay = y.Pay,
                        Payda = y.Payda
                    }).OrderBy(y=> y.EnumCalismaTip).ThenBy(y=> y.Sira).ToList()
                }).ToList();
                return payList;
            }
        }
    }
}
