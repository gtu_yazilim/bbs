﻿using AutoMapper;
using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Bolum.Core.ABS.DERS.OSINAV;
using Sabis.Bolum.Core.ABS.DERS.SANALSINIF.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sabis.Bolum.Core.ABS.DERS.SANALSINIF.Dto;
using Sabis.Bolum.Core.ABS.DERS.SANALSINIF.Models;
using Sabis.Bolum.Core.ABS.DERS.HOME;
using Sabis.Bolum.Core.ABS.DERS.SANALSINIF.Ogrenci;
using PerculusSDK.Helpers;
using System.IO;
using System.Data;
using System.Web;
using RestSharp;
using Sabis.Bolum.Core.ENUM.ABS;
using SauLive;
using SauLive.Models.Session;
using SauLive.Models.Participants;


namespace Sabis.Bolum.Bll.SOURCE.ABS.DERS
{
    public class SanalSinifMain : ISanalSinif
    {
        IMapper _mapper;
        public SanalSinifMain(IMapper mapper)
        {
            _mapper = mapper;
        }
        public List<SanalSinifDto> SanalSinifListe(int hocaid)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var grupid = db.OGRDersGrupHoca.Where(h => h.ID == hocaid).FirstOrDefault().FKDersGrupID;
                var sorgu = (from sg in db.SanalSinifGrup
                             join ss in db.SanalSinif on sg.SSinifID equals ss.ID
                             where
                                 sg.FKDersGrupID == grupid &&
                                 ss.Silindi == false
                             select new SanalSinifDto
                             {
                                 ID = ss.ID,
                                 Ad = ss.Ad,
                                 Baslangic = ss.Baslangic,
                                 Sure = ss.Sure,
                                 Yayinlandi = ss.Yayinlandi,
                                 SorunBildirim = ss.SorunBildirim,
                                 YeniSorunBildirim = db.ABSGeriBildirim.Where(x => x.EnumModulTipi == (int)EnumModulTipi.SANAL_SINIF && x.AktiviteID == ss.ID && x.EnumGeriBildirimDurum == (int)EnumGeriBildirimDurum.YENI).Count(),
                                 OkunanSorunBildirim = db.ABSGeriBildirim.Where(x => x.EnumModulTipi == (int)EnumModulTipi.SANAL_SINIF && x.AktiviteID == ss.ID && x.EnumGeriBildirimDurum == (int)EnumGeriBildirimDurum.OKUNDU).Count(),
                                 TamamlananSorunBildirim = db.ABSGeriBildirim.Where(x => x.EnumModulTipi == (int)EnumModulTipi.SANAL_SINIF && x.AktiviteID == ss.ID && x.EnumGeriBildirimDurum == (int)EnumGeriBildirimDurum.TAMAMLANDI).Count(),
                             }).OrderBy(x => x.Baslangic).ToList();
                return sorgu;
            }
        }
        public List<HocaGruplar> HocaDegerlendirmeGrup(int personelid, int yil, int donem)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var sorgu = (from ortakdeg in db.ABSOrtakDegerlendirme
                             join ortakdeg_grup in db.ABSOrtakDegerlendirmeGrup on ortakdeg.ID equals ortakdeg_grup.FKOrtakDegerlendirmeID
                             where
                             ortakdeg.FKPersonelID == personelid &&
                             ortakdeg.Yil == yil &&
                             ortakdeg.Donem == donem &&
                             ortakdeg.Silindi == false
                             select new
                             {
                                 FKDersGrupID = ortakdeg_grup.FKDersGrupID,
                                 FKPersonelID = ortakdeg.FKPersonelID
                             });
                if (sorgu.ToList().Count() > 0)
                {
                    //degerlendirmegrubu
                    var sorgu2 = (from q in sorgu
                                  join grup in db.OGRDersGrup on q.FKDersGrupID equals grup.ID
                                  join sg in db.SanalSinifGrup on grup.ID equals sg.FKDersGrupID into tbl1
                                  from sg in tbl1.DefaultIfEmpty()
                                  select new HocaGruplar
                                  {
                                      FKDersGrupID = q.FKDersGrupID,
                                      FKPersonelID = q.FKPersonelID,
                                      GrupAd = grup.GrupAd,
                                      OgretimTuru = grup.EnumOgretimTur,
                                      Selected = sg == null ? false : true,
                                      SelectedOld = sg == null ? false : true,
                                      KoordinatorEkledi = true,
                                  }).ToList();
                    return sorgu2;

                }
                return null;
                //var sorgu = (from hocagrup in db.OGRDersGrupHoca
                //             join grup in db.OGRDersGrup on hocagrup.FKDersGrupID equals grup.ID
                //             join ortakdeg in db.ABSOrtakDegerlendirme on grup.FKDersPlanAnaID equals ortakdeg.FKDersPlanAnaID
                //             join ortakdeg_grup in db.ABSOrtakDegerlendirmeGrup on new { ORTAKDEGERLENDIRMEID = ortakdeg.ID, KULLANICI = ortakdeg.Kullanici } equals new { ORTAKDEGERLENDIRMEID = ortakdeg_grup.FKOrtakDegerlendirmeID, KULLANICI = ortakdeg_grup.Kullanici } into tbl1
                //             from ortakdeg_grup in tbl1.DefaultIfEmpty()
                //             join grup2 in db.OGRDersGrup on ortakdeg_grup.FKDersGrupID equals grup2.ID
                //             where
                //             hocagrup.ID == hocaid &&
                //             grup.OgretimYili == yil &&
                //             grup.EnumDonem == donem &&
                //             grup.Acik == true &&
                //             grup.Silindi == false
                //             select new
                //             {
                //                 FKPersonelID = ortakdeg.FKPersonelID,
                //                 FKHocaGrupID = hocagrup.ID,
                //                 FKDersPlanAnaID = grup.FKDersPlanAnaID,
                //                 OrtakDegerlendirmeID = ortakdeg == null ? 0 : ortakdeg.ID,
                //                 FKOrtakDegerlendirmeGrupID = ortakdeg_grup == null ? 0 : ortakdeg_grup.FKDersGrupID,
                //                 FKGrupID = grup.ID,
                //                 GrupAd = grup2.GrupAd,
                //                 OgretimTur = grup2.EnumOgretimTur
                //             }).ToList();
                //if (sorgu.Where(s => s.FKOrtakDegerlendirmeGrupID != 0 && s.FKPersonelID == personelid).Count() > 0)
                //{
                //    //genel degerlendirici
                //    var gruplar = (from g in sorgu
                //                   join sg in db.SanalSinifGrup on g.FKOrtakDegerlendirmeGrupID equals sg.FKDersGrupID into tbl1
                //                   from sg in tbl1.DefaultIfEmpty()
                //                   select new HocaGruplar
                //                   {
                //                       FKDersGrupID = g == null ? 0 : g.FKOrtakDegerlendirmeGrupID,
                //                       GrupAd = g.GrupAd,
                //                       OgretimTuru = g.OgretimTur,
                //                       Selected = sg == null ? false : true,
                //                       SelectedOld = sg == null ? false : true,
                //                       KoordinatorEkledi = true

                //                   }).ToList();
                //    return gruplar;
                //}
                //else
                //{
                //    //gruphoca
                //    var bilgi = (from hocagrup in db.OGRDersGrupHoca
                //                 join grup in db.OGRDersGrup on hocagrup.FKDersGrupID equals grup.ID
                //                 join dpa in db.OGRDersPlanAna on grup.FKDersPlanAnaID equals dpa.ID
                //                 where
                //                 hocagrup.ID == hocaid
                //                 select new
                //                 {
                //                     HocaID = hocagrup.ID,
                //                     GrupID = grup.ID,
                //                     AnadersID = dpa.ID,
                //                     PersonelID = hocagrup.FKPersonelID
                //                 }).FirstOrDefault(); ;

                //    var gruplar = (from grup in db.OGRDersGrup
                //                   join anaders in db.OGRDersPlanAna on grup.FKDersPlanAnaID equals anaders.ID
                //                   join gruphoca in db.OGRDersGrupHoca on grup.ID equals gruphoca.FKDersGrupID
                //                   join sg in db.OSinavGrup on grup.ID equals sg.FKDersGrupID into tbl1
                //                   from sg in tbl1.DefaultIfEmpty()
                //                   where
                //                       anaders.ID == bilgi.AnadersID &&
                //                       gruphoca.FKPersonelID == bilgi.PersonelID &&
                //                       grup.OgretimYili == yil &&
                //                       grup.EnumDonem == donem &&
                //                       grup.Acik == true &&
                //                       grup.Silindi == false
                //                   select new HocaGruplar
                //                   {
                //                       SinavID = sg == null ? 0 : sg.SinavID,
                //                       FKDersGrupID = gruphoca.FKDersGrupID,
                //                       FKPersonelID = gruphoca.FKPersonelID,
                //                       GrupAd = grup.GrupAd,
                //                       OgretimTuru = grup.EnumOgretimTur,
                //                       Selected = sg.FKDersGrupID == null ? false : true,
                //                       SelectedOld = sg.FKDersGrupID == null ? false : true,
                //                       KoordinatorEkledi = false
                //                   }).ToList();
                //    return gruplar;
                //}
                //return null;
            }
        }
        public int SanalSinifKaydet(SanalSinifDto model)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                try
                {
                    var veri = _mapper.Map<SanalSinif>(model);
                    db.SanalSinif.Add(veri);
                    db.SaveChanges();
                    return veri.ID;
                }
                catch (Exception ex)
                {

                    return 0;
                }

            }
        }

        public bool SanalSinifGrupEkle(SanalSinifDto model, int sanalsinifid, int FKDersPlanAnaID, int FKDersPlanID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var eklenecekgruplar = model.hocagrups.Where(x => x.Selected && !x.SelectedOld).Select(x =>
                    new SanalSinifGrup()
                    {
                        SSinifID = sanalsinifid,
                        FKDersPlanAnaID = FKDersPlanAnaID,
                        FKDersPlanID = FKDersPlanID,
                        FKDersGrupID = x.FKDersGrupID.Value
                    }).ToList();
                if (eklenecekgruplar.Count() > 0)
                {
                    db.SanalSinifGrup.AddRange(eklenecekgruplar);
                    db.SaveChanges();
                }
                var silinecekgruplar = model.hocagrups.Where(x => !x.Selected && x.SelectedOld).Select(x =>
                    new SanalSinifGrup()
                    {
                        SSinifID = sanalsinifid,
                        FKDersPlanAnaID = FKDersPlanAnaID,
                        FKDersPlanID = FKDersPlanID,
                        FKDersGrupID = x.FKDersGrupID.Value
                    }).ToList();
                if (silinecekgruplar.Count() > 0)
                {
                    foreach (var silgrup in silinecekgruplar)
                    {
                        var grp = db.SanalSinifGrup.Where(g => g.FKDersGrupID == silgrup.FKDersGrupID && g.SSinifID == silgrup.SSinifID).FirstOrDefault();
                        db.SanalSinifGrup.Remove(grp);
                    }
                    db.SaveChanges();
                    // eğer bütün gruplar silinmiş ve sanalsınıfı boşta kalmışsa onu da silindi yap.
                    var sanalsinifgrup = db.SanalSinifGrup.Where(s => s.SSinifID == sanalsinifid).Count();
                    if (sanalsinifgrup == 0)
                    {
                        var sanalsinif = db.SanalSinif.FirstOrDefault(s => s.ID == sanalsinifid);
                        sanalsinif.Silindi = true;
                        db.SaveChanges();
                    }
                }
                return true;
            }
        }
        public bool Katil(int ssid)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var aktivite = db.SanalSinif.Find(ssid);
                if (aktivite == null)
                {
                    return false;
                }
                var katil = new KatilVM();
                katil.Ad = aktivite.Ad;
                katil.SSinifID = aktivite.ID;

                return true;
            }


        }
        public SanalSinifDto SanalSinifGetir(int sanalsinifid)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var model = db.SanalSinif.FirstOrDefault(s => s.ID == sanalsinifid);
                return _mapper.Map<SanalSinifDto>(model);
            }
        }
        public List<SanalSinifGrupDto> SanalSinifGrupGetir(int sanalsinifid)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var siniflar = (from g in db.SanalSinifGrup
                                join gr in db.OGRDersGrup on g.FKDersGrupID equals gr.ID
                                where g.SSinifID == sanalsinifid
                                select new SanalSinifGrupDto
                                {
                                    GrupAd = gr.GrupAd,
                                    GrupID = gr.ID,
                                    OgretimTuru = gr.EnumOgretimTur
                                }).ToList();

                //var result = _mapper.Map<List<SanalSinifGrupDto>>(siniflar);
                return siniflar;
            }
        }
        public bool SanalSinifGrupSil(int sanalsinifid)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var model = db.SanalSinifGrup.Where(s => s.SSinifID == sanalsinifid).ToList();
                foreach (var m in model)
                {
                    var sinif = db.SanalSinifGrup.Where(s => s.SSinifID == m.SSinifID && s.FKDersGrupID == m.FKDersGrupID).FirstOrDefault();
                    db.SanalSinifGrup.Remove(sinif);
                }
                db.SaveChanges();
                var ssinif = db.SanalSinif.Where(s => s.ID == sanalsinifid).FirstOrDefault();
                ssinif.Silindi = true;
                db.SaveChanges();

                return true;
            }
        }
        public int SanalSinifDuzenle(SanalSinifDto model)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                try
                {
                    var sanalsinif = db.SanalSinif.FirstOrDefault(s => s.ID == model.ID);
                    sanalsinif.Ad = model.Ad;
                    sanalsinif.Baslangic = model.Baslangic;
                    sanalsinif.Sure = model.Sure;
                    sanalsinif.Bitis = model.Baslangic.AddMinutes(model.Sure);
                    sanalsinif.Yayinlandi = model.Yayinlandi;
                    sanalsinif.Guncelleyen = model.Guncelleyen;
                    sanalsinif.GuncelleyenID = model.GuncelleyenID;
                    sanalsinif.GuncellemeTarihi = model.GuncellemeTarihi;
                    db.SaveChanges();
                    return sanalsinif.ID;
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }
        public List<HocaGruplar> HocaninGruplari_Ekle(int personelid, int anadersid, int yil, int donem)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var kadi = db.PersonelBilgisi.Find(personelid).KullaniciAdi;
                var sorgu = (from hoca in db.OGRDersGrupHoca
                             join grup in db.OGRDersGrup on hoca.FKDersGrupID equals grup.ID
                             where
                             grup.FKDersPlanAnaID == anadersid &&
                             grup.Acik == true &&
                             grup.Silindi == false &&
                             (
                                hoca.FKPersonelID == personelid ||
                                hoca.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.OGRDersPlanAna4.Koordinator == kadi
                             ) &&
                             (
                                (
                                    grup.OGRDersPlan.OGRDersPlanAna.EnumDersSureTip == (int)Sabis.Enum.EnumDersSureTip.DONEMLIK &&
                                    grup.OgretimYili == yil && grup.EnumDonem == donem
                                ) ||
                                (grup.OGRDersPlan.OGRDersPlanAna.EnumDersSureTip == (int)Sabis.Enum.EnumDersSureTip.YILLIK && grup.OgretimYili == yil))
                             orderby grup.GrupAd
                             select new
                             {
                                 FKDersGrupID = grup.ID,

                             });
                var hocayaaitgruplar = (from q in sorgu
                                        join grup in db.OGRDersGrup on q.FKDersGrupID equals grup.ID
                                        join birim in db.Birimler on grup.FKBirimID equals birim.ID
                                        select new HocaGruplar
                                        {
                                            BirimAd = birim.BirimAdi,
                                            FKDersGrupID = q.FKDersGrupID,
                                            GrupAd = grup.GrupAd,
                                            OgretimTuru = grup.EnumOgretimTur
                                        }).ToList();
                //var hocayaaitgruplar = (from q in sorgu
                //                        join grup in db.OGRDersGrup on q.FKDersGrupID equals grup.ID
                //                        join sg in db.SanalSinifGrup on grup.ID equals sg.FKDersGrupID into tbl1
                //                        from sg in tbl1.DefaultIfEmpty()
                //                        select new HocaGruplar
                //                        {
                //                            FKDersGrupID = q.FKDersGrupID,
                //                            GrupAd = grup.GrupAd,
                //                            OgretimTuru = grup.EnumOgretimTur,
                //                            Selected = sg == null ? false : true,
                //                            SelectedOld = sg == null ? false : true,
                //                            KoordinatorEkledi = true,
                //                        }).ToList();

                return hocayaaitgruplar;
            }
        }
        public List<HocaGruplar> HocaninGruplari_Duzenle(int personelid, int anadersid, int yil, int donem, int sanalsinifid)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var kadi = db.PersonelBilgisi.Find(personelid).KullaniciAdi;
                var sorgu = (from hoca in db.OGRDersGrupHoca
                             join grup in db.OGRDersGrup on hoca.FKDersGrupID equals grup.ID
                             where
                             grup.FKDersPlanAnaID == anadersid &&
                             grup.Acik == true &&
                             grup.Silindi == false &&
                             (
                                hoca.FKPersonelID == personelid ||
                                hoca.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.OGRDersPlanAna4.Koordinator == kadi
                             ) &&
                             (
                                (
                                    grup.OGRDersPlan.OGRDersPlanAna.EnumDersSureTip == (int)Sabis.Enum.EnumDersSureTip.DONEMLIK &&
                                    grup.OgretimYili == yil && grup.EnumDonem == donem
                                ) ||
                                (grup.OGRDersPlan.OGRDersPlanAna.EnumDersSureTip == (int)Sabis.Enum.EnumDersSureTip.YILLIK && grup.OgretimYili == yil))
                             orderby grup.GrupAd
                             select new
                             {
                                 FKDersGrupID = grup.ID,

                             });
                var hocayaaitgruplar = (from q in sorgu
                                        join grup in db.OGRDersGrup on q.FKDersGrupID equals grup.ID
                                        join birim in db.Birimler on grup.FKBirimID equals birim.ID
                                        join sg in db.SanalSinifGrup on grup.ID equals sg.FKDersGrupID into tbl1
                                        from sg in tbl1.Where(p => p.SSinifID == sanalsinifid).DefaultIfEmpty()
                                        select new HocaGruplar
                                        {
                                            BirimAd = birim.BirimAdi,
                                            FKDersGrupID = q.FKDersGrupID,
                                            GrupAd = grup.GrupAd,
                                            OgretimTuru = grup.EnumOgretimTur,
                                            Selected = sg == null ? false : true,
                                            SelectedOld = sg == null ? false : true,
                                            KoordinatorEkledi = true,
                                        }).ToList();

                return hocayaaitgruplar;
            }
        }
        public SanalSinifSunucuDto SunucuSec(DateTime baslangic, int sure)
        {
            var baslangict = baslangic;
            var bitist = baslangic.AddMinutes(sure);
            using (UYSv2Entities db = new UYSv2Entities())
            {
                //var sorgu = (from pv in db.SanalSinifProvider
                //             join ss in db.SanalSinif on pv.PId equals ss.SunucuID into tbl1
                //             from ss in tbl1.Where(p => p.Baslangic >= baslangict && p.Bitis <= bitist).DefaultIfEmpty()
                //             where pv.Enable == true
                //             group pv by new { pv.Data, pv.PId } into Grup
                //             orderby Grup.Count() ascending
                //             select new SanalSinifSunucuDto
                //             {
                //                 SunucuAd = Grup.Key.Data,
                //                 SunucuID = Grup.Key.PId
                //             }).FirstOrDefault();
                var sorgu = (from pv in db.SanalSinifProvider
                             join ss in db.SanalSinif on pv.PId equals ss.SunucuID into tbl1
                             from ss in tbl1.Where(p =>
                             p.Baslangic >= DateTime.Now || // şimdi yada sonra başlayan -  
                             p.Bitis >= DateTime.Now // yada hala devam eden
                             ).DefaultIfEmpty()
                             where
                             pv.Enable == true
                             group pv by new { pv.Data, pv.PId } into Grup
                             orderby Grup.Count() ascending
                             select new SanalSinifSunucuDto
                             {
                                 SunucuAd = Grup.Key.Data,
                                 SunucuID = Grup.Key.PId
                             }).FirstOrDefault();
                return sorgu;
            }
        }

        public bool SanalSinifGrupKontrol(int sanalsinifid, int grupid)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var varMI = db.SanalSinifGrup.Any(x => x.SSinifID == sanalsinifid && x.FKDersGrupID == grupid);
                    if (varMI)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public bool SorunBildir(int sanalsinifid, bool deger)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var model = db.SanalSinif.FirstOrDefault(x => x.ID == sanalsinifid);
                    model.SorunBildirim = deger;
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public List<SorunBildirimDto> SorunBildirimleriniGetir(int sanalsinifid)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var liste = db.ABSGeriBildirim.Where(x => x.AktiviteID == sanalsinifid && x.EnumModulTipi == (int)EnumModulTipi.SANAL_SINIF).Select(x => new SorunBildirimDto
                {
                    AdSoyad = x.OGRKimlik.Kisi.Ad +" "+ x.OGRKimlik.Kisi.Soyad,
                    Baslik = x.Baslik,
                    Icerik = x.Icerik,
                    Tarih = x.TarihKayit,
                    Durum = (Sabis.Bolum.Core.ENUM.ABS.EnumGeriBildirimDurum)x.EnumGeriBildirimDurum,
                    SanalSinifID = x.AktiviteID,
                    GeriBildirimID = x.ID
                }).OrderBy(o => o.Durum).ToList();
                return liste;
            }
        }

        public SorunBildirimDto SorunBildirimGetirDetay(int gid)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var geriBildirim = db.ABSGeriBildirim.FirstOrDefault(x => x.ID == gid);
                geriBildirim.EnumGeriBildirimDurum = (int)EnumGeriBildirimDurum.OKUNDU;
                db.SaveChanges();

                var model = db.ABSGeriBildirim.Where(x => x.ID == gid).Select(y => new SorunBildirimDto
                {
                    AdSoyad = y.OGRKimlik.Kisi.Ad + " " + y.OGRKimlik.Kisi.Soyad,
                    Baslik = y.Baslik,
                    Icerik = y.Icerik,
                    DosyaKey = y.DosyaKey,
                    Tarih = y.TarihKayit,
                    Durum = (Sabis.Bolum.Core.ENUM.ABS.EnumGeriBildirimDurum)y.EnumGeriBildirimDurum,
                    Cevap = y.Cevap,
                    SanalSinifID = y.AktiviteID,
                    GeriBildirimID = y.ID
                }).FirstOrDefault();

                

                return model;
            }
        }

        public bool SanalSinifBildirimKaydet(int gid, string Cevap)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var model = db.ABSGeriBildirim.FirstOrDefault(x => x.ID == gid && x.EnumModulTipi == (int)EnumModulTipi.SANAL_SINIF);
                if (model != null)
                {
                    if (model.EnumGeriBildirimDurum != 2)
                    {
                        model.EnumGeriBildirimDurum = 2;
                    }
                    model.Cevap = Cevap;
                    db.SaveChanges();

                    return true;
                }
                return false;
            }
        }
    }
    public class SanalSinifApiData
    {
        public static List<OgrenciSanalSinif> SanalSinifListele(int grupID, int ogrenciID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var sorgu = (from sg in db.SanalSinifGrup
                             join ss in db.SanalSinif on sg.SSinifID equals ss.ID
                             join se in db.SanalSinifOgrenci.Where(s => s.OgrenciID == ogrenciID) on ss.ID equals se.SSinifID into tbl1
                             from se in tbl1.DefaultIfEmpty()
                             where
                             sg.FKDersGrupID == grupID &&
                             ss.Silindi == false
                             select new OgrenciSanalSinif
                             {
                                 SSinifID = ss.ID,
                                 SSinifKey = ss.SSinifKey,
                                 Ad = ss.Ad,
                                 DersAd = sg.OGRDersPlanAna.DersAd,
                                 DersGrupID = sg.FKDersGrupID,
                                 //Adres = ss.Adres,
                                 Adres = se.IzlemeKey,
                                 Baslangic = ss.Baslangic,
                                 Hafta = ss.Hafta,
                                 Sure = ss.Sure
                             }
                             ).OrderBy(x => x.Hafta).ToList();
                return sorgu;
            }
        }

        public static KatilVM OgrenciSanalSinifKatil(int sanalSinifID, int ogrenciID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var kullanici = (from ku in db.Kullanici
                                 join ki in db.Kisi on ku.FKKisiID equals ki.ID
                                 where
                                 ku.FKOgrenciID == ogrenciID
                                 select new
                                 {
                                     ki.Ad,
                                     ki.Soyad,
                                     ku.KullaniciAdi
                                 }).FirstOrDefault();

                var sanalsinif = db.SanalSinif.Find(sanalSinifID);

                var vcNow = (from vItem in db.SanalSinif
                             join en in db.SanalSinifOgrenci.Where(x => x.OgrenciID == ogrenciID) on vItem.ID
                                 equals en.SSinifID into gj
                             from en in gj.DefaultIfEmpty()
                             where vItem.ID == sanalSinifID
                             select new EnrollItem { Vitem = vItem, En = en }).FirstOrDefault();

                if (vcNow.En == null)
                {
                    if (ogrenciID != null)
                    {
                        vcNow.En = new SanalSinifOgrenci
                        {
                            KatilimIlk = DateTime.Now,
                            KatilimSon = DateTime.Now,
                            OgrenciID = ogrenciID,
                            SSinifID = sanalSinifID
                        };
                        db.SanalSinifOgrenci.Add(vcNow.En);
                        db.SaveChanges();
                    }
                }
                if (sanalsinif.ProviderType == (int)EnumSanalSinifProvider.PERCULUS)
                {
                    #region PERCULUS

                    var vc = new KatilVM();
                    var status = VCHelper.CheckDate(vcNow.Vitem.Baslangic, vcNow.Vitem.Sure);
                    var errorMessage = "";
                    switch (status)
                    {
                        case SSDurum.Bitti:
                            if (vcNow.Vitem.SSinifKey.HasValue)
                            {
                                var sResult = VCHelper.ShowReplay(vcNow.Vitem.SSinifKey, KullaniciTipEnum.Ogrenci, kullanici.Ad, kullanici.Soyad, kullanici.KullaniciAdi, vcNow.Vitem.SunucuID.Value, ref errorMessage);
                                if (sResult != null)
                                {
                                    vc.Adres = HttpUtility.UrlDecode(sResult).Replace("local.sabis.", "");
                                    vc.Durum = SSDurum.Bitti;
                                    vc.DurumMesaj = "Sanal Sınıfın Tarihi Geçti";
                                }
                                else
                                    vc.DurumMesaj = "Sanal Sınıfın Tarihi Geçti";
                            }
                            else
                            {
                                vc.Durum = SSDurum.Yapilmadi;
                                vc.DurumMesaj = "Ders Yapılmamış";
                            }

                            break;
                        case SSDurum.Basladi:
                            vc.AktifSSinifID = sanalSinifID;
                            vc.DurumMesaj = "Sanal Sınıf Başladı";
                            vc.Durum = SSDurum.Basladi;
                            if (vcNow.En != null && !string.IsNullOrEmpty(vcNow.En.IzlemeKey))
                            {
                                vc.Adres = HttpUtility.UrlDecode(vcNow.En.IzlemeKey).Replace("local.sabis.", "");
                            }
                            else if (vcNow.En != null && (string.IsNullOrEmpty(vcNow.En.IzlemeKey) && vcNow.Vitem.SSinifKey.HasValue))
                            {
                                var result = VCHelper.RegisterAttendee(KullaniciTipEnum.Ogrenci, vcNow.Vitem.SSinifKey.Value, kullanici.Ad, kullanici.Soyad, kullanici.KullaniciAdi, vcNow.Vitem.SunucuID.Value);
                                if (result != null)
                                {
                                    vcNow.En.IzlemeKey = HttpUtility.UrlDecode(result).Replace("local.sabis.", "");
                                    db.SaveChanges();
                                    vc.Adres = HttpUtility.UrlDecode(result);
                                }
                                else
                                    vc.DurumMesaj = "Kullanıcı Kayıt edilemedi";
                            }
                            else
                            {
                                if (vcNow.Vitem.SunucuID.HasValue)
                                {
                                    var dto = new SanalSinifDto();
                                    dto.ID = vcNow.Vitem.ID;
                                    dto.SunucuID = vcNow.Vitem.SunucuID;
                                    dto.SSinifKey = vcNow.Vitem.SSinifKey;
                                    dto.Adres = vcNow.Vitem.Adres;
                                    dto.Ad = vcNow.Vitem.Ad;
                                    dto.Hafta = vcNow.Vitem.Hafta;
                                    dto.Yayinlandi = vcNow.Vitem.Yayinlandi;
                                    dto.Silindi = vcNow.Vitem.Silindi;
                                    dto.Baslangic = vcNow.Vitem.Baslangic;
                                    dto.Sure = vcNow.Vitem.Sure;

                                    var result = VCHelper.VcStart(dto, KullaniciTipEnum.Ogrenci, ref errorMessage, kullanici.Ad, kullanici.Soyad, kullanici.KullaniciAdi, vcNow.Vitem.SunucuID.Value);
                                    if (vcNow.Vitem.SSinifKey.HasValue)
                                    {
                                        db.Entry(vcNow.Vitem).State = System.Data.Entity.EntityState.Modified;

                                        if (result != null)
                                        {
                                            if (vcNow.En != null) vcNow.En.IzlemeKey = result;
                                            db.SaveChanges();
                                            vc.Adres = HttpUtility.UrlDecode(result).Replace("local.sabis.", "");
                                        }
                                        else
                                        {
                                            vc.DurumMesaj = errorMessage == string.Empty ? "Kullanıcı Kayıt Edilemedi" : errorMessage;
                                            db.SaveChanges();
                                        }
                                    }
                                }

                            }
                            if (vc.Durum == SSDurum.Basladi && vc.Adres == null)
                            {
                                vc.DurumMesaj = "Sanal Sınıf Başladı, Öğretim Görevlisinin Katılması Bekleniyor.";
                            }
                            break;
                        case SSDurum.Baslamadi:
                            vc.Durum = SSDurum.Baslamadi;
                            vc.DurumMesaj = "Ders Henüz Başlamadı";
                            vc.Baslangic = vcNow.Vitem.Baslangic;
                            break;
                    }
                    vc.Ad = vcNow.Vitem.Ad;
                    vc.SSinifID = vcNow.Vitem.ID;
                    vc.Baslangic = vcNow.Vitem.Baslangic;
                    return vc;
                    #endregion
                }
                else if (sanalsinif.ProviderType == (int)EnumSanalSinifProvider.SAULIVE)
                {
                    #region SAULIVE
                    var vc = new KatilVM { SSinifID = sanalsinif.ID, Ad = sanalsinif.Ad };
                    vc.Durum = (SSDurum)SauLiveHelper.TarihKontrol(sanalsinif.Baslangic, sanalsinif.Sure);
                    switch (vc.Durum)
                    {
                        case SSDurum.Bitti:
                            if (vcNow.Vitem.SSinifKey.HasValue)
                            {
                                var Service = new SessionService(SettingsService.LiveUrl, SettingsService.UName, SettingsService.UPass);
                                var session = new SessionCreate() { };
                                session.Participants = new List<ParticipantCreate>();
                                session.Participants.Add(new ParticipantCreate()
                                {
                                    SessionId = vcNow.Vitem.SSinifKey,
                                    Code = Guid.NewGuid(),
                                    Email = kullanici.KullaniciAdi + "@sakarya.edu.tr",
                                    Name = kullanici.Ad,
                                    Surname = kullanici.Soyad,
                                    Role = "Participant"
                                });
                                var sonuc = Service.Create(session);
                                if (sonuc.Success && sonuc.SessionId.HasValue)
                                {
                                    vc.Adres = vcNow.Vitem.Adres;
                                    vc.Durum = SSDurum.Bitti;
                                    vc.DurumMesaj = "Sanal Sınıfın Tarihi Geçti";
                                }
                                else if (!sonuc.IsValid)
                                {
                                    vc.DurumMesaj = "Geçersiz veri girisi :";
                                    if (sonuc.ModelErrors.Count > 0)
                                    {
                                        foreach (var item in sonuc.ModelErrors)
                                        {
                                            vc.DurumMesaj = vc.DurumMesaj + " - " + item.Key + " " + item.Message;
                                        }
                                    }

                                }
                                else if (sonuc.Error)
                                {
                                    vc.DurumMesaj = "Hata oluştu : " + sonuc.ErrorMessage;
                                }
                            }
                            else
                            {
                                vc.Durum = SSDurum.Yapilmadi;
                                vc.DurumMesaj = "Ders Yapılmamış";
                            }
                            break;
                        case SSDurum.Basladi:
                            vc.AktifSSinifID = sanalSinifID;
                            vc.DurumMesaj = "Sanal Sınıf Başladı";
                            vc.Durum = SSDurum.Basladi;
                            if (vcNow.En != null && !string.IsNullOrEmpty(vcNow.En.IzlemeKey))
                            {
                                vc.Adres = vcNow.En.IzlemeKey;
                            }
                            else if (vcNow.En != null && (string.IsNullOrEmpty(vcNow.En.IzlemeKey) && vcNow.Vitem.SSinifKey.HasValue))
                            {
                                var Service = new SessionService(SettingsService.LiveApiUrl, SettingsService.UName, SettingsService.UPass);
                                var participant = new ParticipantCreate();
                                participant.SessionId = vcNow.Vitem.SSinifKey;
                                participant.Code = Guid.NewGuid();
                                participant.Email = kullanici.KullaniciAdi + "@sakarya.edu.tr";
                                participant.Name = kullanici.Ad;
                                participant.Surname = kullanici.Soyad;
                                participant.Role = "Participant";

                                var sonuc = Service.ParticipantCreate(participant);
                                if (sonuc.Success)
                                {
                                    vcNow.En.IzlemeKey = SettingsService.LiveUrl + "/Live/Live?Code=" + sonuc.Code;
                                    db.SaveChanges();
                                    vc.Adres = vcNow.En.IzlemeKey;
                                }
                               
                            }
                            else
                            {
                                if (vcNow.Vitem.SunucuID.HasValue)
                                {
                                    var dto = new SanalSinifDto();
                                    dto.ID = vcNow.Vitem.ID;
                                    dto.SunucuID = vcNow.Vitem.SunucuID;
                                    dto.SSinifKey = vcNow.Vitem.SSinifKey;
                                    dto.Adres = vcNow.Vitem.Adres;
                                    dto.Ad = vcNow.Vitem.Ad;
                                    dto.Hafta = vcNow.Vitem.Hafta;
                                    dto.Yayinlandi = vcNow.Vitem.Yayinlandi;
                                    dto.Silindi = vcNow.Vitem.Silindi;
                                    dto.Baslangic = vcNow.Vitem.Baslangic;
                                    dto.Sure = vcNow.Vitem.Sure;

                                    if (vcNow.Vitem.SSinifKey.HasValue)
                                    {
                                        db.Entry(vcNow.Vitem).State = System.Data.Entity.EntityState.Modified;


                                        if (vcNow.En != null) vcNow.En.IzlemeKey = sanalsinif.Adres;
                                        db.SaveChanges();
                                        vc.Adres = HttpUtility.UrlDecode(sanalsinif.Adres);


                                    }
                                }
                            }
                            if (vc.Durum == SSDurum.Basladi && vc.Adres == null)
                            {
                                vc.DurumMesaj = "Sanal Sınıf Başladı, Öğretim Görevlisinin Katılması Bekleniyor.";
                            }
                            break;
                        case SSDurum.Baslamadi:
                            vc.Durum = SSDurum.Baslamadi;
                            vc.DurumMesaj = "Ders Henüz Başlamadı";
                            vc.Baslangic = vcNow.Vitem.Baslangic;
                            break;
                        case SSDurum.Yapilmadi:
                            break;
                        default:
                            break;
                    }
                    #endregion
                    return vc;
                }
                return null;




            }
        }

        public static object SanalSinifListele(int ogrenciID, int yil, int donem)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var sorgu = (from sg in db.SanalSinifGrup
                             join ss in db.SanalSinif.Where(x => !x.Silindi) on sg.SSinifID equals ss.ID
                             join se in db.SanalSinifOgrenci.Where(s => s.OgrenciID == ogrenciID) on ss.ID equals se.SSinifID into tbl1
                             join yazilma in db.OGROgrenciYazilma.Where(x => x.FKOgrenciID == ogrenciID && !x.Iptal && x.Yil == yil && x.EnumDonem == donem) on sg.FKDersGrupID equals yazilma.FKDersGrupID
                             from se in tbl1.DefaultIfEmpty()
                             select new
                             {
                                 SSinifID = ss.ID,
                                 SSinifKey = ss.SSinifKey,
                                 Ad = ss.Ad,
                                 DersAd = sg.OGRDersPlanAna.DersAd,
                                 DersGrupID = sg.FKDersGrupID,
                                 Adres = se.IzlemeKey,
                                 Baslangic = ss.Baslangic,
                                 Hafta = ss.Hafta,
                                 Sure = ss.Sure
                             }).OrderBy(x => x.Hafta).ToList();
                return sorgu;
            }
        }
    }

    public class KatilVM
    {
        public string Ad { get; set; }
        public string Adres { get; set; }
        public int SSinifID { get; set; }
        public DateTime Baslangic { get; set; }
        public int AktifSSinifID { get; set; }
        public SSDurum Durum { get; set; }
        public string DurumMesaj { get; set; }
    }
    public class EnrollItem
    {
        public SanalSinif Vitem { get; set; }
        public SanalSinifOgrenci En { get; set; }
    }
}
