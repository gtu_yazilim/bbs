﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Bolum.Core.ABS.DERS.DERSDETAY;
using Sabis.Mesaj.Core.Istek;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Sabis.Bolum.Bll.SOURCE.ABS.DERS
{
    public class DUYURUMAIN
    {
        public static List<DuyuruVM> GetirDuyuruListesi(int dersGrupID, int personelID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var sonuc = db.ABSMesaj.Where(x => x.FKDersGrupID == dersGrupID)
                                  .Select(x => new DuyuruVM
                                  {
                                      ID = x.ID,
                                      FKPersonelID = x.FKPersonelID,
                                      FKDersGrupID = x.FKDersGrupID,
                                      Konu = x.Konu,
                                      Mesaj = x.Mesaj,
                                      Tarih = x.Tarih,
                                      Yil = x.Yil,
                                      Donem = x.Donem,
                                      EkleyenIP = x.EkleyenIP,
                                  }).ToList();
                return sonuc;
                //return db.ABSMesaj.Where(x => x.FKDersGrupID == dersGrupID)
                //                  .Select(x => new DuyuruVM
                //                  {
                //                      ID = x.ID,
                //                      FKPersonelID = x.FKPersonelID,
                //                      FKDersGrupID = x.FKDersGrupID,
                //                      Konu = x.Konu,
                //                      Mesaj = x.Mesaj,
                //                      Tarih = x.Tarih,
                //                      Yil = x.Yil,
                //                      Donem = x.Donem,
                //                      EkleyenIP = x.EkleyenIP,
                //                  }).ToList();
            }
        }

        public static void KaydetDuyuru(DuyuruVM duyuru, int fkDersGrupHocaID, int? fkOgrenciID = null, bool tumGruplaraGonderilsinmi = false, bool tumDersGruplarinaGonderilsinMi = false, bool duyuruEklensinMi = true)
        {
            if (duyuruEklensinMi)
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    ABSMesaj mesaj = new ABSMesaj();

                    mesaj.FKPersonelID = duyuru.FKPersonelID;
                    mesaj.FKDersGrupID = duyuru.FKDersGrupID;
                    mesaj.Konu = duyuru.Konu;
                    mesaj.Mesaj = duyuru.Mesaj;
                    mesaj.Tarih = duyuru.Tarih;
                    mesaj.Yil = duyuru.Yil;
                    mesaj.Donem = duyuru.Donem;
                    mesaj.EkleyenIP = duyuru.EkleyenIP;


                    db.ABSMesaj.Add(mesaj);
                    db.SaveChanges();
                    MesajGonder(duyuru, fkOgrenciID);

                }
            }

            if (tumGruplaraGonderilsinmi && !tumDersGruplarinaGonderilsinMi)
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var dgHOCA = db.OGRDersGrupHoca.FirstOrDefault(x => x.ID == fkDersGrupHocaID);
                    var liste = db.OGRDersGrupHoca.Where(x => x.OGRDersGrup.FKDersPlanAnaID == dgHOCA.OGRDersGrup.FKDersPlanAnaID && x.FKPersonelID == dgHOCA.FKPersonelID && x.OGRDersGrup.Acik == true && x.OGRDersGrup.OgretimYili == dgHOCA.OGRDersGrup.OgretimYili && x.OGRDersGrup.EnumDonem == dgHOCA.OGRDersGrup.EnumDonem).ToList();

                    foreach (var item in liste.Where(x => x.FKDersGrupID != duyuru.FKDersGrupID))
                    {
                        ABSMesaj yeniMesaj = new ABSMesaj();

                        yeniMesaj.FKPersonelID = duyuru.FKPersonelID;
                        yeniMesaj.FKDersGrupID = item.FKDersGrupID;
                        yeniMesaj.Konu = duyuru.Konu;
                        yeniMesaj.Mesaj = duyuru.Mesaj;
                        yeniMesaj.Tarih = duyuru.Tarih;
                        yeniMesaj.Yil = duyuru.Yil;
                        yeniMesaj.Donem = duyuru.Donem;
                        yeniMesaj.EkleyenIP = duyuru.EkleyenIP;

                        db.ABSMesaj.Add(yeniMesaj);
                        db.SaveChanges();

                        duyuru.FKDersGrupID = item.FKDersGrupID;
                        MesajGonder(duyuru);

                    }
                }

            }

            if (tumDersGruplarinaGonderilsinMi)
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    OGRDersGrup dersGrup = db.OGRDersGrupHoca.FirstOrDefault(x => x.ID == fkDersGrupHocaID).OGRDersGrup;
                    List<OGRDersGrupHoca> grupList = db.OGRDersGrupHoca.Where(x => x.OGRDersGrup.FKDersPlanAnaID == dersGrup.FKDersPlanAnaID && x.OGRDersGrup.Acik == true && x.OGRDersGrup.OgretimYili == dersGrup.OgretimYili && x.OGRDersGrup.EnumDonem == dersGrup.EnumDonem).ToList();
                    foreach (var item in grupList.Where(x => x.FKDersGrupID != duyuru.FKDersGrupID))
                    {
                        ABSMesaj yeniMesaj = new ABSMesaj();

                        yeniMesaj.FKPersonelID = duyuru.FKPersonelID;
                        yeniMesaj.FKDersGrupID = item.FKDersGrupID;
                        yeniMesaj.Konu = duyuru.Konu;
                        yeniMesaj.Mesaj = duyuru.Mesaj;
                        yeniMesaj.Tarih = duyuru.Tarih;
                        yeniMesaj.Yil = duyuru.Yil;
                        yeniMesaj.Donem = duyuru.Donem;
                        yeniMesaj.EkleyenIP = duyuru.EkleyenIP;

                        db.ABSMesaj.Add(yeniMesaj);
                        db.SaveChanges();

                        duyuru.FKDersGrupID = item.FKDersGrupID;
                        MesajGonder(duyuru);

                    }
                }
            }

        }

        private static void MesajGonder(DuyuruVM duyuru, int? fkOgrenciID = null)
        {
            if (duyuru.FKDersGrupID.HasValue)
            {
                try
                {
                    string kullaniciAdi;
                    string gonderenTamAd;
                    string dersAd;
                    using (UYSv2Entities db = new UYSv2Entities())
                    {
                        var grup = db.OGRDersGrup.First(x => x.ID == duyuru.FKDersGrupID.Value);
                        var hoca = db.PersonelBilgisi.First(x => x.ID == duyuru.FKPersonelID);
                        kullaniciAdi = hoca.KullaniciAdi;
                        gonderenTamAd = hoca.Kisi.Ad + " " + hoca.Kisi.Soyad;
                        dersAd = grup.OGRDersPlan.OGRDersPlanAna.DersAd;
                    }

                    var ogrenciListesi = DersIslem.GetirDersOgrenciListesi(duyuru.FKDersGrupID.Value);

                    if (fkOgrenciID != null)
                    {
                        ogrenciListesi = ogrenciListesi.Where(x => x.ID == fkOgrenciID).ToList();
                    }

                    List<Sabis.Mesaj.Core.Istek.Mesaj> mesajList = new List<Sabis.Mesaj.Core.Istek.Mesaj>();
                    foreach (var item in ogrenciListesi)
                    {
                        Sabis.Mesaj.Core.Istek.Mesaj sMesaj = new Sabis.Mesaj.Core.Istek.Mesaj();
                        sMesaj.DBKaydet = false;
                        string aliciKAdi = OGRENCI.OGRENCIMAIN.GetirKullaniciAdi(item.Numara);
                        sMesaj.Alici = new Alici { KullaniciAdi = aliciKAdi };
                        sMesaj.EnumMesajTipi = Sabis.Mesaj.Core.MesajEnum.EnumMesajTipi.DersDuyuru;
                        string baslikMesaj = string.Format("{0} dersi için yeni bir duyurunuz var", dersAd);
                        string icerikEposta = string.Format("{0}<br/>{1}<br/><br/>{2}", duyuru.Konu, duyuru.Mesaj, gonderenTamAd);
                        sMesaj.EPosta = new EPosta { Baslik = "SABİS - Ders Duyurusu", Icerik = icerikEposta, ReplyTo = kullaniciAdi + "@sakarya.edu.tr" };
                        sMesaj.Gonderen = new Gonderen { IPAdresi = duyuru.EkleyenIP, KullaniciAdi = kullaniciAdi };
                        sMesaj.MobilBildirim = new MobilBildirim { Baslik = baslikMesaj, Icerik = baslikMesaj };
                        sMesaj.FacebookBildirim = new FacebookBildirim { Baslik = baslikMesaj };
                        mesajList.Add(sMesaj);
                    }
                    Mesaj.MesajClient.Gonder(mesajList);
                }
                catch (Exception)
                {

                    throw;
                }
            }
        }
    }
}
