﻿using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Core.DTO.GrupArama;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Bll.SOURCE.ABS.DERS
{
    public class DersIslem
    {
        public static List<Core.dto.Ogrenci.OgrenciBilgiDto> GetirDersOgrenciListesi(int dersGrupID)
        {
            List<Core.dto.Ogrenci.OgrenciBilgiDto> result;
            using (UYSv2Entities db = new UYSv2Entities())
            {
                result = (from yazilma in db.OGROgrenciYazilma
                          where yazilma.FKDersGrupID == dersGrupID && yazilma.Iptal == false && yazilma.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL && yazilma.EnumDonem == yazilma.OGRDersGrup.EnumDonem
                          orderby yazilma.OGRKimlik.Numara
                          select new Sabis.Bolum.Core.dto.Ogrenci.OgrenciBilgiDto
                          {
                              Numara = yazilma.OGRKimlik.Numara,
                              Ad = yazilma.OGRKimlik.Kisi.Ad,
                              Soyad = yazilma.OGRKimlik.Kisi.Soyad,
                              ID = yazilma.OGRKimlik.ID,
                              KisiID = yazilma.OGRKimlik.FKKisiID
                          }).ToList();

                foreach (var item in result)
                {
                    item.KullaniciAdi = OGRENCI.OGRENCIMAIN.GetirKullaniciAdi(item.Numara);
                    item.Telefon = OGRENCI.OGRENCIMAIN.GetirOgrenciTelefon(item.KisiID);
                    item.FotoUrl = FotografClient.Client.FotografGetir(item.KullaniciAdi);
                }
            }
            return result;
        }

    }
}
