﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Bolum.Core.ABS.DERS.SINAV;
using Sabis.Bolum.Core.ABS.DERS.ODEV;
using Sabis.Bolum.Core.ENUM.ABS;
using Sabis.Bolum.Bll.DATA;

namespace Sabis.Bolum.Bll.SOURCE.ABS.DERS
{
    public class ODEVMAIN
    {
        public static OdevVM GetirOdev(int FKPaylarID, int FKDersGrupHocaID, int FKPersonelID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                

                var dersGrup = db.OGRDersGrupHoca.FirstOrDefault(x => x.ID == FKDersGrupHocaID).OGRDersGrup;
                //var ortakDegerlendirme = db.ABSOrtakDegerlendirmeGrup.FirstOrDefault(x => x.FKDersGrupID == dersGrup.ID).ABSOrtakDegerlendirme;

                var odevBilgi = db.ABSPaylarOlcme.FirstOrDefault(x => x.Silindi == false && x.FKPaylarID == FKPaylarID); // && x.FKABSOrtakDegerlendirmeID == ortakDegerlendirme.ID

                OOdev odevGrup = new OOdev();
                OdevVM odev = new OdevVM();

                if (db.OOdevGrup.Any(x => x.FKOGRDersGrupID == dersGrup.ID && x.OOdev.FKAbsPaylarID == FKPaylarID && x.OOdev.Silindi == false))
                {
                    odevGrup = db.OOdevGrup.FirstOrDefault(x => x.FKOGRDersGrupID == dersGrup.ID && x.OOdev.FKAbsPaylarID == FKPaylarID && x.OOdev.Silindi == false).OOdev;

                    List<OOdevGrup> odevGrupList = db.OOdevGrup.Where(x => x.FKOOdevID == odevGrup.ID).ToList();

                    odev.ID = odevGrup.ID;
                    odev.Ad = odevGrup.Ad;
                    odev.Aciklama = odevGrup.Aciklama;
                    odev.Baslangic = odevGrup.Baslangic;
                    odev.Bitis = odevGrup.Bitis;
                    odev.FKAbsPaylarID = odevGrup.FKAbsPaylarID;
                    odev.FKPersonelID = odevGrup.FKPersonelID;
                    odev.MaxYuklemeSayisi = odevGrup.MaxYuklemeSayisi;
                    odev.OdevDosyaKey = odevGrup.OdevDosyaKey;
                    odev.OdevDosyaBoyut = odevGrup.OdevDosyaBoyut;
                    odev.OdevDosyaTuru = odevGrup.OdevDosyaTuru;
                    odev.Yayinlandi = odevGrup.Yayinlandi;
                    odev.SGTarih = odevGrup.SGTarih;
                    odev.SGKullanici = odevGrup.SGKullanici;
                    odev.SGIP = odevGrup.SGIP;
                    odev.Silindi = odevGrup.Silindi;
                    odev.OnlineOdevMi = odevGrup.OnlineOdevMi;
                    odev.OrtakOdevMi = odevGrup.OrtakOdevMi;
                    odev.GenelOdevMi = odevGrup.GenelOdevMi;

                    odev.ABSPaylarOlcme = db.ABSPaylarOlcme.Where(x => x.FKPaylarID == FKPaylarID && x.Silindi == false).Select(x => new PaylarOlcmeVM // && x.FKABSOrtakDegerlendirmeID == ortakDegerlendirme.ID
                    {
                        ID = x.ID,
                        FKPaylarID = x.FKPaylarID,
                        SoruNo = x.SoruNo,
                        SoruPuan = x.SoruPuan,
                        FKKoordinatorID = x.FKKoordinatorID,
                        Zaman = x.Zaman,
                        EnumSinavTuru = x.EnumSinavTuru,
                        EnumSoruTipi = x.EnumSoruTipi,
                        ProgramCiktilari = (from pcikti in db.ABSPaylarOlcmeProgram
                                            join cikti in db.BolumYeterlilik on pcikti.FKProgramCiktiID equals cikti.ID
                                            join dcikti in db.DilBolumYeterlilik on cikti.ID equals dcikti.FKBolumYeterlilikID
                                            where pcikti.FKPaylarOlcmeID == x.ID && dcikti.EnumDilID == 1 && pcikti.ABSPaylarOlcme.Silindi == false
                                            select new PaylarOlcmeProgramCiktilariVM
                                            {
                                                ID = pcikti.ID,
                                                FKPaylarOlcmeID = pcikti.FKPaylarOlcmeID,
                                                Sira = cikti.YetID,
                                                FKProgramCiktiID = pcikti.FKProgramCiktiID,
                                                Icerik = dcikti.Icerik
                                            }).OrderBy(y => y.Sira).ToList(),
                        OgrenmeCiktilari = (from pcikti in db.ABSPaylarOlcmeOgrenme
                                            join cikti in db.DersCikti on pcikti.FKOgrenmeCiktiID equals cikti.ID
                                            where pcikti.FKPaylarOlcmeID == x.ID && pcikti.ABSPaylarOlcme.Silindi == false
                                            select new PaylarOlcmeOgrenmeCiktilariVM
                                            {
                                                ID = pcikti.ID,
                                                FKPaylarOlcmeID = pcikti.FKPaylarOlcmeID,
                                                Sira = cikti.CiktiNo,
                                                FKOgrenmeCiktiID = pcikti.FKOgrenmeCiktiID,
                                                Icerik = db.DilDersCikti.FirstOrDefault(d => d.FKDersCiktiID == cikti.ID && d.EnumDilID == 1).Icerik
                                            }).OrderBy(y => y.Sira).ToList(),
                        OOdevID = odevGrup.ID
                    }).ToList();


                    odev.DersGrupListesi = db.OGRDersGrup.Where(x => x.FKDersPlanAnaID == dersGrup.FKDersPlanAnaID && x.OgretimYili == dersGrup.OgretimYili && x.EnumDonem == dersGrup.EnumDonem).Select(x => new OdevGrupListesiVM
                    {
                        FKDersGrupID = x.ID,
                        GrupAd = x.OGRDersGrupHoca.Any() ? x.Birimler.BirimAdi + " " + x.EnumOgretimTur + " " + x.GrupAd + " Grubu" + " (" + x.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.UnvanAd + " " + x.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.Ad + " " + x.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.Soyad + ")" : "",
                        GrupEkliMi = db.OOdevGrup.Any(y => y.FKOOdevID == odev.ID && y.FKOGRDersGrupID == x.ID && y.OOdev.Silindi == false), //odevGrupList.Any(y=> y.FKOGRDersGrupID == x.ID)
                        DegisiklikYapilabilirMi = x.OGRDersGrupHoca.Any() ? x.OGRDersGrupHoca.FirstOrDefault().FKPersonelID == FKPersonelID || x.OGRDersGrupHoca.FirstOrDefault().FKPersonelID != FKPersonelID && db.OOdevGrup.Any(y => y.FKOOdevID == odev.ID && y.FKOGRDersGrupID == x.ID && y.OOdev.Silindi == false) : false,
                        //FKPersonelID = x.OGRDersGrupHoca.FirstOrDefault().FKPersonelID.Value
                    }).ToList();

                    //odev.OdevOgrenciListesi = db.OOdevOgrenci.Where(x => x.FKOOdevID == odev.ID && x.Silindi == false).Select(x => new OdevOgrenciVM
                    //{
                    //    ID = x.ID,
                    //    FKOdevID = x.FKOOdevID,
                    //    FKOgrenciID = x.FKOgrenciID,
                    //    OgrenciNumara = x.OGRKimlik.Numara,
                    //    OgrenciAdSoyad = x.OGRKimlik.Kisi.Ad + " " + x.OGRKimlik.Kisi.Soyad,
                    //    DosyaAdi = x.DosyaAdi,
                    //    DosyaKey = x.DosyaKey,
                    //    Aciklama = x.Aciklama,
                    //    YuklemeTarihi = x.YuklemeTarihi
                    //}).OrderByDescending(x=> x.YuklemeTarihi).ToList();

                    List<OOdevOgrenci> odevListe = db.OOdevOgrenci.Where(x => x.FKOOdevID == odev.ID && x.Silindi == false).ToList();

                    List<int> ogrIDList = db.OGROgrenciYazilma.Where(x => x.FKDersGrupID == dersGrup.ID && x.Iptal == false).Select(x => x.FKOgrenciID.Value).ToList();

                    odev.OdevOgrenciListesi = odevListe.Where(x=> ogrIDList.Contains(x.FKOgrenciID)).GroupBy(x => x.FKOgrenciID).Select(x => new OdevOgrenciVM
                    {
                        ID = odevListe.Where(y=> y.FKOgrenciID == x.Key).OrderByDescending(y=> y.ID).FirstOrDefault().ID,
                        FKOdevID = odevListe.Where(y => y.FKOgrenciID == x.Key).OrderByDescending(y => y.ID).FirstOrDefault().FKOOdevID,
                        FKOgrenciID = x.Key,
                        OgrenciNumara = odevListe.Where(y => y.FKOgrenciID == x.Key).OrderByDescending(y => y.ID).FirstOrDefault().OGRKimlik.Numara,
                        OgrenciAdSoyad = odevListe.Where(y => y.FKOgrenciID == x.Key).OrderByDescending(y => y.ID).FirstOrDefault().OGRKimlik.Kisi.Ad + " " + odevListe.Where(y => y.FKOgrenciID == x.Key).OrderByDescending(y => y.ID).FirstOrDefault().OGRKimlik.Kisi.Soyad,
                        DosyaAdi = odevListe.Where(y => y.FKOgrenciID == x.Key).OrderByDescending(y => y.ID).FirstOrDefault().DosyaAdi,
                        DosyaKey = odevListe.Where(y => y.FKOgrenciID == x.Key).OrderByDescending(y => y.ID).FirstOrDefault().DosyaKey,
                        Aciklama = odevListe.Where(y => y.FKOgrenciID == x.Key).OrderByDescending(y => y.ID).FirstOrDefault().Aciklama,
                        YuklemeTarihi = odevListe.Where(y => y.FKOgrenciID == x.Key).OrderByDescending(y => y.ID).FirstOrDefault().YuklemeTarihi
                    }).OrderByDescending(x=> x.YuklemeTarihi).ToList();
                        
                        
                        
                    //    .Select(x => new OdevOgrenciVM
                    //{
                    //    ID = x.ID,
                    //    FKOdevID = x.FKOOdevID,
                    //    FKOgrenciID = x.FKOgrenciID,
                    //    OgrenciNumara = x.OGRKimlik.Numara,
                    //    OgrenciAdSoyad = x.OGRKimlik.Kisi.Ad + " " + x.OGRKimlik.Kisi.Soyad,
                    //    DosyaAdi = x.DosyaAdi,
                    //    DosyaKey = x.DosyaKey,
                    //    Aciklama = x.Aciklama,
                    //    YuklemeTarihi = x.YuklemeTarihi
                    //}).OrderByDescending(x => x.YuklemeTarihi).ToList();

                    //if (ortakDegerlendirme.FKPersonelID != FKPersonelID)
                    //{
                    //    odev.DersGrupListesi = odev.DersGrupListesi.Where(x => x.FKPersonelID == FKPersonelID).ToList();
                    //}

                    //odev.DersGrupListesi = db.OOdevGrup.Where(x => x.FKOOdevID == odev.ID && x.OOdev.Silindi == false).Select(x => new OdevGrupListesiVM
                    //{
                    //    ID = x.ID,
                    //    FKDersGrupID = x.FKOGRDersGrupID,
                    //    FKOdevID = x.FKOOdevID,
                    //    GrupAd = x.OGRDersGrup.Birimler.BirimAdi + " " + x.OGRDersGrup.EnumOgretimTur + " " + x.OGRDersGrup.GrupAd + " Grubu" + " " + x.OGRDersGrup.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.UnvanAd + " " + x.OGRDersGrup.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.Ad + " " + x.OGRDersGrup.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi.Soyad
                    //}).ToList();
                }


                return odev;
            }
        }

        public static bool OdevGuncelle(OdevVM odev)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    OOdev gOdev = db.OOdevGrup.FirstOrDefault(x => x.FKOGRDersGrupID == odev.FKDersGrupID && x.OOdev.FKAbsPaylarID == odev.FKAbsPaylarID && x.OOdev.Silindi == false).OOdev;

                    if (odev.OnlineOdevMi)
                    {
                        gOdev.MaxYuklemeSayisi = 3;
                        gOdev.OnlineOdevMi = odev.OnlineOdevMi;
                    }

                    gOdev.Bitis = odev.Bitis;
                    gOdev.Baslangic = odev.Baslangic;
                    if (odev.OdevDosyaKey != null)
                    {
                        gOdev.OdevDosyaKey = odev.OdevDosyaKey;
                    }
                    gOdev.OdevDosyaBoyut = odev.OdevDosyaBoyut;
                    gOdev.OdevDosyaTuru = odev.OdevDosyaTuru;
                    gOdev.Aciklama = odev.Aciklama;

                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool OdevOlustur(OdevVM odev, int yil, int donem, int FKPersonelID)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    OGRDersGrup dersGrup = db.OGRDersGrup.FirstOrDefault(x => x.ID == odev.FKDersGrupID);
                    List<OGRDersGrup> tumGruplar = db.OGRDersGrup.Where(x => x.FKDersPlanAnaID == dersGrup.FKDersPlanAnaID && x.OgretimYili == yil && x.EnumDonem == donem && x.Acik == true).ToList();
                    ABSPaylar payBilgi = db.ABSPaylar.FirstOrDefault(x => !x.Silindi && x.ID == odev.FKAbsPaylarID);

                    if (payBilgi.FKDersPlanID.HasValue)
                    {
                        tumGruplar = tumGruplar.Where(x => x.FKDersPlanID == payBilgi.FKDersPlanID).ToList();
                    }

                    if (db.ABSPaylarOlcme.Any(x=> x.FKPaylarID == odev.FKAbsPaylarID && !x.Silindi.Value))
                    {
                        return true;
                    }

                    ABSPaylarOlcme yeniOlcme = new ABSPaylarOlcme();
                    yeniOlcme.FKPaylarID = odev.FKAbsPaylarID;
                    yeniOlcme.FKABSOrtakDegerlendirmeID = null;
                    yeniOlcme.SoruNo = 1;
                    yeniOlcme.SoruPuan = 100;
                    yeniOlcme.FKKoordinatorID = odev.FKPersonelID;
                    yeniOlcme.Zaman = DateTime.Now;
                    yeniOlcme.EnumSinavTuru = odev.EnumSinavTuru;
                    yeniOlcme.EnumSoruTipi = (int)EnumSoruTipi.KLASIK;
                    //yeniOlcme.FKOOdevID = yeniOdev.ID;
                    yeniOlcme.Silindi = false;

                    db.ABSPaylarOlcme.Add(yeniOlcme);
                    db.SaveChanges();

                    if (odev.GenelOdevMi)
                    {
                        OOdev yeniOdev = new OOdev();
                        yeniOdev.Ad = payBilgi.Sira + ". " + Sabis.Enum.Utils.GetEnumDescription((Sabis.Enum.EnumCalismaTip)payBilgi.EnumCalismaTip);
                        yeniOdev.Aciklama = odev.Aciklama;
                        yeniOdev.Baslangic = odev.Baslangic;
                        yeniOdev.Bitis = odev.Bitis;
                        yeniOdev.FKAbsPaylarID = odev.FKAbsPaylarID;
                        yeniOdev.FKPersonelID = odev.FKPersonelID;
                        yeniOdev.MaxYuklemeSayisi = 0;
                        yeniOdev.OdevDosyaKey = odev.OdevDosyaKey;
                        yeniOdev.OdevDosyaBoyut = odev.OdevDosyaBoyut;
                        yeniOdev.OdevDosyaTuru = odev.OdevDosyaTuru;
                        yeniOdev.Yayinlandi = odev.Yayinlandi;
                        yeniOdev.SGTarih = DateTime.Now;
                        yeniOdev.SGKullanici = odev.SGKullanici;
                        yeniOdev.SGIP = odev.SGIP;
                        yeniOdev.Silindi = false;
                        yeniOdev.GenelOdevMi = odev.GenelOdevMi;

                        if (odev.EnumSinavTuru == (int)EnumSinavTuru.ONLINEODEV)
                        {
                            yeniOdev.OnlineOdevMi = true;
                            yeniOdev.MaxYuklemeSayisi = 3;//odev.MaxYuklemeSayisi
                        }

                        db.OOdev.Add(yeniOdev);
                        db.SaveChanges();

                        foreach (var dersGrubu in tumGruplar)
                        {
                            OOdevGrup yeniOdevGrup = new OOdevGrup();
                            yeniOdevGrup.FKOGRDersGrupID = dersGrubu.ID;
                            yeniOdevGrup.FKOOdevID = yeniOdev.ID;
                            db.OOdevGrup.Add(yeniOdevGrup);
                            db.SaveChanges();
                        }
                    }
                    else
                    {
                        foreach (var dersGrubu in tumGruplar)
                        {
                            OOdev yeniOdev = new OOdev();
                            yeniOdev.Ad = payBilgi.Sira + ". " + Sabis.Enum.Utils.GetEnumDescription((Sabis.Enum.EnumCalismaTip)payBilgi.EnumCalismaTip);
                            yeniOdev.Aciklama = odev.Aciklama;
                            yeniOdev.Baslangic = odev.Baslangic;
                            yeniOdev.Bitis = odev.Bitis;
                            yeniOdev.FKAbsPaylarID = odev.FKAbsPaylarID;
                            yeniOdev.FKPersonelID = odev.FKPersonelID;
                            yeniOdev.MaxYuklemeSayisi = 0;
                            yeniOdev.OdevDosyaKey = odev.OdevDosyaKey;
                            yeniOdev.OdevDosyaBoyut = odev.OdevDosyaBoyut;
                            yeniOdev.OdevDosyaTuru = odev.OdevDosyaTuru;
                            yeniOdev.Yayinlandi = odev.Yayinlandi;
                            yeniOdev.SGTarih = DateTime.Now;
                            yeniOdev.SGKullanici = odev.SGKullanici;
                            yeniOdev.SGIP = odev.SGIP;
                            yeniOdev.Silindi = false;
                            yeniOdev.GenelOdevMi = odev.GenelOdevMi;

                            if (odev.EnumSinavTuru == (int)EnumSinavTuru.ONLINEODEV)
                            {
                                yeniOdev.OnlineOdevMi = true;
                                yeniOdev.MaxYuklemeSayisi = 3;//odev.MaxYuklemeSayisi
                            }

                            db.OOdev.Add(yeniOdev);
                            db.SaveChanges();

                            OOdevGrup yeniOdevGrup = new OOdevGrup();
                            yeniOdevGrup.FKOGRDersGrupID = dersGrubu.ID;
                            yeniOdevGrup.FKOOdevID = yeniOdev.ID;
                            db.OOdevGrup.Add(yeniOdevGrup);
                            db.SaveChanges();
                        }
                    }


                }

                UYSv2Entities db1 = new UYSv2Entities();
                KagitOkumaEntities db2 = new KagitOkumaEntities();

                OGRDersGrup dGrup = db1.OGRDersGrup.FirstOrDefault(x => x.ID == odev.FKDersGrupID);

                if (!db2.Sinav.Any(x=> x.FKABSPaylarID == odev.FKAbsPaylarID && !x.Silindi))
                {
                    Sinav yeniSinav = new Sinav()
                    {
                        FKDersPlanAnaID = dGrup.FKDersPlanAnaID.Value,
                        FKABSPaylarID = odev.FKAbsPaylarID,
                        FKDersGrupID = dGrup.ID,
                        EnumSinavTuru = (int)EnumSinavTuru.KLASIK,
                        SoruSayisi = 1,
                        Okunabilme = "0",
                        OlusturanKullanici = odev.SGKullanici,
                        OlusturanIP = odev.SGIP,
                        OlusturmaTarih = DateTime.Now,
                        Silindi = false,
                    };
                    db2.Sinav.Add(yeniSinav);
                    db2.SaveChanges();
                }

                db1.Dispose();
                db2.Dispose();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool OdevSil(int FKDersGrupHocaID, int FKPaylarID, string kullanici, string IP)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    OGRDersGrup dersGrup = db.OGRDersGrupHoca.FirstOrDefault(x => x.ID == FKDersGrupHocaID).OGRDersGrup;

                    if (SINAVMAIN.SoruNotGirilmisMi(FKPaylarID, dersGrup.ID, dersGrup.OgretimYili.Value, dersGrup.EnumDonem.Value))
                    {
                        return false;
                    }

                    List<ABSPaylarOlcme> OdevList = db.ABSPaylarOlcme.Where(x => x.FKPaylarID == FKPaylarID && x.Silindi == false).ToList(); // && x.FKABSOrtakDegerlendirmeID == ortakDegerlendirmeID
                    List<OOdev> OOdevList = db.OOdev.Where(x => x.FKAbsPaylarID == FKPaylarID && x.Silindi == false).ToList();

                    foreach (var item in OOdevList)
                    {
                        item.Silindi = true;
                        item.SGTarih = DateTime.Now;
                        item.SGKullanici = kullanici;
                        item.SGIP = IP;
                    }
                    db.SaveChanges();

                    foreach (var item in OdevList)
                    {
                        item.Silindi = true;
                        item.SilindiTarih = DateTime.Now;
                        item.SGKullanici = kullanici;
                        item.SGIP = IP;
                    }
                    db.SaveChanges();
                }

                using (KagitOkumaEntities db = new KagitOkumaEntities())
                {
                    Sinav sinav = db.Sinav.FirstOrDefault(x => x.FKABSPaylarID == FKPaylarID && x.Silindi == false);

                    sinav.SilenIP = IP;
                    sinav.SilenKullanici = kullanici;
                    sinav.SilindiTarih = DateTime.Now;
                    sinav.Silindi = true;

                    List<SinavKagit> kagitList = db.SinavKagit.Where(x => x.FKSinavID == sinav.ID).ToList();

                    foreach (var kagit in kagitList)
                    {
                        kagit.Silindi = true;
                        kagit.SilenKullanici = kullanici;
                        kagit.SilindiTarih = DateTime.Now;
                        kagit.SilenIP = IP;
                    }
                    db.SaveChanges();
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static List<OdevGrupListesiVM> GetirOdevGrupListesi(int FKDersGrupHocaID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                OGRDersGrup dersGrup = db.OGRDersGrupHoca.FirstOrDefault(x => x.ID == FKDersGrupHocaID).OGRDersGrup;

                int? ortakDegerlendirmeID = null;

                if (db.ABSOrtakDegerlendirmeGrup.Any(x=> x.FKDersGrupID == dersGrup.ID))
                {
                    ortakDegerlendirmeID = db.ABSOrtakDegerlendirmeGrup.FirstOrDefault(x => x.FKDersGrupID == dersGrup.ID).FKOrtakDegerlendirmeID;

                    return db.ABSOrtakDegerlendirmeGrup.Where(x => x.FKOrtakDegerlendirmeID == ortakDegerlendirmeID).Select(x => new OdevGrupListesiVM
                    {
                        ID = x.ID,
                        FKDersGrupID = x.FKDersGrupID,
                        GrupAd = x.OGRDersGrup.GrupAd
                    }).ToList();
                }
                else
                {
                    return new List<OdevGrupListesiVM>();
                }
            }
        }

        public static bool GrupEkleSil(int FKOdevID, int FKDersGrupID, bool durum)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    if (db.OOdevOgrenci.Any(x => x.FKOOdevID == FKOdevID))
                    {
                        return false;
                    }

                    if (durum)
                    {
                        OOdevGrup yeniGrup = new OOdevGrup();
                        yeniGrup.FKOOdevID = FKOdevID;
                        yeniGrup.FKOGRDersGrupID = FKDersGrupID;
                        db.OOdevGrup.Add(yeniGrup);
                    }
                    else
                    {
                        OOdevGrup odevGrup = db.OOdevGrup.FirstOrDefault(x => x.FKOOdevID == FKOdevID && x.FKOGRDersGrupID == FKDersGrupID);
                        db.OOdevGrup.Remove(odevGrup);
                    }
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool YeniSoruEkle(int fkAbsPaylarID, int kullaniciID)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    List<ABSPaylarOlcme> oncekiSorular = db.ABSPaylarOlcme.Where(x => x.FKPaylarID == fkAbsPaylarID && x.Silindi == false).ToList();
                    ABSPaylarOlcme yeniSoru = new ABSPaylarOlcme();
                    yeniSoru.FKPaylarID = fkAbsPaylarID;
                    yeniSoru.SoruNo = Convert.ToByte(oncekiSorular.Count() + 1);
                    yeniSoru.FKKoordinatorID = kullaniciID;
                    yeniSoru.EnumSinavTuru = (int)EnumSinavTuru.ODEV;
                    yeniSoru.EnumSoruTipi = (int)EnumSoruTipi.KLASIK;
                    yeniSoru.Silindi = false;
                    db.ABSPaylarOlcme.Add(yeniSoru);
                    db.SaveChanges();
                }

                using (KagitOkumaEntities db = new KagitOkumaEntities())
                {
                    var sinav = db.Sinav.FirstOrDefault(x => x.FKABSPaylarID == fkAbsPaylarID && !x.Silindi);
                    sinav.SoruSayisi = sinav.SoruSayisi + 1;
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool SoruSil (int fkAbsPaylarOlcmeID, string kullanici, string IP)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    ABSPaylarOlcme soru = db.ABSPaylarOlcme.FirstOrDefault(x => x.ID == fkAbsPaylarOlcmeID);
                    soru.Silindi = true;
                    soru.SilindiTarih = DateTime.Now;
                    soru.SGKullanici = kullanici;
                    soru.SGIP = IP;
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool SoruPuanGuncelle (int fkAbsPaylarOlcmeID, double SoruPuan)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    ABSPaylarOlcme soru = db.ABSPaylarOlcme.FirstOrDefault(x => x.ID == fkAbsPaylarOlcmeID);
                    soru.SoruPuan = SoruPuan;
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool OdevTuruDegistir(int fkAbsPaylarID, int fkDersGrupID, int enumSinavTuru)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    List<ABSPaylarOlcme> soruList = db.ABSPaylarOlcme.Where(x => x.FKPaylarID == fkAbsPaylarID && x.Silindi == false).ToList();
                    foreach (var soru in soruList)
                    {
                        soru.EnumSinavTuru = enumSinavTuru;
                    }

                    OOdev odev = db.OOdevGrup.FirstOrDefault(x => x.FKOGRDersGrupID == fkDersGrupID && x.OOdev.FKAbsPaylarID == fkAbsPaylarID && x.OOdev.Silindi == false).OOdev;

                    if (enumSinavTuru == (int)EnumSinavTuru.ODEV)
                    {
                        odev.OnlineOdevMi = false;
                    }
                    if (enumSinavTuru == (int)EnumSinavTuru.ONLINEODEV)
                    {
                        odev.OnlineOdevMi = true;
                    }
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool YuklemeHakkiVer(int fkOdevDosyaID, string kullaniciAdi, string IP, int fkDersGrupHocaID, Sabis.Bolum.Core.ABS.DERS.DERSDETAY.DuyuruVM duyuru)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var odevDosya = db.OOdevOgrenci.FirstOrDefault(x => x.ID == fkOdevDosyaID);
                    var silinecekDosya = db.OOdevOgrenci.Where(x => x.FKOgrenciID == odevDosya.FKOgrenciID && x.FKOOdevID == odevDosya.FKOOdevID && !x.Silindi).OrderByDescending(x => x.ID).First();

                    silinecekDosya.Silindi = true;
                    silinecekDosya.SilinmeTarihi = DateTime.Now;
                    silinecekDosya.SGKullanici = kullaniciAdi;
                    silinecekDosya.SGIP = IP;

                    db.SaveChanges();

                    DUYURUMAIN.KaydetDuyuru(duyuru, fkDersGrupHocaID, odevDosya.FKOgrenciID, false, false, false);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
