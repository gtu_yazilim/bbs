﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Bolum.Core.ABS.DERS.PROJE;

namespace Sabis.Bolum.Bll.SOURCE.ABS.DERS
{
    public class PROJEMAIN
    {
        public static ProjeTakvimVM GetirProjeTakvimi(int FKDersPlanAnaID,int FKDersGrupID, int yil, int donem)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return db.ABSProjeTakvim.Where(x => x.FKDersPlanAnaID == FKDersPlanAnaID && x.FKDersGrupID == FKDersGrupID && x.Yil == yil && x.EnumDonem == donem).Select(x => new ProjeTakvimVM
                {
                    ID = x.ID,
                    FKDersPlanAnaID = x.FKDersPlanAnaID,
                    Yil = x.Yil,
                    EnumDonem = x.EnumDonem,
                    Baslik = x.Baslik,
                    Aciklama = x.Aciklama,
                    MaxDosyaBoyut = x.MaxBoyut.Value,
                    RaporSayisi = x.RaporSayisi,
                    Rapor1BitisTarihi = x.Rapor1BitisTarihi,
                    Rapor2BitisTarihi = x.Rapor2BitisTarihi,
                    Rapor3BitisTarihi = x.Rapor3BitisTarihi,
                    Rapor4BitisTarihi = x.Rapor4BitisTarihi,
                    Rapor5BitisTarihi = x.Rapor5BitisTarihi,
                    Ekleyen = x.Ekleyen,
                    EklenmeTarihi = x.EklenmeTarihi,
                    EkleyenIP = x.EkleyenIP
                }).FirstOrDefault();
            }
        }
        public static ProjeTakvimVM GetirProje(int projeid)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    return db.ABSProjeTakvim.Where(x => x.ID == projeid).Select(x => new ProjeTakvimVM
                    {
                        ID = x.ID,
                        FKDersPlanAnaID = x.FKDersPlanAnaID,
                        Yil = x.Yil,
                        EnumDonem = x.EnumDonem,
                        Baslik = x.Baslik,
                        Aciklama = x.Aciklama,
                        MaxDosyaBoyut = x.MaxBoyut.Value,
                        RaporSayisi = x.RaporSayisi,
                        Rapor1BitisTarihi = x.Rapor1BitisTarihi,
                        Rapor2BitisTarihi = x.Rapor2BitisTarihi,
                        Rapor3BitisTarihi = x.Rapor3BitisTarihi,
                        Rapor4BitisTarihi = x.Rapor4BitisTarihi,
                        Rapor5BitisTarihi = x.Rapor5BitisTarihi,
                        Ekleyen = x.Ekleyen,
                        EklenmeTarihi = x.EklenmeTarihi,
                        EkleyenIP = x.EkleyenIP
                    }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public static bool EkleProje(ProjeTakvimVM model, int yil, int donem,string kullanici, string IP)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    int sayi = db.ABSProjeTakvim.Count(x => x.FKDersPlanAnaID == model.FKDersPlanAnaID && x.FKDersGrupID == model.FKDersGrupID && x.Yil == yil && x.EnumDonem == donem)+1;

                    ABSProjeTakvim takvim = new ABSProjeTakvim();
                    takvim.FKDersPlanAnaID = model.FKDersPlanAnaID;
                    takvim.FKDersGrupID = model.FKDersGrupID;
                    takvim.Baslik = model.Baslik;
                    takvim.Aciklama = model.Aciklama;
                    takvim.MaxBoyut = model.MaxDosyaBoyut;
                    takvim.Yil = yil;
                    takvim.EnumDonem = donem;
                    takvim.RaporSayisi = model.RaporSayisi.Value;
                    takvim.Rapor1BitisTarihi = model.Rapor1BitisTarihi;
                    takvim.Rapor2BitisTarihi = model.Rapor2BitisTarihi;
                    takvim.Rapor3BitisTarihi = model.Rapor3BitisTarihi;
                    takvim.Rapor4BitisTarihi = model.Rapor4BitisTarihi;
                    takvim.Rapor5BitisTarihi = model.Rapor5BitisTarihi;
                    takvim.Ekleyen = kullanici;
                    takvim.EklenmeTarihi = DateTime.Now;
                    takvim.EkleyenIP = IP;
                    db.ABSProjeTakvim.Add(takvim);
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool SilProjeTarih (int id)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var item = db.ABSProjeTakvim.FirstOrDefault(x => x.ID == id);
                    db.ABSProjeTakvim.Remove(item);
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool GuncelleProje(ProjeTakvimVM model)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var proje = db.ABSProjeTakvim.FirstOrDefault(x => x.ID == model.ID);
                    if (proje != null)
                    {
                        proje.Baslik = model.Baslik;
                        proje.Aciklama = model.Aciklama;
                        proje.RaporSayisi = model.RaporSayisi.Value;
                        proje.Rapor1BitisTarihi = model.Rapor1BitisTarihi;
                        proje.Rapor2BitisTarihi = model.Rapor2BitisTarihi;
                        proje.Rapor3BitisTarihi = model.Rapor3BitisTarihi;
                        proje.Rapor4BitisTarihi = model.Rapor4BitisTarihi;
                        proje.Rapor5BitisTarihi = model.Rapor5BitisTarihi;
                        db.SaveChanges();
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }
        #region Proje Konu
        public static List<ProjeKonu> ProjeKonuListeGetir(int projeid)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var model = (from pk in db.ABSProjeKonu
                                 where pk.ProjeID == projeid
                                 select new ProjeKonu
                                 {
                                     ID = pk.ID,
                                     ProjeID = pk.ProjeID,
                                     KonuBaslik = pk.KonuBaslik,
                                     Konu = pk.Konu,
                                     OgrenciListe = (from o in db.ABSProjeOgrenci where o.KonuID == pk.ID select new OgrenciBilgi { OgrenciID = o.OgrenciID }).ToList()
                                 }).ToList();
                    var test = model;
                    return model;

                    //var model = (from pk in db.ABSProjeKonu
                    //             join po in db.ABSProjeOgrenci on pk.ID equals po.KonuID into tbl1
                    //             from po in tbl1.DefaultIfEmpty()
                    //             select new ProjeKonu
                    //             {
                    //                 ID = pk.ID,
                    //                 ProjeID = pk.ProjeID,
                    //                 KonuBaslik = pk.KonuBaslik,
                    //                 Konu = pk.Konu,
                    //                 OgrenciListe = (from o in db.ABSProjeOgrenci where o.KonuID == pk.ID select o.OgrenciID).ToList()
                    //             }).ToList();
                    //var test = model;
                    //return model;

                    //return db.ABSProjeKonu.Where(x => x.ProjeID == projeid).Select(x => new ProjeKonu
                    //{
                    //    ID = x.ID,
                    //    ProjeID = x.ProjeID,
                    //    KonuBaslik = x.KonuBaslik,
                    //    Konu = x.Konu
                    //}).ToList();
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public static bool ProjeKonuEkle(int projeid,string konubaslik, string konu)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var model = new ABSProjeKonu();
                    model.ProjeID = projeid;
                    model.KonuBaslik = konubaslik;
                    model.Konu = konu;
                    model.Kapasite = 0;
                    model.OgrenciSayisi = 0;
                    db.ABSProjeKonu.Add(model);
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {

                return false;
            }
            
        }

        public static bool ProjeKonuDuzenle(int konuid, string konubaslik, string konu)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var projeKonu = db.ABSProjeKonu.FirstOrDefault(x => x.ID == konuid);
                    if (projeKonu != null)
                    {
                        projeKonu.KonuBaslik = konubaslik;
                        projeKonu.Konu = konu;
                        db.SaveChanges();
                        return true;
                    }
                    return false;                   
                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        public static bool ProjeKonuSil(int konuid)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var model = db.ABSProjeKonu.FirstOrDefault(x => x.ID == konuid);
                    db.ABSProjeKonu.Remove(model);
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {

                return false;
            }
        }

        public static ProjeKonu ProjeKonuGetir(int konuid)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var konu = (from k in db.ABSProjeKonu
                                where k.ID == konuid
                                select new ProjeKonu
                                {
                                    ID = k.ID,
                                    KonuBaslik = k.KonuBaslik,
                                    Konu = k.Konu
                                }).FirstOrDefault();
                    return konu;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        #endregion
        #region Proje Konu Öğrenci
        public static bool OgrenciKonuKaydet(int ProjeID, int KonuID, List<int> OgrenciListe)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    foreach (var ogrid in OgrenciListe)
                    {
                        var konuyaKayitliMi = db.ABSProjeOgrenci.Any(x => x.OgrenciID == ogrid && x.ProjeID == ProjeID && x.KonuID == KonuID);
                        if (!konuyaKayitliMi)
                        {
                            var model = new ABSProjeOgrenci();
                            model.ProjeID = ProjeID;
                            model.KonuID = KonuID;
                            model.OgrenciID = ogrid;
                            db.ABSProjeOgrenci.Add(model);
                        }
                        db.SaveChanges();
                        
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public static bool OgrenciKonuSil(int ProjeID, int KonuID, int OgrenciID)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var model = db.ABSProjeOgrenci.FirstOrDefault(x => x.ProjeID == ProjeID && x.KonuID == KonuID && x.OgrenciID == OgrenciID);
                    if (model != null)
                    {
                        db.ABSProjeOgrenci.Remove(model);
                        db.SaveChanges();
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }
        #endregion
        #region
        public static ProjeRaporDto RaporGetir(int raporid)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var rapor = new ProjeRaporDto();
                    var model = db.ABSProjeRapor.FirstOrDefault(x => x.ID == raporid);
                    if (model != null)
                    {
                        rapor.ID = model.ID;
                        rapor.FKProjeOgrenciID = model.FKProjeOgrenciID;
                        rapor.HocaAciklama = model.HocaAciklama;
                        rapor.ProjeDosyaKey = model.DosyaKey;
                        rapor.RaporDurum = model.RaporDurum;
                        rapor.RaporNo = model.RaporNo;
                    }
                    return rapor;

                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static bool RaporDuzenle(ProjeRaporDto model)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var rapor = db.ABSProjeRapor.FirstOrDefault(x => x.ID == model.ID);
                    if (rapor != null)
                    {
                        rapor.RaporDurum = model.RaporDurum;
                        rapor.HocaAciklama = model.HocaAciklama;
                        rapor.TarihHocaCevap = DateTime.Now;
                        db.SaveChanges();
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion
    }
}
