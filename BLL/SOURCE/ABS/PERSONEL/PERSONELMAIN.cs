﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Bolum.Core.ABS.PERSONEL;


namespace Sabis.Bolum.Bll.SOURCE.ABS.PERSONEL
{
    public class PERSONELMAIN
    {
        public static List<PersonelBilgiVM> AraPersonel(string aranan, List<int> birimIDList, int? yil, int? donem)
        {
            if (!birimIDList.Any())
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var personel = db.PersonelBilgisi.Where(d => d.EnumPersonelDurum != (int)Sabis.Enum.EnumPersonelDurum.PASIF && d.KullaniciAdi.Contains(aranan) || (d.Ad + " " + d.Soyad).Contains(aranan) && d.EnumPersonelDurum == (int)Sabis.Enum.EnumPersonelDurum.AKTIF);
                    if (personel != null)
                    {
                        return personel.Select(p => new PersonelBilgiVM
                        {
                            Ad = p.Ad,
                            Soyad = p.Soyad,
                            ID = p.ID,
                            Unvan = p.UnvanAd,
                            KullaniciAdi = p.KullaniciAdi,
                            UnvanAdSoyad = p.UnvanAd + " " + p.Ad + " " + p.Soyad,
                            EnumPersonelTipi = p.EnumPersonelTipi,
                            SicilNo = p.SicilNo,
                            UnvanID = p.FKUnvanID,
                            FK_GorevFakID = p.FK_GorevFakID,
                            FK_GorevBolID = p.FK_GorevBolID,
                            FK_GorevAbdID = p.FK_GorevAbdID,
                            FKGorevBirimID = p.FKGorevBirimID,
                        }).ToList();
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
            }
            else
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    List<int> personelIDList = db.OGRDersGrupHoca.Where(x => birimIDList.Contains(x.OGRDersGrup.FKBirimID) && x.OGRDersGrup.OgretimYili == yil && x.OGRDersGrup.EnumDonem == donem && x.FKPersonelID != null).Select(x=> x.FKPersonelID.Value).Distinct().ToList();

                    return db.PersonelBilgisi.Where(x => personelIDList.Contains(x.ID)).Where(d => d.KullaniciAdi.Contains(aranan) || (d.Ad + " " + d.Soyad).Contains(aranan) && d.EnumPersonelDurum == (int)Sabis.Enum.EnumPersonelDurum.AKTIF).Select(p => new PersonelBilgiVM
                    {
                        Ad = p.Ad,
                        Soyad = p.Soyad,
                        ID = p.ID,
                        Unvan = p.UnvanAd,
                        KullaniciAdi = p.KullaniciAdi,
                        UnvanAdSoyad = p.UnvanAd + " " + p.Ad + " " + p.Soyad,
                        EnumPersonelTipi = p.EnumPersonelTipi,
                        SicilNo = p.SicilNo,
                        UnvanID = p.FKUnvanID,
                        FK_GorevFakID = p.FK_GorevFakID,
                        FK_GorevBolID = p.FK_GorevBolID,
                        FK_GorevAbdID = p.FK_GorevAbdID,
                        FKGorevBirimID = p.FKGorevBirimID,
                    }).ToList();

                }
            }
        }

        public static PersonelBilgiVM GetirPersonelBilgi(string kullaniciAdi)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                if (kullaniciAdi == "ismailkoc")
                {
                    return db.PersonelBilgisi.Where(x=> x.KullaniciAdi == "ustundag").Select(x => new PersonelBilgiVM
                    {
                        Ad = x.Ad,
                        Soyad = x.Soyad,
                        ID = x.ID,
                        Unvan = x.UnvanAd,
                        KullaniciAdi = x.KullaniciAdi,
                        UnvanAdSoyad = x.UnvanAd + " " + x.Ad + " " + x.Soyad,
                        EnumPersonelTipi = x.EnumPersonelTipi,
                        SicilNo = x.SicilNo,
                        UnvanID = x.FKUnvanID,
                        FK_GorevFakID = x.FK_GorevFakID,
                        FK_GorevBolID = x.FK_GorevBolID,
                        FK_GorevAbdID = x.FK_GorevAbdID,
                        FKGorevBirimID = x.FKGorevBirimID,
                    }).First(); ;
                }

                if (db.PersonelBilgisi.Any(x => x.KullaniciAdi == kullaniciAdi && x.EnumPersonelDurum == (int)Sabis.Enum.EnumPersonelDurum.AKTIF))
                {
                    return db.PersonelBilgisi.Where(x => x.KullaniciAdi == kullaniciAdi && x.EnumPersonelDurum == (int)Sabis.Enum.EnumPersonelDurum.AKTIF)
                                         .Select(x => new PersonelBilgiVM
                                         {
                                             Ad = x.Ad,
                                             Soyad = x.Soyad,
                                             ID = x.ID,
                                             Unvan = x.UnvanAd,
                                             KullaniciAdi = x.KullaniciAdi,
                                             UnvanAdSoyad = x.UnvanAd + " " + x.Ad + " " + x.Soyad,
                                             EnumPersonelTipi = x.EnumPersonelTipi,
                                             SicilNo = x.SicilNo,
                                             UnvanID = x.FKUnvanID,
                                             FK_GorevFakID = x.FK_GorevFakID,
                                             FK_GorevBolID = x.FK_GorevBolID,
                                             FK_GorevAbdID = x.FK_GorevAbdID,
                                             FKGorevBirimID = x.FKGorevBirimID,
                                         }).First();
                }
                else
                {
                    return new PersonelBilgiVM();
                }
                
            }
        }

        public static PersonelBilgisi GetirPersonelBilgiByID(int FKPersonelID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return db.PersonelBilgisi.FirstOrDefault(x => x.ID == FKPersonelID);
            }
        }

        private static Dictionary<int, int> kullaniciPersonelDict;
        public static int GetirPersonelID(int kullaniciID)
        {
            int result = 0;
            DoldurKullaniciPersonelDict();
            kullaniciPersonelDict.TryGetValue(kullaniciID, out result);
            return result;
        }
        private static object lockObj = new object();
        private static DateTime ExpireDate;
        private static void DoldurKullaniciPersonelDict()
        {
            if (ExpireDate != null && ExpireDate < DateTime.Now)
            {
                kullaniciPersonelDict = null;
            }

            if (kullaniciPersonelDict == null)
            {
                lock (lockObj)
                {
                    if (kullaniciPersonelDict == null)
                    {
                        ExpireDate = DateTime.Now.AddHours(1);
                        kullaniciPersonelDict = new Dictionary<int, int>();
                        using (UYSv2Entities db = new UYSv2Entities())
                        {
                            kullaniciPersonelDict = db.Kullanici.Where(x => x.FKPersonelID.HasValue).ToDictionary(x => x.ID, y => y.FKPersonelID.Value);
                        }
                    }
                }
            }

        }
    }
}
