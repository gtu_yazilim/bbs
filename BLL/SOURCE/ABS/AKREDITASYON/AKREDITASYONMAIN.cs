﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Bolum.Core.ABS.AKREDITASYON;
using Sabis.Enum;


namespace Sabis.Bolum.Bll.SOURCE.ABS.AKREDITASYON
{
    public class AKREDITASYONMAIN
    {
        public static BirimDersListesiVM GetirBirimDersListesi(int birimID, int yil, int enumDonem)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                int bolumID = db.Birimler.FirstOrDefault(x => x.ID == birimID).UstBirimID.Value;

                var zorunluDersIdList = db.OGRDersPlan.Where(x => x.FKBirimID == bolumID && x.Yil == yil && x.EnumDonem == enumDonem && x.EnumDersSecimTipi == (int)Sabis.Enum.EnumDersSecimTipi.ZORUNLU && (x.EnumDersBirimTipi == (int)Sabis.Enum.EnumDersBirimTipi.BOLUM || x.EnumDersBirimTipi == (int)Sabis.Enum.EnumDersBirimTipi.BOS) && x.FKDersPlanAnaID != 28511).Select(x => x.FKDersPlanAnaID).Distinct();
                var secmeliDersIdList = db.OGRDersPlan.Where(x => x.FKBirimID == bolumID && x.Yil == yil && x.EnumDonem == enumDonem && x.EnumDersSecimTipi == (int)Sabis.Enum.EnumDersSecimTipi.SECMELI && (x.EnumDersBirimTipi == (int)Sabis.Enum.EnumDersBirimTipi.BOLUM || x.EnumDersBirimTipi == (int)Sabis.Enum.EnumDersBirimTipi.BOS)).Select(x => x.FKDersPlanAnaID).Distinct();

                //var dersIDList = db.OGRDersPlan.Where(o => o.FKBolumPrBirimID == birimID && o.Yil == yil && o.EnumDonem == enumDonem).Select(o => o.FKDersPlanAnaID).Distinct();

                
                int birimciOgretimID = db.Birimler.FirstOrDefault(x => x.UstBirimID == bolumID && x.EnumOgrenimTuru == (int)Sabis.Enum.EnumOgretimTur.OGRETIM_1).ID;

                BirimDersListesiVM dersListe = new BirimDersListesiVM();

                List<BirimDersListVM> zorunluDersList = db.OGRDersPlanAna.Where(x => zorunluDersIdList.Contains(x.ID) &&
                                                                          x.EnumKokDersTipi != (int)Sabis.Enum.EnumKokDersTipi.BITIRME &&
                                                                          x.EnumKokDersTipi != (int)Sabis.Enum.EnumKokDersTipi.STAJ &&
                                                                          x.EnumKokDersTipi != (int)Sabis.Enum.EnumKokDersTipi.TASARIM &&
                                                                          x.EnumKokDersTipi != (int)Sabis.Enum.EnumKokDersTipi.HAZIRLIK &&
                                                                          x.EnumDersAktif == (int)Sabis.Enum.EnumDersAktif.AKTIF)
                .Select(x => new BirimDersListVM
                {
                    ID = x.ID,
                    BirimID = birimID,
                    Yil = yil,
                    EnumDonem = enumDonem,
                    DersAd = x.DersAd,
                    DersPCList = db.DersYeterlilikIliski.Where(y => y.OGRDersPlan.FKDersPlanAnaID == x.ID && y.Yil == yil && y.FKProgramBirimID == birimciOgretimID && y.Silindi == false).OrderBy(y => y.BolumYeterlilik.YetID).Select(y => new BirimPCListe
                    {
                        BirimID = birimID,
                        ID = y.FKYeterlilikID.Value,
                        PCNo = y.BolumYeterlilik.YetID.Value,
                        PCIcerik = y.BolumYeterlilik.DilBolumYeterlilik.FirstOrDefault(z => z.FKBolumYeterlilikID == y.FKYeterlilikID).Icerik,
                        KatkiDuzey = y.KatkiDuzeyi
                    }).ToList(),
                    DersAKTS = x.ECTSKredi
                }).OrderBy(x => x.DersAd).ToList();

                List<BirimDersListVM> secmeliDersList = db.OGRDersPlanAna.Where(x => secmeliDersIdList.Contains(x.ID) &&
                                                                          x.EnumKokDersTipi != (int)Sabis.Enum.EnumKokDersTipi.BITIRME &&
                                                                          x.EnumKokDersTipi != (int)Sabis.Enum.EnumKokDersTipi.STAJ &&
                                                                          x.EnumKokDersTipi != (int)Sabis.Enum.EnumKokDersTipi.TASARIM &&
                                                                          x.EnumKokDersTipi != (int)Sabis.Enum.EnumKokDersTipi.HAZIRLIK &&
                                                                          x.EnumDersAktif == (int)Sabis.Enum.EnumDersAktif.AKTIF)
                .Select(x => new BirimDersListVM
                {
                    ID = x.ID,
                    BirimID = birimID,
                    Yil = yil,
                    EnumDonem = enumDonem,
                    DersAd = x.DersAd,
                    DersPCList = db.DersYeterlilikIliski.Where(y => y.OGRDersPlan.FKDersPlanAnaID == x.ID && y.Yil == yil && y.FKProgramBirimID == birimciOgretimID && y.Silindi == false).OrderBy(y => y.BolumYeterlilik.YetID).Select(y => new BirimPCListe
                    {
                        BirimID = birimID,
                        ID = y.FKYeterlilikID.Value,
                        PCNo = y.BolumYeterlilik.YetID.Value,
                        PCIcerik = y.BolumYeterlilik.DilBolumYeterlilik.FirstOrDefault(z => z.FKBolumYeterlilikID == y.FKYeterlilikID).Icerik,
                        KatkiDuzey = y.KatkiDuzeyi
                    }).ToList(),
                    DersAKTS = x.ECTSKredi
                }).OrderBy(x => x.DersAd).ToList();

                dersListe.ZorunluDersListe = zorunluDersList;
                dersListe.SecmeliDersListe = secmeliDersList;
                return dersListe;
            }
        }

        public static List<int> GetirBirimDersIDListe(int birimID, int yil)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return db.OGRDersPlan.Where(x => x.FKBirimID == birimID && x.Yil == yil && 
                                                 x.OGRDersPlanAna.EnumKokDersTipi != (int)EnumKokDersTipi.BITIRME &&
                                                 x.OGRDersPlanAna.EnumKokDersTipi != (int)EnumKokDersTipi.STAJ &&
                                                 x.OGRDersPlanAna.EnumKokDersTipi != (int)EnumKokDersTipi.TASARIM &&
                                                 x.OGRDersPlanAna.EnumKokDersTipi != (int)EnumKokDersTipi.HAZIRLIK &&
                                                 x.OGRDersPlanAna.EnumDersAktif == (int)EnumDersAktif.AKTIF
                                                ).Select(x => x.FKDersPlanAnaID.Value).Distinct().ToList();
            }
        }

        public static List<BirimPCListe> GetirBirimPCList(int birimID, int yil)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                int bolumID = db.Birimler.FirstOrDefault(x => x.ID == birimID).UstBirimID.Value;
                int birinciOgretimID = db.Birimler.FirstOrDefault(x => x.UstBirimID == bolumID && x.EnumOgrenimTuru == (int)Sabis.Enum.EnumOgretimTur.OGRETIM_1).ID;
                return db.DilBolumYeterlilik.Where(x => x.BolumYeterlilik.FKProgramBirimID == birinciOgretimID && x.BolumYeterlilik.Yil == yil && x.EnumDilID == 1).Select(x => new BirimPCListe
                {
                    ID = x.BolumYeterlilik.ID,
                    BirimID = birimID,
                    PCNo = (int)x.BolumYeterlilik.YetID,
                    PCIcerik = x.Icerik
                }).ToList();
            }
        }
        public static List<DersOCListe> GetirDersOCList(int dersPlanAnaID, int yil)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return db.DilDersCikti.Where(x => x.DersCikti.FKDersPlanAnaID == dersPlanAnaID && x.DersCikti.Yil == yil && x.EnumDilID == 1).Select(x => new DersOCListe
                {
                    ID = x.DersCikti.ID,
                    DersPlanAnaID = x.DersCikti.FKDersPlanAnaID.Value,
                    OCNo = (int)x.DersCikti.CiktiNo,
                    OCIcerik = x.Icerik
                }).ToList();
            }
        }
        public static List<PCVM> GetirPCDonemOrtalama(int birimID, int yil, int enumDonem)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                int bolumID = db.Birimler.FirstOrDefault(x => x.ID == birimID).UstBirimID.Value;
                int birinciOgretimID = db.Birimler.FirstOrDefault(x => x.UstBirimID == bolumID && x.EnumOgrenimTuru == (int)Sabis.Enum.EnumOgretimTur.OGRETIM_1).ID;
                int enumOgretimTuru = db.Birimler.FirstOrDefault(x => x.ID == birimID).EnumOgrenimTuru.Value;

                var DPList = db.OGRDersPlan.Where(x => x.FKBirimID == birimID && x.Yil == yil && x.EnumDonem == enumDonem).Select(x => x.FKDersPlanAnaID).ToList();

                var DersList = db.OGRDersPlanAna.Where(x => DPList.Contains(x.ID) && x.EnumDersPlanAnaZorunlu == (int)Sabis.Enum.EnumDersPlanZorunlu.ZORUNLU).ToList();

                var DersIDList = DersList.Select(x => x.ID).ToList();

                var NotList = db.ABSOgrenciNotDetay.Where(x => DersIDList.Contains(x.ABSPaylarOlcme.ABSPaylar.OGRDersPlanAna.ID) && x.Silindi == false).ToList();

                List<PCVM> pcList = db.DilBolumYeterlilik.Where(x => x.BolumYeterlilik.FKProgramBirimID == birinciOgretimID && x.BolumYeterlilik.Yil == yil && x.EnumDilID == 1).Select(x => new PCVM
                {
                    ID = x.BolumYeterlilik.ID,
                    BirimID = birimID,
                    PCNo = x.BolumYeterlilik.YetID.ToString(),

                }).ToList();

                var pcIDList = pcList.Select(x => x.ID).ToList();

                var sorulist = db.ABSPaylarOlcmeProgram.Where(x => pcIDList.Contains(x.FKProgramCiktiID.Value)).ToList();

                //var OGRliste = NotList.Select(x => x.FKOgrenciID).Distinct();

                List<PCVM> result = new List<PCVM>();

                foreach (var item in pcList)
                {


                    var pcSoruList = sorulist.Where(x => x.FKProgramCiktiID.Value == item.ID).ToList();
                    var pcSoruIDList = sorulist.Where(x => x.FKProgramCiktiID.Value == item.ID).Select(x => x.FKPaylarOlcmeID).ToList();

                    //var soruPuanOrtalama = pcSoruList.Where(x => x.FKProgramCiktiID.Value == item.ID).Average(x => x.ABSPaylarOlcme.SoruPuan);
                    var soruPuanToplam = pcSoruList.Where(x => x.FKProgramCiktiID.Value == item.ID).Sum(x => x.ABSPaylarOlcme.SoruPuan);


                    //var OGRliste = NotList.Where(x => pcSoruIDList.Contains(x.FKAbsPaylarOlcmeID.Value) && x.Notu != 120 && x.Notu != 130).Select(x=>x.FKOgrenciID).Distinct().ToList();

                    PCVM yeniPC = new PCVM();
                    yeniPC.ID = item.ID;
                    yeniPC.BirimID = item.BirimID;
                    yeniPC.PCNo = "PÇ " + item.PCNo;
                    //var nt = NotList.Where(x => pcSoruIDList.Contains(x.FKAbsPaylarOlcmeID.Value) && x.Notu != 120 && x.Notu != 130).Sum(x => x.Notu.Value);

                    //var ort = soruPuanOrtalama.Value;

                    yeniPC.PCBasariYuzde = Math.Round((NotList.Where(x => pcSoruIDList.Contains(x.FKAbsPaylarOlcmeID.Value) && x.Notu != 120 && x.Notu != 130).Sum(x => x.Notu.Value) / soruPuanToplam.Value) / 100, 2);
                    result.Add(yeniPC);
                }


                return result;
            }
        }
        public static List<ToplamSonucVM> GetirSoruToplamSonuc(int dersPlanAnaID, int yil, int enumDonem, int birimID)
        {
            List<ToplamSonucVM> result = new List<ToplamSonucVM>();
            using (UYSv2Entities db = new UYSv2Entities())
            {
                int enumOgretimTuru = db.Birimler.FirstOrDefault(x => x.ID == birimID).EnumOgrenimTuru.Value;


                var dersGruplari = db.OGRDersGrup.Where(x => x.FKDersPlanAnaID == dersPlanAnaID && x.OgretimYili == yil && x.EnumDonem == enumDonem && x.Acik && !x.Silindi); // && x.OGRDersPlan.EnumOgretimTur == enumOgretimTuru

                var dersGrupIDList = dersGruplari.Select(x => x.ID).ToList();

                List<int> OGRliste = db.ABSBasariNot.Where(x => dersGrupIDList.Contains(x.FKDersGrupID.Value) && x.SilindiMi == false)
                                                    .Where(x => x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.DC &&
                                                                x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.DD &&
                                                                x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.DZ &&
                                                                x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.FF &&
                                                                x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.GR &&
                                                                x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.YT &&
                                                                x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.YZ)
                                                    .Select(x => x.FKOgrenciID.Value).ToList();

                //List<int> OGRlisteTumu = new List<int>();

                //List<int> payIDlist = db.ABSPaylar.Where(x => x.FKDersPlanAnaID == dersPlanAnaID && x.Yil == yil && x.EnumDonem == enumDonem && x.EnumCalismaTip != (int)Sabis.Enum.EnumCalismaTip.YilIcininBasariya).Select(x => x.ID).ToList();

                var payListe = db.ABSPaylar.Where(x => !x.Silindi && x.FKDersPlanAnaID == dersPlanAnaID && x.Yil == yil && x.EnumDonem == enumDonem && x.EnumCalismaTip != (int)Sabis.Enum.EnumCalismaTip.YilIcininBasariya).ToList();
                List<int> payIDlist = payListe.Select(x => x.ID).ToList();


                List<ABSOgrenciNotDetay> ogrNotList = db.ABSOgrenciNotDetay.Where(x => payIDlist.Contains(x.FKABSPaylarID.Value) && OGRliste.Contains(x.FKOgrenciID.Value) && x.Silindi == false).ToList();


                //var soruNotListe = db.ABSOgrenciNotDetay.Where(x => payListe.Select(y=>y.ID).Contains(x.ABSPaylarOlcme.FKPaylarID.Value) && OGRliste.Contains(x.FKOgrenciID)).ToList();

                foreach (var item in payListe)
                {
                    if (!ogrNotList.Any(x => x.FKABSPaylarID == item.ID))
                    {
                        //payListe.Remove(item);
                        continue;
                    }

                    OGRliste = ogrNotList.Where(x => x.FKABSPaylarID == item.ID && OGRliste.Contains(x.FKOgrenciID.Value)).Select(x => x.FKOgrenciID.Value).Distinct().ToList();
                    var paySoruListesi = db.ABSPaylarOlcme.Where(x => x.Silindi == false && x.FKPaylarID == item.ID).ToList();
                    var payPCListesi = db.ABSPaylarOlcmeProgram.Where(x => x.ABSPaylarOlcme.FKPaylarID == item.ID).ToList();
                    var payOCListesi = db.ABSPaylarOlcmeOgrenme.Where(x => x.ABSPaylarOlcme.FKPaylarID == item.ID).ToList();
                    //var payNotListesi = db.ABSOgrenciNotDetay.Where(x => x.FKABSPaylarID == item.ID && OGRliste.Contains(x.FKOgrenciID.Value)).ToList();
                    //var payNotListesiTumu = db.ABSOgrenciNotDetay.Where(x => x.FKABSPaylarID == item.ID && OGRlisteTumu.Contains(x.FKOgrenciID.Value)).ToList();

                    ToplamSonucVM yeniSonuc = new ToplamSonucVM();
                    yeniSonuc.EnumCalismaTip = item.EnumCalismaTip.Value;
                    yeniSonuc.CalismaTip = item.Sira + ". " + Sabis.Enum.Utils.GetEnumDescription((Sabis.Enum.EnumCalismaTip)item.EnumCalismaTip.Value);
                    yeniSonuc.FKABSPaylarID = item.ID;
                    //yeniSonuc.BasariYuzdesi = payNotListesi.Sum(x=>x.Notu) / OGRliste.Count() / ;

                    List<SoruToplamListeVM> soruList = new List<SoruToplamListeVM>();
                    foreach (var soru in paySoruListesi)
                    {

                        if (!ogrNotList.Any(x => x.FKAbsPaylarOlcmeID == soru.ID && x.FKABSPaylarID == item.ID && OGRliste.Contains(x.FKOgrenciID.Value) && x.Notu != 120 && x.Notu != 130))
                        {
                            continue;
                        }

                        var soruOgrNotListesi = ogrNotList.Where(x => x.FKAbsPaylarOlcmeID == soru.ID && x.FKABSPaylarID == item.ID && OGRliste.Contains(x.FKOgrenciID.Value) && x.Notu != 120 && x.Notu != 130).Sum(x => x.Notu).Value;
                        var soruPuan = soru.SoruPuan.Value;
                        //var soruOgrNotListesiTumu = ogrNotList.Where(x => x.FKAbsPaylarOlcmeID == soru.ID && x.FKABSPaylarID == item.ID && OGRlisteTumu.Contains(x.FKOgrenciID.Value) && x.Notu != 120 && x.Notu != 130).Sum(x => x.Notu).Value;

                        SoruToplamListeVM yeniSoru = new SoruToplamListeVM();
                        yeniSoru.ID = soru.ID;
                        yeniSoru.SoruNo = "Soru " + soru.SoruNo;
                        yeniSoru.SoruPuan = (double)soru.SoruPuan;
                        yeniSoru.SoruPCSayisi = db.ABSPaylarOlcmeProgram.Count(y => y.FKPaylarOlcmeID == soru.ID);
                        yeniSoru.SoruOCSayisi = db.ABSPaylarOlcmeOgrenme.Count(y => y.FKPaylarOlcmeID == soru.ID);
                        yeniSoru.SoruOrtalamaNot = Math.Round(soruOgrNotListesi / OGRliste.Count(), 2);
                        yeniSoru.SoruBasariYuzde = Math.Round((soruOgrNotListesi / OGRliste.Count()) / soruPuan, 2);
                        //yeniSoru.SoruBasariYuzdeGenel = Math.Round((soruOgrNotListesiTumu / OGRlisteTumu.Count()) / soru.SoruPuan.Value, 2);
                        soruList.Add(yeniSoru);
                    }
                    yeniSonuc.SoruListesi = soruList;

                    List<PCToplamListeVM> pcList = new List<PCToplamListeVM>();
                    foreach (var pc in payPCListesi)
                    {
                        var pcSoru = soruList.FirstOrDefault(x => x.ID == pc.FKPaylarOlcmeID);

                        if (pcSoru == null)
                        {
                            continue;
                        }

                        //var pcPuan = Math.Round((pcSoru.SoruOrtalamaNot / pcSoru.SoruPuan) / pcSoru.SoruPCSayisi, 2);
                        var pcPuan = Math.Round((pcSoru.SoruOrtalamaNot / pcSoru.SoruPuan), 2);

                        var pcNotToplam = pc.ABSPaylarOlcme.ABSOgrenciNotDetay.Where(x => x.Silindi == false && x.ABSPaylarOlcme.ID == pc.FKPaylarOlcmeID && x.FKABSPaylarID == item.ID && OGRliste.Contains(x.FKOgrenciID.Value)).Sum(x => x.Notu).Value;
                        //var pcNotToplamTumu = pc.ABSPaylarOlcme.ABSOgrenciNotDetay.Where(x => x.ABSPaylarOlcme.ID == pc.FKPaylarOlcmeID && x.FKABSPaylarID == item.ID && OGRlisteTumu.Contains(x.FKOgrenciID.Value)).Sum(x => x.Notu).Value;
                        var pcSoruPuanOrtalama = pcSoru.SoruBasariYuzde.Value * (pcSoru.SoruOrtalamaNot / pcSoru.SoruPCSayisi);
                        //var pcSoruPuanOrtalama = Math.Round(db.ABSPaylarOlcmeProgram.Where(y => y.FKProgramCiktiID == pc.FKProgramCiktiID.Value).Average(y => y.ABSPaylarOlcme.SoruPuan).Value / pcSoru.SoruPCSayisi, 2);
                        var pcPuanOrtalamaTumu = Math.Round(db.ABSPaylarOlcmeProgram.Where(y => y.FKProgramCiktiID == pc.FKProgramCiktiID.Value).Average(y => y.ABSPaylarOlcme.SoruPuan).Value / pcSoru.SoruPCSayisi, 2);

                        PCToplamListeVM yeniPC = new PCToplamListeVM();
                        yeniPC.ID = pc.FKProgramCiktiID;
                        yeniPC.PCNo = pc.BolumYeterlilik.YetID.ToString();
                        //yeniPC.PCPuan = pcPuan;
                        yeniPC.PCPuan = pcPuan;
                        yeniPC.PCBasariYuzde = pcSoru.SoruBasariYuzde.Value;
                        //yeniPC.PCBasariYuzde = Math.Round((pcNotToplam / OGRliste.Count()) / pcSoruPuanOrtalama, 2);
                        //yeniPC.PCBasariYuzdeGenel = Math.Round((pcNotToplamTumu / OGRlisteTumu.Count()) / pcPuanOrtalamaTumu, 2);
                        pcList.Add(yeniPC);
                    }
                    yeniSonuc.PCListe = pcList.GroupBy(x => x.ID).Select(x => new PCToplamListeVM
                    {
                        ID = x.Key,
                        PCNo = "PÇ " + x.FirstOrDefault(y => y.ID == x.Key).PCNo,
                        //PCPuan = x.Where(y=>y.ID == x.Key).b
                        PCPuan = x.Where(y => y.ID == x.Key).Sum(y => y.PCPuan) / x.Count(y => y.ID == x.Key),
                        PCBasariYuzde = Math.Round(x.Where(y => y.ID == x.Key).Sum(y => y.PCBasariYuzde) / x.Where(y => y.ID == x.Key).Sum(y => y.PCPuan)),
                        //PCBasariYuzde = Math.Round(x.Where(y => y.ID == x.Key).Sum(y => y.PCBasariYuzde), 2),
                        PCBasariYuzdeGenel = Math.Round(x.Where(y => y.ID == x.Key).Sum(y => y.PCBasariYuzdeGenel), 2)
                    }).OrderBy(x => x.PCNo).ToList();

                    List<OCToplamListeVM> ocList = new List<OCToplamListeVM>();
                    foreach (var oc in payOCListesi)
                    {
                        var ocSoru = soruList.FirstOrDefault(x => x.ID == oc.FKPaylarOlcmeID);

                        if (ocSoru == null)
                        {
                            continue;
                        }

                        var ocPuan = Math.Round((ocSoru.SoruOrtalamaNot / ocSoru.SoruPuan), 2);

                        var ocNotToplam = oc.ABSPaylarOlcme.ABSOgrenciNotDetay.Where(x => x.Silindi == false && x.ABSPaylarOlcme.ID == oc.FKPaylarOlcmeID && x.FKABSPaylarID == item.ID && OGRliste.Contains(x.FKOgrenciID.Value)).Sum(x => x.Notu).Value;
                        //var ocNotToplamTumu = oc.ABSPaylarOlcme.ABSOgrenciNotDetay.Where(x => x.ABSPaylarOlcme.ID == oc.FKPaylarOlcmeID && x.FKABSPaylarID == item.ID && OGRlisteTumu.Contains(x.FKOgrenciID.Value)).Sum(x => x.Notu).Value;
                        var ocSoruPuanOrtalama = ocSoru.SoruBasariYuzde.Value * (ocSoru.SoruOrtalamaNot / ocSoru.SoruPCSayisi);
                        var ocPuanOrtalamaTumu = Math.Round(db.ABSPaylarOlcmeOgrenme.Where(y => y.FKOgrenmeCiktiID == oc.FKOgrenmeCiktiID.Value).Average(y => y.ABSPaylarOlcme.SoruPuan).Value / ocSoru.SoruPCSayisi, 2);

                        OCToplamListeVM yeniOC = new OCToplamListeVM();
                        yeniOC.ID = oc.FKOgrenmeCiktiID;
                        yeniOC.OCNo = oc.DersCikti.CiktiNo.ToString();
                        yeniOC.OCPuan = ocPuan;
                        yeniOC.OCBasariYuzde = ocSoru.SoruBasariYuzde.Value;
                        //yeniOC.OCBasariYuzdeGenel = Math.Round((ocNotToplamTumu / OGRlisteTumu.Count()) / ocPuanOrtalamaTumu, 2);
                        ocList.Add(yeniOC);
                    }
                    yeniSonuc.OCListe = ocList.GroupBy(x => x.ID).Select(x => new OCToplamListeVM
                    {
                        ID = x.Key,
                        OCNo = "ÖÇ " + x.FirstOrDefault(y => y.ID == x.Key).OCNo,
                        OCPuan = x.Where(y => y.ID == x.Key).Sum(y => y.OCPuan) / x.Count(y => y.ID == x.Key),
                        OCBasariYuzde = Math.Round(x.Where(y => y.ID == x.Key).Sum(y => y.OCBasariYuzde) / x.Where(y => y.ID == x.Key).Sum(y => y.OCPuan)),
                        OCBasariYuzdeGenel = Math.Round(x.Where(y => y.ID == x.Key).Sum(y => y.OCBasariYuzdeGenel), 2)
                    }).OrderBy(x => x.OCNo).ToList();

                    //yeniSonuc.GenelPCPuan = yeniSonuc.PCListe.
                    //yeniSonuc.SoruPuanOrtalama = payNotListesi.Sum(x=>x.Notu) / OGRliste.Count() / paySoruListesi.;
                    result.Add(yeniSonuc);
                }
                return result;
            }
        }

        public static Dictionary<int, List<SoruListesiVM>> GetirSoruSonucListe(int dersGrupID, bool sonHalVerildiMi, int dersGrupHocaID, string kullaniciAdi, string IP)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                Dictionary<int, List<SoruListesiVM>> dictListe = new Dictionary<int, List<SoruListesiVM>>();
                List<int> OGRliste = new List<int>();

                if (sonHalVerildiMi)
                {
                    OGRliste = db.ABSBasariNot.Where(x => x.FKDersGrupID == dersGrupID).Where(x => x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.DC && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.DD && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.DZ && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.FF && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.GR && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.YT && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.YZ).Select(x => x.FKOgrenciID.Value).ToList();
                }
                else
                {
                    OGRliste = db.OGROgrenciYazilma.Where(x => x.FKDersGrupID == dersGrupID && x.Iptal == false).Select(x => x.FKOgrenciID.Value).ToList();
                }
                //

                var payList = Sabis.Bolum.Bll.SOURCE.ABS.DERS.PAYMAIN.GetirPayBilgileriv2(dersGrupHocaID, kullaniciAdi, IP);

                foreach (var pay in payList)
                {
                    var soruNotListe = db.ABSOgrenciNotDetay.Where(x => x.FKABSPaylarID == pay.PayID && OGRliste.Contains(x.FKOgrenciID.Value) && x.Silindi == false).ToList();

                    var soruGrup = soruNotListe.GroupBy(x => x.SoruNo).Select(x => new SoruListesiVM
                    {
                        Soru = "Soru " + x.Key.Value.ToString(),
                        SoruPuan = x.FirstOrDefault(y => y.SoruNo == x.Key.Value).ABSPaylarOlcme.SoruPuan.Value,
                        SoruPuanOrtalama = Math.Round((x.Where(y => y.SoruNo == x.Key.Value && y.Notu != 120 && y.Notu != 130).Sum(y => y.Notu).Value / x.Where(y => y.SoruNo == x.Key.Value).Count() / (x.FirstOrDefault(y => y.SoruNo == x.Key.Value).ABSPaylarOlcme.SoruPuan.Value)), 2),
                        Renk = "#4096EE"
                    }).ToList();

                    dictListe.Add(pay.PayID.Value, soruGrup);
                }



                //var soruNotListe = db.ABSOgrenciNotDetay.Where(x => x.FKABSPaylarID == payID && OGRliste.Contains(x.FKOgrenciID.Value) && x.Silindi == false).ToList();

                //var soruGrup = soruNotListe.GroupBy(x => x.SoruNo).Select(x => new SoruListesiVM
                //{
                //    Soru = "Soru " + x.Key.Value.ToString(),
                //    SoruPuan = x.FirstOrDefault(y => y.SoruNo == x.Key.Value).ABSPaylarOlcme.SoruPuan.Value,
                //    SoruPuanOrtalama = Math.Round((x.Where(y => y.SoruNo == x.Key.Value && y.Notu != 120 && y.Notu != 130).Sum(y => y.Notu).Value / x.Where(y => y.SoruNo == x.Key.Value).Count() / (x.FirstOrDefault(y => y.SoruNo == x.Key.Value).ABSPaylarOlcme.SoruPuan.Value)), 2),
                //    Renk = "#4096EE"
                //}).ToList();
                //return soruGrup;
                return dictListe;
            }
        }

        public static List<SoruListesiVM> GetirSoruSonuc(int dersGrupID, int payID, bool sonHalVerildiMi)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                List<int> OGRliste = new List<int>();

                if (sonHalVerildiMi)
                {
                    OGRliste = db.ABSBasariNot.Where(x => x.FKDersGrupID == dersGrupID).Where(x => x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.DC && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.DD && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.DZ && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.FF && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.GR && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.YT && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.YZ).Select(x => x.FKOgrenciID.Value).ToList();
                }
                else
                {
                    OGRliste = db.OGROgrenciYazilma.Where(x => x.FKDersGrupID == dersGrupID && x.Iptal == false).Select(x => x.FKOgrenciID.Value).ToList();
                }
                //

                

                var soruNotListe = db.ABSOgrenciNotDetay.Where(x => x.FKABSPaylarID == payID && OGRliste.Contains(x.FKOgrenciID.Value) && x.Silindi == false).ToList();

                var soruGrup = soruNotListe.GroupBy(x => x.SoruNo).Select(x => new SoruListesiVM
                {
                    Soru = "Soru " + x.Key.Value.ToString(),
                    SoruPuan = x.FirstOrDefault(y => y.SoruNo == x.Key.Value).ABSPaylarOlcme.SoruPuan.Value,
                    SoruPuanOrtalama = Math.Round((x.Where(y => y.SoruNo == x.Key.Value && y.Notu != 120 && y.Notu != 130).Sum(y => y.Notu).Value / x.Where(y => y.SoruNo == x.Key.Value).Count() / (x.FirstOrDefault(y => y.SoruNo == x.Key.Value).ABSPaylarOlcme.SoruPuan.Value)), 2),
                    Renk = "#4096EE"
                }).ToList();
                return soruGrup;
            }
        }

        public static List<CiktiListeVM> GetirPCBasariListesi(int dersGrupID, int payID, bool sonHalVerildiMi)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                List<int?> OGRliste = new List<int?>();

                if (sonHalVerildiMi)
                {
                    OGRliste = db.ABSBasariNot.Where(x => x.FKDersGrupID == dersGrupID).Where(x => x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.DC && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.DD && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.DZ && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.FF && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.GR && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.YT && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.YZ && !x.SilindiMi).Select(x => x.FKOgrenciID).ToList();
                }
                else
                {
                    OGRliste = db.OGROgrenciYazilma.Where(x => x.FKDersGrupID == dersGrupID && x.Iptal == false).Select(x => x.FKOgrenciID).ToList();
                }

                var soruNotListe = db.ABSOgrenciNotDetay.Where(x => x.FKABSPaylarID == payID && OGRliste.Contains(x.FKOgrenciID) && x.Silindi == false).ToList();

                var pcList = db.ABSPaylarOlcmeProgram.Where(x => x.ABSPaylarOlcme.FKPaylarID == payID).GroupBy(x => x.FKProgramCiktiID).Select(x => new CiktiListeVM
                {
                    Cikti = "PÇ " + db.ABSPaylarOlcmeProgram.FirstOrDefault(y => y.FKProgramCiktiID == x.Key.Value).BolumYeterlilik.YetID,
                    CiktiToplamPuan = db.ABSPaylarOlcmeProgram.Where(y => y.FKProgramCiktiID == x.Key && y.ABSPaylarOlcme.FKPaylarID == payID && y.ABSPaylarOlcme.SoruPuan != null).Sum(y => (double?)(y.ABSPaylarOlcme.SoruPuan) ?? 0),
                    CiktiPuanOrtalama = Math.Round(((db.ABSOgrenciNotDetay.Where(q => q.Silindi == false && q.FKABSPaylarID == payID && OGRliste.Contains(q.FKOgrenciID) && q.Notu != 120 && q.Notu != 130 && q.Notu != null && db.ABSPaylarOlcmeProgram.Where(w => w.FKProgramCiktiID == x.Key.Value && w.ABSPaylarOlcme.FKPaylarID == payID).Select(w => w.FKPaylarOlcmeID).Contains(q.FKAbsPaylarOlcmeID)).Sum(e => e.Notu.Value)) / OGRliste.Count()) / (db.ABSPaylarOlcmeProgram.Where(y => y.FKProgramCiktiID == x.Key && y.ABSPaylarOlcme.FKPaylarID == payID).Sum(y => y.ABSPaylarOlcme.SoruPuan.Value)), 2),
                    Sira = db.ABSPaylarOlcmeProgram.FirstOrDefault(y => y.FKProgramCiktiID == x.Key.Value).BolumYeterlilik.YetID.Value,
                    Renk = "#4096EE"
                }).ToList();

                return pcList;
            }
        }

        public static List<CiktiListeVM> GetirOCBasariListesi(int dersGrupID, int payID, bool sonHalVerildiMi)
        {
            //List<PCListeDTO> dersList = new List<PCListeDTO>();
            using (UYSv2Entities db = new UYSv2Entities())
            {
                List<int?> OGRliste = new List<int?>();

                if (sonHalVerildiMi)
                {
                    OGRliste = db.ABSBasariNot.Where(x => x.FKDersGrupID == dersGrupID).Where(x => x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.DC && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.DD && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.DZ && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.FF && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.GR && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.YT && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.YZ && !x.SilindiMi).Select(x => x.FKOgrenciID).ToList();
                }
                else
                {
                    OGRliste = db.OGROgrenciYazilma.Where(x => x.FKDersGrupID == dersGrupID && x.Iptal == false).Select(x => x.FKOgrenciID).ToList();
                }

                var soruNotListe = db.ABSOgrenciNotDetay.Where(x => x.Silindi == false && x.FKABSPaylarID == payID && OGRliste.Contains(x.FKOgrenciID)).ToList();

                //var soruListe = db.ABSPaylarOlcme.Where(x => x.FKPaylarID == payID).ToList();

                //var ocListe = db.ABSPaylarOlcmeOgrenme.Where(x => x.ABSPaylarOlcme.FKPaylarID == payID).ToList();


                //var ocList = db.ABSPaylarOlcmeOgrenme.Where(x => x.ABSPaylarOlcme.FKPaylarID == payID).GroupBy(x => x.FKOgrenmeCiktiID).Select(x => new PCListeDTO
                //{
                //    PC = "ÖÇ " + db.ABSPaylarOlcmeOgrenme.FirstOrDefault(y => y.FKOgrenmeCiktiID == x.Key.Value).DersCikti.CiktiNo,
                //    PCToplamPuan = db.ABSPaylarOlcmeProgram.Where(y => y.FKProgramCiktiID == x.Key && y.ABSPaylarOlcme.FKPaylarID == payID).Sum(y => y.ABSPaylarOlcme.SoruPuan),
                //    PCSoru = db.ABSPaylarOlcmeOgrenme.Where(y => y.FKOgrenmeCiktiID == x.Key && y.ABSPaylarOlcme.FKPaylarID == payID).Select(y => y.ABSPaylarOlcme.ID).Count(),
                //    PCPuanOrtalama = Math.Round((((db.ABSOgrenciNotDetay.Where(q => q.FKABSPaylarID == payID && OGRliste.Contains(q.FKOgrenciID) && q.Notu != 120 && q.Notu != 130 && db.ABSPaylarOlcmeOgrenme.Where(w => w.FKOgrenmeCiktiID == x.Key.Value && w.ABSPaylarOlcme.FKPaylarID == payID).Select(w => w.FKPaylarOlcmeID).Contains(q.FKAbsPaylarOlcmeID)).Sum(e => e.Notu.Value)) / OGRliste.Count()) / (db.ABSPaylarOlcmeOgrenme.Where(y => y.FKOgrenmeCiktiID == x.Key && y.ABSPaylarOlcme.FKPaylarID == payID).Sum(y => y.ABSPaylarOlcme.SoruPuan.Value))) / db.ABSPaylarOlcmeOgrenme.Count(y => y.FKOgrenmeCiktiID == x.Key && y.ABSPaylarOlcme.FKPaylarID == payID), 2),
                //    Sira = db.ABSPaylarOlcmeOgrenme.FirstOrDefault(y => y.FKOgrenmeCiktiID == x.Key.Value).DersCikti.CiktiNo.Value

                //}).ToList();

                var ocList = db.ABSPaylarOlcmeOgrenme.Where(x => x.ABSPaylarOlcme.FKPaylarID == payID).GroupBy(x => x.FKOgrenmeCiktiID).Select(x => new CiktiListeVM
                {
                    Cikti = "ÖÇ " + db.ABSPaylarOlcmeOgrenme.FirstOrDefault(y => y.ABSPaylarOlcme.FKPaylarID == payID && y.FKOgrenmeCiktiID == x.Key.Value).DersCikti.CiktiNo,
                    CiktiToplamPuan = db.ABSPaylarOlcmeOgrenme.Where(y => y.FKOgrenmeCiktiID == x.Key && y.ABSPaylarOlcme.FKPaylarID == payID && y.ABSPaylarOlcme.SoruPuan != null).Sum(y => y.ABSPaylarOlcme.SoruPuan),
                    CiktiPuanOrtalama = Math.Round((db.ABSOgrenciNotDetay.Where(q => q.Silindi == false && q.FKABSPaylarID == payID && OGRliste.Contains(q.FKOgrenciID) && q.Notu != 120 && q.Notu != 130 && q.Notu != null && db.ABSPaylarOlcmeOgrenme.Where(w => w.FKOgrenmeCiktiID == x.Key.Value && w.ABSPaylarOlcme.FKPaylarID == payID).Select(w => w.FKPaylarOlcmeID).Contains(q.FKAbsPaylarOlcmeID)).Sum(e => e.Notu.Value) / OGRliste.Count()) / (db.ABSPaylarOlcmeOgrenme.Where(y => y.FKOgrenmeCiktiID == x.Key && y.ABSPaylarOlcme.FKPaylarID == payID).Sum(y => y.ABSPaylarOlcme.SoruPuan.Value)), 2),
                    Sira = db.ABSPaylarOlcmeOgrenme.FirstOrDefault(y => y.ABSPaylarOlcme.FKPaylarID == payID && y.FKOgrenmeCiktiID == x.Key.Value).DersCikti.CiktiNo.Value,
                    Renk = "#4096EE"
                }).ToList();

                return ocList;
            }
        }

        //public static List<CiktiListeVM> GetirDersGrupPCBasariListesi(int dersGrupID, int yil, int donem)
        //{
        //    using (UYSv2Entities db = new UYSv2Entities())
        //    {
        //        List<CiktiListeVM> ciktiList = new List<CiktiListeVM>();

        //        OGRDersGrup dersGrup = db.OGRDersGrup.FirstOrDefault(x => x.ID == dersGrupID);
        //        List<int?> OGRliste = db.OGROgrenciYazilma.Where(x => x.FKDersGrupID == dersGrupID && x.Iptal == false).Select(x => x.FKOgrenciID).ToList();
        //        List<int> payList = db.ABSPaylar.Where(x => x.FKDersPlanAnaID == dersGrup.FKDersPlanAnaID && x.Yil == yil && x.EnumDonem == donem && x.EnumCalismaTip != (int)Sabis.Enum.EnumCalismaTip.YilIcininBasariya && x.EnumCalismaTip != (int)Sabis.Enum.EnumCalismaTip.Odev).Select(x => x.ID).ToList();


        //        foreach (var payID in payList)
        //        {
        //            if (!db.ABSPaylarOlcmeProgram.Any(x=> x.ABSPaylarOlcme.FKPaylarID == payID))
        //            {
        //                continue;
        //            }
        //            List<CiktiListeVM> pcList = db.ABSPaylarOlcmeProgram.Where(x => x.ABSPaylarOlcme.FKPaylarID == payID).GroupBy(x => x.FKProgramCiktiID).Select(x => new CiktiListeVM
        //            {
        //                Cikti = "PÇ " + db.ABSPaylarOlcmeProgram.FirstOrDefault(y => y.FKProgramCiktiID == x.Key.Value).BolumYeterlilik.YetID,
        //                CiktiToplamPuan = db.ABSPaylarOlcmeProgram.Where(y => y.FKProgramCiktiID == x.Key && y.ABSPaylarOlcme.FKPaylarID == payID && y.ABSPaylarOlcme.SoruPuan != null).Sum(y => (double?)(y.ABSPaylarOlcme.SoruPuan) ?? 0),
        //                CiktiPuanOrtalama = Math.Round(((db.ABSOgrenciNotDetay.Where(q => q.Silindi == false && q.FKABSPaylarID == payID && OGRliste.Contains(q.FKOgrenciID) && q.Notu != 120 && q.Notu != 130 && q.Notu != null && db.ABSPaylarOlcmeProgram.Where(w => w.FKProgramCiktiID == x.Key.Value && w.ABSPaylarOlcme.FKPaylarID == payID).Select(w => w.FKPaylarOlcmeID).Contains(q.FKAbsPaylarOlcmeID)).Sum(e => e.Notu.Value)) / OGRliste.Count()) / (db.ABSPaylarOlcmeProgram.Where(y => y.FKProgramCiktiID == x.Key && y.ABSPaylarOlcme.FKPaylarID == payID).Sum(y => y.ABSPaylarOlcme.SoruPuan.Value)), 2),
        //                Sira = db.ABSPaylarOlcmeProgram.FirstOrDefault(y => y.FKProgramCiktiID == x.Key.Value).BolumYeterlilik.YetID.Value,
        //                Renk = "#4096EE"
        //            }).ToList();

        //            ciktiList = ciktiList.Concat(pcList).ToList();
        //        }
        //        return ciktiList;
        //    }

        //}

        //public static List<CiktiListeVM> GetirDersGrupPCBasariListesi(int dersGrupID, int yil, int donem)
        //{
        //    using (UYSv2Entities db = new UYSv2Entities())
        //    {
        //        OGRDersGrup dersGrup = db.OGRDersGrup.FirstOrDefault(x => x.ID == dersGrupID);

        //        List<int> OGRliste = db.ABSBasariNot.Where(x => x.FKDersGrupID == dersGrupID).Where(x => x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.DC && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.DD && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.DZ && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.FF && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.GR && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.YT && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.YZ).Select(x => x.FKOgrenciID.Value).ToList();

        //        //List<int> payList = db.ABSPaylar.Where(x => x.FKDersPlanAnaID == dersGrup.FKDersPlanAnaID && x.Yil == yil && x.EnumDonem == donem && x.EnumCalismaTip != (int)Sabis.Enum.EnumCalismaTip.YilIcininBasariya && x.EnumCalismaTip != (int)Sabis.Enum.EnumCalismaTip.Odev).Select(x => x.ID).ToList();


        //        List<ABSPaylar> payList = db.ABSPaylar.Where(x => x.FKDersPlanAnaID == dersGrup.FKDersPlanAnaID && x.Yil == yil && x.EnumDonem == donem && x.EnumCalismaTip != (int)Sabis.Enum.EnumCalismaTip.YilIcininBasariya).ToList();
        //        List<int> payIDList = payList.Select(x=> x.ID).ToList();

        //        List<ABSOgrenciNotDetay> notListe = db.ABSOgrenciNotDetay.Where(x => x.Silindi == false && payIDList.Contains(x.FKABSPaylarID.Value) && OGRliste.Contains(x.FKOgrenciID.Value)).ToList();

        //        List<int> notPCIDlist = notListe.Select(x => x.ABSPaylarOlcme.ID).Distinct().ToList();

        //        List<ABSPaylarOlcmeProgram> pCiktiList = db.ABSPaylarOlcmeProgram.Where(x => payIDList.Contains(x.ABSPaylarOlcme.FKPaylarID.Value) && notPCIDlist.Contains(x.FKPaylarOlcmeID.Value)).ToList();

        //        List<CiktiListeVM> ciktiList = new List<CiktiListeVM>();

        //        foreach (var pay in payList)
        //        {
        //            var payPCList = pCiktiList.Where(x => x.ABSPaylarOlcme.FKPaylarID == pay.ID).GroupBy(x => x.FKProgramCiktiID);
        //            //var payPCList = pCiktiList.Where(x => x.ABSPaylarOlcme.FKPaylarID == pay.ID).ToList();

        //            foreach (var pc in payPCList)
        //            {
        //                CiktiListeVM yeniCikti = new CiktiListeVM();
        //                yeniCikti.ID = pc.Key;

        //                yeniCikti.CiktiToplamPuan = pCiktiList.Where(y => y.FKProgramCiktiID == pc.Key).Sum(y => y.ABSPaylarOlcme.SoruPuan);

        //                double toplamNot = notListe.Where(y => y.Silindi == false && y.FKABSPaylarID == pay.ID && y.ABSPaylarOlcme.ABSPaylarOlcmeProgram.Any(z => z.FKProgramCiktiID == pc.Key) && y.ABSPaylarOlcme.SoruPuan != null).Sum(y => y.Notu.Value);
        //                double azamiToplamNot = pCiktiList.Where(y => y.FKProgramCiktiID == pc.Key && y.ABSPaylarOlcme.FKPaylarID == pay.ID).Sum(y => y.ABSPaylarOlcme.SoruPuan.Value) * OGRliste.Count();
        //                int pcAdet = pCiktiList.Count(y => y.ABSPaylarOlcme.FKPaylarID == pay.ID && y.FKProgramCiktiID == pc.Key);



        //                //yeniCikti.CiktiPuanOrtalama = (((toplamNot / azamiToplamNot) * pay.Oran) / 100) * pCiktiList.Count(y=> y.FKProgramCiktiID == pc.Key); //  pCiktiList.Where(y => y.FKProgramCiktiID == pc.Key).Sum(y => y.ABSPaylarOlcme.SoruPuan.Value)  / pCiktiList.Count(y => y.FKProgramCiktiID == pc.Key) / 100);
        //                yeniCikti.CiktiPuanOrtalama = (((toplamNot / azamiToplamNot) * pay.Oran) / 100) * pcAdet;
        //                yeniCikti.Sira = pCiktiList.FirstOrDefault(y => y.FKProgramCiktiID == pc.Key).BolumYeterlilik.YetID.Value;
        //                yeniCikti.Cikti = "PÇ " + yeniCikti.Sira;
        //                ciktiList.Add(yeniCikti);
        //            }
        //        }

        //        ciktiList = ciktiList.GroupBy(x => x.ID).Select(x => new CiktiListeVM
        //        {
        //            ID = x.Key,
        //            CiktiToplamPuan = x.Where(y => y.ID == x.Key).Sum(y => y.CiktiToplamPuan),
        //            CiktiPuanOrtalama = x.Where(y => y.ID == x.Key).Sum(y => y.CiktiPuanOrtalama),
        //            Cikti = x.FirstOrDefault(y => y.ID == x.Key).Cikti,
        //            Sira = x.FirstOrDefault(y=> y.ID == x.Key).Sira
        //        }).ToList();

        //        KaydetProgramCiktiListe(ciktiList, dersGrup.FKDersPlanAnaID.Value, dersGrupID);

        //        return ciktiList.OrderBy(x => x.Sira).ToList();
        //    }
        //}


        //public static List<CiktiListeVM> GetirOrtakDersGrupPCBasariListesi(int fkDersPlanAnaID, int yil, int donem)
        //{
        //    using (UYSv2Entities db = new UYSv2Entities())
        //    {
        //        List<int> OGRliste = db.ABSBasariNot.Where(x => x.FKDersPlanAnaID == fkDersPlanAnaID && x.Yil == yil && x.EnumDonem == donem && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.DC && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.DD && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.DZ && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.FF && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.GR && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.YT && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.YZ).Select(x => x.FKOgrenciID.Value).ToList();
        //        List<int> payList = db.ABSPaylar.Where(x => x.FKDersPlanAnaID == fkDersPlanAnaID && x.Yil == yil && x.EnumDonem == donem && x.EnumCalismaTip != (int)Sabis.Enum.EnumCalismaTip.YilIcininBasariya).Select(x => x.ID).ToList();
        //        List<ABSOgrenciNot> notListe = db.ABSOgrenciNot.Where(x => x.Silindi == false && payList.Contains(x.FKAbsPaylarID.Value)).ToList();
        //        int dersGrupID = db.OGRDersGrup.FirstOrDefault(x => x.FKDersPlanAnaID == fkDersPlanAnaID && x.OgretimYili == yil && x.EnumDonem == donem && x.Acik == true).ID;


        //        List<CiktiListeVM> ciktiList = new List<CiktiListeVM>();

        //        foreach (var payID in payList)
        //        {
        //            List<ABSOgrenciNot> notFiltre = notListe.Where(x => OGRliste.Contains(x.FKOgrenciID)).ToList();
        //            CiktiListeVM yeniCikti = new CiktiListeVM();
        //                yeniCikti.ID = 21248;
        //                yeniCikti.CiktiToplamPuan = 0;

        //                double toplamNot = notListe.Where(y => y.Silindi == false && y.FKAbsPaylarID == payID).Sum(y => y.NotDeger);
        //                double azamiToplamNot = 100 * OGRliste.Count();

        //                yeniCikti.CiktiPuanOrtalama = toplamNot / azamiToplamNot; //  pCiktiList.Where(y => y.FKProgramCiktiID == pc.Key).Sum(y => y.ABSPaylarOlcme.SoruPuan.Value)  / pCiktiList.Count(y => y.FKProgramCiktiID == pc.Key) / 100);
        //                yeniCikti.Sira = 1;
        //                yeniCikti.Cikti = "PÇ " + 1;
        //                ciktiList.Add(yeniCikti);
        //        }

        //        ciktiList = ciktiList.GroupBy(x => x.ID).Select(x => new CiktiListeVM
        //        {
        //            ID = x.Key,
        //            CiktiToplamPuan = x.Where(y => y.ID == x.Key).Sum(y => y.CiktiToplamPuan),
        //            CiktiPuanOrtalama = x.Where(y => y.ID == x.Key).Sum(y => y.CiktiPuanOrtalama) / x.Count(y => y.ID == x.Key),
        //            Cikti = x.FirstOrDefault(y => y.ID == x.Key).Cikti,
        //            Sira = x.FirstOrDefault(y => y.ID == x.Key).Sira
        //        }).ToList();

        //        if (ciktiList.Count() > 0)
        //        {
        //            KaydetProgramCiktiListe(ciktiList, fkDersPlanAnaID, dersGrupID);
        //        }
        //        return ciktiList;
        //    }

        //}

        public static List<CiktiListeVM> GetirDersGrupPCBasariListesi(int dersGrupID, int yil, int donem)
        {
            //List<int> ortakDersIDList = new List<int>();
            //ortakDersIDList.Add(51518);
            //ortakDersIDList.Add(51519);
            //ortakDersIDList.Add(51946);
            //ortakDersIDList.Add(51947);
            //ortakDersIDList.Add(51925);
            //ortakDersIDList.Add(51926);

            using (UYSv2Entities db = new UYSv2Entities())
            {
                OGRDersGrup dersGrup = db.OGRDersGrup.FirstOrDefault(x => x.ID == dersGrupID);

                List<int> OGRliste = db.ABSBasariNot.Where(x => x.FKDersGrupID == dersGrupID).Where(x => x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.DZ && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.FF && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.GR && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.YT && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.YZ && !x.SilindiMi).Select(x => x.FKOgrenciID.Value).ToList();

                //bool ortakDersMi = ortakDersIDList.Contains(dersGrup.FKDersPlanAnaID.Value);

                //if (ortakDersMi)
                //{
                //    GetirOrtakDersGrupPCBasariListesi(dersGrup.FKDersPlanAnaID.Value, yil, donem);
                //    return new List<CiktiListeVM>();
                //}

                List<int> payList = db.ABSPaylar.Where(x => !x.Silindi && x.FKDersPlanAnaID == dersGrup.FKDersPlanAnaID && x.Yil == yil && x.EnumDonem == donem && x.EnumCalismaTip != (int)Sabis.Enum.EnumCalismaTip.YilIcininBasariya).Select(x => x.ID).ToList();

                List<ABSOgrenciNotDetay> notListe = db.ABSOgrenciNotDetay.Where(x => x.Silindi == false && payList.Contains(x.FKABSPaylarID.Value) && OGRliste.Contains(x.FKOgrenciID.Value)).ToList();

                List<int> notPCIDlist = notListe.Select(x => x.ABSPaylarOlcme.ID).ToList();

                List<ABSPaylarOlcmeProgram> pCiktiList = db.ABSPaylarOlcmeProgram.Where(x => payList.Contains(x.ABSPaylarOlcme.FKPaylarID.Value) && x.ABSPaylarOlcme.Silindi == false).ToList();

                List<CiktiListeVM> ciktiList = new List<CiktiListeVM>();

                foreach (var payID in payList)
                {
                    var payPCList = pCiktiList.Where(x => x.ABSPaylarOlcme.FKPaylarID == payID && notPCIDlist.Contains(x.FKPaylarOlcmeID.Value)).GroupBy(x => x.FKProgramCiktiID);

                    foreach (var pc in payPCList)
                    {
                        CiktiListeVM yeniCikti = new CiktiListeVM();
                        yeniCikti.ID = pc.Key;
                        yeniCikti.CiktiToplamPuan = pCiktiList.Where(y => y.FKProgramCiktiID == pc.Key).Sum(y => y.ABSPaylarOlcme.SoruPuan);

                        double toplamNot = notListe.Where(y => y.Silindi == false && y.FKABSPaylarID == payID && y.ABSPaylarOlcme.ABSPaylarOlcmeProgram.Any(z => z.FKProgramCiktiID == pc.Key) && y.ABSPaylarOlcme.SoruPuan != null).Sum(y => y.Notu.Value);
                        double azamiToplamNot = pCiktiList.Where(y => y.FKProgramCiktiID == pc.Key && y.ABSPaylarOlcme.FKPaylarID == payID).Sum(y => y.ABSPaylarOlcme.SoruPuan.Value) * OGRliste.Count();

                        yeniCikti.CiktiPuanOrtalama = toplamNot / azamiToplamNot; //  pCiktiList.Where(y => y.FKProgramCiktiID == pc.Key).Sum(y => y.ABSPaylarOlcme.SoruPuan.Value)  / pCiktiList.Count(y => y.FKProgramCiktiID == pc.Key) / 100);
                        yeniCikti.Sira = pCiktiList.FirstOrDefault(y => y.FKProgramCiktiID == pc.Key).BolumYeterlilik.YetID.Value;
                        yeniCikti.Cikti = "PÇ " + yeniCikti.Sira;
                        ciktiList.Add(yeniCikti);
                    }
                }

                ciktiList = ciktiList.GroupBy(x => x.ID).Select(x => new CiktiListeVM
                {
                    ID = x.Key,
                    CiktiToplamPuan = x.Where(y => y.ID == x.Key).Sum(y => y.CiktiToplamPuan),
                    CiktiPuanOrtalama = x.Where(y => y.ID == x.Key).Sum(y => y.CiktiPuanOrtalama) / x.Count(y => y.ID == x.Key),
                    Cikti = x.FirstOrDefault(y => y.ID == x.Key).Cikti,
                    Sira = x.FirstOrDefault(y => y.ID == x.Key).Sira
                }).ToList();

                if (ciktiList.Count() > 0)
                {
                    KaydetProgramCiktiListe(ciktiList, dersGrup.FKDersPlanAnaID.Value, dersGrupID);
                }
                

                return ciktiList.OrderBy(x => x.Sira).ToList();
            }
        }

        public static List<CiktiListeVM> GetirDersGrupOCBasariListesi(int dersGrupID, int yil, int donem)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                OGRDersGrup dersGrup = db.OGRDersGrup.FirstOrDefault(x => x.ID == dersGrupID);

                List<int> OGRliste = db.ABSBasariNot.Where(x => x.FKDersGrupID == dersGrupID).Where(x => x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.DZ && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.FF && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.GR && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.YT && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.YZ && !x.SilindiMi).Select(x => x.FKOgrenciID.Value).ToList();

                List<int> payList = db.ABSPaylar.Where(x => !x.Silindi && x.FKDersPlanAnaID == dersGrup.FKDersPlanAnaID && x.Yil == yil && x.EnumDonem == donem && x.EnumCalismaTip != (int)Sabis.Enum.EnumCalismaTip.YilIcininBasariya).Select(x => x.ID).ToList();

                List<ABSOgrenciNotDetay> notListe = db.ABSOgrenciNotDetay.Where(x => x.Silindi == false && payList.Contains(x.FKABSPaylarID.Value) && OGRliste.Contains(x.FKOgrenciID.Value)).ToList();

                List<int> notPCIDlist = notListe.Select(x => x.ABSPaylarOlcme.ID).ToList();

                List<ABSPaylarOlcmeOgrenme> oCiktiList = db.ABSPaylarOlcmeOgrenme.Where(x => payList.Contains(x.ABSPaylarOlcme.FKPaylarID.Value) && x.ABSPaylarOlcme.Silindi == false).ToList();

                List<CiktiListeVM> ciktiList = new List<CiktiListeVM>();

                foreach (var payID in payList)
                {
                    var payOCList = oCiktiList.Where(x => x.ABSPaylarOlcme.FKPaylarID == payID && notPCIDlist.Contains(x.FKPaylarOlcmeID.Value)).GroupBy(x => x.FKOgrenmeCiktiID);

                    foreach (var oc in payOCList)
                    {
                        CiktiListeVM yeniCikti = new CiktiListeVM();
                        yeniCikti.ID = oc.Key;
                        yeniCikti.CiktiToplamPuan = oCiktiList.Where(y => y.FKOgrenmeCiktiID == oc.Key).Sum(y => y.ABSPaylarOlcme.SoruPuan);

                        double toplamNot = notListe.Where(y => y.Silindi == false && y.FKABSPaylarID == payID && y.ABSPaylarOlcme.ABSPaylarOlcmeOgrenme.Any(z => z.FKOgrenmeCiktiID == oc.Key) && y.ABSPaylarOlcme.SoruPuan != null).Sum(y => y.Notu.Value);
                        double azamiToplamNot = oCiktiList.Where(y => y.FKOgrenmeCiktiID == oc.Key && y.ABSPaylarOlcme.FKPaylarID == payID).Sum(y => y.ABSPaylarOlcme.SoruPuan.Value) * OGRliste.Count();

                        yeniCikti.CiktiPuanOrtalama = toplamNot / azamiToplamNot; //  pCiktiList.Where(y => y.FKProgramCiktiID == pc.Key).Sum(y => y.ABSPaylarOlcme.SoruPuan.Value)  / pCiktiList.Count(y => y.FKProgramCiktiID == pc.Key) / 100);
                        yeniCikti.Sira = oCiktiList.FirstOrDefault(y => y.FKOgrenmeCiktiID == oc.Key).DersCikti.CiktiNo.Value;
                        yeniCikti.Cikti = "ÖÇ " + yeniCikti.Sira;
                        ciktiList.Add(yeniCikti);
                    }
                }

                ciktiList = ciktiList.GroupBy(x => x.ID).Select(x => new CiktiListeVM
                {
                    ID = x.Key,
                    CiktiToplamPuan = x.Where(y => y.ID == x.Key).Sum(y => y.CiktiToplamPuan),
                    CiktiPuanOrtalama = x.Where(y => y.ID == x.Key).Sum(y => y.CiktiPuanOrtalama) / x.Count(y => y.ID == x.Key),
                    Cikti = x.FirstOrDefault(y => y.ID == x.Key).Cikti,
                }).ToList();

                if (ciktiList.Count() > 0)
                {
                    KaydetOgrenmeCiktiListe(ciktiList, dersGrup.FKDersPlanAnaID.Value, dersGrupID);
                }

                return ciktiList.OrderBy(x => x.Cikti).ToList();
            }
        }

        public static void KaydetProgramCiktiListe(List<CiktiListeVM> ciktiListe, int dersPlanAnaID, int dersGrupID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                OGRDersGrup dersGrup = db.OGRDersGrup.FirstOrDefault(x => x.ID == dersGrupID);
                List<AKRGenelProgramCiktilari> listPC = db.AKRGenelProgramCiktilari.Where(x => x.FKDersGrupID == dersGrupID).ToList();

                //List<BolumYeterlilik> ortakBolumYeterlilik = db.BolumYeterlilik.Where(b => b.FKProgramBirimID == 255 && b.Yil == 2016).ToList();

                //List<int> ortakDersIDList = new List<int>();
                //ortakDersIDList.Add(51518);
                //ortakDersIDList.Add(51519);
                //ortakDersIDList.Add(51946);
                //ortakDersIDList.Add(51947);
                //ortakDersIDList.Add(51925);
                //ortakDersIDList.Add(51926);

                //if (ortakDersIDList.Contains(dersPlanAnaID))
                //{
                //    listPC = db.AKRGenelProgramCiktilari.Where(x => x.FKDersPlanAnaID == dersPlanAnaID).ToList();
                //}


                foreach (var cikti in listPC)
                {
                    db.AKRGenelProgramCiktilari.Remove(cikti);
                }
                db.SaveChanges();

                foreach (var cikti in ciktiListe)
                {
                    AKRGenelProgramCiktilari yeniPC = new AKRGenelProgramCiktilari();
                    yeniPC.FKDersPlanAnaID = dersPlanAnaID;
                    yeniPC.FKDersGrupID = dersGrupID;
                    yeniPC.EnumOgretimTur = dersGrup.EnumOgretimTur;
                    yeniPC.FKProgramCiktiID = cikti.ID.Value;
                    yeniPC.CiktiPuanOrtalama = cikti.CiktiPuanOrtalama.Value;
                    yeniPC.ProgramCikti = cikti.Cikti;
                    yeniPC.SGTarih = DateTime.Now;
                    yeniPC.SGIP = "1";
                    yeniPC.SGKullanici = "abc";
                    yeniPC.Yil = dersGrup.OgretimYili.Value;
                    yeniPC.Donem = dersGrup.EnumDonem.Value;
                    db.AKRGenelProgramCiktilari.Add(yeniPC);
                }
                db.SaveChanges();
            }
        }

        public static void KaydetOgrenmeCiktiListe(List<CiktiListeVM> ciktiListe, int dersPlanAnaID, int dersGrupID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                OGRDersGrup dersGrup = db.OGRDersGrup.FirstOrDefault(x => x.ID == dersGrupID);
                List<AKRGenelOgrenmeCiktilari> listPC = db.AKRGenelOgrenmeCiktilari.Where(x => x.FKDersGrupID == dersGrupID).ToList();

                foreach (var cikti in listPC)
                {
                    db.AKRGenelOgrenmeCiktilari.Remove(cikti);
                }
                db.SaveChanges();

                foreach (var cikti in ciktiListe)
                {
                    AKRGenelOgrenmeCiktilari yeniOC = new AKRGenelOgrenmeCiktilari();
                    yeniOC.FKDersPlanAnaID = dersPlanAnaID;
                    yeniOC.FKDersGrupID = dersGrupID;
                    yeniOC.EnumOgretimTur = dersGrup.EnumOgretimTur;
                    yeniOC.FKOgrenmeCiktiID = cikti.ID.Value;
                    yeniOC.CiktiPuanOrtalama = cikti.CiktiPuanOrtalama.Value;
                    yeniOC.OgrenmeCikti = cikti.Cikti;
                    yeniOC.SGTarih = DateTime.Now;
                    yeniOC.SGIP = "1";
                    yeniOC.SGKullanici = "abc";
                    yeniOC.Yil = dersGrup.OgretimYili.Value;
                    yeniOC.Donem = dersGrup.EnumDonem.Value;
                    db.AKRGenelOgrenmeCiktilari.Add(yeniOC);
                }
                db.SaveChanges();
            }
        }


        public static List<DersListe> GetirProgramCiktiListe(List<int> dersIDList, int birimID, int programID, int yil, int donem)
        {
            dersIDList.Add(51947);
            dersIDList.Add(51926);
            dersIDList.Add(51518);
            dersIDList.Add(68050);
            //dersIDList.Add(39877);

            dersIDList = dersIDList.Distinct().ToList();
            using (UYSv2Entities db = new UYSv2Entities())
            {
                //dersIDList.AddRange(db.AKRGenelProgramCiktilari.Where(x => x.Yil == yil && x.Donem == donem && x.OGRDersGrup.FKBirimID == programID).Select(x => x.FKDersPlanAnaID).Distinct());

                List<int> bitirmeTasarimDersIDList = db.OGRDersPlan.Where(x => x.Yil == yil && (x.OGRDersPlanAna.EnumKokDersTipi == 3 || x.OGRDersPlanAna.EnumKokDersTipi == 8) && x.FKBirimID == birimID).Select(x=> x.FKDersPlanAnaID.Value).Distinct().ToList();

                List<int> stajDersIDList = db.OGRDersPlan.Where(x => x.Yil == yil && (x.OGRDersPlanAna.EnumKokDersTipi == 6) && x.FKBirimID == birimID).Select(x=> x.FKDersPlanAnaID.Value).Distinct().ToList();

                dersIDList.AddRange(bitirmeTasarimDersIDList);
                dersIDList.AddRange(stajDersIDList);

                List<OGRDersPlanAna> dersList = db.OGRDersPlanAna.Where(x => dersIDList.Contains(x.ID)).ToList();
                List<OGRDersPlan> dersPlanList = db.OGRDersPlan.Where(x => dersIDList.Contains(x.FKDersPlanAnaID.Value)).ToList(); //x.FKBirimID == birimID




                //List<OGRDersPlan> dpOrtak = db.OGRDersPlan.Where(x => x.Yil == yil && x.FKDersPlanAnaID == 51947 || x.FKDersPlanAnaID == 51926 || x.FKDersPlanAnaID == 51518 || x.FKDersPlanAnaID == 68050).ToList();

                //dersPlanList.AddRange(dpOrtak);

                List<BolumYeterlilik> yeterlilikList = db.BolumYeterlilik.Where(x => x.FKProgramBirimID == birimID && x.Yil == yil).ToList();
                //List<BolumYeterlilik> yeterlilikList = db.BolumYeterlilik.Where(x => x.Yil == yil).ToList();
                List<DersYeterlilikIliski> yeterlilikIliskiList = db.DersYeterlilikIliski.Where(x => x.Yil == yil && dersIDList.Contains(x.FKDersPlanAnaID.Value) && x.Silindi == false).ToList(); // && dersIDList.Contains(x.FKDersPlanAnaID.Value) //x.FKProgramBirimID == birimID
                //List<int> ortakDersList = new List<int>(3) { 51926, 51947, 51518 };

                //List<Sabis.Bolum.Core.EBS.DERS.DersListesiVM> ortakDersler = Sabis.Bolum.Bll.SOURCE.EBS.DERS.DERSMAIN.GetirBirimUniversiteOrtakDersList(birimID, yil);
                //List<int> ortakDersIDList = ortakDersler.Select(x => x.ID).ToList();

                List<DersListe> ciktiListesi = new List<DersListe>();

                foreach (var dersID in dersIDList)
                {
                    

                    OGRDersPlanAna ders = dersList.FirstOrDefault(x => x.ID == dersID);
                    List<DersYeterlilikIliski> yeterlilikFiltre = yeterlilikIliskiList.Where(x => x.FKDersPlanAnaID == dersID).ToList();

                    DersListe yeniDers = new DersListe();
                    yeniDers.DersPlanAnaID = dersID;
                    yeniDers.DersAd = ders.DersAd;
                    yeniDers.AKTS = ders.ECTSKredi;
                    yeniDers.DersTuru = Utils.GetEnumDescription((EnumDersSecimTipi)(dersPlanList.FirstOrDefault(x => x.FKDersPlanAnaID == dersID).EnumDersSecimTipi));
                    yeniDers.Yariyil = dersPlanList.FirstOrDefault(x => x.FKDersPlanAnaID == dersID).Yariyil;
                    
                    List<int> dersGrupIDList = db.OGRDersGrup.Where(x => x.FKDersPlanAnaID == dersID && x.FKBirimID == programID && x.OgretimYili == yil && x.EnumDonem == donem && x.Acik == true).Select(x => x.ID).ToList();


                    var akrList = db.AKRGenelProgramCiktilari.Where(x => x.FKDersPlanAnaID == dersID && dersGrupIDList.Contains(x.FKDersGrupID)).ToList();


                    if (dersID == 51926 || dersID == 51947 || dersID == 51518 || dersID == 68050)
                    {
                        //if (dersID == 68050)
                        //{
                        //    string test = "123";
                        //}
                        List<int> ogrIDList = db.ABSBasariNot.Where(x => x.FKDersPlanAnaID == dersID && x.Yil == yil && x.EnumDonem == donem).Where(x => x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.DC && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.DD && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.DZ && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.FF && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.GR && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.YT && x.EnumHarfBasari != (int)Sabis.Enum.EnumHarfBasari.YZ).Select(x => x.FKOgrenciID.Value).ToList();

                        //List<int> ogrIDList = db.OGROgrenciYazilma.Where(x => x.FKDersPlanAnaID == dersID && x.Yil == yil && x.EnumDonem == donem).Select(x => x.FKOgrenciID.Value).ToList();
                        List<OGRKimlik> ogrList = db.OGRKimlik.Where(x => x.FKBolumPrBirimID == programID && x.EnumOgrDurum == (int)Sabis.Enum.EnumOgrDurum.NORMAL && ogrIDList.Contains(x.ID)).ToList();

                        List<int> ogrIDFiltre = ogrList.Select(x => x.ID).ToList();

                        List<ABSOgrenciNot> notList = db.ABSOgrenciNot.Where(x => x.FKDersPlanAnaID == dersID && x.Yil == yil && x.EnumDonem == donem && ogrIDFiltre.Contains(x.FKOgrenciID)).ToList();

                        notList = notList.Where(x => ogrIDList.Contains(x.FKOgrenciID)).ToList();

                        if (notList.Count() == 0)
                        {
                            continue;
                        }

                        List<CiktiListeVM> bList = new List<CiktiListeVM>();

                        yeterlilikFiltre = yeterlilikIliskiList.Where(x => x.FKDersPlanAnaID == dersID && x.FKProgramBirimID == birimID).ToList();

                        foreach (var item in yeterlilikFiltre)
                        {
                            CiktiListeVM yCikti = new CiktiListeVM();
                            yCikti.ID = item.ID;
                            yCikti.CiktiToplamPuan = 0;
  
                            if (item.KatkiDuzeyi != 0 )
                            {
                                yCikti.CiktiPuanOrtalama = notList.Sum(x=> x.NotDeger) / (notList.Count() * 100);
                            }
                            else
                            {
                                yCikti.CiktiPuanOrtalama = 0;
                            }

                            yCikti.Cikti = "PÇ " + item.BolumYeterlilik.YetID;
                            yCikti.FKDersPlanAnaID = dersID;
                            yCikti.Sira = item.BolumYeterlilik.YetID.Value;
                            yCikti.KatkiDuzeyi = item.KatkiDuzeyi.Value;
                            bList.Add(yCikti);
                        }
                        yeniDers.CiktiListesi = bList;
                    }
                    else if (bitirmeTasarimDersIDList.Contains(dersID) || stajDersIDList.Contains(dersID))
                    {
                        List<CiktiListeVM> cList = new List<CiktiListeVM>();

                        List<int> grpIDList = db.OGRDersGrup.Where(x => x.FKDersPlanAnaID == dersID && x.OgretimYili == yil && x.EnumDonem == donem && x.Acik == true).Select(x => x.ID).ToList();

                        var akrListFiltre = akrList.Where(x => grpIDList.Contains(x.FKDersGrupID)).GroupBy(x=> x.FKDersGrupID).ToList();

                        double ort = akrList.Where(x => grpIDList.Contains(x.FKDersGrupID)).Sum(x => x.CiktiPuanOrtalama) / akrList.Count(x => grpIDList.Contains(x.FKDersGrupID));

                        foreach (var item in yeterlilikFiltre)
                        {
                            CiktiListeVM yCikti = new CiktiListeVM();
                            yCikti.ID = item.ID;
                            yCikti.CiktiToplamPuan = 0;
                            if (item.KatkiDuzeyi != 0)
                            {
                                yCikti.CiktiPuanOrtalama = ort;
                            }
                            else
                            {
                                yCikti.CiktiPuanOrtalama = 0;
                            }

                            yCikti.Cikti = "PÇ " + item.BolumYeterlilik.YetID;
                            yCikti.FKDersPlanAnaID = dersID;
                            yCikti.Sira = item.BolumYeterlilik.YetID.Value;
                            yCikti.KatkiDuzeyi = item.KatkiDuzeyi.Value;
                            cList.Add(yCikti);
                        }
                        yeniDers.CiktiListesi = cList.OrderBy(x => x.Sira).ToList();
                    }
                    else if (ders.EnumDersTipi == (int)Sabis.Enum.EnumDersTipi.FAKULTE_DERSI) //dersID == 39878 || dersID == 41083 || dersID == 39877 || dersID == 28306 || dersID == 28511 || dersID == 28515
                    {
                        List<CiktiListeVM> cList = new List<CiktiListeVM>();

                        foreach (var item in akrList.GroupBy(x => x.FKProgramCiktiID))
                        {
                            int yetid = akrList.FirstOrDefault(c => c.FKProgramCiktiID == item.Key).BolumYeterlilik.YetID.Value;

                            CiktiListeVM yCikti = new CiktiListeVM();
                            yCikti.ID = item.Key;
                            yCikti.CiktiToplamPuan = 0;
                            yCikti.CiktiPuanOrtalama = akrList.Where(y => y.BolumYeterlilik.YetID == yetid).Sum(y => y.CiktiPuanOrtalama) / akrList.Count(y => y.BolumYeterlilik.YetID == yetid);
                            yCikti.Cikti = akrList.FirstOrDefault(y => y.BolumYeterlilik.YetID == yetid).ProgramCikti;
                            yCikti.FKDersPlanAnaID = dersID;
                            yCikti.Sira = item.FirstOrDefault(y => y.BolumYeterlilik.YetID == yetid).BolumYeterlilik.YetID.Value;
                            yCikti.KatkiDuzeyi = yeterlilikFiltre.Any(y => y.BolumYeterlilik.YetID == yetid) ? yeterlilikFiltre.FirstOrDefault(y => y.BolumYeterlilik.YetID == yetid).KatkiDuzeyi : 0;
                            cList.Add(yCikti);
                        }

                        List<int> eklenenPCler = cList.Select(x => x.ID.Value).ToList();
                        foreach (var item in yeterlilikList.Where(x => !eklenenPCler.Contains(x.ID)))
                        {
                            //int yetid = akrList.FirstOrDefault(c => c.FKProgramCiktiID == item.ID).BolumYeterlilik.YetID.Value;
                            if (cList.Any(v => v.Cikti == "PÇ " + item.YetID))
                            {
                                continue;
                            }
                            CiktiListeVM yCikti = new CiktiListeVM();
                            yCikti.ID = item.ID;
                            yCikti.CiktiToplamPuan = 0;
                            yCikti.CiktiPuanOrtalama = 0;
                            yCikti.Cikti = "PÇ " + item.YetID;
                            yCikti.FKDersPlanAnaID = dersID;
                            yCikti.Sira = item.YetID.Value;
                            yCikti.KatkiDuzeyi = yeterlilikFiltre.Any(y => y.FKYeterlilikID == item.ID) ? yeterlilikFiltre.FirstOrDefault(y => y.FKYeterlilikID == item.ID).KatkiDuzeyi : 0;
                            cList.Add(yCikti);
                        }

                        yeniDers.CiktiListesi = cList.GroupBy(x => x.Cikti).Select(x => new CiktiListeVM
                        {
                            ID = x.FirstOrDefault(y => y.Cikti == x.Key).ID,
                            CiktiToplamPuan = 0,
                            CiktiPuanOrtalama = cList.Where(y => y.Cikti == x.Key).Sum(y => y.CiktiPuanOrtalama) / cList.Count(y => y.Cikti == x.Key),
                            Cikti = cList.FirstOrDefault(y => y.Cikti == x.Key).Cikti,
                            FKDersPlanAnaID = cList.FirstOrDefault(y => y.Cikti == x.Key).FKDersPlanAnaID,
                            Sira = cList.FirstOrDefault(y => y.Cikti == x.Key).Sira,
                            KatkiDuzeyi = cList.FirstOrDefault(y => y.Cikti == x.Key).KatkiDuzeyi,
                        }).OrderBy(x => x.Sira).ToList();
                    }
                    else
                    {
                        List<CiktiListeVM> cList = new List<CiktiListeVM>();

                        foreach (var item in akrList.GroupBy(x => x.FKProgramCiktiID))
                        {
                            CiktiListeVM yCikti = new CiktiListeVM();
                            yCikti.ID = item.Key;
                            yCikti.CiktiToplamPuan = 0;
                            yCikti.CiktiPuanOrtalama = akrList.Where(y => y.FKProgramCiktiID == item.Key).Sum(y => y.CiktiPuanOrtalama) / akrList.Count(y => y.FKProgramCiktiID == item.Key);
                            yCikti.Cikti = akrList.FirstOrDefault(y => y.FKProgramCiktiID == item.Key).ProgramCikti;
                            yCikti.FKDersPlanAnaID = dersID;
                            yCikti.Sira = item.FirstOrDefault(y => y.FKProgramCiktiID == item.Key).BolumYeterlilik.YetID.Value;
                            yCikti.KatkiDuzeyi = yeterlilikFiltre.Any(y => y.FKYeterlilikID == item.Key) ? yeterlilikFiltre.FirstOrDefault(y => y.FKYeterlilikID == item.Key).KatkiDuzeyi : 0;
                            cList.Add(yCikti);
                        }

                        List<int> eklenenPCler = cList.Select(x => x.ID.Value).ToList();
                        foreach (var item in yeterlilikList.Where(x => !eklenenPCler.Contains(x.ID)))
                        {
                            if (cList.Any(v => v.Cikti == "PÇ " + item.YetID))
                            {
                                continue;
                            }
                            CiktiListeVM yCikti = new CiktiListeVM();
                            yCikti.ID = item.ID;
                            yCikti.CiktiToplamPuan = 0;
                            yCikti.CiktiPuanOrtalama = 0;
                            yCikti.Cikti = "PÇ " + item.YetID;
                            yCikti.FKDersPlanAnaID = dersID;
                            yCikti.Sira = item.YetID.Value;
                            yCikti.KatkiDuzeyi = yeterlilikFiltre.Any(y => y.FKYeterlilikID == item.ID) ? yeterlilikFiltre.FirstOrDefault(y => y.FKYeterlilikID == item.ID).KatkiDuzeyi : 0;
                            cList.Add(yCikti);
                        }

                        yeniDers.CiktiListesi = cList.GroupBy(x => x.Cikti).Select(x => new CiktiListeVM
                        {
                            ID = x.FirstOrDefault(y => y.Cikti == x.Key).ID,
                            CiktiToplamPuan = 0,
                            CiktiPuanOrtalama = cList.Where(y => y.Cikti == x.Key).Sum(y => y.CiktiPuanOrtalama) / cList.Count(y => y.Cikti == x.Key),
                            Cikti = cList.FirstOrDefault(y => y.Cikti == x.Key).Cikti,
                            FKDersPlanAnaID = cList.FirstOrDefault(y => y.Cikti == x.Key).FKDersPlanAnaID,
                            Sira = cList.FirstOrDefault(y => y.Cikti == x.Key).Sira,
                            KatkiDuzeyi = cList.FirstOrDefault(y => y.Cikti == x.Key).KatkiDuzeyi,
                        }).OrderBy(x => x.Sira).ToList();
                    }

                    ciktiListesi.Add(yeniDers);
                }
                if (donem == (int)EnumDonem.GUZ)
                {
                    ciktiListesi = ciktiListesi.Where(x => x.Yariyil == 1 || x.Yariyil == 3 || x.Yariyil == 5 || x.Yariyil == 7 || x.Yariyil == 0).ToList();
                }

                if (donem == (int)EnumDonem.BAHAR)
                {
                    ciktiListesi = ciktiListesi.Where(x => x.Yariyil == 2 || x.Yariyil == 4 || x.Yariyil == 6 || x.Yariyil == 8 || x.Yariyil == 0).ToList();
                }

                foreach (var ders in ciktiListesi)
                {
                    foreach (var cikti in ders.CiktiListesi)
                    {
                        if (double.IsNaN(cikti.CiktiPuanOrtalama.Value))
                        {
                            cikti.CiktiPuanOrtalama = 0;
                        }
                    }
                }

                return ciktiListesi.Where(x=> x.CiktiListesi.Any(y=> y.CiktiPuanOrtalama != 0) && x.CiktiListesi.Count()>0).OrderBy(x=> x.Yariyil).ThenBy(x=> x.DersTuru).ToList();
            }
        }
        public static List<DersListe> GetirOgrenmeCiktiListe(List<int> dersIDList, int birimID, int programID, int yil, int donem)
        {
            
            using (UYSv2Entities db = new UYSv2Entities())
            {
                //List<int> dersIDList = new List<int>();
                //dersIDList.AddRange(db.AKRGenelOgrenmeCiktilari.Where(x => x.Yil == yil && x.Donem == donem && x.OGRDersGrup.FKBirimID == programID).Select(x => x.FKDersPlanAnaID).Distinct());


                List<OGRDersPlanAna> dersList = db.OGRDersPlanAna.Where(x => dersIDList.Contains(x.ID)).ToList();
                List<OGRDersPlan> dersPlanList = db.OGRDersPlan.Where(x => x.FKBirimID == birimID).ToList();
                List<DersCikti> ogrenmeCiktiList = db.DersCikti.Where(x => dersIDList.Contains(x.FKDersPlanAnaID.Value) && x.Yil == yil).ToList();

                List<DersListe> ciktiListesi = new List<DersListe>();

                foreach (var dersID in dersIDList)
                {
                    OGRDersPlanAna ders = dersList.FirstOrDefault(x => x.ID == dersID);

                    DersListe yeniDers = new DersListe();
                    yeniDers.DersPlanAnaID = dersID;
                    yeniDers.DersAd = ders.DersAd;
                    yeniDers.AKTS = ders.ECTSKredi;
                    yeniDers.DersTuru = Sabis.Enum.Utils.GetEnumDescription((Sabis.Enum.EnumDersSecimTipi)(dersPlanList.FirstOrDefault(x => x.FKDersPlanAnaID == dersID).EnumDersSecimTipi));
                    yeniDers.Yariyil = dersPlanList.FirstOrDefault(x => x.FKDersPlanAnaID == dersID).Yariyil;

                    List<int> dersGrupIDList = db.OGRDersGrup.Where(x => x.FKDersPlanAnaID == dersID && x.FKBirimID == programID && x.OgretimYili == yil && x.EnumDonem == donem && x.Acik == true).Select(x => x.ID).ToList();

                    var akrList = db.AKRGenelOgrenmeCiktilari.Where(x => x.FKDersPlanAnaID == dersID && dersGrupIDList.Contains(x.FKDersGrupID));

                    List<CiktiListeVM> cList = new List<CiktiListeVM>();
                    foreach (var item in akrList.GroupBy(x => x.FKOgrenmeCiktiID))
                    {
                        CiktiListeVM yCikti = new CiktiListeVM();
                        yCikti.ID = item.Key;
                        yCikti.CiktiToplamPuan = 0;
                        yCikti.CiktiPuanOrtalama = akrList.Where(y => y.FKOgrenmeCiktiID == item.Key).Sum(y => y.CiktiPuanOrtalama) / akrList.Count(y => y.FKOgrenmeCiktiID == item.Key);
                        yCikti.Cikti = akrList.FirstOrDefault(y => y.FKOgrenmeCiktiID == item.Key).OgrenmeCikti;
                        yCikti.FKDersPlanAnaID = dersID;
                        yCikti.Sira = item.FirstOrDefault(y => y.FKOgrenmeCiktiID == item.Key).DersCikti.CiktiNo.Value;
                        cList.Add(yCikti);
                    }

                    List<int> eklenenPCler = cList.Select(x => x.ID.Value).ToList();
                    foreach (var item in ogrenmeCiktiList.Where(x => !eklenenPCler.Contains(x.ID)))
                    {
                        CiktiListeVM yCikti = new CiktiListeVM();
                        yCikti.ID = item.ID;
                        yCikti.CiktiToplamPuan = 0;
                        yCikti.CiktiPuanOrtalama = 0;
                        yCikti.Cikti = "ÖÇ " + item.CiktiNo;
                        yCikti.FKDersPlanAnaID = dersID;
                        yCikti.Sira = item.CiktiNo.Value;
                        cList.Add(yCikti);
                    }

                    yeniDers.CiktiListesi = cList.OrderBy(x => x.Sira).ToList();

                    if (donem == (int)EnumDonem.GUZ)
                    {
                        ciktiListesi = ciktiListesi.Where(x => x.Yariyil == 1 || x.Yariyil == 3 || x.Yariyil == 5 || x.Yariyil == 7).ToList();
                    }

                    if (donem == (int)EnumDonem.BAHAR)
                    {
                        ciktiListesi = ciktiListesi.Where(x => x.Yariyil == 2 || x.Yariyil == 4 || x.Yariyil == 6 || x.Yariyil == 8).ToList();
                    }

                    ciktiListesi.Add(yeniDers);
                }

                foreach (var ders in ciktiListesi)
                {
                    foreach (var cikti in ders.CiktiListesi)
                    {
                        if (double.IsNaN(cikti.CiktiPuanOrtalama.Value))
                        {
                            cikti.CiktiPuanOrtalama = 0;
                        }
                    }
                }


                return ciktiListesi.Where(x => x.CiktiListesi.Count() > 0).OrderBy(x => x.Yariyil).ThenBy(x => x.DersTuru).ToList();
            }
        }

        public static List<CiktiListeVM> GetirProgramCiktiOrtalamaListe(int birimID, int programID, int yil, int donem)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                List<OGRDersPlan> dersPlanList = db.OGRDersPlan.Where(x => x.FKBirimID == birimID && x.Yil == yil && x.EnumDersSecimTipi == (int)Sabis.Enum.EnumDersSecimTipi.ZORUNLU).ToList();
                List<int> dersIDList = dersPlanList.Select(x => x.FKDersPlanAnaID.Value).Distinct().ToList();
                List<int> bitirmeTasarimDersIDList = db.OGRDersPlan.Where(x => x.Yil == yil && (x.OGRDersPlanAna.EnumKokDersTipi == 3 || x.OGRDersPlanAna.EnumKokDersTipi == 8) && x.FKBirimID == birimID).Select(x => x.FKDersPlanAnaID.Value).Distinct().ToList();
                List<int> stajDersIDList = db.OGRDersPlan.Where(x => x.Yil == yil && (x.OGRDersPlanAna.EnumKokDersTipi == 6) && x.FKBirimID == birimID).Select(x => x.FKDersPlanAnaID.Value).Distinct().ToList();

                bitirmeTasarimDersIDList.Add(51947);
                bitirmeTasarimDersIDList.Add(51926);
                bitirmeTasarimDersIDList.Add(51518);
                bitirmeTasarimDersIDList.Add(68050);

                dersIDList.AddRange(bitirmeTasarimDersIDList);
                dersIDList.AddRange(stajDersIDList);

                List<OGRDersGrup> dersGrupList = db.OGRDersGrup.Where(x => dersIDList.Contains(x.FKDersPlanAnaID.Value) && x.FKBirimID == programID && x.OgretimYili == yil && x.EnumDonem == donem).ToList();
                List<int> dersGrupIDList = dersGrupList.Select(x => x.ID).ToList();
                List<OGRDersPlanAna> dersList = db.OGRDersPlanAna.Where(x => dersIDList.Contains(x.ID)).ToList();
                List<BolumYeterlilik> yeterlilikList = db.BolumYeterlilik.Where(x => x.FKProgramBirimID == birimID && x.Yil == yil).ToList();
                List<AKRGenelProgramCiktilari> akrList = db.AKRGenelProgramCiktilari.Where(x => dersGrupIDList.Contains(x.FKDersGrupID)).ToList(); //dersIDList.Contains(x.FKDersPlanAnaID) &&

                //List<DersYeterlilikIliski> katkiListesi = db.DersYeterlilikIliski.Where(x => x.FKProgramBirimID == birimID && x.Yil == yil).ToList();
                List<DersYeterlilikIliski> katkiListesi = db.DersYeterlilikIliski.Where(x => dersIDList.Contains(x.FKDersPlanAnaID.Value) && x.Yil == yil && x.Silindi == false).ToList();

                List<CiktiListeVM> ciktiListesi = new List<CiktiListeVM>();

                foreach (var cikti in yeterlilikList)
                {
                    List<int> eklenmisDersIDList = akrList.Where(x => x.FKProgramCiktiID == cikti.ID).Select(x => x.FKDersPlanAnaID).ToList();
                    eklenmisDersIDList.AddRange(bitirmeTasarimDersIDList);
                    eklenmisDersIDList.AddRange(stajDersIDList);
                    List<OGRDersPlanAna> eklenmisDersList = db.OGRDersPlanAna.Where(x => eklenmisDersIDList.Contains(x.ID)).ToList();

                    double agirlikliOrtalama = new double();

                    double ciktiToplam = 0;
                    double aktsToplam = new double();

                    if (!akrList.Any(y => y.FKProgramCiktiID == cikti.ID))
                    {
                        agirlikliOrtalama = 0;
                    }
                    else
                    {
                        foreach (var ders in eklenmisDersList)
                        {
                            if (akrList.Any(x => x.FKDersPlanAnaID == ders.ID && x.FKProgramCiktiID == cikti.ID))
                            {
                                double ort = (akrList.Where(x => x.FKDersPlanAnaID == ders.ID && x.FKProgramCiktiID == cikti.ID).Sum(x => x.CiktiPuanOrtalama) / akrList.Count(x => x.FKDersPlanAnaID == ders.ID && x.FKProgramCiktiID == cikti.ID)) * ders.ECTSKredi.Value;
                                ciktiToplam += ort;
                                aktsToplam += ders.ECTSKredi.Value;
                            }
                        }
                        agirlikliOrtalama = ciktiToplam / aktsToplam;
                        //if (Double.IsNaN(agirlikliOrtalama))
                        //{
                        //    continue;
                        //}
                    }

                    CiktiListeVM yCikti = new CiktiListeVM();
                    yCikti.ID = cikti.ID;
                    yCikti.CiktiToplamPuan = 1;
                    yCikti.CiktiPuanOrtalama = Double.IsNaN(agirlikliOrtalama) ? 0 : agirlikliOrtalama;
                    yCikti.Cikti = akrList.Any(y => y.FKProgramCiktiID == cikti.ID) ? akrList.FirstOrDefault(y => y.FKProgramCiktiID == cikti.ID).ProgramCikti : "PÇ " + cikti.YetID;
                    yCikti.FKDersPlanAnaID = 0;
                    yCikti.Sira = cikti.YetID.Value;
                    ciktiListesi.Add(yCikti);
                }

                foreach (var cikti in yeterlilikList)
                {
                    List<int> eklenmisDersIDList = akrList.Where(x => x.FKProgramCiktiID == cikti.ID).Select(x => x.FKDersPlanAnaID).ToList();
                    List<OGRDersPlanAna> eklenmisDersList = db.OGRDersPlanAna.Where(x => eklenmisDersIDList.Contains(x.ID)).ToList();

                    

                    double ciktiToplam = 0;
                    double aktsToplam = new double();
                    double katSayiToplam = new double();
                    double agirlikliOrtalama = new double();

                    if (!akrList.Any(y => y.FKProgramCiktiID == cikti.ID))
                    {
                        agirlikliOrtalama = 0;
                    }
                    else
                    {
                        foreach (var ders in eklenmisDersList)
                        {
                            bool kontrol = katkiListesi.Any(x => x.FKYeterlilikID == cikti.ID && x.FKProgramBirimID == birimID && x.FKDersPlanAnaID == ders.ID) && katkiListesi.FirstOrDefault(x => x.FKYeterlilikID == cikti.ID && x.FKProgramBirimID == birimID && x.FKDersPlanAnaID == ders.ID).KatkiDuzeyi != null;

                            int katkiDuzeyi = katkiListesi.Any(x => x.FKYeterlilikID == cikti.ID && x.FKProgramBirimID == birimID && x.FKDersPlanAnaID == ders.ID) ? katkiListesi.FirstOrDefault(x => x.FKYeterlilikID == cikti.ID && x.FKProgramBirimID == birimID && x.FKDersPlanAnaID == ders.ID).KatkiDuzeyi.Value : -1;

                            if (katkiDuzeyi == -1)
                            {
                                continue;
                            }
                            //if (katkiListesi.Any(x => x.FKYeterlilikID == cikti.ID && x.FKProgramBirimID == birimID && x.FKDersPlanAnaID == ders.ID))
                            //{
                            //    katkiDuzeyi = 0;
                            //}
                            //else
                            //{
                            //    katkiDuzeyi = katkiListesi.FirstOrDefault(x => x.FKYeterlilikID == cikti.ID && x.FKProgramBirimID == birimID && x.FKDersPlanAnaID == ders.ID).KatkiDuzeyi.Value;
                            //}
                            ciktiToplam += akrList.Where(x => x.FKDersPlanAnaID == ders.ID && x.FKProgramCiktiID == cikti.ID).Sum(x => x.CiktiPuanOrtalama) / akrList.Count(x => x.FKDersPlanAnaID == ders.ID && x.FKProgramCiktiID == cikti.ID) * ders.ECTSKredi.Value * katkiDuzeyi;
                            aktsToplam += ders.ECTSKredi.Value;
                            katSayiToplam += kontrol ? ders.ECTSKredi.Value * katkiListesi.FirstOrDefault(x => x.FKYeterlilikID == cikti.ID && x.FKProgramBirimID == birimID && x.FKDersPlanAnaID == ders.ID).KatkiDuzeyi.Value : 0;
                        }

                        agirlikliOrtalama = ciktiToplam / katSayiToplam;
                    }

                    //string k = Convert.ToString(agirlikliOrtalama);
                    //int n;
                    //bool isNumeric = int.TryParse(k, out n);

                    //if (!isNumeric)
                    //{
                    //    continue;
                    //}

                    CiktiListeVM yCikti = new CiktiListeVM();
                    yCikti.ID = cikti.ID;
                    yCikti.CiktiToplamPuan = 2;
                    yCikti.CiktiPuanOrtalama = Double.IsNaN(agirlikliOrtalama) ? 0 : agirlikliOrtalama;
                    yCikti.Cikti = akrList.Any(y => y.FKProgramCiktiID == cikti.ID) ? akrList.FirstOrDefault(y => y.FKProgramCiktiID == cikti.ID).ProgramCikti : "PÇ" + cikti.YetID;
                    yCikti.FKDersPlanAnaID = 0;
                    yCikti.Sira = cikti.YetID.Value;
                    ciktiListesi.Add(yCikti);
                }

                foreach (var cikti in ciktiListesi)
                {
                    if (double.IsNaN(cikti.CiktiPuanOrtalama.Value))
                    {
                        cikti.CiktiPuanOrtalama = 0;
                    }
                }

                return ciktiListesi;
            }
        }

        public static bool AkreditasyonTopluGuncelle(int yil, int donem)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    //List<AKRGenelProgramCiktilari> akrPCList = db.AKRGenelProgramCiktilari.ToList();
                    //List<AKRGenelOgrenmeCiktilari> akrOCList = db.AKRGenelOgrenmeCiktilari.ToList();

                    //foreach (var pc in akrPCList)
                    //{
                    //    db.AKRGenelProgramCiktilari.Remove(pc);
                    //}

                    //foreach (var oc in akrOCList)
                    //{
                    //    db.AKRGenelOgrenmeCiktilari.Remove(oc);
                    //}

                    //db.SaveChanges();

                    List<int> dersIDListesi = db.ABSPaylarOlcmeProgram.Where(x => x.ABSPaylarOlcme.ABSPaylar.Yil == yil && x.ABSPaylarOlcme.ABSPaylar.EnumDonem == donem).Select(x => x.ABSPaylarOlcme.ABSPaylar.FKDersPlanAnaID.Value).Distinct().ToList();

                    //dersIDListesi.Add(51518);
                    //dersIDListesi.Add(51519);
                    //dersIDListesi.Add(51946);
                    //dersIDListesi.Add(51947);
                    //dersIDListesi.Add(51925);
                    //dersIDListesi.Add(51926);

                    foreach (var dersID in dersIDListesi)
                    {
                        List<OGRDersGrup> dersGrupList = db.OGRDersGrup.Where(x => x.FKDersPlanAnaID == dersID && x.OgretimYili == yil && x.EnumDonem == donem).ToList();

                        foreach (var dersGrup in dersGrupList)
                        {
                            try
                            {
                                GetirDersGrupPCBasariListesi(dersGrup.ID, yil, donem);
                                GetirDersGrupOCBasariListesi(dersGrup.ID, yil, donem);
                            }
                            catch (Exception ex)
                            {
                                Sabis.Bolum.Bll.SOURCE.GENEL.GENELMAIN.MailGonder(ex.ToString(), "hakki", "", null);
                                continue;
                            }
                        }
                    }

                    //GetirOrtakDersGrupPCBasariListesi(51518, yil, donem);
                    //GetirOrtakDersGrupPCBasariListesi(51519, yil, donem);
                    //GetirOrtakDersGrupPCBasariListesi(51946, yil, donem);
                    //GetirOrtakDersGrupPCBasariListesi(51947, yil, donem);
                    //GetirOrtakDersGrupPCBasariListesi(51925, yil, donem);
                    //GetirOrtakDersGrupPCBasariListesi(51926, yil, donem);

                }
                return true;
            }
            catch (Exception ex)
            {
                Sabis.Bolum.Bll.SOURCE.GENEL.GENELMAIN.MailGonder(ex.ToString(), "hakki", "", null);
                return false;
            }
        }

        public class DersListe
        {
            public int DersPlanAnaID { get; set; }
            public string DersAd { get; set; }
            public double? AKTS { get; set; }
            public string DersTuru { get; set; }
            public int? Yariyil { get; set; }
            public List<CiktiListeVM> CiktiListesi { get; set; }
        }
        
    }
}
