﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sabis.Bolum.Core.ABS.NOT;
using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Enum;
using Sabis.Bolum.Core.ABS.DEGERLENDIRME;

namespace Sabis.Bolum.Bll.SOURCE.ABS.NOT
{
    public class NOTMAIN
    {
        public static List<OgrenciListesiVM> GetirOgrenciNotlari(int dersGrupID, string listPayID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                List<int> idlist = new List<int>();
                string[] dizi = listPayID.Split('-');
                for (int i = 0; i < dizi.Length; i++)
                {
                    idlist.Add(Convert.ToInt32(dizi[i]));
                }

                List<OgrenciListesiVM> liste = new List<OgrenciListesiVM>();

                List<ABSPaylar> payListesi = db.ABSPaylar.Where(d => !d.Silindi && idlist.Contains(d.ID)).OrderBy(x => x.EnumCalismaTip).ToList();

                var rawSQL = db.OGROgrenciYazilma.AsQueryable();
                bool butunlemeMi = db.OGROgrenciYazilma.Any(x => x.FKDersGrupID == dersGrupID && x.Iptal != true && x.ButunlemeMi == true);

                if (butunlemeMi)
                {
                    rawSQL = rawSQL.Where(x => (x.ButunlemeMi.HasValue && x.ButunlemeMi.Value == true));
                }
                var sorgu = from yazilma in rawSQL
                            where yazilma.FKDersGrupID == dersGrupID && yazilma.Iptal == false && yazilma.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL
                            select new
                            {
                                Numara = yazilma.OGRKimlik.Numara,
                                Ad = yazilma.OGRKimlik.Kisi.Ad,
                                Soyad = yazilma.OGRKimlik.Kisi.Soyad,
                                OgrenciID = yazilma.OGRKimlik.ID,
                                DersPlanAnaID = yazilma.OGRDersGrup.OGRDersPlan.FKDersPlanAnaID.Value,
                                DersGrupID = yazilma.FKDersGrupID.Value,
                                DersPlanID = yazilma.OGRDersGrup.FKDersPlanID.Value,
                                //Notlar = db.ABSOgrenciNot.Where(x => x.FKOgrenciID == yazilma.FKOgrenciID && x.FKDersGrupID == dersGrupID && x.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL).OrderBy(d => d.ID).ToList(),
                                Notlar = yazilma.OGRDersGrup.ABSOgrenciNot.Where(x => x.FKOgrenciID == yazilma.FKOgrenciID && x.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL && x.Silindi == false).OrderBy(d => d.ABSPaylar.EnumCalismaTip),
                                YazilmaID = yazilma.ID,
                            };
                foreach (var ogrenciYazilma in sorgu)
                {
                    OgrenciListesiVM ogPayNotBilgileri = new OgrenciListesiVM();

                    ogPayNotBilgileri.Ad = ogrenciYazilma.Ad;
                    ogPayNotBilgileri.Soyad = ogrenciYazilma.Soyad;
                    ogPayNotBilgileri.DersGrupID = ogrenciYazilma.DersGrupID;
                    ogPayNotBilgileri.DersPlanID = ogrenciYazilma.DersPlanID;
                    ogPayNotBilgileri.OgrenciID = Convert.ToInt32(ogrenciYazilma.OgrenciID);
                    ogPayNotBilgileri.DersPlanAnaID = Convert.ToInt32(ogrenciYazilma.DersPlanAnaID);
                    ogPayNotBilgileri.Numara = ogrenciYazilma.Numara;

                    foreach (var pay in payListesi)
                    {
                        bool sistemTarihKontrol = true;

                        OgrenciNotlariVM pvn = new OgrenciNotlariVM();
                        pvn.PayID = pay.ID;
                        pvn.EnumCalismaTip = pay.EnumCalismaTip.Value;
                        pvn.Sira = pay.Sira;
                        pvn.Aktif = sistemTarihKontrol;
                        pvn.Oran = pay.Oran;


                        ABSOgrenciNot notu = ogrenciYazilma.Notlar.Where(d => d.Silindi == false && d.FKOgrenciID == ogPayNotBilgileri.OgrenciID && d.FKAbsPaylarID == pay.ID && d.FKDersGrupID == dersGrupID && d.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL).FirstOrDefault();

                        if (notu != null)
                        {
                            //  pvn.Notu = notu.Notu;
                            pvn.Notu = NotuHarfeCevirDevamsizGirmedi(notu.NotDeger);
                            pvn.OgrenciNotID = notu.ID;
                        }
                        else
                        {
                            pvn.Notu = "";
                            pvn.OgrenciNotID = null;
                        }
                        ogPayNotBilgileri.OgrenciNotListesi.Add(pvn);
                    }
                    liste.Add(ogPayNotBilgileri);
                }
                return liste.OrderBy(d => d.Numara).ToList();
            }


        }

        private static string NotuHarfeCevirDevamsizGirmedi(double notu)
        {
            string notDeger = notu.ToString();
            string returnDeger = "";
            switch (notDeger)
            {
                case "120":
                    returnDeger = "GR";
                    break;
                case "130":
                    returnDeger = "DZ";
                    break;
                default:
                    returnDeger = notDeger;
                    break;
            }
            return returnDeger;
        }

        public static bool NotlariSilindiYap(List<int> ogrIDList, List<int> payIDList, string IP = "", string kullanici = "")
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    List<ABSOgrenciNot> grupNotListesi = new List<ABSOgrenciNot>();

                    foreach (var payID in payIDList)
                    {
                        grupNotListesi.AddRange(db.ABSOgrenciNot.Where(x => x.FKAbsPaylarID == payID && ogrIDList.Contains(x.FKOgrenciID) && x.Silindi == false).ToList());
                    }

                    foreach (var item in grupNotListesi)
                    {
                        item.Silindi = true;
                        item.SilindiTarih = DateTime.Now;
                        item.SilenKullanici = kullanici;
                        item.SilenIP = IP;
                    }
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool NotDetaylariniSilindiYap(List<int> ogrIDList, List<int> payIDList, string IP = "", string kullanici = "")
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    List<ABSOgrenciNotDetay> detayNotList = new List<ABSOgrenciNotDetay>();

                    foreach (var payID in payIDList)
                    {
                        detayNotList.AddRange(db.ABSOgrenciNotDetay.Where(x => x.FKABSPaylarID == payID && ogrIDList.Contains(x.FKOgrenciID.Value) && x.Silindi == false).ToList());
                    }

                    foreach (var item in detayNotList)
                    {
                        item.Silindi = true;
                        item.SilindiTarih = DateTime.Now;
                        item.SilenKullanici = kullanici;
                        item.SilenIP = IP;
                    }
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool AnaNotlariniSilindiYap(List<int> FKDersGrupIDList, int FKPaylarID, string IP = "", string kullanici = "")
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    List<ABSOgrenciNot> grupNotListesi = db.ABSOgrenciNot.Where(x => FKDersGrupIDList.Contains(x.FKDersGrupID.Value) && x.FKAbsPaylarID == FKPaylarID && x.Silindi == false).ToList();

                    foreach (var item in grupNotListesi)
                    {
                        item.Silindi = true;
                        item.SilindiTarih = DateTime.Now;
                        item.SilenKullanici = kullanici;
                        item.SilenIP = IP;
                    }
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool AnaNotDetaylariniSilindiYap(List<int> FKDersGrupIDList, int FKPaylarID, string IP = "", string kullanici = "")
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    List<ABSOgrenciNotDetay> detayNotList = db.ABSOgrenciNotDetay.Where(x => x.FKABSPaylarID == FKPaylarID && FKDersGrupIDList.Contains(x.FKDersGrupID.Value) && x.Silindi == false).ToList();

                    foreach (var item in detayNotList)
                    {
                        item.Silindi = true;
                        item.SilindiTarih = DateTime.Now;
                        item.SilenKullanici = kullanici;
                        item.SilenIP = IP;
                    }
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool NotKaydet(List<NotKayitVM> ogrenciNotList, string IP = "", string kullanici = "")
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                Guid yeniGuid = Guid.NewGuid();
                //List<ABSOgrenciNotLog> LOGList = new List<ABSOgrenciNotLog>();

                int grupID = ogrenciNotList.First().DersGrupID.Value;
                //int payID = ogrenciNotList.First().PayID;

                List<int> payIDList = ogrenciNotList.Select(x => x.PayID).Distinct().ToList();
                List<ABSPaylar> payList = db.ABSPaylar.Where(x => !x.Silindi && payIDList.Contains(x.ID)).ToList();

                var dersGrup = db.OGRDersGrup.FirstOrDefault(x => x.ID == grupID);
                int grupEnumDonem = dersGrup.EnumDonem.Value;

                List<int> ogrIDList = ogrenciNotList.Select(x => x.OgrenciID).ToList();

                bool sonHalVerildiMi = db.ABSDegerlendirme.Any(x => x.FKDersGrupID == grupID && x.SilindiMi == false && x.EnumSonHal == (int)EnumSonHal.EVET);
                //bool butunlemeMi = db.ABSPaylar.FirstOrDefault(x => x.ID == payID).EnumCalismaTip.Value == (int)EnumCalismaTip.Butunleme;

                try
                {
                    bool detayNotlarSilindiMi = NotDetaylariniSilindiYap(ogrIDList, payIDList, IP, kullanici);
                    bool notlarSilindiMi = NotlariSilindiYap(ogrIDList, payIDList, IP, kullanici);

                    if (!notlarSilindiMi || !detayNotlarSilindiMi)
                    {
                        return false;
                    }

                    foreach (var ogrenciNotu in ogrenciNotList)
                    {
                        if (sonHalVerildiMi && payList.FirstOrDefault(x => x.ID == ogrenciNotu.PayID).EnumCalismaTip != (int)Sabis.Enum.EnumCalismaTip.Butunleme)
                        {
                            continue;
                        }
                        ABSOgrenciNot ogrYeniNot = new ABSOgrenciNot();

                        if (!String.IsNullOrEmpty(ogrenciNotu.Notu))
                        {
                            ogrYeniNot.Zaman = DateTime.Now;
                            ogrYeniNot.Kullanici = kullanici;
                            ogrYeniNot.Makina = IP;
                            ogrYeniNot.AktarilanVeriMi = false;
                            ogrYeniNot.Yil = dersGrup.OgretimYili.Value;
                            ogrYeniNot.EnumDonem = dersGrup.EnumDonem.Value;
                            ogrYeniNot.FKOgrenciID = ogrenciNotu.OgrenciID;
                            ogrYeniNot.FKDersPlanID = ogrenciNotu.DersPlanID;
                            ogrYeniNot.FKDersPlanAnaID = ogrenciNotu.DersPlanAnaID;
                            ogrYeniNot.FKDersGrupID = ogrenciNotu.DersGrupID;
                            ogrYeniNot.FKAbsPaylarID = ogrenciNotu.PayID;
                            if (ogrenciNotu.Notu.ToUpper() == "GR")
                            {
                                ogrYeniNot.NotDeger = 120;
                            }
                            else if (ogrenciNotu.Notu.ToUpper() == "DZ")
                            {
                                ogrYeniNot.NotDeger = 130;
                            }
                            else
                            {
                                ogrYeniNot.NotDeger = Convert.ToDouble(ogrenciNotu.Notu);
                            }
                            ogrYeniNot.UZEMID = null;
                            ogrYeniNot.Silindi = false;
                            ogrYeniNot.GUID = yeniGuid;
                            db.ABSOgrenciNot.Add(ogrYeniNot);
                        }

                    }
                    db.SaveChanges();

                    #region degerlendirmeleri silindi yap
                    var grupIDList = ogrenciNotList.GroupBy(d => d.DersGrupID).Select(d => d.Key).ToList();
                    var degerlendirilmisler = db.ABSDegerlendirme.Where(d => grupIDList.Contains(d.FKDersGrupID.Value) && d.SilindiMi == false && d.EnumSonHal != (int)EnumSonHal.EVET).Select(x => x.OrtakDegerlendirmeGUID).Distinct().ToList();
                    foreach (var item in degerlendirilmisler)
                    {
                        var ortakGruplar = db.ABSDegerlendirme.Where(x => x.OrtakDegerlendirmeGUID == item).ToList();
                        foreach (var grupDegerlendirme in ortakGruplar)
                        {
                            grupDegerlendirme.SilindiMi = true;
                        }
                    }
                    db.SaveChanges();
                    #endregion

                    return true;
                }
                catch (Exception)
                {
                    return false;
                }

            }
        }



        public static List<AktarimSonuc> AnaNotKaydet(List<NotKayitVM> ogrenciNotList, int yil, int donem, string IP, string kullanici)
        {
            List<AktarimSonuc> result = new List<AktarimSonuc>();
            using (UYSv2Entities db = new UYSv2Entities())
            {
                Guid yeniG = Guid.NewGuid();

                //int? grupID = ogrenciNotList.First().DersGrupID;
                int fkDersPlanAnaID = ogrenciNotList.First().DersPlanAnaID;
                List<int> grupIDList = db.OGRDersGrup.Where(x => x.FKDersPlanAnaID == fkDersPlanAnaID && x.OgretimYili == yil).Select(x => x.ID).ToList();
                List<int> ogrIDList = ogrenciNotList.Select(x => x.OgrenciID).ToList();
                List<int> payIDList = ogrenciNotList.Select(x => x.PayID).Distinct().ToList();

                bool sonHalVerildi = false;
                bool detayNotlarSilindiMi = NotDetaylariniSilindiYap(ogrIDList, payIDList, IP, kullanici);
                bool notlarSilindiMi = NotlariSilindiYap(ogrIDList, payIDList, IP, kullanici);

                if (grupIDList.Count() > 0)
                {
                    sonHalVerildi = db.ABSDegerlendirme.Any(d => grupIDList.Contains(d.FKDersGrupID.Value) && d.SilindiMi == false && d.EnumSonHal == (int)EnumSonHal.EVET);
                }

                foreach (var ogrenciNotu in ogrenciNotList)
                {
                    if (sonHalVerildi)
                    {
                        continue;
                    }

                    ABSOgrenciNot ogrenciNot = new ABSOgrenciNot();

                    if (!String.IsNullOrEmpty(ogrenciNotu.Notu))
                    {
                        ogrenciNot.Zaman = DateTime.Now;
                        ogrenciNot.Kullanici = kullanici;
                        ogrenciNot.Makina = IP;
                        ogrenciNot.AktarilanVeriMi = false;
                        ogrenciNot.Yil = yil;
                        ogrenciNot.EnumDonem = 2;
                        ogrenciNot.FKOgrenciID = ogrenciNotu.OgrenciID;
                        if (ogrenciNotu.DersGrupID.HasValue)
                        {
                            ogrenciNot.FKDersGrupID = ogrenciNotu.DersGrupID;
                        }
                        if (ogrenciNotu.DersPlanID.HasValue)
                        {
                            ogrenciNot.FKDersPlanID = ogrenciNotu.DersPlanID;
                        }
                        ogrenciNot.FKDersPlanAnaID = ogrenciNotu.DersPlanAnaID;
                        ogrenciNot.FKAbsPaylarID = ogrenciNotu.PayID;

                        if (ogrenciNotu.Notu.ToUpper() == "GR")
                        {
                            ogrenciNot.NotDeger = 120;
                        }
                        else if (ogrenciNotu.Notu.ToUpper() == "DZ")
                        {
                            ogrenciNot.NotDeger = 130;
                        }
                        else
                        {
                            ogrenciNot.NotDeger = Convert.ToDouble(ogrenciNotu.Notu);
                        }
                        ogrenciNot.GUID = yeniG;
                        db.ABSOgrenciNot.Add(ogrenciNot);
                        db.SaveChanges();
                    }

                }

                var degerlendirilmisler = db.ABSDegerlendirme.Where(d => grupIDList.Contains(d.FKDersGrupID.Value) && d.SilindiMi == false && d.EnumSonHal != (int)EnumSonHal.EVET).Select(x => x.OrtakDegerlendirmeGUID).Distinct().ToList();
                foreach (var item in degerlendirilmisler)
                {
                    var ortakGruplar = db.ABSDegerlendirme.Where(x => x.OrtakDegerlendirmeGUID == item).ToList();
                    foreach (var grupDegerlendirme in ortakGruplar)
                    {
                        grupDegerlendirme.SilindiMi = true;
                    }
                }
                db.SaveChanges();

                return result;
            }
        }

        //public static ABSOgrenciNotLog ABSNotLogDonustur(ABSOgrenciNot notBilgisi, string IP, string kullaniciAdi, Sabis.Bolum.Core.ENUM.ABS.EnumABSNotIslemTipi islemTuru, Guid guid)
        //{
        //    ABSOgrenciNotLog yeniLog = new ABSOgrenciNotLog();
        //    yeniLog.Zaman = DateTime.Now;
        //    yeniLog.IP = IP;
        //    yeniLog.Kullanici = kullaniciAdi;
        //    yeniLog.NotDeger = notBilgisi.NotDeger;
        //    yeniLog.Yil = notBilgisi.Yil;
        //    yeniLog.Donem = notBilgisi.EnumDonem;
        //    yeniLog.FKOgrenciID = notBilgisi.FKOgrenciID;
        //    yeniLog.FKDersPlanID = notBilgisi.FKDersPlanID;
        //    yeniLog.FKDersPlanAnaID = notBilgisi.FKDersPlanAnaID;
        //    yeniLog.FKDersGrupID = notBilgisi.FKDersGrupID;
        //    yeniLog.FKAbsPaylarID = notBilgisi.FKAbsPaylarID.Value;
        //    yeniLog.EnumNotIslemTipi = (int)islemTuru;
        //    yeniLog.FKABSOgrenciNotID = notBilgisi.ID;
        //    yeniLog.UZEMID = notBilgisi.UZEMID;
        //    yeniLog.IslemGUID = guid;
        //    return yeniLog;
        //}
        public static List<OgrenciListesiVM> ListeleOgrenciNotlari(int dersGrupID, string listPayID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                List<int> idlist = new List<int>();
                string[] dizi = listPayID.Split('-');
                for (int i = 0; i < dizi.Length; i++)
                {
                    idlist.Add(Convert.ToInt32(dizi[i]));
                }

                List<OgrenciListesiVM> liste = new List<OgrenciListesiVM>();

                List<ABSPaylar> payListesi = db.ABSPaylar.Where(d => !d.Silindi && idlist.Contains(d.ID)).ToList();

                var rawSQL = db.OGROgrenciYazilma.AsQueryable();
                bool butunlemeMi = db.OGROgrenciYazilma.Any(x => x.FKDersGrupID == dersGrupID && x.Iptal != true && x.ButunlemeMi == true);

                if (butunlemeMi)
                {
                    rawSQL = rawSQL.Where(x => (x.ButunlemeMi.HasValue && x.ButunlemeMi.Value == true));
                }
                var sorgu = from yazilma in rawSQL
                            where yazilma.FKDersGrupID == dersGrupID && yazilma.Iptal == false && yazilma.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL
                            select new
                            {
                                Numara = yazilma.OGRKimlik.Numara,
                                Ad = yazilma.OGRKimlik.Kisi.Ad,
                                Soyad = yazilma.OGRKimlik.Kisi.Soyad,
                                OgrenciID = yazilma.OGRKimlik.ID,
                                DersPlanAnaID = yazilma.OGRDersGrup.OGRDersPlan.FKDersPlanAnaID.Value,
                                DersGrupID = yazilma.FKDersGrupID.Value,
                                DersPlanID = yazilma.OGRDersGrup.FKDersPlanID.Value,
                                //Notlar = db.ABSOgrenciNot.Where(x => x.FKOgrenciID == yazilma.FKOgrenciID && x.FKDersGrupID == dersGrupID && x.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL).OrderBy(d => d.ID).ToList(),
                                Notlar = yazilma.OGRDersGrup.ABSOgrenciNot.Where(x => x.Silindi == false && x.FKOgrenciID == yazilma.FKOgrenciID && x.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL).OrderBy(d => d.ID),
                                YazilmaID = yazilma.ID,
                            };
                foreach (var ogrenciYazilma in sorgu)
                {
                    OgrenciListesiVM ogPayNotBilgileri = new OgrenciListesiVM();

                    ogPayNotBilgileri.Ad = ogrenciYazilma.Ad;
                    ogPayNotBilgileri.Soyad = ogrenciYazilma.Soyad;
                    ogPayNotBilgileri.DersGrupID = ogrenciYazilma.DersGrupID;
                    ogPayNotBilgileri.DersPlanID = ogrenciYazilma.DersPlanID;
                    ogPayNotBilgileri.OgrenciID = Convert.ToInt32(ogrenciYazilma.OgrenciID);
                    ogPayNotBilgileri.DersPlanAnaID = Convert.ToInt32(ogrenciYazilma.DersPlanAnaID);
                    ogPayNotBilgileri.Numara = ogrenciYazilma.Numara;

                    foreach (var pay in payListesi)
                    {
                        bool sistemTarihKontrol = true;

                        OgrenciNotlariVM pvn = new OgrenciNotlariVM();
                        pvn.PayID = pay.ID;
                        pvn.EnumCalismaTip = pay.EnumCalismaTip.Value;
                        pvn.Sira = pay.Sira;
                        pvn.Aktif = sistemTarihKontrol;
                        pvn.Oran = pay.Oran;


                        ABSOgrenciNot notu = ogrenciYazilma.Notlar.Where(d => d.Silindi == false && d.FKOgrenciID == ogPayNotBilgileri.OgrenciID && d.FKAbsPaylarID == pay.ID && d.FKDersGrupID == dersGrupID && d.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL).FirstOrDefault();

                        if (notu != null)
                        {
                            //  pvn.Notu = notu.Notu;
                            pvn.Notu = NotuHarfeCevirDevamsizGirmedi(notu.NotDeger);
                            pvn.OgrenciNotID = notu.ID;
                        }
                        else
                        {
                            pvn.Notu = "";
                            pvn.OgrenciNotID = null;
                        }
                        ogPayNotBilgileri.OgrenciNotListesi.Add(pvn);
                    }
                    liste.Add(ogPayNotBilgileri);
                }
                return liste.OrderBy(d => d.Numara).ToList();
            }
        }
        public static List<OgrenciListesiv2VM> ListeleOgrenciNotlariv2(NotGirisListParamModel model)
        {
            UYSv2Entities dbref = new UYSv2Entities();
            using (UYSv2Entities db = new UYSv2Entities())
            {
                List<OgrenciListesiv2VM> liste = new List<OgrenciListesiv2VM>();

                List<ABSPaylar> payListesi = db.ABSPaylar.Where(d => !d.Silindi && model.PayIDList.Contains(d.ID)).ToList();
                var yazilmaSorgu = db.OGROgrenciYazilma.Where(yazilma => yazilma.Iptal == false && yazilma.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL).AsQueryable();
                List<OgrenciListesiv2VM> OgrenciIDList = new List<OgrenciListesiv2VM>();
                if (model.GrupID.HasValue)
                {
                    yazilmaSorgu = yazilmaSorgu.Where(d => d.FKDersGrupID == model.GrupID);
                }
                else
                {
                    var altGrupList = DERS.DERSMAIN.GetirAltDersGrupIDList(ref dbref, model.DersPlanAnaID, model.Yil, model.Donem);
                    yazilmaSorgu = yazilmaSorgu.Where(d => altGrupList.Contains(d.FKDersGrupID.Value));
                }
                OgrenciIDList = yazilmaSorgu.Select(d => new OgrenciListesiv2VM
                {
                    Numara = d.OGRKimlik.Numara,
                    Ad = d.OGRKimlik.Kisi.Ad,
                    Soyad = d.OGRKimlik.Kisi.Soyad,
                    OgrenciID = d.FKOgrenciID.Value,
                    DersPlanAnaID = d.OGRDersGrup.OGRDersPlan.FKDersPlanAnaID.Value,
                    DersGrupID = d.FKDersGrupID.Value,
                    DersPlanID = d.OGRDersGrup.FKDersPlanID.Value,
                    YazilmaID = d.ID,
                    ButunlemeMi = d.ButunlemeMi
                }).ToList();


                Dictionary<int, OgrenciListesiv2VM> dict = new Dictionary<int, OgrenciListesiv2VM>();
                foreach (var item in OgrenciIDList)
                {
                    if (!dict.ContainsKey(item.OgrenciID))
                    {
                        if (model.GrupID.HasValue == false)
                        {
                            item.DersGrupID = null;
                            item.DersPlanID = null;
                            item.YazilmaID = null;
                        }
                        dict.Add(item.OgrenciID, item);
                    }
                }
                foreach (var ogrenci in dict)
                {
                    OgrenciListesiv2VM ogPayNotBilgileri = ogrenci.Value;

                    bool ogrenciButunlemeMi = false;
                    bool payButunlemeMi = false;

                    if (ogrenci.Value.ButunlemeMi == true)
                    {
                        ogrenciButunlemeMi = true;
                    }

                    foreach (var pay in payListesi)
                    {
                        bool sistemTarihKontrol = true;

                        if (pay.EnumCalismaTip == (int)Sabis.Enum.EnumCalismaTip.Butunleme)
                        {
                            payButunlemeMi = true;
                        }

                        OgrenciNotlariVM pvn = new OgrenciNotlariVM();
                        pvn.PayID = pay.ID;
                        pvn.EnumCalismaTip = pay.EnumCalismaTip.Value;
                        pvn.Sira = pay.Sira;
                        pvn.Aktif = sistemTarihKontrol;
                        pvn.Oran = pay.Oran;
                        pvn.DersGrupID = ogrenci.Value.DersGrupID;

                        ABSOgrenciNot notu;

                        var notSorgu = db.ABSOgrenciNot.Where(d => d.FKOgrenciID == ogPayNotBilgileri.OgrenciID && d.FKAbsPaylarID == pay.ID && d.Silindi == false).AsQueryable();

                        if (model.GrupID.HasValue)
                        {
                            notu = notSorgu.Where(x => x.FKDersGrupID == model.GrupID.Value).FirstOrDefault();
                        }
                        else
                        {
                            notu = notSorgu.Where(x => x.FKDersPlanAnaID == model.DersPlanAnaID).FirstOrDefault();
                        }

                        if (notu != null)
                        {
                            pvn.Notu = NotuHarfeCevirDevamsizGirmedi(notu.NotDeger);
                            pvn.OgrenciNotID = notu.ID;
                        }
                        else
                        {
                            pvn.Notu = "";
                            pvn.OgrenciNotID = null;
                        }
                        ogPayNotBilgileri.OgrenciNotListesi.Add(pvn);
                    }
                    if (!payButunlemeMi)
                    {
                        liste.Add(ogPayNotBilgileri);
                    }
                    else
                    {
                        if (ogrenciButunlemeMi)
                        {
                            liste.Add(ogPayNotBilgileri);
                        }
                    }
                }
                return liste.OrderBy(d => d.Numara).ToList();
            }
        }

        public static Sabis.Bolum.Core.ABS.DERS.DERSDETAY.PayNotAdetVM GetirPayNotAdet(int payID, int yil, int donem, int grupID = 0)
        {
            Sabis.Bolum.Core.ABS.DERS.DERSDETAY.PayNotAdetVM result = new Sabis.Bolum.Core.ABS.DERS.DERSDETAY.PayNotAdetVM();
            UYSv2Entities db = new UYSv2Entities();
            var pay = db.ABSPaylar.FirstOrDefault(x => !x.Silindi && x.ID == payID);
            if (pay == null)
            {
                throw new Exception("Geçersiz pay ID!");
            }

            int dersPlanAnaID = pay.FKDersPlanAnaID.Value;
            result.PayID = pay.ID;



            //Ders grubu seçilerek geldiyse gruba göre
            if (grupID > 0)
            {
                var grup = db.OGRDersGrup.First(x => x.ID == grupID);
                result.Girilen = db.ABSOgrenciNot.Where(x => x.FKAbsPaylarID == payID && x.FKDersGrupID == grupID && x.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL && x.Silindi == false).Count();
                result.Toplam = db.OGROgrenciYazilma.Count(x => x.FKDersGrupID == grupID && x.Iptal == false && x.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL);
            }
            //Hiyerarşik bir ders seçilerek geldiyse
            else
            {
                result.Girilen = db.ABSOgrenciNot.Where(x => x.FKAbsPaylarID == payID && x.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL && x.Silindi == false).Select(x => x.FKOgrenciID).Distinct().Count();
            }
            db.Dispose();
            db = null;
            return result;
        }
    }
}
