﻿using Sabis.Bolum.Core.ABS.NOT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sabis.Bolum.Bll.DATA.UysModel;

namespace Sabis.Bolum.Bll.SOURCE.ABS.NOT
{
    public class NOTAKTARIM
    {
        //public static List<AktarimSonuc> Aktar(List<NotKayitVM> notList, int yil, int donem)
        //{
        //    List<AktarimSonuc> result = new List<AktarimSonuc>();
        //    var notListHatali = notList.Where(x => !(x.DersGrupID.HasValue && x.DersPlanAnaID > 0 && x.UzemID.HasValue && x.OgrenciID > 0 && !string.IsNullOrEmpty(x.Notu)));
        //    foreach (var item in notListHatali)
        //    {
        //        var hata = new AktarimSonuc();
        //        hata.EnumAktarimDurum = EnumAktarimDurum.Hata;
        //        hata.OgrenciNot = item;
        //        hata.Hata = "Parametre hatası";
        //        result.Add(hata);
        //    }
        //    var notListFiltreli = notList.Where(x => x.DersGrupID.HasValue && x.DersPlanAnaID > 0 && x.UzemID.HasValue && x.OgrenciID > 0 && !string.IsNullOrEmpty(x.Notu));
        //    var grupNotList = notListFiltreli.GroupBy(x => x.DersGrupID.Value);
        //    foreach (var item in grupNotList)
        //    {
        //        int grupID = item.Key;
        //        var grupResult = NOTMAIN.NotKaydetv2(item.ToList(), yil, donem, "UZEM", "UZEM");
        //        var payGrup = item.GroupBy(x => x.PayID).Select(x => x.Key);
        //        foreach (var pay in payGrup)
        //        {
        //            DERS.PAYMAIN.YayinlaPay(pay, grupID, "uzem", false);
        //        }
        //        result.AddRange(grupResult);
        //    }
        //    return result;
        //}

        public static List<int> GetirSinavMerkeziDersList()
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return db.ABSOgrenciNot.Where(a => a.Kullanici == "SINAVAKTARIM15052017").Select(x => x.FKDersPlanAnaID).Distinct().ToList();
            }
        }
    }
}
