﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sabis.Bolum.Core.ABS.NOT;
using Sabis.Bolum.Bll.DATA.UysModel;


namespace Sabis.Bolum.Bll.SOURCE.ABS.NOT
{
    public class HIYERARSIKMAIN
    {
        public static List<DersPayListVM> PayGetir(int FKDersPlanAnaID, int yil, int EnumDonem)
        {
            //var list = GetirDersPayList(FKDersPlanAnaID, yil, EnumDonem);
            return GetirDersPayList(FKDersPlanAnaID, yil, EnumDonem);
        }

        private static List<DersPayListVM> GetirDersPayList(int dersPlanAnaID, int yil, int donem)
        {
            List<DersPayListVM> result = new List<DersPayListVM>();
            UYSv2Entities db = new UYSv2Entities();
            var altDersler = GetirAltDPAna(ref db, dersPlanAnaID);
            foreach (var ders in altDersler)
            {
                DersPayListVM dersPay = new DersPayListVM();
                dersPay.DersPlanAnaID = ders.Key;
                dersPay.DersAd = ders.Value;
                dersPay.PayList = db.ABSPaylar.Where(x => !x.Silindi && x.FKDersPlanAnaID == ders.Key && x.Yil == yil &&
                                                            !(
                                                                x.EnumCalismaTip == (int)Sabis.Enum.EnumCalismaTip.AltDersinFinalKatkisi ||
                                                                x.EnumCalismaTip == (int)Sabis.Enum.EnumCalismaTip.AltDersinYiliciKatkisi ||
                                                                x.EnumCalismaTip == (int)Sabis.Enum.EnumCalismaTip.YilIcininBasariya
                                                            )
                                                    )
                .Select(x => new Sabis.Bolum.Core.ABS.DERS.DERSDETAY.PayHiyerarsikVM() { ID = x.ID, EnumCalismaTip = x.EnumCalismaTip, Oran = x.Oran, Sira = x.Sira, FKDersPlanAnaID = x.FKDersPlanAnaID, Pay = x.Pay, Payda = x.Payda, FKDersGrupID = x.OGRDersGrup.ID, FKDersPlanID = x.OGRDersPlan.ID }).ToList();
                result.Add(dersPay);
            }
            return result;
        }

        public static List<Sabis.Bolum.Core.ABS.OGRENCI.HiyerarsikOgrenciList> GetirOgrenciYazilmaByDersPlanAnaID(int dersPlanAnaID, int yil, int donem)
        {
            var result = new List<Sabis.Bolum.Core.ABS.OGRENCI.HiyerarsikOgrenciList>();
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var dersPlanAna = db.OGRDersPlanAna.FirstOrDefault(x => x.ID == dersPlanAnaID);
                if (dersPlanAna != null)
                {
                    var sorgu = db.OGROgrenciYazilma.Where(x => !x.Iptal &&
                (x.Yil == yil && (x.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.EnumDersSureTip == (int)Sabis.Enum.EnumDersSureTip.YILLIK || (x.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.EnumDersSureTip == (int)Sabis.Enum.EnumDersSureTip.DONEMLIK && x.EnumDonem == donem)))
               ).AsQueryable();
                    if (dersPlanAna.Hiyerarsik)
                    {
                        if (dersPlanAna.FKHiyerarsikKokID.HasValue)
                        {
                            sorgu = sorgu.Where(x => x.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.FKHiyerarsikKokID == dersPlanAna.FKHiyerarsikKokID.Value);
                        }
                        else
                        {
                            sorgu = sorgu.Where(x => x.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.FKHiyerarsikKokID == dersPlanAna.ID);
                        }
                    }
                    else
                    {
                        sorgu = sorgu.Where(x => x.OGRDersGrup.OGRDersPlan.FKDersPlanAnaID == dersPlanAna.ID);
                    }

                    result = sorgu.Select(x => new Sabis.Bolum.Core.ABS.OGRENCI.HiyerarsikOgrenciList
                    {
                        OgrenciID = x.FKOgrenciID.Value,
                        Numara = x.OGRKimlik.Numara,
                        Ad = x.OGRKimlik.Kisi.Ad,
                        Soyad = x.OGRKimlik.Kisi.Soyad
                    }).Distinct().ToList();
                }
            }
            return result;
        }

        public static List<Sabis.Bolum.Core.ABS.OGRENCI.HiyerarsikOgrenciList> GetirOgrenciYazilmaByKokDersPlanAnaID(int kokDersPlanAnaID, int yil, int donem)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var list = db.OGROgrenciYazilma.Where(x => !x.Iptal &&
                (x.Yil == yil && (x.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.EnumDersSureTip == (int)Sabis.Enum.EnumDersSureTip.YILLIK || (x.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.EnumDersSureTip == (int)Sabis.Enum.EnumDersSureTip.DONEMLIK && x.EnumDonem == donem)))
                && x.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.FKHiyerarsikKokID == kokDersPlanAnaID).Select(x => new Sabis.Bolum.Core.ABS.OGRENCI.HiyerarsikOgrenciList
                {
                    OgrenciID = x.FKOgrenciID.Value,
                    Numara = x.OGRKimlik.Numara,
                    Ad = x.OGRKimlik.Kisi.Ad,
                    Soyad = x.OGRKimlik.Kisi.Soyad,
                    FKDersPlanAnaID = x.OGRDersGrup.OGRDersPlan.FKDersPlanAnaID
                }).Distinct().ToList();
                return list;
            }
        }

        private static Dictionary<int, string> GetirAltDPAna(ref UYSv2Entities db, int dersPlanAnaID)
        {
            Dictionary<int, string> result = new Dictionary<int, string>();
            var dpAna = db.OGRDersPlanAna.FirstOrDefault(x => x.ID == dersPlanAnaID);
            result.Add(dpAna.ID, dpAna.DersAd);
            var altDPAnaList = db.OGRDersPlanAna.Where(x => x.FKDersPlanAnaUstID == dpAna.ID).Select(x => x.ID).ToList();
            foreach (var item in altDPAnaList)
            {
                var altList = GetirAltDPAna(ref db, item);
                foreach (var dpAlt in altList)
                {
                    result.Add(dpAlt.Key, dpAlt.Value);
                }
            }
            return result;
        }
    }
}
