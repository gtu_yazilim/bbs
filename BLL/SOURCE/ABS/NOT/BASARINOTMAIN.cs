﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sabis.Enum;
using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Bolum.Core.ABS.NOT;
using Sabis.Bolum.Core.ABS.DEGERLENDIRME;
using Sabis.Bolum.Core.ABS.DERS.PROJE;

namespace Sabis.Bolum.Bll.SOURCE.ABS.NOT
{
    public class BASARINOTMAIN
    {
        public static List<OgrenciListesiVM> GetirBasariNotListesi(int dersGrupID, int personelID, bool butunlemeMi)
        {
            List<OgrenciListesiVM> result = null;
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {

                    bool yayinlandiMi = db.ABSYayinlananBasariNotlar.Any(x => x.FKDersGrupID == dersGrupID && x.Yayinlandi == true && x.ButunlemeMi == butunlemeMi);

                    var sqlRAW = db.OGROgrenciYazilma.Where(a=>a.OGROgrenciNot.EnumOgrenciBasariNot!=(int)EnumOgrenciBasariNot.W).AsQueryable();
                    if (butunlemeMi)
                    {
                        sqlRAW = sqlRAW.Where(x => x.ButunlemeMi.HasValue && x.ButunlemeMi.Value == true);
                    }
                    var resultSQL = from yazilma in sqlRAW
                                    where yazilma.FKDersGrupID == dersGrupID && yazilma.Iptal == false && yazilma.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL
                                    orderby yazilma.OGRKimlik.Numara
                                    select new
                                    {
                                        Numara = yazilma.OGRKimlik.Numara,
                                        Ad = yazilma.OGRKimlik.Kisi.Ad,
                                        Soyad = yazilma.OGRKimlik.Kisi.Soyad,
                                        OgrenciID = yazilma.OGRKimlik.ID,
                                        DersPlanAnaID = yazilma.OGRDersPlanAna.ID,
                                        DersGrupID = yazilma.FKDersGrupID,
                                        DersPlanID = yazilma.FKDersPlanID,
                                        PersonelID = personelID,
                                        BasariNot = yazilma.OGRDersGrup.ABSBasariNot.FirstOrDefault(x => x.FKOgrenciID == yazilma.FKOgrenciID && x.ButunlemeMi == butunlemeMi),
                                        ButunlemeOncesiBasariNot = yazilma.OGRDersGrup.ABSBasariNot.FirstOrDefault(x => x.FKOgrenciID == yazilma.FKOgrenciID && x.ButunlemeMi == false), // sadece butunlemeMi == true iken anlamlı veri olacak
                                        YazilmaID = yazilma.ID,
                                        //Raporlar = (from p in db.ABSProjeOgrenci
                                        //            where p.OgrenciID == yazilma.FKOgrenciID
                                        //            select new
                                        //            {
                                        //                OgrenciRapor = p.ABSProjeRapor.Where(x => x.FKProjeOgrenciID == p.ID)
                                        //            }).ToList(),
                                        Raporlar = (from p in db.ABSProjeOgrenci
                                                     join r in db.ABSProjeRapor on p.ID equals r.FKProjeOgrenciID
                                                     where 
                                                        p.OgrenciID == yazilma.FKOgrenciID &&
                                                        p.ABSProjeTakvim.FKDersGrupID == dersGrupID
                                                    select new ProjeRaporDto
                                                     {
                                                         ID = r.ID,
                                                         FKProjeOgrenciID = r.FKProjeOgrenciID,
                                                         HocaAciklama = r.HocaAciklama,
                                                         ProjeDosyaKey = r.DosyaKey,
                                                         RaporDurum = r.RaporDurum,
                                                         RaporNo = r.RaporNo
                                                     }).ToList()

                                    };

                    //if (bolumBaskaniMi == false)
                    //{
                    //    resultSQL = resultSQL.Where(d => d.OgrenciID);
                    //}
                    result = new List<OgrenciListesiVM>();
                    foreach (var item in resultSQL)
                    {
                        OgrenciListesiVM kayit = new OgrenciListesiVM();
                        kayit.Ad = item.Ad;
                        kayit.Soyad = item.Soyad;
                        if (item.BasariNot == null)
                        {
                            kayit.BasariHarf = (int)EnumHarfBasari.BELIRSIZ;
                            kayit.MutlakHarf = (int)EnumHarfMutlak.BELIRSIZ;
                        }
                        else
                        {
                            kayit.BasariHarf = item.BasariNot.EnumHarfBasari;
                            kayit.MutlakHarf = item.BasariNot.EnumHarfMutlak;
                        }
                        kayit.YazilmaID = item.YazilmaID;
                        kayit.DersGrupID = item.DersGrupID.Value;
                        kayit.DersPlanAnaID = item.DersPlanAnaID;
                        kayit.DersPlanID = item.DersPlanID.Value;

                        kayit.Numara = item.Numara;
                        kayit.OgrenciID = item.OgrenciID;
                        kayit.PersonelID = item.PersonelID;

                        kayit.YayinlandiMi = yayinlandiMi;
                        kayit.OgrenciRaporlar = item.Raporlar;
                        kayit.ButunlemeMi = butunlemeMi;
                        


                        if (item.ButunlemeOncesiBasariNot == null)
                        {
                            kayit.ButunlemeOncesiMutlakHarf = (int)EnumHarfMutlak.BELIRSIZ;
                        }
                        else
                        {
                            kayit.ButunlemeOncesiMutlakHarf = item.ButunlemeOncesiBasariNot.EnumHarfMutlak;
                        }


                        result.Add(kayit);
                    }
                }
            }
            catch (Exception e)
            {

            }


            return result;

        }
        public static BasariNotDegerlendirmeVM KaydetBasariNot(BasariNotKayitVM basariNotKayit, string kullaniciAdi, string IPAdresi)
        {
            try
            {
                List<BasariNotDegerlendirmeVM> DegerlendirmeKayit = new List<BasariNotDegerlendirmeVM>();
                BasariNotDegerlendirmeVM degerlendirme = new BasariNotDegerlendirmeVM();
                degerlendirme.BagilOrtalama = 0;
                degerlendirme.BarkodNo = null;
                degerlendirme.ButunlemeMi = basariNotKayit.ButunlemeMi;
                degerlendirme.EnBuyukNot = 0;
                degerlendirme.EnumDonem = basariNotKayit.Donem;
                degerlendirme.EnumSonHal = (int)EnumSonHal.HAYIR;
                degerlendirme.FKDersGrupID = basariNotKayit.DersGrupID;
                degerlendirme.FKPersonelID = basariNotKayit.PersonelID;
                degerlendirme.Kullanici = kullaniciAdi;
                degerlendirme.Makina = IPAdresi;
                Guid guid = Guid.NewGuid();
                degerlendirme.OrtakDegerlendirmeGUID = guid;
                degerlendirme.Ortalama = 0;
                degerlendirme.StandartSapma = 0;
                degerlendirme.Tarih = DateTime.Now;
                degerlendirme.UstDeger = 100;
                degerlendirme.Yil = basariNotKayit.Yil;
                degerlendirme.Zaman = DateTime.Now;
                DegerlendirmeKayit.Add(degerlendirme);
                int degerlendirmeID = KaydetDegerlendirme(DegerlendirmeKayit);
                BasariNotKayit(basariNotKayit, degerlendirmeID);
                return degerlendirme;
                //EnumDegerlendirmeAsama = AbisDTO.Degerlendirme.EnumDegerlendirmeAsama.KAYITLI;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static int KaydetDegerlendirme(List<BasariNotDegerlendirmeVM> DegerlendirmeKayit)
        {
            int result = 0;
            using (UYSv2Entities db = new UYSv2Entities())
            {
                foreach (var item in DegerlendirmeKayit)
                {
                    bool yeniMi = false;
                    ABSDegerlendirme kayit = db.ABSDegerlendirme.FirstOrDefault(x => x.FKDersGrupID == item.FKDersGrupID && x.ButunlemeMi == item.ButunlemeMi && x.SilindiMi == false);
                    if (kayit == null)
                    {
                        yeniMi = true;
                        kayit = new ABSDegerlendirme();
                    }
                    kayit.ButunlemeMi = item.ButunlemeMi;
                    kayit.BagilOrtalama = item.BagilOrtalama;
                    kayit.BarkodNo = item.BarkodNo;
                    kayit.EnBuyukNot = item.EnBuyukNot;
                    kayit.EnumDonem = item.EnumDonem;
                    kayit.EnumSonHal = item.EnumSonHal;
                    kayit.FKDersGrupID = item.FKDersGrupID;
                    kayit.FKPersonelID = item.FKPersonelID;
                    kayit.Kullanici = item.Kullanici;
                    kayit.Makina = item.Makina;
                    kayit.OrtakDegerlendirmeGUID = item.OrtakDegerlendirmeGUID;
                    kayit.Ortalama = item.Ortalama;
                    kayit.StandartSapma = item.StandartSapma;
                    kayit.Tarih = item.Tarih;
                    kayit.UstDeger = item.UstDeger;
                    kayit.Yil = item.Yil;
                    kayit.Zaman = item.Zaman;
                    kayit.SilindiMi = false;

                    if (yeniMi)
                    {
                        db.ABSDegerlendirme.Add(kayit);
                    }
                    db.SaveChanges();
                    result = kayit.ID;
                }
            }
            return result;
        }
        public static void BasariNotKayit(BasariNotKayitVM basariNotKayit, int degerlendirmeID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                Dictionary<int, ABSBasariNot> ogrBasariNotDict = db.ABSBasariNot.Where(x => x.FKDersGrupID == basariNotKayit.DersGrupID && x.ButunlemeMi == basariNotKayit.ButunlemeMi).ToDictionary(k => k.FKOgrenciID.Value, v => v);
                if (ogrBasariNotDict.Count == 0)
                {
                    ogrBasariNotDict = new Dictionary<int, ABSBasariNot>();
                }
                foreach (OgrenciListesiVM ogrNot in basariNotKayit.ListBasariNot)
                {
                    bool yeniMi = false;
                    ABSBasariNot basariNot = null;
                    if (!ogrBasariNotDict.Any(x => x.Key == ogrNot.OgrenciID))
                    {
                        yeniMi = true;
                        basariNot = new ABSBasariNot();
                    }
                    else
                    {
                        basariNot = ogrBasariNotDict.First(x => x.Key == ogrNot.OgrenciID).Value;
                    }
                    basariNot.Zaman = DateTime.Now;
                    basariNot.Bagil = (int)ogrNot.BasariHarf;
                    basariNot.Mutlak = (int)ogrNot.BasariHarf;
                    basariNot.EnumDonem = basariNotKayit.Donem;
                    basariNot.Yil = basariNotKayit.Yil;
                    basariNot.Tarih = DateTime.Now;
                    basariNot.EnumHarfBasari = (int)ogrNot.BasariHarf;
                    basariNot.EnumHarfMutlak = (int)ogrNot.BasariHarf;
                    basariNot.Bagil = (int)ogrNot.BasariHarf;
                    basariNot.FKDersGrupID = basariNotKayit.DersGrupID;
                    basariNot.FKOgrenciID = ogrNot.OgrenciID;
                    basariNot.Kullanici = basariNotKayit.KullaniciAdi;
                    basariNot.Makina = basariNotKayit.IPAdresi;
                    basariNot.ButunlemeMi = basariNotKayit.ButunlemeMi;
                    basariNot.SilindiMi = false;
                    basariNot.FKDegerlendirmeID = degerlendirmeID;
                    if (yeniMi)
                    {
                        db.ABSBasariNot.Add(basariNot);
                    }
                }
                db.SaveChanges();
            }
        }

        public static bool BasariNotYayinIslem(int fkDersPlanAnaID, int fkDersGrupID, bool yayinDurum)
        {
            try
            {


                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var grup = db.OGRDersGrup.FirstOrDefault(x => x.ID == fkDersGrupID);
                    if (db.ABSYayinlananBasariNotlar.Any(x => x.FKDersGrupID == fkDersGrupID && x.Yil == grup.OgretimYili && x.EnumDonem == grup.EnumDonem))
                    {
                        var yayin = db.ABSYayinlananBasariNotlar.FirstOrDefault(x => x.FKDersGrupID == fkDersGrupID && x.Yil == grup.OgretimYili && x.EnumDonem == grup.EnumDonem);

                        yayin.Yayinlandi = yayinDurum;
                        db.SaveChanges();
                    }
                    else
                    {
                        ABSYayinlananBasariNotlar yeniYayin = new ABSYayinlananBasariNotlar();

                        yeniYayin.FKDersGrupID = fkDersGrupID;
                        yeniYayin.YayinlanmaTarihi = DateTime.Now;
                        yeniYayin.Yayinlandi = yayinDurum;
                        yeniYayin.Yil = grup.OgretimYili;
                        yeniYayin.EnumDonem = grup.EnumDonem;
                        yeniYayin.FKDersPlanAnaID = fkDersPlanAnaID;

                        db.ABSYayinlananBasariNotlar.Add(yeniYayin);
                        db.SaveChanges();
                    }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
