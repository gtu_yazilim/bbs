﻿using Sabis.Bolum.Core.RAPOR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sabis.Bolum.Core.ENUM.RAPOR;
using Sabis.Enum;
using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Bolum.Bll.SOURCE.ABS.RAPOR.exceptions;

namespace Sabis.Bolum.Bll.SOURCE.ABS.RAPOR
{
    public class RaporDataRepo : IRaporDataRepo
    {
        public Tuple<string, string, EnumRaporServisTuru> GetirRaporServisKey(EnumBelgeTuru enumBelgeTuru, List<int> HiyerarsikBirimIDList = null, EnumOgretimSeviye? enumSeviye = null, EnumDil enumDil = EnumDil.TURKCE)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    OGRRaporTanim dbKayit = null;
                    List<OGRRaporTanim> dbKayitList = null;
                    dbKayitList = db.OGRRaporTanim
                        .Where(x => x.EnumBelgeTuru == (int)enumBelgeTuru && x.EnumDil == (int)enumDil)
                        .OrderBy(x => x.EnumRaporServisTuru) // önce servis türü 1 olanlar gelsin (jsreport)
                        .ThenByDescending(f => f.EnumOgretimSeviye == null)// daha sonra null olanlar gelsin (enumSeviye parametresi null gelirse alt method önce null olan tasarımı görsün null yoksa bir sonrakine gitsin)
                        .ThenBy(x => x.EnumOgretimSeviye)
                        .ToList();
                    //Verilen dil parametresine göre rapor bulunamazsa dil olmadan ara
                    if (!dbKayitList.Any())
                    {
                        dbKayitList = db.OGRRaporTanim.Where(x => x.EnumBelgeTuru == (int)enumBelgeTuru).ToList();
                    }

                    foreach (var birimID in HiyerarsikBirimIDList)
                    {
                        dbKayit = dbKayitList.FirstOrDefault(x => x.FKBirimID == birimID && (enumSeviye.HasValue ? (x.EnumOgretimSeviye.HasValue && x.EnumOgretimSeviye == (int?)enumSeviye) : true));
                        if (dbKayit != null)
                        {
                            break;
                        }
                    }

                    if (dbKayit == null)
                    {
                        //Birimin seviyesi olduğu halde tasarım bulunamazsa seviyesiz olarak ara
                        if (enumSeviye.HasValue)
                        {
                            foreach (var birimID in HiyerarsikBirimIDList)
                            {
                                dbKayit = dbKayitList.FirstOrDefault(x => x.FKBirimID == birimID && x.EnumOgretimSeviye == null);
                                if (dbKayit != null)
                                {
                                    break;
                                }
                            }
                        }
                        if (dbKayit == null)
                            throw new DBRecordNotFoundException("OIS-101:Belge türüne ait tasarim bulunamadı.");
                    }

                    return new Tuple<string, string, EnumRaporServisTuru>(dbKayit.FKRaporTasarimKey, dbKayit.FKRaporTasarimArkaKey, EnumRaporServisTuru.JSReport);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
