﻿using Sabis.Core.Exception;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Bll.SOURCE.ABS.RAPOR.exceptions
{
    public class DBRecordSaveException : SabisException
    {
        public DBRecordSaveException()
        {
        }

        public DBRecordSaveException(string message) : base(message)
        {
        }

        public DBRecordSaveException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected DBRecordSaveException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
