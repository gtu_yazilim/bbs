﻿using Sabis.Core.Exception;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Bll.SOURCE.ABS.RAPOR.exceptions
{
    public class DBRecordDeleteException : SabisException
    {
        public DBRecordDeleteException()
        {
        }

        public DBRecordDeleteException(string message) : base(message)
        {
        }

        public DBRecordDeleteException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected DBRecordDeleteException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
