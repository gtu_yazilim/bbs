﻿using Sabis.Core.Exception;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Bll.SOURCE.ABS.RAPOR.exceptions
{
    public class DBRecordNotFoundException : SabisException
    {
        public DBRecordNotFoundException()
        {
        }

        public DBRecordNotFoundException(string message) : base(message)
        {
        }

        public DBRecordNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected DBRecordNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
