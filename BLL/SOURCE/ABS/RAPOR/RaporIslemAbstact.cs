﻿using Sabis.Bolum.Core.RAPOR;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sabis.Core;
using Sabis.Enum;
using System.IO;
using jsreport.Client;
using Sabis.Bolum.Core.ENUM.RAPOR;
using Sabis.Bolum.Core.RAPOR.Dto;

namespace Sabis.Bolum.Bll.SOURCE.ABS.RAPOR
{
    public class RaporIslemAbstact : IRaporIslem
    {
        private IRaporDataRepo _raporRepo;
        private IDosyaIslem _dosyaIslem;
        private IBelgeIslem _belgeIslem;


        public RaporIslemAbstact(IRaporDataRepo raporRepo,IDosyaIslem dosyaIslem, IBelgeIslem belgeIslem)
        {
            _raporRepo = raporRepo;
            _dosyaIslem = dosyaIslem;
            _belgeIslem = belgeIslem;
        }

        #region constr
        //private static string jsReportConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SabisJSReport"].ToString();

        //private Tuple<string, string, string> GetJsReportConnStr
        //{
        //    get
        //    {
        //        string server = string.Empty;
        //        string username = string.Empty;
        //        string password = string.Empty;

        //        var items = jsReportConnectionString.Split(';');
        //        foreach (var item in items)
        //        {
        //            var head = item.Split('=');
        //            switch (head[0])
        //            {
        //                case "server":
        //                    server = head[1];
        //                    break;

        //                case "username":
        //                    username = head[1];
        //                    break;

        //                case "password":
        //                    password = head[1];
        //                    break;

        //                default:
        //                    break;
        //            }
        //        }

        //        Tuple<string, string, string> result = new Tuple<string, string, string>(server, username, password);
        //        return result;
        //    }
        //}
#endregion
        public IslemSonuc<RaporCiktiDto> Olustur(BelgeDto belgeDto, BelgeConfig config, int birimID, Dictionary<string, string> sdfsFileAttributes = null)
        {
            RaporCiktiDto data = new RaporCiktiDto();
            //Dil çevirisi
            RaporDto raporDto = new RaporDto(belgeDto);

            var birimlerIDList = new List<int>() { birimID };

            birimlerIDList.AddRange(Sabis.Client.BirimIslem.Instance.getirUstBirimIDList(birimID));
            //yüksek lisans veya doktoraysa enumSeviye ekle(aynı fakultede her ikisi de var)
            DtoBirimler birim = Sabis.Client.BirimIslem.Instance.BirimDict[birimID];
            raporDto.RaporServisKey = _raporRepo.GetirRaporServisKey(belgeDto.EnumBelgeTuru, birimlerIDList, (EnumOgretimSeviye?)birim.EnumSeviye, config.EnumDil);
            byte[] pdf = null;

            if (raporDto.RaporServisKey.Item3 == EnumRaporServisTuru.Telerik)
            {
                var reportKeyList = new List<KeyValuePair<int, object>>();
                reportKeyList.Add(new KeyValuePair<int, object>(Convert.ToInt32(raporDto.RaporServisKey.Item1), belgeDto));
                if (!string.IsNullOrEmpty(raporDto.RaporServisKey.Item2))
                    reportKeyList.Add(new KeyValuePair<int, object>(Convert.ToInt32(raporDto.RaporServisKey.Item2), belgeDto));

                try
                {
                    pdf = Sabis.Rapor.Client.Servis.Getirv2(reportKeyList, config.EnumDil, config.Adet);
                }
                catch (System.Exception ex)
                {
                    var sonuc = new IslemSonuc<RaporCiktiDto>(data, EnumShared.EnumIslemSonuc.Hata);
                    sonuc.MesajList = new List<string> { ex.Message };
                    return sonuc;
                }
            }
            else
            {
                pdf = GetFromJSReport(raporDto.RaporServisKey.Item1, belgeDto);
            }

            if (config.DogrulamaKoduEkle)
            {
                //SDFS e raporu pdf olarak kaydet.
                //OGRDosya'ya da doğrulama kodunu yazar.
                var dosyaDto = _dosyaIslem.Kaydet(pdf, belgeDto.DosyaAdi, sdfsFileAttributes);

                //SDFSe doğrulama anahtarını ekler (öğrenci numarası)
                KeyValuePair<string, string> dogrulamaAnahtari;
                if (config.IkiAdimBelgeDogrulama)
                {
                    dogrulamaAnahtari = belgeDto.DogrulamaAnahtari;
                }
                else
                {
                    dogrulamaAnahtari = new KeyValuePair<string, string>();
                }
                KaydetDogrulamaKodu(belgeDto.DogrulamaKodu, dosyaDto.DosyaKey, dogrulamaAnahtari);

                //Verilen belgelere dosya idsi ile ekle
                //belgeDto.BelgeSayiID ile ara ve dosyaDto.ID sini güncelle
                if (belgeDto.BelgeSayiID.HasValue)
                    _belgeIslem.GuncelleBelgeSayiWithDosyaID(dosyaDto.ID, belgeDto.BelgeSayiID.Value);

                data.Url = _dosyaIslem.UrlOlustur(dosyaDto.DosyaKey);
            }
            data.DosyaAdi = belgeDto.DosyaAdi;
            data.Pdf = pdf;
            var result = new IslemSonuc<RaporCiktiDto>(data, EnumShared.EnumIslemSonuc.Basari);

            return result;
        }

        public IslemSonuc<RaporCiktiDto> Olustur<T>(List<T> belgeDto, BelgeConfig config, int birimID, Dictionary<string, string> sdfsFileAttributes = null) where T : BelgeDto
        {
            throw new NotImplementedException();
        }

        private byte[] GetFromJSReport(string reportKey, BelgeDto belgeDto)
        {
            byte[] result = null;

            try
            {
                var _reportingService = new ReportingService("http://jsreport.apollo.gtu.edu.tr/", "admmin", "password");
                var jsonobject = Newtonsoft.Json.JsonConvert.SerializeObject(belgeDto);
                var report = _reportingService.RenderAsync(reportKey, jsonobject).Result;
                using (MemoryStream ms = new MemoryStream())
                {
                    report.Content.CopyTo(ms);
                    result = ms.ToArray();
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null && ex.InnerException.Message.Contains("jsreport.init()"))
                {
                    result = GetFromJSReport(reportKey, belgeDto);
                }
                else
                {
                    throw ex;
                }
            }
            return result;
        }

        private byte[] GetFromJSReportToplu<T>(string reportKey, List<T> belgeDto) where T : BelgeDto
        {
            byte[] result = null;

            try
            {
                var _reportingService = new ReportingService("http://jsreport.apollo.gtu.edu.tr/", "admmin", "password");
                var jsonobject = Newtonsoft.Json.JsonConvert.SerializeObject(belgeDto);

                var report = _reportingService.RenderAsync(reportKey, belgeDto).Result;
                using (MemoryStream ms = new MemoryStream())
                {
                    report.Content.CopyTo(ms);
                    result = ms.ToArray();
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null && ex.InnerException.Message.Contains("jsreport.init()"))
                {
                    result = GetFromJSReportToplu(reportKey, belgeDto);
                }
                else
                {
                    throw ex;
                }
            }
            return result;
        }

        public virtual IslemSonuc<RaporCiktiDto> OlusturDuplicate(BelgeDto belgeDto, BelgeConfig config, int birimID, Dictionary<string, string> sdfsFileAttributes = null)
        {
            var xx = Olustur(belgeDto, config, birimID, sdfsFileAttributes = null);
            return xx;
        }

        public virtual void KaydetDogrulamaKodu(string dogrulamaKodu, string dosyaKey, KeyValuePair<string, string> dogrulamaAnahtari)
        {
            Sabis.BelgeDogrulama.Client.Anahtar.DosyaEkle(dogrulamaKodu, dosyaKey, dogrulamaAnahtari.Key, dogrulamaAnahtari.Value);
        }
        public IslemSonuc<RaporCiktiDto> raporlariBirlestir(List<Tuple<RaporCiktiDto, int>> raporList, string yenidosyaadi = "Birleştirilmiş Rapor", string sayfatipi = "a4", bool yatay = false)
        {
            throw new NotImplementedException();
        }

        public IslemSonuc<RaporCiktiDto> raporlariBirlestir(Tuple<RaporCiktiDto, int> rapor, string sayfatipi = "a4", bool yatay = false)
        {
            throw new NotImplementedException();
        }

        public void test()
        {
            throw new NotImplementedException();
        }
    }
}
