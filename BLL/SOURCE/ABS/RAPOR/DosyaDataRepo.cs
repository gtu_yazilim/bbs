﻿using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Bolum.Core.RAPOR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Bll.SOURCE.ABS.RAPOR
{
    public class DosyaDataRepo : IDosyaDataRepo
    {
        public DosyaDto Kaydet(string dosyaKey)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                OGRDosya kayit = new OGRDosya()
                {
                    DosyaKey = dosyaKey,
                    TarihKayit = DateTime.Now
                };
                db.OGRDosya.Add(kayit);
                db.SaveChanges();
                //var result = _mapper.Map<DosyaDto>(kayit);
                var result = new DosyaDto();
                result.DosyaKey = kayit.DosyaKey;
                result.TarihKayit = kayit.TarihKayit;
                return result;
            }
        }
    }
}
