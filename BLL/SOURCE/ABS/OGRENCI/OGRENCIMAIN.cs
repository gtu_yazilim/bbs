﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Bolum.Core.ABS.OGRENCI;
using Sabis.Enum;

namespace Sabis.Bolum.Bll.SOURCE.ABS.OGRENCI
{
    public class OGRENCIMAIN
    {
        public static List<SecilenDerslerVM> GetirOgrenciSecilenDersListesi(int FKOgrenciID, int Yil, int Donem)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var sorgu = from y in db.OGROgrenciYazilma
                            where y.FKOgrenciID == FKOgrenciID && y.Yil == Yil && y.EnumDonem == Donem && y.Iptal == false
                            orderby y.ID
                            select new
                            {
                                Akts = y.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.ECTSKredi,
                                Kredi = y.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.Kredi,
                                BolKod = y.OGRDersGrup.OGRDersPlan.BolKodAd,
                                DersKod = y.OGRDersGrup.OGRDersPlan.DersKod,
                                EnumKokDersTipi = y.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.EnumKokDersTipi,
                                DersAd = y.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.DersAd,
                                Notlari = y.OGRKimlik.ABSOgrenciNot.Where(d => d.Silindi == false && d.FKOgrenciID == y.FKOgrenciID && d.FKDersGrupID == y.FKDersGrupID).ToList(),
                                PaylarNormal = db.ABSPaylar.Where(d => !d.Silindi && d.FKDersPlanAnaID == y.FKDersPlanAnaID && d.FKDersPlanID == null && d.Yil == Yil && d.EnumDonem == Donem).ToList(),
                                PaylarUzmanlikAlani = db.ABSPaylar.Where(d => !d.Silindi && d.FKDersPlanAnaID == y.FKDersPlanAnaID && d.FKDersPlanID == y.FKDersPlanID && d.Yil == Yil && d.EnumDonem == Donem).ToList(),
                            };

                List<SecilenDerslerVM> secilenDersListe = new List<SecilenDerslerVM>();
                foreach (var s in sorgu)
                {
                    SecilenDerslerVM sd = new SecilenDerslerVM();
                    sd.DersAd = s.DersAd;
                    sd.DersKod = s.DersKod;
                    sd.BolKod = s.BolKod;
                    sd.Akts = s.Akts;
                    sd.Kredi = s.Kredi;

                    if (s.EnumKokDersTipi == (int)EnumKokDersTipi.UZMANLIKALANI)
                    {
                        foreach (var js in s.PaylarUzmanlikAlani)
                        {
                            SecilenDersDetayVM dd = new SecilenDersDetayVM();
                            dd.CalismaTip = js.EnumCalismaTip;
                            dd.CalismaTipAd = Utils.GetEnumDescription((EnumCalismaTip)js.EnumCalismaTip);
                            if (s.Notlari.Any(d => d.FKAbsPaylarID == js.ID))
                            {
                                dd.Notu = s.Notlari.FirstOrDefault(d => d.FKAbsPaylarID == js.ID).Notu;
                            }
                            else
                            {
                                dd.Notu = null;
                            }
                            dd.Oran = js.Oran;
                            dd.PayID = js.ID;


                            sd.DersDetay.Add(dd);
                        }
                    }
                    else
                    {
                        foreach (var js in s.PaylarNormal)
                        {
                            SecilenDersDetayVM dd = new SecilenDersDetayVM();
                            dd.CalismaTip = js.EnumCalismaTip;
                            dd.CalismaTipAd = Utils.GetEnumDescription((EnumCalismaTip)js.EnumCalismaTip);
                            if (s.Notlari.Any(d => d.FKAbsPaylarID == js.ID))
                            {
                                dd.Notu = s.Notlari.FirstOrDefault(d => d.FKAbsPaylarID == js.ID).Notu;
                            }
                            else
                            {
                                dd.Notu = null;
                            }

                            dd.Oran = js.Oran;
                            dd.PayID = js.ID;

                            sd.DersDetay.Add(dd);
                        }
                    }

                    secilenDersListe.Add(sd);
                }
                return secilenDersListe;
            }
        }
        public static List<OgrenciListesiVM> GetirDersOgrenciListesi(int dersGrupID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return db.OGROgrenciYazilma.Where(x => x.FKDersGrupID == dersGrupID && x.Iptal == false && x.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL).Select(x => new OgrenciListesiVM
                {
                    Numara = x.OGRKimlik.Numara,
                    Ad = x.OGRKimlik.Kisi.Ad,
                    Soyad = x.OGRKimlik.Kisi.Soyad,
                    OgrenciID = x.OGRKimlik.ID,
                    KullaniciAdi = x.OGRKimlik.KullaniciAdi,
                    KisiID = x.OGRKimlik.FKKisiID, // değişmesi gerekebilir,
                    KullaniciID = db.Kullanici.FirstOrDefault(y => y.FKOgrenciID == x.FKOgrenciID).ID,
                    DanismanAdSoyad = x.OGRKimlik.PersonelBilgisi.Ad + " " + x.OGRKimlik.PersonelBilgisi.Soyad
                    //BagliOlduguYonetmelik = Apollo.Core.OgrenciYonetmelik.OgrenciYonetmelikRepo.GetirYonetmelikAdiOgrenciNoIle(x.OGRKimlik.ID).Data
                }).OrderBy(x => x.Numara).ToList();
            }
        }
        public static List<OgrenciListesiVM> AraOgrenci(string adsoyad, int fKPersonelID, int yil, int enumDonem)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                List<OgrenciListesiVM> ogrenciList = new List<OgrenciListesiVM>();
                PersonelBilgisi personel = db.PersonelBilgisi.Find(fKPersonelID);

                var birimler = Sabis.Client.BirimIslem.Instance.getirAltBirimlerID(personel.IdariGorev.FirstOrDefault().FKBolumID, true);

                ogrenciList = (from ok in db.OGRKimlik
                                join k in db.Kisi on ok.FKKisiID equals k.ID
                                where birimler.Contains(ok.FKBolumPrBirimID)
                                && ok.EnumOgrDurum == (int)EnumOgrDurum.NORMAL
                                && ok.EnumOgrencilikStatusu != (int)EnumOgrencilikStatusu.Mezun 
                                && ok.EnumOgrencilikStatusu != (int)EnumOgrencilikStatusu.KaydiSilinen
                                && ok.EnumOgrencilikStatusu != (int)EnumOgrencilikStatusu.YanlisKayit
                                && ok.SilindiMi == false
                                    select new OgrenciListesiVM
                                    {
                                        OgrenciID = ok.ID,
                                        Ad = k.Ad + " " + k.Soyad,
                                        Soyad = k.Soyad,
                                        Numara = ok.Numara,
                                        //KullaniciAdi =(ok.Kullanici1 != null && ok.Kullanici1.Count>0) ? ok.Kullanici1.FirstOrDefault().KullaniciAdi : null,
                                        FotoAdres = "",
                                        OgrenciBirim = ok.Birimler.BirimAdi,
                                        GirisYil = ok.GirisYil,
                                        DanismanAdSoyad = ok.PersonelBilgisi.Ad+ " " + ok.PersonelBilgisi.Soyad
                                           
                                    }).Distinct().Where(x => x.Ad.Contains(adsoyad)).OrderByDescending(x => x.GirisYil).Take(10).ToList();
           
                foreach (var ogrenci in ogrenciList)
                {
                    var foto = FotografClient.Client.FotografGetir(ogrenci.KullaniciAdi);
                    ogrenci.FotoAdres = foto != null ? foto : "";
                    //ogrenci.BagliOlduguYonetmelik = Apollo.Core.OgrenciYonetmelik.OgrenciYonetmelikRepo.GetirYonetmelikAdiOgrenciNoIle(ogrenci.OgrenciID).Data;
                }
                return ogrenciList;
            }
        }
        public static OgrenciListesiVM GetirOgrenciOzetBilgi(int ogrenciID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var ogrenci = db.OGRKimlik.Where(x => x.ID == ogrenciID).Select(x => new OgrenciListesiVM
                {
                    OgrenciID = x.ID,
                    KisiID = x.FKKisiID,
                    Ad = x.Kisi.Ad,
                    Soyad = x.Kisi.Soyad,
                    Numara = x.Numara,
                    KullaniciAdi = x.Kullanici1.FirstOrDefault().KullaniciAdi,
                    FotoAdres = "",
                    EnumArsivDurum = x.OGRArsiv.FirstOrDefault(y => y.FKOgrenciID == x.ID && !y.Silindi).EnumArsivDurum,
                    OgrenciBirim = x.Birimler.BirimAdi,
                    DanismanAdSoyad = x.PersonelBilgisi.Ad + " " + x.PersonelBilgisi.Soyad,
                    BagliOlduguMufredat = x.OGRMufredat != null ? x.OGRMufredat.MufredatAdi : ""
                    
                }).FirstOrDefault();

                ogrenci.FotoAdres = FotografClient.Client.FotografGetir(ogrenci.KullaniciAdi);
                ogrenci.BagliOlduguYonetmelik = Apollo.Core.OgrenciYonetmelik.OgrenciYonetmelikRepo.GetirYonetmelikAdiOgrenciNoIle(ogrenci.OgrenciID).Data;
                ogrenci.Telefon = GetirOgrenciTelefon(ogrenci.KisiID);
                return ogrenci;
            }
        }

        public static List<DanismanMesajVM> GetirDanismanMesajListesi(int FKOgrenciID, int personelID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return (from m in db.OGRDanismanMesaj
                        where m.FKOgrenciID == FKOgrenciID && m.FKDanismanPersonelID == personelID
                        orderby m.ID descending
                        select new DanismanMesajVM
                        {
                            FKOgrenciID = m.FKOgrenciID,
                            FKPersonelID = m.FKDanismanPersonelID,
                            ID = m.ID,
                            Mesaj = m.Mesaj,
                            Tarih = m.Tarih,
                            TarihOkunma = m.TarihOkunma,
                            OgrenciNumara = m.OGRKimlik.Numara,
                            OgrenciAdSoyad = m.OGRKimlik.Kisi.Ad + " " + m.OGRKimlik.Kisi.Soyad,
                            PersonelUnvanAdSoyad = m.PersonelBilgisi.UnvanAd + " " + m.PersonelBilgisi.Ad + " " + m.PersonelBilgisi.Soyad,
                            GonderenOgrenci = m.GonderenOgrenci,
                        }).ToList();
            }
        }

        public static List<DersOgrenciListesiVM> GetirDersTumOgrenciListesi(int fkDersPlanANaID, int fkPersonelID, int yil, int donem)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var yazilmaList = db.OGROgrenciYazilma.Where(x => x.FKDersPlanAnaID == fkDersPlanANaID && x.Yil == yil && x.EnumDonem == donem && !x.Iptal).ToList();

                var hocaList = db.OGRDersGrupHoca.Where(x => x.FKPersonelID == fkPersonelID && !x.OGRDersGrup.Silindi).ToList();

                var grupList = hocaList.Select(x => x.OGRDersGrup).ToList();

                var grupIDList = grupList.Select(x => x.ID).ToList();

                yazilmaList = yazilmaList.Where(x => grupIDList.Contains(x.FKDersGrupID.Value)).ToList();

                var ogrList = yazilmaList.Select(x => x.OGRKimlik).ToList();

                var kisiList = ogrList.Select(x => x.Kisi).ToList();


                var birimList = ogrList.Select(x => x.FKBirimID).Distinct().ToList();

                Dictionary<int, string> fakulteDict = new Dictionary<int, string>();
                Dictionary<int, string> bolumDict = new Dictionary<int, string>();

                foreach (var birimID in birimList)
                {
                    fakulteDict.Add(birimID, Sabis.Client.BirimIslem.Instance.getirBagliFakulte(birimID).BirimAdi);
                    bolumDict.Add(birimID, Sabis.Client.BirimIslem.Instance.getirBagliBolum(birimID).BirimAdi);
                }

                List<DersOgrenciListesiVM> list = new List<DersOgrenciListesiVM>();

                foreach (var yazilma in yazilmaList)
                {
                    var ogr = ogrList.FirstOrDefault(x => x.ID == yazilma.FKOgrenciID);
                    var kisi = kisiList.FirstOrDefault(x => x.ID == ogr.FKKisiID);
                    var personel = hocaList.FirstOrDefault(x => x.FKDersGrupID == yazilma.FKDersGrupID).PersonelBilgisi;
                    DersOgrenciListesiVM yeniOgrenci = new DersOgrenciListesiVM();
                    yeniOgrenci.Numara = ogr.Numara;
                    yeniOgrenci.Ad = kisi.Ad;
                    yeniOgrenci.Soyad = kisi.Soyad;
                    yeniOgrenci.Fakulte = fakulteDict[ogr.FKBirimID];//Sabis.Client.BirimIslem.Instance.getirBagliFakulte(ogr.FKBirimID).BirimAdi;
                    yeniOgrenci.Bolum = bolumDict[ogr.FKBirimID];
                    yeniOgrenci.FKBirimID = ogr.FKBirimID;
                    yeniOgrenci.OgretimGorevlisi = personel.Ad + " " + personel.Soyad + " " + personel.UnvanAd;
                    yeniOgrenci.Grubu = Sabis.Enum.Utils.GetEnumDescription((Sabis.Enum.EnumOgretimTur)yazilma.OGRDersGrup.EnumOgretimTur) + " " + yazilma.OGRDersGrup.GrupAd;
                    yeniOgrenci.FKDersGrupID = yazilma.FKDersGrupID.Value;
                    list.Add(yeniOgrenci);
                }

                return list.OrderBy(l => l.FKDersGrupID).ThenBy(l => l.Bolum).ToList();
            }
        }

        public static List<Sabis.Bolum.Core.ABS.NOT.OgrenciListesiVM> GetirOgrenciHocaNotListesi(int fkPersonelID, int fkOgrenciID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var ogrenci = db.OGRKimlik.FirstOrDefault(x => x.ID == fkOgrenciID);
                var perDersList = db.OGRDersGrupHoca.Where(x => x.FKPersonelID == fkPersonelID).Select(x => x.OGRDersGrup).ToList();
                var ogrDersList = db.OGROgrenciYazilma.Where(x => x.FKOgrenciID == fkOgrenciID && !x.Iptal).Select(x => x.OGRDersGrup).ToList();
                var ogrPerCrossList = perDersList.Where(x => ogrDersList.Contains(x)).ToList();
                var dersGrupIDList = ogrPerCrossList.Select(x => x.ID).ToList();
                var ogrNotList = db.ABSOgrenciNot.Where(x => x.FKOgrenciID == fkOgrenciID && !x.Silindi && dersGrupIDList.Contains(x.FKDersGrupID.Value)).ToList();
                var dersIDList = ogrNotList.Select(x => x.FKDersPlanAnaID).ToList();
                var dersList = db.OGRDersPlanAna.Where(x => dersIDList.Contains(x.ID)).ToList();

                List<Sabis.Bolum.Core.ABS.NOT.OgrenciListesiVM> ogrNot = new List<Core.ABS.NOT.OgrenciListesiVM>();
                //List<Sabis.Bolum.Core.ABS.NOT.OgrenciNotlariVM> ogrNot = new List<Core.ABS.NOT.OgrenciNotlariVM>();

                foreach (var ders in dersList)
                {
                    Sabis.Bolum.Core.ABS.NOT.OgrenciListesiVM yeniDers = new Core.ABS.NOT.OgrenciListesiVM();
                    yeniDers.OgrenciID = fkOgrenciID;
                    yeniDers.Numara = ogrenci.Numara;
                    yeniDers.Ad = ogrenci.Kisi.Ad;
                    yeniDers.Soyad = ogrenci.Kisi.Soyad;
                    yeniDers.DersAd = ders.DersAd;
                    yeniDers.DersPlanAnaID = ders.ID;
                    yeniDers.OgrenciNotListesi = ogrNotList.Where(x=> x.FKDersPlanAnaID == ders.ID).Select(x => new Core.ABS.NOT.OgrenciNotlariVM
                    {
                        PayID = x.FKAbsPaylarID.Value,
                        Notu = x.NotDeger.ToString(),
                        OgrenciNotID = x.ID,
                        Oran = x.ABSPaylar.Oran,
                        Sira = x.ABSPaylar.Sira,
                        EnumCalismaTip = x.ABSPaylar.EnumCalismaTip.Value,
                    }).ToList();
                    ogrNot.Add(yeniDers);
                }

                return ogrNot;
            }
        }


        public static string GetirOgrenciNo(string kullaniciadi)
        {
             
            using (UYSv2Entities db = new UYSv2Entities())
            {

                var numara = db.Kullanici.FirstOrDefault(x => x.KullaniciAdi == kullaniciadi).OGRKimlik.Numara;
                return numara.ToUpper();
            }

        }

        public static string GetirKullaniciAdi(string ogrenciNo)
        {
            string result = string.Empty;

            using (UYSv2Entities db = new UYSv2Entities())
            {
                var ogrenci = db.OGRKimlik.FirstOrDefault(x => x.Numara == ogrenciNo);
                var kullanici = ogrenci.Kullanici1.FirstOrDefault(); 
                if (kullanici != null)
                {
                    result = kullanici.KullaniciAdi != null ? kullanici.KullaniciAdi : "-";
                } 
            }
            return result;

        }

        public static string GetirOgrenciTelefon(int kisiID)
        {
            string telefon = "";
            using (UYSv2Entities db = new UYSv2Entities())
            {
                Kisi kisi = db.Kisi.Where(a => a.ID == kisiID).First();

                List<TelefonBilgisi> telefonlBilgisi = kisi.TelefonBilgisi.ToList();

                foreach(TelefonBilgisi tb in telefonlBilgisi)
                {
                    telefon = telefon + tb.Numara + ", ";
                }
            }
            int l = telefon.Length;
            telefon = telefon.Substring(0, l - 2);
            return telefon;
        }

        public static List<OgrenciBilgileri> GetirAktifOgrenciListesi(int FKPersonelID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                PersonelBilgisi personel = db.PersonelBilgisi.Find(FKPersonelID);
                var idariGorevler = personel.IdariGorev.Where(b=>b.FKBolumID!=null).ToList();
                List<int> birimler = new List<int>();
                foreach(IdariGorev ig in idariGorevler)
                {
                    var birim = Sabis.Client.BirimIslem.Instance.getirAltBirimlerID(ig.FKBolumID, true);
                    birimler.AddRange(birim);
                }
                //var birimler = Sabis.Client.BirimIslem.Instance.getirAltBirimlerID(personel.IdariGorev.FirstOrDefault().FKBolumID, true);

                var liste = db.OGRKimlik.Where(x => x.EnumOgrDurum == (int)EnumOgrDurum.NORMAL && x.SilindiMi == false 
                && x.EnumOgrencilikStatusu != (int)EnumOgrencilikStatusu.Mezun
                && x.EnumOgrencilikStatusu != (int)EnumOgrencilikStatusu.KaydiSilinen
                && x.EnumOgrencilikStatusu != (int)EnumOgrencilikStatusu.YanlisKayit
                && x.EnumOgrencilikStatusu != (int)EnumOgrencilikStatusu.ArsivYoksiseBildirilmez
                //&& x.EnumKayitNedeni != (int)EnumKayitNedeni.ANLASMALI_MISAFIR
                //&& x.EnumKayitNedeni != (int)EnumKayitNedeni.YUKSEK_LISANS_OZEL_OGRENCI
                //&& x.EnumKayitNedeni != (int)EnumKayitNedeni.DOKTORA_OZEL_OGRENCI
                //&& x.EnumKayitNedeni != (int)EnumKayitNedeni.LISANS_OZEL_OGRENCI
                && birimler.Contains(x.FKBolumPrBirimID)).Select(x => new OgrenciBilgileri
                {
                    Numara = x.Numara,
                    Ad = x.Kisi.Ad,
                    Soyad = x.Kisi.Soyad,
                    OgrenciID = x.ID,
                    KullaniciAdi = x.KullaniciAdi,
                    KisiID = x.FKKisiID, // değişmesi gerekebilir,
                    //KullaniciID = db.Kullanici.FirstOrDefault(y => y.FKOgrenciID == x.ID).ID,
                    DanismanAdSoyad = x.PersonelBilgisi.Ad + " " + x.PersonelBilgisi.Soyad,
                    BolumAdi = x.Birimler.BirimAdi,
                    Sinif = x.Sinif,
                    AktifYariyil = x.AktifYariyil,
                    KayitYili = x.GirisYil,
                    EnumOgrencilikStatusu = (int)x.EnumOgrencilikStatusu,
                    EnumSeviye = (int) x.Birimler.EnumSeviye
                    //BagliOlduguYonetmelik = Apollo.Core.OgrenciYonetmelik.OgrenciYonetmelikRepo.GetirYonetmelikAdiOgrenciNoIle(x.OGRKimlik.ID).Data
                }).OrderBy(x => x.Numara).ToList();


                foreach(OgrenciBilgileri ob in liste)
                {
                    ob.EnumOgrencilikStatusuDesc = Sabis.Enum.Utils.GetEnumDescription((EnumOgrencilikStatusu)ob.EnumOgrencilikStatusu);
                }

                return liste;
            }
        }

        public static List<OgrenciBilgileriExcelVM> OgrenciBilgileriExcelFormat(List<OgrenciBilgileri> ogrenciBilgileri)
        {
            List<OgrenciBilgileriExcelVM> ogrenciBilgileriList = OgrenciBilgileriExcelFormatindaGetir(ogrenciBilgileri);
            return ogrenciBilgileriList;
        }

        private static List<OgrenciBilgileriExcelVM> OgrenciBilgileriExcelFormatindaGetir(List<OgrenciBilgileri> ogrenciBilgileri)
        {
            List<OgrenciBilgileriExcelVM> ogrenciBilgileriList = new List<OgrenciBilgileriExcelVM>();
            foreach (var ob in ogrenciBilgileri)
            {
                OgrenciBilgileriExcelVM vm = new OgrenciBilgileriExcelVM();
                vm.Numara = ob.Numara;
                vm.AdSoyad = ob.Ad + " " + ob.Soyad;
                vm.GirisYili = ob.KayitYili.ToString();
                vm.Sinif = ob.Sinif;
                vm.AktifYariyil = ob.AktifYariyil.ToString();
                vm.OgrencilikStatusu = ob.EnumOgrencilikStatusuDesc;

                ogrenciBilgileriList.Add(vm);
            }

            return ogrenciBilgileriList;
        }


    }
}
