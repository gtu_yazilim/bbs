﻿using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Bolum.Core.dto.Ogrenci;
using Sabis.Bolum.Core.dto.Yoklama;
using Sabis.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Bll.SOURCE.ABS.Yoklama
{
    public class YoklamaIslem
    {
        public static List<OgrenciKatilimDto> Listele(int dersGrupHocaID, List<int> dersProgramIDList)
        {
            List<OgrenciKatilimDto> result = new List<OgrenciKatilimDto>();
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var ogrenciList = (from yazilma in db.OGROgrenciYazilma
                                   join dersGrup in db.OGRDersGrup on yazilma.FKDersGrupID equals dersGrup.ID
                                   join dersGrupHoca in db.OGRDersGrupHoca on dersGrup.ID equals dersGrupHoca.FKDersGrupID
                                   where yazilma.Iptal == false && dersGrupHoca.ID == dersGrupHocaID && yazilma.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL && yazilma.FKOgrenciID.HasValue
                                   select new OgrenciBilgiDto
                                   {
                                       ID = yazilma.FKOgrenciID.Value,
                                       Ad = yazilma.OGRKimlik.Kisi.Ad,
                                       Soyad = yazilma.OGRKimlik.Kisi.Soyad,
                                       Numara = yazilma.OGRKimlik.Numara,
                                       KisiID = yazilma.OGRKimlik.FKKisiID
                                   }).ToList();

                foreach (var item in ogrenciList)
                {
                    item.KullaniciAdi = OGRENCI.OGRENCIMAIN.GetirKullaniciAdi(item.Numara);
                    item.FotoUrl = FotografClient.Client.FotografGetir(item.KullaniciAdi);
                }

                var dersProgrami = db.OGRDersProgramiv2
                    .Where(x => x.FKDersGrupHocaID == dersGrupHocaID && dersProgramIDList.Contains(x.ID))
                    .OrderBy(x => x.DersTarihSaat)
                    .ToList();

                var devamListesi = db.ABSDevamTakipV2
                    .Where(x => x.OGRDersProgramiv2.FKDersGrupHocaID == dersGrupHocaID && !x.Silindi)
                    .Select(x => new DevamListeDto
                    {
                        OgrenciID = x.FK_OgrenciID,
                        DersTarihSaat = x.OGRDersProgramiv2.DersTarihSaat,
                        DersProgramID = x.FK_DersProgramV2ID
                    })
                    .GroupBy(x => x.OgrenciID)
                    .ToDictionary(x => x.Key, y => y.ToList());

                foreach (var item in ogrenciList)
                {
                    OgrenciKatilimDto katilim = new OgrenciKatilimDto();
                    katilim.Ogrenci = item;

                    katilim.DetayList = new List<KatilimDetayDto>();

                    List<DevamListeDto> ogrenciDevam = null;

                    if (!devamListesi.TryGetValue(item.ID, out ogrenciDevam))
                    {

                    }

                    foreach (var dp in dersProgrami)
                    {
                        KatilimDetayDto detay = new KatilimDetayDto();
                        detay.DersProgramID = dp.ID;
                        detay.Tarih = dp.DersTarihSaat;
                        detay.Durum = false;
                        if (ogrenciDevam != null && ogrenciDevam.Any(x => x.DersProgramID == dp.ID))
                        {
                            detay.Durum = true;
                        }
                        katilim.DetayList.Add(detay);
                    }
                    result.Add(katilim);
                }
            }
            return result.OrderBy(x => x.Ogrenci.Numara).ToList();
        }

        public static bool KaydetListe(YoklamaKayitDto kayitDto, string ip, string kullanici)
        {
            bool result = false;
            using (DATA.UysModel.UYSv2Entities db = new DATA.UysModel.UYSv2Entities())
            {
                foreach (var ogrenciID in kayitDto.OgrenciIDList)
                {
                    Sil(ogrenciID, kayitDto.DersProgramIDList, ip, kullanici);
                    foreach (var item in kayitDto.DersProgramIDList)
                    {
                        ABSDevamTakipV2 kayit = new ABSDevamTakipV2();
                        kayit.YoklamaTarihi = DateTime.Now;
                        kayit.FK_DersProgramV2ID = item;
                        kayit.FK_OgrenciID = ogrenciID;
                        kayit.IP = ip;
                        kayit.Kullanici = kullanici;
                        kayit.Yil = null;
                        kayit.Donem = null;
                        kayit.Silindi = false;
                        db.ABSDevamTakipV2.Add(kayit);
                    }
                    db.SaveChanges();
                    result = true;
                }
            }
            return result;
        }

        public static bool SilListe(YoklamaKayitDto kayitDto, string ip, string kullanici)
        {
            bool result = false;
            foreach (var ogrenciID in kayitDto.OgrenciIDList)
            {
                Sil(ogrenciID, kayitDto.DersProgramIDList, ip, kullanici);

            }
            result = true;
            return result;
        }

        static Random rnd = new Random();
        public static int PinUret(int gecerlilikSuresi, int personelID, List<int> dersProgramIDList)
        {
            int result = 0;
            using (DATA.UysModel.UYSv2Entities db = new DATA.UysModel.UYSv2Entities())
            {
                var ornekID = dersProgramIDList.First();
                var aktifKayitlar = db.ABSYoklamaOtomatik.Where(x => x.ABSYoklamaOtomatikDersProgram.Any(y => y.FKDersProgramID == ornekID) && !x.Silindi && x.BitisTarihi > DateTime.Now);
                foreach (var item in aktifKayitlar)
                {
                    item.Silindi = true;
                }
                db.SaveChanges();
                ABSYoklamaOtomatik kayit = new ABSYoklamaOtomatik();
                kayit.BaslangisTarihi = DateTime.Now;
                kayit.BitisTarihi = DateTime.Now.AddSeconds(gecerlilikSuresi);
                kayit.Silindi = false;
                kayit.Pin = rnd.Next(1000, 9999);
                kayit.FKPersonelID = personelID;
                db.ABSYoklamaOtomatik.Add(kayit);
                db.SaveChanges();
                foreach (var item in dersProgramIDList)
                {
                    ABSYoklamaOtomatikDersProgram p = new ABSYoklamaOtomatikDersProgram();
                    p.FKYoklamaOtomatikID = kayit.ID;
                    p.FKDersProgramID = item;
                    db.ABSYoklamaOtomatikDersProgram.Add(p);
                }
                db.SaveChanges();
                result = kayit.Pin;
            }
            return result;
        }

        public static bool Kaydet(int ogrenciID, List<int> dersProgramIDList, string ip, string kullanici)
        {
            bool result = false;
            using (DATA.UysModel.UYSv2Entities db = new DATA.UysModel.UYSv2Entities())
            {
                Sil(ogrenciID, dersProgramIDList, ip, kullanici);
                foreach (var item in dersProgramIDList)
                {
                    ABSDevamTakipV2 kayit = new ABSDevamTakipV2();
                    kayit.YoklamaTarihi = DateTime.Now;
                    kayit.FK_DersProgramV2ID = item;
                    kayit.FK_OgrenciID = ogrenciID;
                    kayit.IP = ip;
                    kayit.Kullanici = kullanici;
                    kayit.Yil = null;
                    kayit.Donem = null;
                    kayit.Silindi = false;
                    db.ABSDevamTakipV2.Add(kayit);
                }
                db.SaveChanges();
                result = true;
            }
            return result;
        }

        public static bool Sil(int ogrenciID, List<int> dersProgramIDList, string ip, string kullanici)
        {
            bool result = false;
            using (DATA.UysModel.UYSv2Entities db = new DATA.UysModel.UYSv2Entities())
            {
                var devamList = db.ABSDevamTakipV2.Where(x => x.FK_OgrenciID == ogrenciID && dersProgramIDList.Contains(x.FK_DersProgramV2ID) && !x.Silindi);
                foreach (var item in devamList)
                {
                    item.Silindi = true;
                }
                db.SaveChanges();
                result = true;
            }
            return result;
        }

        public static KatilimOraniDto ListeleOgrenciKatilimOrani(int ogrenciID, int dersGrupID)
        {
            KatilimOraniDto katilim = new KatilimOraniDto();
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var dersProgrami = db.OGRDersProgramiv2
                    .Where(x => x.OGRDersGrupHoca.FKDersGrupID == dersGrupID)
                    .GroupBy(x => new { x.DersTarihSaat.Year, x.DersTarihSaat.Month, x.DersTarihSaat.Day })
                    .Select(x => new { Yil = x.Key.Year, Ay = x.Key.Month, Gun = x.Key.Day, DersProgListe = x.Select(y => new { DersProgramID = y.ID, y.DersTarihSaat }) })
                    .ToList();

                var ogrenciDevam = db.ABSDevamTakipV2
                    .Where(x => x.OGRDersProgramiv2.OGRDersGrupHoca.FKDersGrupID == dersGrupID && x.FK_OgrenciID == ogrenciID && !x.Silindi)
                    .Select(x => new DevamListeDto
                    {
                        OgrenciID = x.FK_OgrenciID,
                        DersTarihSaat = x.OGRDersProgramiv2.DersTarihSaat,
                        DersProgramID = x.FK_DersProgramV2ID
                    })
                    .ToList();

                var tmpOgrenci = db.OGRKimlik.FirstOrDefault(x => x.ID == ogrenciID);
                OgrenciBilgiDto ogrenci = null;
                if (tmpOgrenci != null)
                {
                    ogrenci = new OgrenciBilgiDto
                    {
                        ID = tmpOgrenci.ID,
                        Ad = tmpOgrenci.Kisi.Ad,
                        Soyad = tmpOgrenci.Kisi.Soyad,
                        Numara = tmpOgrenci.Numara,
                        KisiID = tmpOgrenci.FKKisiID
                    };
                }
                katilim.Ogrenci = ogrenci;

                katilim.HaftaList = new List<KatilimOraniHaftaDto>();

                double toplamDersSaati = 0;
                double geldigiDersSaati = 0;

                foreach (var dp in dersProgrami)
                {
                    KatilimOraniHaftaDto hatfa = new KatilimOraniHaftaDto();
                    hatfa.KatilimSaati = 0;
                    hatfa.Tarih = new DateTime(dp.Yil, dp.Ay, dp.Gun);
                    foreach (var programGun in dp.DersProgListe)
                    {
                        if (ogrenciDevam != null && ogrenciDevam.Any(x => x.DersProgramID == programGun.DersProgramID))
                        {
                            hatfa.KatilimSaati++;
                            geldigiDersSaati++;
                        }
                    }
                    hatfa.ToplamDersSaati = dp.DersProgListe.Count();
                    toplamDersSaati += hatfa.ToplamDersSaati;
                    katilim.HaftaList.Add(hatfa);
                }
                double oran = 0;
                if (toplamDersSaati != 0)
                {
                    oran = (geldigiDersSaati / toplamDersSaati) * 100d;
                }
                katilim.Orani = oran;
            }
            return katilim;
        }

        public static List<KatilimOraniDto> ListeleKatilimOrani(int dersGrupHocaID)
        {
            List<KatilimOraniDto> katilimOrani = new List<KatilimOraniDto>();
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var ogrenciList = (from yazilma in db.OGROgrenciYazilma
                                   join dersGrup in db.OGRDersGrup on yazilma.FKDersGrupID equals dersGrup.ID
                                   join dersGrupHoca in db.OGRDersGrupHoca on dersGrup.ID equals dersGrupHoca.FKDersGrupID
                                   where yazilma.Iptal == false && dersGrupHoca.ID == dersGrupHocaID && yazilma.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL && yazilma.FKOgrenciID.HasValue
                                   select new OgrenciBilgiDto
                                   {
                                       ID = yazilma.FKOgrenciID.Value,
                                       Ad = yazilma.OGRKimlik.Kisi.Ad,
                                       Soyad = yazilma.OGRKimlik.Kisi.Soyad,
                                       Numara = yazilma.OGRKimlik.Numara,
                                       KisiID = yazilma.OGRKimlik.FKKisiID
                                   }).ToList();

                foreach (var item in ogrenciList)
                {
                    item.KullaniciAdi = OGRENCI.OGRENCIMAIN.GetirKullaniciAdi(item.Numara);
                    item.FotoUrl = FotografClient.Client.FotografGetir(item.KullaniciAdi);
                }

                var dersProgrami = db.OGRDersProgramiv2
                    .Where(x => x.FKDersGrupHocaID == dersGrupHocaID)
                    .GroupBy(x => new { x.DersTarihSaat.Year, x.DersTarihSaat.Month, x.DersTarihSaat.Day })
                    .Select(x => new { Yil = x.Key.Year, Ay = x.Key.Month, Gun = x.Key.Day, DersProgListe = x.Select(y => new { DersProgramID = y.ID, y.DersTarihSaat }) })
                    .ToList();

                var devamListesi = db.ABSDevamTakipV2
                    .Where(x => x.OGRDersProgramiv2.FKDersGrupHocaID == dersGrupHocaID && !x.Silindi)
                    .Select(x => new DevamListeDto
                    {
                        OgrenciID = x.FK_OgrenciID,
                        DersTarihSaat = x.OGRDersProgramiv2.DersTarihSaat,
                        DersProgramID = x.FK_DersProgramV2ID
                    })
                    .GroupBy(x => x.OgrenciID)
                    .ToDictionary(x => x.Key, y => y.ToList());

                foreach (var item in ogrenciList)
                {
                    KatilimOraniDto katilim = new KatilimOraniDto();
                    katilim.Ogrenci = item;
                    katilim.HaftaList = new List<KatilimOraniHaftaDto>();

                    List<DevamListeDto> ogrenciDevam = null;
                    double toplamDersSaati = 0;
                    double geldigiDersSaati = 0;

                    if (!devamListesi.TryGetValue(item.ID, out ogrenciDevam))
                    {

                    }

                    foreach (var dp in dersProgrami)
                    {
                        KatilimOraniHaftaDto hatfa = new KatilimOraniHaftaDto();
                        hatfa.KatilimSaati = 0;
                        hatfa.Tarih = new DateTime(dp.Yil, dp.Ay, dp.Gun);
                        foreach (var programGun in dp.DersProgListe)
                        {
                            if (ogrenciDevam != null && ogrenciDevam.Any(x => x.DersProgramID == programGun.DersProgramID))
                            {
                                hatfa.KatilimSaati++;
                                geldigiDersSaati++;
                            }
                        }
                        hatfa.ToplamDersSaati = dp.DersProgListe.Count();
                        toplamDersSaati += hatfa.ToplamDersSaati;
                        katilim.HaftaList.Add(hatfa);
                    }
                    double oran = 0;
                    if (toplamDersSaati != 0)
                    {
                        oran = (geldigiDersSaati / toplamDersSaati) * 100d;
                    }
                    katilim.Orani = oran;
                    katilimOrani.Add(katilim);
                }
            }
            return katilimOrani.OrderBy(d => d.Ogrenci.Numara).ToList();
        }

        private class DevamListeDto
        {
            public int DersProgramID { get; set; }
            public DateTime DersTarihSaat { get; set; }
            public int OgrenciID { get; set; }
        }
    }
}
