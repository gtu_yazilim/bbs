﻿using RestSharp;
using Sabis.Mesaj.Core.Istek;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Bll.SOURCE.ABS.Mesaj
{
    public class MesajClient
    {
        public static void Gonder(List<Sabis.Mesaj.Core.Istek.Mesaj> mesajList)
        {
            var client = new RestClient("http://mesaj.local.apollo.gtu.edu.tr");
//#if DEBUG
//            client = new RestClient("http://mesaj.apollo.gtu.edu.tr");
//#endif
            var request = new RestRequest("api/Mesaj/GonderList", Method.POST);
            request.JsonSerializer = new JsonSerializer();
            request.AddJsonBody(mesajList);
            IRestResponse response = client.Execute(request);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                //Hata var
            }
        }

        public static void Test(string ogrenci)
        {
            List<Sabis.Mesaj.Core.Istek.Mesaj> mesajList = new List<Sabis.Mesaj.Core.Istek.Mesaj>();
            Sabis.Mesaj.Core.Istek.Mesaj sMesaj = new Sabis.Mesaj.Core.Istek.Mesaj();
            sMesaj.DBKaydet = true;
            string aliciKAdi = OGRENCI.OGRENCIMAIN.GetirKullaniciAdi(ogrenci);
            sMesaj.Alici = new Alici { KullaniciAdi = aliciKAdi };
            sMesaj.EnumMesajTipi = Sabis.Mesaj.Core.MesajEnum.EnumMesajTipi.DersDuyuru;
            string baslikMesaj = string.Format("{0} dersi için yeni bir duyurunuz var", "Test Dersi");
            string icerikEposta = string.Format("{0}<br/>{1}<br/><br/>{2}", "TestKonu", "TestMesaj", "kaynak");
            //sMesaj.EPosta = new EPosta { Baslik = "SABİS - Ders Duyurusu", Icerik = icerikEposta, ReplyTo = "kaynak@sakarya.edu.tr" };
            sMesaj.Gonderen = new Gonderen { IPAdresi = "::1", KullaniciAdi = "kaynak" };
            sMesaj.MobilBildirim = new MobilBildirim { Baslik = baslikMesaj, Icerik = baslikMesaj };
            sMesaj.FacebookBildirim = new FacebookBildirim { Baslik = baslikMesaj };
            mesajList.Add(sMesaj);

            Mesaj.MesajClient.Gonder(mesajList);
        }
    }
}
