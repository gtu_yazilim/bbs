﻿using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Bolum.Core.ABS.SORUNBILDIRIM.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Bll.SOURCE.ABS.SORUNBILDIRIM
{
    public class SORUNBILDIRIMMAIN
    {
        public static List<SorunListeleme> SorunListele(int GrupHocaID, int PayID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                int FKDersGrupID = db.OGRDersGrupHoca.FirstOrDefault(x => x.ID == GrupHocaID).FKDersGrupID.Value;
                if (db.OSinavGrup.Any(x => x.PayID == PayID && x.FKDersGrupID == FKDersGrupID))
                {
                    int FKSinavID = db.OSinavGrup.FirstOrDefault(x => x.PayID == PayID && x.FKDersGrupID == FKDersGrupID && x.Silindi == false).FKSinavID;
                    var sorunlar = db.ABSGeriBildirim.Where(x => x.AktiviteID == FKSinavID && x.GrupID == FKDersGrupID).Select(x => new SorunListeleme
                    {
                        FKSinavID = x.AktiviteID,
                        FKDersGrupID = x.GrupID,
                        EnumModulTipi = x.EnumModulTipi,
                        EnumGeriBildirimDurum = x.EnumGeriBildirimDurum,
                        FKOgrenciID = x.OgrenciID,
                        OgrenciNo = x.OGRKimlik.KullaniciAdi,
                        AdSoyad = x.OGRKimlik.Kisi.Ad +" "+ x.OGRKimlik.Kisi.Soyad,
                        Baslik = x.Baslik,
                        Icerik = x.Icerik,
                        DosyaKey = x.DosyaKey,
                        EklemeTarih = x.TarihKayit.ToString(),
                    }).OrderBy(x => x.EnumGeriBildirimDurum).ThenBy(x => x.AdSoyad).ToList();
                    return sorunlar;
                }
                return null;
                
            }
            
        }
    }
}
