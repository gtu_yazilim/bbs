﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sabis.Bolum.Core.ABS.DEGERLENDIRME;
using Sabis.Enum;
using Sabis.Bolum.Bll.DATA.UysModel;


namespace Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME
{
    public class DegerlendirmeYonetim : AbstractDegerlendirme
    {
        public new List<GPay> PayList { get; set; }
        public List<GPay> PayListOrijinal { get; set; }
        public List<GPay> PayListYiliciYok { get; set; }
        public List<Sabis.Bolum.Core.ABS.DEGERLENDIRME.Grup> OrtakGruplar { get; set; }
        private List<int> OrtakGruplarInt;
        private List<int> OrtakGruplarOgrenciFiltreInt;
        public List<OgrenciPayveNotBilgileriDTO> TumNotlar { get; set; }
        public OgrenciSinif OgrenciSinif { get; set; }
        public EnumDersPlanOrtalama EnumDersPlanOrtalama { get; set; }
        public DegerlendirmeYonetim(int grupID, string kullaniciAdi, int personelID, string IP, string simuleEden = null)
        {
            SeciliDers = GetirDersDTO(grupID);
            HiyerarsikDersKontrol(false);
            //Payları getir
            PayList = data.PaylariGetir(SeciliDers.DersGrupID.Value);
            PayListOrijinal = PayList.ToList();
            ButunlemeMi = SeciliDers.ButunlemeMi;
            //Bütünleme ise bütünleme payını kontrol et, yoksa ekle
            if (ButunlemeMi)
            {
                PayList = ButunlemeKontrolu(PayList);
            }
            else
            {
                PayList = PayList.Where(x => x.EnumCalismaTip != EnumCalismaTip.Butunleme).ToList();
            }

            PayListYiliciYok = PayList.Where(x => x.EnumCalismaTip != EnumCalismaTip.YilIcininBasariya).ToList();

            base.Init(kullaniciAdi, personelID, IP, simuleEden);

            Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME.OrtakGrupYonetim.Olustur(SeciliDers.DersPlanAnaID, SeciliDers.Yil, SeciliDers.Donem, kullaniciAdi);

            //Ortak değerlendirme için ortak grupları hesapla
            OrtakGruplar = OrtakDegerlendirmeGrupBul(SeciliDers.DersGrupID.Value, PersonelID);
            OrtakGruplarInt = OrtakGruplar.Select(x => x.DersGrupID).ToList();
            OrtakGruplarOgrenciFiltreInt = OrtakGruplar.Where(x=> x.OgrenciSayisi != 0).Select(x => x.DersGrupID).ToList();

            //Tüm öğrencilerin notlarını getir
            TumNotlar = getirTumNotlarByGrupID(OrtakGruplarInt, PayList, true, SeciliDers.Yil, SeciliDers.Donem);

            PaylarDuzgunMu(PayList.ConvertAll(x => (IPay)x), ButunlemeMi);

            //Değerlendirme için uygun mu?
            var degerlendiriciPersonel = data.GetirDegerlendirecekPersonel(grupID);
            DegerlendiriciMi = degerlendiriciPersonel.ID == personelID;
            DegerlendirmeyeUygunMu();

            //Değerlendirmeye ait bir hesap kaydı varsa getir
            DegerlendirmeKayit = data.getirDegerlendirme(OrtakGruplarInt, ButunlemeMi);
            //Değerlendirmenin aşamasını bul
            EnumDegerlendirmeAsama = AsamaBul(DegerlendirmeKayit);

            //Degerlendirme işlemini gerçekleştir
            OgrenciSinif = sinifBelirle();
            EnumDersPlanOrtalama = SeciliDers.EnumDersPlanOrtalama;
            IlkHesaplamaYap();
        }



        //public DegerlendirmeYonetim(int grupID, int personelID)
        //{
        //    PersonelID = personelID;
        //    SeciliDers = GetirDersDTO(grupID);
        //    ButunlemeMi = SeciliDers.ButunlemeMi;
        //    DegiskenleriAta(true);
        //}



        private void IlkHesaplamaYap()
        {
            //Üst değer ve bağıl ortalamayı durumlara göre getir
            double? ustDeger;
            double? bagilOrtalama;
            getirUstDegerBagilOrtalama(out ustDeger, out bagilOrtalama);
            Degerlendir(ustDeger, bagilOrtalama);
        }

        private List<GPay> ButunlemeKontrolu(List<GPay> PayList)
        {
            if (ButunlemeMi)
            {
                GPay final = PayList.AsReadOnly().FirstOrDefault(x => x.EnumCalismaTip == EnumCalismaTip.Final);
                if (PayList.Any(x => x.EnumCalismaTip == EnumCalismaTip.Butunleme) == false)
                {
                    //Bütünleme payı yok, ekle
                    if (final == null)
                    {
                        throw new DegerlendirmeException("Bütünleme için baz alınacak final oranı bulunamadı!");
                    }
                    GPay butunlemePayi = new GPay();
                    butunlemePayi.DersPlanAnaID = final.DersPlanAnaID;
                    butunlemePayi.DersPlanID = final.DersPlanID;
                    butunlemePayi.EnumCalismaTip = EnumCalismaTip.Butunleme;
                    butunlemePayi.Oran = final.Oran;
                    butunlemePayi.Pay = final.Pay;
                    butunlemePayi.Payda = final.Payda;
                    butunlemePayi.Yuzde = final.Yuzde;
                    butunlemePayi.Sira = 1;
                    butunlemePayi.Yil = final.Yil;
                    butunlemePayi.EnumDonem = final.EnumDonem;

                    butunlemePayi = data.PayKaydet(butunlemePayi);
                    PayList.Add(butunlemePayi);
                }

                if (final != null)
                {
                    PayList = PayList.Where(x => x.PayID != final.PayID).ToList();
                }
            }
            return PayList;
        }

        private void getirUstDegerBagilOrtalama(out double? ustDeger, out double? bagilOrtalama)
        {
            ustDeger = 100;
            bagilOrtalama = null;
            if (EnumDegerlendirmeAsama == EnumDegerlendirmeAsama.BOS && ButunlemeMi == false)
            {
                ustDeger = 100;
                bagilOrtalama = null;
            }
            else if (EnumDegerlendirmeAsama != EnumDegerlendirmeAsama.BOS && ButunlemeMi == false)
            {
                var degOrnegi = DegerlendirmeKayit.First();
                ustDeger = degOrnegi.UstDeger;
                bagilOrtalama = degOrnegi.BagilOrtalama;
            }
            else if (ButunlemeMi == true)
            {
                var gecmisDegerlendirme = data.getirDegerlendirme(OrtakGruplarInt, false).FirstOrDefault();
                if (gecmisDegerlendirme == null)
                {
                    throw new DegerlendirmeException("Dönem içi değerlendirme kaydı bulunamadığı için değerlendirme yapılamıyor. Lütfen öğrenci işleri ile irtibata geçiniz!");
                }
                ustDeger = gecmisDegerlendirme.UstDeger;
                bagilOrtalama = gecmisDegerlendirme.BagilOrtalama;
            }
        }



        protected override bool DegerlendirmeyeUygunMu()
        {
            bool result = true;
            if (!TumNotlarGirildiMi())
            {
                throw new DegerlendirmeException("Değerlendirme yapılabilmesi için dersin tüm notlarının girilmiş olması gerekmektedir!");
            }
            if (ButunlemeMi && data.AktarildiMiTumu(OrtakGruplarInt) == false)
            {
                throw new DegerlendirmeException("Bütünleme değerlendirmesi yapılabilmesi için dersin tüm ortak gruplarının aktarılmış olması gerekmektedir!");
            }
            List<Tuple<int, IPay>> yayinlanmayanPayList;
            if (!TumNotlarYayinlandiMi(PayList.ConvertAll(x => (IPay)x), out yayinlanmayanPayList))
            {
                //throw new DegerlendirmeException("Değerlendirme yapılabilmesi için dersin tüm notlarının yayınlanmış olması gerekmektedir!");
                NotificationList.Add(new Notification("Yayınlanmayan Notlar", "Yayınlanmayan notlar mevcuttur. Lisans ve Önlisans Eğitim-Öğretim ve Sınav Yönetmeliğinin 16/7 maddesine göre; Öğretim elemanları, öğrencilerin başarılarının değerlendirmesine yönelik dönem içi bütün çalışma sonuçlarını, çalışmanın yapıldığı tarihten itibaren iki hafta içinde öğrencilere ilan etmekle yükümlüdürler.", Notification.MasterMesajTip.Uyari));
            }
            return result;
        }

        protected override bool TumNotlarGirildiMi()
        {
            bool result = false;
            foreach (var ogrenci in TumNotlar)
            {
                if (ogrenci.listPaylarVeNotlar.Any(x => x.NotDeger.HasValue == false && x.EnumCalismaTip != EnumCalismaTip.YilIcininBasariya))
                {
                    return false;
                }
            }
            result = true;
            return result;
        }

        protected override bool TumNotlarYayinlandiMi(List<IPay> payList, out List<Tuple<int, IPay>> yayinlanmayanPayList)
        {
            bool result = true;
            //grupid, payid
            List<Tuple<int, int>> yayinlananPayList = data.YayinlananPaylariGetir(OrtakGruplarOgrenciFiltreInt);
            yayinlanmayanPayList = new List<Tuple<int, IPay>>();
            foreach (int grupID in OrtakGruplarInt)
            {
                foreach (IPay pay in PayListYiliciYok)
                {
                    if (!yayinlananPayList.Any(x => x.Item1 == grupID && x.Item2 == pay.PayID))
                    {
                        yayinlanmayanPayList.Add(new Tuple<int, IPay>(grupID, pay));
                        result = false;
                    }
                }
            }
            return result;
        }
        /// <summary>
        /// Ortak değerlendirme yapılacak grupları bulur
        /// </summary>
        /// <param name="dersPlanAna"></param>
        /// <param name="personelID"></param>
        /// <param name="ogretimTuru"></param>
        /// <returns></returns>
        public List<Sabis.Bolum.Core.ABS.DEGERLENDIRME.Grup> OrtakDegerlendirmeGrupBul(int grupID, int personelID)
        {
            List<Sabis.Bolum.Core.ABS.DEGERLENDIRME.Grup> result = null;
            if (SeciliDers.Fakulte.ID == 15)
            {
                result = data.OrtakDegerlendirmeGrupBulADAMYO(grupID, personelID, SeciliDers.Yil, SeciliDers.Donem, ButunlemeMi);
            }
            else
            {
                result = data.OrtakDegerlendirmeGrupBul(grupID, ButunlemeMi);
            }
            return result;
        }

        public List<OgrenciPayveNotBilgileriDTO> getirTumNotlarByGrupID(List<int> grupIDList, List<GPay> payList, bool ogrenciDurumaBak, int yil, int donem)
        {
            return data.getirTumNotlarByGrupID(grupIDList, payList, ButunlemeMi, ogrenciDurumaBak, yil, donem);
        }

        public override bool PaylarDuzgunMu(List<IPay> payList, bool butunlemeMi)
        {
            bool result = true;

            if (Math.Round(payList.Where(x => !(x.EnumCalismaTip == EnumCalismaTip.Final || x.EnumCalismaTip == EnumCalismaTip.Butunleme || x.EnumCalismaTip == EnumCalismaTip.YilIcininBasariya)).Sum(x => x.Yuzde), 2) != 100)
            {
                throw new DegerlendirmeException("Yıliçi çalışma tipleri toplamı 100 olmalıdır!");
            }
            if (ButunlemeMi)
            {
                if (Math.Round(payList.Where(x => x.EnumCalismaTip == EnumCalismaTip.Butunleme || x.EnumCalismaTip == EnumCalismaTip.YilIcininBasariya).Sum(x => x.Yuzde), 2) != 100)
                {
                    throw new DegerlendirmeException("Bütünleme ve yıl içinin başarıya etkisi toplamı 100 olmalıdır!");
                }
            }
            else
            {
                if (Math.Round(payList.Where(x => x.EnumCalismaTip == EnumCalismaTip.Final || x.EnumCalismaTip == EnumCalismaTip.YilIcininBasariya).Sum(x => x.Yuzde), 2) != 100)
                {
                    throw new DegerlendirmeException("Final ve yıl içinin başarıya etkisi toplamı 100 olmalıdır!");
                }
            }
            if (payList.Any(x => x.EnumCalismaTip == EnumCalismaTip.Butunleme) && butunlemeMi)
            {
                if (PayListOrijinal.First(x => x.EnumCalismaTip == EnumCalismaTip.Final).Oran != payList.First(x => x.EnumCalismaTip == EnumCalismaTip.Butunleme).Yuzde)
                { throw new DegerlendirmeException("Final ve bütünleme oranları aynı olmalıdır!"); }
            }
            else if (payList.Any(x => x.EnumCalismaTip == EnumCalismaTip.Butunleme) == false && butunlemeMi)
            {
                throw new DegerlendirmeException("Bütünleme oranı girilmiş olmalıdır!");
            }

            return result;
        }

        protected override EnumDegerlendirmeAsama AsamaBul(List<DegerlendirmeDTO> kayitliDegerlendirmeler)
        {
            EnumDegerlendirmeAsama asama = EnumDegerlendirmeAsama.BOS;

            if (kayitliDegerlendirmeler == null || !kayitliDegerlendirmeler.Any())
            {
                asama = EnumDegerlendirmeAsama.BOS;
            }
            else
            {
                asama = EnumDegerlendirmeAsama.KAYITLI;
                if (data.AktarildiMi(DegerlendirmeKayit))
                {
                    asama = EnumDegerlendirmeAsama.AKTARILDI;
                }
                else if (kayitliDegerlendirmeler.Count != OrtakGruplarInt.Count)
                {
                    hataList.Add("Değerlendirme kaydı sayısı grup sayısı kadar olmalı!");
                    asama = EnumDegerlendirmeAsama.HATALI;
                }
                else
                {
                    int sonHalSayisi = kayitliDegerlendirmeler.Where(x => x.EnumSonHal == (int)EnumSonHal.EVET).Count();
                    if (sonHalSayisi == kayitliDegerlendirmeler.Count)
                    {
                        asama = EnumDegerlendirmeAsama.SONHAL;
                    }
                    else if (sonHalSayisi > 0 && sonHalSayisi < kayitliDegerlendirmeler.Count)
                    {
                        hataList.Add("Son hal sayısı değerlendirme kaydı sayısı kadar olmalı!");
                        asama = EnumDegerlendirmeAsama.HATALI;
                    }
                    else if (sonHalSayisi == 0 && data.TumBasariNotlarYayinlandiMi(OrtakGruplarInt, ButunlemeMi))
                    {
                        asama = EnumDegerlendirmeAsama.YAYINLANDI;
                    }
                }
            }
            return asama;
        }

        public override void Degerlendir(double? _ustDeger, double? _bagilAritmetikOrtalama = null)
        {
            if (OrtakGruplar == null || OrtakGruplar.Any() == false)
            {
                throw new DegerlendirmeException("Değerlendirme işlemi için en az bir grup olmalıdır!");
            }
            //if (!DegerlendiriciMi)
            //{
            //    throw new DegerlendirmeException("Bu ders için değerlendirme yetkiniz bulunmamaktadır!");
            //}
            NOTDEGERLENDIRME.Degerlendirme degerlendir = null;
            bool HatalariGec = false;
            if (EnumDegerlendirmeAsama == EnumDegerlendirmeAsama.AKTARILDI)
            {
                HatalariGec = true;
            }
            if (ButunlemeMi)
            {
                var degerlendirmeVeri = data.getirDegerlendirme(OrtakGruplarInt, false).FirstOrDefault();
                if (degerlendirmeVeri == null)
                {
                    throw new DegerlendirmeException("Geçmiş değerlendirme verisi bulunamadı!");
                }
                degerlendir = new NOTDEGERLENDIRME.Degerlendirme(TumNotlar, degerlendirmeVeri.UstDeger.Value, degerlendirmeVeri.BagilOrtalama.Value, degerlendirmeVeri.Ortalama.Value, degerlendirmeVeri.StandartSapma.Value, degerlendirmeVeri.EnBuyukNot.Value, EnumDersPlanOrtalama, OgrenciSinif, HatalariGec, DegerlendirmeKural);
            }
            else
            {
                degerlendir = new NOTDEGERLENDIRME.Degerlendirme(TumNotlar, _ustDeger, _bagilAritmetikOrtalama, EnumDersPlanOrtalama, OgrenciSinif, HatalariGec, DegerlendirmeKural);
            }
            degerlendir.Hesapla();
            //DegerlendirmeKayit = new List<ABSDegerlendirme>();
            Guid ortakDegerGUID = Guid.NewGuid();
            foreach (Sabis.Bolum.Core.ABS.DEGERLENDIRME.Grup grup in OrtakGruplar)
            {
                var degerlendirmeKayit = DegerlendirmeKayit.FirstOrDefault(x => x.FKDersGrupID == grup.DersGrupID);
                bool yeniMi = false;
                if (degerlendirmeKayit == null)
                {
                    degerlendirmeKayit = new DegerlendirmeDTO();
                    yeniMi = true;
                }
                degerlendirmeKayit.EnBuyukNot = degerlendir.EnBuyukNot;
                degerlendirmeKayit.EnKucukNot = degerlendir.EnKucukNot;
                degerlendirmeKayit.StandartSapma = degerlendir.StandartSapma;
                degerlendirmeKayit.Ortalama = degerlendir.Ortalama;
                degerlendirmeKayit.BagilOrtalama = degerlendir.BagilAritmetikOrtalama;
                degerlendirmeKayit.Tarih = DateTime.Now;
                degerlendirmeKayit.FKPersonelID = PersonelID;
                degerlendirmeKayit.OrtakDegerlendirmeGUID = ortakDegerGUID;
                degerlendirmeKayit.FKDersGrupID = grup.DersGrupID;
                degerlendirmeKayit.FKDersPlanAnaID = grup.DersPlanAnaID;
                degerlendirmeKayit.Kullanici = KullaniciAdi;
                degerlendirmeKayit.BarkodNo = null;
                degerlendirmeKayit.UstDeger = degerlendir.UstDeger;
                degerlendirmeKayit.EnumDonem = SeciliDers.Donem;
                degerlendirmeKayit.Yil = SeciliDers.Yil;
                degerlendirmeKayit.EnumSonHal = (int)EnumSonHal.HAYIR;
                degerlendirmeKayit.Makina = IPAdresi;
                degerlendirmeKayit.ButunlemeMi = ButunlemeMi;
                if (yeniMi)
                {
                    DegerlendirmeKayit.Add(degerlendirmeKayit);
                }
            }
            DegerlendirmeSonuc = degerlendir;
        }

        public override void Kaydet()
        {
            if (EnumDegerlendirmeAsama == EnumDegerlendirmeAsama.AKTARILDI)
            {
                throw new DegerlendirmeException("Öğrenci işlerine aktarımı gerçekleştirilen değerlendirmeler değiştirilemez!");
            }
            if (EnumDegerlendirmeAsama == EnumDegerlendirmeAsama.SONHAL)
            {
                throw new DegerlendirmeException("Son hal verilen değerlendirmeler değiştirilemez!");
            }
            if (EnumDegerlendirmeAsama == EnumDegerlendirmeAsama.YAYINLANDI)
            {
                throw new DegerlendirmeException("Değişiklik yapabilmek için lütfen önce değerlendirmeleri yayından kaldırınız!");
            }
            if (!TarihUygunMu)
            {
                throw new DegerlendirmeException("Değerlendirme için geçersiz tarih aralığı!");
            }
            DegerlendirmeKayit = data.KaydetDegerlendirme(DegerlendirmeKayit);
            var dersGrupGrupla = DegerlendirmeSonuc.NotList.GroupBy(x => x.GrupID);
            foreach (var dersGrup in dersGrupGrupla)
            {
                var degerlendirme = DegerlendirmeKayit.FirstOrDefault(x => x.FKDersGrupID == dersGrup.Key);
                if (degerlendirme == null)
                {
                    throw new DegerlendirmeException("DEG404: Grup için değerlendirme kaydı bulunamadı! Lütfen öğrenci işleri ile irtibata geçiniz.");
                }
                data.KaydetBasariNot(DegerlendirmeSonuc.NotList, OrtakGruplarInt, degerlendirme.ID, KullaniciAdi, IPAdresi, SeciliDers.Yil, SeciliDers.Donem, ButunlemeMi);
            }
            EnumDegerlendirmeAsama = EnumDegerlendirmeAsama.KAYITLI;
            NotificationList.Add(new Notification("Kaydedildi", "Değerlendirme işlemi başarıyla kaydedildi.", Notification.MasterMesajTip.Basari));
        }

        private OgrenciSinif sinifBelirle()
        {
            OgrenciSinif result = OgrenciSinif.Lisans;
            //if (OrtakGruplar.Any(x => x.Fakulte.Kod == "40" || x.Fakulte.Kod == "50" || x.Fakulte.Kod == "60" || x.Fakulte.Kod == "70"))
            if (OrtakGruplar.Any(x => x.Fakulte.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.Enstitu))
            {
                result = OgrenciSinif.YuksekLisans;
                NotificationList.Add(new Notification("Mutlak Değerlendirme", "Mutlak değerlendirme yapılacaktır.", Notification.MasterMesajTip.Bilgi));
            }
            return result;
        }

        public override void Sifirla(bool hesaplansinMi = true)
        {
            if (EnumDegerlendirmeAsama == EnumDegerlendirmeAsama.AKTARILDI)
            {
                throw new DegerlendirmeException("Öğrenci işlerine aktarımı gerçekleştirilen değerlendirmeler değiştirilemez!");
            }
            if (EnumDegerlendirmeAsama == EnumDegerlendirmeAsama.SONHAL)
            {
                throw new DegerlendirmeException("Son hal verilen değerlendirmeler değiştirilemez!");
            }
            if (EnumDegerlendirmeAsama == EnumDegerlendirmeAsama.YAYINLANDI)
            {
                throw new DegerlendirmeException("Değişiklik yapabilmek için lütfen önce değerlendirmeleri yayından kaldırınız!");
            }
            data.Sil(OrtakGruplar, ButunlemeMi);
            this.EnumDegerlendirmeAsama = EnumDegerlendirmeAsama.BOS;
            if (hesaplansinMi)
            {
                IlkHesaplamaYap();
            }
        }

        public override void Yayinla()
        {
            if (EnumDegerlendirmeAsama == EnumDegerlendirmeAsama.AKTARILDI)
            {
                throw new DegerlendirmeException("Öğrenci işlerine aktarımı gerçekleştirilen değerlendirmeler değiştirilemez!");
            }
            if (EnumDegerlendirmeAsama == EnumDegerlendirmeAsama.SONHAL)
            {
                throw new DegerlendirmeException("Son hal verilen değerlendirmeler değiştirilemez!");
            }
            if (EnumDegerlendirmeAsama == EnumDegerlendirmeAsama.YAYINLANDI)
            {
                throw new DegerlendirmeException("Değişiklik yapabilmek için lütfen önce değerlendirmeleri yayından kaldırınız!");
            }
            if (EnumDegerlendirmeAsama == EnumDegerlendirmeAsama.BOS)
            {
                throw new DegerlendirmeException("Yayınlama yapabilmek için önce hesaplama yapılmalıdır!");
            }
            data.Yayinla(DegerlendirmeKayit);
            List<Tuple<int, IPay>> yayinlanmayanPayList;
            if (!TumNotlarYayinlandiMi(PayList.ConvertAll(x => (IPay)x), out yayinlanmayanPayList))
            {
                //Ders.DersRepository dersRepo = new Ders.DersRepository();

                foreach (var item in yayinlanmayanPayList)
                {
                    PayYayinla(item.Item1, item.Item2.PayID, SeciliDers.Yil, SeciliDers.Donem);
                }
                NotificationList.Add(new Notification("Yıliçi Notlar", "Yayınlanmayan yıliçi notlar bulundu ve otomatik yayınlandı!", Notification.MasterMesajTip.Bilgi));
            }
            this.EnumDegerlendirmeAsama = EnumDegerlendirmeAsama.YAYINLANDI;
            NotificationList.Add(new Notification("Yayınlandı", "Değerlendirme sonuçları başarıyla yayınlandı.", Notification.MasterMesajTip.Basari));
        }

        protected override void PayYayinla(int dersGrupID, int payID, int yil, int donem)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var yayinBilgisi = db.ABSYayinlananPayNotlar.FirstOrDefault(d => d.FKDersGrupID == dersGrupID && d.FKABSPayID == payID && d.Yayinlandi == true);
                    if (yayinBilgisi == null)
                    {
                        ABSYayinlananPayNotlar yayinlananNotPay = new ABSYayinlananPayNotlar();
                        yayinlananNotPay.FKDersGrupID = dersGrupID;
                        yayinlananNotPay.FKABSPayID = payID;
                        yayinlananNotPay.YayinlanmaTarihi = DateTime.Now;
                        yayinlananNotPay.Yayinlandi = true;
                        yayinlananNotPay.EnumDonem = donem;
                        yayinlananNotPay.Yil = yil;
                        db.ABSYayinlananPayNotlar.Add(yayinlananNotPay);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Pay yayınlama işleminde hata oluştu. " + ex.Message); ;
            }
        }

        public override void YayindanKaldir()
        {
            if (EnumDegerlendirmeAsama == EnumDegerlendirmeAsama.AKTARILDI)
            {
                throw new DegerlendirmeException("Öğrenci işlerine aktarımı gerçekleştirilen değerlendirmeler değiştirilemez!");
            }
            if (EnumDegerlendirmeAsama == EnumDegerlendirmeAsama.SONHAL)
            {
                throw new DegerlendirmeException("Son hal verilen değerlendirmeler değiştirilemez!");
            }
            if (EnumDegerlendirmeAsama == EnumDegerlendirmeAsama.BOS)
            {
                throw new DegerlendirmeException("Yayından kaldırabilmek için önce yayınlanma yapılmalıdır!");
            }
            if (EnumDegerlendirmeAsama == EnumDegerlendirmeAsama.KAYITLI)
            {
                throw new DegerlendirmeException("Yayından kaldırabilmek için önce yayınlanma yapılmalıdır!");
            }
            data.YayindanKaldir(OrtakGruplarInt, ButunlemeMi);
            this.EnumDegerlendirmeAsama = EnumDegerlendirmeAsama.KAYITLI;
            NotificationList.Add(new Notification("Yayından Kaldırıldı", "Değerlendirme sonuçları yayından kaldırıldı!", Notification.MasterMesajTip.Uyari));
        }

        public override void SonHal()
        {
            if (!TarihUygunMu)
            {
                throw new DegerlendirmeException("Değerlendirme için geçersiz tarih aralığı!");
            }
            if (EnumDegerlendirmeAsama != EnumDegerlendirmeAsama.YAYINLANDI)
            {
                throw new DegerlendirmeException("Son hal işlemi için öncelikle değerlendirmeleri yayınlamalısınız!");
            }
            foreach (var item in DegerlendirmeKayit)
            {
                string barkodNo = string.Empty;
                Random Ramdom = new Random();
                int randomNumber = Ramdom.Next(10, 99);
                barkodNo = DateTime.Now.Minute.ToString() + randomNumber.ToString() + item.FKDersGrupID.ToString();
                item.BarkodNo = barkodNo;
            }
            data.SonHal(DegerlendirmeKayit, KullaniciAdi, IPAdresi);
            this.EnumDegerlendirmeAsama = EnumDegerlendirmeAsama.SONHAL;
            NotificationList.Add(new Notification("Son Hal", "Değerlendirme sonuçlarına son hal verildi!", Notification.MasterMesajTip.Basari));
        }

        public override void SonHalKaldir()
        {
            if (EnumDegerlendirmeAsama == EnumDegerlendirmeAsama.AKTARILDI)
            {
                throw new DegerlendirmeException("Öğrenci işlerine aktarılan değerlendirmeler geri alınamaz!");
            }
            data.SonHalKaldir(DegerlendirmeKayit);
            this.EnumDegerlendirmeAsama = EnumDegerlendirmeAsama.YAYINLANDI;
        }

        public BasariListesiDTO BasariListesiRapor(int? dersGrupID = null)
        {
            BasariListesiDTO result = new BasariListesiDTO();
            List<OgrenciPayveNotBilgileriDTO> resultNot = data.GetirBasariNotListesi(PersonelID, ButunlemeMi, dersGrupID);

            List<int> gruplar = new List<int>();
            if (dersGrupID.HasValue)
            {
                gruplar.Add(dersGrupID.Value);
            }
            else
            {
                gruplar.AddRange(OrtakGruplarInt);
            }
            List<OgrenciPayveNotBilgileriDTO> tumNotlarla = getirTumNotlarByGrupID(gruplar, PayListYiliciYok, false, SeciliDers.Yil, SeciliDers.Donem);
            foreach (var item in resultNot)
            {
                var tmpNotRow = tumNotlarla.FirstOrDefault(x => x.OgrenciID == item.OgrenciID);
                if (tmpNotRow != null)
                {
                    item.listPaylarVeNotlar = tmpNotRow.listPaylarVeNotlar;
                }
            }
            var degerlendirme = data.getirDegerlendirme(gruplar, ButunlemeMi).FirstOrDefault();
            if (degerlendirme == null)
            {
                throw new DegerlendirmeException("Değerlendirme bilgisi veritabanında bulunamadığı için rapor oluşturulamadı!");
            }
            if (degerlendirme.EnumSonHal == (int)EnumSonHal.EVET)
            {
                result.SonHalTarihi = degerlendirme.Tarih.Value;
                result.StandartSapma = degerlendirme.StandartSapma.Value;
                result.AritmetikOrtalama = degerlendirme.Ortalama.Value;
                result.Barkod = degerlendirme.BarkodNo;
                result.EBYSDocTreeID = degerlendirme.EBYSDocTreeID;
            }
            //Kopyalama
            result.Ders = new DersDTO();
            result.Ders.ButunlemeMi = SeciliDers.ButunlemeMi;
            result.Ders.ButunlemeMiOrijinal = SeciliDers.ButunlemeMiOrijinal;
            result.Ders.DersAd = SeciliDers.DersAd;
            result.Ders.DersGrupAd = SeciliDers.DersGrupAd;
            result.Ders.DersGrupID = SeciliDers.DersGrupID;
            result.Ders.DersID = SeciliDers.DersID;
            result.Ders.DersKod = SeciliDers.DersKod;
            result.Ders.DersPlanAnaID = SeciliDers.DersPlanAnaID;
            result.Ders.DersPlanID = SeciliDers.DersPlanID;
            result.Ders.Donem = SeciliDers.Donem;
            result.Ders.DSaat = SeciliDers.DSaat;
            result.Ders.EnumDersPlanOrtalama = SeciliDers.EnumDersPlanOrtalama;
            result.Ders.EnumWebGosterim = SeciliDers.EnumWebGosterim;
            result.Ders.EnumZorunluDers = SeciliDers.EnumZorunluDers;
            result.Ders.Fakulte = SeciliDers.Fakulte;
            result.Ders.FinalTarihi = SeciliDers.FinalTarihi;
            result.Ders.GrupCalismaOgrenciIDleri = SeciliDers.GrupCalismaOgrenciIDleri;
            result.Ders.Kredi = SeciliDers.Kredi;
            result.Ders.OgrenciSayisi = SeciliDers.OgrenciSayisi;
            result.Ders.OgretimTuru = SeciliDers.OgretimTuru;
            result.Ders.OgretimTuruAd = SeciliDers.OgretimTuruAd;
            result.Ders.Personel = SeciliDers.Personel;
            result.Ders.SonHal = SeciliDers.SonHal;
            result.Ders.USaat = SeciliDers.USaat;
            result.Ders.Yariyil = SeciliDers.Yariyil;
            result.Ders.Yil = SeciliDers.Yil;
            //Kopyalama bitiş
            Core.ABS.DEGERLENDIRME.Grup grupBilgi = data.GetirGrupBilgi(dersGrupID);
            result.Ders.DersGrupAd = grupBilgi.Ad;
            result.Ders.DersGrupID = grupBilgi.DersGrupID;
            result.Ders.OgretimTuru = (int)grupBilgi.EnumOgretimTur;
            result.Ders.OgretimTuruAd = Sabis.Enum.Utils.GetEnumDescription(grupBilgi.EnumOgretimTur);
            result.Bolum = SeciliDers.Bolum;
            result.Fakulte = SeciliDers.Fakulte;
            result.NotList = resultNot;
            return result;
        }

        public List<BasariListesiDTO> BasariListesiRaporList()
        {
            List<BasariListesiDTO> result = new List<BasariListesiDTO>();
            foreach (var grup in OrtakGruplar)
            {
                var raporItem = BasariListesiRapor(grup.DersGrupID);
                result.Add(raporItem);
            }
            return result;
        }

        public void EBYSAktarimaUygunMu(int grupID, out int ebysID)
        {
            data.EBYSAktarimaUygunMu(grupID, out ebysID);
        }

        public static void BasariRaporKaydet(int grupID, string dosyaKey, string kullanici, string IP)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var degerlendirme = db.ABSDegerlendirme.FirstOrDefault(x => x.FKDersGrupID == grupID && !x.SilindiMi);
                if (degerlendirme != null)
                {

                    var tmp = new ABSDegerlendirmeDosya
                    {
                        DosyaKey = dosyaKey,
                        TarihKayit = DateTime.Now,
                        IP = IP,
                        Kullanici = kullanici,
                        FKDegerlendirmeID = degerlendirme.ID,
                        FKDersGrupID = grupID
                    };

                    degerlendirme.DosyaKey = dosyaKey;

                    db.ABSDegerlendirmeDosya.Add(tmp);

                    db.SaveChanges();
                }
            }
        }

        public static List<DegerlendirmeDosya> BasariRaporListele(int grupID)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                return db.ABSDegerlendirmeDosya.Where(x => x.FKDersGrupID == grupID).OrderBy(x => x.ID).Select(x => new DegerlendirmeDosya
                {
                    DosyaKey = x.DosyaKey,
                    Tarih = x.TarihKayit
                }).ToList();
            }
        }
    }
}
