﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sabis.Bolum.Core.ABS.DEGERLENDIRME;


namespace Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME
{
    public class DegerlendirmeBL
    {
        public static DegerlendirmeViewModel GetirDegerlendirme(int ID, bool hiyerarsik, string kullaniciAdi, int personelID, string IP, string simuleEden, int yil, int donem)
        {
            DegerlendirmeViewModel model = new DegerlendirmeViewModel();
            model.NotificationList = new List<Notification>();
            //#if !DEBUG
            try
            {
                //#endif
                if (hiyerarsik)
                {
                    HDegerlendirmeYonetim deg = new HDegerlendirmeYonetim(ID, yil, donem, kullaniciAdi, personelID, IP, simuleEden);
                    model.DegerlendirmeCikti = deg.DegerlendirmeSonuc;
                    model.Ders = deg.SeciliDers;
                    model.AkademikTakvim = deg.AkademikTakvim;
                    model.NotificationList = deg.NotificationList;
                    model.EnumDegerlendirmeAsama = deg.EnumDegerlendirmeAsama;
                    model.SonHalKaldirmaYetkisiVarMi = deg.SonHalKaldirmaYetkisiVarMi;
                    model.TarihAraligiUygunMu = deg.TarihUygunMu;
                    model.ButunlemeMi = deg.ButunlemeMi;
                    model.DegerlendiriciMi = deg.DegerlendiriciMi;
                }
                else
                {
                    DegerlendirmeYonetim deg = new DegerlendirmeYonetim(ID, kullaniciAdi, personelID, IP, simuleEden);
                    model.DegerlendirmeCikti = deg.DegerlendirmeSonuc;
                    model.Ders = deg.SeciliDers;
                    model.AkademikTakvim = deg.AkademikTakvim;
                    model.NotificationList = deg.NotificationList;
                    model.OrtakGruplar = deg.OrtakGruplar;
                    model.EnumDegerlendirmeAsama = deg.EnumDegerlendirmeAsama;
                    model.SonHalKaldirmaYetkisiVarMi = deg.SonHalKaldirmaYetkisiVarMi;
                    model.TarihAraligiUygunMu = deg.TarihUygunMu;
                    model.ButunlemeMi = deg.ButunlemeMi;
                    model.DegerlendiriciMi = deg.DegerlendiriciMi;
                }

                if (model.DegerlendirmeCikti != null)
                {
                    model.DegerlendirmeCikti.Ortalama = Math.Round(model.DegerlendirmeCikti.Ortalama, 3);
                    model.DegerlendirmeCikti.BagilAritmetikOrtalama = Math.Round(model.DegerlendirmeCikti.BagilAritmetikOrtalama, 3);
                    model.DegerlendirmeCikti.StandartSapma = Math.Round(model.DegerlendirmeCikti.StandartSapma, 3);
                }

                //#if !DEBUG
            }
            catch (Exception ex)
            {
                model.NotificationList.Add(new Notification("HATA", ex.Message, Notification.MasterMesajTip.Hata));
            }
            //#endif
            return model;
        }

        public static DegerlendirmeViewModel SonHalSerbest(int grupID, string kullaniciAdi, int personelID, string IP, string simuleEden)
        {
            DegerlendirmeViewModel model = new DegerlendirmeViewModel();
            model.NotificationList = new List<Notification>();
             
            try
            {
                BasariNotYonetim degerlendirmeBL = new BasariNotYonetim(grupID, personelID, IP, kullaniciAdi);
                //degerlendirmeBL.SonHaleUygunMu();

                //degerlendirmeBL.Yayinla();  
                degerlendirmeBL.SonHal();
            }
            catch (Exception ex)
            {
                model.NotificationList.Add(new Notification("HATA", ex.Message, Notification.MasterMesajTip.Hata));
            }

            return model;
        }

        public static DegerlendirmeViewModel SonHalKaldirSerbest(int grupID, string kullaniciAdi, int personelID, string IP, string simuleEden)
        {
            DegerlendirmeViewModel model = new DegerlendirmeViewModel();
            model.NotificationList = new List<Notification>();
            try
            {
                BasariNotYonetim degerlendirmeBL = new BasariNotYonetim(grupID, personelID, IP, kullaniciAdi);
                degerlendirmeBL.SonHalKaldir();
            }
            catch (Exception ex)
            {
                model.NotificationList.Add(new Notification("HATA", ex.Message, Notification.MasterMesajTip.Hata));
            }

            return model;
        }

        public static DegerlendirmeViewModel YayinlaSerbest(int grupID, string kullaniciAdi, int personelID, string IP, string simuleEden)
        {
            DegerlendirmeViewModel model = new DegerlendirmeViewModel();
            model.NotificationList = new List<Notification>();
            try
            {
                BasariNotYonetim degerlendirmeBL = new BasariNotYonetim(grupID, personelID, IP, kullaniciAdi);
                degerlendirmeBL.Yayinla();
            }
            catch (Exception ex)
            {
                model.NotificationList.Add(new Notification("HATA", ex.Message, Notification.MasterMesajTip.Hata));
            }

            return model;
        }

        public static DegerlendirmeViewModel YayindanKaldirSerbest(int grupID, string kullaniciAdi, int personelID, string IP, string simuleEden)
        {
            DegerlendirmeViewModel model = new DegerlendirmeViewModel();
            model.NotificationList = new List<Notification>();
            try
            {
                BasariNotYonetim degerlendirmeBL = new BasariNotYonetim(grupID, personelID, IP, kullaniciAdi);
                degerlendirmeBL.YayindanKaldir();
            }
            catch (Exception ex)
            {
                model.NotificationList.Add(new Notification("HATA", ex.Message, Notification.MasterMesajTip.Hata));
            }

            return model;
        }

        public static BasariDegerlendirmeVM BasariDegerlendirmeDurum(int grupID, string kullaniciAdi, int personelID, string IP, string simuleEden)
        {
            BasariNotYonetim degerlendirmeBL = new BasariNotYonetim(grupID, personelID, IP, kullaniciAdi);

            BasariDegerlendirmeVM result = new BasariDegerlendirmeVM
            {
                EnumDegerlendirmeAsama = degerlendirmeBL.EnumDegerlendirmeAsama,
                SonHalYetkisiVarMi = true, //degerlendirmeBL.KoordinatorMu, // RMZN edit : her hoca kendi dersine son hal verebilir.
                SonHalKaldirmaYetkisiVarMi = degerlendirmeBL.SonHalKaldirmaYetkisiVarMi,
                OrtakGruplar = degerlendirmeBL.OrtakGruplar,
                DersGrupID = grupID,
                NotificationList = degerlendirmeBL.NotificationList
            };

            return result;
        }

        public static BasariListesiDTO BasariListesiRapor(int ID, int yil, int donem, string kullaniciAdi, int personelID, string IP, string simuleEden, bool hiyerarsik = false)
        {
            BasariListesiDTO result = null;
            try
            {
                if (hiyerarsik)
                {
                    HDegerlendirmeYonetim deg = new HDegerlendirmeYonetim(ID, yil, donem, kullaniciAdi, personelID, IP, simuleEden);
                    result = deg.BasariListesiRapor();
                }
                else
                {
                    DegerlendirmeYonetim deg = new DegerlendirmeYonetim(ID, kullaniciAdi, personelID, IP, simuleEden);
                    result = deg.BasariListesiRapor(ID);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public static BasariListesiDTO BasariListesiRaporSerbest(int grupID, string kullaniciAdi, int personelID, string IP, string simuleEden)
        {
            BasariListesiDTO result = null;
            try
            {
                BasariNotYonetim degerlendirmeBL = new BasariNotYonetim(grupID, personelID, IP, kullaniciAdi);
                result = degerlendirmeBL.BasariListesiRapor(grupID);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public static BasariListesiDTO BasariListesiRaporSerbestFinal(int grupID, string kullaniciAdi, int personelID, string IP, string simuleEden)
        {
            BasariListesiDTO result = null;
            try
            {
                BasariNotYonetim degerlendirmeBL = new BasariNotYonetim(grupID, personelID, IP, kullaniciAdi);
                result = degerlendirmeBL.BasariListesiRaporFinal(grupID);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public static BasariListesiDTO BasariListesiRaporSerbestButunleme(int grupID, string kullaniciAdi, int personelID, string IP, string simuleEden)
        {
            BasariListesiDTO result = null;
            try
            {
                BasariNotYonetim degerlendirmeBL = new BasariNotYonetim(grupID, personelID, IP, kullaniciAdi);
                result = degerlendirmeBL.BasariListesiRaporButunleme(grupID);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }



        public static bool BasariRaporKaydet(int grupID, string dosyaKey, string kullanici, string IP)
        {
            try
            {
                DegerlendirmeYonetim.BasariRaporKaydet(grupID, dosyaKey, kullanici, IP);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return true;
        }

        public static List<DegerlendirmeDosya> BasariRaporListele(int grupID)
        {
            List<DegerlendirmeDosya> result;
            try
            {
                result = DegerlendirmeYonetim.BasariRaporListele(grupID);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public static List<BasariListesiDTO> BasariListesiRaporList(int grupID, string kullaniciAdi, int personelID, string IP, string simuleEden)
        {
            List<BasariListesiDTO> result = null;
            try
            {
                DegerlendirmeYonetim deg = new DegerlendirmeYonetim(grupID, kullaniciAdi, personelID, IP, simuleEden);
                result = deg.BasariListesiRaporList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public static DegerlendirmeViewModel Hesapla(double bagilOrtalama, double ustSinir, int grupID, string kullaniciAdi, int personelID, string IP, string simuleEden)
        {
            DegerlendirmeViewModel model = new DegerlendirmeViewModel();
            model.NotificationList = new List<Notification>();
            try
            {
                DegerlendirmeYonetim deg = new DegerlendirmeYonetim(grupID, kullaniciAdi, personelID, IP, simuleEden);
                deg.Degerlendir(ustSinir, bagilOrtalama);
                deg.Kaydet();
                model.DegerlendirmeCikti = deg.DegerlendirmeSonuc;
                model.Ders = deg.SeciliDers;
                model.AkademikTakvim = deg.AkademikTakvim;
                model.NotificationList = deg.NotificationList;
                model.OrtakGruplar = deg.OrtakGruplar;
                model.EnumDegerlendirmeAsama = deg.EnumDegerlendirmeAsama;
                model.SonHalKaldirmaYetkisiVarMi = deg.SonHalKaldirmaYetkisiVarMi;
            }
            catch (Exception ex)
            {
                model.NotificationList.Add(new Notification("HATA", ex.Message, Notification.MasterMesajTip.Hata));
            }

            return model;
        }

        public static DegerlendirmeViewModel Kaydet(int grupID, string kullaniciAdi, int personelID, string IP, string simuleEden)
        {
            DegerlendirmeViewModel model = new DegerlendirmeViewModel();
            model.NotificationList = new List<Notification>();
            try
            {
                DegerlendirmeYonetim deg = new DegerlendirmeYonetim(grupID, kullaniciAdi, personelID, IP, simuleEden);
                deg.Kaydet();
                model.DegerlendirmeCikti = deg.DegerlendirmeSonuc;
                model.Ders = deg.SeciliDers;
                model.AkademikTakvim = deg.AkademikTakvim;
                model.NotificationList = deg.NotificationList;
                model.OrtakGruplar = deg.OrtakGruplar;
                model.EnumDegerlendirmeAsama = deg.EnumDegerlendirmeAsama;
                model.SonHalKaldirmaYetkisiVarMi = deg.SonHalKaldirmaYetkisiVarMi;
            }
            catch (Exception ex)
            {
                model.NotificationList.Add(new Notification("HATA", ex.Message, Notification.MasterMesajTip.Hata));
            }

            return model;
        }

        public static DegerlendirmeViewModel Sifirla(int grupID, string kullaniciAdi, int personelID, string IP, string simuleEden)
        {
            DegerlendirmeViewModel model = new DegerlendirmeViewModel();
            model.NotificationList = new List<Notification>();
            try
            {
                DegerlendirmeYonetim deg = new DegerlendirmeYonetim(grupID, kullaniciAdi, personelID, IP, simuleEden);
                deg.Sifirla();
                model.DegerlendirmeCikti = deg.DegerlendirmeSonuc;
                model.Ders = deg.SeciliDers;
                model.AkademikTakvim = deg.AkademikTakvim;
                model.NotificationList = deg.NotificationList;
                model.OrtakGruplar = deg.OrtakGruplar;
                model.EnumDegerlendirmeAsama = deg.EnumDegerlendirmeAsama;
                model.SonHalKaldirmaYetkisiVarMi = deg.SonHalKaldirmaYetkisiVarMi;
            }
            catch (Exception ex)
            {
                model.NotificationList.Add(new Notification("HATA", ex.Message, Notification.MasterMesajTip.Hata));
            }

            return model;
        }

        public static DegerlendirmeViewModel Yayinla(int grupID, string kullaniciAdi, int personelID, string IP, string simuleEden)
        {
            DegerlendirmeViewModel model = new DegerlendirmeViewModel();
            model.NotificationList = new List<Notification>();
            try
            {
                DegerlendirmeYonetim deg = new DegerlendirmeYonetim(grupID, kullaniciAdi, personelID, IP, simuleEden);
                deg.Yayinla();
                model.DegerlendirmeCikti = deg.DegerlendirmeSonuc;
                model.Ders = deg.SeciliDers;
                model.AkademikTakvim = deg.AkademikTakvim;
                model.NotificationList = deg.NotificationList;
                model.OrtakGruplar = deg.OrtakGruplar;
                model.EnumDegerlendirmeAsama = deg.EnumDegerlendirmeAsama;
                model.SonHalKaldirmaYetkisiVarMi = deg.SonHalKaldirmaYetkisiVarMi;
            }
            catch (Exception ex)
            {
                model.NotificationList.Add(new Notification("HATA", ex.Message, Notification.MasterMesajTip.Hata));
            }

            return model;
        }

        public static DegerlendirmeViewModel YayindanKaldir(int grupID, string kullaniciAdi, int personelID, string IP, string simuleEden)
        {
            DegerlendirmeViewModel model = new DegerlendirmeViewModel();
            model.NotificationList = new List<Notification>();
            try
            {
                DegerlendirmeYonetim deg = new DegerlendirmeYonetim(grupID, kullaniciAdi, personelID, IP, simuleEden);
                deg.YayindanKaldir();
                model.DegerlendirmeCikti = deg.DegerlendirmeSonuc;
                model.Ders = deg.SeciliDers;
                model.AkademikTakvim = deg.AkademikTakvim;
                model.NotificationList = deg.NotificationList;
                model.OrtakGruplar = deg.OrtakGruplar;
                model.EnumDegerlendirmeAsama = deg.EnumDegerlendirmeAsama;
                model.SonHalKaldirmaYetkisiVarMi = deg.SonHalKaldirmaYetkisiVarMi;
            }
            catch (Exception ex)
            {
                model.NotificationList.Add(new Notification("HATA", ex.Message, Notification.MasterMesajTip.Hata));
            }

            return model;
        }

        public static DegerlendirmeViewModel SonHal(int grupID, string kullaniciAdi, int personelID, string IP, string simuleEden)
        {
            DegerlendirmeViewModel model = new DegerlendirmeViewModel();
            model.NotificationList = new List<Notification>();
            try
            {
                DegerlendirmeYonetim deg = new DegerlendirmeYonetim(grupID, kullaniciAdi, personelID, IP, simuleEden);
                deg.Yayinla();
                deg.SonHal();
                model.DegerlendirmeCikti = deg.DegerlendirmeSonuc;
                model.Ders = deg.SeciliDers;
                model.AkademikTakvim = deg.AkademikTakvim;
                model.NotificationList = deg.NotificationList;
                model.OrtakGruplar = deg.OrtakGruplar;
                model.EnumDegerlendirmeAsama = deg.EnumDegerlendirmeAsama;
                model.SonHalKaldirmaYetkisiVarMi = deg.SonHalKaldirmaYetkisiVarMi;
            }
            catch (Exception ex)
            {
                model.NotificationList.Add(new Notification("HATA", ex.Message, Notification.MasterMesajTip.Hata));
            }

            return model;
        }

        public static DegerlendirmeViewModel SonHalKaldir(int grupID, string kullaniciAdi, int personelID, string IP, string simuleEden)
        {
            DegerlendirmeViewModel model = new DegerlendirmeViewModel();
            model.NotificationList = new List<Notification>();
            try
            {
                DegerlendirmeYonetim deg = new DegerlendirmeYonetim(grupID, kullaniciAdi, personelID, IP, simuleEden);
                deg.SonHalKaldir();
                model.DegerlendirmeCikti = deg.DegerlendirmeSonuc;
                model.Ders = deg.SeciliDers;
                model.AkademikTakvim = deg.AkademikTakvim;
                model.NotificationList = deg.NotificationList;
                model.OrtakGruplar = deg.OrtakGruplar;
                model.EnumDegerlendirmeAsama = deg.EnumDegerlendirmeAsama;
                model.SonHalKaldirmaYetkisiVarMi = deg.SonHalKaldirmaYetkisiVarMi;
            }
            catch (Exception ex)
            {
                model.NotificationList.Add(new Notification("HATA", ex.Message, Notification.MasterMesajTip.Hata));
            }

            return model;
        }

        //public static CiktiDTO HDegerlendir(int dersPlanAnaID, int yil, int donem, int personelID, string IP)
        //{
        //    //DegerlendirmeViewModel model = new DegerlendirmeViewModel();
        //    //model.NotificationList = new List<Notification>();
        //    //try
        //    //{
        //    //    DegerlendirmeYonetim deg = new DegerlendirmeYonetim(grupID, kullaniciAdi, personelID, IP, simuleEden);
        //    //    deg.SonHalKaldir();
        //    //    model.DegerlendirmeCikti = deg.DegerlendirmeSonuc;
        //    //    model.Ders = deg.SeciliDers;
        //    //    model.AkademikTakvim = deg.AkademikTakvim;
        //    //    model.NotificationList = deg.NotificationList;
        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //    model.NotificationList.Add(new Notification("HATA", ex.Message, Notification.MasterMesajTip.Hata));
        //    //}
        //    var model = HDegerlendirmeYonetimEski.Degerlendir(dersPlanAnaID, yil, donem, personelID, IP);
        //    return model;
        //}

        public static List<string> GetirDersGrupBasariListeleri(int fkDersGrupID)
        {
            using (DATA.UysModel.UYSv2Entities db = new DATA.UysModel.UYSv2Entities())
            {
                return db.ABSDegerlendirme.Where(x => x.FKDersGrupID == fkDersGrupID && !x.SilindiMi).Select(x => x.DosyaKey).ToList();
            }
        }

        public static List<string> GetirEksikNotGirilenDersGruplari(int fkDersPlanAnaID, int fkDersGrupID, int yil, int donem)
        {
            using (DATA.UysModel.UYSv2Entities db = new DATA.UysModel.UYSv2Entities())
            {
                List<string> eksikGrupList = new List<string>();
                int payCount = db.ABSPaylar.Count(x => !x.Silindi && x.FKDersPlanAnaID == fkDersPlanAnaID && x.Yil == yil && x.EnumDonem == donem && x.EnumCalismaTip != (int)Sabis.Enum.EnumCalismaTip.YilIcininBasariya);

                int ortakDegerlendirmeID;
                List<int> ortakDegerlendirmeGrupList = new List<int>();

                if (db.ABSOrtakDegerlendirmeGrup.Any(x => x.FKDersGrupID == fkDersGrupID))
                {
                    ortakDegerlendirmeID = db.ABSOrtakDegerlendirmeGrup.FirstOrDefault(x => x.FKDersGrupID == fkDersGrupID).FKOrtakDegerlendirmeID;
                    ortakDegerlendirmeGrupList = db.ABSOrtakDegerlendirmeGrup.Where(x => x.FKOrtakDegerlendirmeID == ortakDegerlendirmeID).Select(x => x.FKDersGrupID).ToList();
                }

                var dersGrupList = db.OGRDersGrup.Where(x => x.FKDersPlanAnaID == fkDersPlanAnaID && x.OgretimYili == yil && x.EnumDonem == donem && ortakDegerlendirmeGrupList.Contains(x.ID) && x.Acik && !x.Silindi).ToList();
                var dersGrupIDList = dersGrupList.Select(x => x.ID).ToList();
                var ogrList = db.OGROgrenciYazilma.Where(x => dersGrupIDList.Contains(x.FKDersGrupID.Value) && x.Yil == yil && x.EnumDonem == donem && !x.Iptal && x.OGRKimlik.EnumOgrDurum == (int)Sabis.Enum.EnumOgrDurum.NORMAL).ToList();

                foreach (var dersGrup in dersGrupList)
                {
                    int ogrSayi = ogrList.Count(x => x.FKDersGrupID == dersGrup.ID) * payCount;
                    int notSayi = db.ABSOgrenciNot.Count(x => x.FKDersGrupID == dersGrup.ID && !x.Silindi);

                    if (ogrSayi > notSayi)
                    {
                        var personel = dersGrup.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi;
                        eksikGrupList.Add(personel.Ad + " " + personel.Soyad + "-" + dersGrup.OGRDersPlanAna.DersAd + " " + Sabis.Enum.Utils.GetEnumDescription((Sabis.Enum.EnumOgretimTur)dersGrup.EnumOgretimTur) + " " + dersGrup.GrupAd + " Grubu");
                    }
                }
                return eksikGrupList;
            }
        }

        public static List<string> GetirYayinlanmayanPayListesi(int fkDersPlanAnaID, int fkDersGrupID, int yil, int donem)
        {
            using (DATA.UysModel.UYSv2Entities db = new DATA.UysModel.UYSv2Entities())
            {
                var payList = db.ABSPaylar.Where(x => !x.Silindi && x.FKDersPlanAnaID == fkDersPlanAnaID && x.Yil == yil && x.EnumDonem == donem && x.EnumCalismaTip != (int)Sabis.Enum.EnumCalismaTip.YilIcininBasariya);

                int ortakDegerlendirmeID;
                List<int> ortakDegerlendirmeGrupList = new List<int>();

                if (db.ABSOrtakDegerlendirmeGrup.Any(x => x.FKDersGrupID == fkDersGrupID))
                {
                    ortakDegerlendirmeID = db.ABSOrtakDegerlendirmeGrup.FirstOrDefault(x => x.FKDersGrupID == fkDersGrupID).FKOrtakDegerlendirmeID;
                    ortakDegerlendirmeGrupList = db.ABSOrtakDegerlendirmeGrup.Where(x => x.FKOrtakDegerlendirmeID == ortakDegerlendirmeID).Select(x => x.FKDersGrupID).ToList();
                }

                var dersGrupList = db.OGRDersGrup.Where(x => x.FKDersPlanAnaID == fkDersPlanAnaID && x.OgretimYili == yil && x.EnumDonem == donem && ortakDegerlendirmeGrupList.Contains(x.ID) && x.Acik && !x.Silindi).ToList();
                var dersGrupIDList = dersGrupList.Select(x => x.ID).ToList();
                var yayinlananPayList = db.ABSYayinlananPayNotlar.Where(x => dersGrupIDList.Contains(x.FKDersGrupID.Value) && x.Yayinlandi);

                List<string> yayinlanmayanPaylar = new List<string>();

                foreach (var dersGrup in dersGrupList)
                {
                    if (!dersGrup.OGRDersGrupHoca.Any())
                    {
                        continue;
                    }
                    var personel = dersGrup.OGRDersGrupHoca.FirstOrDefault().PersonelBilgisi;
                    foreach (var pay in payList)
                    {
                        if (!yayinlananPayList.Any(x => x.FKDersGrupID == dersGrup.ID && x.FKABSPayID == pay.ID) && personel != null)
                        {
                            yayinlanmayanPaylar.Add(personel.Ad + " " + personel.Soyad + " - " + dersGrup.OGRDersPlanAna.DersAd + " " + dersGrup.EnumOgretimTur + " " + dersGrup.GrupAd + " Grubu " + pay.Sira + ". " + Sabis.Enum.Utils.GetEnumDescription((Sabis.Enum.EnumCalismaTip)pay.EnumCalismaTip));
                        }
                    }
                }
                return yayinlanmayanPaylar;
            }

        }
    }
}
