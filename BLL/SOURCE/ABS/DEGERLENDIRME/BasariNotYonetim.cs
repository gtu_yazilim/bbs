﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sabis.Enum;
using Sabis.Bolum.Core.ABS.DEGERLENDIRME;

namespace Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME
{
    public class BasariNotYonetim : AbstractDegerlendirme
    {
        BasariNotYonetimData dataBasariNot = new BasariNotYonetimData();
        public bool KoordinatorMu { get; set; }
        public List<Grup> OrtakGruplar { get; set; }
        private List<int> OrtakGruplarInt;
        public BasariNotYonetim(int grupID, int personelID, string ipAdres, string kullaniciAdi)
        {

            SeciliDers = GetirDersDTO(grupID);
            Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME.OrtakGrupYonetim.Olustur(SeciliDers.DersPlanAnaID, SeciliDers.Yil, SeciliDers.Donem, kullaniciAdi);
            KoordinatorMu = koordinatorMu(grupID, personelID);
            Init(kullaniciAdi, personelID, ipAdres, kullaniciAdi);

            if (SeciliDers.EnumDegerlendirmeTipi == EnumDegerlendirmeTipi.BASARI_NOT)
            {
                try
                {
                    OrtakGruplar = data.OrtakDegerlendirmeGrupBul(grupID, ButunlemeMi);
                }
                catch (Exception e)
                {
                    NotificationList.Add(new Notification("Ortak Grup", e.Message, Notification.MasterMesajTip.Bilgi));
                }

                OrtakGruplarInt = new List<int>();
                OrtakGruplarInt.Add(grupID);
            }
            


           
            DegerlendirmeKayit = dataBasariNot.getirDegerlendirme(grupID, ButunlemeMi);
            EnumDegerlendirmeAsama = AsamaBul(DegerlendirmeKayit);

            if (!KoordinatorMu)
            {
                NotificationList.Add(new Notification("Koordinatör", "Son hal işlemini dersin koordinatörleri gerçekleştirebilir!", Notification.MasterMesajTip.Bilgi));
            }
        }


        public BasariListesiDTO GetirBasariListesiNot()
        {
            BasariListesiDTO result = new BasariListesiDTO();
            List<OgrenciPayveNotBilgileriDTO> notListesi = dataBasariNot.GetirBasariNotlar(SeciliDers, PersonelID, KoordinatorMu);
            result.Ders = SeciliDers;
            result.Bolum = SeciliDers.Bolum;
            result.Fakulte = SeciliDers.Fakulte;
            result.NotList = notListesi;
            return result;
        }

        private bool koordinatorMu(int bolumID, int personelID)
        {
            return dataBasariNot.KoordinatorMu(bolumID, personelID);
        }

        public BasariListesiDTO BasariListesiRapor(int? dersGrupID)
        {
            BasariListesiDTO result = new BasariListesiDTO();
            List<OgrenciPayveNotBilgileriDTO> resultNot = dataBasariNot.GetirBasariNotListesi(PersonelID, ButunlemeMi, dersGrupID);


            var degerlendirme = dataBasariNot.getirDegerlendirme(dersGrupID.Value, ButunlemeMi).FirstOrDefault();
            if (degerlendirme == null)
            {
                throw new Exception("Değerlendirme bilgisi veritabanında bulunamadığı için rapor oluşturulamadı!");
            }

            if (degerlendirme.EnumSonHal == (int)EnumSonHal.EVET)
            {
                result.SonHalTarihi = degerlendirme.Tarih.Value;
                result.StandartSapma = degerlendirme.StandartSapma.Value;
                result.AritmetikOrtalama = degerlendirme.Ortalama.Value;
                result.Barkod = degerlendirme.BarkodNo;
            }
            result.Ders = SeciliDers;
            result.Bolum = SeciliDers.Bolum;
            result.Fakulte = SeciliDers.Fakulte;
            result.NotList = resultNot;
            return result;
        }

        public BasariListesiDTO BasariListesiRaporButunleme(int? dersGrupID)
        {
            BasariListesiDTO result = new BasariListesiDTO();
            List<OgrenciPayveNotBilgileriDTO> resultNot = dataBasariNot.GetirBasariNotListesi(PersonelID, true, dersGrupID);


            var degerlendirme = dataBasariNot.getirDegerlendirme(dersGrupID.Value, true).FirstOrDefault();
            if (degerlendirme == null)
            {
                throw new Exception("Değerlendirme bilgisi veritabanında bulunamadığı için rapor oluşturulamadı!");
            }

            if (degerlendirme.EnumSonHal == (int)EnumSonHal.EVET)
            {
                result.SonHalTarihi = degerlendirme.Tarih.Value;
                result.StandartSapma = degerlendirme.StandartSapma.Value;
                result.AritmetikOrtalama = degerlendirme.Ortalama.Value;
                result.Barkod = degerlendirme.BarkodNo;
            }
            result.Ders = SeciliDers;
            result.Bolum = SeciliDers.Bolum;
            result.Fakulte = SeciliDers.Fakulte;
            result.NotList = resultNot;
            return result;
        }

        public BasariListesiDTO BasariListesiRaporFinal(int? dersGrupID)
        {
            BasariListesiDTO result = new BasariListesiDTO();
            List<OgrenciPayveNotBilgileriDTO> resultNot = dataBasariNot.GetirBasariNotListesi(PersonelID, false, dersGrupID);


            var degerlendirme = dataBasariNot.getirDegerlendirme(dersGrupID.Value, false).FirstOrDefault();
            if (degerlendirme == null)
            {
                throw new Exception("Değerlendirme bilgisi veritabanında bulunamadığı için rapor oluşturulamadı!");
            }

            if (degerlendirme.EnumSonHal == (int)EnumSonHal.EVET)
            {
                result.SonHalTarihi = degerlendirme.Tarih.Value;
                result.StandartSapma = degerlendirme.StandartSapma.Value;
                result.AritmetikOrtalama = degerlendirme.Ortalama.Value;
                result.Barkod = degerlendirme.BarkodNo;
            }
            result.Ders = SeciliDers;
            result.Bolum = SeciliDers.Bolum;
            result.Fakulte = SeciliDers.Fakulte;
            result.NotList = resultNot;
            return result;
        }

        public override void SonHal()
        {
            if (EnumDegerlendirmeAsama != EnumDegerlendirmeAsama.YAYINLANDI)
            {
                throw new Exception("Son hal işlemi için öncelikle değerlendirmeleri yayınlamalısınız!");
            }
            List<OgrenciPayveNotBilgileriDTO> notListesi = dataBasariNot.GetirBasariNotlar(SeciliDers, PersonelID, KoordinatorMu);
            if (notListesi.Any(x => x.BasariHarf == EnumHarfBasari.BELIRSIZ))
            {
                throw new Exception("Belirsiz notlar ile son hal verilemez!");
            }
            foreach (var item in DegerlendirmeKayit)
            {
                string barkodNo = string.Empty;
                Random Ramdom = new Random();
                int randomNumber = Ramdom.Next(10, 99);
                barkodNo = DateTime.Now.Minute.ToString() + randomNumber.ToString() + item.FKDersGrupID.ToString();
                item.BarkodNo = barkodNo;
            }
            dataBasariNot.SonHal(DegerlendirmeKayit, KullaniciAdi, IPAdresi);
            this.EnumDegerlendirmeAsama = EnumDegerlendirmeAsama.SONHAL;
            NotificationList.Add(new Notification("Son Hal", "Değerlendirme sonuçlarına son hal verildi!", Notification.MasterMesajTip.Basari));
        }

        public override void Yayinla()
        {
            if (EnumDegerlendirmeAsama == EnumDegerlendirmeAsama.AKTARILDI)
            {
                throw new Exception("Öğrenci işlerine aktarımı gerçekleştirilen değerlendirmeler değiştirilemez!");
            }
            if (EnumDegerlendirmeAsama == EnumDegerlendirmeAsama.SONHAL)
            {
                throw new Exception("Son hal verilen değerlendirmeler değiştirilemez!");
            }
            if (EnumDegerlendirmeAsama == EnumDegerlendirmeAsama.YAYINLANDI)
            {
                throw new Exception("Değişiklik yapabilmek için lütfen önce değerlendirmeleri yayından kaldırınız!");
            }
            if (EnumDegerlendirmeAsama == EnumDegerlendirmeAsama.BOS)
            {
                throw new Exception("Yayınlama yapabilmek için önce hesaplama yapılmalıdır!");
            }
            dataBasariNot.Yayinla(DegerlendirmeKayit);

            this.EnumDegerlendirmeAsama = EnumDegerlendirmeAsama.YAYINLANDI;
            NotificationList.Add(new Notification("Yayınlandı", "Değerlendirme sonuçları başarıyla yayınlandı.", Notification.MasterMesajTip.Basari));
        }

        public override void YayindanKaldir()
        {
            if (EnumDegerlendirmeAsama == EnumDegerlendirmeAsama.AKTARILDI)
            {
                throw new Exception("Öğrenci işlerine aktarımı gerçekleştirilen değerlendirmeler değiştirilemez!");
            }
            if (EnumDegerlendirmeAsama == EnumDegerlendirmeAsama.SONHAL)
            {
                throw new Exception("Son hal verilen değerlendirmeler değiştirilemez!");
            }
            if (EnumDegerlendirmeAsama == EnumDegerlendirmeAsama.BOS)
            {
                throw new Exception("Yayından kaldırabilmek için önce yayınlanma yapılmalıdır!");
            }
            if (EnumDegerlendirmeAsama == EnumDegerlendirmeAsama.KAYITLI)
            {
                throw new Exception("Yayından kaldırabilmek için önce yayınlanma yapılmalıdır!");
            }
            List<int> OrtakGruplarInt = new List<int>();
            OrtakGruplarInt.Add(SeciliDers.DersGrupID.Value);
            dataBasariNot.YayindanKaldir(OrtakGruplarInt, ButunlemeMi);
            this.EnumDegerlendirmeAsama = EnumDegerlendirmeAsama.KAYITLI;
            NotificationList.Add(new Notification("Yayından Kaldırıldı", "Değerlendirme sonuçları yayından kaldırıldı!", Notification.MasterMesajTip.Uyari));
        }

        public override void SonHalKaldir()
        {
            if (EnumDegerlendirmeAsama == EnumDegerlendirmeAsama.AKTARILDI)
            {
                throw new Exception("Öğrenci işlerine aktarılan değerlendirmeler geri alınamaz!");
            }
            dataBasariNot.SonHalKaldir(DegerlendirmeKayit);
            this.EnumDegerlendirmeAsama = EnumDegerlendirmeAsama.YAYINLANDI;
        }

        protected override EnumDegerlendirmeAsama AsamaBul(List<DegerlendirmeDTO> kayitliDegerlendirmeler)
        {
            EnumDegerlendirmeAsama asama = EnumDegerlendirmeAsama.BOS;

            if (kayitliDegerlendirmeler == null || !kayitliDegerlendirmeler.Any())
            {
                asama = EnumDegerlendirmeAsama.BOS;
            }
            else
            {
                asama = EnumDegerlendirmeAsama.KAYITLI;
                if (dataBasariNot.AktarildiMi(DegerlendirmeKayit))
                {
                    asama = EnumDegerlendirmeAsama.AKTARILDI;
                }
                else if (kayitliDegerlendirmeler.Count != OrtakGruplarInt.Count)
                {
                    hataList.Add("Değerlendirme kaydı sayısı grup sayısı kadar olmalı!");
                    asama = EnumDegerlendirmeAsama.HATALI;
                }
                else
                {
                    int sonHalSayisi = kayitliDegerlendirmeler.Where(x => x.EnumSonHal == (int)EnumSonHal.EVET).Count();
                    if (sonHalSayisi == kayitliDegerlendirmeler.Count)
                    {
                        asama = EnumDegerlendirmeAsama.SONHAL;
                    }
                    else if (sonHalSayisi > 0 && sonHalSayisi < kayitliDegerlendirmeler.Count)
                    {
                        hataList.Add("Son hal sayısı değerlendirme kaydı sayısı kadar olmalı!");
                        asama = EnumDegerlendirmeAsama.HATALI;
                    }
                    else if (sonHalSayisi == 0 && dataBasariNot.TumBasariNotlarYayinlandiMi(OrtakGruplarInt, ButunlemeMi))
                    {
                        asama = EnumDegerlendirmeAsama.YAYINLANDI;
                    }
                }
            }
            return asama;
        }

        public override bool PaylarDuzgunMu(List<IPay> payList, bool butunlemeMi)
        {
            throw new NotImplementedException();
        }

        protected override bool TumNotlarGirildiMi()
        {
            throw new NotImplementedException();
        }

        protected override bool TumNotlarYayinlandiMi(List<IPay> payList, out List<Tuple<int, IPay>> yayinlanmayanPayList)
        {
            throw new NotImplementedException();
        }

        protected override bool DegerlendirmeyeUygunMu()
        {
            throw new NotImplementedException();
        }



        public override void Degerlendir(double? _ustDeger, double? _bagilAritmetikOrtalama = default(double?))
        {
            throw new NotImplementedException();
        }

        public override void Kaydet()
        {
            throw new NotImplementedException();
        }

        public override void Sifirla(bool hesaplansinMi = true)
        {
            throw new NotImplementedException();
        }

        protected override void PayYayinla(int ID, int payID, int yil, int donem)
        {
            throw new NotImplementedException();
        }
    }
}
