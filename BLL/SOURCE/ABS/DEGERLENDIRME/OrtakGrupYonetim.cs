﻿using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Bolum.Core.ABS.DEGERLENDIRME;
using Sabis.Core;
using Sabis.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME
{
    public class OrtakGrupYonetim
    {
        class OrtakGrupDto
        {
            public int BirimID { get; set; }
            public DtoBirimler Birim { get; set; }
            public int DersGrupID { get; set; }
            public int DersPlanAnaID { get; set; }
            public int DersPlanID { get; set; }
            public int EnumDersBirimTipi { get; set; }
            public DtoBirimler Bolum { get; set; }
            public DtoBirimler Fakulte { get; set; }
            public int EnumDil { get; set; }
            public int DersiVerenPersonelID { get; set; }
        }

        public static void Olustur(int dersPlanAnaID, int yil, int donem, string kullanici)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var dpAna = db.OGRDersPlanAna.Find(dersPlanAnaID);
                if (dpAna.EnumKokDersTipi != (int)EnumKokDersTipi.NORMAL)
                {
                    return;
                }

                bool varMi = db.ABSOrtakDegerlendirmeGrup.Any(x => !x.Silindi && 
                                !x.ABSOrtakDegerlendirme.Silindi && 
                                x.OGRDersGrup.OGRDersPlan.FKDersPlanAnaID == dersPlanAnaID && 
                                x.OGRDersGrup.OgretimYili == yil && 
                                x.OGRDersGrup.EnumDonem == donem);
                if (varMi)
                {
                    return;
                }

                return;

                var degerlendirecekPersonelID = Sabis.Bolum.Bll.SOURCE.EBS.DERS.DERSMAIN.GetirKoordinatorIDByDersPlanAnaID(dersPlanAnaID);

                var gruplar = db.OGRDersGrup
                    .Where(x => x.Acik && !x.Silindi && x.OGRDersPlan.FKDersPlanAnaID == dersPlanAnaID && x.OgretimYili == yil && x.EnumDonem == donem && x.EnumDersGrupDil.HasValue)
                    .Select(x => new OrtakGrupDto { DersGrupID = x.ID, BirimID = x.FKBirimID, EnumDersBirimTipi = x.OGRDersPlan.EnumDersBirimTipi, EnumDil = x.EnumDersGrupDil.Value, DersPlanAnaID = x.OGRDersPlan.FKDersPlanAnaID.Value, DersPlanID = x.FKDersPlanID.Value }).ToList();

                foreach (var item in gruplar)
                {
                    item.Birim = Sabis.Client.BirimIslem.Instance.getirBirimbyID(item.BirimID);
                    if (item.Birim.EnumBirimTuru == (int)EnumBirimTuru.Fakulte)
                    {
                        item.Fakulte = Sabis.Client.BirimIslem.Instance.getirBagliFakulte(item.BirimID);
                    }
                    else
                    {
                        item.Bolum = Sabis.Client.BirimIslem.Instance.getirBagliBolum(item.BirimID);
                        item.Fakulte = Sabis.Client.BirimIslem.Instance.getirBagliFakulte(item.BirimID);
                    }
                    var dgHocaList = db.OGRDersGrupHoca.Where(x => x.FKDersGrupID == item.DersGrupID && x.FKPersonelID.HasValue && x.Aktif).ToList();
                    if (dgHocaList.Any())
                    {
                        var dersiVeren = dgHocaList.FirstOrDefault(x => x.EnumDersHocaUnvan == (int)EnumDersHocaUnvan.OGRETIM_ELEMANI);
                        if (dersiVeren == null)
                        {
                            dersiVeren = dgHocaList.First();
                        }
                        item.DersiVerenPersonelID = dersiVeren.FKPersonelID.Value;
                    }
                }

                //İstisnalar
                gruplar = IstisnaAyir(gruplar, dersPlanAnaID, kullanici, yil, donem);

                var gruplu = gruplar.GroupBy(x => x.EnumDersBirimTipi);

                foreach (var dersBirimTipGrup in gruplu)
                {
                    EnumDersBirimTipi birimTipi = (EnumDersBirimTipi)dersBirimTipGrup.Key;

                    switch (birimTipi)
                    {
                        case EnumDersBirimTipi.BOS:
                            break;
                        case EnumDersBirimTipi.BOLUM:
                            var grupBolumListe = dersBirimTipGrup.GroupBy(x => new { x.Bolum.ID, x.EnumDil });
                            foreach (var grup in grupBolumListe)
                            {
                                Kaydet(grup.Select(x => x.DersGrupID).ToList(), degerlendirecekPersonelID, dersPlanAnaID, kullanici, yil, donem);
                            }
                            break;
                        case EnumDersBirimTipi.FAKULTE:
                            var grupFakulteListe = dersBirimTipGrup.GroupBy(x => new { x.Fakulte.ID, x.EnumDil });
                            foreach (var grup in grupFakulteListe)
                            {
                                Kaydet(grup.Select(x => x.DersGrupID).ToList(), degerlendirecekPersonelID, dersPlanAnaID, kullanici, yil, donem);
                            }
                            break;
                        case EnumDersBirimTipi.UNIVERSITE:
                            var grupUnivListe = dersBirimTipGrup.GroupBy(x => x.EnumDil);
                            foreach (var grup in grupUnivListe)
                            {
                                Kaydet(grup.Select(x => x.DersGrupID).ToList(), degerlendirecekPersonelID, dersPlanAnaID, kullanici, yil, donem);
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        private static List<OrtakGrupDto> IstisnaAyir(List<OrtakGrupDto> gruplar, int dersPlanAnaID, string kullanici, int yil, int donem)
        {
            List<OrtakGrupDto> result = new List<DEGERLENDIRME.OrtakGrupYonetim.OrtakGrupDto>();
            List<OrtakGrupDto> ozelGrupList = new List<DEGERLENDIRME.OrtakGrupYonetim.OrtakGrupDto>();

            foreach (var item in gruplar)
            {
                //Devlet konservatuarı
                if (item.Fakulte.ID == 412 && item.EnumDersBirimTipi != (int)EnumDersBirimTipi.UNIVERSITE)
                {
                    ozelGrupList.Add(item);
                }
                //Enstitüler
                else if (item.Fakulte.EnumBirimTuru == (int)EnumBirimTuru.Enstitu)
                {
                    //ayrilacakList.Add(item);
                    Kaydet(new List<int>() { item.DersGrupID }, item.DersiVerenPersonelID, dersPlanAnaID, kullanici, yil, donem);
                }
                //Yabancı diller
                else if (item.Fakulte.ID == 1295)
                {
                    //ayrilacakList.Add(item);
                    Kaydet(new List<int>() { item.DersGrupID }, item.DersiVerenPersonelID, dersPlanAnaID, kullanici, yil, donem);
                }
                else
                {
                    result.Add(item);
                }
            }

            if (ozelGrupList.Any())
            {
                var grupluOzel = ozelGrupList.GroupBy(x => new { x.DersPlanAnaID, x.DersiVerenPersonelID, x.EnumDil });
                foreach (var item in grupluOzel)
                {
                    Kaydet(item.Select(x => x.DersGrupID).ToList(), item.Key.DersiVerenPersonelID, item.Key.DersPlanAnaID, kullanici, yil, donem);
                }
            }

            return result;
        }

        private static void Kaydet(List<int> grupIDList, int degerlendirecekPersonelID, int dersPlanAnaID, string kullanici, int yil, int donem)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                ABSOrtakDegerlendirme ortakDeg = new ABSOrtakDegerlendirme();
                ortakDeg.Kullanici = kullanici;
                ortakDeg.FKDersPlanAnaID = dersPlanAnaID;
                ortakDeg.FKPersonelID = degerlendirecekPersonelID;
                ortakDeg.Silindi = false;
                ortakDeg.Yil = yil;
                ortakDeg.Donem = donem;
                ortakDeg.TarihKayit = DateTime.Now;
                db.ABSOrtakDegerlendirme.Add(ortakDeg);
                db.SaveChanges();
                foreach (var item in grupIDList)
                {
                    ABSOrtakDegerlendirmeGrup grupDeg = new ABSOrtakDegerlendirmeGrup();
                    grupDeg.FKDersGrupID = item;
                    grupDeg.FKOrtakDegerlendirmeID = ortakDeg.ID;
                    grupDeg.Kullanici = kullanici;
                    grupDeg.Silindi = false;
                    grupDeg.TarihKayit = DateTime.Now;
                    db.ABSOrtakDegerlendirmeGrup.Add(grupDeg);
                }
                db.SaveChanges();
            }
        }
    }
}
