﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AutoMapper;
using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Bolum.Core.ABS.DEGERLENDIRME;
using Sabis.Enum;

namespace Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME
{
    public class Data
    {
        private static bool AutoMapperConfigured;
        public Data()
        {
            if (!AutoMapperConfigured)
            {
                var config = new MapperConfiguration(cfg => cfg.CreateMap<Sabis.Bolum.Bll.DATA.UysModel.ABSDegerlendirme, DegerlendirmeDTO>());
                var mapper = config.CreateMapper();
                //Mapper.CreateMap<ABSDegerlendirme, DegerlendirmeDTO>(); //yeni sürümde çalışmıyor
                AutoMapperConfigured = true;
            }
        }

        private Sabis.Bolum.Bll.DATA.UysModel.UYSv2Entities context = new Sabis.Bolum.Bll.DATA.UysModel.UYSv2Entities();

        public List<Core.ABS.DEGERLENDIRME.Grup> OrtakDegerlendirmeGrupBul(int grupID, bool butunlemeMi)
        {
            List<Core.ABS.DEGERLENDIRME.Grup> result = new List<Core.ABS.DEGERLENDIRME.Grup>();


            var grupData = context.OGRDersGrup.First(x => x.ID == grupID);

            Core.ABS.DEGERLENDIRME.Grup grup = new Core.ABS.DEGERLENDIRME.Grup();
            grup.Ad = grupData.GrupAd;
            grup.DersGrupID = grupData.ID;
            grup.DersPlanAnaID = grupData.OGRDersPlan.FKDersPlanAnaID.Value;
            grup.EnumOgretimTur = (EnumOgretimTur)grupData.EnumOgretimTur;
            grup.Bolum = Sabis.Client.BirimIslem.Instance.getirBagliBolum(grupData.FKBirimID, true);
            grup.Fakulte = Sabis.Client.BirimIslem.Instance.getirBagliFakulte(grupData.FKBirimID, true);
            if (butunlemeMi)
            {
                grup.OgrenciSayisi = context.OGROgrenciYazilma.Where(y => y.Iptal == false && (y.ButunlemeMi.HasValue && y.ButunlemeMi.Value == true) && y.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL && y.FKDersGrupID == grupData.ID).Count();
            }
            else
            {
                grup.OgrenciSayisi = context.OGROgrenciYazilma.Where(y => y.Iptal == false && y.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL && y.FKDersGrupID == grupData.ID).Count();
            }
            grup.PersonelList = new List<PersonelDTO>();
            foreach (var hoca in grupData.OGRDersGrupHoca)
            {
                grup.PersonelList.Add(new PersonelDTO() { Ad = hoca.PersonelBilgisi.Kisi.Ad, Soyad = hoca.PersonelBilgisi.Soyad, UnvanAd = hoca.PersonelBilgisi.UnvanAd, ID = hoca.FKPersonelID.Value });
            }
            result.Add(grup);
            return result;
                
            
            
        }

        public PersonelDTO GetirDegerlendirecekPersonel(int grupID)
        {
            PersonelDTO result = null;
            var degGrup = context.ABSOrtakDegerlendirmeGrup.FirstOrDefault(x => x.FKDersGrupID == grupID && !x.Silindi && !x.ABSOrtakDegerlendirme.Silindi);
            if (degGrup == null)
            {
                throw new Exception("Ortak değerlendirme grubu bulunamadı!");
            }

            result = new PersonelDTO()
            {
                Ad = degGrup.ABSOrtakDegerlendirme.PersonelBilgisi.Kisi.Ad,
                Soyad = degGrup.ABSOrtakDegerlendirme.PersonelBilgisi.Kisi.Soyad,
                UnvanAd = degGrup.ABSOrtakDegerlendirme.PersonelBilgisi.UnvanAd,
                ID = degGrup.ABSOrtakDegerlendirme.FKPersonelID
            };

            return result;
        }

        /// <summary>
        /// Ortak değerlendirme yapılacak grupları bulur
        /// </summary>
        /// <param name="dersPlanAna"></param>
        /// <param name="personelID"></param>
        /// <param name="ogretimTuru"></param>
        /// <returns></returns>
        [Obsolete]
        public List<Core.ABS.DEGERLENDIRME.Grup> OrtakDegerlendirmeGrupBulEski(int grupID, int personelID, int yil, int donem, bool butunlemeMi)
        {

            //TODO
            //donem = 3;
            List<Sabis.Bolum.Bll.DATA.UysModel.OGRDersGrup> resultTMP = null;
            Sabis.Bolum.Bll.DATA.UysModel.OGRDersGrup seciliGrup = context.OGRDersGrup.First(x => x.ID == grupID);
            EnumOgretimTur ogretimTuru = (EnumOgretimTur)seciliGrup.EnumOgretimTur;

            //Devlet konservatuarı
            if (seciliGrup.OGRDersPlan.FKFakulteBirimID.Value == 412)
            {
                resultTMP = (from x in context.OGRDersGrupHoca
                             where x.OGRDersGrup.FKDersPlanAnaID == seciliGrup.FKDersPlanAnaID && x.OGRDersGrup.OGRDersPlan.FKFakulteBirimID.Value == 412 && x.FKPersonelID == personelID && x.OGRDersGrup.EnumDonem == donem && x.OGRDersGrup.OgretimYili == yil && x.OGRDersGrup.GrupLimit != 999
                             && x.OGRDersGrup.EnumDersGrupDil == seciliGrup.EnumDersGrupDil
                             select x.OGRDersGrup).ToList();
            }
            //Enstitüler
            else if (Sabis.Client.BirimIslem.Instance.BirimDict[seciliGrup.OGRDersPlan.FKFakulteBirimID.Value].EnumBirimTuru == (int)EnumBirimTuru.Enstitu)
            {
                resultTMP = (from x in context.OGRDersGrupHoca
                             where x.FKDersGrupID == seciliGrup.ID && x.OGRDersGrup.OGRDersPlan.FKFakulteBirimID.Value == seciliGrup.OGRDersPlan.FKFakulteBirimID.Value && x.FKPersonelID == personelID && x.OGRDersGrup.EnumDonem == donem && x.OGRDersGrup.OgretimYili == yil && x.OGRDersGrup.GrupLimit != 999
                             && x.OGRDersGrup.EnumDersGrupDil == seciliGrup.EnumDersGrupDil
                             select x.OGRDersGrup).ToList();
            }
            //Yabancı diller
            else if (seciliGrup.OGRDersPlan.FKFakulteBirimID.Value == 1295)
            {
                resultTMP = (from x in context.OGRDersGrupHoca
                             where x.FKDersGrupID == seciliGrup.ID && x.OGRDersGrup.OGRDersPlan.FKFakulteBirimID.Value == seciliGrup.OGRDersPlan.FKFakulteBirimID.Value && x.FKPersonelID == personelID && x.OGRDersGrup.EnumDonem == donem && x.OGRDersGrup.OgretimYili == yil && x.OGRDersGrup.GrupLimit != 999
                             && x.OGRDersGrup.EnumDersGrupDil == seciliGrup.EnumDersGrupDil
                             select x.OGRDersGrup).ToList();
            }
            else
            {
                if (seciliGrup.OGRDersPlan.EnumDersPlanZorunlu == (int)EnumDersPlanZorunlu.BOLUM_SECMELI || seciliGrup.OGRDersPlan.EnumDersPlanZorunlu == (int)EnumDersPlanZorunlu.ZORUNLU)
                {
                    if ((int)ogretimTuru < 3)
                    {
                        var birim = Sabis.Client.BirimIslem.Instance.BirimDict.First(x => x.Key == seciliGrup.OGRDersPlan.FKBolumPrBirimID.Value).Value;
                        List<int> digerOgretimTurleri = new List<int>() { birim.ID };
                        var digerListe = Sabis.Client.BirimIslem.Instance.getirProgramDigerOgretimTuruList(birim.ID);
                        if (digerListe != null)
                        {
                            digerOgretimTurleri = digerListe.Select(x => x.ID).ToList();
                        }

                        resultTMP = (from x in context.OGRDersGrupHoca
                                     where x.OGRDersGrup.FKDersPlanAnaID == seciliGrup.FKDersPlanAnaID && x.OGRDersGrup.OGRDersPlan.FKBolumPrBirimID.HasValue && digerOgretimTurleri.Contains(x.OGRDersGrup.OGRDersPlan.FKBolumPrBirimID.Value) && x.FKPersonelID == personelID && x.OGRDersGrup.EnumOgretimTur < 3 && x.OGRDersGrup.EnumDonem == donem && x.OGRDersGrup.OgretimYili == yil && x.OGRDersGrup.GrupLimit != 999
                                     && x.OGRDersGrup.EnumDersGrupDil == seciliGrup.EnumDersGrupDil
                                     select x.OGRDersGrup).ToList();
                    }
                    else//Uzaktan eğitim ise
                    {
                        resultTMP = (from x in context.OGRDersGrupHoca
                                     where x.OGRDersGrup.FKDersPlanAnaID == seciliGrup.FKDersPlanAnaID && x.OGRDersGrup.OGRDersPlan.FKBolumPrBirimID == seciliGrup.OGRDersPlan.FKBolumPrBirimID && x.FKPersonelID == personelID && x.OGRDersGrup.EnumOgretimTur == (int)ogretimTuru && x.OGRDersGrup.EnumDonem == donem && x.OGRDersGrup.OgretimYili == yil && x.OGRDersGrup.GrupLimit != 999
                                     && x.OGRDersGrup.EnumDersGrupDil == seciliGrup.EnumDersGrupDil
                                     select x.OGRDersGrup).ToList();
                    }
                }
                else if (seciliGrup.OGRDersPlan.EnumDersPlanZorunlu == (int)EnumDersPlanZorunlu.FAKULTE_SOSYAL_SECMELI || seciliGrup.OGRDersPlan.EnumDersPlanZorunlu == (int)EnumDersPlanZorunlu.UNIVERSITE_ORTAK_DERS)
                {
                    resultTMP = (from x in context.OGRDersGrupHoca
                                 where x.OGRDersGrup.FKDersPlanAnaID == seciliGrup.FKDersPlanAnaID && x.FKPersonelID == personelID
                                 //&& x.OGRDersGrup.OGRDersPlan.EnumOgretimTur == (int)ogretimTuru 
                                 && x.OGRDersGrup.EnumDonem == donem && x.OGRDersGrup.OgretimYili == yil && x.OGRDersGrup.GrupLimit != 999
                                 && x.OGRDersGrup.EnumDersGrupDil == seciliGrup.EnumDersGrupDil
                                 select x.OGRDersGrup).ToList();
                }
                else
                {
                    resultTMP = (from x in context.OGRDersGrupHoca
                                 where x.OGRDersGrup.ID == seciliGrup.ID && x.FKPersonelID == personelID
                                 //&& x.OGRDersGrup.OGRDersPlan.EnumOgretimTur == (int)ogretimTuru 
                                 && x.OGRDersGrup.EnumDonem == donem
                                 && x.OGRDersGrup.OgretimYili == yil && x.OGRDersGrup.GrupLimit != 999
                                 && x.OGRDersGrup.EnumDersGrupDil == seciliGrup.EnumDersGrupDil
                                 select x.OGRDersGrup).ToList();
                }
            }
            Sabis.Core.DtoBirimler bolumTMP = null;
            if (seciliGrup.OGRDersPlan.EnumDersPlanZorunlu == (int)EnumDersPlanZorunlu.UNIVERSITE_ORTAK_DERS || seciliGrup.OGRDersPlan.EnumDersPlanZorunlu == (int)EnumDersPlanZorunlu.FAKULTE_SOSYAL_SECMELI)
            {
                bolumTMP = new Sabis.Core.DtoBirimler() { ID = 0, BirimAdi = "" };
            }
            List<Core.ABS.DEGERLENDIRME.Grup> result = new List<Core.ABS.DEGERLENDIRME.Grup>();
            foreach (var item in resultTMP)
            {
                Sabis.Core.DtoBirimler bolumBirim = bolumTMP;
                if (item.OGRDersPlan.FKBolumPrBirimID.HasValue)
                {
                    var bBirim = Sabis.Client.BirimIslem.Instance.BirimDict.First(x => x.Key == item.OGRDersPlan.FKBolumPrBirimID.Value).Value;
                    bolumBirim = bBirim;
                }

                var fBirim = Sabis.Client.BirimIslem.Instance.BirimDict.First(x => x.Key == item.OGRDersPlan.FKFakulteBirimID.Value).Value;
                Core.ABS.DEGERLENDIRME.Grup grup = new Core.ABS.DEGERLENDIRME.Grup
                {
                    DersGrupID = item.ID,
                    DersPlanAnaID = item.OGRDersPlan.FKDersPlanAnaID.Value,
                    Ad = item.GrupAd,
                    Bolum = bolumBirim,
                    Fakulte = fBirim,
                    EnumOgretimTur = (EnumOgretimTur)item.EnumOgretimTur
                };
                if (butunlemeMi)
                {
                    grup.OgrenciSayisi = context.OGROgrenciYazilma.Where(y => y.Iptal == false && (y.ButunlemeMi.HasValue && y.ButunlemeMi.Value == true) && y.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL && y.FKDersGrupID == item.ID).Count();
                }
                else
                {
                    grup.OgrenciSayisi = context.OGROgrenciYazilma.Where(y => y.Iptal == false && y.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL && y.FKDersGrupID == item.ID).Count();
                }
                result.Add(grup);
            }
            return result;
        }

        public List<Core.ABS.DEGERLENDIRME.Grup> OrtakDegerlendirmeGrupBulADAMYO(int grupID, int personelID, int yil, int donem, bool butunlemeMi)
        {
            Sabis.Bolum.Bll.DATA.UysModel.OGRDersGrup seciliGrup = context.OGRDersGrup.First(x => x.ID == grupID);
            EnumOgretimTur ogretimTuru = (EnumOgretimTur)seciliGrup.OGRDersPlan.EnumOgretimTur;
            var tumBolumler = Sabis.Client.BirimIslem.Instance.getirProgramDigerOgretimTuruList(seciliGrup.OGRDersPlan.FKBolumPrBirimID.Value).Select(x => x.ID);
            var resultTMP = (from x in context.OGRDersGrupHoca
                             where x.OGRDersGrup.OGRDersPlan.FKDersPlanAnaID == seciliGrup.FKDersPlanAnaID && x.OGRDersGrup.OGRDersPlan.FKBolumPrBirimID.HasValue && tumBolumler.Contains(x.OGRDersGrup.OGRDersPlan.FKBolumPrBirimID.Value) && x.OGRDersGrup.EnumDonem == donem && x.OGRDersGrup.OgretimYili == yil && x.OGRDersGrup.GrupLimit != 999
                             select x.OGRDersGrup).AsQueryable();

            var resultTMPList = resultTMP.ToList();

            Sabis.Core.DtoBirimler bolumTMP = null;
            if (seciliGrup.OGRDersPlan.EnumDersPlanZorunlu == (int)EnumDersPlanZorunlu.UNIVERSITE_ORTAK_DERS || seciliGrup.OGRDersPlan.EnumDersPlanZorunlu == (int)EnumDersPlanZorunlu.FAKULTE_SOSYAL_SECMELI)
            {
                bolumTMP = new Sabis.Core.DtoBirimler() { ID = 0, BirimAdi = "" };
            }
            List<Core.ABS.DEGERLENDIRME.Grup> result = new List<Core.ABS.DEGERLENDIRME.Grup>();
            foreach (var item in resultTMPList)
            {

                Sabis.Core.DtoBirimler bolumBirim = bolumTMP;
                if (item.OGRDersPlan.FKBolumPrBirimID.HasValue)
                {
                    bolumBirim = Sabis.Client.BirimIslem.Instance.BirimDict.First(x => x.Key == item.OGRDersPlan.FKBolumPrBirimID.Value).Value;
                }

                var fBirim = Sabis.Client.BirimIslem.Instance.BirimDict.First(x => x.Key == item.OGRDersPlan.FKFakulteBirimID.Value).Value;
                Core.ABS.DEGERLENDIRME.Grup grup = new Core.ABS.DEGERLENDIRME.Grup
                {
                    DersGrupID = item.ID,
                    Ad = item.GrupAd,
                    Bolum = bolumBirim,
                    Fakulte = fBirim,
                    EnumOgretimTur = (EnumOgretimTur)item.OGRDersPlan.EnumOgretimTur
                };
                if (butunlemeMi)
                {
                    grup.OgrenciSayisi = item.OGROgrenciYazilma.Where(y => y.Iptal == false && (y.ButunlemeMi.HasValue && y.ButunlemeMi.Value == true) && y.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL).Count();
                }
                else
                {
                    grup.OgrenciSayisi = item.OGROgrenciYazilma.Where(y => y.Iptal == false && y.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL).Count();
                }
                result.Add(grup);
            }
            return result;
        }

        public List<OgrenciPayveNotBilgileriDTO> getirTumNotlarByGrupID(List<int> grupIDList, List<GPay> payList, bool butunlemeMi, bool ogrenciDurumaBak, int yil, int donem)
        {
            List<OgrenciPayveNotBilgileriDTO> result = new List<OgrenciPayveNotBilgileriDTO>();

            var tumNotList = context.ABSOgrenciNot.Where(x => x.Silindi == false && x.Yil == yil && grupIDList.Contains(x.FKDersGrupID.Value)).Select(x => new OgrenciNotDataDTO { ID = x.ID, FKOgrenciID = x.FKOgrenciID, FKAbsPaylarID = x.FKAbsPaylarID, NotDeger = x.NotDeger, FKDersGrupID = x.FKDersGrupID, Numara = x.OGRKimlik.Numara, Ad = x.OGRKimlik.Kisi.Ad, Soyad = x.OGRKimlik.Kisi.Soyad, EnumOgrenciDurum = x.OGRKimlik.EnumOgrDurum, DersPlanAnaID = x.OGRDersGrup.OGRDersPlan.FKDersPlanAnaID.Value, DersPlanID = x.OGRDersGrup.FKDersPlanID.Value });

            var tumNotListGroup = tumNotList.GroupBy(x => x.FKOgrenciID).ToDictionary(x => x.Key, y => y.ToList());

            List<DegerlendirmeYazilmaSQLDto> ogrenciListe = new List<DegerlendirmeYazilmaSQLDto>();
            foreach (var grupID in grupIDList)
            {
                var rawSQL = context.OGROgrenciYazilma.Where(x => x.Yil == yil && x.FKDersGrupID.HasValue && x.FKDersGrupID.Value == grupID && x.Iptal == false).AsQueryable();
                if (butunlemeMi == true)
                {
                    rawSQL = rawSQL.Where(x => x.ButunlemeMi == butunlemeMi);
                }

                //var yazilmaList = rawSQL.Select(x=> new { FKOgrenciID = x.FKOgrenciID.Value,  }) //rawSQL.ToString();

                //if (ogrenciDurumaBak)
                //{
                //    rawSQL = rawSQL.Where(x => x.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL);
                //}
                var sorgu = from yazilma in rawSQL
                            select new DegerlendirmeYazilmaSQLDto
                            {
                                Numara = yazilma.OGRKimlik.Numara,
                                AdSoyad = yazilma.OGRKimlik.Kisi.Ad + " " + yazilma.OGRKimlik.Kisi.Soyad,
                                EnumOgrenciDurum = yazilma.OGRKimlik.EnumOgrDurum,
                                OgrenciID = yazilma.FKOgrenciID.Value,
                                DersPlanAnaID = yazilma.OGRDersGrup.OGRDersPlan.FKDersPlanAnaID.Value,
                                DersGrupID = yazilma.FKDersGrupID,
                                DersPlanID = yazilma.OGRDersGrup.FKDersPlanID,
                                //Notlar = yazilma.OGRDersGrup.ABSOgrenciNot.Where(x => x.FKOgrenciID == yazilma.FKOgrenciID).OrderBy(d => d.ID).Select(x => new { ID = x.ID, FKOgrenciID = x.FKOgrenciID, FKAbsPaylarID = x.FKAbsPaylarID, NotDeger = x.NotDeger }),
                                YazilmaID = yazilma.ID,
                                ButunlemeMi = yazilma.ButunlemeMi
                            };
#if DEBUG
                string sql = sorgu.ToString();
#endif
                ogrenciListe.AddRange(sorgu.ToList());
            }

            foreach (var ogr in ogrenciListe)
            {
                if (ogrenciDurumaBak && ogr.EnumOgrenciDurum != (int)EnumOgrDurum.NORMAL)
                {
                    continue;
                }
                OgrenciPayveNotBilgileriDTO ogPayNotBilgileri = new OgrenciPayveNotBilgileriDTO();
                ogPayNotBilgileri.listPaylarVeNotlar = new List<PaylarVeNotlarDTO>();
                ogPayNotBilgileri.AdSoyad = ogr.AdSoyad;
                ogPayNotBilgileri.DersGrupID = ogr.DersGrupID;
                ogPayNotBilgileri.OgrenciID = Convert.ToInt32(ogr.OgrenciID);
                ogPayNotBilgileri.DersPlanAnaID = Convert.ToInt32(ogr.DersPlanAnaID);
                ogPayNotBilgileri.Numara = ogr.Numara;
                ogPayNotBilgileri.DersPlanID = ogr.DersPlanID;
                if (ogr.ButunlemeMi.HasValue && ogr.ButunlemeMi.Value)
                {
                    ogPayNotBilgileri.ButunlemeMi = true;
                }
                else
                {
                    ogPayNotBilgileri.ButunlemeMi = false;
                }

                int altSayacToplam = 0;

                ogPayNotBilgileri.MutlakNot = 0;
                List<OgrenciNotDataDTO> ogrNotList;
                tumNotListGroup.TryGetValue(ogr.OgrenciID, out ogrNotList); //.FirstOrDefault(x => x.Key == ogr.OgrenciID);
                if (ogrNotList != null)
                {
                    foreach (var pay in payList)
                    {
                        altSayacToplam++;
                        PaylarVeNotlarDTO pvn = new PaylarVeNotlarDTO();
                        pvn.PayID = pay.PayID;
                        pvn.EnumCalismaTip = pay.EnumCalismaTip;
                        pvn.Yuzde = pay.Yuzde;
                        var notu = ogrNotList.Where(x => x.FKAbsPaylarID == pay.PayID).OrderByDescending(x => x.ID).FirstOrDefault();
                        if (notu != null)
                        {
                            ogPayNotBilgileri.MutlakNot += Convert.ToDouble(notu.NotDeger * pay.Yuzde) / 100;
                            pvn.NotDeger = notu.NotDeger;
                            pvn.Notu = notu.NotDeger.ToString();
                            pvn.OgrenciNotID = notu.ID;
                        }
                        ogPayNotBilgileri.listPaylarVeNotlar.Add(pvn);
                    }
                }
                result.Add(ogPayNotBilgileri);
            }
            return result;
        }

        public List<GPay> PaylariGetir(int grupID)
        {
            Sabis.Bolum.Bll.DATA.UysModel.OGRDersGrup tmpGrup = context.OGRDersGrup.First(x => x.ID == grupID);
            int dersPlanID = tmpGrup.FKDersPlanID.Value;
            int? dersplanAnaID = tmpGrup.OGRDersPlan.FKDersPlanAnaID.Value;
            int yil = tmpGrup.OgretimYili.Value;
            int donem = tmpGrup.EnumDonem.Value;
            string yilStr = yil.ToString();
            var sorgu = (from pay in context.ABSPaylar
                         where pay.Sira.HasValue && pay.Yil.HasValue && pay.FKDersPlanAnaID == dersplanAnaID && pay.FKDersPlanID == dersPlanID &&
                         pay.Yil == yil && (pay.OGRDersPlanAna.EnumDersSureTip == (int)EnumDersSureTip.YILLIK || (pay.OGRDersPlanAna.EnumDersSureTip == (int)EnumDersSureTip.DONEMLIK && pay.EnumDonem == donem))
                         orderby pay.EnumCalismaTip ascending
                         select new
                         {
                             PayID = pay.ID,
                             EnumCalismaTip = pay.EnumCalismaTip,
                             Sira = pay.Sira.Value,
                             Oran = pay.Oran,
                             DersPlanID = pay.FKDersPlanID,
                             DersPlanAnaID = pay.FKDersPlanAnaID.Value,
                             Yil = pay.Yil.Value,
                             EnumDonem = pay.EnumDonem,
                             Pay = pay.Pay,
                             Payda = pay.Payda
                         }).AsQueryable();

            if (!sorgu.Any())
            {
                sorgu = (from pay in context.ABSPaylar
                         where pay.Sira.HasValue && pay.Yil.HasValue && pay.FKDersPlanAnaID == dersplanAnaID && pay.FKDersPlanID == null &&
                         pay.Yil == yil && (pay.OGRDersPlanAna.EnumDersSureTip == (int)EnumDersSureTip.YILLIK || (pay.OGRDersPlanAna.EnumDersSureTip == (int)EnumDersSureTip.DONEMLIK && pay.EnumDonem == donem))
                         orderby pay.EnumCalismaTip ascending
                         select new
                         {
                             PayID = pay.ID,
                             EnumCalismaTip = pay.EnumCalismaTip,
                             Sira = pay.Sira.Value,
                             Oran = pay.Oran,
                             DersPlanID = pay.FKDersPlanID,
                             DersPlanAnaID = pay.FKDersPlanAnaID.Value,
                             Yil = pay.Yil.Value,
                             EnumDonem = pay.EnumDonem,
                             Pay = pay.Pay,
                             Payda = pay.Payda
                         });
            }

            List<GPay> result = new List<GPay>();
            foreach (var item in sorgu)
            {
                GPay pay = new GPay();
                pay.DersPlanAnaID = item.DersPlanAnaID;
                pay.DersPlanID = item.DersPlanID;
                pay.EnumCalismaTip = (EnumCalismaTip)item.EnumCalismaTip;
                pay.EnumDonem = item.EnumDonem;
                if (item.Oran.HasValue)
                {
                    pay.Oran = item.Oran;
                    pay.Yuzde = item.Oran.Value;
                }
                else
                {
                    pay.Pay = item.Pay;
                    pay.Payda = item.Payda;
                    // 1 / 4 = 0,25 ancak oranlarda bu değer 25 olarak giriliyor bu nedenle *100
                    pay.Yuzde = ((double)item.Pay.Value / (double)item.Payda.Value) * 100;
                }
                pay.PayID = item.PayID;
                pay.Sira = item.Sira;
                pay.Yil = item.Yil;
                result.Add(pay);
            }

            return result;
        }

        /// <summary>
        /// Bir dizi grup için yayınlanan payların idlerini döndürür
        /// </summary>
        /// <param name="grupList">Sorgulanacak Gruplar Listesi</param>
        /// <returns>GrupID, PayID</returns>
        public List<Tuple<int, int>> YayinlananPaylariGetir(List<int> grupList)
        {
            List<Tuple<int, int>> result = new List<Tuple<int, int>>();
            foreach (var item in context.ABSYayinlananPayNotlar.Where(x => x.Yayinlandi && grupList.Contains(x.FKDersGrupID.Value)))
            {
                result.Add(new Tuple<int, int>(item.FKDersGrupID.Value, item.FKABSPayID));
            }
            return result;
        }

        public List<DegerlendirmeDTO> getirDegerlendirme(List<int> dersGrupIDList, bool butunlemeMi)
        {
            List<DegerlendirmeDTO> result;
            context.Set<Sabis.Bolum.Bll.DATA.UysModel.ABSDegerlendirme>().AsNoTracking();
            var degerlendirmeKayit = context.ABSDegerlendirme.Where(x => dersGrupIDList.Contains(x.FKDersGrupID.Value) && x.ButunlemeMi == butunlemeMi && x.SilindiMi == false).OrderByDescending(x => x.ID).ToList();

            //var config = new MapperConfiguration(cfg => {});
            //var mapper = config.CreateMapper();

            var config = new MapperConfiguration(cfg => cfg.CreateMap<Sabis.Bolum.Bll.DATA.UysModel.ABSDegerlendirme, DegerlendirmeDTO>());
            var mapper = config.CreateMapper();

            result = mapper.Map<List<DegerlendirmeDTO>>(degerlendirmeKayit);

            //result = Mapper.Map<List<DegerlendirmeDTO>>(degerlendirmeKayit);
            return result;
        }

        public bool TumBasariNotlarYayinlandiMi(List<int> dersGrupIDList, bool butunlemeMi)
        {
            return context.ABSYayinlananBasariNotlar.Any(x => x.FKDersGrupID.HasValue && dersGrupIDList.Contains(x.FKDersGrupID.Value) && x.Yayinlandi && x.ButunlemeMi == butunlemeMi && x.ABSDegerlendirme.SilindiMi == false);
        }

        public bool AktarildiMi(List<DegerlendirmeDTO> degerlendirmeList)
        {
            bool result = false;
            if (degerlendirmeList.Any())
            {
                var guid = degerlendirmeList.First().OrtakDegerlendirmeGUID;
                result = context.OGRBarkodOkuma.Any(x => x.ABSDegerlendirme.OrtakDegerlendirmeGUID == guid);
            }
            return result;
        }

        public bool AktarildiMiTumu(List<int> grupIDList)
        {
            bool result = true;
            foreach (int item in grupIDList)
            {
                result = context.OGRBarkodOkuma.Any(x => x.ABSDegerlendirme.SilindiMi == false && x.ABSDegerlendirme.FKDersGrupID == item);
                if (result == false)
                {
                    break;
                }
            }
            return result;
        }

        public List<DegerlendirmeDTO> KaydetDegerlendirme(List<DegerlendirmeDTO> DegerlendirmeKayit)
        {
            foreach (var item in DegerlendirmeKayit)
            {
                bool yeniMi = false;
                Sabis.Bolum.Bll.DATA.UysModel.ABSDegerlendirme kayit = context.ABSDegerlendirme.FirstOrDefault(x => x.FKDersGrupID == item.FKDersGrupID && x.FKDersPlanAnaID == x.FKDersPlanAnaID && x.ButunlemeMi == item.ButunlemeMi && x.SilindiMi == false);
                if (kayit == null)
                {
                    yeniMi = true;
                    kayit = new Sabis.Bolum.Bll.DATA.UysModel.ABSDegerlendirme();
                }
                kayit.ButunlemeMi = item.ButunlemeMi;
                kayit.BagilOrtalama = item.BagilOrtalama;
                kayit.BarkodNo = item.BarkodNo;
                kayit.EnBuyukNot = item.EnBuyukNot;
                kayit.EnKucukNot = item.EnKucukNot;
                kayit.EnumDonem = item.EnumDonem;
                kayit.EnumSonHal = item.EnumSonHal;
                kayit.FKDersGrupID = item.FKDersGrupID;
                kayit.FKDersPlanAnaID = item.FKDersPlanAnaID;
                kayit.FKPersonelID = item.FKPersonelID;
                kayit.Kullanici = item.Kullanici;
                kayit.Makina = item.Makina;
                kayit.OrtakDegerlendirmeGUID = item.OrtakDegerlendirmeGUID;
                kayit.Ortalama = item.Ortalama;
                kayit.StandartSapma = item.StandartSapma;
                kayit.Tarih = item.Tarih;
                kayit.UstDeger = item.UstDeger;
                kayit.Yil = item.Yil;
                kayit.Zaman = item.Zaman;
                kayit.SilindiMi = false;
                if (yeniMi)
                {
                    context.ABSDegerlendirme.Add(kayit);
                }
                context.SaveChanges();
                item.ID = kayit.ID;
            }
            return DegerlendirmeKayit;
        }

        public void KaydetBasariNot(List<OgrNot> list, List<int> grupIDList, int degerlendirmeID, string KullaniciAdi, string IPAdresi, int yil, int donem, bool butunlemeMi)
        {
            //Dictionary<int, ABSBasariNot> ogrBasariNotDict = context.ABSBasariNot.Where(x => grupIDList.Contains(x.FKDersGrupID.Value) && x.ButunlemeMi == butunlemeMi && x.SilindiMi == false).ToDictionary(k => k.FKOgrenciID.Value, v => v);
            //if (ogrBasariNotDict.Count == 0)
            //{
            //    ogrBasariNotDict = new Dictionary<int, ABSBasariNot>();
            //}
            int sayac = 0;
            foreach (OgrNot ogrNot in list)
            {
#if DEBUG
                //if (ogrNot.OgrID == 221644)
                //{

                //}
#endif
                bool yeniMi = false;
                Sabis.Bolum.Bll.DATA.UysModel.ABSBasariNot basariNot = context.ABSBasariNot.FirstOrDefault(x => x.FKDersGrupID == ogrNot.GrupID && x.FKOgrenciID == ogrNot.OgrID && x.ButunlemeMi == butunlemeMi && x.SilindiMi == false);
                if (basariNot == null)
                {
                    yeniMi = true;
                    basariNot = new Sabis.Bolum.Bll.DATA.UysModel.ABSBasariNot();
                }
                //if (!ogrBasariNotDict.Any(x => x.Key == ogrNot.OgrID))
                //{
                //    yeniMi = true;
                //    basariNot = new ABSBasariNot();
                //}
                //else
                //{
                //    basariNot = ogrBasariNotDict.First(x => x.Key == ogrNot.OgrID).Value;
                //}

                basariNot.Bagil = ogrNot.BasariNot;
                basariNot.Mutlak = ogrNot.Not;
                basariNot.EnumDonem = donem;
                basariNot.Yil = yil;
                basariNot.Tarih = DateTime.Now;
                basariNot.EnumHarfBasari = (int)ogrNot.BasariHarf;
                basariNot.EnumHarfMutlak = (int)ogrNot.MutlakNot;
                basariNot.Bagil = ogrNot.BasariNot;
                basariNot.FKDersGrupID = ogrNot.GrupID;
                basariNot.FKOgrenciID = ogrNot.OgrID;
                basariNot.Kullanici = KullaniciAdi;
                basariNot.Makina = IPAdresi;
                basariNot.ButunlemeMi = ogrNot.ButunlemeMi;
                basariNot.SilindiMi = false;
                basariNot.FKDersPlanAnaID = ogrNot.DersPlanAnaID;
                basariNot.FKDegerlendirmeID = degerlendirmeID;
                if (yeniMi)
                {
                    context.ABSBasariNot.Add(basariNot);
                }
                sayac++;
                if (sayac % 50 == 0)
                {
                    context.SaveChanges();
                    context.Dispose();
                    context = new Sabis.Bolum.Bll.DATA.UysModel.UYSv2Entities();
                }
            }
            context.SaveChanges();
        }

        public void Sil(List<Core.ABS.DEGERLENDIRME.Grup> OrtakGruplar, bool ButunlemeMi)
        {
            foreach (Core.ABS.DEGERLENDIRME.Grup grup in OrtakGruplar.OrderBy(x => x.DersGrupID))
            {
                var kayitlarBasariNot = context.ABSBasariNot.Where(x => x.FKDersGrupID == grup.DersGrupID && x.ButunlemeMi == ButunlemeMi);
                foreach (var kayit in kayitlarBasariNot)
                {
                    kayit.SilindiMi = true;
                }
                var kayitlarDegerlendirme = context.ABSDegerlendirme.Where(x => x.FKDersGrupID == grup.DersGrupID && x.ButunlemeMi == ButunlemeMi);
                foreach (var kayit in kayitlarDegerlendirme)
                {
                    kayit.SilindiMi = true;
                }
            }
            context.SaveChanges();
        }

        public void Yayinla(List<DegerlendirmeDTO> DegerlendirmeKayit)
        {
            foreach (var degerlendirme in DegerlendirmeKayit)
            {
                bool yeniMi = false;
                var sorgu = context.ABSYayinlananBasariNotlar.Where(x => x.ButunlemeMi == degerlendirme.ButunlemeMi && x.ABSDegerlendirme.SilindiMi == false).AsQueryable();
                if (degerlendirme.FKDersGrupID.HasValue)
                {
                    sorgu = sorgu.Where(x => x.FKDersGrupID == degerlendirme.FKDersGrupID.Value);
                }
                else
                {
                    sorgu = sorgu.Where(x => x.FKDersPlanAnaID == degerlendirme.FKDersPlanAnaID.Value && x.Yil == degerlendirme.Yil && x.EnumDonem == degerlendirme.EnumDonem);
                }
                Sabis.Bolum.Bll.DATA.UysModel.ABSYayinlananBasariNotlar kayit = sorgu.FirstOrDefault();
                if (kayit == null)
                {
                    kayit = new Sabis.Bolum.Bll.DATA.UysModel.ABSYayinlananBasariNotlar();
                    yeniMi = true;
                }
                kayit.ButunlemeMi = degerlendirme.ButunlemeMi;
                kayit.FKABSDegerlendirme = degerlendirme.ID;
                kayit.FKDersGrupID = degerlendirme.FKDersGrupID;
                kayit.FKDersPlanAnaID = degerlendirme.FKDersPlanAnaID;
                kayit.Yayinlandi = true;
                kayit.YayinlanmaTarihi = DateTime.Now;
                kayit.Yil = degerlendirme.Yil;
                kayit.EnumDonem = degerlendirme.EnumDonem;
                if (yeniMi)
                {
                    context.ABSYayinlananBasariNotlar.Add(kayit);
                }
            }
            context.SaveChanges();
        }

        public void YayindanKaldir(List<int> grupIDList, bool ButunlemeMi)
        {
            foreach (var item in context.ABSYayinlananBasariNotlar.Where(x => x.FKDersGrupID.HasValue && grupIDList.Contains(x.FKDersGrupID.Value) && x.ButunlemeMi == ButunlemeMi && x.Yayinlandi == true))
            {
                item.Yayinlandi = false;
            }
            context.SaveChanges();
        }

        public void SonHal(List<DegerlendirmeDTO> DegerlendirmeKayit, string KullaniciAdi, string IPAdresi)
        {
            foreach (var item in DegerlendirmeKayit)
            {
                Sabis.Bolum.Bll.DATA.UysModel.ABSDegerlendirme kayit = context.ABSDegerlendirme.First(x => x.ID == item.ID);
                kayit.EnumSonHal = (int)EnumSonHal.EVET;
                kayit.Kullanici = KullaniciAdi;
                kayit.Makina = IPAdresi;
                kayit.BarkodNo = item.BarkodNo;
                kayit.Yil = item.Yil;
                kayit.EnumDonem = item.EnumDonem;
                kayit.TarihSonHal = DateTime.Now;
            }
            context.SaveChanges();
        }

        public void SonHalKaldir(List<DegerlendirmeDTO> DegerlendirmeKayit)
        {
            foreach (var item in DegerlendirmeKayit)
            {
                Sabis.Bolum.Bll.DATA.UysModel.ABSDegerlendirme kayit = context.ABSDegerlendirme.First(x => x.ID == item.ID);
                kayit.EnumSonHal = (int)EnumSonHal.HAYIR;
                kayit.BarkodNo = null;
                kayit.TarihSonHal = null;
            }
            context.SaveChanges();
        }

        public GPay PayKaydet(GPay butunlemePayi)
        {
            Sabis.Bolum.Bll.DATA.UysModel.ABSPaylar kayit = context.ABSPaylar.FirstOrDefault(x => x.FKDersPlanAnaID == butunlemePayi.DersPlanAnaID && x.FKDersPlanID == butunlemePayi.DersPlanID && x.EnumCalismaTip == (int)butunlemePayi.EnumCalismaTip);
            bool yeniMi = false;
            if (kayit == null)
            {
                kayit = new Sabis.Bolum.Bll.DATA.UysModel.ABSPaylar();
                yeniMi = true;
            }
            kayit.EnumCalismaTip = (int)butunlemePayi.EnumCalismaTip;
            kayit.FKDersPlanAnaID = butunlemePayi.DersPlanAnaID;
            kayit.FKDersPlanID = butunlemePayi.DersPlanID;
            kayit.Oran = butunlemePayi.Oran;
            kayit.Pay = butunlemePayi.Pay;
            kayit.Payda = butunlemePayi.Payda;
            kayit.Sira = butunlemePayi.Sira;
            kayit.Yil = butunlemePayi.Yil;
            kayit.EnumDonem = butunlemePayi.EnumDonem;
            if (yeniMi)
            {
                context.ABSPaylar.Add(kayit);
            }
            context.SaveChanges();
            butunlemePayi.PayID = kayit.ID;
            return butunlemePayi;
        }

        public void GetirKullaniciYetkileri(string kullaniciAdi, string IPAdresi, out bool tarihYetkisiVarMi, out bool sonHalKaldirmaYetkisiVarMi)
        {
            Sabis.Bolum.Bll.DATA.UysModel.ABSIPYetki yetki = context.ABSIPYetki.FirstOrDefault(x => x.IPAdresi == IPAdresi || x.KullaniciAdi == kullaniciAdi);
            tarihYetkisiVarMi = false;
            sonHalKaldirmaYetkisiVarMi = false;
            if (yetki != null)
            {
                tarihYetkisiVarMi = yetki.YetkiVar;
                if (yetki.SonHalKaldirmaVarMi)
                {
                    sonHalKaldirmaYetkisiVarMi = true;
                }
            }
        }


        public List<OgrenciPayveNotBilgileriDTO> GetirBasariNotListesi(int personelID, bool butunlemeMi, int? grupID = null)
        {
            List<OgrenciPayveNotBilgileriDTO> result = null;
            context.Database.CommandTimeout = 600;
            var basariNotSQL = context.ABSBasariNot.Where(x => x.ButunlemeMi == butunlemeMi && x.SilindiMi == false).AsQueryable();
            if (grupID.HasValue)
            {
                basariNotSQL = basariNotSQL.Where(x => x.FKDersGrupID == grupID.Value);
            }
            var resultSQL = from basariNot in basariNotSQL
                            select new
                            {
                                Numara = basariNot.OGRKimlik.Numara,
                                AdSoyad = basariNot.OGRKimlik.Kisi.Ad + " " + basariNot.OGRKimlik.Kisi.Soyad,
                                OgrenciID = basariNot.OGRKimlik.ID,
                                DersPlanAnaID = basariNot.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.ID,
                                DersGrupID = basariNot.FKDersGrupID,
                                DersPlanID = basariNot.OGRDersGrup.FKDersPlanID,
                                PersonelID = personelID,
                                Notlar = basariNot.OGRDersGrup.ABSOgrenciNot.Where(v=> v.Silindi == false).OrderBy(d => d.ID),
                                BasariNot = basariNot.OGRDersGrup.ABSBasariNot.FirstOrDefault(x => x.FKOgrenciID == basariNot.FKOgrenciID && x.ButunlemeMi == butunlemeMi && x.SilindiMi == false),
                                YazilmaID = basariNot.ID
                            };
            result = new List<OgrenciPayveNotBilgileriDTO>();
            var resultTMP = resultSQL.ToList();
            foreach (var item in resultTMP)
            {
                OgrenciPayveNotBilgileriDTO kayit = new OgrenciPayveNotBilgileriDTO();
                kayit.AdSoyad = item.AdSoyad;
                kayit.BasariHarf = (Sabis.Enum.EnumHarfBasari)item.BasariNot.EnumHarfBasari;
                kayit.MutlakHarf = (Sabis.Enum.EnumHarfMutlak)item.BasariNot.EnumHarfMutlak;
                kayit.DersGrupID = item.DersGrupID;
                kayit.DersPlanAnaID = item.DersPlanAnaID;
                kayit.DersPlanID = item.DersPlanID;
                kayit.MutlakNot = item.BasariNot.Mutlak.Value;
                kayit.Numara = item.Numara;
                kayit.OgrenciID = item.OgrenciID;
                kayit.PersonelID = item.PersonelID;
                result.Add(kayit);
            }
            return result;
        }

        public Core.ABS.DEGERLENDIRME.Grup GetirGrupBilgi(int? dersGrupID)
        {
            var data = context.OGRDersGrup.First(x => x.ID == dersGrupID);
            Core.ABS.DEGERLENDIRME.Grup result = new Core.ABS.DEGERLENDIRME.Grup();
            result.Ad = data.GrupAd;
            result.DersGrupID = data.ID;
            result.EnumOgretimTur = (EnumOgretimTur)data.EnumOgretimTur;
            return result;
        }

        public void EBYSAktarimaUygunMu(int grupID, out int ebysID)
        {
            ebysID = 0;
            var sonuc = context.ABSDegerlendirme.FirstOrDefault(x => x.FKDersGrupID == grupID);
            if (sonuc != null)
            {
                if (sonuc.EBYSDocTreeID.HasValue)
                {
                    throw new Exception("Bu gruba ait belge zaten EBYSye aktarılmış. Tekrar aktarım yapılamaz! EBYSID: " + sonuc.EBYSDocTreeID.Value.ToString());
                }
            }
            else
            {
                throw new Exception("Bu gruba ait herhangi bir değerlendirme kaydı bulunamadı!");
            }
        }

        private class DegerlendirmeYazilmaSQLDto
        {
            public string AdSoyad { get; set; }
            public bool? ButunlemeMi { get; set; }
            public int? DersGrupID { get; set; }
            public int DersPlanAnaID { get; set; }
            public int? DersPlanID { get; set; }
            public int? EnumOgrenciDurum { get; set; }
            public string Numara { get; set; }
            public int OgrenciID { get; set; }
            public int YazilmaID { get; set; }
        }
    }

    internal class OgrenciNotDataDTO
    {
        public string Ad { get; set; }
        public int DersPlanAnaID { get; set; }
        public int DersPlanID { get; set; }
        public int? EnumOgrenciDurum { get; set; }
        public int? FKAbsPaylarID { get; set; }
        public int? FKDersGrupID { get; set; }
        public int FKOgrenciID { get; set; }
        public int ID { get; set; }
        public double NotDeger { get; set; }
        public string Numara { get; set; }
        public string Soyad { get; set; }
    }
}
