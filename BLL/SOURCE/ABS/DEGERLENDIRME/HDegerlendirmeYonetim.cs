﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AutoMapper;
using Sabis.Bolum.Core.ABS.DEGERLENDIRME;
using Sabis.Bolum.Core.ABS.DEGERLENDIRME.Hiyerarsik;
using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Enum;

namespace Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME
{
    public class HDegerlendirmeYonetim : AbstractDegerlendirme, IDisposable
    {
        public new List<HPay> PayList { get; set; }
        Dictionary<OgrenciBilgi, List<HPayOgrenciNot>> TumNotlar;
        public int Yil { get; set; }
        public int Donem { get; set; }
        UYSv2Entities db = new UYSv2Entities();
        public HDegerlendirmeYonetim(int dersPlanAnaID, int yil, int donem, string kullaniciAdi, int personelID, string IP, string simuleEden = null)
        {
            Yil = yil;
            Donem = donem;

            SeciliDers = GetirDersDTO(dersPlanAnaID);
            HiyerarsikDersKontrol(true);
            PaylariDoldur(dersPlanAnaID);
            base.Init(kullaniciAdi, personelID, IP, simuleEden);

            TumNotlariGetir();
            //TODO Paylar düzgün mü?

            //Değerlendirme için uygun mu?
            DegerlendiriciMi = true;
            DegerlendirmeyeUygunMu();

            //Değerlendirmeye ait bir hesap kaydı varsa getir
            GetirDegerlendirme();
            //Değerlendirmenin aşamasını bul
            EnumDegerlendirmeAsama = AsamaBul(DegerlendirmeKayit);

            GetirKural();

            Degerlendir(default(double?));
        }

        private void GetirDegerlendirme()
        {
            db.Set<ABSDegerlendirme>().AsNoTracking();
            var degerlendirmeKayit = db.ABSDegerlendirme.Where(x => x.FKDersPlanAnaID == SeciliDers.DersPlanAnaID && x.ButunlemeMi == ButunlemeMi && x.SilindiMi == false && x.Yil == Yil && (SeciliDers.EnumDersSureTip == EnumDersSureTip.YILLIK || (SeciliDers.EnumDersSureTip == EnumDersSureTip.DONEMLIK && x.EnumDonem == Donem))).OrderByDescending(x => x.ID).ToList();

            List<DegerlendirmeDTO> result = new List<DegerlendirmeDTO>();

            foreach (var item in degerlendirmeKayit)
            {
                DegerlendirmeDTO kayit = new DegerlendirmeDTO();
                kayit.AktarilanVeriMi = item.AktarilanVeriMi;
                kayit.AktarimTarihi = item.AktarimTarihi;
                kayit.BagilOrtalama = item.BagilOrtalama;
                kayit.BarkodNo = item.BarkodNo;
                kayit.ButunlemeMi = item.ButunlemeMi;
                kayit.EBYSDocTreeID = item.EBYSDocTreeID;
                kayit.EnBuyukNot = item.EnBuyukNot;
                kayit.EnKucukNot = item.EnKucukNot;
                kayit.EnumDonem = item.EnumDonem;
                kayit.EnumSonHal = item.EnumSonHal;
                kayit.FKDersGrupID = item.FKDersGrupID;
                kayit.FKDersPlanAnaID = item.FKDersPlanAnaID;
                kayit.FKPersonelID = item.FKPersonelID;
                kayit.ID = item.ID;
                kayit.Kullanici = item.Kullanici;
                kayit.Makina = item.Makina;
                kayit.OrtakDegerlendirmeGUID = item.OrtakDegerlendirmeGUID;
                kayit.Ortalama = item.Ortalama;
                kayit.SicilNo = item.SicilNo;
                kayit.StandartSapma = item.StandartSapma;
                kayit.Tarih = item.Tarih;
                kayit.UstDeger = item.UstDeger;
                kayit.Yil = item.Yil;
                kayit.Zaman = item.Zaman;
                result.Add(kayit);
            }

            DegerlendirmeKayit = result;
        }

        private void TumNotlariGetir()
        {
            Dictionary<int, Dictionary<int, HPayOgrenciNot>> payDict = new Dictionary<int, Dictionary<int, HPayOgrenciNot>>();
            HashSet<int> yazilmaHash = GetirYazilmaList();

            GetirHNotDict(ref payDict, PayList);
            Dictionary<int, List<HPayOgrenciNot>> paketDict = HazirlaOgrenciDict(ref payDict);
            paketDict = OgrenciNotPaketDict(PayList, ref payDict, ref paketDict, ref yazilmaHash);
            TumNotlar = GetirOgrenciBilgiNotDict(paketDict);
#if DEBUG
            /*
           foreach (var item in TumNotlar)
           {
               RastgeleNotGir(item.Value);
           }
           */
#endif
        }

        private HashSet<int> GetirYazilmaList()
        {
            HashSet<int> result = new HashSet<int>();
            var sorgu = db.OGROgrenciYazilma.Where(x => !x.Iptal && x.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.FKHiyerarsikKokID == SeciliDers.DersPlanAnaID && x.Yil == SeciliDers.Yil).AsQueryable();
            if (ButunlemeMi)
            {
                sorgu = sorgu.Where(x => x.ButunlemeMi == true);
            }
            else
            {
                sorgu = sorgu.Where(x => (x.ButunlemeMi.HasValue == false || x.ButunlemeMi == false));
            }
            var sorgu2 = sorgu.Select(x => x.FKOgrenciID.Value).AsQueryable();
            var sql = sorgu2.ToString();

            foreach (var item in sorgu2.ToList())
            {
                result.Add(item);
            }
            return result;
        }

        Random rr = new Random();
        private void RastgeleNotGir(List<HPayOgrenciNot> value)
        {

            foreach (var item in value)
            {
                if (item.AltHPayList.Count > 0)
                {
                    RastgeleNotGir(item.AltHPayList);
                }

                item.NotDeger = rr.Next(0, 100);
            }
        }


        private Dictionary<OgrenciBilgi, List<HPayOgrenciNot>> GetirOgrenciBilgiNotDict(Dictionary<int, List<HPayOgrenciNot>> paketDict)
        {
            Dictionary<OgrenciBilgi, List<HPayOgrenciNot>> result = new Dictionary<OgrenciBilgi, List<HPayOgrenciNot>>();

            foreach (var item in paketDict)
            {
                var ogrenciBilgi = db.OGRKimlik.Where(x => x.ID == item.Key).Select(x => new OgrenciBilgi { OgrenciID = x.ID, Numara = x.Numara, AdSoyad = x.Kisi.Ad + " " + x.Kisi.Soyad }).First();
                result.Add(ogrenciBilgi, item.Value);
            }

            return result;
        }

        private Dictionary<int, List<HPayOgrenciNot>> HazirlaOgrenciDict(ref Dictionary<int, Dictionary<int, HPayOgrenciNot>> payDict)
        {
            Dictionary<int, List<HPayOgrenciNot>> result = new Dictionary<int, List<HPayOgrenciNot>>();
            foreach (var pay in payDict)
            {
                foreach (var item in pay.Value)
                {
                    if (!result.ContainsKey(item.Key))
                    {
                        result.Add(item.Key, new List<HPayOgrenciNot>());
                    }
                }
            }
            return result;
        }

        private void GetirHNotDict(ref Dictionary<int, Dictionary<int, HPayOgrenciNot>> payNotDict, List<HPay> payH)
        {
            foreach (var pItem in payH)
            {
                if (payNotDict.ContainsKey(pItem.PayID))
                {
                    continue;
                }
                Dictionary<int, HPayOgrenciNot> notDict = new Dictionary<int, HPayOgrenciNot>();
                var payNotList = db.ABSOgrenciNot.Where(x => x.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL && x.FKAbsPaylarID == pItem.PayID && x.FKDersPlanAnaID == pItem.DersPlanAnaID && x.Silindi == false).OrderByDescending(x => x.ID);
                foreach (var item in payNotList)
                {
                    if (notDict.ContainsKey(item.FKOgrenciID))
                    {
                        continue;
                    }
                    HPayOgrenciNot ogrPayNot = new HPayOgrenciNot();
                    ogrPayNot.DersPlanAnaID = item.FKDersPlanAnaID;
                    ogrPayNot.EnumCalismaTip = pItem.EnumCalismaTip;
                    ogrPayNot.NotID = item.ID;
                    ogrPayNot.NotDeger = item.NotDeger;
                    ogrPayNot.OgrenciID = item.FKOgrenciID;
                    ogrPayNot.Yuzde = pItem.Yuzde;
                    ogrPayNot.PayID = pItem.PayID;
                    ogrPayNot.Sira = pItem.Sira;
                    notDict.Add(ogrPayNot.OgrenciID, ogrPayNot);
                }
                payNotDict.Add(pItem.PayID, notDict);
                if (pItem.AltHPayList != null)
                {
                    GetirHNotDict(ref payNotDict, pItem.AltHPayList);
                }
            }
        }

        private Dictionary<int, List<HPayOgrenciNot>> OgrenciNotPaketDict(List<HPay> payH, ref Dictionary<int, Dictionary<int, HPayOgrenciNot>> payDict, ref Dictionary<int, List<HPayOgrenciNot>> ogrenciPaketDict, ref HashSet<int> yazilmaHash)
        {
            Dictionary<int, List<HPayOgrenciNot>> result = new Dictionary<int, List<HPayOgrenciNot>>();

            foreach (var ogrenci in ogrenciPaketDict)
            {
                int ogrenciID = ogrenci.Key;
                if (!yazilmaHash.Contains(ogrenciID))
                {
                    continue;
                }
                List<HPayOgrenciNot> ogrNotList = GetirOgrenciPayNot(payH, ref payDict, ogrenciID);
                result.Add(ogrenciID, ogrNotList);
            }

            return result;
        }

        private List<HPayOgrenciNot> GetirOgrenciPayNot(List<HPay> payH, ref Dictionary<int, Dictionary<int, HPayOgrenciNot>> payDict, int ogrenciID)
        {
            List<HPayOgrenciNot> result = new List<HPayOgrenciNot>();
            foreach (var pay in payH)
            {
                Dictionary<int, HPayOgrenciNot> ogrNotDict = null;
                if (payDict.TryGetValue(pay.PayID, out ogrNotDict))
                {
                    HPayOgrenciNot kokPayNot = null;
                    if (!ogrNotDict.TryGetValue(ogrenciID, out kokPayNot))
                    {
                        kokPayNot = new HPayOgrenciNot(pay);
                        kokPayNot.OgrenciID = ogrenciID;
                    }
                    kokPayNot.AltHPayList = GetirOgrenciAltNot(pay, ogrenciID, ref payDict);
                    result.Add(kokPayNot);
                }
            }

            return result;
        }

        private List<HPayOgrenciNot> GetirOgrenciAltNot(HPay ustPay, int ogrenciID, ref Dictionary<int, Dictionary<int, HPayOgrenciNot>> payDict)
        {
            List<HPayOgrenciNot> result = new List<HPayOgrenciNot>();
            foreach (var pay in ustPay.AltHPayList)
            {
                Dictionary<int, HPayOgrenciNot> ogrNotDict = null;
                if (payDict.TryGetValue(pay.PayID, out ogrNotDict))
                {
                    HPayOgrenciNot ogrPayNot = null;
                    if (!ogrNotDict.TryGetValue(ogrenciID, out ogrPayNot))
                    {
                        ogrPayNot = new HPayOgrenciNot(pay);
                        ogrPayNot.NotID = null;
                        ogrPayNot.NotDeger = 120;
                        ogrPayNot.OgrenciID = ogrenciID;
                    }
                    if (pay.AltHPayList != null)
                    {
                        ogrPayNot.AltHPayList = GetirOgrenciAltNot(pay, ogrenciID, ref payDict);
                    }
                    result.Add(ogrPayNot);
                }
            }
            return result;
        }

        private void PaylariDoldur(int dersPlanAnaID)
        {
            var payH = new List<HPay>();
            var payHAltDersler = GetirHPay(SeciliDers.DersPlanAnaID, EnumCalismaTip.AltDersinKatkisi, Yil, Donem);
            if (payHAltDersler.Any())
            {
                payH.AddRange(payHAltDersler);
            }
            else
            {
                var payHYilIci = GetirHPay(SeciliDers.DersPlanAnaID, EnumCalismaTip.AltDersinYiliciKatkisi, Yil, Donem);
                var payHFinal = GetirHPay(SeciliDers.DersPlanAnaID, EnumCalismaTip.AltDersinFinalKatkisi, Yil, Donem);
                payH.AddRange(payHYilIci);
                payH.AddRange(payHFinal);
            }
            PayList = payH;
        }

        private List<HPay> GetirHPay(int dersPlanAnaID, EnumCalismaTip ustCalismaTip, int yil, int donem)
        {
            List<HPay> result = new List<HPay>();
            var payListSorgu = db.ABSPaylar.AsNoTracking().Where(x => x.Silindi && x.FKDersPlanAnaID == dersPlanAnaID && x.Yil == yil && (x.OGRDersPlanAna.EnumDersSureTip == (int)EnumDersSureTip.YILLIK || (x.OGRDersPlanAna.EnumDersSureTip == (int)EnumDersSureTip.DONEMLIK && x.EnumDonem == donem))).AsQueryable();

            if (ustCalismaTip == EnumCalismaTip.AltDersinYiliciKatkisi)
            {
                payListSorgu = payListSorgu.Where(x => x.EnumCalismaTip == (int)EnumCalismaTip.AltDersinYiliciKatkisi || (x.EnumCalismaTip < 10 || x.EnumCalismaTip == (int)EnumCalismaTip.PRATIK || x.EnumCalismaTip == (int)EnumCalismaTip.TEORIK));
            }
            else if (ustCalismaTip == EnumCalismaTip.AltDersinFinalKatkisi)
            {
                payListSorgu = payListSorgu.Where(x => x.EnumCalismaTip == (int)EnumCalismaTip.AltDersinFinalKatkisi || x.EnumCalismaTip == (int)EnumCalismaTip.Final || x.EnumCalismaTip == (int)EnumCalismaTip.Butunleme);
            }
            var payList = payListSorgu.Select(x => new { PayID = x.ID, DersPlanAnaID = x.FKDersPlanAnaID.Value, EnumCalismaTip = x.EnumCalismaTip.Value, Oran = x.Oran, Sira = x.Sira.Value, FKAltDersPlanAnaID = x.FKAltDersPlanAnaID, Pay = x.Pay, Payda = x.Payda, Yil = x.Yil, Donem = x.EnumDonem });
            foreach (var item in payList)
            {
                HPay pay = new HPay();
                pay.DersPlanAnaID = item.DersPlanAnaID;
                pay.EnumCalismaTip = (EnumCalismaTip)item.EnumCalismaTip;
                pay.Oran = item.Oran;
                pay.Pay = item.Pay;
                pay.Payda = item.Payda;
                pay.PayID = item.PayID;
                pay.Sira = item.Sira;
                if (item.Oran.HasValue)
                {
                    pay.Yuzde = item.Oran.Value;
                }
                else
                {
                    pay.Yuzde = (double)item.Pay.Value / (double)item.Payda.Value;
                }
                if (item.FKAltDersPlanAnaID.HasValue)
                {
                    pay.AltHPayList = GetirHPay(item.FKAltDersPlanAnaID.Value, pay.EnumCalismaTip, yil, donem);
                }
                else
                {
                    pay.AltHPayList = new List<HPay>();
                }
                result.Add(pay);
            }

            if (result.Any(x => x.EnumCalismaTip == EnumCalismaTip.Butunleme))
            {
                result = result.Where(x => x.EnumCalismaTip != EnumCalismaTip.Final).ToList();
            }
            return result;
        }

        private void HiyerarsikDersKontrol(int dersPlanAnaID)
        {
            bool hiyerarsikDersMi = false;
            using (UYSv2Entities db = new UYSv2Entities())
            {
                hiyerarsikDersMi = db.OGRDersPlanAna.Any(x => x.ID == dersPlanAnaID && x.Hiyerarsik);
            }
            if (!hiyerarsikDersMi)
            {
                throw new Exception("Hiyerarşik olmayan ders değerlendirlemeri bu ekrandan yapılamaz!");
            }
        }

        protected override DersDTO GetirDersDTO(int dersPlanAnaID, string dil = "tr")
        {
            DersDTO result = null;
            using (UYSv2Entities db = new UYSv2Entities())
            {
                //Kök ders değilse kök derse yönlendir.
                var bulunan = db.OGRDersPlanAna.FirstOrDefault(x => x.ID == dersPlanAnaID && x.Hiyerarsik);
                if (bulunan != null && bulunan.FKDersPlanAnaUstID.HasValue)
                {
                    throw new Exception("Sadece kök dersler değerlendirilebilir!");
                }
                else if (bulunan == null)
                {
                    return null;
                }

                var hocaDersSorgu = (from veri in db.OGRDersPlanAna
                                     where veri.Hiyerarsik && veri.ID == dersPlanAnaID
                                     select new
                                     {
                                         DersAd = (dil == "en" ? veri.YabDersAd : veri.DersAd),
                                         OgrenciSayisi = db.OGROgrenciYazilma.Where(x => !x.Iptal &&
                                         x.Yil == Yil && (x.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.EnumDersSureTip == (int)EnumDersSureTip.YILLIK || (x.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.EnumDersSureTip == (int)EnumDersSureTip.DONEMLIK && x.EnumDonem == Donem)) &&
                                         x.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.FKHiyerarsikKokID == dersPlanAnaID && x.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL).Select(x => x.FKOgrenciID.Value).Distinct().Count(),
                                         OgrenciSayisiButunleme = db.OGROgrenciYazilma.Where(x => !x.Iptal &&
                                          x.Yil == Yil && (x.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.EnumDersSureTip == (int)EnumDersSureTip.YILLIK || (x.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.EnumDersSureTip == (int)EnumDersSureTip.DONEMLIK && x.EnumDonem == Donem)) &&
                                         x.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.FKHiyerarsikKokID == dersPlanAnaID && x.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL && (x.ButunlemeMi.HasValue && x.ButunlemeMi.Value)).Select(x => x.FKOgrenciID.Value).Distinct().Count(),
                                         DersPlanAnaID = veri.ID,
                                         Kredi = veri.Kredi,
                                         DSaat = veri.DSaat,
                                         USaat = veri.USaat,
                                         ButunlemeMi = db.OGROgrenciYazilma.Where(x => !x.Iptal && x.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.FKHiyerarsikKokID == dersPlanAnaID && x.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL && (x.ButunlemeMi.HasValue && x.ButunlemeMi.Value == true)).Select(x => x.FKOgrenciID.Value).Distinct().Any(),
                                         Personel = veri.DersKoordinator.Where(x => x.BaslangicTarih < DateTime.Now && (!x.BitisTarih.HasValue || x.BitisTarih.Value > DateTime.Now)),
                                         Hiyerarsik = veri.Hiyerarsik,
                                         EnumDersSureTip = veri.EnumDersSureTip
                                     });
#if DEBUG

                var sql = hocaDersSorgu.ToString();
#endif
                var item = hocaDersSorgu.FirstOrDefault();

                if (item == null)
                {
                    return null;
                }

                bool butunlemeMi = item.ButunlemeMi;
                var degerlendirme = db.ABSDegerlendirme.FirstOrDefault(x => x.FKDersPlanAnaID == dersPlanAnaID && x.SilindiMi == false && x.ButunlemeMi == butunlemeMi);
                EnumSonHal sonHal = EnumSonHal.BELIRSIZ;

                if (degerlendirme == null)
                {
                    sonHal = EnumSonHal.HAYIR;
                }
                else if (degerlendirme.EnumSonHal == (int)EnumSonHal.EVET)
                {
                    sonHal = EnumSonHal.EVET;
                }
                else
                {
                    sonHal = EnumSonHal.HAYIR;
                }

                result = new DersDTO();
                result.ButunlemeMi = item.ButunlemeMi;
                result.ButunlemeMiOrijinal = item.ButunlemeMi;
                if (result.ButunlemeMiOrijinal)
                {
                    //Bütünleme notu girilmiş ise bütünleme modunu aktifleştir.
                    result.ButunlemeMi = db.ABSOgrenciNot.Any(x => x.Yil == Yil && x.OGRDersPlanAna.FKHiyerarsikKokID == item.DersPlanAnaID && x.ABSPaylar.EnumCalismaTip == (int)EnumCalismaTip.Butunleme && x.Silindi == false);
                }

                result.Bolum = new Sabis.Core.DtoBirimler();
                result.Bolum.BirimAdi = item.DersAd;
                result.Bolum.BasilanBirimAdi = item.DersAd;
                result.Bolum.ID = 21398;

                result.Fakulte = new Sabis.Core.DtoBirimler();
                result.Fakulte.BirimAdi = item.DersAd;
                result.Fakulte.BasilanBirimAdi = item.DersAd;
                result.Fakulte.ID = 21398;

                result.DersAd = item.DersAd;
                result.DersPlanAnaID = item.DersPlanAnaID;
                result.Donem = Donem;
                result.Yil = Yil;
                result.OgrenciSayisi = item.OgrenciSayisi;
                result.OgrenciSayisiButunleme = item.OgrenciSayisiButunleme;
                result.Kredi = item.Kredi;
                result.DSaat = item.DSaat;
                result.USaat = item.USaat;
                result.SonHal = sonHal;
                result.EnumDersSureTip = (EnumDersSureTip)item.EnumDersSureTip;
                result.Hiyerarsik = item.Hiyerarsik;

                if (item.Personel.Any())
                {
                    var koord = item.Personel.First();
                    result.Personel = new PersonelDTO
                    {
                        Ad = koord.PersonelBilgisi.Kisi.Ad,
                        Soyad = koord.PersonelBilgisi.Kisi.Soyad,
                        UnvanAd = koord.PersonelBilgisi.UnvanAd
                    };
                }
            }
            return result;
        }

        public override void Degerlendir(double? _ustDeger, double? _bagilAritmetikOrtalama = default(double?))
        {
            if (SeciliDers == null)
            {
                throw new DegerlendirmeException("Değerlendirme işlemi için en az bir ders seçili olmalıdır!");
            }
            var result = NOTDEGERLENDIRME.HDegerlendirme.Hesapla(TumNotlar, DegerlendirmeKural, ButunlemeMi);
            result.BagilAritmetikOrtalama = result.NotList.Average(x => x.BasariNot);
            result.EnBuyukNot = result.NotList.Max(x => x.BasariNot);
            result.EnKucukNot = result.NotList.Min(x => x.BasariNot);
            result.MinNot = result.NotList.Min(x => x.BasariNot);
            result.Ortalama = result.NotList.Average(x => x.Not);
            result.Butunleme = ButunlemeMi;
            Guid ortakDegerGUID = Guid.NewGuid();
            var degerlendirmeKayit = DegerlendirmeKayit.FirstOrDefault(x => x.FKDersPlanAnaID == SeciliDers.DersPlanAnaID);
            bool yeniMi = false;
            if (degerlendirmeKayit == null)
            {
                degerlendirmeKayit = new DegerlendirmeDTO();
                yeniMi = true;
            }
            degerlendirmeKayit.EnBuyukNot = result.EnBuyukNot;
            degerlendirmeKayit.StandartSapma = result.StandartSapma;
            degerlendirmeKayit.Ortalama = result.Ortalama;
            degerlendirmeKayit.BagilOrtalama = result.BagilAritmetikOrtalama;
            degerlendirmeKayit.Tarih = DateTime.Now;
            degerlendirmeKayit.FKPersonelID = PersonelID;
            degerlendirmeKayit.OrtakDegerlendirmeGUID = ortakDegerGUID;
            degerlendirmeKayit.FKDersGrupID = null;
            degerlendirmeKayit.FKDersPlanAnaID = SeciliDers.DersPlanAnaID;
            degerlendirmeKayit.Kullanici = KullaniciAdi;
            degerlendirmeKayit.BarkodNo = null;
            degerlendirmeKayit.UstDeger = result.UstDeger;
            degerlendirmeKayit.EnumDonem = SeciliDers.Donem;
            degerlendirmeKayit.Yil = SeciliDers.Yil;
            degerlendirmeKayit.EnumSonHal = (int)EnumSonHal.HAYIR;
            degerlendirmeKayit.Makina = IPAdresi;
            degerlendirmeKayit.ButunlemeMi = ButunlemeMi;
            if (yeniMi)
            {
                DegerlendirmeKayit.Add(degerlendirmeKayit);
            }
            DegerlendirmeSonuc = result;
        }

        public override void Kaydet()
        {
            if (EnumDegerlendirmeAsama == EnumDegerlendirmeAsama.AKTARILDI)
            {
                throw new DegerlendirmeException("Öğrenci işlerine aktarımı gerçekleştirilen değerlendirmeler değiştirilemez!");
            }
            if (EnumDegerlendirmeAsama == EnumDegerlendirmeAsama.SONHAL)
            {
                throw new DegerlendirmeException("Son hal verilen değerlendirmeler değiştirilemez!");
            }
            if (EnumDegerlendirmeAsama == EnumDegerlendirmeAsama.YAYINLANDI)
            {
                throw new DegerlendirmeException("Değişiklik yapabilmek için lütfen önce değerlendirmeleri yayından kaldırınız!");
            }
            if (!TarihUygunMu)
            {
                throw new DegerlendirmeException("Değerlendirme için geçersiz tarih aralığı!");
            }
            DegerlendirmeKayit = data.KaydetDegerlendirme(DegerlendirmeKayit);

            KaydetBasariNot(DegerlendirmeKayit.First().ID);
            EnumDegerlendirmeAsama = EnumDegerlendirmeAsama.KAYITLI;
            NotificationList.Add(new Notification("Kaydedildi", "Değerlendirme işlemi başarıyla kaydedildi.", Notification.MasterMesajTip.Basari));
        }

        private void KaydetBasariNot(int degerlendirmeID)
        {
            int sayac = 0;
            foreach (var ogrNot in DegerlendirmeSonuc.NotList)
            {
                bool yeniMi = false;
                ABSBasariNot basariNot = db.ABSBasariNot.FirstOrDefault(x => x.FKDersPlanAnaID == SeciliDers.DersPlanAnaID &&
                x.Yil == SeciliDers.Yil && (SeciliDers.EnumDersSureTip == EnumDersSureTip.YILLIK || (SeciliDers.EnumDersSureTip == EnumDersSureTip.DONEMLIK && x.EnumDonem == SeciliDers.Donem))
                && x.FKOgrenciID == ogrNot.OgrID && x.ButunlemeMi == ButunlemeMi && x.SilindiMi == false);
                if (basariNot == null)
                {
                    yeniMi = true;
                    basariNot = new ABSBasariNot();
                }

                basariNot.Bagil = ogrNot.BasariNot;
                basariNot.Mutlak = ogrNot.Not;
                basariNot.EnumDonem = SeciliDers.Donem;
                basariNot.Yil = SeciliDers.Yil;
                basariNot.Tarih = DateTime.Now;
                basariNot.EnumHarfBasari = (int)ogrNot.BasariHarf;
                basariNot.EnumHarfMutlak = (int)ogrNot.MutlakNot;
                basariNot.Bagil = ogrNot.BasariNot;
                basariNot.FKDersGrupID = null;
                basariNot.FKDersPlanID = null;
                basariNot.FKOgrenciID = ogrNot.OgrID;
                basariNot.Kullanici = KullaniciAdi;
                basariNot.Makina = IPAdresi;
                basariNot.ButunlemeMi = ogrNot.ButunlemeMi;
                basariNot.SilindiMi = false;
                basariNot.FKDersPlanAnaID = SeciliDers.DersPlanAnaID;
                basariNot.FKDegerlendirmeID = degerlendirmeID;
                if (yeniMi)
                {
                    db.ABSBasariNot.Add(basariNot);
                }
                sayac++;
                if (sayac % 50 == 0)
                {
                    db.SaveChanges();
                    db.Dispose();
                    db = new UYSv2Entities();
                }
            }
            db.SaveChanges();
        }

        public override bool PaylarDuzgunMu(List<IPay> payList, bool butunlemeMi)
        {
            throw new NotImplementedException();
        }

        public override void Sifirla(bool hesaplansinMi = true)
        {
            throw new NotImplementedException();
        }

        public override void SonHal()
        {
            if (!TarihUygunMu)
            {
                throw new DegerlendirmeException("Değerlendirme için geçersiz tarih aralığı!");
            }
            if (EnumDegerlendirmeAsama != EnumDegerlendirmeAsama.YAYINLANDI)
            {
                throw new DegerlendirmeException("Son hal işlemi için öncelikle değerlendirmeleri yayınlamalısınız!");
            }
            foreach (var item in DegerlendirmeKayit)
            {
                string barkodNo = string.Empty;
                Random Ramdom = new Random();
                int randomNumber = Ramdom.Next(10, 99);
                barkodNo = DateTime.Now.Year.ToString().Substring(2, 2) + DateTime.Now.Minute.ToString() + randomNumber.ToString() + item.FKDersPlanAnaID.ToString();
                item.BarkodNo = barkodNo;
            }
            data.SonHal(DegerlendirmeKayit, KullaniciAdi, IPAdresi);
            this.EnumDegerlendirmeAsama = EnumDegerlendirmeAsama.SONHAL;
            NotificationList.Add(new Notification("Son Hal", "Değerlendirme sonuçlarına son hal verildi!", Notification.MasterMesajTip.Basari));
        }

        public override void SonHalKaldir()
        {
            if (EnumDegerlendirmeAsama == EnumDegerlendirmeAsama.AKTARILDI)
            {
                throw new DegerlendirmeException("Öğrenci işlerine aktarılan değerlendirmeler geri alınamaz!");
            }
            data.SonHalKaldir(DegerlendirmeKayit);
            this.EnumDegerlendirmeAsama = EnumDegerlendirmeAsama.YAYINLANDI;
        }

        public override void YayindanKaldir()
        {
            if (EnumDegerlendirmeAsama == EnumDegerlendirmeAsama.AKTARILDI)
            {
                throw new DegerlendirmeException("Öğrenci işlerine aktarımı gerçekleştirilen değerlendirmeler değiştirilemez!");
            }
            if (EnumDegerlendirmeAsama == EnumDegerlendirmeAsama.SONHAL)
            {
                throw new DegerlendirmeException("Son hal verilen değerlendirmeler değiştirilemez!");
            }
            if (EnumDegerlendirmeAsama == EnumDegerlendirmeAsama.BOS)
            {
                throw new DegerlendirmeException("Yayından kaldırabilmek için önce yayınlanma yapılmalıdır!");
            }
            if (EnumDegerlendirmeAsama == EnumDegerlendirmeAsama.KAYITLI)
            {
                throw new DegerlendirmeException("Yayından kaldırabilmek için önce yayınlanma yapılmalıdır!");
            }

            foreach (var item in db.ABSYayinlananBasariNotlar.Where(x => x.FKDersPlanAnaID == SeciliDers.DersPlanAnaID && x.ButunlemeMi == ButunlemeMi && x.Yayinlandi == true))
            {
                item.Yayinlandi = false;
            }
            db.SaveChanges();

            this.EnumDegerlendirmeAsama = EnumDegerlendirmeAsama.KAYITLI;
            NotificationList.Add(new Notification("Yayından Kaldırıldı", "Değerlendirme sonuçları yayından kaldırıldı!", Notification.MasterMesajTip.Uyari));
        }

        public override void Yayinla()
        {
            if (EnumDegerlendirmeAsama == EnumDegerlendirmeAsama.AKTARILDI)
            {
                throw new DegerlendirmeException("Öğrenci işlerine aktarımı gerçekleştirilen değerlendirmeler değiştirilemez!");
            }
            if (EnumDegerlendirmeAsama == EnumDegerlendirmeAsama.SONHAL)
            {
                throw new DegerlendirmeException("Son hal verilen değerlendirmeler değiştirilemez!");
            }
            if (EnumDegerlendirmeAsama == EnumDegerlendirmeAsama.YAYINLANDI)
            {
                throw new DegerlendirmeException("Değişiklik yapabilmek için lütfen önce değerlendirmeleri yayından kaldırınız!");
            }
            if (EnumDegerlendirmeAsama == EnumDegerlendirmeAsama.BOS)
            {
                throw new DegerlendirmeException("Yayınlama yapabilmek için önce hesaplama yapılmalıdır!");
            }
            data.Yayinla(DegerlendirmeKayit);
            List<Tuple<int, IPay>> yayinlanmayanPayList;
            if (!TumNotlarYayinlandiMi(PayList.ConvertAll(x => (IPay)x), out yayinlanmayanPayList))
            {
                //Ders.DersRepository dersRepo = new Ders.DersRepository();

                foreach (var item in yayinlanmayanPayList)
                {
                    PayYayinla(item.Item1, item.Item2.PayID, SeciliDers.Yil, SeciliDers.Donem);
                }
                NotificationList.Add(new Notification("Yıliçi Notlar", "Yayınlanmayan yıliçi notlar bulundu ve otomatik yayınlandı!", Notification.MasterMesajTip.Bilgi));
            }
            this.EnumDegerlendirmeAsama = EnumDegerlendirmeAsama.YAYINLANDI;
            NotificationList.Add(new Notification("Yayınlandı", "Değerlendirme sonuçları başarıyla yayınlandı.", Notification.MasterMesajTip.Basari));
        }

        protected override EnumDegerlendirmeAsama AsamaBul(List<DegerlendirmeDTO> kayitliDegerlendirmeler)
        {
            EnumDegerlendirmeAsama asama = EnumDegerlendirmeAsama.BOS;

            if (kayitliDegerlendirmeler == null || !kayitliDegerlendirmeler.Any())
            {
                asama = EnumDegerlendirmeAsama.BOS;
            }
            else
            {
                asama = EnumDegerlendirmeAsama.KAYITLI;
                if (data.AktarildiMi(DegerlendirmeKayit))
                {
                    asama = EnumDegerlendirmeAsama.AKTARILDI;
                }
                else if (kayitliDegerlendirmeler.Count != 1)
                {
                    hataList.Add("Değerlendirme kaydı sayısı grup sayısı kadar olmalı!");
                    asama = EnumDegerlendirmeAsama.HATALI;
                }
                else
                {
                    int sonHalSayisi = kayitliDegerlendirmeler.Where(x => x.EnumSonHal == (int)EnumSonHal.EVET).Count();
                    if (sonHalSayisi == kayitliDegerlendirmeler.Count)
                    {
                        asama = EnumDegerlendirmeAsama.SONHAL;
                    }
                    else if (sonHalSayisi > 0 && sonHalSayisi < kayitliDegerlendirmeler.Count)
                    {
                        hataList.Add("Son hal sayısı değerlendirme kaydı sayısı kadar olmalı!");
                        asama = EnumDegerlendirmeAsama.HATALI;
                    }
                    else if (sonHalSayisi == 0 && TumBasariNotlarYayinlandiMi())
                    {
                        asama = EnumDegerlendirmeAsama.YAYINLANDI;
                    }
                }
            }
            return asama;
        }

        private bool TumBasariNotlarYayinlandiMi()
        {
            return db.ABSYayinlananBasariNotlar.Any(x => !x.FKDersGrupID.HasValue && x.FKDersPlanAnaID.HasValue && x.FKDersPlanAnaID.Value == SeciliDers.DersPlanAnaID && x.Yayinlandi && x.ButunlemeMi == ButunlemeMi && x.ABSDegerlendirme.SilindiMi == false);
        }

        protected override bool DegerlendirmeyeUygunMu()
        {
            bool result = true;
            if (!TumNotlarGirildiMi())
            {
                throw new DegerlendirmeException("Değerlendirme yapılabilmesi için dersin tüm notlarının girilmiş olması gerekmektedir!");
            }
            //if (ButunlemeMi && !AktarildiMiTumu())
            //{
            //    throw new DegerlendirmeException("Bütünleme değerlendirmesi yapılabilmesi için dersin tüm ortak gruplarının aktarılmış olması gerekmektedir!");
            //}
            List<Tuple<int, IPay>> yayinlanmayanPayList;
            if (!TumNotlarYayinlandiMi(PayList.ConvertAll(x => (IPay)x), out yayinlanmayanPayList))
            {
                //throw new Core.ViewModel.Degerlendirme.DegerlendirmeException("Değerlendirme yapılabilmesi için dersin tüm notlarının yayınlanmış olması gerekmektedir!");
                NotificationList.Add(new Notification("Yayınlanmayan Notlar", "Yayınlanmayan notlar mevcuttur. Lisans ve Önlisans Eğitim-Öğretim ve Sınav Yönetmeliğinin 16/7 maddesine göre; Öğretim elemanları, öğrencilerin başarılarının değerlendirmesine yönelik dönem içi bütün çalışma sonuçlarını, çalışmanın yapıldığı tarihten itibaren iki hafta içinde öğrencilere ilan etmekle yükümlüdürler.", Notification.MasterMesajTip.Uyari));
            }
            return result;
        }

        private bool AktarildiMiTumu()
        {
            var sorgu = db.OGRBarkodOkuma.Any(x => x.ABSDegerlendirme.SilindiMi == false && x.ABSDegerlendirme.FKDersPlanAnaID == SeciliDers.DersPlanAnaID && x.ABSDegerlendirme.Yil == Yil
            //&& (x.ABSDegerlendirme.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.EnumDersSureTip == (int)EnumDersSureTip.YILLIK || (x.ABSDegerlendirme.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.EnumDersSureTip == (int)EnumDersSureTip.DONEMLIK && x.ABSDegerlendirme.EnumDonem == Donem))
            //&& x.ABSDegerlendirme.ButunlemeMi == ButunlemeMi
            );
            return sorgu;
        }

        protected override void PayYayinla(int dersPlanAnaID, int payID, int yil, int donem)
        {
            try
            {
                using (UYSv2Entities db = new UYSv2Entities())
                {
                    var yayinBilgisi = db.ABSYayinlananPayNotlar.FirstOrDefault(d => d.FKDersPlanAnaID == dersPlanAnaID &&
                                            d.Yil == yil && (SeciliDers.EnumDersSureTip == EnumDersSureTip.YILLIK || (SeciliDers.EnumDersSureTip == EnumDersSureTip.DONEMLIK && d.EnumDonem == donem))
                                            && d.FKABSPayID == payID && d.Yayinlandi == true);

                    if (yayinBilgisi == null)
                    {
                        ABSYayinlananPayNotlar yayinlananNotPay = new ABSYayinlananPayNotlar();
                        yayinlananNotPay.FKDersGrupID = null;
                        yayinlananNotPay.FKDersPlanAnaID = dersPlanAnaID;
                        yayinlananNotPay.FKABSPayID = payID;
                        yayinlananNotPay.YayinlanmaTarihi = DateTime.Now;
                        yayinlananNotPay.Yayinlandi = true;
                        yayinlananNotPay.EnumDonem = donem;
                        yayinlananNotPay.Yil = yil;
                        db.ABSYayinlananPayNotlar.Add(yayinlananNotPay);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Pay yayınlama işleminde hata oluştu. " + ex.Message); ;
            }
        }

        protected override bool TumNotlarGirildiMi()
        {
            bool result = false;
            bool tipMi = DegerlendirmeKural != null && DegerlendirmeKural.YiliciDegerlendirTip;
            foreach (var ogrenci in TumNotlar)
            {
                foreach (var item in ogrenci.Value)
                {
                    if (!item.NotlarinTumuGirilmisMi(tipMi))
                    {
                        return false;
                    }
                }
            }
            result = true;
            return result;
        }

        protected override bool TumNotlarYayinlandiMi(List<IPay> payList, out List<Tuple<int, IPay>> yayinlanmayanPayList)
        {
            bool result = true;
            yayinlanmayanPayList = new List<Tuple<int, IPay>>();
            var paylar = payList.ConvertAll(x => (HPay)x);
            List<HPay> hPayList = new List<HPay>();
            foreach (var item in paylar)
            {
                hPayList.AddRange(item.GetirAltPaylarLineer());
            }
            var payIDList = hPayList.Select(x => x.PayID).ToList();
            var sorgu = db.ABSYayinlananPayNotlar.Where(x => x.Yil == Yil &&
                                                                (SeciliDers.EnumDersSureTip == EnumDersSureTip.YILLIK || (SeciliDers.EnumDersSureTip == EnumDersSureTip.DONEMLIK && x.EnumDonem == Donem)) &&
                                                                 payIDList.Contains(x.FKABSPayID) &&
                                                                 x.Yayinlandi &&
                                                                 x.FKDersPlanAnaID.HasValue
                                                            )
                                                            .Select(x => new { DersPlanAnaID = x.FKDersPlanAnaID.Value, PayID = x.FKABSPayID }).AsQueryable();
#if DEBUG
            var sql = sorgu.ToString();
#endif
            var yayinlanan = sorgu.ToList();

            foreach (var item in hPayList)
            {
                if (!yayinlanan.Any(x => x.PayID == item.PayID))
                {
                    result = false;
                    yayinlanmayanPayList.Add(new Tuple<int, IPay>(item.DersPlanAnaID, item));
                }
            }
            return result;
        }

        public BasariListesiDTO BasariListesiRapor()
        {
            BasariListesiDTO result = new BasariListesiDTO();
            List<OgrenciPayveNotBilgileriDTO> resultNot = GetirBasariNotListesi(PersonelID, ButunlemeMi, SeciliDers.DersPlanAnaID, Yil, Donem);
            GetirDegerlendirme();
            var degerlendirme = DegerlendirmeKayit.FirstOrDefault();
            if (degerlendirme == null)
            {
                throw new DegerlendirmeException("Değerlendirme bilgisi veritabanında bulunamadığı için rapor oluşturulamadı!");
            }
            if (degerlendirme.EnumSonHal == (int)EnumSonHal.EVET)
            {
                result.SonHalTarihi = degerlendirme.Tarih.Value;
                result.StandartSapma = degerlendirme.StandartSapma.Value;
                result.AritmetikOrtalama = degerlendirme.Ortalama.Value;
                result.Barkod = degerlendirme.BarkodNo;
                result.EBYSDocTreeID = degerlendirme.EBYSDocTreeID;
            }
            //Kopyalama
            result.Ders = new DersDTO();
            result.Ders.ButunlemeMi = SeciliDers.ButunlemeMi;
            result.Ders.ButunlemeMiOrijinal = SeciliDers.ButunlemeMiOrijinal;
            result.Ders.DersAd = SeciliDers.DersAd;
            result.Ders.DersGrupAd = SeciliDers.DersGrupAd;
            result.Ders.DersGrupID = SeciliDers.DersGrupID;
            result.Ders.DersID = SeciliDers.DersID;
            result.Ders.DersKod = SeciliDers.DersKod;
            result.Ders.DersPlanAnaID = SeciliDers.DersPlanAnaID;
            result.Ders.DersPlanID = SeciliDers.DersPlanID;
            result.Ders.Donem = SeciliDers.Donem;
            result.Ders.DSaat = SeciliDers.DSaat;
            result.Ders.EnumDersPlanOrtalama = SeciliDers.EnumDersPlanOrtalama;
            result.Ders.EnumWebGosterim = SeciliDers.EnumWebGosterim;
            result.Ders.EnumZorunluDers = SeciliDers.EnumZorunluDers;
            result.Ders.Fakulte = SeciliDers.Fakulte;
            result.Ders.FinalTarihi = SeciliDers.FinalTarihi;
            result.Ders.GrupCalismaOgrenciIDleri = SeciliDers.GrupCalismaOgrenciIDleri;
            result.Ders.Kredi = SeciliDers.Kredi;
            result.Ders.OgrenciSayisi = SeciliDers.OgrenciSayisi;
            result.Ders.OgretimTuru = SeciliDers.OgretimTuru;
            result.Ders.OgretimTuruAd = SeciliDers.OgretimTuruAd;
            result.Ders.Personel = SeciliDers.Personel;
            result.Ders.SonHal = SeciliDers.SonHal;
            result.Ders.USaat = SeciliDers.USaat;
            result.Ders.Yariyil = SeciliDers.Yariyil;
            result.Ders.Yil = SeciliDers.Yil;
            //Kopyalama bitiş
            result.Ders.DersGrupAd = "A";
            result.Ders.DersGrupID = SeciliDers.DersPlanAnaID;
            result.Ders.OgretimTuru = (int)EnumOgretimTur.OGRETIM_1;
            result.Ders.OgretimTuruAd = Sabis.Enum.Utils.GetEnumDescription(EnumOgretimTur.OGRETIM_1);
            result.Bolum = SeciliDers.Bolum;
            result.Fakulte = SeciliDers.Fakulte;
            result.NotList = resultNot;
            return result;
        }

        private List<OgrenciPayveNotBilgileriDTO> GetirBasariNotListesi(int personelID, bool butunlemeMi, int FKDersPlanAnaID, int yil, int donem)
        {
            List<OgrenciPayveNotBilgileriDTO> result = null;
            db.Database.CommandTimeout = 600;
            var basariNotSQL = db.ABSBasariNot.Where(x => x.ButunlemeMi == butunlemeMi && x.SilindiMi == false && x.FKDersPlanAnaID == FKDersPlanAnaID && x.Yil == yil && x.EnumDonem == donem).AsQueryable();
            var resultSQL = from basariNot in basariNotSQL
                            select new
                            {
                                Numara = basariNot.OGRKimlik.Numara,
                                AdSoyad = basariNot.OGRKimlik.Kisi.Ad + " " + basariNot.OGRKimlik.Kisi.Soyad,
                                OgrenciID = basariNot.OGRKimlik.ID,
                                DersPlanAnaID = basariNot.OGRDersPlanAna.ID,
                                PersonelID = personelID,
                                BasariNot = basariNot,
                                YazilmaID = basariNot.ID
                            };
            result = new List<OgrenciPayveNotBilgileriDTO>();
            var resultTMP = resultSQL.ToList();
            foreach (var item in resultTMP)
            {
                OgrenciPayveNotBilgileriDTO kayit = new OgrenciPayveNotBilgileriDTO();
                kayit.AdSoyad = item.AdSoyad;
                kayit.BasariHarf = (Sabis.Enum.EnumHarfBasari)item.BasariNot.EnumHarfBasari;
                kayit.MutlakHarf = (Sabis.Enum.EnumHarfMutlak)item.BasariNot.EnumHarfMutlak;
                kayit.DersGrupID = null;
                kayit.DersPlanAnaID = item.DersPlanAnaID;
                kayit.DersPlanID = null;
                kayit.MutlakNot = item.BasariNot.Mutlak.Value;
                kayit.Numara = item.Numara;
                kayit.OgrenciID = item.OgrenciID;
                kayit.PersonelID = item.PersonelID;
                kayit.listPaylarVeNotlar = new List<PaylarVeNotlarDTO>();
                result.Add(kayit);
            }
            return result;
        }

        public void Dispose()
        {
            db.Dispose();
            db = null;
        }
    }
}
