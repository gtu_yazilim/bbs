﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sabis.Enum;
using Sabis.Core.DTO;
using Sabis.Bolum.Core.ABS.DEGERLENDIRME;
using Sabis.Bolum.Bll.DATA.UysModel;


namespace Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME
{
    public abstract class AbstractDegerlendirme
    {
        protected Data data = new Data();
        public EnumDegerlendirmeAsama EnumDegerlendirmeAsama { get; set; }
        public List<IPay> PayList { get; set; }
        public string KullaniciAdi { get; set; }
        public string IPAdresi { get; set; }
        public string SimuleEden { get; set; }
        public int PersonelID { get; set; }
        public List<DegerlendirmeDTO> DegerlendirmeKayit { get; set; }
        protected List<string> hataList = new List<string>();
        public CiktiDTO DegerlendirmeSonuc { get; set; }
        public bool ButunlemeMi { get; set; }
        /// <summary>
        /// Bu kullanıcı belirlenen tarihler dışında giriş yapabilir mi?
        /// </summary>
        public bool TarihYetkisiVarMi { get; set; }
        /// <summary>
        /// Son hali geri alma yetkisi var mı?
        /// </summary>
        public bool SonHalKaldirmaYetkisiVarMi { get; set; }
        public DersDTO SeciliDers { get; set; }
        public List<Notification> NotificationList { get; set; }
        public bool TarihUygunMu { get; set; }
        public AkademikTakvim AkademikTakvim { get; protected set; }
        public DegerlendirmeKural DegerlendirmeKural { get; set; }
        public bool DegerlendiriciMi { get; set; }

        protected void Init(string kullaniciAdi, int personelID, string IP, string simuleEden)
        {
            NotificationList = new List<Notification>();

            //Ön şartlar uygun mu?
            DegerlendirmeyeGirerMi(SeciliDers);
            

            ButunlemeMi = SeciliDers.ButunlemeMi;
            //IP Bilgilerini Al
            IPAdresi = IP;
            //işlemi yapan kullanıcının yetkilerini sorgula
            KullaniciAdi = kullaniciAdi;
            PersonelID = personelID;
            SimuleEden = simuleEden;


            bool tarihYetkisiVarMi;
            bool sonHalKaldirmaYetkisiVarMi;
            data.GetirKullaniciYetkileri(kullaniciAdi, IPAdresi, out tarihYetkisiVarMi, out sonHalKaldirmaYetkisiVarMi);
            TarihYetkisiVarMi = tarihYetkisiVarMi;
            SonHalKaldirmaYetkisiVarMi = sonHalKaldirmaYetkisiVarMi;

            //Tarih müsait mi?
            var tarihTipi = EnumAkademikTarih.DEGERLENDIRME;
            if (ButunlemeMi)
            {
                tarihTipi = EnumAkademikTarih.DEGERLENDIRME_BUTUNLEME;
            }
            int birimID = 21398;
            //if (SeciliDers.Bolum == null || SeciliDers.Bolum.ID == 0)
            //{
            //    birimID = SeciliDers.Fakulte.ID;
            //}
            //else
            //{
            //    birimID = SeciliDers.Bolum.ID;
            //}
            int donem = 1;
            if (SeciliDers.Donem > 0)
            {
                donem = SeciliDers.Donem;
            }
            this.AkademikTakvim = Sabis.Client.AkademikTakvimIslem.GetirTarih(tarihTipi, SeciliDers.Yil, donem, birimID);
            TarihUygunMu = TarihMusaitMiYetkisiVarMi(ButunlemeMi, SeciliDers, TarihYetkisiVarMi);
#if DEBUG
            //TODO sil
           // TarihUygunMu = true;
#endif
            GetirKural();
        }

        protected void GetirKural()
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                DegerlendirmeKural = db.ABSDegerlendirmeKural.Where(x => x.FKDersPlanAnaID == SeciliDers.DersPlanAnaID).OrderByDescending(x => x.Yil).ThenByDescending(x => x.EnumDonem).Select(x => new DegerlendirmeKural { ID = x.ID, EnumDonem = x.EnumDonem, Yil = x.Yil, TarihKayit = x.TarihKayit, FKDersPlanAnaID = x.FKDersPlanAnaID, Kullanici = x.Kullanici, MinFinal = x.MinFinal, MinOrtalama = x.MinOrtalama, MinYilIci = x.MinYilIci, MinKurulOrtalamaTip = x.MinKurulOrtalamaTip, MinYilIciOrtalamaTip = x.MinYilIciOrtalamaTip, YiliciDegerlendirTip = x.YiliciDegerlendirTip }).FirstOrDefault();
            }
        }

        protected void HiyerarsikDersKontrol(bool hiyerarsik)
        {
            if (SeciliDers.Hiyerarsik && !hiyerarsik)
            {
                throw new Exception("Hiyerarşik ders değerlendirlemeri bu ekrandan yapılamaz!");
            }
            else if (!SeciliDers.Hiyerarsik && hiyerarsik)
            {
                throw new Exception("Hiyerarşik ders değerlendirlemeri bu ekrandan yapılamaz!");
            }
        }
        private bool TarihMusaitMiYetkisiVarMi(bool butunlemeMi, DersDTO seciliDers, bool tarihYetkisiVarMi)
        {
            bool result = true;
            if (tarihYetkisiVarMi)
            {
                result = true;
            }
            else
            {
                var tarihTipi = EnumAkademikTarih.DEGERLENDIRME;
                if (butunlemeMi)
                {
                    tarihTipi = EnumAkademikTarih.DEGERLENDIRME_BUTUNLEME;
                }
                int birimID = 21398;
                //if (seciliDers.Bolum == null || seciliDers.Bolum.ID == 0)
                //{
                //    birimID = seciliDers.Fakulte.ID;
                //}
                //else
                //{
                //    birimID = seciliDers.Bolum.ID;
                //}


                try
                {
                    if (!Sabis.Client.AkademikTakvimIslem.TarihKontrolu(tarihTipi, seciliDers.Yil, seciliDers.Donem, birimID))
                    {
                        result = false;
                        NotificationList.Add(new Notification("Tarih", "Seçili ders için belirlenen değerlendirme tarihleri aralığında değilsiniz!", Notification.MasterMesajTip.Uyari));
                    }
                }
                catch (Exception e)
                {
                    NotificationList.Add(new Notification("Tarih", "Değerlendirme tarihleri sisteme işlenmedi", Notification.MasterMesajTip.Hata));
                    //throw new Exception("Değerlendirme tarihleri sisteme işlenmedi");
                }


            }
            return result;
        }
        private void DegerlendirmeyeGirerMi(DersDTO seciliDers)
        {
            //if (seciliDers.EnumWebGosterim == 3)
            //{
            //    throw new Core.ViewModel.Degerlendirme.DegerlendirmeException("Sadece başarı not girişi yapılabilir!");
            //}
#if DEBUG
#else
            if (seciliDers.Fakulte.ID == 15 && !(SimuleEden == "iyatik" || SimuleEden == "kaynak"))
            {
                throw new Sabis.Bolum.Core.ABS.DEGERLENDIRME.DegerlendirmeException("ADAMYO için değerlendirme yapamazsınız!");
            }
#endif
        }

        protected virtual DersDTO GetirDersDTO(int grupID, string dil = "tr")
        {
            DersDTO result = null;
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var koordinatorVar = db.OGRDersGrupHoca.Any(x => x.FKDersGrupID == grupID && x.EnumDersHocaUnvan == (int)EnumDersHocaUnvan.KOORDINATOR && x.FKPersonelID.HasValue);
                var sorgu = db.OGRDersGrupHoca.AsQueryable();
                if (koordinatorVar)
                {
                    sorgu = sorgu.Where(x => (x.EnumDersHocaUnvan == (int)EnumDersHocaUnvan.KOORDINATOR));
                }
                var hocaDersSorgu = (from veri in sorgu
                                     where veri.FKDersGrupID == grupID && veri.FKPersonelID.HasValue
                                     select new
                                     {
                                         DersGrupHocaID = veri.ID,
                                         DersKod = veri.OGRDersGrup.OGRDersPlan.BolKodAd + veri.OGRDersGrup.OGRDersPlan.DersKod,
                                         DersAd = (dil == "en" ? veri.OGRDersGrup.OGRDersPlanAna.YabDersAd : veri.OGRDersGrup.OGRDersPlanAna.DersAd),
                                         OgretimTuru = veri.OGRDersGrup.EnumOgretimTur,
                                         Donem = veri.OGRDersGrup.EnumDonem.Value,
                                         BolumKod = veri.OGRDersGrup.OGRDersPlan.BolKodAd,
                                         OgrenciSayisi = veri.OGRDersGrup.OGROgrenciYazilma.Where(x => x.Iptal == false && x.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL).Count(),
                                         OgrenciSayisiButunleme = veri.OGRDersGrup.OGROgrenciYazilma.Where(x => x.Iptal == false && (x.ButunlemeMi.HasValue && x.ButunlemeMi.Value) && x.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL).Count(),
                                         DersGrupAd = veri.OGRDersGrup.GrupAd,
                                         DersGrupID = veri.OGRDersGrup.ID,
                                         DersPlanID = veri.OGRDersGrup.FKDersPlanID,
                                         DersPlanAnaID = veri.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.ID,
                                         Yil = veri.OGRDersGrup.OgretimYili.Value,
                                         Kredi = veri.OGRDersGrup.OGRDersPlanAna.Kredi,
                                         EnumDersPlanZorunlu = veri.OGRDersGrup.OGRDersPlan.EnumDersPlanZorunlu,
                                         FakulteID = veri.OGRDersGrup.OGRDersPlan.FKFakulteBirimID,
                                         BolumID = veri.OGRDersGrup.OGRDersPlan.FKBolumPrBirimID,
                                         DSaat = veri.OGRDersGrup.OGRDersPlanAna.DSaat,
                                         USaat = veri.OGRDersGrup.OGRDersPlanAna.USaat,
                                         YariYil = veri.OGRDersGrup.OGRDersPlan.Yariyil,
                                         FinalTarihi = veri.OGRDersGrup.FinalTarihi,
                                         EnumDersPlanOrtalama = veri.OGRDersGrup.OGRDersPlan.EnumDersPlanOrtalama,
                                         ButunlemeMi = veri.OGRDersGrup.OGROgrenciYazilma.Any(x => x.Iptal == false && (x.ButunlemeMi.HasValue && x.ButunlemeMi.Value == true) && x.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL),
                                         EnumSeviye = veri.OGRDersGrup.OGRDersPlan.Birimler.EnumSeviye,
                                         EnumOgrenimTur = veri.OGRDersGrup.OGRDersPlan.Birimler.EnumOgrenimTuru,
                                         Personel = veri.PersonelBilgisi,
                                         Hiyerarsik = veri.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.Hiyerarsik,
                                         EnumKokDersTipi = veri.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.EnumKokDersTipi,
                                         EnumDersSureTip = veri.OGRDersGrup.OGRDersPlan.EnumDersSureTip,
                                         EnumDegerlendirmeTipi = veri.OGRDersGrup.OGRDersPlan.EnumDegerlendirmeTipi
                                     });

                var item = hocaDersSorgu.FirstOrDefault();

                if (item == null)
                {
                    return null;
                }

                bool butunlemeMi = item.ButunlemeMi;
                var degerlendirme = db.ABSDegerlendirme.FirstOrDefault(x => x.FKDersGrupID == item.DersGrupID && x.SilindiMi == false && x.ButunlemeMi == butunlemeMi);
                EnumSonHal sonHal = EnumSonHal.BELIRSIZ;

                if (degerlendirme == null)
                {
                    sonHal = EnumSonHal.HAYIR;
                }
                else if (degerlendirme.EnumSonHal == (int)EnumSonHal.EVET)
                {
                    sonHal = EnumSonHal.EVET;
                }
                else
                {
                    sonHal = EnumSonHal.HAYIR;
                }

                result = new DersDTO();
                result.ButunlemeMi = item.ButunlemeMi;
                result.ButunlemeMiOrijinal = item.ButunlemeMi;
                if (result.ButunlemeMiOrijinal)
                {
                    //Bütünleme notu girilmiş ise bütünleme modunu aktifleştir.
                    //result.ButunlemeMi = db.ABSOgrenciNot.Any(x => x.FKDersGrupID == item.DersGrupID && x.ABSPaylar.EnumCalismaTip == (int)EnumCalismaTip.Butunleme && x.Silindi == false);
                }

                if (item.FakulteID == null && item.BolumID == null)
                {
                    result.Bolum = new Sabis.Core.DtoBirimler();
                    result.Bolum.BirimAdi = "ÜNİVERSİTE ORTAK SEÇMELİ";
                    result.Bolum.BasilanBirimAdi = "ÜNİVERSİTE ORTAK SEÇMELİ";
                }
                else if (item.FakulteID != null && item.BolumID == null)
                {
                    result.Bolum = new Sabis.Core.DtoBirimler();
                    result.Bolum.BirimAdi = "FAKÜLTE ORTAK SEÇMELİ";
                    result.Bolum.BasilanBirimAdi = "FAKÜLTE ORTAK SEÇMELİ";
                }
                else
                {
                    result.Bolum = Sabis.Client.BirimIslem.Instance.getirBirimbyID(item.BolumID.Value);
                }

                if (item.FakulteID.HasValue)
                {
                    result.Fakulte = Sabis.Client.BirimIslem.Instance.getirBirimbyID(item.FakulteID.Value);
                }
                else
                {
                    result.Fakulte = new Sabis.Core.DtoBirimler();
                    result.Fakulte.BirimAdi = "ÜNİVERSİTE ORTAK SEÇMELİ";
                    result.Fakulte.BasilanBirimAdi = "ÜNİVERSİTE ORTAK SEÇMELİ";
                }

                result.DersGrupAd = item.DersGrupAd;
                result.EnumZorunluDers = item.EnumDersPlanZorunlu;
                result.Yariyil = item.YariYil;
                result.DersKod = item.DersKod;
                result.DersAd = item.DersAd;
                result.DersPlanAnaID = item.DersPlanAnaID;
                result.Donem = item.Donem;
                result.OgretimTuru = item.OgretimTuru;
                result.OgretimTuruAd = Sabis.Enum.Utils.GetEnumDescription((EnumOgretimTur)item.OgretimTuru);
                result.Yil = item.Yil;
                result.OgrenciSayisi = item.OgrenciSayisi;
                result.OgrenciSayisiButunleme = item.OgrenciSayisiButunleme;
                result.Kredi = item.Kredi;
                result.DSaat = item.DSaat;
                result.USaat = item.USaat;
                result.DersGrupID = item.DersGrupID;
                result.DersPlanID = item.DersPlanID;
                result.FinalTarihi = item.FinalTarihi;
                result.SonHal = sonHal;
                result.EnumKokDersTipi = item.EnumKokDersTipi;
                result.EnumDegerlendirmeTipi = (Sabis.Enum.EnumDegerlendirmeTipi)item.EnumDegerlendirmeTipi;
                if (item.EnumDersPlanOrtalama.HasValue)
                {
                    result.EnumDersPlanOrtalama = (EnumDersPlanOrtalama)item.EnumDersPlanOrtalama.Value;
                }
                else
                {
                    result.EnumDersPlanOrtalama = EnumDersPlanOrtalama.BELIRSIZ;
                }
                result.EnumSeviye = item.EnumSeviye;
                result.EnumOgrenimTur = item.EnumOgrenimTur;
                //result.EnumDersSureTip = item.EnumDersSureTip;

                if (item.Personel != null)
                {
                    result.Personel = new PersonelDTO
                    {
                        Ad = item.Personel.Kisi.Ad,
                        Soyad = item.Personel.Kisi.Soyad,
                        UnvanAd = item.Personel.UnvanAd
                    };
                }
            }
            return result;
        }

        public abstract bool PaylarDuzgunMu(List<IPay> payList, bool butunlemeMi);
        protected abstract bool TumNotlarGirildiMi();
        protected abstract bool TumNotlarYayinlandiMi(List<IPay> payList, out List<Tuple<int, IPay>> yayinlanmayanPayList);
        protected abstract bool DegerlendirmeyeUygunMu();

        protected abstract EnumDegerlendirmeAsama AsamaBul(List<DegerlendirmeDTO> kayitliDegerlendirmeler);
        public abstract void Degerlendir(double? _ustDeger, double? _bagilAritmetikOrtalama = null);
        public abstract void Kaydet();
        public abstract void Yayinla();
        public abstract void YayindanKaldir();
        public abstract void SonHal();
        public abstract void SonHalKaldir();
        public abstract void Sifirla(bool hesaplansinMi = true);
        protected abstract void PayYayinla(int ID, int payID, int yil, int donem);
    }
}
