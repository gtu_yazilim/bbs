﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AutoMapper;
using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Enum;
using Sabis.Bolum.Core.ABS.DEGERLENDIRME;


namespace Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME
{
    public class BasariNotYonetimData
    {
        private static bool AutoMapperConfigured;
        public BasariNotYonetimData()
        {
            if (!AutoMapperConfigured)
            {
                var config = new MapperConfiguration(cfg => cfg.CreateMap<ABSDegerlendirme, DegerlendirmeDTO>());
                var mapper = config.CreateMapper();
                //Mapper.CreateMap<ABSDegerlendirme, DegerlendirmeDTO>(); //yeni sürümde çalışmıyor
                AutoMapperConfigured = true;
            }
        }

        private UYSv2Entities context = new UYSv2Entities();

        public List<OgrenciPayveNotBilgileriDTO> GetirBasariNotlar(DersDTO seciliDers, int? personelID, bool bolumBaskaniMi)
        {
            List<OgrenciPayveNotBilgileriDTO> result = null;
            // IEnumerable<int> grupCalismasi = context.OGRDersGrup.FirstOrDefault(d => d.ID == seciliDers.DersGrupID).ABSGrupCalisma.Where(d => d.FK_PersonelID == personelID).Select(x => x.FK_OgrenciID);
            var sqlRAW = context.OGROgrenciYazilma.AsQueryable();
            if (seciliDers.ButunlemeMiOrijinal)
            {
                sqlRAW = sqlRAW.Where(x => x.ButunlemeMi.HasValue && x.ButunlemeMi.Value == true);
            }
            var resultSQL = from yazilma in sqlRAW
                            where yazilma.FKDersGrupID == seciliDers.DersGrupID && yazilma.Yil == seciliDers.Yil && yazilma.EnumDonem == seciliDers.Donem && yazilma.Iptal == false && yazilma.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL
                            orderby yazilma.OGRKimlik.Numara
                            select new
                            {
                                Numara = yazilma.OGRKimlik.Numara,
                                AdSoyad = yazilma.OGRKimlik.Kisi.Ad + " " + yazilma.OGRKimlik.Kisi.Soyad,
                                OgrenciID = yazilma.OGRKimlik.ID,
                                DersPlanAnaID = yazilma.OGRDersPlanAna.ID,
                                DersGrupID = yazilma.FKDersGrupID,
                                DersPlanID = yazilma.FKDersPlanID,
                                PersonelID = personelID,
                                BasariNot = yazilma.OGRDersGrup.ABSBasariNot.FirstOrDefault(x => x.FKOgrenciID == yazilma.FKOgrenciID && x.ButunlemeMi == seciliDers.ButunlemeMiOrijinal),
                                YazilmaID = yazilma.ID
                            };

            //if (bolumBaskaniMi == false)
            //{
            //    resultSQL = resultSQL.Where(d => d.OgrenciID);
            //}
            result = new List<OgrenciPayveNotBilgileriDTO>();
            foreach (var item in resultSQL)
            {
                OgrenciPayveNotBilgileriDTO kayit = new OgrenciPayveNotBilgileriDTO();
                kayit.AdSoyad = item.AdSoyad;
                if (item.BasariNot == null)
                {
                    kayit.BasariHarf = EnumHarfBasari.BELIRSIZ;
                    kayit.MutlakHarf = EnumHarfMutlak.BELIRSIZ;
                }
                else
                {
                    kayit.BasariHarf = (EnumHarfBasari)item.BasariNot.EnumHarfBasari;
                    kayit.MutlakHarf = (EnumHarfMutlak)item.BasariNot.EnumHarfMutlak;
                }
                kayit.DersGrupID = item.DersGrupID;
                kayit.DersPlanAnaID = item.DersPlanAnaID;
                kayit.DersPlanID = item.DersPlanID;

                kayit.Numara = item.Numara;
                kayit.OgrenciID = item.OgrenciID;
                kayit.PersonelID = item.PersonelID.Value;
                result.Add(kayit);
            }
            return result;
        }


        public bool KoordinatorMu(int grupID, int personelID)
        {
            bool result = false;
            result = context.OGRDersGrupHoca.Any(x => x.FKDersGrupID == grupID && x.FKPersonelID == personelID && x.EnumDersHocaUnvan == (int)EnumDersHocaUnvan.KOORDINATOR);

            int enumKokDersTip = context.OGRDersGrup.FirstOrDefault(x => x.ID == grupID).OGRDersPlanAna.EnumKokDersTipi;


            if (enumKokDersTip == (int)EnumKokDersTipi.BITIRME || enumKokDersTip == (int)EnumKokDersTipi.SEMINER || enumKokDersTip == (int)EnumKokDersTipi.UZMANLIKALANI || enumKokDersTip ==(int)EnumKokDersTipi.ISYERI_UYGULAMASI || enumKokDersTip == (int)EnumKokDersTipi.ISYERI_EGITIMI)
            {
                result = true;
            }

            return result;
        }

        public List<OgrenciPayveNotBilgileriDTO> GetirBasariNotListesi(int personelID, bool butunlemeMi, int? grupID)
        {
            List<OgrenciPayveNotBilgileriDTO> result = null;
            var basariNotSQL = context.ABSBasariNot.Where(x => x.ButunlemeMi == butunlemeMi && x.SilindiMi == false).AsQueryable();

            basariNotSQL = basariNotSQL.Where(x => x.FKDersGrupID == grupID.Value);

            basariNotSQL = basariNotSQL.Where(x=> context.OGROgrenciYazilma.Any(y=> y.FKDersGrupID == x.FKDersGrupID && y.FKOgrenciID == x.FKOgrenciID && y.Iptal == false));

            var resultSQL = from basariNot in basariNotSQL
                            select new
                            {
                                Numara = basariNot.OGRKimlik.Numara,
                                AdSoyad = basariNot.OGRKimlik.Kisi.Ad + " " + basariNot.OGRKimlik.Kisi.Soyad,
                                OgrenciID = basariNot.OGRKimlik.ID,
                                DersPlanAnaID = basariNot.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.ID,
                                DersGrupID = basariNot.FKDersGrupID,
                                DersPlanID = basariNot.OGRDersGrup.FKDersPlanID,
                                PersonelID = personelID,
                                Notlar = basariNot.OGRDersGrup.ABSOgrenciNot.Where(v=> v.Silindi == false).OrderBy(d => d.ID),
                                BasariNot = basariNot.OGRDersGrup.ABSBasariNot.FirstOrDefault(x => x.FKOgrenciID == basariNot.FKOgrenciID && x.ButunlemeMi == butunlemeMi),
                                YazilmaID = basariNot.ID
                            };
            result = new List<OgrenciPayveNotBilgileriDTO>();
            foreach (var item in resultSQL)
            {
                OgrenciPayveNotBilgileriDTO kayit = new OgrenciPayveNotBilgileriDTO();
                kayit.AdSoyad = item.AdSoyad;
                kayit.BasariHarf = (EnumHarfBasari)item.BasariNot.EnumHarfBasari;
                kayit.MutlakHarf = (EnumHarfMutlak)item.BasariNot.EnumHarfMutlak;
                kayit.DersGrupID = item.DersGrupID;
                kayit.DersPlanAnaID = item.DersPlanAnaID;
                kayit.DersPlanID = item.DersPlanID;
                kayit.MutlakNot = item.BasariNot.Mutlak.Value;
                kayit.Numara = item.Numara;
                kayit.OgrenciID = item.OgrenciID;
                kayit.PersonelID = item.PersonelID;
                result.Add(kayit);
            }
            return result;
        }

        public List<DegerlendirmeDTO> getirDegerlendirme(int dersGrupID, bool butunlemeMi)
        {
            List<DegerlendirmeDTO> result;
            context.Set<ABSDegerlendirme>().AsNoTracking();
            var degerlendirmeKayit = context.ABSDegerlendirme.Where(x => x.FKDersGrupID.Value == dersGrupID && x.ButunlemeMi == butunlemeMi && x.SilindiMi == false).OrderByDescending(x => x.ID).ToList();
            Mapper.Initialize(cfg => cfg.CreateMap<ABSDegerlendirme, DegerlendirmeDTO>());
            result = Mapper.Map<List<DegerlendirmeDTO>>(degerlendirmeKayit);

            //var config = new MapperConfiguration(cfg => cfg.CreateMap<ABSDegerlendirme, DegerlendirmeDTO>());
            //var mapper = new Mapper(config);
            //DegerlendirmeDTO dto = mapper.Map<DegerlendirmeDTO>(ABSDegerlendirme);
            return result;
        }

        public bool AktarildiMi(List<DegerlendirmeDTO> degerlendirmeList)
        {
            bool result = false;
            foreach (DegerlendirmeDTO item in degerlendirmeList)
            {
                result = context.OGRBarkodOkuma.Any(x => x.FKABSDegerlendirme == item.ID);
                if (result)
                {
                    break;
                }
            }
            return result;
        }

        public bool TumBasariNotlarYayinlandiMi(List<int> dersGrupIDList, bool butunlemeMi)
        {
            return context.ABSYayinlananBasariNotlar.Any(x => x.FKDersGrupID.HasValue && dersGrupIDList.Contains(x.FKDersGrupID.Value) && x.Yayinlandi && x.ButunlemeMi == butunlemeMi && x.ABSDegerlendirme.SilindiMi == false);
        }

        public void SonHal(List<DegerlendirmeDTO> DegerlendirmeKayit, string KullaniciAdi, string IPAdresi)
        {
            foreach (var item in DegerlendirmeKayit)
            {
                ABSDegerlendirme kayit = context.ABSDegerlendirme.First(x => x.ID == item.ID);
                kayit.EnumSonHal = (int)EnumSonHal.EVET;
                kayit.Kullanici = KullaniciAdi;
                kayit.Makina = IPAdresi;
                kayit.BarkodNo = item.BarkodNo;
                kayit.Yil = item.Yil;
                kayit.EnumDonem = item.EnumDonem;
                kayit.TarihSonHal = DateTime.Now;
            }
            context.SaveChanges();
        }

        public void GetirKullaniciYetkileri(string kullaniciAdi, string IPAdresi, out bool tarihYetkisiVarMi, out bool sonHalKaldirmaYetkisiVarMi)
        {
            ABSIPYetki yetki = context.ABSIPYetki.FirstOrDefault(x => x.IPAdresi == IPAdresi || x.KullaniciAdi == kullaniciAdi);
            tarihYetkisiVarMi = false;
            sonHalKaldirmaYetkisiVarMi = false;
            if (yetki != null)
            {
                tarihYetkisiVarMi = true;
                if (yetki.SonHalKaldirmaVarMi)
                {
                    sonHalKaldirmaYetkisiVarMi = true;
                }
            }
        }

        public void Yayinla(List<DegerlendirmeDTO> DegerlendirmeKayit)
        {
            foreach (var degerlendirme in DegerlendirmeKayit)
            {
                bool yeniMi = false;
                ABSYayinlananBasariNotlar kayit = context.ABSYayinlananBasariNotlar.FirstOrDefault(x => x.FKDersGrupID == degerlendirme.FKDersGrupID && x.ButunlemeMi == degerlendirme.ButunlemeMi && x.ABSDegerlendirme.SilindiMi == false);
                if (kayit == null)
                {
                    kayit = new ABSYayinlananBasariNotlar();
                    yeniMi = true;
                }
                kayit.ButunlemeMi = degerlendirme.ButunlemeMi;
                kayit.FKABSDegerlendirme = degerlendirme.ID;
                kayit.FKDersGrupID = degerlendirme.FKDersGrupID.Value;
                kayit.Yayinlandi = true;
                kayit.YayinlanmaTarihi = DateTime.Now;
                kayit.Yil = degerlendirme.Yil;
                kayit.EnumDonem = degerlendirme.EnumDonem;
                if (yeniMi)
                {
                    context.ABSYayinlananBasariNotlar.Add(kayit);
                }
            }
            context.SaveChanges();
        }

        public void YayindanKaldir(List<int> grupIDList, bool ButunlemeMi)
        {
            foreach (var item in context.ABSYayinlananBasariNotlar.Where(x => x.FKDersGrupID.HasValue && grupIDList.Contains(x.FKDersGrupID.Value) && x.ButunlemeMi == ButunlemeMi && x.Yayinlandi == true))
            {
                item.Yayinlandi = false;
            }
            context.SaveChanges();
        }

        public void SonHalKaldir(List<DegerlendirmeDTO> DegerlendirmeKayit)
        {
            foreach (var item in DegerlendirmeKayit)
            {
                ABSDegerlendirme kayit = context.ABSDegerlendirme.First(x => x.ID == item.ID);
                kayit.EnumSonHal = (int)EnumSonHal.HAYIR;
                kayit.BarkodNo = null;
                kayit.TarihSonHal = null;
            }
            context.SaveChanges();
        }

        public void KaydetDegerlendirme(List<DegerlendirmeDTO> DegerlendirmeKayit)
        {
            foreach (var item in DegerlendirmeKayit)
            {
                bool yeniMi = false;
                ABSDegerlendirme kayit = context.ABSDegerlendirme.FirstOrDefault(x => x.FKDersGrupID == item.FKDersGrupID && x.ButunlemeMi == item.ButunlemeMi && x.SilindiMi == false);
                if (kayit == null)
                {
                    yeniMi = true;
                    kayit = new ABSDegerlendirme();
                }
                kayit.ButunlemeMi = item.ButunlemeMi;
                kayit.BagilOrtalama = item.BagilOrtalama;
                kayit.BarkodNo = item.BarkodNo;
                kayit.EnBuyukNot = item.EnBuyukNot;
                kayit.EnumDonem = item.EnumDonem;
                kayit.EnumSonHal = item.EnumSonHal;
                kayit.FKDersGrupID = item.FKDersGrupID;
                kayit.FKPersonelID = item.FKPersonelID;
                kayit.Kullanici = item.Kullanici;
                kayit.Makina = item.Makina;
                kayit.OrtakDegerlendirmeGUID = item.OrtakDegerlendirmeGUID;
                kayit.Ortalama = item.Ortalama;
                kayit.StandartSapma = item.StandartSapma;
                kayit.Tarih = item.Tarih;
                kayit.UstDeger = item.UstDeger;
                kayit.Yil = item.Yil;
                kayit.Zaman = item.Zaman;
                kayit.SilindiMi = false;
                if (yeniMi)
                {
                    context.ABSDegerlendirme.Add(kayit);
                }
                context.SaveChanges();
            }
        }

        private DersDTO GetirDersDTO(int grupID, string dil = "tr")
        {
            DersDTO result = null;
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var hocaDersSorgu = (from veri in db.OGRDersGrupHoca
                                     where veri.FKDersGrupID == grupID
                                     select new
                                     {
                                         DersGrupHocaID = veri.ID,
                                         DersKod = veri.OGRDersGrup.OGRDersPlan.BolKodAd + veri.OGRDersGrup.OGRDersPlan.DersKod,
                                         DersAd = (dil == "en" ? veri.OGRDersGrup.OGRDersPlanAna.YabDersAd : veri.OGRDersGrup.OGRDersPlanAna.DersAd),
                                         OgretimTuru = veri.OGRDersGrup.EnumOgretimTur,
                                         Donem = veri.OGRDersGrup.EnumDonem.Value,
                                         BolumKod = veri.OGRDersGrup.OGRDersPlan.BolKodAd,
                                         OgrenciSayisi = veri.OGRDersGrup.OGROgrenciYazilma.Where(x => x.Iptal == false && x.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL).Count(),
                                         OgrenciSayisiButunleme = veri.OGRDersGrup.OGROgrenciYazilma.Where(x => x.Iptal == false && (x.ButunlemeMi.HasValue && x.ButunlemeMi.Value) && x.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL).Count(),
                                         DersGrupAd = veri.OGRDersGrup.GrupAd,
                                         DersGrupID = veri.OGRDersGrup.ID,
                                         DersPlanID = veri.OGRDersGrup.FKDersPlanID,
                                         DersPlanAnaID = veri.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.ID,
                                         Yil = veri.OGRDersGrup.OgretimYili.Value,
                                         Kredi = veri.OGRDersGrup.OGRDersPlanAna.Kredi,
                                         EnumDersPlanZorunlu = veri.OGRDersGrup.OGRDersPlan.EnumDersPlanZorunlu,
                                         FakulteID = veri.OGRDersGrup.OGRDersPlan.FKFakulteBirimID,
                                         BolumID = veri.OGRDersGrup.OGRDersPlan.FKBolumPrBirimID,
                                         DSaat = veri.OGRDersGrup.OGRDersPlanAna.DSaat,
                                         USaat = veri.OGRDersGrup.OGRDersPlanAna.USaat,
                                         YariYil = veri.OGRDersGrup.OGRDersPlan.Yariyil,
                                         FinalTarihi = veri.OGRDersGrup.FinalTarihi,
                                         EnumDersPlanOrtalama = veri.OGRDersGrup.OGRDersPlan.EnumDersPlanOrtalama,
                                         ButunlemeMi = veri.OGRDersGrup.OGROgrenciYazilma.Any(x => x.Iptal == false && (x.ButunlemeMi.HasValue && x.ButunlemeMi.Value == true) && x.OGRKimlik.EnumOgrDurum == (int)EnumOgrDurum.NORMAL),
                                         EnumSeviye = veri.OGRDersGrup.OGRDersPlan.Birimler.EnumSeviye,
                                         EnumOgrenimTur = veri.OGRDersGrup.OGRDersPlan.Birimler.EnumOgrenimTuru,
                                         Personel = veri.PersonelBilgisi,
                                         EnumKokDersTipi = veri.OGRDersGrup.OGRDersPlan.OGRDersPlanAna.EnumKokDersTipi
                                     });

                var item = hocaDersSorgu.FirstOrDefault();

                if (item == null)
                {
                    return null;
                }

                bool butunlemeMi = item.ButunlemeMi;
                var degerlendirme = db.ABSDegerlendirme.FirstOrDefault(x => x.FKDersGrupID == item.DersGrupID && x.SilindiMi == false && x.ButunlemeMi == butunlemeMi);
                EnumSonHal sonHal = EnumSonHal.BELIRSIZ;

                if (degerlendirme == null)
                {
                    sonHal = EnumSonHal.HAYIR;
                }
                else if (degerlendirme.EnumSonHal == (int)EnumSonHal.EVET)
                {
                    sonHal = EnumSonHal.EVET;
                }
                else
                {
                    sonHal = EnumSonHal.HAYIR;
                }

                result = new DersDTO();
                result.ButunlemeMi = item.ButunlemeMi;
                result.ButunlemeMiOrijinal = item.ButunlemeMi;
                if (result.ButunlemeMiOrijinal)
                {
                    //Bütünleme notu girilmiş ise bütünleme modunu aktifleştir.
                    result.ButunlemeMi = db.ABSOgrenciNot.Any(x => x.FKDersGrupID == result.DersGrupID && x.ABSPaylar.EnumCalismaTip == (int)EnumCalismaTip.Butunleme && x.Silindi == false);
                }

                if (item.FakulteID == null && item.BolumID == null)
                {
                    result.Bolum = new Sabis.Core.DtoBirimler();
                    result.Bolum.BirimAdi = "ÜNİVERSİTE ORTAK SEÇMELİ";
                    result.Bolum.BasilanBirimAdi = "ÜNİVERSİTE ORTAK SEÇMELİ";
                }
                else if (item.FakulteID != null && item.BolumID == null)
                {
                    result.Bolum = new Sabis.Core.DtoBirimler();
                    result.Bolum.BirimAdi = "FAKÜLTE ORTAK SEÇMELİ";
                    result.Bolum.BasilanBirimAdi = "FAKÜLTE ORTAK SEÇMELİ";
                }
                else
                {

                    result.Bolum = Sabis.Client.BirimIslem.Instance.getirBirimbyID(item.BolumID.Value);
                }

                if (item.FakulteID.HasValue)
                {
                    result.Fakulte = Sabis.Client.BirimIslem.Instance.getirBirimbyID(item.FakulteID.Value);
                }
                else
                {
                    result.Fakulte = new Sabis.Core.DtoBirimler();
                    result.Fakulte.BirimAdi = "ÜNİVERSİTE ORTAK SEÇMELİ";
                    result.Fakulte.BasilanBirimAdi = "ÜNİVERSİTE ORTAK SEÇMELİ";
                }

                result.DersGrupAd = item.DersGrupAd;
                result.EnumZorunluDers = item.EnumDersPlanZorunlu;
                result.Yariyil = item.YariYil;
                result.DersKod = item.DersKod;
                result.DersAd = item.DersAd;
                result.DersPlanAnaID = item.DersPlanAnaID;
                result.Donem = item.Donem;
                result.OgretimTuru = item.OgretimTuru;
                result.OgretimTuruAd = Sabis.Enum.Utils.GetEnumDescription((EnumOgretimTur)item.OgretimTuru);
                result.Yil = item.Yil;
                result.OgrenciSayisi = item.OgrenciSayisi;
                result.OgrenciSayisiButunleme = item.OgrenciSayisiButunleme;
                result.Kredi = item.Kredi;
                result.DSaat = item.DSaat;
                result.USaat = item.USaat;
                result.DersGrupID = item.DersGrupID;
                result.DersPlanID = item.DersPlanID;
                result.FinalTarihi = item.FinalTarihi;
                result.SonHal = sonHal;
                result.EnumKokDersTipi = item.EnumKokDersTipi;
                if (item.EnumDersPlanOrtalama.HasValue)
                {
                    result.EnumDersPlanOrtalama = (EnumDersPlanOrtalama)item.EnumDersPlanOrtalama.Value;
                }
                else
                {
                    result.EnumDersPlanOrtalama = EnumDersPlanOrtalama.BELIRSIZ;
                }
                result.EnumSeviye = item.EnumSeviye;
                result.EnumOgrenimTur = item.EnumOgrenimTur;

                if (item.Personel != null)
                {
                    result.Personel = new PersonelDTO
                    {
                        Ad = item.Personel.Kisi.Ad,
                        Soyad = item.Personel.Kisi.Soyad,
                        UnvanAd = item.Personel.UnvanAd
                    };
                }
            }
            return result;
        }
    }
}
