﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Sabis.Bolum.WebUI.Startup))]
namespace Sabis.Bolum.WebUI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            //Sabis.Bolum.Bll.Startup.Init();
        }
    }
}
