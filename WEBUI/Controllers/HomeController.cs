﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Sabis.Bolum.Core.ABS.DERS.HOME;
using Sabis.Abs.WebUI.Templates;

namespace Sabis.Bolum.WebUI.Controllers

{
    //[AllowAnonymous]
    public class HomeController : Controller
    {
        IDers _ders;
        public HomeController(IDers ders)
        {
            _ders = ders;
        }
        //[AllowAnonymous]
        public ActionResult Index()
        {
            //if (Sabitler.KullaniciID == null || Sabitler.KullaniciID == 0)
            //{
            //    return Redirect("http://login.dev-app.gtu.edu.tr");
            //}
            //else
            //{
                return View();
            //}
        }
        //[AllowAnonymous]
        public ActionResult AcilanDersListesi()
        {
            return View();
        }
        //[AllowAnonymous]
        public ActionResult AcilanDersListesiV2()
        {
            return View();
        }

        public ActionResult _pAltDersListesi()
        {
            return PartialView(_ders.ListelePersonelDersleriv2(Sabitler.KullaniciID, Sabitler.Yil, Sabitler.Donem));
            //return PartialView(Sabis.Bolum.Bll.SOURCE.ABS.DERS.DERSMAIN.ListelePersonelDersleriv2(Sabitler.KullaniciID, Sabitler.Yil, Sabitler.Donem));
        }
        //[AllowAnonymous]
        public ActionResult _pAcilanDersListesi()
        {
            return PartialView(_ders.ListeleAcilanPersonelDersleri(Sabitler.KullaniciID, Sabitler.Yil, Sabitler.Donem));
        }
        //[AllowAnonymous]
        public ActionResult _pAcilanDersListesiV2()
        {
            return PartialView(_ders.ListeleAcilanPersonelDersleri(Sabitler.KullaniciID, Sabitler.Yil, Sabitler.Donem));
        }

        [AllowAnonymous]
        public void ExceleAktar()
        {
            var acilanDersler = _ders.ListeleAcilanPersonelDersleri(Sabitler.KullaniciID, Sabitler.Yil, Sabitler.Donem);
            var list = _ders.AcilanDerslerExcelFormat(acilanDersler);
            ExcelExport.EpplusExportToExcelFromObjectList(list.ToList<object>(), "Açılan Ders Listesi", true);

        }
        [AllowAnonymous]
        public void Cikti()
        {
            //List<DtoAramaSonuc> result = (List<DtoAramaSonuc>)Session["EXCEL_DETAYLI_ARAMA_CIKTISI"];
            //ExcelExport.EpplusExportToExcelFromObjectList(result.ToList<object>(), "Öğrenci Listesi", true);

        }
        public JsonResult DonemDegistir(int yil, int donem)
        {
            Session["yil"] = yil;
            Session["donem"] = donem;
            return Json("true");
        }
        public JsonResult GetirBirimListe(int ustBirimID, int? FKPersonelID, int? EnumBirimTuru, bool yetkiKontrol = true, int? yetkiGrupID = 3, int? enumOgretimSeviye = null, int? enumOgretimTur = null, bool birimGrup = false)
        {
            // RMZN edit : Comment'e alındı. Tarih tanıklık etsin bu koda 
            //if (Sabitler.KullaniciAdi == "umit")
            //{
            //    yetkiKontrol = false;
            //}
            return Json(Sabis.Bolum.Bll.SOURCE.ABS.GENEL.GENELMAIN.BirimGetir(ustBirimID, FKPersonelID, EnumBirimTuru, yetkiKontrol, yetkiGrupID, enumOgretimSeviye, enumOgretimTur, birimGrup));
        }

        public JsonResult GetirApiAltBirim(int ustBirimID, int EnumBirimTuru)
        {
            List<Sabis.Bolum.Core.ABS.GENEL.BirimVM> birimList = new List<Core.ABS.GENEL.BirimVM>();
            var result = Sabis.Client.BirimIslem.Instance.getirAltBirimler(ustBirimID, false, false, new List<int> { 4, 23 }, null, null, null);
            birimList.AddRange(result.Select(x => new Core.ABS.GENEL.BirimVM
            {
                ID = x.ID,
                Ad = x.BirimAdi,
                EnumBirimTuru = x.EnumBirimTuru,
                BirimAdiUzun = x.BirimUzunAdi,
                UstBirimID = x.UstBirimID,
                EnumOgretimTur = x.EnumOgrenimTuru
            }));
            return Json(birimList);
            //return Json("");
        }

        public JsonResult OgrenciAra(string adSoyad)
        {
           return Json(Sabis.Bolum.Bll.SOURCE.ABS.OGRENCI.OGRENCIMAIN.AraOgrenci(adSoyad, Sabitler.KullaniciID, Sabitler.Yil, Sabitler.Donem).Select(c => new { Value = c.OgrenciID, Text = c.Ad, Foto = c.FotoAdres, Birim = c.OgrenciBirim }), JsonRequestBehavior.AllowGet);
        }
    }
}