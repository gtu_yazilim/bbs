﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Sabis.Bolum.Bll.SOURCE.ABS.AKREDITASYON;

namespace Sabis.Bolum.WebUI.Controllers
{
    [AllowAnonymous]
    public class AkreditasyonController : Controller
    {
        // GET: Akreditasyon
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult _pSoruBasari()
        {
            return PartialView();
        }

        public JsonResult GetirSoruBasariJson(int dersGrupID, int payID, bool sonHalVerildiMi)
        {
            return Json(AKREDITASYONMAIN.GetirSoruSonuc(dersGrupID, payID, sonHalVerildiMi));
        }

        public ActionResult _pPCBasari()
        {
            return PartialView();
        }

        public JsonResult GetirPCBasariJson(int dersGrupID, int payID, bool sonHalVerildiMi)
        {
            return Json(AKREDITASYONMAIN.GetirPCBasariListesi(dersGrupID, payID, sonHalVerildiMi));
        }

        public ActionResult _pOCBasari()
        {
            return PartialView();
        }

        public ActionResult _pPCDersGrupGenelBasari() { return View(); }

        public ActionResult _pOCDersGrupGenelBasari() { return View(); }

        public JsonResult GetirDersGrupGenelPCBasari(int dersGrupID)
        {
            return Json(AKREDITASYONMAIN.GetirDersGrupPCBasariListesi(dersGrupID, Sabitler.Yil, Sabitler.Donem));
        }

        public JsonResult GetirDersGrupGenelOCBasari(int dersGrupID)
        {
            return Json(AKREDITASYONMAIN.GetirDersGrupOCBasariListesi(dersGrupID, Sabitler.Yil, Sabitler.Donem));
        }

        public JsonResult GetirOCBasariJson(int dersGrupID, int payID, bool sonHalVerildiMi)
        {
            return Json(AKREDITASYONMAIN.GetirOCBasariListesi(dersGrupID, payID, sonHalVerildiMi));
        }

        public JsonResult GetirBirimPCListe(int birimID, int yil)
        {
            return Json(AKREDITASYONMAIN.GetirBirimPCList(birimID, yil));
        }

        public JsonResult GetirBirimDersListe(int birimID, int yil, int enumDonem)
        {
            return Json(AKREDITASYONMAIN.GetirBirimDersListesi(birimID, yil, enumDonem));
        }

        public JsonResult GetirBirimDersIDListe(int birimID, int yil)
        {
            return Json(AKREDITASYONMAIN.GetirBirimDersIDListe(birimID, yil));
        }

        public ActionResult _pPCGenelRapor()
        {
            return PartialView();
        }

        public JsonResult GetirGenelPCRapor(int dersPlanAnaID, int yil, int enumDonem, int birimID)
        {
            return Json(AKREDITASYONMAIN.GetirSoruToplamSonuc(dersPlanAnaID, yil, enumDonem, birimID));
        }

        public ActionResult _pOCGenelRapor()
        {
            return PartialView();
        }

        public JsonResult GetirGenelOCRapor(int dersPlanAnaID, int yil, int enumDonem, int birimID)
        {
            return Json(AKREDITASYONMAIN.GetirSoruToplamSonuc(dersPlanAnaID, yil, enumDonem, birimID));
        }

        public JsonResult AkreditasyonTopluGuncelle()
        {
            return Json(AKREDITASYONMAIN.AkreditasyonTopluGuncelle(Sabitler.Yil, 2), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Ders()
        {
            return View();
        }

        public JsonResult GetirProgramCiktiListesi(List<int> dersIDList, int birimID, int programID, int yil, int donem)
        {
            return Json(AKREDITASYONMAIN.GetirProgramCiktiListe(dersIDList, birimID, programID, yil, donem));
            //var jsonResult = Json(AKREDITASYONMAIN.GetirProgramCiktiListe(dersIDList, birimID, programID, yil, donem), JsonRequestBehavior.AllowGet);
            //jsonResult.MaxJsonLength = Int32.MaxValue;
            //return Json(jsonResult);
        }
        public JsonResult GetirOgrenmeCiktiListesi(List<int> dersIDList, int birimID, int programID, int yil, int donem)
        {
            return Json(AKREDITASYONMAIN.GetirOgrenmeCiktiListe(dersIDList, birimID, programID, yil, donem));
            //var jsonResult = Json(AKREDITASYONMAIN.GetirOgrenmeCiktiListe(dersIDList, birimID, programID, yil, donem), JsonRequestBehavior.AllowGet);
            //jsonResult.MaxJsonLength = Int32.MaxValue;
            //return jsonResult;
        }

        public JsonResult GetirOrtalamaProgramCiktiListesi(int birimID, int programID, int yil, int donem)
        {
            return Json(AKREDITASYONMAIN.GetirProgramCiktiOrtalamaListe(birimID, programID, yil, donem));
        }

        //public JsonResult GetirOrtalamaKatsayiProgramCiktiListesi(int birimID, int yil, int donem)
        //{
        //    return Json(AKREDITASYONMAIN.GetirProgramCiktiOrtalamaKatsayiListe(birimID, yil, donem));
        //}
    }
}