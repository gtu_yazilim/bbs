﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Sabis.Bolum.Core.ABS.OGRENCI;
using Sabis.Bolum.Rapor;
using Sabis.Bolum.Bll.SOURCE.ABS.DERS;
using Sabis.Bolum.Bll.SOURCE.ABS.OGRENCI;
using Sabis.Bolum.Core.dto.Ders;
using Telerik.Reporting;
using Sabis.Bolum.Core.dto.Yoklama;
using jsreport.Types;
using System.Web.Script.Serialization;

namespace Sabis.Bolum.WebUI.Controllers
{
    [AllowAnonymous]
    public class RaporController : Controller
    {
        public ActionResult DevamTakip5(int id)
        {
            var dersDetay = Sabitler.GetirDersDetay(id);
            List<OgrenciListesiVM> liste = OGRENCIMAIN.GetirDersOgrenciListesi(dersDetay.DersGrupID);
            DevamTakip5Hafta rapor = new DevamTakip5Hafta(dersDetay.FakulteAd, dersDetay.BolumAd, dersDetay.DersAd + " (" + dersDetay.GrupAd + ") " + " (" + dersDetay.DersGrupID + ") ", dersDetay.BolumKod + " " + dersDetay.DersKod, dersDetay.DersSaati.ToString(), Sabitler.UnvanAdSoyad, Sabis.Enum.Utils.GetEnumDescription((Sabis.Enum.EnumOgretimTur)dersDetay.EnumOgretimTur), Sabitler.Yil.ToString() + " - " + Convert.ToString(Sabitler.Yil+1), Sabis.Enum.Utils.GetEnumDescription((Sabis.Enum.EnumDonem)dersDetay.Donem), liste, Sabitler.UniversiteAdi, Sabitler.LogoUrl);
            ExportToPDF(rapor);
            return new EmptyResult();
        }
        public ActionResult DevamTakip15(int id)
        {
            var dersDetay = Sabitler.GetirDersDetay(id);
            List<OgrenciListesiVM> liste = OGRENCIMAIN.GetirDersOgrenciListesi(dersDetay.DersGrupID);
            Sabis.Bolum.Rapor.DevamTakip15Hafta rapor = new Sabis.Bolum.Rapor.DevamTakip15Hafta(dersDetay.FakulteAd, dersDetay.BolumAd, dersDetay.DersAd + " (" + dersDetay.GrupAd + ") " + " (" + dersDetay.DersGrupID + ") ", dersDetay.BolumKod + " " + dersDetay.DersKod, dersDetay.DersSaati.ToString(), Sabitler.UnvanAdSoyad, Sabis.Enum.Utils.GetEnumDescription((Sabis.Enum.EnumOgretimTur)dersDetay.EnumOgretimTur), Sabitler.Yil.ToString() + " - " + Convert.ToString(Sabitler.Yil + 1), Sabis.Enum.Utils.GetEnumDescription((Sabis.Enum.EnumDonem)dersDetay.Donem), liste, Sabitler.UniversiteAdi, Sabitler.LogoUrl);
            ExportToPDF(rapor);
            return new EmptyResult();
        }
        public ActionResult YoklamaListe(int id, string tarih, int? sure)
        {
            var dersDetay = Sabitler.GetirDersDetay(id);

            //DateTime t = DateTime.ParseExact(tarih, "dd.MM.yyyy HH:mm", null);
            DateTime t = Convert.ToDateTime(tarih);

            if (!sure.HasValue)
            {
                //var t = Convert.ToDateTime(tarih);
                sure = DERSMAIN.GetirDersHaftaSaat(id, t.Year, t.Month, t.Day);
            }

            string qrCode = "TARIH:" + tarih + "+DERSPLANANAID:" + dersDetay.DersPlanAnaID + "+DERSGRUPHOCAID:" + dersDetay.DersGrupHocaID + "+DERSGRUPID:" + dersDetay.DersGrupID + "+OGRENCISAYISI:" + dersDetay.OgrenciSayisi;
            List<OgrenciListesiVM> liste = OGRENCIMAIN.GetirDersOgrenciListesi(dersDetay.DersGrupID);
            foreach (var item in liste)
            {
                item.Ad = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(item.Ad.ToLower());
                item.Soyad = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(item.Soyad.ToLower());
            }
            YoklamaListe rapor = new YoklamaListe(dersDetay.FakulteAd, dersDetay.BolumAd, dersDetay.DersAd + " (" + dersDetay.GrupAd + ") ", dersDetay.BolumKod + " " + dersDetay.DersKod, t, sure.Value, dersDetay.DersProgramiv2ID.Value, Sabitler.UnvanAdSoyad, Sabis.Enum.Utils.GetEnumDescription((Sabis.Enum.EnumOgretimTur)dersDetay.EnumOgretimTur), Sabitler.Yil.ToString() + " - " + Convert.ToString(Sabitler.Yil + 1), Sabis.Enum.Utils.GetEnumDescription((Sabis.Enum.EnumDonem)dersDetay.Donem), liste, qrCode, Sabitler.UniversiteAdi, Sabitler.LogoUrl);
            ExportToPDF(rapor);
            return new EmptyResult();
        }
        public ActionResult YoklamaListeTumu()
        {
            DateTime Pazartesi = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            DateTime Pazar = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
            Pazartesi = Pazartesi.AddDays(1 - Pazartesi.DayOfWeek.GetHashCode());
            Pazar = Pazar.AddDays(7 - Pazar.DayOfWeek.GetHashCode());

            var dersListe = Sabis.Bolum.Bll.SOURCE.ABS.DERS.DERSPROGRAMI.GetirOgretimElemaniDersProgramiv2(Sabitler.KullaniciID, Sabitler.Yil, Sabitler.Donem);
            var eventList = dersListe.Where(x => x.Baslangic >= Pazartesi && x.Bitis <= Pazar).Select(x => new HaftalikDersProgramiDto
            {
                ID = x.ID,
                DersBilgisi = x.DersBilgisi.DersAd + (x.DersBilgisi.EnumOgretimTur == Sabis.Enum.EnumOgretimTur.BELIRSIZ ? " " : x.DersBilgisi.EnumOgretimTur == Sabis.Enum.EnumOgretimTur.OGRETIM_1 ? " 1" : x.DersBilgisi.EnumOgretimTur == Sabis.Enum.EnumOgretimTur.OGRETIM_2 ? " 2" : x.DersBilgisi.EnumOgretimTur == Sabis.Enum.EnumOgretimTur.UZAKTAN ? " 3" : "").ToString() + "-" + Sabis.Bolum.Bll.SOURCE.ABS.DERS.DERSMAIN.GetirDersDetay(x.DersBilgisi.DersGrupHocaID).GrupAd,
                Baslangic = x.Baslangic,
                Bitis = x.Bitis,
                Color = x.DersBilgisi.EnumOgretimTur == Sabis.Enum.EnumOgretimTur.BELIRSIZ ? "#2F353B" : x.DersBilgisi.EnumOgretimTur == Sabis.Enum.EnumOgretimTur.OGRETIM_1 ? "#3598DC" : x.DersBilgisi.EnumOgretimTur == Sabis.Enum.EnumOgretimTur.OGRETIM_2 ? "#F36A5A" : x.DersBilgisi.EnumOgretimTur == Sabis.Enum.EnumOgretimTur.UZAKTAN ? "#2C3E50" : "",
                FKDersGrupHocaID = x.DersBilgisi.DersGrupHocaID,
                Mekan = x.Mekan == null ? "" : x.Mekan.Ad
            }).OrderBy(y => y.Baslangic).ToList();

            var reportBook = new ReportBook();
            foreach (var item in eventList)
            {
                var dersDetay = Sabitler.GetirDersDetay(item.FKDersGrupHocaID);
                DateTime t = Convert.ToDateTime(item.Baslangic);
                string qrCode = "TARIH:" + item.Baslangic + "+DERSPLANANAID:" + dersDetay.DersPlanAnaID + "+DERSGRUPHOCAID:" + dersDetay.DersGrupHocaID + "+DERSGRUPID:" + dersDetay.DersGrupID + "+OGRENCISAYISI:" + dersDetay.OgrenciSayisi;
                List<OgrenciListesiVM> ogrenciliste = OGRENCIMAIN.GetirDersOgrenciListesi(dersDetay.DersGrupID);
                foreach (var item2 in ogrenciliste)
                {
                    item2.Ad = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(item2.Ad.ToLower());
                    item2.Soyad = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(item2.Soyad.ToLower());
                }
                Sabis.Bolum.Rapor.YoklamaListe rapor = new Sabis.Bolum.Rapor.YoklamaListe(dersDetay.FakulteAd, dersDetay.BolumAd, dersDetay.DersAd + " (" + dersDetay.GrupAd + ") ", dersDetay.BolumKod + " " + dersDetay.DersKod, t, 30, dersDetay.DersProgramiv2ID.Value, Sabitler.UnvanAdSoyad, Sabis.Enum.Utils.GetEnumDescription((Sabis.Enum.EnumOgretimTur)dersDetay.EnumOgretimTur), Sabitler.Yil.ToString() + " - " + Convert.ToString(Sabitler.Yil + 1), Sabis.Enum.Utils.GetEnumDescription((Sabis.Enum.EnumDonem)dersDetay.Donem), eventList, qrCode, Sabitler.UniversiteAdi, Sabitler.LogoUrl);
                reportBook.Reports.Add(rapor);


            }
            ExportToPDFAll(reportBook);
            return new EmptyResult();
        }
        public ActionResult NotListesi(int id, string payIDList)
        {
            List<Sabis.Bolum.Core.ABS.NOT.OgrenciListesiVM> bilgiler = Sabis.Bolum.Bll.SOURCE.ABS.NOT.NOTMAIN.ListeleOgrenciNotlari(Sabitler.GetirDersDetay(id).DersGrupID, payIDList);

            var dersDetay = Sabitler.GetirDersDetay(id);

            NotListesi notListesi = new NotListesi(dersDetay.FakulteAd, dersDetay.BolumAd, dersDetay.DersAd, dersDetay.DersKod, (dersDetay.DersSaati + dersDetay.UygulamaSaati).ToString(), Sabitler.UnvanAdSoyad, Sabis.Enum.Utils.GetEnumDescription((Sabis.Enum.EnumOgretimTur)dersDetay.EnumOgretimTur), Sabitler.Yil.ToString(), Sabis.Enum.Utils.GetEnumDescription((Sabis.Enum.EnumDonem)Sabitler.Donem), bilgiler, Sabitler.UniversiteAdi, Sabitler.LogoUrl);

            ExportToPDF(notListesi);
            return new EmptyResult();
        }
        [AllowAnonymous]
        public JsonResult NotListesiJson(string payIDList, int id)
        {
            List<Sabis.Bolum.Core.ABS.NOT.OgrenciListesiVM> bilgiler = Sabis.Bolum.Bll.SOURCE.ABS.NOT.NOTMAIN.ListeleOgrenciNotlari(Sabitler.GetirDersDetay(id).DersGrupID, payIDList);

            return Json(bilgiler, JsonRequestBehavior.AllowGet);
        }
        public static void ExportToPDF(Telerik.Reporting.Report reportToExport)
        {
            Telerik.Reporting.Processing.ReportProcessor reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
            Telerik.Reporting.Processing.RenderingResult result = reportProcessor.RenderReport("PDF", reportToExport, null);

            string fileName = result.DocumentName + ".pdf";

            //string fileName;
            //var tarih = (Telerik.Reporting.TextBox)reportToExport.Items.FirstOrDefault().Items.FirstOrDefault(t => t.Name == "txtTarih");
            //if (tarih != null)
            //{
            //    fileName = result.DocumentName + " - " + tarih.Value + ".pdf";
            //}
            //else
            //{
            //    fileName = result.DocumentName + ".pdf";
            //}


            System.Web.HttpContext.Current.Response.Clear();
            System.Web.HttpContext.Current.Response.ContentType = result.MimeType;
            System.Web.HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1254");
            System.Web.HttpContext.Current.Response.Charset = "windows-1254";
            System.Web.HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.Private);
            System.Web.HttpContext.Current.Response.Expires = -1;
            System.Web.HttpContext.Current.Response.Buffer = true;

            System.Web.HttpContext.Current.Response.AddHeader("Content-Disposition",
                               string.Format("{0};FileName=\"{1}\"",
                                             "attachment",
                                             fileName));

            System.Web.HttpContext.Current.Response.BinaryWrite(result.DocumentBytes);
            System.Web.HttpContext.Current.Response.End();
        }
        public static void ExportToPDFAll(Telerik.Reporting.ReportBook reportToExport)
        {
           

            Telerik.Reporting.Processing.ReportProcessor reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
            Telerik.Reporting.Processing.RenderingResult result = reportProcessor.RenderReport("PDF", reportToExport, null);
            string fileName = result.DocumentName + ".pdf";
            System.Web.HttpContext.Current.Response.Clear();
            System.Web.HttpContext.Current.Response.ContentType = result.MimeType;
            System.Web.HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1254");
            System.Web.HttpContext.Current.Response.Charset = "windows-1254";
            System.Web.HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.Private);
            System.Web.HttpContext.Current.Response.Expires = -1;
            System.Web.HttpContext.Current.Response.Buffer = true;

            System.Web.HttpContext.Current.Response.AddHeader("Content-Disposition",
                               string.Format("{0};FileName=\"{1}\"",
                                             "attachment",
                                             fileName));

            System.Web.HttpContext.Current.Response.BinaryWrite(result.DocumentBytes);
            System.Web.HttpContext.Current.Response.End();
        }

        #region JsReport

        public ActionResult YoklamaListe_JsReport(int id, string tarih, int? sure)
        {
            var dersDetay = Sabitler.GetirDersDetay(id);
            DateTime t = Convert.ToDateTime(tarih);
            if (!sure.HasValue)
                sure = DERSMAIN.GetirDersHaftaSaat(id, t.Year, t.Month, t.Day);

            string qrCode = "TARIH:" + tarih + "+DERSPLANANAID:" + dersDetay.DersPlanAnaID + "+DERSGRUPHOCAID:" + dersDetay.DersGrupHocaID + "+DERSGRUPID:" + dersDetay.DersGrupID + "+OGRENCISAYISI:" + dersDetay.OgrenciSayisi;
            List<OgrenciListesiVM> liste = OGRENCIMAIN.GetirDersOgrenciListesi(dersDetay.DersGrupID);
            foreach (var item in liste)
            {
                item.Ad = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(item.Ad.ToLower());
                item.Soyad = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(item.Soyad.ToLower());
            }
            var model = new YoklamaVM();
            model.DersDetay = dersDetay;
            model.Tarih = t;
            model.Sure = sure;
            model.qrCode = qrCode;
            model.OgrenciListe = liste;
            return View("YoklamaListe_JsReport",model);
        }
        public ActionResult TumYoklamaListe_JsReport()
        {
            var model = new List<YoklamaVM>();


            DateTime Pazartesi = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            DateTime Pazar = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
            Pazartesi = Pazartesi.AddDays(1 - Pazartesi.DayOfWeek.GetHashCode());
            Pazar = Pazar.AddDays(7 - Pazar.DayOfWeek.GetHashCode());

            var dersListe = Sabis.Bolum.Bll.SOURCE.ABS.DERS.DERSPROGRAMI.GetirOgretimElemaniDersProgramiv2(Sabitler.KullaniciID, Sabitler.Yil, Sabitler.Donem);
            var eventList = dersListe.Where(x => x.Baslangic >= Pazartesi && x.Bitis <= Pazar).Select(x => new HaftalikDersProgramiDto
            {
                ID = x.ID,
                DersBilgisi = x.DersBilgisi.DersAd + (x.DersBilgisi.EnumOgretimTur == Sabis.Enum.EnumOgretimTur.BELIRSIZ ? " " : x.DersBilgisi.EnumOgretimTur == Sabis.Enum.EnumOgretimTur.OGRETIM_1 ? " 1" : x.DersBilgisi.EnumOgretimTur == Sabis.Enum.EnumOgretimTur.OGRETIM_2 ? " 2" : x.DersBilgisi.EnumOgretimTur == Sabis.Enum.EnumOgretimTur.UZAKTAN ? " 3" : "").ToString() + "-" + Sabis.Bolum.Bll.SOURCE.ABS.DERS.DERSMAIN.GetirDersDetay(x.DersBilgisi.DersGrupHocaID).GrupAd,
                Baslangic = x.Baslangic,
                Bitis = x.Bitis,
                Color = x.DersBilgisi.EnumOgretimTur == Sabis.Enum.EnumOgretimTur.BELIRSIZ ? "#2F353B" : x.DersBilgisi.EnumOgretimTur == Sabis.Enum.EnumOgretimTur.OGRETIM_1 ? "#3598DC" : x.DersBilgisi.EnumOgretimTur == Sabis.Enum.EnumOgretimTur.OGRETIM_2 ? "#F36A5A" : x.DersBilgisi.EnumOgretimTur == Sabis.Enum.EnumOgretimTur.UZAKTAN ? "#2C3E50" : "",
                FKDersGrupHocaID = x.DersBilgisi.DersGrupHocaID,
                Mekan = x.Mekan == null ? "" : x.Mekan.Ad
            }).OrderBy(y => y.Baslangic).ToList();


            foreach (var item in eventList)
            {
                var dersDetay = Sabitler.GetirDersDetay(item.FKDersGrupHocaID);
                DateTime t = Convert.ToDateTime(item.Baslangic);
                string qrCode = "TARIH:" + item.Baslangic + "+DERSPLANANAID:" + dersDetay.DersPlanAnaID + "+DERSGRUPHOCAID:" + dersDetay.DersGrupHocaID + "+DERSGRUPID:" + dersDetay.DersGrupID + "+OGRENCISAYISI:" + dersDetay.OgrenciSayisi;
                List<OgrenciListesiVM> ogrenciliste = OGRENCIMAIN.GetirDersOgrenciListesi(dersDetay.DersGrupID);
                foreach (var item2 in ogrenciliste)
                {
                    item2.Ad = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(item2.Ad.ToLower());
                    item2.Soyad = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(item2.Soyad.ToLower());
                }
                var vm = new YoklamaVM();
                vm.DersDetay = dersDetay;
                vm.Tarih = t;
                vm.Sure = 30;
                vm.qrCode = qrCode;
                vm.OgrenciListe = ogrenciliste;

                model.Add(vm);

            }
            return View("TumYoklamaListe_JsReport", model);
        }
        #endregion
    }
}