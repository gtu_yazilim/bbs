﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using Sabis.Enum;
using Ical.Net;
using Ical.Net.DataTypes;
using Ical.Net.Serialization.iCalendar.Serializers;
using Ical.Net.Serialization;

namespace Sabis.Bolum.WebUI.Controllers
{
    [AllowAnonymous]
    public class DersProgramiController : Controller
    {
        // GET: DersProgrami
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult _pGetirDersMekan()
        {
            return PartialView();
        }

        public JsonResult GetirOgretimElemaniDersProgrami(int? fkDersGrupHocaID)
        {

            var dersListe = Sabis.Bolum.Bll.SOURCE.ABS.DERS.DERSPROGRAMI.GetirGrupDersProgramiv2(Sabitler.KullaniciID, Sabitler.GetirDersDetay(fkDersGrupHocaID.Value).DersGrupID);

            var eventList = dersListe.Select(x => new
            {
                id = x.ID,
                title = x.DersBilgisi.DersAd,
                start = x.Baslangic.AddHours(3),
                end = x.Bitis.AddHours(3),
                color = x.DersBilgisi.EnumOgretimTur == EnumOgretimTur.BELIRSIZ ? "#2F353B" : x.DersBilgisi.EnumOgretimTur == EnumOgretimTur.OGRETIM_1 ? "#3598DC" : x.DersBilgisi.EnumOgretimTur == EnumOgretimTur.OGRETIM_2 ? "#F36A5A" : x.DersBilgisi.EnumOgretimTur == EnumOgretimTur.UZAKTAN ? "#2C3E50" : "",
                dersGrupHocaID = x.DersBilgisi.DersGrupHocaID,
                mekan = x.Mekan == null ? "" : x.Mekan.Ad
            });

            return Json(eventList.OrderByDescending(x => x.start).ToArray(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetirOgretimElemaniDersProgramiTumu(int? fkDersGrupHocaID)
        {

            var dersListe = Sabis.Bolum.Bll.SOURCE.ABS.DERS.DERSPROGRAMI.GetirOgretimElemaniDersProgramiv2(Sabitler.KullaniciID, Sabitler.Yil, Sabitler.Donem);

            var eventList = dersListe.Select(x => new
            {
                id = x.ID,
                title = x.DersBilgisi.DersAd + (x.DersBilgisi.EnumOgretimTur == EnumOgretimTur.BELIRSIZ ? " " : x.DersBilgisi.EnumOgretimTur == EnumOgretimTur.OGRETIM_1 ? " 1" : x.DersBilgisi.EnumOgretimTur == EnumOgretimTur.OGRETIM_2 ? " 2" : x.DersBilgisi.EnumOgretimTur == EnumOgretimTur.UZAKTAN ? " 3" : "").ToString() + "-" + Sabis.Bolum.Bll.SOURCE.ABS.DERS.DERSMAIN.GetirDersDetay(x.DersBilgisi.DersGrupHocaID).GrupAd,
                //title = x.DersBilgisi.DersAd,
                start = x.Baslangic,
                end = x.Bitis,
                color = x.DersBilgisi.EnumOgretimTur == EnumOgretimTur.BELIRSIZ ? "#2F353B" : x.DersBilgisi.EnumOgretimTur == EnumOgretimTur.OGRETIM_1 ? "#3598DC" : x.DersBilgisi.EnumOgretimTur == EnumOgretimTur.OGRETIM_2 ? "#F36A5A" : x.DersBilgisi.EnumOgretimTur == EnumOgretimTur.UZAKTAN ? "#2C3E50" : "",
                dersGrupHocaID = x.DersBilgisi.DersGrupHocaID,
                mekan = x.Mekan == null ? "" : x.Mekan.Ad
            });

            if (fkDersGrupHocaID.HasValue)
            {
                eventList = eventList.Where(x=> x.dersGrupHocaID == fkDersGrupHocaID);
            }

            return Json(eventList.OrderByDescending(x => x.start).ToArray(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult iCalendar()
        {
            var dersListe = Sabis.Bolum.Bll.SOURCE.ABS.DERS.DERSPROGRAMI.GetirOgretimElemaniDersProgramiv2(Sabitler.KullaniciID, Sabitler.Yil, Sabitler.Donem);
            var program = dersListe.Select(x => new
            {
                ID = x.ID,
                DersBilgisi = x.DersBilgisi.DersAd + (x.DersBilgisi.EnumOgretimTur == Sabis.Enum.EnumOgretimTur.BELIRSIZ ? " " : x.DersBilgisi.EnumOgretimTur == Sabis.Enum.EnumOgretimTur.OGRETIM_1 ? " 1" : x.DersBilgisi.EnumOgretimTur == Sabis.Enum.EnumOgretimTur.OGRETIM_2 ? " 2" : x.DersBilgisi.EnumOgretimTur == Sabis.Enum.EnumOgretimTur.UZAKTAN ? " 3" : "").ToString() + "-" + Sabis.Bolum.Bll.SOURCE.ABS.DERS.DERSMAIN.GetirDersDetay(x.DersBilgisi.DersGrupHocaID).GrupAd,
                Baslangic = x.Baslangic,
                Bitis = x.Bitis,
                Color = x.DersBilgisi.EnumOgretimTur == Sabis.Enum.EnumOgretimTur.BELIRSIZ ? "#2F353B" : x.DersBilgisi.EnumOgretimTur == Sabis.Enum.EnumOgretimTur.OGRETIM_1 ? "#3598DC" : x.DersBilgisi.EnumOgretimTur == Sabis.Enum.EnumOgretimTur.OGRETIM_2 ? "#F36A5A" : x.DersBilgisi.EnumOgretimTur == Sabis.Enum.EnumOgretimTur.UZAKTAN ? "#2C3E50" : "",
                FKDersGrupHocaID = x.DersBilgisi.DersGrupHocaID,
                Mekan = x.Mekan == null ? "" : x.Mekan.Ad
            }).OrderBy(y => y.Baslangic).ToList();

            string DownloadFileName = "DersProgrami.ics";
            Calendar iCal = new Calendar();

            foreach (var item in program)
            {
                CalendarEvent evt = iCal.Create<CalendarEvent>();
                evt.Uid = item.ID.ToString();
                evt.Start = new CalDateTime(item.Baslangic);
                evt.End = new CalDateTime(item.Bitis);
                evt.Description = item.DersBilgisi;
                if (item.Mekan != null)
                {
                    evt.Location = item.Mekan.ToString();
                }
                evt.Summary = item.DersBilgisi;
                iCal.Events.Add(evt);

            }
            var serializer = new CalendarSerializer(new SerializationContext());
            var serializedCalendar = serializer.SerializeToString(iCal);
            var contentType = "text/calendar";
            var bytes = Encoding.UTF8.GetBytes(serializedCalendar);
            return File(bytes, contentType, DownloadFileName);
        }
    }
}