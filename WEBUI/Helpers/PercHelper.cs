﻿using PerculusSDK.ServiceReference1;
using System.Collections.Generic;

namespace LMS.VClass.Helpers
{
    public static class PercHelper
    {
        public static ModuleSetting[] GetModuleSettings()
        {
            var moduleSettings = new List<ModuleSetting>
            {
                new ModuleSetting {Value = "1", key = "allowPrivateMessaging", mname = "Chat"},
                new ModuleSetting {Value = "1", key = "allowMessaging", mname = "Chat"},
                new ModuleSetting {Value = "1", key = "showAllUsers", mname = "Chat"},
                new ModuleSetting {Value = "1", key = "allowStartOthersStream", mname = "StreamList"},
                new ModuleSetting {Value = "1", key = "allowResizeVideos", mname = "StreamList"},
                new ModuleSetting {Value = "1", key = "defaultVideoW", mname = "StreamList"},
                new ModuleSetting {Value = "1", key = "defaultVideoH", mname = "StreamList"},
                new ModuleSetting {Value = "1", key = "streamCount", mname = "StreamList"},
                new ModuleSetting {Value = "2", key = "extraTime", mname = "Session"},
                new ModuleSetting {Value = "5", key = "warnBefore", mname = "Session"},
                new ModuleSetting {Value = "1", key = "showTimer", mname = "Session"},
                new ModuleSetting
                {
                    Value = "0x104ba9,0x104ba9",
                    key = "panelHeaderColors",
                    mname = "Session"
                },
                new ModuleSetting
                {
                    Value = "0xffffff,0xcccccc",
                    key = "panelFooterColors",
                    mname = "Session"
                },
                new ModuleSetting {Value = "0xffffff", key = "panelTitleColor", mname = "Session"}
            };


            return moduleSettings.ToArray();
        }
    }
}