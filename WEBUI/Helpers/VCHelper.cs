﻿using Sabis.Bolum.WebUI.Helpers;
using PerculusSDK;
using PerculusSDK.ServiceReference1;
using Sabis.Bolum.Core.ABS.DERS.SANALSINIF.Models;
using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Configuration;
using Sabis.Bolum.Bll.DATA.UysModel;

namespace LMS.VClass.Helpers
{
    public static class VcHelper
    {
        public static int ProviderID { get; set; }
        public static string ProviderAd { get; set; }
        public static string AttemptVcLass()
        {
            return null;
        }

        public static SSDurum CheckDate(DateTime startDate, int duration)
        {
            //ders başlamadan 30 dk önce ve bittikten 10 dakika sonraya kadar giriş yapılabilir
            int beforeStart = 30;
            int afterEnd = 10;
            var sDate = startDate.AddMinutes(-beforeStart);
            var eDate = startDate.AddMinutes(duration + afterEnd);
            var now = DateTime.Now;
            if (now < sDate)
            {
                return SSDurum.Baslamadi;
            }
            if (sDate <= now && now <= eDate)
            {
                return SSDurum.Basladi;
            }
            return SSDurum.Bitti;
        }

        internal static string ShowReplay(int? vcKEy, KullaniciTipEnum userType,int sunucuid, ref string errorMessage)
        {
            if (vcKEy.HasValue)
            {
                try
                {
                    switch (sunucuid)
                    {
                        case 1: PercSdk.Instance.RoomService.PublishReplay(vcKEy.Value); break;
                        case 2: PercSdk.Instance.RoomService2.PublishReplay(vcKEy.Value); break;
                        case 3: PercSdk.Instance.RoomService3.PublishReplay(vcKEy.Value); break;
                        case 4: PercSdk.Instance.RoomService4.PublishReplay(vcKEy.Value); break;
                        default:
                            break;
                    }
                    //PercSdk.Instance.RoomService.PublishReplay(vcKEy.Value);

                    var kullaniciadi = Sabitler.KullaniciAdi;
                    var kullanicibilgi = Sabitler.GetirPersonelBilgi(kullaniciadi);

                    
                    var participant = new Participant();
                    participant.FirstName = kullanicibilgi.Ad;
                    participant.LastName = kullanicibilgi.Soyad;
                    participant.Role = UserTypeConverter(userType);
                    participant.Email = Sabitler.KullaniciAdi + "@gtu.edu.tr";

                    switch (sunucuid)
                    {
                        case 1: return PercSdk.Instance.RoomService.RegisterAndGetURL(participant, vcKEy.Value); break;
                        case 2: return PercSdk.Instance.RoomService2.RegisterAndGetURL(participant, vcKEy.Value); break;
                        case 3: return PercSdk.Instance.RoomService3.RegisterAndGetURL(participant, vcKEy.Value); break;
                        case 4: return PercSdk.Instance.RoomService4.RegisterAndGetURL(participant, vcKEy.Value); break;
                        default:
                            return PercSdk.Instance.RoomService2.RegisterAndGetURL(participant, vcKEy.Value); break;
                            break;
                    }

                            //return PercSdk.Instance.RoomService.RegisterAndGetURL(participant, vcKEy.Value);
                }
                catch (Exception ex)
                {
                    errorMessage = ex.Message;
                    return null;
                }
            }
            return null;
        }
        public static string TurkishName2(string phrase)
        {
            int maxLength = 100;
            string str = phrase.ToLower();
            int i = str.IndexOfAny(new char[] { 'ş', 'ç', 'ö', 'ğ', 'ü', 'ı' });
            //if any non-english charr exists,replace it with proper char
            if (i > -1)
            {
                StringBuilder outPut = new StringBuilder(str);
                outPut.Replace('ö', 'o');
                outPut.Replace('ç', 'c');
                outPut.Replace('ş', 's');
                outPut.Replace('ı', 'i');
                outPut.Replace('ğ', 'g');
                outPut.Replace('ü', 'u');
                str = outPut.ToString();
            }
            // if there are other invalid chars, convert them into blank spaces
            str = Regex.Replace(str, @"[^a-z0-9\s-]", "");
            // convert multiple spaces and hyphens into one space       
            str = Regex.Replace(str, @"[\s-]+", " ").Trim();
            // cut and trim string
            str = str.Substring(0, str.Length <= maxLength ? str.Length : maxLength).Trim();
            // add hyphens
            str = Regex.Replace(str, @"\s", "-");
            return str;
        }

        internal static string VcStart(SanalSinif vc, KullaniciTipEnum userType, int SunucuID, ref string errorMessage)
        {
            try
            {
                var rm = VcToRoom(vc);

                if (rm.ROOMID <= 0)
                {
                    var message = "";
                    //rm = PercSdk.Instance.RoomService.RoomSave(rm, ref message);
                    switch (SunucuID)
                    {
                        case 1: rm = PercSdk.Instance.RoomService.RoomSave(rm, ref message); break;
                        case 2: rm = PercSdk.Instance.RoomService2.RoomSave(rm, ref message); break;
                        case 3: rm = PercSdk.Instance.RoomService3.RoomSave(rm, ref message); break;
                        case 4: rm = PercSdk.Instance.RoomService4.RoomSave(rm, ref message); break;

                        default: rm = PercSdk.Instance.RoomService2.RoomSave(rm, ref message); break;
                    }

                    vc.SSinifKey = rm.ROOMID;

                }
                if (rm.ROOMID <= 0)
                {
                    // error
                    Console.Out.WriteLine("error:" + PercSdk.Instance.RoomService.GetLastError());
                }

                // add attendee
                if (rm.ROOMID > 0)
                {
                    return RegisterAttendee(userType, rm.ROOMID, SunucuID);
                }
                return null;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return null;
            }
        }

        public static bool RoomPaket(int roomid, int sunucuid)
        {
            switch (sunucuid)
            {
                case 1: return  PercSdk.Instance.RoomService.PackRoom(roomid); break;
                case 2: return  PercSdk.Instance.RoomService2.PackRoom(roomid); break;
                case 3: return  PercSdk.Instance.RoomService3.PackRoom(roomid); break;
                case 4: return  PercSdk.Instance.RoomService4.PackRoom(roomid);  break;
                default:
                    return PercSdk.Instance.RoomService2.PackRoom(roomid); break;
                    break;
            }
            //return PercSdk.Instance.RoomService.PackRoom(roomid);
        }
        public static string RegisterAttendee(KullaniciTipEnum userType, int roomid, int SunucuID)
        {
            var kullaniciadi = Sabitler.KullaniciAdi;
            var kullanicibilgi = Sabitler.GetirPersonelBilgi(kullaniciadi);

            var participant = new Participant();
            participant.FirstName = kullanicibilgi.Ad; 
            participant.LastName = kullanicibilgi.Soyad;
            participant.Role = UserTypeConverter(userType);
            participant.Email = Sabitler.KullaniciAdi + "@gtu.edu.tr";

            switch (SunucuID)
            {
                case 1: return PercSdk.Instance.RoomService.RegisterAndGetURL(participant, roomid); break;
                case 2: return PercSdk.Instance.RoomService2.RegisterAndGetURL(participant, roomid); break;
                case 3: return PercSdk.Instance.RoomService3.RegisterAndGetURL(participant, roomid); break;
                case 4: return PercSdk.Instance.RoomService4.RegisterAndGetURL(participant, roomid); break;
                default:return PercSdk.Instance.RoomService2.RegisterAndGetURL(participant, roomid); break;
                    break;
            }
            //return PercSdk.Instance.RoomService.RegisterAndGetURL(participant, roomid);
            //return PercSdk.Instance.RoomService2.RegisterAndGetURL(participant, roomid);
        }

        private static Room VcToRoom2(SanalSinif vc)
        {
            return new Room
            {
                ROOMID = vc.SSinifKey ?? 0,
                BEGINDATE = vc.Baslangic,
                SESSIONNAME = vc.Ad,
                DESCRIPTION = "",
                FORMENROLL_CAPACITY = 50,
                SEND_ICS = true,
                USERRIGHTSCHEMA = "Standart",
                DURATION = vc.Sure,
                CATEGORY = "11003",
                PERMITTEDSYSTEMLAYOUTS = ",meeting,one2one,layout-twovideo,layout-wide-1,layout-videoconf,,elearning,",

                REPLAYSIZE = 0,
                SOUND_RATE = 22,
                VIDEO_QUALITY = 0,
                VIDEO_BANDWIDTH = 200,
                STREAMCOUNT = 4, 
                COLORCODE = "#cccccc", // gray background
                LANGUAGEFILE = "tr-TR" // english: en-US, turkish: tr-TR
            };
        }
        private static Room VcToRoom(SanalSinif m)
        {
            Room room = new Room();
            room.BEGINDATE = m.Baslangic;
            room.DURATION = Convert.ToInt32(m.Sure);
            room.DESCRIPTION = "";
            room.SESSIONNAME = m.Ad +" ("+m.ID+")";
            room.ADDDATE = DateTime.Now;
            room.CATEGORY = "11003";
            room.COLORCODE = "#171717";

            room.COMPANYID = 1; //m.CompanyId;
            room.PASSKEY = ""; // StringUtils.GenerateUniqueString(6);
            room.REQUIRESLOGIN = false; // pass key  olacağı için login olmasına gerek yok
            room.SEND_ICS = false;
            room.SEND_PASSKEY = false;

            room.FLASHFILE = "room"; // standart sürüm
            room.FORMENROLL_ALLOW = false;
            room.FORMENROLL_CAPACITY = 100;
            room.FORMENROLL_TYPE = 0;
            room.INREPLAYMODE = false;
            room.LANGUAGEFILE = "tr-TR";
            room.REPLAYSIZE = 0;
            room.SOUND_RATE = 22;
            room.VIDEO_QUALITY = 0;
            room.VIDEO_BANDWIDTH = 200;
            room.STREAMCOUNT = 4;
            room.AUTOPACKENABLED = true;
            //room.ROOMOPTIONS = RoomOptions.AllowCamera | RoomOptions.AllowChat | RoomOptions.AllowFile | RoomOptions.AllowPoll | RoomOptions.AllowScreenshare | RoomOptions.AllowWatch | RoomOptions.AllowWhiteboard;
            room.USERRIGHTSCHEMA = "standard";
            if (m.SSinifKey.HasValue)
            {
                room.ROOMID = m.SSinifKey.Value;
            }
            return room;
        }
        private static string UserTypeConverter(KullaniciTipEnum usertype)
        {
            switch (usertype)
            {
                case KullaniciTipEnum.Hoca:
                    return Participant.PARTICIPANT_INSTRUCTOR;
                case KullaniciTipEnum.Ogrenci:
                    return Participant.PARTICIPANT_USER;
                case KullaniciTipEnum.Admin:
                    return Participant.PARTICIPANT_ADMIN;
                default:
                    return Participant.PARTICIPANT_USER;
            }
        }

        #region Raporlama
        public static double GetirHocaAktifSure(int roomId)
        {
            var test = PercSdk.Instance.ReportService.GetDurationActive(roomId);
            return 0;
        }
        #endregion
    }

    public class PercSdk
    {
        private static PercSdk _instance;
        private static readonly object Padlock = new object();
        private readonly string _perculusApiPassword = WebConfigurationManager.AppSettings["VC:password"];
        private readonly string _perculusApiUsername = WebConfigurationManager.AppSettings["VC:username"];
        private readonly string _perculusBaseUri = WebConfigurationManager.AppSettings["VC:Base_Url"];
        public readonly RoomService RoomService;
        public readonly RoomService RoomService2;
        public readonly RoomService RoomService3;
        public readonly RoomService RoomService4;


        public readonly ReportService ReportService;
        public readonly ReportService ReportService2;
        public readonly ReportService ReportService3;
        public readonly ReportService ReportService4;

        private PercSdk()
        {
            RoomService = new RoomService();
            RoomService.Initialize(new Uri("http://etoplanti.local.apollo.gtu.edu.tr/"), "system", "perculus07");
            RoomService2 = new RoomService();
            RoomService2.Initialize(new Uri("http://etoplanti2.local.apollo.gtu.edu.tr/"), "system", "perculus07");
            RoomService3 = new RoomService();
            RoomService3.Initialize(new Uri("http://etoplanti3.sakarya.edu.tr/"), "system", "perculus07");
            RoomService4 = new RoomService();
            RoomService4.Initialize(new Uri("http://etoplanti4.sakarya.edu.tr/"), "system", "perculus07");

            ReportService = new ReportService();
            ReportService.Initialize(new Uri("http://www.etoplanti.sakarya.edu.tr/"), "system", "perculus07");
            ReportService2 = new ReportService();
            ReportService.Initialize(new Uri("http://etoplanti2.sakarya.edu.tr/"), "system", "perculus07");
            ReportService3 = new ReportService();
            ReportService.Initialize(new Uri("http://etoplanti3.sakarya.edu.tr/"), "system", "perculus07");
            ReportService4 = new ReportService();
            ReportService.Initialize(new Uri("http://etoplanti4.sakarya.edu.tr/"), "system", "perculus07");
        }

        public static PercSdk Instance
        {
            get
            {
                lock (Padlock)
                {
                    if (_instance == null)
                    {
                        _instance = new PercSdk();
                    }
                    return _instance;
                }
            }
        }
    }
}