﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Configuration;
using System.Collections.Specialized;
using Exceptionless;

public class Sabitler
{
    private static NameValueCollection appSettings = ConfigurationManager.GetSection("customAppSettingsGroup/LoginSettings") as NameValueCollection;
    public static int Yil
    {
        get
        {
            if (!string.IsNullOrEmpty(Convert.ToString(HttpContext.Current.Session["yil"])))
            {
                return Convert.ToInt32(HttpContext.Current.Session["yil"]);
            }
            else
            {
                return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["Yil"]);
            }
        }
    }
    public static int Donem
    {
        get
        {
            if (!string.IsNullOrEmpty(Convert.ToString(HttpContext.Current.Session["donem"])))
            {
                return Convert.ToInt32(HttpContext.Current.Session["donem"]);
            }
            else
            {
                return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["Donem"]);
            }
            //return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["Donem"]);
        }
    }
    public static int EBSYil
    {
        get { return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["EBSYil"]); }
    }
    public static int EBSDonem
    {
        get { return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["EBSDonem"]); }
    }
    public static int DegerlendirmeDonem
    {
        get { return 2; }
    }
    public static Sabis.Bolum.Core.ABS.DERS.DERSDETAY.DersDetayVM GetirDersDetay(int dersGrupHocaID, int? FKPersonelID = null)
    {
        return Sabis.Bolum.Bll.SOURCE.ABS.DERS.DERSMAIN.GetirDersDetay(dersGrupHocaID, FKPersonelID);
    }
    public static Sabis.Bolum.Core.ABS.PERSONEL.PersonelBilgiVM GetirPersonelBilgi(string kullaniciAdi)
    {
        try
        {
            return Sabis.Bolum.Bll.SOURCE.ABS.PERSONEL.PERSONELMAIN.GetirPersonelBilgi(kullaniciAdi);
        }
        catch (Exception error)
        {
            Sabitler.LogException(error);
            return new Sabis.Bolum.Core.ABS.PERSONEL.PersonelBilgiVM();
        }
    }
    public static string KullaniciAdi
    {
        get
        {
            List<string> AdminListe = ConfigurationManager.AppSettings["AdminListe"].ToString().Split(',').ToList();
            string kullanici = (HttpContext.Current.User.Identity as SSOClient.AuthModel.SSOGenericIdentity).SSOTicket.KullaniciAdi;
            //var test = HttpContext.Current.Session["simulasyon"];
            if (!string.IsNullOrEmpty(Convert.ToString(HttpContext.Current.Session["simulasyon"])))
            {
                kullanici = Convert.ToString(HttpContext.Current.Session["simulasyon"]);
            }
            else
            {
                if (AdminListe.Contains(kullanici))
                {
                    return DebugKullaniciAdi;
                }
            }
            return kullanici;
        }
    }
    public static int KullaniciID
    {
        get
        {
            List<string> AdminListe = ConfigurationManager.AppSettings["AdminListe"].ToString().Split(',').ToList();
            int kullaniciID = Sabis.Bolum.Bll.SOURCE.ABS.PERSONEL.PERSONELMAIN.GetirPersonelBilgi(KullaniciAdi).ID;
            if (!string.IsNullOrEmpty(Convert.ToString(HttpContext.Current.Session["simulasyon"])))
            {
                kullaniciID = Sabis.Bolum.Bll.SOURCE.ABS.PERSONEL.PERSONELMAIN.GetirPersonelBilgi(Convert.ToString(HttpContext.Current.Session["simulasyon"])).ID;
            }
            else
            {
                if (AdminListe.Contains(KullaniciAdi))
                {
                    return DebugKullaniciID;
                }
            }
            return kullaniciID;
        }
    }
    public static string UnvanAdSoyad
    {
        get { return Sabis.Bolum.Bll.SOURCE.ABS.PERSONEL.PERSONELMAIN.GetirPersonelBilgi(KullaniciAdi).UnvanAdSoyad; }
    }
    public static string SimulasyonKullaniciAdi
    {
        get
        {
            {
                if (!string.IsNullOrEmpty(Convert.ToString(HttpContext.Current.Session["simulasyon"])))
                {
                    return Convert.ToString(HttpContext.Current.Session["simuleEden"]);
                }
                return "hakki";
            }

        }
    }
    public static string IPAdresi
    {
        get
        {
            HttpContext context = HttpContext.Current;
            string clientIp = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (!string.IsNullOrEmpty(clientIp))
            {
                string[] forwardedIps = clientIp.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                clientIp = forwardedIps[forwardedIps.Length - 1];

                string[] IPPort = clientIp.Split(new char[] { ':' }, StringSplitOptions.None);
                clientIp = IPPort[0];
            }
            else
            {
                clientIp = context.Request.ServerVariables["REMOTE_ADDR"];
            }
            return clientIp;
        }
    }
    public static string ApiAdresDanismanlik { get { return ConfigurationManager.AppSettings["apiadresdanismanlik"].ToString(); } }
    public static string ApiAdresRandevu { get { return ConfigurationManager.AppSettings["apiadresrandevu"].ToString(); } }
    public static string ApiAdresAbs { get { return ConfigurationManager.AppSettings["apiadresabs"].ToString(); } }
    public static int UniversiteID
    {
        get
        {
            return Convert.ToInt32(ConfigurationManager.AppSettings["UniversiteID"]);
        }
    }

    public static Sabis.Enum.EnumUniversite EnumUniversite
    {
        get
        {
            Sabis.Enum.EnumUniversite result = Sabis.Enum.EnumUniversite.Gtu;
            if (ConfigurationManager.AppSettings["EnumUniversite"] == "SAU")
            {
                result = Sabis.Enum.EnumUniversite.SAU;
            }
            else if (ConfigurationManager.AppSettings["EnumUniversite"] == "Kisbu")
            {
                result = Sabis.Enum.EnumUniversite.Kisbu;
            }
            return result;
        }
    }
    public static string DebugKullaniciAdi
    {
        get
        {
            string result = null;
            // Get the AppSettings section.
            if (appSettings["DebugKullaniciAdi"] != null)
            {
                result = appSettings["DebugKullaniciAdi"];
            }
            else
            {
                result = "";
            }
            return result;
        }
    }

    public static int DebugKullaniciID
    {
        get
        {
            int result = 0;
            // Get the AppSettings section.
            if (appSettings["DebugKullaniciID"] != null)
            {
                result = Convert.ToInt32(appSettings["DebugKullaniciID"]);
            }
            else
            {
                result = 0;
            }
            return result;
        }
    }

    public static List<string> AdminListesi
    {
        get
        {
            return ConfigurationManager.AppSettings["AdminListe"].ToString().Split(',').ToList();
        }
    }

    public static bool DosyaUzantiKontrol(string uzanti)
    {
        List<string> GecerliDosyaTurleri = ConfigurationManager.AppSettings["GecerliDosyaTurleri"].ToString().Split(',').ToList();

        uzanti = uzanti.Replace(".", "").ToLower();

        if (GecerliDosyaTurleri.Contains(uzanti))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static string LogoUrl
    {
        get
        {
            return ConfigurationManager.AppSettings["Logo"];
        }
    }

    public static string UniversiteAdi
    {
        get
        {
            return ConfigurationManager.AppSettings["UniversiteAdi"];
        }
    }

    public static void LogException(Exception error)
    {
        error.ToExceptionless()
             .SetException(error)
             .SetUserDescription(KullaniciAdi + "@gtu.edu.tr", UnvanAdSoyad)
             .SetUserIdentity(new Exceptionless.Models.Data.UserInfo
             {
                 Identity = KullaniciAdi,
                 Name = KullaniciAdi,
             }).Submit();
    }

    public static int AnketDonem
    {
        get
        {
            return 2;
        }
    }

    public static int AnketYil
    {
        get
        {
            return 2016;
        }
    }
}