﻿$(document).ready(function () {
    $('#calendar').fullCalendar({
        dayNamesShort: ['Pazar', 'Pazartesi', 'Salı', 'Çarşamba', 'Perşembe', 'Cuma', 'Cumartesi'],
        theme: true,
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        businessHours: {
            start: '07:00',
            end: '24:00',
        },
        defaultView: 'agendaWeek',
        //defaultDate: '2016-08-29',
        editable: false,
        firstDay: 1,
        minTime: '07:00:00',
        maxTime: '24:00:00',
        //displayEventEnd: true,
        timeFormat: 'HH:mm',
        axisFormat: 'HH:mm',
        events: "/DersProgrami/GetirOgretimElemaniDersProgramiTumu",
        eventClick: function (event, jsEvent, view) {
            $('#modalTitle').html(event.title);
            $('#modalBody').load('/DersProgrami/_pGetirDersMekan/' + event.dersGrupHocaID);
            $('#eventUrl').attr('href', event.url);
            $('#GeneralModalBox').modal();
        },
        weekends: true,
        themeButtonIcons: {
            prev: 'circle-triangle-w',
            next: 'circle-triangle-e',
            prevYear: 'seek-prev',
            nextYear: 'seek-next'
        },
        textEscape: false,
        eventRender: function (event, element) {
            element.find('.fc-title').append("<br/><b style='color: yellow'>" + event.mekan + "</b>");
        },
        timezone: "local",
        //timezone: "Asia/Baghdad",
        //ignoreTimezone: true
        //eventAfterRender: function(event, element)
        //{
        //    element.find('.fc-title').after("<span>" + event.description + "</span>");
        //}
        //eventRender: function (event, element, view) {
        //    return $('<div class="fc-content">' + event.title + '</div>');
        //}
    });

    $(window).bind('resize', function () {
        if (window.innerWidth < 800) $('#calendar').fullCalendar('changeView', 'agendaDay');
        else $('#calendar').fullCalendar('changeView', 'agendaWeek');
    });

    if (window.innerWidth < 800) {
        $('#calendar').fullCalendar('changeView', 'agendaDay');
    }
});