﻿// jQuery dependency
// isotope dependency



$('#txtOgrenciAra').autocomplete({
    source: function (request, response) {
        blockStatus = false;
        $.post("/Home/OgrenciAra", { adSoyad: request.term })
            .success(function (data) {
                response($.map(data, function (item) { return { label: item.Text, value: item.Value } }));
            })
    },
    messages: {
        noResults: '',
        results: function () { }
    },
    select: function (event, ui) {
        window.location.href = "/Ders/Ogrenci/Profil/" + ui.item.value;
        return false;
    },
    minLength: 3
});