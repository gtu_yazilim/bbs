﻿// jQuery dependency
// jsrender dependency

var sure = 59;
$(document).ready(function () {
    BaslikEkle();
    var template = $.templates("myTmpl", '<tr><td>{{:Numara}}</td><td>{{:Ad}} {{:Soyad}}</td>{{for OgrenciNotListesi}}<td class="text-center"><input type="text" class="txtNot" id="{{>#parent.parent.data.OgrenciID}}.{{:PayID}}" data-target="{{>#parent.parent.data.OgrenciID}}.{{:PayID}}" data-payid="{{:PayID}}" data-numara="{{>#parent.parent.data.Numara}}" data-ogrenciid="{{>#parent.parent.data.OgrenciID}}" maxlength="4" value="{{:Notu}}"></td>{{/for}}</tr>');
    $.post("/Ders/NotGiris/ListeleAnaOgrenciBilgileri", veri).done(function (json) { console.log(json); $("#tblNot").html(template.render(json)); }).promise().done(function () { TxtAyar(); });
});

function BaslikEkle() {
    var idArray = idListe.split("-");
    var kod = "";
    for (var z = 0; z < idArray.length; z++) {
        kod += '<th class="col-md-1 text-center"><b>' + idArray[z] + '</b></th>';
    }
    $("#tblNotBaslik").html('<tr class="info"><th class="col-md-1">Öğrenci No</th><th class="col-md-2">Ad Soyad</th>' + kod + '</tr>');
}

$('button[name="BtnKaydet"]').on("click", function () {
    var liste = new Array();

    $.each($("#tblNot tr"), function (i, item) {

        var notlar = $(item).find('input');

        $.each(notlar, function (j, item2) {
            var tmpParam = {
                Notu: $(this).val(),
                OgrenciID: $(this).attr("data-ogrenciid"),
                PayID: $(this).attr("data-payid"),
                KaydedenKulanici: kullaniciAdi,
                Yil: yil,
                Donem: donem,
                DersplanAnaID: veri.id
            };

            liste.push(tmpParam);
        });
    });

    $.post('/Ders/NotGiris/AnaNotKaydet', { liste: liste }).done(function (json) { json ? toastr.success("Not girişi tamamlandı.") : toastr.error("Not girişi sırasında bir hata oluştu."); });
});

//input filters
var TxtAyar = function () {
    $(".txtNot").keydown(function (e) {
        if (e.keyCode == 37) { $(this).parent().prev().children("input").focus(); }
        if (e.keyCode == 38) { var index = $(this).parent().index(); $(this).parent().parent().prev().find('td').eq(index).children("input").focus(); }
        if (e.keyCode == 39) { $(this).parent().next().children("input").focus(); }
        if (e.keyCode == 40) { var index = $(this).parent().index(); $(this).parent().parent().next().find('td').eq(index).children("input").focus(); }

        //if (($(this).val() == "GR" || $(this).val() == "DZ") && e.keyCode == 8) { $('[data-target="' + $(this).attr("data-target") + '"]').val(""); }
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 110]) !== -1 || (e.keyCode >= 35 && e.keyCode <= 39)) { return; }
        if (e.keyCode == 188 || e.keyCode == 190) { $(this).val($(this).val() + "."); }
        if (e.keyCode == 71) { $('[data-target="' + $(this).attr("data-target") + '"]').val("GR"); }
        if (e.keyCode == 68) { $('[data-target="' + $(this).attr("data-target") + '"]').val("DZ"); }
        if (e.keyCode == 189 || e.keyCode == 109) { $(this).val("-"); }

        $(this).val($(this).val().replace("..", "."));
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $(".txtNot").keyup(function (e) {
        var val = $(this).val();
        val = val.substring(0, 2);
        if ((val == "GR" || val == "DZ") && e.keyCode != 8) { $(this).val(val); }
        if ($(this).val() < -10 || $(this).val() > 100) { $(this).val(""); toastr.error("0 ile 100 arasında bir sayı girilmelidir"); }
    });
}
//input filters

//otomatik kayıt
function geriSayim() {
    var geriSay = setTimeout(function () {
        if ($("#cbOtomatikKaydet").is(":checked")) {
            $("#lblSure").html("Kalan süre: " + sure);
            sure--;
            if (sure < 1) { sure = 59; clearTimeout(geriSay); }
            geriSayim();
        }
        else {
            sure = 59;
            $("#lblSure").html("");
        }
    }, 1000);
}

function otomatikKaydet() {
    var otomatikKayit = setTimeout(function () {
        if ($("#cbOtomatikKaydet").is(":checked")) {
            otomatikKaydet();
            $("#BtnKaydet").trigger("click");
        }
    }, 60000);
}

$("#cbOtomatikKaydet").on("change", function () {
    if ($(this).is(":checked")) {
        otomatikKaydet();
        geriSayim();
    }
    else {
        clearTimeout(otomatikKayit);
    }

});
//otomatik kayıt