﻿// jQuery dependency

$(document).on('click', '#haftaListe', function (e) {
    e.stopPropagation();
});

$(document).on("click", ".pCiktiSil", function () {
    var ciktisil = $.post("/Ders/Sinav/SilProgramCikti", { ID: $(this).data("id") })
        .success(function (json) {
            $("#soruListe").load(soruURL);
        }).promise().done(function () {

        });
});

$(document).on("click", ".oCiktiSil", function () {
    var ciktisil = $.post("/Ders/Sinav/SilOgrenmeCikti", { ID: $(this).data("id") })
        .success(function (json) {
            $("#soruListe").load(soruURL);
        }).promise().done(function () {
        });
});

$("#btnOdevOlustur").one("click", function () {
    var data = {
        Baslangic: $("#txtOdevBaslangicTarihi").val(),
        Bitis: $("#txtOdevBitisTarihi").val(),
        FKAbsPaylarID: FKPaylarID,
        Aciklama: $("#txtAciklama").val(),
        FKPersonelID: fkPersonelID,
        FKDersGrupID: FKDersGrupID,
        EnumSinavTuru: 3,
        MaxYuklemeSayisi: $("#ddlYuklemeHakki").val(),
        OdevDosyakey: dosyaKey,
        OdevDosyaBoyut: dosyaBoyutu,
        OdevDosyaTuru: dosyaTuru,
        Yayinlandi: true,//$("#cbYayinlandi").is(":checked"),
        SGTarih: tarih,
        SGKullanici: kullaniciAdi,
        SGIP: ipAdresi,
        OnlineOdevMi: $("#rdOnlineOdev").is(":checked"),
        Silindi: false,
        GenelOdevMi: $("#rdGenelOdev").is(":checked"),
    };

    if (data.OnlineOdevMi == false) {
        data.MaxYuklemeSayisi = 0;
    }

    $.post("/Ders/Odev/OdevOlustur", { odev: data })
        .done(function (json) {
            if (json) {
                toastr.success("Ödev oluşturuldu");
                if ($("#rdGenelOdev").is(":checked")) {
                    $("#divOdevBaslik").text("Genel Ödev (bu dersin tüm grupları için geçerlidir)");
                }
                else {
                    $("#divOdevBaslik").text("Grup Ödevi");
                }
                $("#divOdevBaslik").show();
                $("#soruListe").load("/Ders/Odev/_pGetirOdev/" + dersGrupHocaID + "?P=" + FKPaylarID);

            }
            else {
                toastr.error("Hata oluştu");
            }
        });
});

$("#btnOdevSil").on("click", function () {
    if (confirm("Ödev silinecektir. Onaylıyor musunuz?")) {
        $.post("/Ders/Odev/OdevSil", { id: dersGrupHocaID, P: FKPaylarID })
            .done(function (json) {
                if (json) {
                    toastr.info("Ödev silindi");
                    $("#btnOdevOlustur").show();
                    $("#divOdevIslem").show();
                    $("#divOdevBaslik").hide();
                    $("#btnOdevSil").hide();
                    $("#btnOdevDetaylari").hide();
                    $("#soruListe").load("/Ders/Odev/_pGetirOdev/" + dersGrupHocaID + "?P=" + FKPaylarID);
                }
                else {
                    toastr.error("Hata oluştu");
                }
            });
    }
});

$("#rdOdev").click(function () {
    $("#yuklemeHakki").hide();
});

$("#rdOnlineOdev").click(function () {
    $("#yuklemeHakki").show();
});

$(document).on('click', '#haftaListe', function (e) {
    e.stopPropagation();
});