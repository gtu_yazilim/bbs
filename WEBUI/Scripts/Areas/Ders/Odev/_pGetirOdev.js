﻿// jQuery dependency
// jQuery multiselect dependency

dosyaKey = null;
dosyaBoyutu = null;
dosyaTuru = null;

var multiSelectProgramCiktilari = function (selector) {
    //$(".selectProgramCikti").multiselect({
    $(selector).multiselect({
        header: false,
        allSelectedText: 'Tümü seçildi',
        height: 400,
        //selectedList: 1,
        //numberDisplayed: 1,
        selectedText: '# adet seçildi',
        noneSelectedText: "Program Çıktısı Seçiniz",
        minWidth: 350,

        close: function () {
            var OlcmeID = $(this).attr("data-fkpaylarolcmeid");
            var pCiktiList = new Array();

            $(this).find("option:selected").filter(":not(option[disabled])").each(function () { //.find("[selected]:not(option[disabled])")
                if ($(this).parent().attr("class") == "optProgramCiktilari") {
                    pCiktiList.push($(this).val());
                }
            });

            if (pCiktiList.length > 0) {
                $.post("/Ders/Sinav/KaydetProgramCiktilari", { programCiktilari: pCiktiList, FKPaylarOlcmeID: OlcmeID }).success(function (json) {

                }).promise().done(function () {
                    yenile();
                });
            }
        }
    });
};

var multiSelectOgrenmeCiktilari = function (selector) {
    $(selector).multiselect({
        header: false,
        allSelectedText: 'Tümü seçildi',
        height: 400,
        //selectedList: 1,
        //numberDisplayed: 1,
        selectedText: '# adet seçildi',
        noneSelectedText: "Öğrenme Çıktısı Seçiniz",
        minWidth: 350,

        close: function () {
            var OlcmeID = $(this).attr("data-fkpaylarolcmeid");
            var oCiktiList = new Array();

            $(this).find("option:selected").filter(":not(option[disabled])").each(function () {
                if ($(this).parent().attr("class") == "optOgrenmeCiktilari") {
                    oCiktiList.push($(this).val());
                }
            });

            if (oCiktiList.length > 0) {
                $.post("/Ders/Sinav/KaydetOgrenmeCiktilari", { ogrenmeCiktilari: oCiktiList, FKPaylarOlcmeID: OlcmeID }).success(function (json) {

                }).promise().done(function () {
                    yenile();
                });
            }
        }
    });
};

$(document).ready(function () {
    multiSelectOgrenmeCiktilari(".selectOcikti");
    multiSelectProgramCiktilari(".selectPcikti");
});

$("#btnYeniSoruEkle").on("click", function () {
    $.post("/Ders/Odev/YeniSoruEkle", { fkAbsPaylarID: fkPaylarID })
     .done(function (json) {
         if (json) {
             toastr.success("İşlem tamamlandı");
             $("#soruListe").load("/Ders/Odev/_pGetirOdev/" + dersGrupHocaID + "?P=" + FKPaylarID);
         }
         else {
             toastr.error("Hata oluştu");
         }
     });
});

$(".txtSoruPuan").on("keyup", function () {
    $.post("/Ders/Odev/SoruPuanGuncelle", { fkAbsPaylarOlcmeID: $(this).attr("data-soruid"), SoruPuan: $(this).val() })
     .done(function (json) {
         if (json) {
             toastr.success("Puan bilgisi güncellendi");
         }
         else {
             toastr.error("Hata oluştu");
         }
     });
});

$(".btnOdevSoruSil").on("click", function () {
    $.post("/Ders/Odev/SoruSil", { fkAbsPaylarOlcmeID: $(this).attr("data-soruid") })
     .done(function (json) {
         if (json) {
             toastr.info("İşlem tamamlandı");
             $("#soruListe").load("/Ders/Odev/_pGetirOdev/" + dersGrupHocaID + "?P=" + FKPaylarID);
         }
         else {
             toastr.error("Hata oluştu");
         }
     });
});

$(function () {
    $('[data-toggle="tooltip"]').tooltip({
        position: { my: "top+15 center", at: "top center" }
    });
});