﻿// jQuery dependency

$(document).on('click', '#haftaListe', function (e) {
    e.stopPropagation();
});

$(document).on("click", ".pCiktiSil", function () {
    var ciktisil = $.post("/Ders/Sinav/SilProgramCikti", { ID: $(this).data("id") })
    .success(function (json) {
        $("#soruListe").load(soruURL);
    }).promise().done(function () {

    });
});

$(document).on("click", ".oCiktiSil", function () {
    var ciktisil = $.post("/Ders/Sinav/SilOgrenmeCikti", { ID: $(this).data("id") })
    .success(function (json) {
        $("#soruListe").load(soruURL);
    }).promise().done(function () {
    });
});
