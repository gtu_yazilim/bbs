﻿//jQuery dependency
//DataTables dependency
//jsRender dependency

$(document).ready(function () {

    "use strict";
    $.views.tags({
        range: {
            boundProps: ["start", "end"],

            baseTag: "for",

            render: function (val) {
                var array = val,
                    tagCtx = this.tagCtx,
                    start = tagCtx.props.start || 0,
                    end = tagCtx.props.end,
                    props = tagCtx.params.props;

                if (start || end) {
                    if (!tagCtx.args.length) {
                        array = [];
                        end = end || 0;
                        for (var i = start; i <= end; i++) {
                            array.push(i);
                        }
                    } else if ($.isArray(array)) {
                        array = array.slice(start, end);
                    }
                }
                return arguments.length || props && (props.start || props.end)
                    ? this.base(array)
                    : this.base();
            },

            onArrayChange: function (ev, eventArgs) {
                this.refresh();
            }
        }
    });
    $.views.settings.allowCode(true);

    var template = $.templates("myTmpl", '<tr>' +
        '{{* window.total = 0}}'+
        '<td> {{:Numara }}</td>' +
        '<td> {{:Ad }} {{:Soyad }}</td>' +
        '{{range OgrenciNotListesi start=0 end=SoruSayisi }}' +
            '{{if Notu == 120 }}' +
                '{{range start= 1 end=SoruSayisi }}' +
                    '<td><input type="text" class="txtNot" data-id="{{>~OgrenciID}}.{{>~root.PayID}}.{{:SoruNo}}" data-target="{{>~root.OgrenciID}}.{{>#parent.parent.data.PayID}}" data-payid="{{>#parent.parent.data.PayID}}" data-ogrenciid="{{>#parent.parent.data.OgrenciID}}" data-numara="{{>#parent.parent.data.Numara}}" data-soruno="{{:SoruNo}}" maxlength="4" value="GR" /></td>' +
                '{{/range}}' +
            '{{else Notu == 130 }}' +
                '{{range start= 1 end=SoruSayisi }}' +
                    '<td><input type="text" class="txtNot" data-id="{{>#parent.parent.data.OgrenciID}}.{{>#parent.parent.data.PayID}}.{{:SoruNo}}" data-target="{{>#parent.parent.data.OgrenciID}}.{{>#parent.parent.data.PayID}}" data-payid="{{>#parent.parent.data.PayID}}" data-ogrenciid="{{>#parent.parent.data.OgrenciID}}" data-numara="{{>#parent.parent.data.Numara}}" data-soruno="{{:SoruNo}}" maxlength="4" value="DZ" /></td>' +
                '{{/range}}' +
            '{{else}}' +
                //'{{* total+= :data.Notu }}'+
                '<td class="text-center"><input type="text" class="txtNot" data-id="{{>#parent.parent.parent.data.OgrenciID}}.{{>#parent.parent.parent.data.PayID}}.{{:SoruNo}}" data-target="{{>#parent.parent.parent.data.OgrenciID}}.{{>#parent.parent.parent.data.PayID}}" data-payid="{{>#parent.parent.data.PayID}}" data-ogrenciid="{{>#parent.parent.parent.data.OgrenciID}}" data-numara="{{>#parent.parent.parent.data.Numara}}" data-soruno="{{:SoruNo}}" maxlength="4" value="{{:Notu}}" /></td>' +
            '{{/if}}' +
        '{{else}}' +
            '{{range start=1 end=SoruSayisi ~outerData=#data}}'+
                '<td class="text-center"><input type="text" class="txtNot" data-id="{{:~outerData.OgrenciID}}.{{:~outerData.PayID}}.{{:#index+1}}" data-target="{{:~outerData.OgrenciID}}.{{:~outerData.PayID}}" data-payid="{{:~outerData.PayID}}" data-ogrenciid="{{:~outerData.OgrenciID}}" data-numara="{{:~outerData.Numara}}" data-soruno="{{:#index+1}}" maxlength="4" value="" /></td>'+
            '{{/range}}' +
        '{{/range}}'+
        '<td class="text-center"><span data-id="{{:OgrenciID}}.{{*:payID}}">{{*: total}}</span></td><td><button class="btn default" data-toggle="modal" data-size="60" data-source="/Ders/Sinav/_pGetirOgrenciSinavKagidi/{{*:dgHOCAID}}?P={{*:payID}}&ogrenciNo={{:KullaniciAdi}}">Sınav Kağıdı</button></td></tr>');

    $.post("/Ders/Sinav/OgrenciBilgileri", { listPayID: payID, id: dgHOCAID }).done(function (json) { console.log(json); $("#tblBaslik").html(BaslikEkle(json[0].SoruSayisi, json[0].OdevMi)); $("#tblIcerik").html(template.render(json)); }).promise().done(function () {

        $('#tblOgrenciListesi').DataTable({
            "fnInitComplete": function () {
                // Disable TBODY scoll bars
                //$('.dataTables_scrollBody').css({
                //    'overflow': 'hidden',
                //    'border': '0'
                //});

                // Enable TFOOT scoll bars
                $('.dataTables_scrollFoot').css('overflow', 'auto');

                $('.dataTables_scrollHead').css('overflow-x', 'auto');
                $('.dataTables_scrollHead').css('overflow-y', 'none');
                // Sync TFOOT scrolling with TBODY
                $('.dataTables_scrollFoot').on('scroll', function () {
                    $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
                });

                $('.dataTables_scrollHead').on('scroll', function () {
                    $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
                });
            },
            "scrollY": 400,
            "scrollX": true,
            "scrollCollapse": true,
            //"sScrollX": "190%",
            'bSort': false,
            searching: false,
            paging: false,
            info: false,
            columnDefs: [
                { className: 'fixedC', targets: 0 }
            ],
            //fixedColumns: {
            //    leftColumns: 2,
            //    "heightMatch": "none"
            //},
            fixedHeader: true,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Turkish.json"
            }
        });

        $(".txtNot").trigger("keyup");

        TxtAyar();
    });
});

function BaslikEkle(soruSayi, odevMi) {
    var kod = "";
    for (var z = 1; z < soruSayi + 1; z++) {
        kod += '<th class="col-md-1 text-center"><b>Soru ' + z + '</b></th>';
    }

    var baslik = "";
    baslik += '<tr class="active" style="height: 55px !important; max-height: 55px !important;"><th>Öğrenci NO</th><th style="left: 120px;">Ad Soyad</th>';
    if (odevMi) {
        baslik += '<th class="text-center">Ödev Açıklama</th><th class="text-center">Ödev Dosyası</th>';
    }
    baslik += kod + '<th class="col-md-1 text-center"><b>Toplam</b></th><th class="text-center"><b>Sınav Kağıdı</b></th></tr>';
    return baslik;
}

//input filters
var TxtAyar = function () {
    $(".txtNot").keydown(function (e) {

        if (e.keyCode == 37) { $(this).parent().prev().children("input").focus(); }
        if (e.keyCode == 38) { var index = $(this).parent().index(); $(this).parent().parent().prev().find('td').eq(index).children("input").focus(); }
        if (e.keyCode == 39) { $(this).parent().next().children("input").focus(); }
        if (e.keyCode == 40) { var index = $(this).parent().index(); $(this).parent().parent().next().find('td').eq(index).children("input").focus(); }

        if (($(this).val() == "GR" || $(this).val() == "DZ") && e.keyCode == 8) { $('[data-target="' + $(this).attr("data-target") + '"]').val(""); }
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 110]) !== -1 || (e.keyCode >= 35 && e.keyCode <= 39)) { return; }
        if (e.keyCode == 188 || e.keyCode == 190) { $(this).val($(this).val() + "."); }
        if (e.keyCode == 71) { $('[data-target="' + $(this).attr("data-target") + '"]').val("GR"); }
        if (e.keyCode == 68) { $('[data-target="' + $(this).attr("data-target") + '"]').val("DZ"); }
        if (e.keyCode == 189 || e.keyCode == 109) { $(this).val("-"); }

        $(this).val($(this).val().replace("..", "."));
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $(".txtNot").keyup(function (e) {
        var target = $(this).attr("data-target");
        var sum = 0;
        $('[data-target="' + target + '"]').each(function () {
            sum += Number($(this).val());
        });
        $('[data-id="' + target + '"]').html(sum);

        var val = $(this).val();
        val = val.substring(0, 2);
        if ((val == "GR" || val == "DZ") && e.keyCode != 8) { $(this).val(val); }
        if ($(this).val() < -10 || $(this).val() > 100) { $(this).val(""); toastr.error("0 ile 100 arasında bir sayı girilmelidir"); }
    });
}
//input filters

//kaydet
$("#BtnKaydet, #BtnKaydet2").click(function () {

    var ogrNotListesi = new Array();
    var satirList = $("#tblIcerik tr");

    $.each(satirList, function (i, item) {

        var notlar = $(item).find('input:not(.odevAciklama)');
        var grDurum = false;
        var dzDurum = false;
        $.each(notlar, function (k, item3) {
            if ($(this).val() == "GR" && grDurum == false) {
                grDurum = true;
            }
            if ($(this).val() == "DZ" && dzDurum == false) {
                dzDurum = true;
            }
        });

        $.each(notlar, function (j, item2) {
            var notTemp = $(this).val();
            if (grDurum == true) {
                notTemp = "GR";
            }
            if (dzDurum == true) {
                notTemp = "DZ";
            }

            var ogrNot = $(this).val();

            if (notTemp == "GR") {
                ogrNot = 120;
            }
            if (notTemp == "DZ") {
                ogrNot = 130;
            }
            var data = {
                Zaman: zaman,
                Kullanici: kullanici,
                Makina: makina,
                AktarilanVeriMi: false,
                SoruNo: $(this).attr("data-soruno"),
                Notu: String(ogrNot).replace(".", ","),
                FKOgrenciNotID: $(this).attr("data-ogrencinotid"),
                FKABSPaylarID: payID,
                FKOgrenciID: $(this).attr("data-ogrenciid"),
            };
            ogrNotListesi.push(data);
            if (notTemp == "GR" || notTemp == "DZ") // tek not gr veya dz ise diğerlerini eklemeye gerek yok
            {
                return false;
            }
        });
    });

    var aciklamaList = $(".odevAciklama");

    var ogrAciklamaList = [];

    $.each(aciklamaList, function (a, aObj) {
        var aData = {
            FKOgrenciID: $(aObj).attr("data-fkogrenciid"),
            FKPayID: $(aObj).attr("data-payid"),
            Aciklama: $(aObj).val()
        };
        ogrAciklamaList.push(aData);
    });

    $.post("/Ders/Sinav/SinavNotKaydet", { notListe: ogrNotListesi, id: dgHOCAID, aciklamaListe: ogrAciklamaList }).done(function (data) { data ? toastr.success("Not kayıt işlemleri tamamlandı.") : toastr.error("Not kayıt işlemleri sırasında hata oluştu."); });
});

$(".dataTables_scrollBody").scroll(function () {
    $("#dblScroll").scrollLeft($(".dataTables_scrollBody").scrollLeft())
});
//kaydet

//otomatik kaydet
var sure = 59;
$("#cbOtomatikKaydet").click(function () {
    if ($(this).is(":checked")) {
        otomatikKaydet();
        geriSayim();
    }
    else {
        clearTimeout(geriSay);
    }
});

function geriSayim() {
    var geriSay = setTimeout(function () {
        if ($("#cbOtomatikKaydet").is(":checked")) {
            $("#lblSure").html("Kalan süre: " + sure);
            sure--;
            if (sure < 1) { sure = 59; clearTimeout(geriSay); }
            geriSayim();
        }
        else {
            sure = 59;
            $("#lblSure").html("");
        }
    }, 1000);
}

function otomatikKaydet() {
    var otomatikKayit = setTimeout(function () {
        if ($("#cbOtomatikKaydet").is(":checked")) {
            otomatikKaydet();
            $("#BtnKaydet").trigger("click");
        }
    }, 60000);
}
//otomatik kaydet

$("#btnNotlariTemizle").click(function () {
    if (confirm("DİKKAT! Bu işlem geri alınamaz! Onaylıyor musunuz?")) {
        $.post("/Ders/Sinav/SilGrupNotlari", { FKDersGrupHocaID: dgHOCAID, FKABSPaylarID: payID })
            .success(function (json) {
                if (json) {
                    toastr.info("Not bilgileri silindi.");
                    window.setTimeout(function () {
                        window.location.href = "/Ders/Detay/Grup/" + dgHOCAID;
                    }, 3000);
                }
                else {
                    toastr.error("Hata oluştu.");
                }
            });
    }
});

$(document).on("click", ".cbSinavKagidiGoster", function () {
    $.post("/Ders/Sinav/SinavKagidiGosterGizle", { FKPaylarID: $(this).attr("data-fkabspaylarid"), FKDersGrupHocaID: dgHOCAID, durum: $(this).is(":checked"), FKOgrenciID: $(this).attr("data-ogrenciid") })
        .success(function (json) { json ? toastr.success("Sınav kağıdı güncellendi.") : toastr.error("Hata"); });
});

$(document).on("click", ".tumKagitlariGoster", function () {
    var durum = $(this).attr("data-durum");
    if (durum) {
        $('.cbSinavKagidiGoster').attr('checked', true);
    } else {
        $('.cbSinavKagidiGoster').attr('checked', false);
    }

    //$('.cbSinavKagidiGoster').prop('checked', $(this).attr("data-durum"));
    $.post("/Ders/Sinav/SinavKagidiGosterGizle", { FKPaylarID: $(this).attr("data-fkabspaylarid"), FKDersGrupHocaID: dgHOCAID, durum: $(this).attr("data-durum"), FKOgrenciID: null })
        .success(function (json) { json ? toastr.success("Sınav kağıtları güncellendi.") : toastr.error("Hata"); });
});