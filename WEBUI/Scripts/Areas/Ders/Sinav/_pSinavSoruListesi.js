﻿ //jQuery dependency
 //jQuery multiselect dependency

var yenile = function () {
    $("#tab_Cikti").load(soruURL);
};

var multiSelectProgramCiktilari = function (selector) {
    //$(".selectProgramCikti").multiselect({
    $(selector).multiselect({
        header: false,
        allSelectedText: 'Tümü seçildi',
        height: 400,
        //selectedList: 1,
        //numberDisplayed: 1,
        selectedText: '# adet seçildi',
        noneSelectedText: "Program Çıktısı Seçiniz",
        minWidth: 350,

        close: function () {
            var OlcmeID = $(this).attr("data-fkpaylarolcmeid");
            var pCiktiList = new Array();

            $(this).find("option:selected").filter(":not(option[disabled])").each(function () { //.find("[selected]:not(option[disabled])")
                if ($(this).parent().attr("class") == "optProgramCiktilari") {
                    pCiktiList.push($(this).val());
                }
            });

            if (pCiktiList.length > 0) {
                $.post("/Ders/Sinav/KaydetProgramCiktilari", { programCiktilari: pCiktiList, FKPaylarOlcmeID: OlcmeID }).success(function (json) {

                }).promise().done(function () {
                    $("#tab_Cikti").load(soruURL);
                });
            }
        }
    });
};

var multiSelectOgrenmeCiktilari = function (selector) {
    $(selector).multiselect({
        header: false,
        allSelectedText: 'Tümü seçildi',
        height: 400,
        //selectedList: 1,
        //numberDisplayed: 1,
        selectedText: '# adet seçildi',
        noneSelectedText: "Öğrenme Çıktısı Seçiniz",
        minWidth: 350,

        close: function () {
            var OlcmeID = $(this).attr("data-fkpaylarolcmeid");
            var oCiktiList = new Array();

            $(this).find("option:selected").filter(":not(option[disabled])").each(function () {
                if ($(this).parent().attr("class") == "optOgrenmeCiktilari") {
                    oCiktiList.push($(this).val());
                }
            });

            if (oCiktiList.length > 0) {
                $.post("/Ders/Sinav/KaydetOgrenmeCiktilari", { ogrenmeCiktilari: oCiktiList, FKPaylarOlcmeID: OlcmeID }).success(function (json) {

                }).promise().done(function () {
                    $("#tab_Cikti").load(soruURL);
                });
            }
        }
    });
};

$(document).on("click", ".pCiktiSil", function () {
    var ciktisil = $.post("/Ders/Sinav/SilProgramCikti", { ID: $(this).data("id") })
        .success(function (json) {
            $("#tab_Cikti").load(soruURL);
        }).promise().done(function () {
        });
});

$(document).on("click", ".oCiktiSil", function () {
    var ciktisil = $.post("/Ders/Sinav/SilOgrenmeCikti", { ID: $(this).data("id") })
        .success(function (json) {
            $("#tab_Cikti").load(soruURL);
        }).promise().done(function () {
        });
});

$(document).ready(function () {
    multiSelectOgrenmeCiktilari(".selectOcikti");
    multiSelectProgramCiktilari(".selectPcikti");

    $("#soruSayisi").html($("#tblSorular tr").length);
    $("#toplamPuan").html(sumTxt(".txtSoruPuan"));
});

$(document).on("keyup", ".txtSoruPuan", function () {
    toastr.remove();
    toastr.clear();

    $(this).val($(this).val().replace(".", ","));
    $("#toplamPuan").html(sumTxt(".txtSoruPuan"));

    var puanList = [];
    $("input.txtSoruPuan").each(function (i, item) {
        var data = {
            ID: $(item).attr("data-soruid"),
            SoruPuan: $(item).val()
        };
        puanList.push(data);
    });
    $.post("/Ders/Sinav/SoruPuanGuncelle", { puanList: puanList }).success(function (json) {
        if (json) {
            toastr.info("Puan güncelleme işlemi tamamlandı.");
        }
    });
});