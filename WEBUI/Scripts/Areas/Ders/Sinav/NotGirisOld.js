﻿//jQuery dependency
//DataTables dependency
//var paySoruListesi = [];
//document ready
$(document).ready(function () {
    //$.post("/Ders/Sinav/PaySinavSoruListesiJson/", { id: deger }).success(function (json) { paySoruListesi = json; });
    //var deger = '@TempData["degerler"]';
    if (payID == null || payID == "")
    {
        window.location.href = "/Ders/Detay/Grup/" + dgHOCAID;
    }
    else
    {
        var veri = { listPayID: payID, id: dgHOCAID };
        $.post("/Ders/Sinav/OgrenciBilgileri", veri)
        .success(function (liste) {
            console.log(liste);
            var soruSayi = 0;
            var odevMi = false;
            var sonuc = "";
            //injectJson(deger, liste);

            for (var i = 0; i < liste.length; i++) {
                sonuc += SatirEkle(liste[i].SoruSayisi, liste[i].OgrenciID, liste[i].Numara, liste[i].Ad, liste[i].Soyad, liste[i].OgrenciNotListesi, liste[i].PayID, liste[i].MutlakNot, liste[i].KullaniciAdi, liste[i].SinavKagidiGorebilirMi, liste[i].OdevMi, liste[i].OdevDosyaKey, liste[i].OdevDosyaAdi, liste[i].OdevAciklama);
                soruSayi = liste[i].SoruSayisi;
                odevMi = liste[i].OdevMi;
            }

            $("#tblIcerik").html(sonuc);
            $("#tblBaslik").html(BaslikEkle(soruSayi, odevMi));

            //if (injCounter > 0 && jsonStudents != null && isSame) {
            //    $.pnotify({ title: 'UYARI', text: 'Sisteme kaydedilmemiş değerler farklı renkte gözükmektedir.', type: 'success' });
            //    $(".ui-pnotify-history-container").remove();
            //}
        }).promise().done(function() {
            $('#tblOgrenciListesi').DataTable( {
                "fnInitComplete": function(){
                    // Disable TBODY scoll bars
                    //$('.dataTables_scrollBody').css({
                    //    'overflow': 'hidden',
                    //    'border': '0'
                    //});

                    // Enable TFOOT scoll bars
                    $('.dataTables_scrollFoot').css('overflow', 'auto');

                    $('.dataTables_scrollHead').css('overflow-x', 'auto');
                    $('.dataTables_scrollHead').css('overflow-y', 'none');
                    // Sync TFOOT scrolling with TBODY
                    $('.dataTables_scrollFoot').on('scroll', function () {
                        $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
                    });

                    $('.dataTables_scrollHead').on('scroll', function () {
                        $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
                    });
                },
                "scrollY": 400,
                "scrollX": true,
                "scrollCollapse": true,
                //"sScrollX": "190%",
                'bSort': false,
                searching: false,
                paging: false,
                info: false,
                columnDefs: [
                    { className: 'fixedC', targets: 0 }
                ],
                //fixedColumns: {
                //    leftColumns: 2,
                //    "heightMatch": "none"
                //},
                fixedHeader: true,
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Turkish.json"
                }
            });
            txtAyar();
            //$('#tblOgrenciListesi').doubleScroll();
            //console.info($(".dataTables_scrollBody").width());
        });

    }

});

function SatirEkle(SoruSayisi, OgrenciID, Numara, Ad, Soyad, Obje, PayID, MutlakNot, KullaniciAdi, SinavKagidiGorebilirMi, OdevMi, OdevDosyaKey, OdevDosyaAdi, OdevAciklama) {

    var Notu = "";
    var OgrenciNotID = 0;
    var kod = "";
    var geciciSatir = "";
    var satir = "";
    var notToplam = 0;

    if (SoruSayisi < 1) {
        
        window.location.href = '/Ders/Detay/Grup/' + dgHOCAID;
        toastr.info("Not Girilmeden Önce Aktivite Oluşturulmalıdır.");
    }

    if (OdevMi) {
        if (OdevDosyaKey != null && OdevDosyaKey != "") {
            if (OdevAciklama == null) {
                OdevAciklama = "";
            }
            geciciSatir += '<td class="text-center"><input type="text" class="form-control odevAciklama" data-fkogrenciid="' + OgrenciID + '" data-payid="' + PayID + '" value="' + OdevAciklama + '" /></td>';
            geciciSatir += '<td class="text-center"><a href="/Ders/Odev/Indir?key=' + OdevDosyaKey + '" target="_blank">Ödev Dosyasını İndir</a></td>';
        }
        else {
            geciciSatir += '<td class="text-center">-</td><td class="text-center">-</td>';
        }
    }

    for (var x = 0; x < SoruSayisi; x++) {
        for (var i = 0; i < SoruSayisi; i++) {
            if (Obje[i] !== undefined) {
                if (Obje[i].SoruNo == Number(x+1)) {
                    Notu = Obje[i].Notu;
                    if(Notu == 120)
                    {
                        Notu = "GR";
                    }
                    if(Notu == 130)
                    {
                        Notu = "DZ";
                    }
                    OgrenciNotID: Obje[i].FKOgrenciNotID;
                }
            }

        }

        if(MutlakNot == 120)
        {
            geciciSatir += '<td class="text-center not"><input type="text" class="txtNot" data-target=' + OgrenciID + '.' + PayID + ' id="' + OgrenciID + '.' + PayID + '.' + x + '" data-id="' + OgrenciID + '.' + PayID + '.' + x + '" data-soruno="' + Number(x + 1) + '" data-ogrenciID="' + OgrenciID + '" data-numara="' + Numara + '"  data-payID="' + PayID + '" data-fkogrencinotid="' + OgrenciNotID + '" data-rangelength="[0,100]" maxlength="4" value="' + "GR" + '" data-durum="true"/></td>';
        }
        if(MutlakNot == 130)
        {
            geciciSatir += '<td class="text-center not"><input type="text" class="txtNot" data-target=' + OgrenciID + '.' + PayID + ' id="' + OgrenciID + '.' + PayID + '.' + x + '" data-id="' + OgrenciID + '.' + PayID + '.' + x + '" data-soruno="' + Number(x + 1) + '" data-ogrenciID="' + OgrenciID + '" data-numara="' + Numara + '"  data-payID="' + PayID + '" data-fkogrencinotid="' + OgrenciNotID + '" data-rangelength="[0,100]" maxlength="4" value="' + "DZ" + '" data-durum="true"/></td>'; // disabled
        }
        if(MutlakNot != 120 && MutlakNot != 130)
        {
            geciciSatir += '<td class="text-center not"><input type="text" class="txtNot" data-target=' + OgrenciID + '.' + PayID + ' id="' + OgrenciID + '.' + PayID + '.' + x + '" data-id="' + OgrenciID + '.' + PayID + '.' + x + '" data-soruno="' + Number(x + 1) + '" data-ogrenciID="' + OgrenciID + '" data-numara="' + Numara + '"  data-payID="' + PayID + '" data-fkogrencinotid="' + OgrenciNotID + '" data-rangelength="[0,100]" maxlength="4" value="' + Notu + '" data-durum="true"/></td>';
        }
        notToplam = Number(notToplam + Notu);
        if(isNaN(notToplam))
        {
            notToplam = 0;
        }
        if(MutlakNot == 120)
        {
            notToplam = "GR";
        }
        if(MutlakNot == 130)
        {
            notToplam = "DZ";
        }
        Notu = "";
    }

    kod = kod + geciciSatir;
    var toplam = '<td class="col-md-1 text-center" style="font-size: 18px;"><span data-id="' + OgrenciID + '.' + PayID + '">' + notToplam + '</span></td></td>'; //<td><input type="checkbox" class="form-control cbSinavKagidiGoster" data-ogrenciid="' + OgrenciID + '" data-fkabspaylarid="' + PayID + '" ' + boolReplace(SinavKagidiGorebilirMi) + '/>
    geciciSatir = "";

    satir = '<tr><td class="text-left">' + Numara + '</td><td class="text-left" style="left: 120px;"<span class="pull-left">' + Ad + ' ' + Soyad + '</span></td>' + kod + toplam + '<td class="text-center"><button class="btn default" data-toggle="modal" data-title="' + Ad + ' ' + Soyad + '(' + Numara + ') Sınav Kağıdı" data-size="50" data-source="/Ders/Sinav/_pGetirOgrenciSinavKagidi/' + dgHOCAID + '?P=' + PayID + '&ogrenciNo=' + KullaniciAdi + '">Sınav Kağıdı</button></td></tr>';
    return satir;
}

function BaslikEkle(soruSayi, odevMi) {
    var kod = "";
    for (var z = 1; z < soruSayi + 1; z++) {
        kod += '<th class="col-md-1 text-center"><b>Soru ' + z + '</b></th>';
    }

    var baslik = "";
    baslik += '<tr class="active" style="height: 55px !important; max-height: 55px !important;"><th>Öğrenci NO</th><th style="left: 120px;">Ad Soyad</th>';
    if (odevMi) {
        baslik += '<th class="text-center">Ödev Açıklama</th><th class="text-center">Ödev Dosyası</th>';
    }
    baslik += kod + '<th class="col-md-1 text-center"><b>Toplam</b></th><th class="text-center"><b>Sınav Kağıdı</b></th></tr>';
    return baslik;
}
//document ready

//excel not giriş
$(function () {
    'use strict';
    $('#fileupload').fileupload({
        url: '/Ders/Sinav/ExcelUpload',
        dataType: 'json',
        done: function (event, data) {
            $.each(data.result, function(index, value) {
                $.each(value.OgrenciNotListesi, function(subIndex, subValue) {
                    if(subValue.Notu == 120)
                    {
                        $('input[data-numara="' + value.Numara +'"][data-soruno="' + subValue.SoruNo + '"]').val("GR");
                    }
                    if(subValue.Notu == 130)
                    {
                        $('input[data-numara="' + value.Numara +'"][data-soruno="' + subValue.SoruNo + '"]').val("DZ");
                    }
                    if(subValue.Notu >= -10 && subValue.Notu <= 100)
                    {
                        $('input[data-numara="' + value.Numara +'"][data-soruno="' + subValue.SoruNo + '"]').val(subValue.Notu);
                    }
                    else
                    {
                        subValue.Notu = "";
                    }
                });
            });
            $("input").each(function() {
                $(this).trigger("keyup");
            });
            toastr.info("Notlar aktarılmıştır. Kontrol ettikten sonra kaydedebilirsiniz.");
        },
        fail: function (event, data) {
            toastr.error("Notlar aktarılırken bir hata oluştu.");
            console.log(data);
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});
//excel not giriş

//kaydet
$("#BtnKaydet, #BtnKaydet2").click(function () {
    $("#BtnKaydet, #BtnKaydet2").attr("disabled", true);
    var listPostEdilecek = new Array();
    var satirList = $("#tblIcerik tr");

    $.each(satirList, function (i, item) {

        var notlar = $(item).find('input:not(.odevAciklama)');
        var grDurum = false;
        var dzDurum = false;
        $.each(notlar, function (k, item3) {
            if ($(this).val() == "GR" && grDurum == false) {
                grDurum = true;
            }
            if ($(this).val() == "DZ" && dzDurum == false) {
                dzDurum = true;
            }
        });

        $.each(notlar, function (j, item2) {
            //var soruNO = 1;
            var notTemp = $(this).val();
            if (grDurum == true) {
                notTemp = "GR";
            }
            if (dzDurum == true) {
                notTemp = "DZ";
            }

            var ogrNot = $(this).val();

            if (notTemp == "GR") {
                ogrNot = 120;
            }
            if (notTemp == "DZ") {
                ogrNot = 130;
            }
            var data = {
                Zaman: zaman,
                Kullanici: kullanici,
                Makina: makina,
                AktarilanVeriMi: false,
                //SoruNo: soruNO,
                SoruNo: $(this).attr("data-soruno"),
                Notu: String(ogrNot).replace(".", ","),
                FKOgrenciNotID: $(this).attr("data-ogrencinotid"),
                FKABSPaylarID: $(this).attr("data-payid"),
                FKOgrenciID: $(this).attr("data-ogrenciid"),
            };
            listPostEdilecek.push(data);
            //soruNO = soruNO + 1;
            if (notTemp == "GR" || notTemp == "DZ") // tek not gr veya dz ise diğerlerini eklemeye gerek yok
            {
                return false;
            }
        });
    });

    var aciklamaList = $(".odevAciklama");

    var aciklamaPostEdilecek = [];

    $.each(aciklamaList, function (a, aObj) {
        var aData = {
            FKOgrenciID: $(aObj).attr("data-fkogrenciid"),
            FKPayID: $(aObj).attr("data-payid"),
            Aciklama: $(aObj).val()
        };
        aciklamaPostEdilecek.push(aData);
    });

    $.post("/Ders/Sinav/SinavNotKaydet", { notListe: listPostEdilecek, id: dgHOCAID, aciklamaListe: aciklamaPostEdilecek })
        .success(function (data) {
            //console.log(data);
            if (data == "true") {
                toastr.success("Not kayıt işlemleri tamamlandı.");
            }
            else {
                toastr.error("Not kayıt işlemleri sırasında hata oluştu.");
            }
            $("#BtnKaydet, #BtnKaydet2").attr("disabled", false);
        });
});

$(".dataTables_scrollBody").scroll(function () {
    $("#dblScroll").scrollLeft($(".dataTables_scrollBody").scrollLeft())
});
//kaydet

//otomatik kaydet
var sure = 59;
$("#cbOtomatikKaydet").click(function () {
    if ($(this).is(":checked")) {
        otomatikKaydet();
        geriSayim();
    }
    else {
        clearTimeout(geriSay);
    }
});

function geriSayim() {
    var geriSay = setTimeout(function () {
        if ($("#cbOtomatikKaydet").is(":checked")) {
            $("#lblSure").html("Kalan süre: " + sure);
            sure--;
            if (sure < 1) { sure = 59; clearTimeout(geriSay); }
            geriSayim();
        }
        else {
            sure = 59;
            $("#lblSure").html("");
        }
    }, 1000);
}

function otomatikKaydet() {
    var otomatikKayit = setTimeout(function () {
        if ($("#cbOtomatikKaydet").is(":checked")) {
            otomatikKaydet();
            $("#BtnKaydet").trigger("click");
        }
    }, 60000);
}
//otomatik kaydet

//input filters
var txtAyar = function () {
    $(".txtNot").keydown(function (e) {

        if (($(this).val() == "GR" || $(this).val() == "DZ") && e.keyCode == 8) { $('[data-target="' + $(this).attr("data-target") + '"]').val(""); }
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 110]) !== -1 || (e.keyCode >= 35 && e.keyCode <= 39)) { return; }
        if (e.keyCode == 188 || e.keyCode == 190) { $(this).val($(this).val() + "."); }
        if (e.keyCode == 71) { $('[data-target="' + $(this).attr("data-target") + '"]').val("GR"); }
        if (e.keyCode == 68) { $('[data-target="' + $(this).attr("data-target") + '"]').val("DZ"); }
        if (e.keyCode == 189 || e.keyCode == 109) { $(this).val("-"); }

        $(this).val($(this).val().replace("..", "."));
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $(".txtNot").keyup(function (e) {
        var target = $(this).attr("data-target");
        var sum = 0;
        $('[data-target="' + target + '"]').each(function () {
            sum += Number($(this).val());
        });
        $('[data-id="' + target + '"]').html(sum);

        var val = $(this).val();
        val = val.substring(0, 2);
        if ((val == "GR" || val == "DZ") && e.keyCode != 8) { $(this).val(val); }
        if ($(this).val() < -10 || $(this).val() > 100) { $(this).val(""); toastr.error("0 ile 100 arasında bir sayı girilmelidir"); }
    });
}
//input filters

$("#btnNotlariTemizle").click(function () {
    if (confirm("DİKKAT! Bu işlem geri alınamaz! Onaylıyor musunuz?")) {
        $.post("/Ders/Sinav/SilGrupNotlari", { FKDersGrupHocaID: dgHOCAID, FKABSPaylarID: payID })
        .success(function (json) {
            if (json) {
                toastr.info("Not bilgileri silindi.");
                window.setTimeout(function () {
                    window.location.href = "/Ders/Detay/Grup/" + dgHOCAID;
                }, 3000);
            }
            else {
                toastr.error("Hata oluştu.");
            }
        });
    }
});

$(document).on("click", ".cbSinavKagidiGoster", function () {
    $.post("/Ders/Sinav/SinavKagidiGosterGizle", { FKPaylarID: $(this).attr("data-fkabspaylarid"), FKDersGrupHocaID: dgHOCAID, durum: $(this).is(":checked"), FKOgrenciID: $(this).attr("data-ogrenciid") })
     .success(function (json) { json ? toastr.success("Sınav kağıdı güncellendi.") : toastr.error("Hata"); });
});

$(document).on("click", "#cbSinavKagitGosterGizle", function() {
    $.post("/Ders/Sinav/SinavKagidiGosterGizle", { FKPaylarID: payID, FKDersGrupHocaID: dgHOCAID, durum: $(this).is(":checked"), FKOgrenciID: null })
        .success(function(json) { json ? toastr.success("Sınav kağıtları güncellendi.") : toastr.error("Hata"); });
});

//$(document).on("click", ".tumKagitlariGoster", function () {
//    var durum = $(this).attr("data-durum");
//    if (durum) {
//        $('.cbSinavKagidiGoster').attr('checked', true);
//    } else {
//        $('.cbSinavKagidiGoster').attr('checked', false);
//    }

//    //$('.cbSinavKagidiGoster').prop('checked', $(this).attr("data-durum"));
//    $.post("/Ders/Sinav/SinavKagidiGosterGizle", { FKPaylarID: $(this).attr("data-fkabspaylarid"), FKDersGrupHocaID: dgHOCAID, durum: $(this).attr("data-durum"), FKOgrenciID: null })
//     .success(function (json) { json ? toastr.success("Sınav kağıtları güncellendi.") : toastr.error("Hata"); });
//});