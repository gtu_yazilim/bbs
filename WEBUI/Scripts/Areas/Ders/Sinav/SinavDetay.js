﻿// jQuery dependency

$(document).on('click', '#haftaListe', function (e) {
    e.stopPropagation();
});

$(document).ready(function () {
    $("#soruListe").load(soruURL);

    $.post("/Ders/Sinav/GetirSinavTuru", { id: dersGrupHocaID, P: FKPaylarID })
     .done(function (json) {
         $("#sinavTuru").append("<h4><span class='label label-primary'>" + json + "</span></h4>");
         if (json.length > 1) {
             $("#btnSnv").hide();
             $("#btnSinavSil").show();
             $("#btnPuanGuncelle").show();
             $("#btnPuanGuncelle_tabCikti").show();
         }
         else {
             $("#btnSnv").show();
             $("#btnSinavSil").hide();
             $("#btnPuanGuncelle").hide();
             $("#btnPuanGuncelle_tabCikti").hide();

             $("#ProgramCikti").hide();
             $("#CevapAnahtari").hide();
             $("#KagitGirisi").hide();
             $("#OnlineSinavIslemleri").hide();
             $("#SinavIstatistik").hide();
         }
         if (json == "TEST" || json == "KARMA") {
             $("#btnCevapAnahtari").show();
             $("#OnlineSinavIslemleri").hide();
             
         }
         else if (json == "KLASIK") {       
             $("#CevapAnahtari").hide();
             //$("#KagitGirisi").hide();
             $("#OnlineSinavIslemleri").hide();
             
         }
         else if (json == "ONLINESINAV") {
             $("#btnOnlineSinavIslem").show();
             $("#CevapAnahtari").hide();
             $("#KagitGirisi").hide();
         }
     });

     $.post("/Ders/Sinav/GetirOnlineSinavTuru", { hocaid: dersGrupHocaID, payid: FKPaylarID })
     .done(function (json) {
         if (json == 1)
         {
             $('#AktiviteTur').show();
             $('#AktiviteTur').append("<br/><h4>Bu sınav <span class='label label-primary'>Sınıf Aktivitesi</span> olarak oluşturulmuştur.</h4>");
         }
         else if (json == 2)
         {
             $('#AktiviteTur').show();
             $('#AktiviteTur').append("<br/><h4>Bu sınav <span class='label label-primary'>Genel Aktivite</span> olarak oluşturulmuştur.</h4>");

         }     
     });
});

var yenile = function () {
    $("#soruListe").load(soruURL);
};



$("#btnSinavOlustur").click(function () {

    var soruSayisi = $("#txtSoruSayisi").val();
    var klasikSoruSayisi = $("#txtKlasikSoruSayisi").val();
    var ortalamaPuan = parseFloat(100 / soruSayisi);
    
    if (Math.floor(soruSayisi) == soruSayisi && $.isNumeric(soruSayisi) && soruSayisi <= 100) {
        var data = {
            soruSayisi: soruSayisi,
            FKPaylarID: FKPaylarID,
            FKKoordinatorID: FKKoordinatorID,
            FKDersGrupID: FKDersGrupID,
            dersGrupHocaID: dersGrupHocaID,
            sinavTuru: $('input[name=sinavTur]:checked').val(),
            klasikSoruSayisi: klasikSoruSayisi,
            OnlineAktiviteTip : $('input[name=aktiviteTur]:checked').val()
        };

        setTimeout(function () { //firefox ta object tamamlanmadan post etmeye çalışıyor bu nedenle işlem tamamlanamıyor
            $.post("/Ders/Sinav/SinavOlustur", data)
                .success(function (json) {
                    if (json) {
                        toastr.success("Sınav oluşturuldu.");
                        $("#soruListe").load(soruURL);
                        $.post("/Ders/Sinav/GetirSinavTuru", { id: dersGrupHocaID, P: FKPaylarID }).done(function (json) {
                            $("#sinavTuru").text(json);
                            if (json == "TEST" || json == "KARMA") {
                                $("#btnCevapAnahtari").show();
                            }
                            if (json == "ONLINESINAV") {
                                $("#btnOnlineSinavIslem").show();
                            }
                        });
                        $("#btnSnv").hide();
                        $("#btnSinavSil").show();
                        $("#btnPuanGuncelle").show();
                        $("#btnPuanGuncelle_tabCikti").show();
                        location.reload();
                    } else {
                        toastr.error("Bir hata oluştu.");
                    }
                });
        }, 250);
    }
    else {
        toastr.error("Soru sayısı 100 den küçük bir tam sayı olmalıdır!");
    }
    
});

$("#btnSinavSil").click(function () {
    var onay = confirm("Sınav soruları silinecektir. Onaylıyor musunuz?");
    if (onay) {
        setTimeout(function () {
            $.post("/Ders/Sinav/SinavSil", { FKPaylarID: FKPaylarID, FKDersGrupID: FKDersGrupID, FKKoordinatorID: FKKoordinatorID, dersGrupHocaID: dersGrupHocaID })
                .success(function (json) {
                    $("#onlineicerik").css({ 'display': 'none' });

                    if (json) {
                        $("#soruListe").load(soruURL);
                        toastr.info("Sınav silindi.");
                        $("#btnSnv").show();
                        $("#btnSinavSil").hide();
                        $("#btnPuanGuncelle").hide();
                        $("#btnPuanGuncelle_tabCikti").hide();
                        $("#btnCevapAnahtari").hide();
                        $("#btnOnlineSinavIslem").hide();
                        $("#onlineicerik").css({ 'display': 'none' });
                        $("#AktiviteTur").css({ 'display': 'none' });
                    }
                    else {
                        $("#soruListe").load(soruURL);
                        toastr.info("Bir hata oluştu.");
                    }
                });
        }, 250);
        
    }
    location.reload();
});