﻿// jQuery dependency

var yenile;

function refresh() {
    blockStatus = false;
    yenile = setTimeout(function () {
        $("#dListe").load(url);
        refresh();
    }, 20000);
}

$(document).ready(function () {
    refresh();
    $("#btnSonuclariDegerlendir").click(function () {
        $.blockUI({ message: "Lütfen bekleyin...", css: { fontSize: '30px' } });
        $.post("/Ders/Sinav/SinavDegerlendir", { FKSinavID: FKSinavID, FKPaylarID: FKPaylarID, FKDersGrupHocaID: FKDersGrupHocaID })
         .success(function (json) {
             if (json) {
                 toastr.success("Öğrenci notları aktarıldı.");
             } else {
                 toastr.error("Bir hata oluştu");
             }
             $.unblockUI();
         });
    });

    $("#dListe").load(url);
    var dosyaSayisi = 1;
    'use strict';
    $('#fileupload,#btnOptikYukle').fileupload({
        url: '/Ders/Sinav/Upload',
        dataType: 'json',
        formData: { FKPaylarID: FKPaylarID, FKDersGrupID: FKDersGrupID },
        done: function (e, data) {
            if (dosyaSayisi == 1) {
                $.post("/Ders/Sinav/GuncelleSinavDurum", { FKSinavID: FKSinavID, Durum: 0 });
                clearTimeout(yenile);
            }

            $("#dosyaSayisi").show().html(dosyaSayisi + " adet dosya yüklendi");
            dosyaSayisi++;
            
        },
        progressall: function (e, data) {
            $("#progress").show();
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css('width', progress + '%');
        },
        stop: function (e) {
            $.blockUI({ message: "Lütfen bekleyin...", css: { fontSize: '30px' } });
            $.post("/Ders/Sinav/GuncelleSinavDurum", { FKSinavID: FKSinavID, Durum: 1 })
            .done(function () {
                refresh();
                $("#dListe").load(url);
                $.unblockUI();
            });
            $("#progress").hide();
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

});

$(document).on("click", ".btnSil", function () {
    var kListe = [];
    var obj = { ID: $(this).attr("data-id") };
    kListe.push(obj);
    $.post("/Ders/Sinav/SilSinavKagidi", { sinavKagitListesi: kListe })
     .done(function (json) {
         $("#dListe").load(url);
         if (json) {
             toastr.info("Dosya silindi");
         }
     });
});

$("#btnKagitlariTemizle").click(function () {
    if (confirm("Tüm sınav kağıtları silinecektir! Bu işlem geri alınamaz. Onaylıyor musunuz?")) {
        $.post("/Ders/Sinav/SilSinavKagitlari", { FKSinavID: FKSinavID, FKDersGrupID: FKDersGrupID })
            .success(function (json) {
                if (json) {
                    $("#dListe").load(url);
                    toastr.info("Sınav kağıtları silindi");
                }
                else {
                    toastr.error("Hata oluştu");
                }
            });
    }
});