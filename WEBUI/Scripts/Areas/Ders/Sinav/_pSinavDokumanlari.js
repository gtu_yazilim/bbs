﻿//jQuery
//jQuery file upload

'use strict';
$('#fileUploadSinavKagidi').fileupload({
    url: '/Ders/Sinav/SinavDokumanUpload',
    dataType: 'json',
    formData: { FKPaylarID: fkPaylarID, FKDersGrupID: fkDersGrupID, enumDokumanTuru: 1 },
    done: function (e, data) {
        if (data.result) {
            toastr.success("Yükleme başarılı");
            $("#divSinavDokumanlari").load("/Ders/Sinav/_pSinavDokumanlari?FKPaylarID=" + fkPaylarID + "&FKDersGrupID=" + fkDersGrupID)
        }
        else {
            toastr.error("Hata oluştu");
        }
    },

    stop: function (e) {

    }
}).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');

'use strict';
$('#fileUploadCevapAnahtari').fileupload({
    url: '/Ders/Sinav/SinavDokumanUpload',
    dataType: 'json',
    formData: { FKPaylarID: fkPaylarID, FKDersGrupID: fkDersGrupID, enumDokumanTuru: 2 },
    done: function (e, data) {
        if (data.result) {
            toastr.success("Yükleme başarılı");
            $("#divSinavDokumanlari").load("/Ders/Sinav/_pSinavDokumanlari?FKPaylarID=" + fkPaylarID + "&FKDersGrupID=" + fkDersGrupID)
        }
        else {
            toastr.error("Hata oluştu");
        }
    },

    stop: function (e) {

    }
}).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');

$(".btnSinavDokumanSil").on("click", function () {
    if (confirm("Silme işlemini onaylıyor musunuz?")) {
        $.post("/Ders/Sinav/SinavDokumanSil", { fkSinavDokumanID: $(this).attr("data-id") })
            .done(function (json) {
                if (json) {
                    toastr.info("Silindi.");
                    $("#divSinavDokumanlari").load("/Ders/Sinav/_pSinavDokumanlari?FKPaylarID=" + fkPaylarID + "&FKDersGrupID=" + fkDersGrupID)
                }
                else {
                    toastr.error("Hata oluştu.");
                }
            });
    }
});