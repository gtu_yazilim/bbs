﻿// jQuery dependency
// isotope dependency

$(document).ready(function () {

    $.post("/DersProgrami/GetirOgretimElemaniDersProgrami", { fkDersGrupHocaID: dgHOCAID })
        .success(function (json) {
            var tarih = new Date();
            $.each(json, function (i, obj) {
                var start = new Date(parseInt(obj.start.substr(6))).toLocaleDateString();
                var today = JSONdatefixer(obj.start);
                if (tarih.toLocaleDateString() == start) {

                    $("#haftaListe").append('<li><a style="color:red;" href="/Rapor/YoklamaListe/' + dgHOCAID + '?tarih=' + today + '"><b>' + today + '</b></a></li>');
                }
                else {
                    $("#haftaListe").append('<li><a href="/Rapor/YoklamaListe/' + dgHOCAID + '?tarih=' + today + '">' + today + '</a></li>');
                }
            });
        });

    $("#btn5HaftaExcel").click(function () {
        ustTabloOlustur("#ustTablo");

        //öğrenci listesi tablo
        $("#altTablo").append('<tr class="col-md-12"><td class="col-md-3">NUMARA</td><td class="col-md-4">AD SOYAD</td><td>.. / .. / ....</td><td>.. / .. / ....</td><td>.. / .. / ....</td><td>.. / .. / ....</td><td>.. / .. / ....</td></tr>');

        $(".ogrenci").each(function () {
            var ogrenciBilgi = $(this).html();
            var ogrenciBilgiYeni = ogrenciBilgi.split("<br>");
            $("#altTablo").append('<tr><td>="' + ogrenciBilgiYeni[1] + '"</td><td>' + ogrenciBilgiYeni[0] + '</td><td></td><td></td><td></td><td></td><td></td></tr>');
        });
        //öğrenci listesi tablo
        tableToExcel('btn5HaftaExcel', 'tblOgrenciListesi', '5 Haftalık Öğrenci Devam Listesi', "5 Haftalık Öğrenci Devam Listesi.xls");
    });

    $("#btn15HaftaExcel").click(function () {

        ustTabloOlustur("#ustTablo15");

        //öğrenci listesi tablo
        $("#altTablo15").append('<tr class="col-md-12"><td class="col-md-3">NUMARA</td><td class="col-md-4">AD SOYAD</td><td>1</td><td>2</td><td>3</td><td>4</td><td>5</td><td>6</td><td>7</td><td>8</td><td>9</td><td>10</td><td>11</td><td>11</td><td>12</td><td>13</td><td>14</td><td>15</td></tr>');

        $(".ogrenci").each(function () {
            var ogrenciBilgi = $(this).html();
            var ogrenciBilgiYeni = ogrenciBilgi.split("<br>");
            $("#altTablo15").append('<tr><td>="' + ogrenciBilgiYeni[1] + '"</td><td>' + ogrenciBilgiYeni[0] + '</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>');
        });
        //öğrenci listesi tablo
        tableToExcel('btn15HaftaExcel', 'tblOgrenciListesi15', '15 Haftalık Öğrenci Devam Listesi', "15 Haftalık Öğrenci Devam Listesi.xls");
    });
});

var ustTabloOlustur = function(tabloID)
{
    //ders bilgileri tablo
    $(tabloID).append('<tr class="col-md-12"><td>Dersin Adı</td><td>' + dersAd + '</td><td>Öğretim Türü</td><td>' + ogrtmTur + '</td></tr><tr class="col-md-12"><td>Kodu / Saati</td><td>' + dersKod + ' / ' + dersSaati + '</td><td>Öğretim Yılı</td><td>' + yil + '</td></tr><tr class="col-md-12"><td>Öğretim Üyesi</td><td>' + ''  + '</td><td>Dönemi</td><td>' + donem + '</td></tr><tr></tr>');
    //ders bilgileri tablo
};

var tableToExcel = (function () {
    var uri = 'data:application/vnd.ms-excel;base64,'
      , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><?xml version="1.0" encoding="UTF-8" standalone="yes"?><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
      , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
      , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
    return function (btnID, table, name, filename) {
        if (!table.nodeType) table = document.getElementById(table)
        var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

        document.getElementById(btnID).href = uri + base64(format(template, ctx));
        document.getElementById(btnID).download = filename;
        //window.location.href = uri + base64(format(template, ctx))
        //document.getElementById(btnID).click();
    }
})();

var filterFns = {
    // show if number is greater than 50
    numberGreaterThan50: function () {
        var number = $(this).find('.number').text();
        return parseInt(number, 10) > 50;
    },
    // show if name ends with -ium
    ium: function () {
        var name = $(this).find('.name').text();
        return name.match(/ium$/);
    }
};

var $grid = $('.ISOTOPE').isotope({
    itemSelector: '.istp',
    //percentPosition: true,
    //stagger: 30,
    containerStyle: null,
    //initLayout: false
    layoutMode: 'fitRows'
});

$('#filters').on('click', '.btn', function () {
    var filterValue = $(this).attr('data-filter');
    // use filterFn if matches value
    filterValue = filterFns[filterValue] || filterValue;
    $grid.isotope({ filter: filterValue });
});

$('.button-group').each(function (i, buttonGroup) {
    var $buttonGroup = $(buttonGroup);
    $buttonGroup.on('click', 'button', function () {
        $buttonGroup.find('.is-checked').removeClass('is-checked');
        $(this).addClass('is-checked');
    });
});

if (navigator.userAgent.indexOf("Edge") > -1 || navigator.userAgent.indexOf("MSIE") > -1 || navigator.userAgent.indexOf("Trident") > -1) {
    $("#btn5HaftaExcel").hide();
    $("#btn15HaftaExcel").hide();
}