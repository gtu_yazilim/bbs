﻿// jQuery dependency

$("#BtnYazdir").click(function () {
    var listPost = new Array();
    var payListPost = "";
    $("input:checkbox[name=ders]:checked").each(function () {
        listPost.push($(this).val());
        payListPost += $(this).val() + "-";
    });
    var str = payListPost;
    var res = str.substr(0, payListPost.length - 1);

    if (res.length < 1) {
        toastr.error("Yazdırmak için bir çalışma tipi seçilmelidir.")
    }
    else {
        $("#BtnYazdir").attr("href", "/Rapor/NotListesi?id=" + dersGrupHocaID + "&payIDList=" + res);
        $("#BtnYazdir").trigger("click");
    }
});

$('.btnEskiNotGiris').click(function () {

    var degerler = "";
    var degerlerAd = "";
    var payIDList = [];
    var payNameList = [];
    var payDurum = [];
    var cbs = $(".cbDers:checked");

    payIDList.push($(this).data("payid"));
    payNameList.push($(this).data("payadi"));
    payDurum.push($(this).data("paydurum"));

    if (payIDList.length > 0) {
        veriYolla(payIDList.toString().split(',').join('-'), payNameList.toString().split(',').join('-'), payDurum.toString().split(',').join('-'));
    }
    else {
        toastr.warning('Not girişi için bir çalışma tipi seçiniz');
    }

});

$('#BtnNotGir').click(function () {

    

    var degerler = "";
    var degerlerAd = "";
    var payIDList = [];
    var payNameList = [];
    var payDurum = [];
    var cbs = $(".cbDers:checked");

    //console.log(sonHalVerildiMi);

    $(".cbDers:checked").each(function () {
        //console.log(sonHalVerildiMi);
        //console.log($(this).attr("data-enumcalismatipi"));
        //console.log(typeof sonHalVerildiMi);
        if (sonHalVerildiMi == "true" && $(this).attr("data-enumcalismatipi") != 13) {

        }
        else {
            payIDList.push($(this).data("payid"));
            payNameList.push($(this).data("payadi"));
            payDurum.push($(this).data("paydurum"));
        }
    });
    //console.log(payIDList);
    if (payIDList.length > 0) {
        veriYolla(payIDList.toString().split(',').join('-'), payNameList.toString().split(',').join('-'), payDurum.toString().split(',').join('-'));
    }
    else {
        toastr.warning('Not girişi için bir çalışma tipi seçiniz');
    }

});

$.ajaxSetup({ cache: false });
function veriYolla(veri, veri2, veri3) {
    var data = { secilenPayID: veri, secilenPayAD: veri2 , secilenDurum: veri3 };
    var kaydet = $.post("/Ders/Detay/SecilenleriKaydet", data);
    setTimeout(yonlendir, 1000)
}

function yonlendir() {
    window.location.replace('/Ders/NotGiris/Grup/' + dersGrupHocaID);
}


$(document).on("click", ".btnPayNotGir", function() {
    var payIDList = [];
    var payNameList = [];
    var payDurum = [];

    payIDList.push($(this).data("payid"));
    payNameList.push($(this).data("payadi"));
    payDurum.push($(this).data("paydurum"));

    veriYolla2(payIDList.toString().split(',').join('-'), payNameList.toString().split(',').join('-'), payDurum.toString().split(',').join('-'), $(this).attr("data-calismatip"));
});

function veriYolla2(veri, veri2, veri3, veri4, veri5) {
    var data = { secilenPayID: veri, secilenPayAD: veri2, secilenDurum: veri3, EnumCalismaTipi: veri4 };
    var kaydet = $.post("/Ders/Detay/SecilenleriKaydet", data);
    setTimeout(yonlendir2, 1000)
}

function yonlendir2() {
    window.location.replace('/Ders/Sinav/NotGiris/' + dersGrupHocaID);
}