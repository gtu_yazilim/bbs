﻿$(document).ready(function () {
    $("#divUyari").pulsate({ glow: true, reach: 50, color: '#ff0000' });
});

$(document).on("click", ".btnYayinla", function () {
    var $cBtn = $(this);
    $cBtn.attr("enabled", true);
    var data = {
        payID: $(this).attr("data-payid"),
        dersGrupID: $(this).attr("data-dersgrupid")
    };

    $.post("/Ders/Detay/DersYayinlaJson", data)
        .success(function (json) {
            if (json == true) {
                $cBtn.removeClass("btnYayinla btn-info").addClass("btnYayindanKaldir btn-inverse").html('<i class="fa fa-envelope"></i> Yayından Kaldır');
                toastr.success("Yayınlandı");
            }
            else {
                toastr.error("Hata oluştu");
            }
        });
});

$(document).on("click", ".btnYayindanKaldir", function () {
    $cBtn = $(this);
    $cBtn.attr("enabled", true);

    var data = {
        PayID: $(this).attr("data-payid"),
        DersGrupID: $(this).attr("data-dersgrupid")
    };

    $.post("/Ders/Detay/DersYayindanKaldirJson", data)
        .success(function (json) {
            if (json == true) {
                $cBtn.removeClass("btnYayindanKaldir btn-inverse").addClass("btnYayinla btn-info").html('<i class="fa fa-envelope"></i>  Yayınla');
                toastr.info("Yayından kaldırıldı.");
            }
            else {
                toastr.error("Hata oluştu");
            }
        });
});