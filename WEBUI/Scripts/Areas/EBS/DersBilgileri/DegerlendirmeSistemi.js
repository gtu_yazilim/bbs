﻿//jQuery dependency

$(document).ready(function() {
    $(document).trigger("keyup");
});

var sumTxt = function (selector) {
    var sum = 0;
    $(selector).each(function () {
        sum += Number($(this).val());
    });
    return sum;
}

$(document).on("keyup", function() {
    var toplam = sumTxt(".yilIci");
    if(toplam > 100)
    {
        $("#yilIciToplam").parent().css("background-color", "red").css("color", "white");
        toastr.warning("Yıl içi toplamı 100\'den fazla olamaz.");
    }
    else
    {
        $("#yilIciToplam").parent().removeAttr("style");
    }
    $("#yilIciToplam").text(toplam);
});

$(document).on("keyup", function() {
    var toplam = sumTxt(".yilSonu");
    if(toplam > 100)
    {
        $("#yilSonuToplam").parent().css("background-color", "red").css("color", "white");
        toastr.warning("Yıl sonu toplamı 100\'den fazla olamaz.");
    }
    else
    {
        $("#yilSonuToplam").parent().removeAttr("style");
    }
    $("#yilSonuToplam").text(toplam);
});

//$(document).on("keyup", ".yilSonu", function () {

    //if ($(this).val() < 20 || $(this).val() > 80) {
    //    toastr.error("Final notu ve yıl içi başarı oranı 20 ile 80 arasında bir tamsayı olmalıdır!");
    //}
    //else {
    //    $('input[data-tip=11]').val(Number(100 - $(this).val()));
    //}

    //var seviye = $(this).attr("data-enumseviye");
    //if (seviye == 1 || seviye == 2) {
    //    if ($(this).val() < 40 || $(this).val() > 60) {
    //        toastr.error("Final notu 40 ile 60 arasında bir tamsayı olmalıdır!");
    //    }
    //    else {
    //        $('input[data-tip=11]').val(Number(100-$(this).val()));
    //    }
    //}
    //if (seviye == 3 || seviye == 4) {
    //    if ($(this).val() < 20 || $(this).val() > 80) {
    //        toastr.error("Final notu 20 ile 80 arasında bir tamsayı olmalıdır!");
    //    }
    //    else {
    //        $('input[data-tip=11]').val(Number(100-$(this).val()));
    //    }
    //}
//});

$("#btnYeniPayEkle").click(function () {
    var calismaTipi = $("#ddlCalismaTipi").val();
    var payListe = [];

    if (FKDersPlanID == 0) {
        FKDersPlanID = null;
    }

    var data = {
        PayID: null,
        EnumCalismaTipi: $("#ddlCalismaTipi").val(),
        SiraNo: null,
        KatkiOrani: 0,
        Donem: EnumDonem,
        Yil: Yil,
        DersPlanID: FKDersPlanID,
        DersGrupID: FKDersGrupID,
        DersPlanAnaID: FKDersPlanAnaID,
        PersonelID: FKPersonelID,
        KullaniciAdi: kullaniciAdi,
        IP: IPAdres
    };

    payListe.push(data);

    $.post("/Ders/PayDuzenle/EklePay", { liste: payListe, id: FKDersPlanAnaID }).done(function (data) { window.location.reload(); });
});

$(document).on("keyup", ".txtListe", function () {

    var payID = $(this).attr("data-payid");
    var oran = $(this).val();
    var listGuncelle = [];

    if (oran == "" || oran == " " || oran == "  " || isNaN(oran) || oran == null || oran == undefined) {
        oran = 0;
    }

    var data = {
        ID: $(this).attr("data-payid"),
        Sira: oran,
        Oran: $(this).val()
    };
    listGuncelle.push(data);

    $.post("/Ders/PayDuzenle/GuncellePay", { payListesi: listGuncelle }).success(function(result) {
        result ? toastr.success("Oranlar güncellendi.") : toastr.error("Hata oluştu");
    });
});

$(".btnPaySil").on("click", function() {
    if (confirm("Pay silinecek! Onaylıyor musunuz?"))
    {
        $.post("/Ders/PayDuzenle/SilPay", { payID: $(this).attr("data-id") }).success(function(data) {
            window.location.reload();
        });
    }
});

$("#ddlGruplar").on("change", function () {
    if ($(this).val() == 0) {
        window.location.href = "/EBS/DersBilgileri/DegerlendirmeSistemi/" + FKDersPlanAnaID + "?dil=tr&birim=0";
    }
    else {
        window.location.href = "/EBS/DersBilgileri/DegerlendirmeSistemi/" + FKDersPlanAnaID + "?dil=tr&birim=0&DP=" + $(this).val();
    }
});