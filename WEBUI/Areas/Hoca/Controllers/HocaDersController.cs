﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Sabis.Bolum.Bll.SOURCE.EBS.BIRIM;
using Sabis.Bolum.Core.EBS.BIRIM;
using Sabis.Bolum.Bll.SOURCE.ABS.OGRENCI;

namespace Sabis.Bolum.WebUI.Areas.Hoca.Controllers
{ 
    public class HocaDersController : Controller
    {
        // GET: EBS/BolumBilgileri
        //[AllowAnonymous]
        public ActionResult Index()
        {
            List<Sabis.Bolum.Core.Hoca.DtoPersonelBilgisi> hocalar = new List<Core.Hoca.DtoPersonelBilgisi>();            
            hocalar = Sabis.Bolum.Bll.SOURCE.HOCA.HocaBilgileri.ListeleBolumHocalari(Sabitler.KullaniciID);
            ViewBag.YilDonem = Sabitler.Yil + "/" + Sabis.Enum.Utils.GetEnumDescription((Sabis.Enum.EnumDonem)Sabitler.Donem);
            ViewBag.Home = true;
            return View(hocalar);
        }

        //[AllowAnonymous]
        public ActionResult HocaDersleriniListele(int personelID)
        {
            Core.Hoca.DtoHocaDersBilgileri hocaDersBilgileri = new Core.Hoca.DtoHocaDersBilgileri();
            hocaDersBilgileri = Sabis.Bolum.Bll.SOURCE.HOCA.HocaBilgileri.HocaDersList(personelID, Sabitler.Yil, Sabitler.Donem);
            ViewBag.YilDonem = Sabitler.Yil + "/" + Sabis.Enum.Utils.GetEnumDescription((Sabis.Enum.EnumDonem)Sabitler.Donem);
            return View(hocaDersBilgileri);
        }

        //[AllowAnonymous]
        public ActionResult DersOgrenciListesi(int dersGrupID, int personelID)
        {
            Core.Hoca.DtoHocaDersOgrenciBilgileri dersiAlanOgrenciListesi = new Core.Hoca.DtoHocaDersOgrenciBilgileri();
            dersiAlanOgrenciListesi = Sabis.Bolum.Bll.SOURCE.HOCA.HocaBilgileri.DersiAlanOgrenciListesi(dersGrupID, personelID);
            ViewBag.YilDonem = Sabitler.Yil + "/" + Sabis.Enum.Utils.GetEnumDescription((Sabis.Enum.EnumDonem)Sabitler.Donem);
            return View(dersiAlanOgrenciListesi);
        }


        //[AllowAnonymous]
        public ActionResult OgrenciProfil(int ogrenciID, int dersGrupHocaId)
        {
            Core.Hoca.DtoHocaDersOgrenciBilgileri dersiAlanOgrenciListesi = new Core.Hoca.DtoHocaDersOgrenciBilgileri();
            dersiAlanOgrenciListesi = Sabis.Bolum.Bll.SOURCE.HOCA.HocaBilgileri.DersiAlanOgrenciListesiByGrupHocaID(dersGrupHocaId);
            ViewBag.PersonelID = dersiAlanOgrenciListesi.PersonelID;
            ViewBag.PersonelAd = dersiAlanOgrenciListesi.Unvan + " " + dersiAlanOgrenciListesi.AdSoyad;
            ViewBag.DersAd = dersiAlanOgrenciListesi.DersKod + " " + dersiAlanOgrenciListesi.DersAd + "(" + dersiAlanOgrenciListesi.GrupAd + ")";
            ViewBag.DersGrupID = dersiAlanOgrenciListesi.DersGrupID;
       
            return View(OGRENCIMAIN.GetirOgrenciOzetBilgi(ogrenciID));
        }
    }
}