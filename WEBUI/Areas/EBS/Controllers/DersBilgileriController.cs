﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Sabis.Bolum.Bll.SOURCE.EBS.DERS;
using Sabis.Bolum.Bll.SOURCE.EBS.GENEL;
using Sabis.Bolum.Bll.SOURCE.EBS.BIRIM;
using Sabis.Bolum.Core.EBS.DERS;

namespace Sabis.Bolum.WebUI.Areas.EBS.Controllers
{
    //[AllowAnonymous]
    public class DersBilgileriController : Controller
    {
        // GET: EBS/DersBilgileri
        public ActionResult Index()
        {
            return View(DERSMAIN.GetirKoordinatorDersleri(Sabitler.KullaniciID, Sabitler.EBSYil, Sabitler.EBSDonem, Sabitler.KullaniciAdi));
        }
        public ActionResult _pMenu()
        {
            return PartialView();
        }

        #region DERS AKIŞI CRUD
        public ActionResult DersAkisi(int id, string dil)
        {
            int dilID = Convert.ToInt32(dil.Replace("tr", "1").Replace("en", "2"));
            return View(DERSMAIN.GetirDersAkisi(id, Sabitler.EBSYil, dilID));
        }

        public ActionResult _pEkleDersAkisiYeniKonu()
        {
            return PartialView();
        }

        public JsonResult EkleDersAkisiYeniKonu(DersAkisiVM yeniKonu, string kullaniciAdi)
        {
            return Json(DERSMAIN.EkleDersAkisiYeniKonu(yeniKonu, kullaniciAdi));
        }

        public JsonResult GuncelleDersAkisiKonu(DersAkisiDilVM konuBilgi)
        {
            return Json(DERSMAIN.GuncelleDersAkisiKonu(konuBilgi, Sabitler.KullaniciAdi));
        }

        public JsonResult SilDersAkisiKonu(int dersKonuID)
        {
            return Json(DERSMAIN.SilDersAkisiKonu(dersKonuID));
        }
        #endregion

        #region ÖĞRENME ÇIKTILARI CRUD
        public ActionResult DersOgrenmeCiktilari(int id)
        {
            return View();
        }
        //TabloYok
        //public ActionResult _pGetirDersOgrenmeCiktilari(int id, int B, string dil)
        //{
        //    int dilID = Convert.ToInt32(dil.Replace("tr", "1").Replace("en", "2"));
        //    return PartialView(DERSMAIN.GetirOgrenmeCiktilari(id, B, Sabitler.EBSYil, dilID));
        //}
        public JsonResult GuncelleOgrenmeCiktisi(OgrenmeCiktilariVM ogrenmeCiktisi)
        {
            return Json(DERSMAIN.OgrenmeCiktisiGuncelle(ogrenmeCiktisi));
        }

        public JsonResult EkleYeniDersOgrenmeCiktisi(OgrenmeCiktilariVM yeniOgrenmeCiktisi)
        {
            return Json(DERSMAIN.EkleYeniOgrenmeCiktisi(yeniOgrenmeCiktisi));
        }
        //TabloYok
        //public JsonResult SilDersOgrenmeCiktisi(int dersCiktiID)
        //{
        //    return Json(DERSMAIN.SilDersOgrenmeCiktisi(dersCiktiID));
        //}
        #endregion

        #region DEĞERLENDİRME SİSTEMİ CRUD
        public ActionResult DegerlendirmeSistemi(int id, int? DP)
        {
            return View(DERSMAIN.GetirDegerlendirmePayListesi(id, Sabitler.EBSYil, Sabitler.EBSDonem, DP));
        }

        public ActionResult HiyerarsikDegerlendirme(int id, int? DP)
        {
            return View(Sabis.Bolum.Bll.SOURCE.ABS.DERS.PAYMAIN.GetirHiyerarsikDersPaylistesi(id, Sabitler.EBSYil, Sabitler.EBSDonem));
        }

        public ActionResult _pGetirPayListesi(int fkDersPlanAnaID, int yil, int donem)
        {
            return PartialView(DERSMAIN.GetirDegerlendirmePayListesi(fkDersPlanAnaID, Sabitler.EBSYil, Sabitler.EBSDonem, null));
        }


        #endregion

        #region DEĞERLENDİRME GRUPLARI
        public ActionResult DegerlendirmeGruplari(int id)
        {
            Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME.OrtakGrupYonetim.Olustur(id, Sabitler.Yil, Sabitler.Donem, Sabitler.KullaniciAdi);
            return View();
        }

        public ActionResult _pDegerlendirmeGrupListesi(int id)
        {
            return PartialView(DERSMAIN.GetirDegerlendirmeGrupListesi(id, Sabitler.Yil, Sabitler.Donem));
        }

        public JsonResult EkleYeniDegerlendirmeGrup(int FKDersPlanAnaID)
        {
            return Json(DERSMAIN.EkleYeniDegerlendirmeGrup(FKDersPlanAnaID, Sabitler.KullaniciID, Sabitler.KullaniciAdi, Sabitler.Yil, Sabitler.Donem));
        }

        public JsonResult SilDegerlendirmeGrup(int DegerlendirmeGrupID)
        {
            return Json(DERSMAIN.SilDegerlendirmeGrup(DegerlendirmeGrupID, Sabitler.EBSYil, Sabitler.EBSDonem));
        }

        public JsonResult DegistirDegistirDersDegerlendirmeGrup(int OrtakGrupID, int FKOrtakDegerlendirmeID)
        {
            return Json(DERSMAIN.DegistirDersDegerlendirmeGrup(OrtakGrupID, FKOrtakDegerlendirmeID));
        }

        public JsonResult DegistirDegerlendirmeGrupHoca(int OrtakDegerlendirmeID, int FKPersonelID)
        {
            return Json(DERSMAIN.DegistirDegerlendirmeGrupHoca(OrtakDegerlendirmeID, FKPersonelID));
        }

        public JsonResult HocalariGruplaraAyir(int FKDersPlanAnaID)
        {
            return Json(DERSMAIN.HocalariGruplaraAyir(FKDersPlanAnaID, Sabitler.Yil, Sabitler.Donem, Sabitler.KullaniciAdi));
        }

        public JsonResult GruplariBirlestir(int FKDersPlanAnaID)
        {
            return Json(DERSMAIN.GruplariBirlestir(FKDersPlanAnaID, Sabitler.Yil, Sabitler.Donem, Sabitler.KullaniciAdi, Sabitler.KullaniciID));
        }

        #endregion

        #region AKTS İŞ YÜKÜ
        public ActionResult AKTSIsYuku(int id, string dil)
        {
            return View(DERSMAIN.GetirDersAkts(1, id, Sabitler.EBSYil)); // Convert.ToByte(dil.Replace("tr", "1").Replace("en", "2"))
        }

        public ActionResult _pEkleYeniAKTSIsYuku()
        {
            return PartialView();
        }

        public JsonResult SilAKTSIsYuku(int dersAktsTnmID)
        {
            return Json(DERSMAIN.SilAKTSIsYuku(dersAktsTnmID));
        }

        public JsonResult EkleAKTSIsYuku(DersAktsVM yeniDersAkts, string kullaniciAdi)
        {
            return Json(DERSMAIN.EkleYeniDersAkts(yeniDersAkts, kullaniciAdi));
        }

        public JsonResult GuncelleAKTSIsYuku(List<DersAktsVM> dersAktsList, string kullaniciAdi)
        {
            return Json(DERSMAIN.GuncelleDersAkts(dersAktsList, kullaniciAdi));
        }
        #endregion

        #region DERS BİLGİLERİ
        public ActionResult _pDersBilgi(int id, string dil, int? birim) // id = FKDersPlanAnaID
        {
            if (!(GENELMAIN.DersKoordinatoruMu(id, Sabitler.KullaniciID)) && (birim.HasValue && birim != 0 && !FAKULTEMAIN.FakulteKoordinatoruMu(Sabitler.KullaniciID, birim.Value)))
            {
                HttpContext.Response.Redirect("/");
            }
            int enumDilID = Convert.ToInt32(dil.Replace("tr", "1").Replace("en", "2"));
            return View(DERSMAIN.GetirDersBilgi(id, Sabitler.EBSYil, enumDilID, birim));
        }

        public ActionResult DersOzet(int id, int? birim, string dil = "tr") // FKDersPlanAnaID
        {
            int enumDilID = Convert.ToInt32(dil.Replace("tr", "1").Replace("en", "2"));
            return View(DERSMAIN.GetirDersBilgi(id, Sabitler.EBSYil, enumDilID, birim));
        }
        [ValidateInput(false)]
        public JsonResult GuncelleDersBilgi(int FKDersPlanAnaID, DersBilgiVM dersBilgi)
        {
            return Json(DERSMAIN.GuncelleDersBilgi(FKDersPlanAnaID, dersBilgi, Sabitler.EBSYil, Sabitler.KullaniciAdi));
        }

        public ActionResult DersProgramCiktilari(int id, string dil)
        {
            //int enumDilID = Convert.ToInt32(dil.Replace("tr", "1").Replace("en", "2"));
            //return PartialView(Bll.EBS.DersBilgileri.GetirDersProgramCiktiKatkiDuzeyi(id, birim, Oturum.Yil, enumDilID));
            return PartialView();
        }

        public ActionResult _pGetirDersProgramCiktilari(int id, int birim, int enumDil)
        {
            return PartialView(DERSMAIN.GetirDersProgramCiktiKatkiDuzeyi(id, birim, Sabitler.EBSYil, enumDil));
        }

        //public JsonResult GetirDersProgramCiktilari(int dersPlanAnaID, string dil, int birim)
        //{
        //    int dilID = Convert.ToInt32(dil.Replace("tr", "1").Replace("en", "2"));
        //    return Json(DERSMAIN.GetirDersProgramCiktiKatkiDuzeyi(dersPlanAnaID, birim, Sabitler.EBSYil, dilID));
        //}

        public JsonResult GuncelleDersProgramCiktiKatkiDuzeyi(List<DersProgramCiktilariVM> katkiDuzeyList, int fkDersPlanAnaID)
        {
            return Json(DERSMAIN.GuncelleDersProgramCiktiKatkiDuzeyi(katkiDuzeyList, fkDersPlanAnaID, Sabitler.EBSYil));
        }

        public ActionResult DersKategorisi(int id, string dil)
        {
            int dilID = Convert.ToInt32(dil.Replace("tr", "1").Replace("en", "2"));
            return PartialView(DERSMAIN.GetirDersKategoriListe(id, Sabitler.EBSYil, dilID));
        }

        public JsonResult GuncelleDersKategori(int dersPlanAnaID, int dersKategoriID)
        {
            return Json(DERSMAIN.GuncelleDersKategori(dersPlanAnaID, dersKategoriID, Sabitler.EBSYil, Sabitler.KullaniciAdi));
        }
        #endregion

        public ActionResult Sinav(int id)
        {
            TempData["sinavMenuGizle"] = "true";
            return View(Sabis.Bolum.Bll.SOURCE.ABS.DERS.PAYMAIN.GetirPayBilgileriByDPAnaID(id, Sabitler.Yil, Sabitler.Donem));
        }

        public JsonResult GetirDersBirimListesi(int id, bool fakulteGoster = false)
        {
            return Json(DERSMAIN.GetirDersBirimListesi(id, Sabitler.EBSYil, fakulteGoster));
        }

        public JsonResult GetirBolumProgramCiktilari(int id, int enumDil = 1)
        {
            return Json(BOLUMMAIN.GetirBolumProgramCiktilari(id, Sabitler.EBSYil, enumDil));
        }
        //TabloYok
        //public JsonResult KaydetDersCiktiYeterlilikIliski(int fkDersPlanAnaID, int fkCiktiID, int fkBirimID, List<int> pCiktiList)
        //{
        //    return Json(DERSMAIN.KaydetDersCiktiYeterlilikIliski(fkDersPlanAnaID, fkCiktiID, fkBirimID, pCiktiList, Sabitler.EBSYil, Sabitler.KullaniciAdi));
        //}

        public JsonResult KaydetDersCiktiYontemIliski(int fkCiktiID, List<int> fkCiktiYontemIDList, int yontemTur)
        {
            return Json(DERSMAIN.KaydetDersCiktiYontemIliski(fkCiktiID, fkCiktiYontemIDList, yontemTur, Sabitler.EBSYil, Sabitler.KullaniciAdi));
        }
    }
}