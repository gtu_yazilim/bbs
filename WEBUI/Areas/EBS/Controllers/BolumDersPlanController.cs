﻿using Sabis.Bolum.Bll.SOURCE.ABS.DERS;
using Sabis.Bolum.Bll.SOURCE.EBS.DERS;
using Sabis.Bolum.Core.EBS.BIRIM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sabis.Bolum.WebUI.Areas.EBS.Controllers
{
    [AllowAnonymous]
    public class BolumDersPlanController : Controller
    {
        // GET: EBS/BolumDersPlan
        public ActionResult Index()
        {
            //SSOClient.SabisYetkiProvider prv = new SSOClient.SabisYetkiProvider();
            //var birimler = prv.GetirYetkiliBirimList(Sabitler.KullaniciAdi, "Index", "Sabis.Bolum.WebUI.Areas.EBS.Controllers.BolumDersPlanController", true);


            var birimler = CAPDERSMAIN.AnaBirimler().Select(x => new SelectListItem() { Text = x.FKFakulteAd + " / " + x.AnaBirimAd, Value = x.FKAnaBirimID.ToString() });
            ViewBag.anabirimler = birimler;
            ViewBag.altbirimler = birimler;

            //ViewBag.anabirimler = CAPDERSMAIN.AnaBirimler().Select(x => new SelectListItem() { Text = x.FKFakulteAd +" / "+ x.AnaBirimAd, Value = x.FKAnaBirimID.ToString() });
            //ViewBag.altbirimler = CAPDERSMAIN.AltBirimler().Select(x => new SelectListItem() { Text = x.FKFakulteAd + " / " + x.AnaBirimAd, Value = x.FKAnaBirimID.ToString() });

            ViewBag.captanimliste = CAPDERSMAIN.CapYandalTanimListe().OrderBy(x => x.FKFakulteAd).ThenBy(y => y.FKCapFakulteAd);
            return View();
        }
        public ActionResult _pCapTanimEkle()
        {
            ViewBag.birimler = CAPDERSMAIN.TumBolumler().Select(x => new SelectListItem() { Text = x.FKFakulteAd +" / "+ x.BolumAd + " (" + x.BolumID +")", Value = x.BolumID.ToString() });
            return View();
        }
        public ActionResult _pCapTanimDuzenle(int CapTanimID)
        {
            return View(CAPDERSMAIN.CapTanimDuzenle(CapTanimID));
        }
        #region CRUD
        [HttpPost]
        public JsonResult CapTanimKaydet(int EnumGecisTipi, int FKAnaBirimID, int FKAltBirimID, int kontenjan)
        {
            try
            {
                if (CAPDERSMAIN.CapDersKaydet(EnumGecisTipi, FKAnaBirimID, FKAltBirimID, false, Sabitler.KullaniciAdi, DateTime.Now, kontenjan))
                {
                    return Json(true);
                }
                else
                {
                    return Json(false);
                }           
            }
            catch (Exception ex)
            {
                return Json(false);
            }

        }

        [HttpPost]
        public JsonResult CapTanimDuzenle(int? FKCapTanimID, int FKAnaBirimID, int FKAltBirimID, int Kontenjan)
        {
            try
            {
                if (CAPDERSMAIN.CapDersDuzenle(FKCapTanimID.Value,FKAnaBirimID,FKAltBirimID,Kontenjan))
                {
                    return Json(true);
                }
                else
                {
                    return Json(false);
                }
            }
            catch (Exception ex)
            {
                return Json(false);
            }

        }
        public JsonResult CapTanimSil(int FKCapTanimID)
        {
            try
            {
                return Json(CAPDERSMAIN.CapTanimSil(FKCapTanimID, Sabitler.KullaniciAdi));
            }
            catch (Exception ex)
            {

                return Json(false);
            }
        }

        public JsonResult CapPlanEkle(int FKAnaBirimID, int FKAltBirimID, int FKAnaDersPlanID, int FKCapDersPlanID)
        {
            try
            {
                if (FKAnaBirimID == null || FKAltBirimID == null || FKAnaDersPlanID == null ||FKCapDersPlanID == null)
                {
                    return Json(false);
                }
                else
                {
                    return Json(CAPDERSMAIN.CapPlanEkle(FKAnaBirimID, FKAltBirimID, FKAnaDersPlanID, FKCapDersPlanID, Sabitler.KullaniciAdi, 2017));
                }              
            }
            catch (Exception ex)
            {

                return Json(false);
            }
        }

        public JsonResult CapPlanSil(int CapDersPlanID)
        {
            try
            {
                return Json(CAPDERSMAIN.CapPlanSil(CapDersPlanID,Sabitler.KullaniciAdi));
            }
            catch (Exception ex)
            {

                return Json(false);
            }
        }
        #endregion
        [HttpPost]
        public JsonResult BolumPlanGetir(int yil, int bolumID)
        {
            try
            {
                return Json(CAPDERSMAIN.BolumPlanGetir(yil, bolumID));
            }
            catch (Exception ex)
            {

                return Json(false);
            }

        }
        public JsonResult CapDersPlanlariGetir(int FKAnaBirimID, int FKAltBirimID)
        {
            try
            {
                SSOClient.SabisYetkiProvider prv = new SSOClient.SabisYetkiProvider();
                var birimler = prv.GetirYetkiliBirimList(Sabitler.KullaniciAdi, "CapDersPlanlariGetir", "Sabis.Bolum.WebUI.Areas.EBS.Controllers.BolumDersPlanController", false);
                return Json(CAPDERSMAIN.CapDersPlanGetir(FKAnaBirimID, FKAltBirimID));
                
            }
            catch (Exception ex)
            {

                return Json(false);
            }
        }

        #region YENISEKME
        public ActionResult Cap()
        {
            ViewBag.DuzenlemeIzin = CAPDERSMAIN.DuzenlemeIzinKontrol();     
            return View();
        }
        public JsonResult FakulteGetir()
        {         
            if (Sabitler.KullaniciAdi == "umit" || Sabitler.KullaniciAdi == "ozmen")
            {
                return Json(CAPDERSMAIN.FakulteGetir(null,false));
            }
            else
            {
                SSOClient.SabisYetkiProvider prv = new SSOClient.SabisYetkiProvider();
                var birimler = prv.GetirYetkiliBirimList(Sabitler.KullaniciAdi, "FakulteGetir", "Sabis.Bolum.WebUI.Areas.EBS.Controllers.BolumDersPlanController", true).Where(x => x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.Fakulte).Select(x => x.BirimID).ToList();
                if (birimler.Count > 0)
                {
                    return Json(CAPDERSMAIN.FakulteGetir(birimler, true));
                }
                else
                {
                    return Json(null);
                }
                
            }
            
        }
        public JsonResult BolumGetir(int FKFakulteID)
        {
            if (Sabitler.KullaniciAdi == "umit" || Sabitler.KullaniciAdi == "ozmen")
            {
                return Json(CAPDERSMAIN.BolumGetir(FKFakulteID,null,false));
            }
            else
            {
                SSOClient.SabisYetkiProvider prv = new SSOClient.SabisYetkiProvider();
                var birimler = prv.GetirYetkiliBirimList(Sabitler.KullaniciAdi, "BolumGetir", "Sabis.Bolum.WebUI.Areas.EBS.Controllers.BolumDersPlanController", true).Where(x => x.UstBirimID == FKFakulteID && x.EnumBirimTuru == (int)Sabis.Enum.EnumBirimTuru.Bolum).Select(x => x.BirimID).ToList();
                return Json(CAPDERSMAIN.BolumGetir(FKFakulteID,birimler,true));
            }
        }
        public JsonResult CapProgramListe(int FKAnaBirimID, int Yil)
        {
            return Json(CAPDERSMAIN.CapProgramListe(FKAnaBirimID,Yil));
        }
        public JsonResult CapDerslerGetir(int FKCapTanimID,int Yil)
        {
            return Json(CAPDERSMAIN.CapDerslerGetir(FKCapTanimID,Yil));
        }
        public JsonResult CapSecmeliDerslerGetir(int FKCapTanimID, int Yil)
        {
            //aslında dersetiketleri getiriyor.
            return Json(CAPDERSMAIN.CapSecmeliDerslerGetir(FKCapTanimID, Yil));
        }
        public JsonResult EBSDersPlanGetir(int FKCapTanimID, int Yil)
        {
            return Json(CAPDERSMAIN.EBSDersPlanGetir(FKCapTanimID,Yil));
        }
        public JsonResult IntibakDerslerGetir(int FKCapTanimID, int Yil)
        {
            return Json(CAPDERSMAIN.IntibakDerslerGetir(FKCapTanimID,Yil));
        }
        public JsonResult IntibakSecmeliDerslerGetir(int FKCapTanimID, int Yil)
        {
            return Json(CAPDERSMAIN.IntibakSecmeliDerslerGetir(FKCapTanimID, Yil));
        }      
        public JsonResult CapProgramBilgi(int FKCapTanimID)
        {
            return Json(CAPDERSMAIN.CapProgramBilgi(FKCapTanimID));
        }
        public ActionResult _IntibakOlustur(int FKCapTanimID,int FKAnaDersPlanID, int Yil)
        {
            var model = CAPDERSMAIN._IntibakOlustur(FKCapTanimID, FKAnaDersPlanID,Yil);
            ViewBag.planlar = model.IntibakYapilacakListe.Select(x => new SelectListItem() { Text = x.DersAd + " (" + x.Yariyil +". Yarıyıl - "+ x.AKTS +" AKTS)", Value = x.FKDersPlanID.ToString()});
            return View(model);
        }
        public ActionResult _IntibakOlusturSecmeli(int FKCapTanimID, int FKAnaEtiketID, int Yil, int FKSaatBilgiID)
        {
            var model = CAPDERSMAIN._IntibakOlusturSecmeli(FKCapTanimID, FKAnaEtiketID, Yil);
            //ViewBag.secmeliplanlar = model.SecmeliListe.Select(x => new SelectListItem() { Text = x.Etiket +" ("+ x.YariYil +". Yarıyıl - "+ x.AKTS +" AKTS)", Value = x.FKEtiketID.ToString() });
            ViewBag.secmeliplanlar = model.SecmeliListe.Select(x => new SelectListItem() { Text = x.Etiket + " (" + x.YariYil + ". Yarıyıl - " + x.AKTS + " AKTS)", Value = x.FKSaatBilgiID.ToString() });
            ViewBag.FKSaatBilgiID = FKSaatBilgiID;
            return View(model);
        }
        public JsonResult _IntibakKaydet(int FKCapTanimID, int FKDersPlanID, int FKCapDersPlanID,int Yil, string Aciklama)
        {
            return Json(CAPDERSMAIN._IntibakKaydet(FKCapTanimID,FKDersPlanID, FKCapDersPlanID,Aciklama,Sabitler.KullaniciAdi,Yil));
        }
        public JsonResult _IntibakSecmeliKaydet(int FKCapTanimID,int FKAnaEtiketID,int FKCapEtiketID, int Adet, int FKSaatBilgiID, int Yil)
        {
            return Json(CAPDERSMAIN._IntibakSecmeliKaydet(FKCapTanimID,FKAnaEtiketID,FKCapEtiketID,Sabitler.KullaniciAdi, Adet, FKSaatBilgiID, Yil));
        }
        public JsonResult IntibakPlanSil(int ID)
        {
            return Json(CAPDERSMAIN.IntibakPlanSil(ID,Sabitler.KullaniciAdi));
        }
        public JsonResult IntibakSecmeliPlanSil(int ID)
        {
            return Json(CAPDERSMAIN.IntibakSecmeliPlanSil(ID, Sabitler.KullaniciAdi));
        }

        public JsonResult KontenjanGuncelle(int FKCapTanimID, int Kontenjan)
        {
            return Json(CAPDERSMAIN.KontenjanGuncelle(FKCapTanimID,Kontenjan));
        }
        public JsonResult DuzenlemeIzinKontrol()
        {
            return Json(CAPDERSMAIN.DuzenlemeIzinKontrol());
        }
        public JsonResult DuzenlemeIzin(bool Deger)
        {
            return Json(CAPDERSMAIN.DuzenlemeIzin(Deger));
        }
        #endregion

    }
}