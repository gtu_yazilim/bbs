﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Sabis.Bolum.Bll.SOURCE.EBS.BIRIM;
using Sabis.Bolum.Core.EBS.BIRIM;

namespace Sabis.Bolum.WebUI.Areas.EBS.Controllers
{ 
    public class BolumBilgileriController : Controller
    {
        // GET: EBS/BolumBilgileri
        public ActionResult Index()
        {
            return View();
        }

        #region BÖLÜM BİLGİLERİ CRUD
        public ActionResult _pBolumBilgileri(int? birimID, int yil, string dil)
        {
            int EnumDilID = Convert.ToInt32(dil.Replace("tr", "1").Replace("en", "2"));
            return PartialView(BOLUMMAIN.GetirBolumBilgi(birimID, yil, EnumDilID));
        }
        [ValidateInput(false)]
        public JsonResult GuncelleBolumBilgi(int FKDilBolumBilgiID, string Icerik)
        {
            Icerik = Icerik.Substring(3, Icerik.Length - 8);
            return Json(BOLUMMAIN.GuncelleBolumBilgi(FKDilBolumBilgiID, Icerik));
        }

        public ActionResult _pBolumBaskanVekili(int id) //bolum id
        {
            return PartialView();
        }

        public JsonResult GetirVekilAdSoyad(int id)
        {
            return Json(BOLUMMAIN.GetirBolumVekilBilgi(id));
        }

        public JsonResult GuncelleBolumKoordinator(int? id, int FKPersonelID, int? BolumID)
        {
            return Json(BOLUMMAIN.GuncelleBolumKoordinator(id, FKPersonelID, BolumID, Sabitler.KullaniciAdi));
        }

        public JsonResult BolumVekiliMi(int BolumID)
        {
            return Json(BOLUMMAIN.BolumVekiliMi(Sabitler.KullaniciID, BolumID));
        }
        #endregion

        #region BÖLÜM PROGRAM ÇIKTILARI CRUD
        public ActionResult _pGetirProgramCiktilari(int id, string dil) //id = birimID
        {
            int dilID = Convert.ToInt32(dil.Replace("tr", "1").Replace("en", "2"));
            return PartialView(BOLUMMAIN.GetirBolumProgramCiktilari(id, Sabitler.EBSYil, dilID));
        }

        public JsonResult ProgramCiktisiGuncelle(BolumProgramCiktilariVM programCiktisi)
        {
            return Json(BOLUMMAIN.GuncelleProgramCiktisi(programCiktisi, Sabitler.EBSYil, Sabitler.KullaniciAdi));
        }

        public JsonResult YeniProgramCiktisiEkle(BolumProgramCiktilariVM yeniProgramCiktisi)
        {
            return Json(BOLUMMAIN.EkleYeniProgramCiktisi(yeniProgramCiktisi));
        }

        public JsonResult ProgramCiktisiSil(int bolumYeterlilikID)
        {
            return Json(BOLUMMAIN.SilDersProgramCiktisi(bolumYeterlilikID));
        }
        #endregion
    }
}