﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sabis.Bolum.WebUI.Areas.EBS.Controllers
{
    [AllowAnonymous]

    public class DersProgramCiktilariController : Controller
    {
        // GET: EBS/DersProgramCiktilari
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetirBolumOrtakDersListesi(int FKBirimID)
        {
            return Json(Sabis.Bolum.Bll.SOURCE.EBS.DERS.DERSMAIN.GetirBirimUniversiteOrtakDersList(FKBirimID, Sabitler.EBSYil));
        }

        public JsonResult GetirBolumProgramCiktiListesi(int FKBirimID, int FKDersPlanAnaID)
        {
            var dersDetay = Sabis.Bolum.Bll.SOURCE.EBS.DERS.DERSMAIN.GetirDersBilgi(FKDersPlanAnaID, Sabitler.EBSYil, 1, FKBirimID);
            List<Sabis.Bolum.Core.EBS.BIRIM.BolumProgramCiktilariVM> bolumProgramCiktiList = new List<Sabis.Bolum.Core.EBS.BIRIM.BolumProgramCiktilariVM>();

            //if (dersDetay.EnumDersPlanAnaZorunlu.HasValue && dersDetay.EnumDersPlanAnaZorunlu != 6)
            //{
                bolumProgramCiktiList = Sabis.Bolum.Bll.SOURCE.EBS.BIRIM.BOLUMMAIN.GetirBolumProgramCiktilari(FKBirimID, Sabitler.EBSYil, 1);
            //}
            return Json(bolumProgramCiktiList);
        }

        public JsonResult GetirDersProgramCiktilari(int dersPlanAnaID, string dil, int birim)
        {
            int dilID = Convert.ToInt32(dil.Replace("tr", "1").Replace("en", "2"));
            return Json(Sabis.Bolum.Bll.SOURCE.EBS.DERS.DERSMAIN.GetirDersProgramCiktiKatkiDuzeyi(dersPlanAnaID, birim, Sabitler.EBSYil, dilID));
        }
    }
}