﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Sabis.Bolum.Core.EBS.DERS;

namespace Sabis.Bolum.WebUI.Areas.EBS.Controllers
{
    [AllowAnonymous]
    public class YandalController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ListeleYandal(int fkBirimID)
        {
            return Json(Bll.SOURCE.EBS.DERS.YANDALMAIN.GetirYandalListesi(fkBirimID, Sabitler.Yil, Sabitler.Donem));
        }

        public JsonResult ListeleYandalBirim(int fkFakulteID)
        {
            return Json(Sabis.Bolum.Bll.SOURCE.EBS.DERS.YANDALMAIN.GetirYandalBirimList(fkFakulteID));
        }

        public JsonResult EkleYeniYandal(YandalVM yandal)
        {
            return Json(Bll.SOURCE.EBS.DERS.YANDALMAIN.EkleYeniYandal(yandal));
        }

        public ActionResult _pDersListesi(int id)
        {
            return PartialView(Bll.SOURCE.EBS.DERS.YANDALMAIN.GetirBolumDersList(id, Sabitler.Yil));
        }

        public JsonResult EkleYandalDers(List<YandalDersVM> yandalDersList)
        {
            return Json(Bll.SOURCE.EBS.DERS.YANDALMAIN.EkleYeniYandalDers(yandalDersList));
        }

        public JsonResult GuncelleKontenjan(int fkTanimID, int adet)
        {
            return Json(Bll.SOURCE.EBS.DERS.YANDALMAIN.GuncelleKontenjan(fkTanimID, adet));
        }
    }
}