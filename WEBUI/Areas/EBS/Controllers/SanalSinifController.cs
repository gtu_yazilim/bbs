﻿using Sabis.Bolum.Bll.SOURCE.EBS.GENEL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sabis.Bolum.WebUI.Areas.EBS.Controllers
{
    [AllowAnonymous]
    public class SanalSinifController : Controller
    {
        // GET: EBS/SanalSinif
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Grup(int id)
        {
            return View(SANALSINIFMAIN.GetirDersGrupBilgi(id,Sabitler.Yil, Sabitler.Donem));
        }
        public ActionResult Rapor(int id)
        {
            return View(SANALSINIFMAIN.GetirRapor(id));
        }
    }
}