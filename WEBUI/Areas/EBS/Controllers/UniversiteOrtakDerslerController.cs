﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sabis.Bolum.WebUI.Areas.EBS.Controllers
{
    public class UniversiteOrtakDerslerController : Controller
    {
        // GET: EBS/UniversiteOrtakDersler
        public ActionResult Index()
        {
            return View(Sabis.Bolum.Bll.SOURCE.EBS.DERS.DERSMAIN.GetirUniversiteOrtakDersListesi(Sabitler.EBSYil));
        }
    }
}