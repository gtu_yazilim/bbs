﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sabis.Bolum.WebUI.Areas.EBS.Controllers
{
    [AllowAnonymous]
    public class HiyerarsikDegerlendirmeController : Controller
    {
        // GET: EBS/HiyerarsikDegerlendirme
        public ActionResult Liste(int id)
        {
            return View(Sabis.Bolum.Bll.SOURCE.ABS.DERS.PAYMAIN.GetirHiyerarsikPayList(id, Sabitler.EBSYil, Sabitler.EBSDonem));
        }
    }
}