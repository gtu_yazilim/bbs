﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Sabis.Bolum.Bll.SOURCE.EBS.DERS;
using Sabis.Bolum.Core.EBS.DERS;
using Sabis.Bolum.Bll.SOURCE.EBS.BIRIM;

namespace Sabis.Bolum.WebUI.Areas.EBS.Controllers
{
    public class YazOkuluController : Controller
    {
        // GET: EBS/YazOkulu
        public ActionResult Index()
        {
            return View();
        }
        
        public JsonResult ListeleYazOkuluDersleri(int FKBirimID)
        {
            //var dersList = YAZOKULUMAIN.ListeleYazOkuluDersleri(FKBirimID, Sabitler.EBSYil).Where(x=> x.EnumDersBirimTipi != (int)Sabis.Enum.EnumDersBirimTipi.UNIVERSITE).ToList();
            //var yetkiliFakulteList = FAKULTEMAIN.GetirFakulteYetkiListesi(Sabitler.KullaniciID);
            //var fakulteID = Sabis.Client.BirimIslem.Instance.getirBagliFakulteID(FKBirimID);
            //if (!yetkiliFakulteList.Any(x=> x == fakulteID))
            //{
            //    dersList = dersList.Where(x => x.EnumDersBirimTipi == (int)Sabis.Enum.EnumDersBirimTipi.BOLUM).ToList();
            //}
            return Json(YAZOKULUMAIN.ListeleYazOkuluDersleri(FKBirimID, Sabitler.EBSYil));
        }
        [AllowAnonymous]
        public JsonResult GetirBirimDersList(int FKBirimID, int? FKProgramID)
        {
            var dersList = BOLUMMAIN.GetirBolumDersListesi(FKBirimID, Sabitler.EBSYil, Sabitler.EBSDonem, Sabitler.KullaniciID, FKProgramID, false).Where(x => x.EnumDersBirimTipi != (int)Sabis.Enum.EnumDersBirimTipi.UNIVERSITE).ToList();
            return Json(dersList);
        }

        //[AllowAnonymous]
        //public JsonResult GetirFakulteDersList(int FKBirimID, int? FKProgramID)
        //{
        //    var dersList = FAKULTEMAIN.GetirFakulteDersListesi(FKBirimID, Sabitler.EBSYil, Sabitler.EBSDonem, Sabitler.KullaniciID, FKProgramID, true).ToList();
        //    return Json(dersList);
        //}
        
        public JsonResult KaydetYazOkuluDersleri(YazOkuluDersleriVM yazOkuluDersleri)
        {
            return Json(YAZOKULUMAIN.KaydetYazOkuluDersleri(yazOkuluDersleri, Sabitler.KullaniciAdi));
        }
        [AllowAnonymous]
        public ActionResult DersListesi()
        {
            return View();
        }
    }
}