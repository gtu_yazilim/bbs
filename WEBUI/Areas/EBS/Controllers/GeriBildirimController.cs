﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sabis.Bolum.WebUI.Areas.EBS.Controllers
{
    [AllowAnonymous]
    public class GeriBildirimController : Controller
    {
        // GET: EBS/GeriBildirim
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Detay(int FKDersPlanID, int m)
        {
            return View(Sabis.Bolum.Bll.SOURCE.EBS.BIRIM.BOLUMMAIN.GeriBildirimDetay(FKDersPlanID,m));
        }
        public ActionResult DersListesiGetir(int FKBirimID, int? FKProgramID)
        {
            return Json(Sabis.Bolum.Bll.SOURCE.EBS.BIRIM.BOLUMMAIN.DersListesiGetir(FKBirimID, Sabitler.Yil, Sabitler.Donem, Sabitler.KullaniciID, FKProgramID));
        }
        public ActionResult DersListesiGetir1(int FKBirimID, int? FKProgramID)
        {
            return Json(Sabis.Bolum.Bll.SOURCE.EBS.BIRIM.BOLUMMAIN.DersListesiGetir1(FKBirimID, Sabitler.Yil, Sabitler.Donem, Sabitler.KullaniciID, FKProgramID));
        }

        public ActionResult _SinavGeriBildirimleri(int FKDersPlanID)
        {
            return PartialView(Sabis.Bolum.Bll.SOURCE.EBS.BIRIM.BOLUMMAIN._SinavGeriBildirimleri(FKDersPlanID,Sabitler.EBSYil, Sabitler.EBSDonem));
        }
        public ActionResult _OdevGeriBildirimleri(int FKDersPlanID)
        {
            return PartialView(Sabis.Bolum.Bll.SOURCE.EBS.BIRIM.BOLUMMAIN._OdevGeriBildirimleri(FKDersPlanID, Sabitler.Yil, Sabitler.Donem));
        }
        public ActionResult _SanalSinifGeriBildirimleri(int FKDersPlanID)
        {
            return PartialView(Sabis.Bolum.Bll.SOURCE.EBS.BIRIM.BOLUMMAIN._SanalSinifGeriBildirimleri(FKDersPlanID,Sabitler.Yil,Sabitler.Donem));
        }
        public ActionResult _IcerikGeriBildirimleri()
        {
            return PartialView();
        }

        public ActionResult _SinavGeriBildirimRapor(int SinavID, int FKOgrenciID, int FKDersGrupID)
        {
            return View(Sabis.Bolum.Bll.SOURCE.EBS.BIRIM.BOLUMMAIN.SinavGeriBildirimGetir(SinavID,FKOgrenciID,FKDersGrupID));
        }
        public ActionResult _SanalSinifGeriBildirimRapor(int SanalSinifID, int FKOgrenciID, int FKDersGrupID)
        {
            return View(Sabis.Bolum.Bll.SOURCE.EBS.BIRIM.BOLUMMAIN.SanalSinifGeriBildirimGetir(SanalSinifID, FKOgrenciID, FKDersGrupID));
        }
        


    }
}