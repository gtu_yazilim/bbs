﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sabis.Bolum.WebUI.Areas.EBS.Controllers
{
    public class FakulteYonetimController : Controller
    {
        // GET: EBS/FakulteYonetim
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetirFakulteOrtakDersList(int fakulteID)
        {
            return Json(Sabis.Bolum.Bll.SOURCE.EBS.BIRIM.FAKULTEMAIN.GetirFakulteDersListesi(fakulteID, Sabitler.EBSYil));
        }

        public JsonResult GetirFakulteBolumDersList(int bolumID)
        {
            return Json(Sabis.Bolum.Bll.SOURCE.EBS.BIRIM.BOLUMMAIN.GetirBolumDersListesi(bolumID, Sabitler.EBSYil, Sabitler.EBSDonem, Sabitler.KullaniciID, null));
        }
    }
}