﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sabis.Bolum.WebUI.Areas.EBS.Controllers
{
    public class BolumYonetimController : Controller
    {
        // GET: EBS/BolumYonetim
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GuncelleYazOkuluDersi(int FKDersPlanAnaID, int? FakulteID, int? BolumID, bool durum, int? ProgramID, bool dersKontrol = true)
        {
            return Json(Sabis.Bolum.Bll.SOURCE.EBS.DERS.DERSMAIN.GuncelleYazOkuluDersi(FKDersPlanAnaID, Sabitler.EBSYil, FakulteID, BolumID, durum, Sabitler.KullaniciAdi, ProgramID, dersKontrol));
        }
    }
}