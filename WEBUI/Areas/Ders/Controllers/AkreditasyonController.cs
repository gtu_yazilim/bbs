﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Sabis.Bolum.Bll.SOURCE.ABS.AKREDITASYON;

namespace Sabis.Bolum.WebUI.Areas.Ders.Controllers
{
    [AllowAnonymous]
    public class AkreditasyonController : Controller
    {
        // GET: Ders/Akreditasyon
        public ActionResult SoruBasari(int id)
        {
            //var dersDetay = Sabitler.GetirDersDetay(id);
            //return View(AKREDITASYONMAIN.GetirSoruSonucListe(dersDetay.DersGrupID, dersDetay.SonHalVerildiMi, dersDetay.DersGrupHocaID, Sabitler.KullaniciAdi, Sabitler.IPAdresi));
            return View();
        }

        public ActionResult ProgramCiktilari(int id)
        {
            return View();
        }

        public ActionResult OgrenmeCiktilari(int id)
        {
            return View();
        }

        //public JsonResult GetirSoruBasariJson(int dersGrupID, int payID, bool sonHalVerildiMi)
        //{
        //    return Json(AKREDITASYONMAIN.GetirSoruSonuc(dersGrupID, payID, sonHalVerildiMi));
        //}
    }
}