﻿using Sabis.Bolum.Bll.SOURCE.ABS.DERS;
using Sabis.Bolum.Bll.SOURCE.ABS.SORUNBILDIRIM;
using Sabis.Bolum.Core.ABS.SORUNBILDIRIM.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Sabis.Bolum.WebUI.Areas.Ders.Controllers
{
    [AllowAnonymous]
    public class SorunBildirimController : Controller
    {
        // GET: Ders/SorunBildirim
        public ActionResult Index(int id)
        {
            return View();
        }
        public ActionResult _OnlineSinavSorunlari(int id)
        {
            var pays = PAYMAIN.GetirPayBilgileriv2(id, Sabitler.KullaniciAdi, Sabitler.IPAdresi).Select(x => new SelectListItem() { Text = x.SiraNo + ". " + Sabis.Enum.Utils.GetEnumDescription((Sabis.Enum.EnumCalismaTip)x.EnumCalismaTipi),Value= x.PayID.ToString() });
            var grups = SINAVMAIN.Gruplar(id, Sabitler.Yil, Sabitler.Donem).Select(x => new SelectListItem() { Text = x.text, Value = x.value.ToString()});

            ViewBag.GrupHocaID = id;
            ViewBag.XX = pays;
            ViewBag.YY = grups;
            return View();
        }
        public ActionResult SorunListele(int GrupHocaID, int PayID)
        {
            return Json(SORUNBILDIRIMMAIN.SorunListele(GrupHocaID, PayID));
        }
        public ActionResult _DosyaGoruntule(string key)
        {
            ViewBag.Url = SDFSClient.FileClient.GetUrl(key);
            return View();
        }
    }
}