﻿using Sabis.Bolum.Bll.SOURCE.ABS.DERS;
using Sabis.Bolum.Bll.SOURCE.ABS.RAPOR;
using Sabis.Bolum.Core.dto.Ders;
using Sabis.Bolum.Core.RAPOR;
using Sabis.Bolum.Core.RAPOR.Dto;
using Sabis.Core.DTO.GrupArama;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sabis.Bolum.WebUI.Areas.Ders.Controllers
{
    [AllowAnonymous]
    public class DevamListeController : Controller
    {
        //private IRaporDataRepo _raporRepo;
        //private IDosyaIslem _dosyaIslem;
        //private IBelgeIslem _belgeIslem;

        //public DevamListeController(IRaporDataRepo raporRepo, IDosyaIslem dosyaIslem, IBelgeIslem belgeIslem)
        //{
        //    _raporRepo = raporRepo;
        //    _dosyaIslem = dosyaIslem;
        //    _belgeIslem = belgeIslem;
        //}

        // GET: Ders/DevamListe
        public ActionResult Index()
        {
            DateTime Pazartesi = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            DateTime Pazar = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
            Pazartesi = Pazartesi.AddDays(1 - Pazartesi.DayOfWeek.GetHashCode());
            Pazar = Pazar.AddDays(7 - Pazar.DayOfWeek.GetHashCode());

            var dersListe = DERSPROGRAMI.GetirOgretimElemaniDersProgramiv2(Sabitler.KullaniciID, Sabitler.Yil, Sabitler.Donem);
            var eventList = dersListe.Where(x => x.Baslangic >= Pazartesi && x.Bitis <= Pazar).Select(x => new HaftalikDersProgramiDto
            {
                ID = x.ID,
                DersBilgisi = x.DersBilgisi.DersAd + (x.DersBilgisi.EnumOgretimTur == Sabis.Enum.EnumOgretimTur.BELIRSIZ ? " " : x.DersBilgisi.EnumOgretimTur == Sabis.Enum.EnumOgretimTur.OGRETIM_1 ? " 1" : x.DersBilgisi.EnumOgretimTur == Sabis.Enum.EnumOgretimTur.OGRETIM_2 ? " 2" : x.DersBilgisi.EnumOgretimTur == Sabis.Enum.EnumOgretimTur.UZAKTAN ? " 3" : "").ToString() + "-" + Sabis.Bolum.Bll.SOURCE.ABS.DERS.DERSMAIN.GetirDersDetay(x.DersBilgisi.DersGrupHocaID).GrupAd,
                Baslangic = x.Baslangic,
                Bitis = x.Bitis,
                Color = x.DersBilgisi.EnumOgretimTur == Sabis.Enum.EnumOgretimTur.BELIRSIZ ? "#2F353B" : x.DersBilgisi.EnumOgretimTur == Sabis.Enum.EnumOgretimTur.OGRETIM_1 ? "#3598DC" : x.DersBilgisi.EnumOgretimTur == Sabis.Enum.EnumOgretimTur.OGRETIM_2 ? "#F36A5A" : x.DersBilgisi.EnumOgretimTur == Sabis.Enum.EnumOgretimTur.UZAKTAN ? "#2C3E50" : "",
                FKDersGrupHocaID = x.DersBilgisi.DersGrupHocaID,
                Mekan = x.Mekan == null ? "" : x.Mekan.Ad
            }).OrderBy(y => y.Baslangic).ToList();


            return View(eventList);
        }
    }
}