﻿using System.Collections.Generic;
using System.Web.Mvc;

using Sabis.Bolum.Core.ABS.DERS.DERSDETAY;
using Sabis.Bolum.Core.ABS.NOT;
using Sabis.Bolum.Bll.SOURCE.ABS.NOT;
using Sabis.Bolum.WebUI.Areas.Ders.Models;
using AutoMapper;
using Apollo.Core.OgrenciDers;

namespace Sabis.Bolum.WebUI.Areas.Ders.Controllers
{
    public class BasariNotGirisiController : Controller
    {
        IMapper _mapper;

        public BasariNotGirisiController(IMapper mapper)
        {
            _mapper = mapper;
        }


        // GET: Ders/BasariNotGirisi
        public ActionResult Grup(int id)
        {
            DersDetayVM dersDetay = Sabitler.GetirDersDetay(id);
            TempData["EnumKokDersTipi"] = dersDetay.EnumKokDersTipi;
            ViewBag.DersGrupID = dersDetay.DersGrupID;

            var olvmList = BASARINOTMAIN.GetirBasariNotListesi(dersDetay.DersGrupID, Sabitler.KullaniciID, dersDetay.ButunlemeMiOrijinal);
            List<OgrenciBasariNotGirisVM> bngL = new List<OgrenciBasariNotGirisVM>();
            foreach (var item in olvmList)
            {
                OgrenciBasariNotGirisVM bngvm = _mapper.Map<OgrenciBasariNotGirisVM>(item);
                bngvm.BasariNotlari = OgrenciNot.GetirHocaninVerebilecegiHarfNotlari(item.OgrenciID, dersDetay);
                Sabis.Core.IslemSonuc<string> yIs = Apollo.Core.OgrenciYonetmelik.OgrenciYonetmelikRepo.GetirYonetmelikAdiOgrenciNoIle(item.OgrenciID);
                bngvm.YonetmelikAciklama = yIs.EnumIslemSonuc == EnumShared.EnumIslemSonuc.Basari ? yIs.Data : "Yönetmelik Bilgisine Ulaşilamıyor";
                //Sabis.Enum.Utils.ConvertEnumToListFilterByAttributes<EnumHarfBasari>(new Type[] { typeof(HocaVerebilir) });
                bngL.Add(bngvm);
            }

            return View(bngL);

            //return View(BASARINOTMAIN.GetirBasariNotListesi(dersDetay.DersGrupID, Sabitler.KullaniciID, dersDetay.ButunlemeMiOrijinal));
        }

        public JsonResult NotKaydet(int dersGrupID, List<OgrenciListesiVM> notListesi, int dersGrupHocaID)
        {
            DersDetayVM dersDetay = Sabitler.GetirDersDetay(dersGrupHocaID);
            BasariNotKayitVM yeniNotKayit = new BasariNotKayitVM();

            yeniNotKayit.ButunlemeMi = dersDetay.ButunlemeMiOrijinal;
            yeniNotKayit.DersGrupID = dersGrupID;
            yeniNotKayit.Yil = Sabitler.Yil;
            yeniNotKayit.Donem = Sabitler.Donem;
            yeniNotKayit.PersonelID = Sabitler.KullaniciID;
            yeniNotKayit.IPAdresi = Sabitler.IPAdresi;
            yeniNotKayit.KullaniciAdi = Sabitler.KullaniciAdi;
            yeniNotKayit.ListBasariNot = notListesi;

            return Json(BASARINOTMAIN.KaydetBasariNot(yeniNotKayit, Sabitler.KullaniciAdi, Sabitler.IPAdresi), JsonRequestBehavior.AllowGet);
        }

        public ActionResult BasariNotDegerlendirme(int grupID)
        {

            Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME.BasariNotYonetim degerlendirmeBL = new Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME.BasariNotYonetim(grupID, Sabitler.KullaniciID, Sabitler.IPAdresi, Sabitler.KullaniciAdi);

            Sabis.Bolum.Core.ABS.DEGERLENDIRME.BasariDegerlendirmeVM result = new Sabis.Bolum.Core.ABS.DEGERLENDIRME.BasariDegerlendirmeVM
            {
                EnumDegerlendirmeAsama = degerlendirmeBL.EnumDegerlendirmeAsama,
                SonHalYetkisiVarMi = true, //degerlendirmeBL.KoordinatorMu,  // RMZN edit : her hoca kendi dersine son hal verebilir.
                SonHalKaldirmaYetkisiVarMi = degerlendirmeBL.SonHalKaldirmaYetkisiVarMi,
                OrtakGruplar = degerlendirmeBL.OrtakGruplar,
                DersGrupID = grupID,
                NotificationList = degerlendirmeBL.NotificationList,
                ButunlemeMi = degerlendirmeBL.ButunlemeMi,
                TarihUygunMu = degerlendirmeBL.TarihUygunMu
            };

            ViewBag.grupID = grupID;

            return View(result);
        }

        public JsonResult BasariNotYayinIslem(int fkDersPlanAnaID, int fkDersGrupID, bool yayinDurum)
        {   
            return Json(BASARINOTMAIN.BasariNotYayinIslem(fkDersPlanAnaID, fkDersGrupID, yayinDurum));
        }

    }
}