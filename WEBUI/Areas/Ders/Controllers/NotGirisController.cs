﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Sabis.Bolum.Core.ABS.NOT;
using Sabis.Bolum.Bll.SOURCE.ABS.NOT;

namespace Sabis.Bolum.WebUI.Areas.Ders.Controllers
{
    public class NotGirisController : Controller
    {
        // GET: Ders/NotGiris
        public ActionResult Index()
        {
            //değer boş gelirse ders detay yönlendir
            if (TempData["degerler"] == null)
            {
                return Redirect("/Ders/Detay/Grup/" + Request.RequestContext.RouteData.Values["id"]);
            }
            else
            {
                TempData["degerler"] = TempData["degerler"];
                TempData["degerlerAd"] = TempData["degerlerAd"];
            }
            return View();
        }

        ArrayList liste = new ArrayList();
        public ActionResult DiziOlustur(string deger)
        {
            liste.Add(deger);
            TempData["degerler"] = liste;
            ViewData["degerler"] = liste;
            return View(ViewData["degerler"]);
        }

        public JsonResult OgrenciBilgileri(string listPayID, int id)
        {
            return Json(NOTMAIN.GetirOgrenciNotlari(Sabitler.GetirDersDetay(id).DersGrupID, listPayID), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Grup(string id)
        {
            if (TempData["degerler"] == null)
            {
                HttpContext.Response.Redirect("/Ders/Detay/Grup/" + id);
            }
            return View();
        }

        public JsonResult GrupNotKaydet(List<NotKayitVM> liste)
        {
            return Json(NOTMAIN.NotKaydet(liste, Request.UserHostAddress, Sabitler.KullaniciAdi), JsonRequestBehavior.AllowGet);
        }

        public JsonResult ListeleAnaOgrenciBilgileri(string listPayID, int id)
        {
            if (listPayID == null || listPayID == "")
            {
                return Json(false);
            }
            List<int> payIDS = new List<int>();
            payIDS = listPayID.Split('-').Select(Int32.Parse).ToList();

            NotGirisListParamModel yeniListe = new NotGirisListParamModel();

            yeniListe.DersPlanAnaID = id;
            yeniListe.PayIDList = payIDS;
            yeniListe.PersonelID = Sabitler.KullaniciID;
            yeniListe.Yil = Sabitler.Yil;
            yeniListe.Donem = Sabitler.Donem;

            return Json(NOTMAIN.ListeleOgrenciNotlariv2(yeniListe));
        }

        public ActionResult Ana(int id)
        {
            if (TempData["degerler"].ToString().Length < 1)
            {
                HttpContext.Response.Redirect("/Ders/Detay/Ana/" + id);
            }
            return View();
        }

        public JsonResult AnaNotKaydet(List<NotKayitVM> liste)
        {
            return Json(NOTMAIN.AnaNotKaydet(liste, Sabitler.Yil, Sabitler.Donem, Sabitler.IPAdresi, Sabitler.KullaniciAdi));
        }

        public class ExcelModel
        {
            public HttpPostedFileBase MyFile { get; set; }
        }
        public JsonResult ExcelUpload(FormCollection formCollection)
        {
            var ogrenciListesi = new List<OgrenciListesiVM>();
            if (Request != null)
            {
                HttpPostedFileBase file = Request.Files["MyFile"];
                if ((file != null) && (file.ContentLength > 0) && !string.IsNullOrEmpty(file.FileName))
                {
                    string fileName = file.FileName;
                    string fileContentType = file.ContentType;
                    byte[] fileBytes = new byte[file.ContentLength];
                    var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));

                    using (var package = new OfficeOpenXml.ExcelPackage(file.InputStream))
                    {
                        var currentSheet = package.Workbook.Worksheets;
                        var workSheet = currentSheet.First();
                        var noOfCol = workSheet.Dimension.End.Column;
                        var noOfRow = workSheet.Dimension.End.Row;

                        for (int i = 2; i <= noOfRow; i++)
                        {
                            var ogrenci = new OgrenciListesiVM();
                            ogrenci.Numara = workSheet.Cells[i, 1].Value.ToString();
                            ogrenci.Ad = workSheet.Cells[i, 2].Value.ToString();

                            for (int j = 3; j <= noOfCol; j++)
                            {
                                var yeniNot = new OgrenciNotlariVM();
                                //yeniNot.Notu = workSheet.Cells[i, j].Value != null ? workSheet.Cells[i, j].Value.ToString() : "";
                                yeniNot.Notu = Convert.ToString(workSheet.Cells[i, j].Value != null ? workSheet.Cells[i, j].Value.ToString() : 101.ToString());
                                yeniNot.PayID = Convert.ToInt32(System.Text.RegularExpressions.Regex.Match(workSheet.Cells[1, j].Value.ToString(), @"(?<=\().+?(?=\))").Value);

                                ogrenci.OgrenciNotListesi.Add(yeniNot);
                            }
                            ogrenciListesi.Add(ogrenci);
                        }
                    }
                }
            }
            return Json(ogrenciListesi);
        }
        [AllowAnonymous]
        public void ExcelIndir(int id, string pay, string ad, string dersAdi, string dersGrup)
        {
            bool baslikEklendiMi = false;
            var payList = pay.Split('-');
            var payAdList = ad.Split('-');
            List<OgrenciListesiVM> ogrenciListesi = NOTMAIN.GetirOgrenciNotlari(Sabitler.GetirDersDetay(id).DersGrupID, pay);

            List<string[]> arrayList = new List<string[]>();


            string[] arrayBaslik = new string[payList.Count() + 2];
            if (!baslikEklendiMi)
            {
                arrayBaslik[0] = "Numara";
                arrayBaslik[1] = "Öğrenci";
                for (int i = 0; i < payList.Length; i++)
                {
                    arrayBaslik[i + 2] = payAdList[i].Replace("(", "").Replace(")", "") + " (" + payList[i] + ")";
                }
                arrayList.Add(arrayBaslik);
                baslikEklendiMi = true;
            }


            foreach (var item in ogrenciListesi)
            {
                string[] array = new string[payList.Count() + 2];
                array[0] = item.Numara;
                array[1] = item.Ad + " " + item.Soyad;

                foreach (var subItem in item.OgrenciNotListesi)
                {
                    //array[item.OgrenciNotListesi.IndexOf(subItem) + 2] = subItem.Notu;
                    array[payList.ToList().IndexOf(Convert.ToString(subItem.PayID)) + 2] = subItem.Notu;
                }

                arrayList.Add(array);
            }

            using (var excelFile = new OfficeOpenXml.ExcelPackage())
            {
                OfficeOpenXml.ExcelWorksheet sheet = excelFile.Workbook.Worksheets.Add("Sayfa1");

                sheet.Cells["A1"].LoadFromArrays(arrayList);

                sheet.Cells[sheet.Dimension.Address].AutoFitColumns();

                //sheet.Protection.AllowInsertRows = false;
                //sheet.Protection.AllowSort = false;
                //sheet.Protection.AllowSelectUnlockedCells = true;
                //sheet.Protection.AllowAutoFilter = false;

                //not girilecek kolonların kilidini kaldır
                for (int i = 0; i < payList.Count(); i++)
                {
                    sheet.Column(i + 3).Style.Locked = false;
                }

                //başlık satırını kilitle
                sheet.Row(1).Style.Locked = true;

                sheet.Protection.IsProtected = true;

                //sheet.Cells["A1:H1"].Style.Locked = true;

                Byte[] fileBytes = excelFile.GetAsByteArray();

                //Download ediyor.
                System.Web.HttpContext.Current.Response.Clear();
                System.Web.HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.Private);
                System.Web.HttpContext.Current.Response.Expires = -1;
                System.Web.HttpContext.Current.Response.Buffer = true;

                System.Web.HttpContext.Current.Response.Charset = System.Text.UTF8Encoding.UTF8.WebName;
                System.Web.HttpContext.Current.Response.ContentEncoding = System.Text.UTF8Encoding.UTF8;
                System.Web.HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                System.Web.HttpContext.Current.Response.AddHeader("content-disposition", "attachment; size=" + fileBytes.Length.ToString() + ";filename=\"" + dersAdi + " " + dersGrup + " GRUBU NOT LİSTESİ.xls" + "\"");
                System.Web.HttpContext.Current.Response.BinaryWrite(fileBytes);
                System.Web.HttpContext.Current.Response.End();

            }
        }
    }
}