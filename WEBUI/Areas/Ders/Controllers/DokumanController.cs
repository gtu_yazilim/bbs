﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Sabis.Bolum.Core.ABS.DERS;
using System.IO;
using Sabis.Bolum.Bll.SOURCE.ABS.DERS;

namespace Sabis.Bolum.WebUI.Areas.Ders.Controllers
{
    [AllowAnonymous]
    public class DokumanController : Controller
    {
        // GET: Ders/Dokuman
        public ActionResult Liste(int id)
        {
            return View();
        }

        public ActionResult _pDokumanListesi(int id)
        {
            return PartialView(DOKUMANMAIN.GetirDokumanListesi(id, Sabitler.KullaniciID, Sabitler.Yil, Sabitler.Donem, true));
        }

        public ActionResult _pEkle(int id) { return View(DOKUMANMAIN.GetirDokumanListesi(id, Sabitler.KullaniciID, Sabitler.Yil, Sabitler.Donem, true)); }

        public ActionResult _pDuzenle(int id) { return View(DOKUMANMAIN.GetirDokuman(id)); }

        public ActionResult _pSil(int id) { return View(); }

        [HttpPost]
        public JsonResult Upload()
        {
            var httpRequest = HttpContext.Request;
            DokumanVM yeniDokuman = new DokumanVM();
            foreach (var item in httpRequest.Files)
            {
                Dictionary<string, string> fileAttributes = new Dictionary<string, string>();
                var file = Request.Files[item.ToString()];

                yeniDokuman.DosyaAdi = Path.GetFileNameWithoutExtension(file.FileName);
                
                yeniDokuman.DosyaTuru = Path.GetExtension(file.FileName);

                yeniDokuman.DosyaBoyutu = file.ContentLength;
                
                if (!Sabitler.DosyaUzantiKontrol(yeniDokuman.DosyaTuru))
                {
                    continue;
                }

                if (yeniDokuman.DosyaTuru == ".mp4" || yeniDokuman.DosyaTuru == ".flv" || yeniDokuman.DosyaTuru == ".mov")
                {
                    yeniDokuman.DosyaKey = SDFSClient.FileClient.UploadPublic(Sabitler.KullaniciAdi, file.ToByteArray(), file.FileName, "abis", "F4tpepP93Hhgd-oT", fileAttributes);
                }
                else
                {
                    yeniDokuman.DosyaKey = SDFSClient.FileClient.Upload(Sabitler.KullaniciAdi, file.ToByteArray(), file.FileName, false, "abis", "F4tpepP93Hhgd-oT", fileAttributes);
                }
                yeniDokuman.DosyaBoyutu = file.ContentLength;
            }
            return Json(yeniDokuman);
        }

        public JsonResult Kaydet(DokumanVM dokuman)
        {
            return Json(DOKUMANMAIN.Kaydet(dokuman, Sabitler.KullaniciAdi, Sabitler.IPAdresi));
        }

        public JsonResult Sil(int dokumanID)
        {
            return Json(DOKUMANMAIN.Sil(dokumanID, Sabitler.KullaniciAdi, Sabitler.IPAdresi));
        }

        public JsonResult GrupEkle(int dokumanID, int dersGrupID)
        {
            return Json(DOKUMANMAIN.GrupEkle(dokumanID, dersGrupID, Sabitler.KullaniciAdi, Sabitler.IPAdresi));
        }

        public JsonResult GrupSil(int dokumanID, int dersGrupID)
        {
            return Json(DOKUMANMAIN.GrupSil(dokumanID, dersGrupID, Sabitler.KullaniciAdi, Sabitler.IPAdresi));
        }
    }
}