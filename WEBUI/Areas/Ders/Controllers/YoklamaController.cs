﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sabis.Bolum.WebUI.Areas.Ders.Controllers
{
    public class YoklamaController : Controller
    {
        public ActionResult Liste()
        {
            return View();
        }

        public JsonResult GetirOgrenciListe(int dersGrupHocaID, DateTime tarih)
        {
            return Json(Sabis.Bolum.Bll.SOURCE.ABS.DERS.YOKLAMAMAIN.GetirYoklamaListe(dersGrupHocaID, tarih));
        }

        public JsonResult KaydetYoklama(List<Sabis.Bolum.Core.ABS.DERS.DERSDETAY.YoklamaVM> yoklamaListe)
        {
            return Json(Sabis.Bolum.Bll.SOURCE.ABS.DERS.YOKLAMAMAIN.KaydetYoklama(yoklamaListe));
        }

        public ActionResult KatilimOrani(int id)
        {
            return View(Sabis.Bolum.Bll.SOURCE.ABS.DERS.YOKLAMAMAIN.ListeleKatilimOraniV2(id));
        }

        [AllowAnonymous]
        public ActionResult Yoklama(int id)
        {
            return View();
        }
    }
}