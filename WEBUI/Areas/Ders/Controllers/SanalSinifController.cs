﻿using LMS.VClass.Helpers;
using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Bolum.Core.ABS.DERS.SANALSINIF.Dto;
using Sabis.Bolum.Core.ABS.DERS.SANALSINIF.Interface;
using Sabis.Bolum.Core.ABS.DERS.SANALSINIF.Models;
using Sabis.Bolum.Core.ENUM.ABS;
using SauLive;
using SauLive.Enums;
using SauLive.Models.Participants;
using SauLive.Models.Session;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace Sabis.Bolum.WebUI.Areas.Ders.Controllers
{
    [AllowAnonymous]
    public class SanalSinifController : Controller
    {
        //silinecek
        UYSv2Entities db = new UYSv2Entities();

        private ISanalSinif _sanalsinif;
        public SanalSinifController(ISanalSinif sanalsinif)
        {
            _sanalsinif = sanalsinif;
        }
        public ActionResult Toplanti(int id)
        {
            ViewBag.HocaID = id;
            return View(_sanalsinif.SanalSinifListe(id));
        }
        public ActionResult Ekle(int id)
        {
            var bilgi = Sabitler.GetirDersDetay(id);
            //Weeks();

            var week = Enumerable.Range(1, 14).Select(x => new SelectListItem() { Text = x + ". Hafta".ToString(), Value = x.ToString() }).ToList();
            ViewBag.weeks = new SelectList(week, "Value", "Text");

            var model = new SanalSinifDto();
            model.Ad = Week + ". Hafta Sanal Sınıf";
            model.Hafta = Week;
            model.Baslangic = DateTime.Now;
            model.Sure = 60;
            model.hocagrups = _sanalsinif.HocaninGruplari_Ekle(bilgi.PersonelID.Value, bilgi.DersPlanAnaID, Sabitler.Yil, Sabitler.Donem);
            model.FKDersPlanAnaID = bilgi.DersPlanAnaID;
            model.FKDersPlanID = bilgi.DersPlanID;
            model.FKDersGrupHocaID = id;
            model.TumDersGruplarinaEkle = false;
            return View(model);
        }
        [HttpPost]
        public ActionResult Ekle(SanalSinifDto model)
        {
            var week = Enumerable.Range(1, 14).Select(x => new SelectListItem() { Text = x + ". Hafta".ToString(), Value = x.ToString() }).ToList();
            ViewBag.weeks = new SelectList(week, "Value", "Text");

            #region validation
            if (model.Baslangic != null)
            {               
                if (model.Baslangic < DateTime.Now.AddMinutes(-10))
                {
                    ModelState.AddModelError("Baslangic", "Başlangıç tarihi şuandan önce olamaz !");
                }
            }
            if (model.Hafta > 14)
            {
                ModelState.AddModelError("Hafta", "1-14. haftalar arası geçerlidir.");
            }
            if (model.hocagrups.Where(x => x.Selected == true).Count() == 0 && !model.TumDersGruplarinaEkle )
            {
                ModelState.AddModelError("Sınıf", "Sınıf Seçiniz.");
            }        
            #endregion
            if (ModelState.IsValid)
            {
                model.Silindi = false;
                model.Ekleyen = Sabitler.KullaniciAdi;
                model.EkleyenID = Sabitler.KullaniciID;
                model.EklemeTarihi = DateTime.Now;
                model.Guncelleyen = Sabitler.KullaniciAdi;
                model.GuncelleyenID = Sabitler.KullaniciID;
                model.GuncellemeTarihi = DateTime.Now;
                model.Replay = true;
                model.Bitis = model.Baslangic.AddMinutes(model.Sure);
                model.ProviderType = model.SauLive == true ? 2 : 1;

                var kayitsonucID = _sanalsinif.SanalSinifKaydet(model);
                if (kayitsonucID != 0)
                {
                    if (model.TumDersGruplarinaEkle)
                    {
                        var tumDersGruplari = db.OGRDersGrup.Where(x => x.FKDersPlanAnaID == model.FKDersPlanAnaID && x.OgretimYili == Sabitler.Yil && x.EnumDonem == Sabitler.Donem).Select(x => x.ID).ToList();
                        foreach (var item in tumDersGruplari)
                        {
                            var kontrol = _sanalsinif.SanalSinifGrupKontrol(kayitsonucID, item);
                            if (kontrol)
                            {
                                var sgrup = new SanalSinifGrup();
                                sgrup.SSinifID = kayitsonucID;
                                sgrup.FKDersGrupID = item;
                                sgrup.FKDersPlanID = model.FKDersPlanID;
                                sgrup.FKDersPlanAnaID = model.FKDersPlanAnaID;
                                db.SanalSinifGrup.Add(sgrup);
                                db.SaveChanges();
                            }
                        }
                    }
                    else
                    {
                        var grupekle = _sanalsinif.SanalSinifGrupEkle(model, kayitsonucID, model.FKDersPlanAnaID, model.FKDersPlanID);
                        if (grupekle == false)
                        {
                            //gruplar eklenemezse sanal sınıfı sildir.
                        }
                    }
                    
                    return RedirectToAction("Toplanti", new { @id = model.FKDersGrupHocaID });
                }
            }
            return View(model);
        }
        public ActionResult Duzenle(int id, int sanalSinifID)
        {
            var model = _sanalsinif.SanalSinifGetir(sanalSinifID);
            if (model != null)
            {
                var bilgi = Sabitler.GetirDersDetay(id);
                model.hocagrups = _sanalsinif.HocaninGruplari_Duzenle(bilgi.PersonelID.Value, bilgi.DersPlanAnaID, Sabitler.Yil, Sabitler.Donem, sanalSinifID);
                model.FKDersGrupHocaID = bilgi.DersGrupHocaID;
                model.FKDersPlanAnaID = bilgi.DersPlanAnaID;
                model.FKDersPlanID = bilgi.DersPlanID;
                return View(model);
            }
            ModelState.Clear();
            return View();
        }
        [HttpPost]
        public ActionResult Duzenle(SanalSinifDto model)
        {
            #region validation
            //if (model.Baslangic != null)
            //{
            //    if (model.Baslangic < DateTime.Now)
            //    {
            //        ModelState.AddModelError("Baslangic", "Başlangıç tarihi şuandan önce olamaz !");
            //    }
            //}
            if (model.hocagrups.Where(x => x.Selected == true).Count() == 0)
            {
                ModelState.AddModelError("Sınıf", "Sınıf Seçiniz.");
            }
            #endregion
            if (ModelState.IsValid)
            {
                try
                {
                    model.Guncelleyen = Sabitler.KullaniciAdi;
                    model.GuncelleyenID = Sabitler.KullaniciID;
                    model.GuncellemeTarihi = DateTime.Now;
                    var result = _sanalsinif.SanalSinifDuzenle(model);
                    if (result != 0)
                    {
                        if (model.TumDersGruplarinaEkle)
                        {
                            var tumDersGruplari = db.OGRDersGrup.Where(x => x.FKDersPlanAnaID == model.FKDersPlanAnaID && x.OgretimYili == Sabitler.Yil && x.EnumDonem == Sabitler.Donem).Select(x => x.ID).ToList();
                            foreach (var item in tumDersGruplari)
                            {
                                var kontrol = _sanalsinif.SanalSinifGrupKontrol(result, item);
                                if (kontrol)
                                {
                                    var sgrup = new SanalSinifGrup();
                                    sgrup.SSinifID = result;
                                    sgrup.FKDersGrupID = item;
                                    sgrup.FKDersPlanID = model.FKDersPlanID;
                                    sgrup.FKDersPlanAnaID = model.FKDersPlanAnaID;
                                    db.SanalSinifGrup.Add(sgrup);
                                    db.SaveChanges();
                                }
                            }
                        }
                        else
                        {
                            //yeni seçilen sınıfları ekle kaldırılanı kaldır.
                            _sanalsinif.SanalSinifGrupEkle(model, result, model.FKDersPlanAnaID, model.FKDersPlanID);
                            
                        }
                        return RedirectToAction("Toplanti", new { @id = model.FKDersGrupHocaID });
                    }
                    return RedirectToAction("Toplanti", new { @id = model.FKDersGrupHocaID });
                }
                catch (Exception ex)
                {

                    throw;
                }
            }
            return View();
        }
        public ActionResult Sil(int id, int sanalsinifid)
        {
            var bilgi = Sabitler.GetirDersDetay(id);
            var model = new SanalSinifSilDto();
            model.sanalsinif = _sanalsinif.SanalSinifGetir(sanalsinifid);
            model.sanalsinif_gruplar = _sanalsinif.SanalSinifGrupGetir(sanalsinifid);
            model.FKDersGrupHocaID = id;
            return View(model);
        }
        [HttpPost]
        public ActionResult Sil(SanalSinifSilDto model)
        {
            var gruplarisil = _sanalsinif.SanalSinifGrupSil(model.sanalsinif.ID);
            return RedirectToAction("Toplanti", new { @id = model.FKDersGrupHocaID});
        }
        public ActionResult Katil(int id)
        {
            var kullanicibilgi = Sabitler.GetirPersonelBilgi(Sabitler.KullaniciAdi);
            var vc = db.SanalSinif.Find(id);
            if (vc == null)
            {
                return HttpNotFound();
            }
            if (vc.ProviderType == (int)EnumSanalSinifProvider.PERCULUS)
            {
                #region PERCULUS
                var vm = new KatilVM
                {
                    Ad = vc.Ad,
                    SSinifID = vc.ID
                };
                var status = VcHelper.CheckDate(vc.Baslangic, vc.Sure);
                vm.Durum = status;
                var errorMessage = "";
                switch (status)
                {
                    case SSDurum.Bitti:
                        if (vc.SSinifKey.HasValue)
                        {
                            var sResult = VcHelper.ShowReplay(vc.SSinifKey, KullaniciTipEnum.Hoca, vc.SunucuID.Value, ref errorMessage);
                            if (sResult != null)
                                vm.Adres = Server.UrlDecode(sResult).Replace("local.sabis.", "");
                            else
                                vm.DurumMesaj = errorMessage;
                        }
                        else
                            vm.DurumMesaj = "Ders Yapılmamış";
                        break;
                    case SSDurum.Basladi:
                        if (vc.SSinifKey.HasValue)
                        {
                            var rs = VcHelper.RegisterAttendee(KullaniciTipEnum.Hoca, vc.SSinifKey.Value, vc.SunucuID.Value);
                            if (rs != null)
                                //vm.Adres = Server.UrlDecode(rs);
                                vm.Adres = Server.UrlDecode(rs).Replace("local.sabis.", "");
                        }
                        else
                        {
                            var sunucuid = _sanalsinif.SunucuSec(vc.Baslangic, vc.Sure).SunucuID;

                            var result = VcHelper.VcStart(vc, KullaniciTipEnum.Hoca, sunucuid, ref errorMessage);
                            if (result != null)
                                //vm.Adres = Server.UrlDecode(result);
                               vm.Adres = Server.UrlDecode(result).Replace("local.sabis.", "");
                            else
                                vm.DurumMesaj = errorMessage;

                            if (vc.SSinifKey.HasValue)
                            {
                                vc.SunucuID = sunucuid;
                                vc.Adres = vm.Adres;
                                db.Entry(vc).State = EntityState.Modified;
                                db.SaveChanges();
                            }

                        }
                        break;
                    case SSDurum.Baslamadi:
                        vm.DurumMesaj = vc.Baslangic.ToString("dd MMMM yyyy HH:mm");
                        break;
                }



                if (vm.Adres != null)
                {
                    return Redirect(vm.Adres);
                }
                return View(vm);
                #endregion
            }
            else if (vc.ProviderType == (int)EnumSanalSinifProvider.SAULIVE)
            {
                #region SAULIVE
                var vm = new KatilVM {SSinifID = vc.ID,Ad = vc.Ad};
                vm.Durum = (SSDurum)SauLiveHelper.TarihKontrol(vc.Baslangic, vc.Sure);

                switch (vm.Durum)
                {
                    case SSDurum.Baslamadi:
                        vm.DurumMesaj = vc.Baslangic.ToString("dd MMMM yyyy HH:mm") + " tarihinde başlayacaktır.";
                        break;
                    case SSDurum.Basladi:
                        if (vc.SSinifKey.HasValue)
                        {
                            vm.Adres = vc.Adres;
                            return Redirect(vm.Adres);
                        }
                        else
                        {
                            var sbilgi = vc.SanalSinifGrup.FirstOrDefault();
                            var Service = new SessionService(SettingsService.LiveApiUrl, SettingsService.UName, SettingsService.UPass);
                            var session = new SessionCreate()
                            {
                                UnitId = sbilgi.OGRDersGrup.FKBirimID,
                                UnitName = sbilgi.OGRDersGrup.Birimler.BirimAdi,
                                CourseId = sbilgi.FKDersPlanID,
                                CourseName = sbilgi.OGRDersGrup.OGRDersPlanAna.DersAd,
                                //  GRUP BOŞ OLABİLİR Mİ 
                                GroupId = sbilgi.FKDersGrupID,
                                GroupName  = sbilgi.OGRDersGrup.GrupAd,

                                GroupingId = null,
                                ParentTitle = sbilgi.OGRDersPlan.BolKodAd + sbilgi.OGRDersPlan.DersKod +" - " + vc.Ad,
                                TypeId = 1,
                                Title = vc.Ad,
                                Description = vc.Ad,
                                Date = vc.Baslangic.ToString("dd.MM.yyyy"),
                                Time = vc.Baslangic.ToString("HH:mm"),
                                Duration = vc.Sure,
                                AccessMethod = 1,
                                AccessPassword = null,
                                AccessMode = 1,
                                ReplayEnable = true,
                                InvitationEnable = false,
                                ManagerMail = kullanicibilgi.KullaniciAdi + "@gtu.edu.tr",

                            };
                            session.Participants = new List<ParticipantCreate>();
                            session.Participants.Add(new ParticipantCreate()
                            {
                                Code = Guid.NewGuid(),
                                Email = kullanicibilgi.KullaniciAdi + "@gtu.edu.tr",
                                Name = kullanicibilgi.Ad,
                                Surname = kullanicibilgi.Soyad,
                                Role = "Manager",
                                IsManager = true,
                                UserByEmail = true,
                            });
                            var sonuc = Service.Create(session);
                            if (sonuc.Success && sonuc.SessionId.HasValue)
                            {
                                vm.DurumMesaj = "Sanal Sınıfa Katılabilirsiniz";
                                vc.SSinifKey = sonuc.SessionId;
                                vc.SunucuID = 9;
                                vc.Adres = SettingsService.LiveUrl+"/Live/Live?Code=" + sonuc.Participants.LastOrDefault().Code;
                                db.Entry(vc).State = EntityState.Modified;
                                db.SaveChanges();

                                return Redirect(SettingsService.LiveUrl + "/Live/Live?Code=" + sonuc.Participants.LastOrDefault().Code);
                            }
                            else if (!sonuc.IsValid)
                            {
                                vm.DurumMesaj = sonuc.ErrorMessage;
                            }
                            else if (sonuc.Error)
                            {
                                var http = (HttpWebRequest)WebRequest.Create("https://saulive.gtu.edu.tr");
                                var response = http.GetResponse();
                                vm.DurumMesaj = response.ToString() + sonuc.ErrorMessage;
                            }
                        }
                        break;
                    case SSDurum.Bitti:
                        if (vc.SSinifKey.HasValue)
                        {
                            vm.Adres = vc.Adres;
                            return Redirect(vc.Adres);
                        }
                        else
                        {
                            vm.DurumMesaj = "Ders Yapılmamış";
                        }
                        break;
                    case SSDurum.Yapilmadi:
                        break;
                    default:
                        break;
                }
                return View(vm);
                #endregion
            }
            return View();

            
        }

        //public JsonResult RoomPaketle(int roomid)
        //{
        //    return Json(VcHelper.RoomPaket(roomid));
        //}

        public JsonResult SorunBildir(int id, bool deger)
        {
            return Json(_sanalsinif.SorunBildir(id,deger));
        }

        public ActionResult RoomPaketle(int roomid,int sunucuid)
        {
            return View(VcHelper.RoomPaket(roomid, sunucuid));
        }

        #region Hafta
        public void GetRange(string name, int startIndex, int count, int selectedValue)
        {
            ViewData[name] = Enumerable.Range(startIndex, count)
                    .Select(
                        x =>
                            new SelectListItem
                            {
                                Text = x.ToString(),
                                Value = x.ToString(),
                                Selected = (x == selectedValue)
                            })
                    .ToList();
        }
        public void Weeks()
        {
            GetRange("Weeks", 1, 14, Week);
        }
        public static int Week
        {
            get
            {
                return 1;
            }
        }
        public static IEnumerable<SelectListItem> Weeks(int? selected = null, int start = 1, int end = 14)
        {
            if (!selected.HasValue)
                selected = Week;
            return Enumerable.Range(start, end)
                       .Select(
                           x =>
                               new SelectListItem
                               {
                                   Text = x.ToString(),
                                   Value = x.ToString(),
                                   Selected = (x == selected)
                               });

        }
        #endregion

        #region Raporlama
        public ActionResult Rapor()
        {
            return View();
        }
        public ActionResult AktifSure()
        {
            var test1 = VcHelper.GetirHocaAktifSure(2313);
            return View();
        }
        #endregion

        #region SorunBildirim

        public ActionResult SorunBildirim(int id, int sid, int m)
        {
            try
            {
                var model = _sanalsinif.SorunBildirimleriniGetir(sid);
                return View(model);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public ActionResult SorunBildirimDetay(int gid)
        {
            try
            {
                var model = _sanalsinif.SorunBildirimGetirDetay(gid);
                return View(model);
            }
            catch (Exception EX)
            {

                throw;
            }
            
        }

        public JsonResult SanalSinifBildirimKaydet(int gid, string Cevap)
        {
            return Json(_sanalsinif.SanalSinifBildirimKaydet(gid, Cevap));
        }

        #endregion
    }
}