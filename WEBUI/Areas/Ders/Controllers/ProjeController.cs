﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Sabis.Bolum.Bll.SOURCE.ABS.DERS;
using Sabis.Bolum.Core.ABS.DERS.PROJE;
using Sabis.Bolum.Core.ABS.DERS.DERSDETAY;
using Sabis.Bolum.Bll.SOURCE.ABS.NOT;

namespace Sabis.Bolum.WebUI.Areas.Ders.Controllers
{
    [AllowAnonymous]
    public class ProjeController : Controller
    {
        // GET: Ders/Proje
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult _pProjeTakvimi(int id,int grupid, string ekleGoster)
        {
            return PartialView(PROJEMAIN.GetirProjeTakvimi(id, grupid, Sabitler.Yil, Sabitler.Donem));
        }
        public ActionResult _pProjeTakvimiDuzenle(int id)
        {
            return PartialView(PROJEMAIN.GetirProje(id));
        }
        public JsonResult DuzenleProjeTakvim(ProjeTakvimVM model)
        {
            return Json(PROJEMAIN.GuncelleProje(model));
        }
        public JsonResult EkleProjeTarih(ProjeTakvimVM model)
        {
            return Json(PROJEMAIN.EkleProje(model, Sabitler.Yil, Sabitler.Donem, Sabitler.KullaniciAdi, Sabitler.IPAdresi));
        }
        public JsonResult SilProjeTarih (int id)
        {
            return Json(PROJEMAIN.SilProjeTarih(id));
        }

        #region Proje Konular
        public ActionResult _pProjeKonulari(int id, int projeID)
        {
            ViewBag.pid = projeID;
            return PartialView(PROJEMAIN.ProjeKonuListeGetir(projeID));
        }

        public ActionResult _pProjeKonuDuzenle(int id)
        {
            return View(PROJEMAIN.ProjeKonuGetir(id));
        }
        public JsonResult ProjeKonuEkle(int projeid, string konubaslik, string konu)
        {
            return Json(PROJEMAIN.ProjeKonuEkle(projeid,konubaslik,konu));
        }
        public JsonResult ProjeKonuDuzenle(int konuid, string konubaslik, string konu)
        {
            return Json(PROJEMAIN.ProjeKonuDuzenle(konuid,konubaslik,konu));
        }
        public JsonResult ProjeKonuSil(int konuid)
        {
            return Json(PROJEMAIN.ProjeKonuSil(konuid));
        }
        #endregion

        #region Proje Öğrenci Konu
        public ActionResult ProjeOgrenci(int id, int projeID)
        {
            DersDetayVM dersDetay = Sabitler.GetirDersDetay(id);

            var konular = PROJEMAIN.ProjeKonuListeGetir(projeID);
            var ogrenciler = BASARINOTMAIN.GetirBasariNotListesi(dersDetay.DersGrupID, Sabitler.KullaniciID, dersDetay.ButunlemeMiOrijinal);

            var model = new ProjeKonuOgrenci();
            model.projeKonu = konular;
            model.ogrenciListesi = ogrenciler;

            var test = model;

            return View(model);
        }
        public JsonResult OgrenciKonuKaydet(int ProjeID, int KonuID, List<int> OgrenciListe)
        {

            return Json(PROJEMAIN.OgrenciKonuKaydet(ProjeID,KonuID,OgrenciListe));
        }
        public JsonResult OgrenciKonuSil(int ProjeID, int KonuID, int OgrenciID)
        {

            return Json(PROJEMAIN.OgrenciKonuSil(ProjeID,KonuID, OgrenciID));
        }
        #endregion

        #region Raporlar
        public ActionResult RaporDetay(int raporid)
        {
            return View(PROJEMAIN.RaporGetir(raporid));
        }
        public JsonResult RaporDuzenle(ProjeRaporDto model)
        {
            return Json(PROJEMAIN.RaporDuzenle(model));
        }
        #endregion

    }
}