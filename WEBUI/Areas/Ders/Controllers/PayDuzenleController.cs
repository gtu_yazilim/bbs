﻿using Sabis.Bolum.Core.ABS.DERS.DERSDETAY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sabis.Bolum.WebUI.Areas.Ders.Controllers
{
    public class PayDuzenleController : Controller
    {
        // GET: Ders/PayDuzenle
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetirPayListesi(int id)
        {
            var dersDetay = Sabitler.GetirDersDetay(id);
            var payListe = Sabis.Bolum.Bll.SOURCE.ABS.DERS.PAYMAIN.GetirPayBilgileri(dersDetay.DersGrupID, true);
            EksikPayGuncelle(id, payListe);
            return PartialView(payListe.OrderBy(s => s.SiraNo).ThenBy(c => c.EnumCalismaTipi).ToList());
        }

        public void EksikPayGuncelle(int id, List<PayBilgileriVM> payListe)
        {
            var dersDetay = Sabitler.GetirDersDetay(id);
            bool basariyaOraniEkliMi = payListe.Any(x => x.EnumCalismaTipi == (int)Sabis.Enum.EnumCalismaTip.YilIcininBasariya);
            bool finalEkliMi = payListe.Any(x => x.EnumCalismaTipi == (int)Sabis.Enum.EnumCalismaTip.Final);



            if (finalEkliMi && !basariyaOraniEkliMi)
            {
                PayBilgileriVM final = payListe.FirstOrDefault(x => x.EnumCalismaTipi == (int)Sabis.Enum.EnumCalismaTip.Final);
                PayBilgileriVM yeniPay = new PayBilgileriVM();
                final.KatkiOrani = 50;
                yeniPay.EnumCalismaTipi = (int)Sabis.Enum.EnumCalismaTip.YilIcininBasariya;
                yeniPay.Donem = final.Donem;
                yeniPay.DersGrupID = final.DersGrupID;
                yeniPay.DersPlanAnaID = final.DersPlanAnaID;
                yeniPay.DersPlanID = final.DersPlanID;
                yeniPay.PersonelID = Sabitler.KullaniciID;
                yeniPay.KatkiOrani = 50;
                yeniPay.SiraNo = Convert.ToByte(final.SiraNo + 1);
                yeniPay.Yil = final.Yil;
                yeniPay.Zaman = DateTime.Now;
                yeniPay.KullaniciAdi = Sabitler.KullaniciAdi;
                yeniPay.IP = Request.UserHostAddress;

                Sabis.Bolum.Bll.SOURCE.ABS.DERS.PAYMAIN.EklePay(yeniPay);
                Sabis.Bolum.Bll.SOURCE.ABS.DERS.PAYMAIN.EklePay(final);

            }
            else if (!basariyaOraniEkliMi)
            {
                PayBilgileriVM yeniPay = new PayBilgileriVM();
                yeniPay.EnumCalismaTipi = (int)Sabis.Enum.EnumCalismaTip.YilIcininBasariya;
                yeniPay.Donem = Sabitler.Donem;
                yeniPay.DersGrupID = dersDetay.DersGrupID;
                yeniPay.DersPlanAnaID = dersDetay.DersPlanAnaID;
                yeniPay.DersPlanID = dersDetay.DersPlanID;
                yeniPay.PersonelID = Sabitler.KullaniciID;
                yeniPay.KatkiOrani = 50;
                yeniPay.SiraNo = Convert.ToByte(payListe.Count() + 1);
                yeniPay.Yil = Sabitler.Yil;
                yeniPay.Zaman = DateTime.Now;
                yeniPay.KullaniciAdi = Sabitler.KullaniciAdi;
                yeniPay.IP = Request.UserHostAddress;

                Sabis.Bolum.Bll.SOURCE.ABS.DERS.PAYMAIN.EklePay(yeniPay);
            }
        }
        
        public JsonResult EklePay(List<PayBilgileriVM> liste, int id)
        {
            if (liste != null)
            {
                foreach (var item in liste)
                {
                    if (item.KatkiOrani == null)
                    {
                        return Json("false");
                    }

                    Sabis.Bolum.Bll.SOURCE.ABS.DERS.PAYMAIN.EklePay(item);
                }
            }
            return Json("true");
        }

        public JsonResult GuncellePay(List<PayVM> payListesi)
        {
            return Json(Sabis.Bolum.Bll.SOURCE.ABS.DERS.PAYMAIN.GuncellePay(payListesi));
        }

        public JsonResult SilPay(int payID)
        {
            return Json(Sabis.Bolum.Bll.SOURCE.ABS.DERS.PAYMAIN.SilPay(payID, Sabitler.KullaniciAdi, Sabitler.IPAdresi));
        }
    }
}