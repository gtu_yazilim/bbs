﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Reflection;
using OfficeOpenXml;
using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Bolum.Core.ABS.DERS.DERSDETAY;
using Sabis.Bolum.Core.ABS.DERS.OSINAV;
using Sabis.Bolum.WebUI.Models;
using System.Net;

namespace Sabis.Bolum.WebUI.Areas.Ders.Controllers
{
    [AllowAnonymous]
    public class OnlineSinavController : Controller
    {
        UYSv2Entities db = new UYSv2Entities();

        #region SINAV
        public ActionResult Liste()
        {
            return View();
        }
        public ActionResult Ekle(int id, int PayID)
        {
            var model = new OSinavVM();
            model.HocaID = id;
            model.PayID = PayID;
            model.Ad = Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.PayAdiGetir(PayID);
            model.GrupID = Sabitler.GetirDersDetay(id).DersGrupID;
            model.hocagrups = Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.HocaninGruplari(id, PayID);
            return View(model);
        }
        [HttpPost]
        public ActionResult Ekle(OSinavVM s)
        {
            #region validation
            if (s.Baslangic.HasValue && s.Bitis.HasValue)
            {
                if (s.Baslangic.Value >= s.Bitis.Value)
                {
                    ModelState.AddModelError("Baslangic", "Başlangıç tarihi bitiş tarihinden sonra olamaz !");
                }
                if (s.Bitis <= DateTime.Now)
                {
                    ModelState.AddModelError("Bitis", "Bitiş tarihi şuandan önce olamaz !");
                }
                if (s.Baslangic < DateTime.Now)
                {
                    ModelState.AddModelError("Baslangic", "Başlangıç tarihi şuandan önce olamaz !");
                }
            }
            //if (s.hocagruplar2.Where(g => g.Selected == true).Count() == 0)
            //{
            //    ModelState.AddModelError("Sınıf Seç", "Sınıf Seçmelisiniz.");
            //}
            #endregion
            if (ModelState.IsValid)
            {
                try
                {
                    var model = new OSinav();
                    model.Ad = s.Ad;
                    model.Aciklama = s.Aciklama;
                    model.Baslangic = s.Baslangic.Value;
                    model.Bitis = s.Bitis.Value;
                    model.Sure = s.Sure.Value;
                    model.SoruSayisi = s.SoruSayisi.Value;
                    model.Yayinlandi = 0;
                    model.Ekleyen = Sabitler.KullaniciAdi;
                    model.EkleyenID = Sabitler.KullaniciID;
                    model.EklemeTarihi = DateTime.Now;
                    model.Guncelleyen = Sabitler.KullaniciAdi;
                    model.GuncelleyenID = Sabitler.KullaniciID;
                    model.GuncellemeTarihi = DateTime.Now;
                    

                    var result = Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.SinavEkle(model);
                    if (result == true)
                    {
                        Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.SinavGrupEkle(s, model.ID);
                        return RedirectToAction("Duzenle", new { id = s.HocaID, sinavID = model.ID });
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            //s.hocagruplar2 = Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.HocayaAitSiniflar2(s.GrupID, s.PayID, Sabitler.Yil, Sabitler.Donem);
            s.hocagrups = Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.HocaninGruplari(s.HocaID, s.PayID);
            return View(s);
        }
        public ActionResult Duzenle(int id, int? sinavID)
        {
            var hocabilgi = Sabitler.GetirDersDetay(id);
            var payid = Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.SinavGrupPayID(sinavID.Value);
            var model = Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.SinavGetir(sinavID.Value);
            var s = new OSinavVM();
            if (model != null)
            {
                s.SinavID = model.ID;
                s.HocaID = hocabilgi.DersGrupHocaID;
                s.Ad = model.Ad;
                s.GrupID = hocabilgi.DersGrupID;
                s.PayID = payid;
                s.Aciklama = model.Aciklama;
                s.Baslangic = model.Baslangic;
                s.Bitis = model.Bitis;
                s.Sure = model.Sure;
                s.SoruSayisi = model.SoruSayisi;
                s.Yayinlandi = 1;
                //s.hocagruplar2 = Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.HocayaAitSiniflar2(hocabilgi.DersGrupID, payid, Sabitler.Yil, Sabitler.Donem);
                s.hocagrups = Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.HocaninGruplari(hocabilgi.DersGrupHocaID, payid);
                return View(s);
            }
            return View();
        }
        [HttpPost]
        public ActionResult Duzenle(OSinavVM s)
        {
            if (s.Baslangic.HasValue && s.Bitis.HasValue)
            {
                if (s.Baslangic.Value >= s.Bitis.Value)
                {
                    ModelState.AddModelError("Baslangic", "Başlangıç tarihi bitiş tarihinden sonra olamaz !");
                }
                if (s.Bitis <= DateTime.Now)
                {
                    ModelState.AddModelError("Bitis", "Bitiş tarihi şuandan önce olamaz !");
                }
                if (s.Baslangic < DateTime.Now)
                {
                    ModelState.AddModelError("Baslangic", "Başlangıç tarihi şuandan önce olamaz !");
                }
            }
            if (ModelState.IsValid)
            {
                try
                {
                    var model = Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.SinavGetir(s.SinavID);
                    var result = Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.SinavDuzenle(s);
                    if (result == true)
                    {
                        Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.SinavGrupEkle(s, model.ID);
                        return RedirectToAction("Liste", new { id = s.HocaID });
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            //s.hocagruplar2 = Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.HocayaAitSiniflar2(s.GrupID, s.PayID, Sabitler.Yil, Sabitler.Donem);
            s.hocagrups = Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.HocaninGruplari(s.HocaID, s.PayID);
            return View(s);
        }

        public ActionResult Sil(int id, int sinavID)
        {
            var hocabilgi = Sabitler.GetirDersDetay(id);
            var payid = Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.SinavGrupPayID(sinavID);
            var model = Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.SinavGetir(sinavID);
            var s = new OSinavVM();
            if (model != null)
            {
                s.SinavID = model.ID;
                s.HocaID = hocabilgi.DersGrupHocaID;
                s.Ad = model.Ad;
                s.GrupID = hocabilgi.DersGrupID;
                s.PayID = payid;
                s.Aciklama = model.Aciklama;
                s.Baslangic = model.Baslangic;
                s.Bitis = model.Bitis;
                s.Sure = model.Sure;
                s.SoruSayisi = model.SoruSayisi;
                s.Yayinlandi = 1; //TODO
                s.Ekleyen = model.Ekleyen;
                //s.hocagruplar2 = Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.HocayaAitSiniflar2(hocabilgi.DersGrupID, payid, Sabitler.Yil, Sabitler.Donem);

                var sinavoturumlari = Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.SinavOturumlari(hocabilgi.DersGrupID, model.ID);
                s.sinavoturumlari = sinavoturumlari;
                //return View(Tuple.Create<OSinavVM, List<SinavOturumDto>>(s, sinavoturumlari));
                return View(s);
            }
            return View();
        }
        [HttpPost]
        public ActionResult Sil(OSinavVM model)
        {
            var silinecekoturumlar = Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.SinavOturumlari(model.GrupID, model.SinavID);
            foreach (var oturumbilgi in silinecekoturumlar)
            {
                // 1. cevap sil
                Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.OgrenciCevapTemizle(oturumbilgi.SinavID, oturumbilgi.OturumID, oturumbilgi.OgrenciID);
                // 2. oturum sil
                Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.OgrenciOturumTemizle(oturumbilgi.SinavID, oturumbilgi.OturumID, oturumbilgi.OgrenciID);
                // 3. enrollment sil
                Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.OgrenciEnrollmentTemizle(oturumbilgi.SinavID, oturumbilgi.OturumID, oturumbilgi.OgrenciID);
            }
            // 4. sınav-grup sil.
            db.OSinavGrup.RemoveRange(db.OSinavGrup.Where(g => g.FKSinavID == model.SinavID).ToList());
            db.SaveChanges();

            //5. sınav soru sil
            db.OSinavSoru.RemoveRange(db.OSinavSoru.Where(s => s.SinavID == model.SinavID).ToList());
            db.SaveChanges();

            //6. sınavı sil. !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            db.OSinav.RemoveRange(db.OSinav.Where(s => s.ID == model.SinavID).ToList());
            db.SaveChanges();



            return RedirectToAction("Liste", new { id = model.HocaID });
        }
        public ActionResult SinaviAskiyaAl(int id, int sinavID)
        {
            Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.SinavAskiyaAl(sinavID);
            return RedirectToAction("Duzenle", new { id = id, sinavID = sinavID });
        }
        public ActionResult SinaviYayinla(int id, int sinavID)
        {
            var sonuc = Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.SinavYayinla(sinavID);
            return RedirectToAction("Duzenle", new { id = id, sinavID = sinavID });
        }
        public ActionResult SinavYeniDenHesapla(int id)
        {
            var sonuc = Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.SinavYenidenHesapla(id);
            if (sonuc == true)
            {
                return Json(new { ASuccess = true }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { ASuccess = false }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region SORU
        public ActionResult SoruListe(int sinavID, int hocaID)
        {
            var model = new OSinavSoruListe();
            model.HocaID = hocaID;
            model.SinavID = sinavID;
            var sorgu = Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.SinavSorulari(sinavID, hocaID);
            if (sorgu != null)
            {
                model.Sorular = sorgu;
            }
            return PartialView(model);
        }
        public ActionResult SoruEkle(int id, int sinavID)
        {
            var zorlukderece = Enumerable.Range(1, 5).Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();
            ViewBag.derece = new SelectList(zorlukderece, "Value", "Text");

            List<int> derece = new List<int>();
            derece.Add(1);
            derece.Add(2);
            derece.Add(3);
            derece.Add(4);
            derece.Add(5);
            //ViewBag.derece = derece;


            var sinav = Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.SinavGetir(sinavID);
            if (sinav != null)
            {
                var soru = new OSinavSoruEkle();
                soru.SinavID = sinav.ID;
                soru.HocaID = id;
                soru.SoruNo = Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.SoruNoBul(sinavID);
                soru.SinavAd = sinav.Ad + " - " + soru.SoruNo + ". Soru";
                soru.ZorlukDerece = derece;
                return View(soru);
            }
            return View();
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SoruEkle(OSinavSoruEkle ss)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var sno = Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.SoruNoBul(ss.SinavID);
                    var soru = new OSinavSoru();
                    soru.SinavID = ss.SinavID;
                    soru.SoruNo = ss.SoruNo;
                    soru.Baslik = WebUtility.HtmlDecode(ss.Baslik);
                    soru.Cevap1 = ss.Cevap1;
                    soru.Cevap2 = ss.Cevap2;
                    soru.Cevap3 = ss.Cevap3;
                    soru.Cevap4 = ss.Cevap4;
                    soru.Cevap5 = ss.Cevap5;
                    soru.DogruCevap = ss.DogruCevap;
                    soru.Silindi = false;
                    soru.Iptal = false;
                    soru.Ekleyen = Sabitler.KullaniciAdi;
                    soru.EkleyenID = Sabitler.KullaniciID;
                    soru.EklemeTarihi = DateTime.Now;
                    soru.Guncelleyen = Sabitler.KullaniciAdi;
                    soru.GuncelleyenID = Sabitler.KullaniciID;
                    soru.GuncellemeTarihi = DateTime.Now;

                    Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.SoruEkle(soru);
                    return RedirectToAction("Duzenle", new { id = ss.HocaID, sinavID = ss.SinavID });
                }
                catch (Exception ex)
                {

                    throw;
                }
            }
            return View(ss);
        }
        public ActionResult SoruDuzenle(int id, int soruID)
        {
            var soruduzenle = new OSinavSoruDuzenle();
            var soru = Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.SoruGetir(soruID);
            if (soru != null)
            {
                soruduzenle.SinavID = soru.SinavID;
                soruduzenle.SoruID = soru.SoruID;
                soruduzenle.SoruNo = soru.SoruNo;
                soruduzenle.HocaID = id;
                soruduzenle.SinavAd = Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.SoruBaslikGetir(soru.SinavID, soru.SoruNo);
                soruduzenle.Baslik = soru.Baslik;
                soruduzenle.Cevap1 = soru.Cevap1;
                soruduzenle.Cevap2 = soru.Cevap2;
                soruduzenle.Cevap3 = soru.Cevap3;
                soruduzenle.Cevap4 = soru.Cevap4;
                soruduzenle.Cevap5 = soru.Cevap5;
                soruduzenle.DogruCevap = soru.DogruCevap;
                soruduzenle.Silindi = soru.Silindi;
                soruduzenle.Iptal = soru.Iptal;
                soruduzenle.Ekleyen = soru.Ekleyen;
                soruduzenle.EkleyenID = soru.EkleyenID;
                soruduzenle.EklemeTarihi = soru.EklemeTarihi;
                soruduzenle.Guncelleyen = soru.Guncelleyen;
                soruduzenle.GuncelleyenID = soru.GuncelleyenID;
                soruduzenle.GuncellemeTarihi = soru.GuncellemeTarihi;


                return View(soruduzenle);
            }
            return View();
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SoruDuzenle(OSinavSoruDuzenle ss)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var islem = Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.SoruDuzenle(ss, Sabitler.KullaniciID, Sabitler.KullaniciAdi);
                    if (islem == true)
                    {
                        return RedirectToAction("Duzenle", new { id = ss.HocaID, sinavID = ss.SinavID });
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            return View(ss);
        }

        public JsonResult SoruSil(int id)
        {
            //soru ve OSinavCevap ilişkisine bak. silinmeye çalışılan soru öğrenciler tarafından görülmüş ise sildirme
            var sorgu = db.OSinavCevap.Where(s => s.SoruID == id).FirstOrDefault();
            if (sorgu == null)
            {
                var soru = db.OSinavSoru.FirstOrDefault(s => s.SoruID == id);
                db.OSinavSoru.Remove(soru);
                db.SaveChanges();
                SinavYeniDenHesapla(soru.SinavID);
                return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { Success = false }, JsonRequestBehavior.AllowGet);
            }
            return Json(null);
        }
        public JsonResult SoruIptal(int id)
        {
            try
            {
                var soru = db.OSinavSoru.Find(id);
                soru.Iptal = true;
                db.SaveChanges();
                SinavYeniDenHesapla(soru.SinavID);
                return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                return Json(new { Success = false }, JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult _pSoruEkle(int id)
        {
            var sinav = db.OSinav.Where(s => s.ID == id).FirstOrDefault();
            if (sinav != null)
            {
                var soru = new OSinavSoru();
                soru.SinavID = sinav.ID;
                soru.SoruNo = Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.SoruNoBul(id);
                return PartialView(soru);
            }
            return PartialView();
        }
        [HttpPost]
        public ActionResult _pSoruEkle(OSinavSoru ss)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var sno = Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.SoruNoBul(ss.SinavID);
                    var soru = new OSinavSoru();
                    soru.SinavID = ss.SinavID;
                    soru.SoruNo = ss.SoruNo;
                    soru.Baslik = WebUtility.HtmlDecode(ss.Baslik);
                    soru.Cevap1 = ss.Cevap1;
                    soru.Cevap2 = ss.Cevap2;
                    soru.Cevap3 = ss.Cevap3;
                    soru.Cevap4 = ss.Cevap4;
                    soru.Cevap5 = ss.Cevap5;
                    soru.DogruCevap = ss.DogruCevap;

                }
                catch (Exception)
                {

                    throw;
                }
            }
            return PartialView(ss);
        }
        #endregion

        #region Excel_Soru_Ekleme
        public JsonResult SoruYukle(int id, int sinavID)
        {
            var sonuc = SoruDosyaYukle(sinavID);
            if (sonuc.Durum == 1)
            {
                SoruDosyaOku(sonuc);
                if (sonuc.Durum == 1)
                {
                    SoruKaydet(sonuc);
                }
            }
            return Json(sonuc, JsonRequestBehavior.AllowGet);
        }
        public SoruImportSonuc SoruDosyaYukle(int sinavID)
        {
            var sonuc = new SoruImportSonuc();
            sonuc.SinavID = sinavID;
            sonuc.ImportId = Guid.NewGuid();
            var vpath = "~/Files/Import/" + Sabitler.KullaniciAdi + "/";
            var path = Server.MapPath(vpath);
            var postedfile = Request.Files[0] as HttpPostedFileBase;
            var uzanti = Path.GetExtension(postedfile.FileName);

            sonuc.DosyaAdi = sonuc.ImportId + "_" + DateTime.Now.ToString("yyyy_MM_dd_hh_mm_ss") + uzanti;
            sonuc.DosyaUrl = vpath + sonuc.DosyaAdi;
            sonuc.Path = path + sonuc.DosyaAdi;
            sonuc.DosyaId = "1";

            try
            {
                if (uzanti != ".xlsx" && uzanti != ".xls")
                {
                    sonuc.Durum = 2;
                    sonuc.Mesaj = "Sadece Excel Dosyası Yükleyebilirsiniz ! (xlsx,xls)";
                }
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                postedfile.SaveAs(path + sonuc.DosyaAdi);
                sonuc.Durum = 1;
                sonuc.Mesaj = "Dosya Yüklendi";
            }
            catch (Exception ex)
            {
                sonuc.Durum = 3;
                sonuc.Mesaj = ex.Message;
            }
            return sonuc;
        }

        public JsonResult ExcelUpload(FormCollection formCollection, int id)
        {
            var soruListesi = new SoruImportSonuc();
            if (Request != null)
            {
                HttpPostedFileBase file = Request.Files["files"];
                if ((file != null) && (file.ContentLength > 0) && !string.IsNullOrEmpty(file.FileName))
                {
                    string fileName = file.FileName;
                    string fileContentType = file.ContentType;
                    byte[] fileBytes = new byte[file.ContentLength];
                    var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));

                    using (var package = new OfficeOpenXml.ExcelPackage(file.InputStream))
                    {
                        var currentSheet = package.Workbook.Worksheets;
                        var workSheet = currentSheet.First();
                        var noOfCol = workSheet.Dimension.End.Column;
                        var noOfRow = workSheet.Dimension.End.Row;

                        var uzanti = Path.GetExtension(file.FileName);
                        soruListesi.SinavID = id;
                        soruListesi.DosyaAdi = fileName;
                        soruListesi.DosyaId =
                        soruListesi.DosyaAdi = soruListesi.ImportId + "_" + DateTime.Now.ToString("yyyy_MM_dd_hh_mm_ss") + uzanti;
                        soruListesi.DosyaId = "1";
                        try
                        {
                            if (uzanti != ".xlsx" && uzanti != ".xls")
                            {
                                soruListesi.Durum = 2;
                                soruListesi.Mesaj = "Sadece Excel Dosyası Yükleyebilirsiniz ! (xlsx,xls)";
                            }
                            soruListesi.Durum = 1;
                            soruListesi.Mesaj = "Dosya Yüklendi";
                        }
                        catch (Exception ex)
                        {
                            soruListesi.Durum = 3;
                            soruListesi.Mesaj = ex.Message;
                        }
                        List<SoruImport> sorular = new List<SoruImport>();
                        
                        for (int i = 2; i <= noOfRow; i++)
                        {
                            var soru = new SoruImport();
                            soru.SoruNo = Convert.ToInt32(workSheet.Cells[i, 1].Value.ToString());
                            soru.Baslik = workSheet.Cells[i, 2].Value.ToString();
                            soru.A = workSheet.Cells[i, 3].Value.ToString();
                            soru.B = workSheet.Cells[i, 4].Value.ToString();
                            soru.C = workSheet.Cells[i, 5].Value.ToString();
                            soru.D = workSheet.Cells[i, 6].Value.ToString();
                            soru.E = workSheet.Cells[i, 7].Value.ToString();
                            soru.DogruCevap = workSheet.Cells[i, 8].Value.ToString();                          
                            sorular.Add(soru);
                        }
                        soruListesi.Sorular = sorular;
                        SoruKaydet(soruListesi);
                    }
                }
            }
            return Json(soruListesi, JsonRequestBehavior.AllowGet);
            //return null;
        }
        private void SoruDosyaOku(SoruImportSonuc sonuc)
        {
            FileInfo fi = new FileInfo(sonuc.Path);
            if (fi.Exists)
            {
                sonuc.Sorular = ExcelOku<SoruImport>(sonuc.Path, 2, 1);
            }
            else
            {
                sonuc.Durum = 2;
                sonuc.Mesaj = "Dosya Bulunamadı.";
            }
        }
        public static List<T> ExcelOku<T>(string path, int fromRow, int fromColumn, int toColumn = 0)
        {
            List<T> retList = new List<T>();
            using (var pck = new ExcelPackage())
            {
                using (var stream = System.IO.File.OpenRead(path))
                {
                    pck.Load(stream);
                }
                var ws = pck.Workbook.Worksheets.First();
                toColumn = toColumn == 0 ? typeof(T).GetProperties().Count() : toColumn;
                List<string> columnNames = new List<string>();
                foreach (var cell in ws.Cells[1, 1, 1, ws.Cells.Count()])
                {
                    columnNames.Add(cell.Value.ToString());
                }
                for (var rowNum = fromRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                {
                    T objT = Activator.CreateInstance<T>();
                    Type myType = typeof(T);
                    PropertyInfo[] myProp = myType.GetProperties();
                    var wsRow = ws.Cells[rowNum, fromColumn, rowNum, ws.Cells.Count()];
                    foreach (var propertyInfo in myProp)
                    {
                        if (columnNames.Contains(propertyInfo.Name))
                        {
                            int position = columnNames.IndexOf(propertyInfo.Name);

                            propertyInfo.SetValue(objT, Convert.ChangeType(wsRow[rowNum, position + 1].Value, propertyInfo.PropertyType));
                        }
                    }

                    retList.Add(objT);
                }
            }
            return retList;
        }
        public void SoruKaydet(SoruImportSonuc sonuc)
        {
            foreach (var s in sonuc.Sorular)
            {
                var soru = db.OSinavSoru.FirstOrDefault(ss => ss.SinavID == sonuc.SinavID && ss.SoruNo == s.SoruNo && !ss.Silindi);
                if (soru == null)
                {
                    soru = new OSinavSoru();
                    soru.SinavID = sonuc.SinavID;
                    soru.Ekleyen = Sabitler.KullaniciAdi;
                    soru.EkleyenID = Sabitler.KullaniciID;
                    soru.EklemeTarihi = DateTime.Now;
                    //db.OSinavSoru.Add(soru);
                }
                else
                {
                    soru.Guncelleyen = Sabitler.KullaniciAdi;
                    soru.GuncelleyenID = Sabitler.KullaniciID;
                    soru.GuncellemeTarihi = DateTime.Now;
                }
                soru.Cevap1 = s.A;
                soru.Cevap2 = s.B;
                soru.Cevap3 = s.C;
                soru.Cevap4 = s.D;
                soru.Cevap5 = s.E;
                soru.DogruCevap = ChooseNumber(s.DogruCevap).Value;
                //soru.SoruNo = OSinavHelper.SoruNoBul(sonuc.SinavID);
                //soru.SoruNo = s.SoruNo;
                soru.SoruNo = Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.SoruNoBul(sonuc.SinavID);
                soru.Baslik = WebUtility.HtmlDecode(s.Baslik);
                soru.Guncelleyen = Sabitler.KullaniciAdi;
                soru.GuncelleyenID = Sabitler.KullaniciID;
                soru.GuncellemeTarihi = DateTime.Now;
                soru.Iptal = false;
                soru.Silindi = false;
                db.OSinavSoru.Add(soru);
                db.SaveChanges();

            }
            sonuc.Durum = 1;
        }
        public int? ChooseNumber(string c)
        {
            switch (c)
            {
                case "A":
                    return 1;
                case "B":
                    return 2;
                case "C":
                    return 3;
                case "D":
                    return 4;
                case "E":
                    return 5;
                default:
                    return null;
            }
        }
        #endregion

        #region GrupOturumTemizle
        public ActionResult GrupOturumTemizle(int id, int sinavid)
        {
            var oturumlar = Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.SinavOturumlari(id, sinavid);
            ViewBag.grupid = id;
            ViewBag.sinavid = sinavid;
            return PartialView(oturumlar);
        }
        public ActionResult GrupOturumTemizleOnay(int id, int sinavid)
        {
            var oturumlar = Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.SinavOturumlari(id, sinavid);
            foreach (var oturum in oturumlar)
            {
                // 1. cevap sil
                Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.OgrenciCevapTemizle(oturum.SinavID, oturum.OturumID, oturum.OgrenciID);
                // 2. oturum sil
                Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.OgrenciOturumTemizle(oturum.SinavID, oturum.OturumID, oturum.OgrenciID);
                // 3. enrollment sil
                Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.OgrenciEnrollmentTemizle(oturum.SinavID, oturum.OturumID, oturum.OgrenciID);
            }
            // 4. sınav-grup sil.
            db.OSinavGrup.RemoveRange(db.OSinavGrup.Where(g => g.FKSinavID == sinavid && g.FKDersGrupID == id).ToList());
            db.SaveChanges();
            return PartialView();
        }
        #endregion

        public ActionResult _pGrupPaylar(int id)
        {
            return PartialView(Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.SinavPayBilgi(id, Sabitler.KullaniciAdi, Sabitler.IPAdresi));
        }
    }
}