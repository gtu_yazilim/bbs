﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sabis.Bolum.Core.ABS.OGRENCI;

using Newtonsoft.Json;
using Sabis.Bolum.Bll.SOURCE.ABS.OGRENCI;
using System.Net.Http;
using System.Net.Mail;
using System.Threading.Tasks;
using Sabis.Bolum.WebUI.Areas.Ders.Models;
using Sabis.Abs.WebUI.Templates;

namespace Sabis.Bolum.WebUI.Areas.Ders.Controllers
{


    [AllowAnonymous]
    public class OgrenciController : Controller
    {


        // GET: Ders/OgrenciListesi
        public ActionResult Liste(int id)
        {
            if (Request.RequestContext.RouteData.Values["id"] == null)
            {
                return RedirectToAction("Index", "Home");
            }

            List<Core.dto.Ogrenci.OgrenciBilgiDto> ogrenciListesi = Sabis.Bolum.Bll.SOURCE.ABS.DERS.DersIslem.GetirDersOgrenciListesi(Sabitler.GetirDersDetay(id).DersGrupID);
            return View(ogrenciListesi);
        }

        public ActionResult AktifOgrenciListesi()
        {
           List<Sabis.Bolum.Core.ABS.OGRENCI.OgrenciBilgileri> ogrenciListesi = Sabis.Bolum.Bll.SOURCE.ABS.OGRENCI.OGRENCIMAIN.GetirAktifOgrenciListesi(Sabitler.KullaniciID);
            return View(ogrenciListesi);
        }
        /// <summary>
        /// Ders grup hoca nın değeri 'id' parametresinden gelmektedir.
        /// </summary>
        /// <param name="mail_icerik"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult topluPosta(string mail_icerik, int id)
        {
           string eposta = Sabitler.GetirPersonelBilgi(Sabitler.KullaniciAdi).KullaniciAdi;
            //int dersGrupHocaID = Convert.ToInt32(Request.RequestContext.RouteData.Values["id"]);
            int sonuc = 0;

            if (id == 0)
            {
                sonuc = (int)DersEnums.Mailsonuc.ders_hoca_id_bos_geldi;
            }
            else
            {
                List<Core.dto.Ogrenci.OgrenciBilgiDto> ogrenciListesi = Sabis.Bolum.Bll.SOURCE.ABS.DERS.DersIslem.GetirDersOgrenciListesi(Sabitler.GetirDersDetay(id).DersGrupID);


                foreach (var item in ogrenciListesi)
                {                        

                    SmtpClient client = new SmtpClient();
                    client.Port = 587;
                    client.Host = "smtp.office365.com";
                    client.EnableSsl = true;
                    //client.Timeout = 10000;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.UseDefaultCredentials = false;
                    client.Credentials = new System.Net.NetworkCredential("apollodestek@gtu.edu.tr", "Ap.!ollo658");

                    MailMessage mail = new MailMessage();

                    mail.From = new MailAddress("apollodestek@gtu.edu.tr", "APOLLO DUYURU");

                   // mail.To.Add("saidosman" + "@gtu.edu.tr");


                    mail.To.Add(item.KullaniciAdi+"@gtu.edu.tr");

                    mail.Subject = "APOLLO";
                    mail.ReplyToList.Add(new MailAddress(eposta+"@gtu.edu.tr", "reply-to"));
                    mail.IsBodyHtml = true;

                    string htmlBody;

                    htmlBody = "Sayın <b>" + item.Ad.ToUpper() + "&nbsp;" + item.Soyad.ToUpper() + "</b>," +
                    "&nbsp;" + "<br><br>" + mail_icerik + "<br><br>" +
                                "İyi Çalışmalar Dilerim.<br>";
                    mail.Body = htmlBody;
                    //mail.BodyEncoding = UTF8Encoding.UTF8;
                    mail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                    
                    try
                    {
                        client.Send(mail);
                        sonuc = (int)DersEnums.Mailsonuc.basarili_gonderildi;
                    }

                    catch (Exception)
                    {
                        sonuc = (int)DersEnums.Mailsonuc.islem_hata;

                    }
                }
            }

            return Json(sonuc, JsonRequestBehavior.AllowGet);



        }
        public ActionResult TumListe(int id)
        {
            var dersDetay = Sabitler.GetirDersDetay(id);
            return View(OGRENCIMAIN.GetirDersTumOgrenciListesi(dersDetay.DersPlanAnaID, Sabitler.KullaniciID, Sabitler.Yil, Sabitler.Donem));
        }

        [AllowAnonymous]
        public JsonResult GetirOgrenciListesi(int id)
        {
            return Json(Sabis.Bolum.Bll.SOURCE.ABS.DERS.DersIslem.GetirDersOgrenciListesi(Sabitler.GetirDersDetay(id).DersGrupID));
        }

        public ActionResult Profil(int id)
        {
            return View(OGRENCIMAIN.GetirOgrenciOzetBilgi(id));
        }

        public ActionResult _OgrenciHocaDersNotlari(int id)
        {
            return PartialView(OGRENCIMAIN.GetirOgrenciHocaNotListesi(Sabitler.KullaniciID, id));
        }
        //public ActionResult _pOzetProfil(int id)
        //{
        //    return PartialView(Sabis.Bolum.Bll.SOURCE.ABS.OGRENCI.OGRENCIMAIN.GetirOgrenciOzetBilgi(id));
        //}
        public ActionResult _pMesajGecmisi(int id)
        {
            return PartialView(OGRENCIMAIN.GetirDanismanMesajListesi(id, Sabitler.KullaniciID));
        }

        public JsonResult OgrenciMesajKaydet(Sabis.Bolum.Core.ABS.OGRENCI.DanismanMesajVM mesaj)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Sabitler.ApiAdresDanismanlik);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                var response = client.PostAsJsonAsync("api/Danismanlik/KaydetOgrenciDanismanMesaj", mesaj).Result;
                return Json("true");
            }
        }

        [HttpGet]
        public ActionResult NotDurum(int ogrenciID)
        {
            OICore.dto.transkript.Transkript transkript = new OICore.dto.transkript.Transkript();
            OICore.dto.transkript.TAyar ayar = new OICore.dto.transkript.TAyar();
            ayar.PasifPlanlariYukle = false;
            ayar.VukuatliMi = true;

            transkript = TranskriptClient.Client.Getir(ogrenciID, ayar);
            ViewData["OgrenciID"] = ogrenciID;
            return PartialView(transkript);
        }

        [HttpGet]
        public ActionResult _pSecilenDersler(int? ogrenciID)
        {
            //HttpClient client = new HttpClient();
            //HttpResponseMessage response = client.GetAsync(Sabitler.ApiAdresDanismanlik + "/api/Danismanlik/ListeleSecilenDersler?FKOgrenciID=" + ogrenciID + "&Yil=" + Sabitler.Yil + "&Donem=" + Sabitler.Donem).Result;
            //var SecilenDersAd = JsonConvert.DeserializeObject<IEnumerable<Sabis.Bolum.Core.ABS.DERS.DERSDETAY.SecilenDersVM>>(response.Content.ReadAsStringAsync().Result);

            return PartialView(OGRENCIMAIN.GetirOgrenciSecilenDersListesi(ogrenciID.Value, Sabitler.Yil, Sabitler.Donem));
        }

        [AllowAnonymous]
        public void ExceleAktar()
        {
            var ogrenciListesi = Sabis.Bolum.Bll.SOURCE.ABS.OGRENCI.OGRENCIMAIN.GetirAktifOgrenciListesi(Sabitler.KullaniciID);
            var list = Sabis.Bolum.Bll.SOURCE.ABS.OGRENCI.OGRENCIMAIN.OgrenciBilgileriExcelFormat(ogrenciListesi);
            ExcelExport.EpplusExportToExcelFromObjectList(list.ToList<object>(), "Kayıtlı Öğrenci Listesi", true);

        }

        public class DtoDersBilgisi
        {
            public DtoDersBilgisi()
            {
                OgrenciListesi = new List<DtoOgrenciListesi>();
            }
            public string DersinAdi { get; set; }
            public int OgretimTuru { get; set; }
            public string Kodu { get; set; }
            public int? Saati { get; set; }
            public int OgretimYili { get; set; }
            public string OgretimUyesi { get; set; }
            public int Donemi { get; set; }
            public List<DtoOgrenciListesi> OgrenciListesi { get; set; }
        }

        public class DtoOgrenciListesi
        {
            public string Numara { get; set; }
            public string Ad { get; set; }
            public string Soyad { get; set; }
        }
    }
}