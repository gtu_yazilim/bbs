﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Sabis.Bolum.Core.ABS.DERS.DERSDETAY;

namespace Sabis.Bolum.WebUI.Areas.Ders.Controllers
{
    public class DuyuruController : Controller
    {
        // GET: Ders/Duyuru
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult _pDuyuruListesi(int id)
        {
            return PartialView(Sabis.Bolum.Bll.SOURCE.ABS.DERS.DUYURUMAIN.GetirDuyuruListesi(Sabitler.GetirDersDetay(id).DersGrupID, Sabitler.KullaniciID));
        }

        [AllowAnonymous]
        [ValidateInput(false)]
        public void DuyuruKaydet(string msghead, string message, int id, bool tumGruplaraGonderilsinMi = false, bool tumDersGruplarinaGonderilsinMi = false)
        {
            DuyuruVM Duyuru = new DuyuruVM();
            Duyuru.FKDersGrupID = Sabitler.GetirDersDetay(id).DersGrupID;
            Duyuru.FKPersonelID = Sabitler.KullaniciID;
            Duyuru.EkleyenIP = Sabitler.IPAdresi;
            Duyuru.Konu = msghead;
            //if (!string.IsNullOrEmpty(Convert.ToString(Session["DuyuruDosya"])))
            //{
            //    message += "<br /><br />Eklenmiş dosyalar:<br />";
            //    List<string> dList = Session["DuyuruDosya"] as List<string>;
            //    foreach (var item in dList)
            //    {
            //        message += SDFSClient.FileClient.GetUrl(Convert.ToString(item)) + "<br />";
            //    }
            //}
            if (!string.IsNullOrEmpty(Convert.ToString(Session["DuyuruDosya"])))
            {
                message = message + "<br /><br />Eklenmiş dosyalar:<br /><a href='" + SDFSClient.FileClient.GetUrl(Convert.ToString(Session["DuyuruDosya"])) + "'>Dosya 1</a>";
                Session.Remove("DuyuruDosya");
            }
            Duyuru.Mesaj = message;
            Duyuru.Tarih = DateTime.Now;
            Duyuru.Yil = Sabitler.Yil;
            Duyuru.Donem = Sabitler.Donem;

            Sabis.Bolum.Bll.SOURCE.ABS.DERS.DUYURUMAIN.KaydetDuyuru(Duyuru, id, null, tumGruplaraGonderilsinMi, tumDersGruplarinaGonderilsinMi);
        }

        [AllowAnonymous]
        public JsonResult DosyaEkle(int DersGrupHocaID)
        {
            //Session["DuyuruDosya"] = "";
            var httpRequest = HttpContext.Request;

            foreach (var item in httpRequest.Files)
            {
                var file = Request.Files[item.ToString()];
                if (file.ContentLength > 5242880)
                {
                    return Json("Azami 5mb boyutunda dosya yüklenebilir.");
                }

                Session["DuyuruDosya"] = SDFSClient.FileClient.Upload(Sabitler.KullaniciAdi, file.ToByteArray(), file.FileName, true, "abis", "F4tpepP93Hhgd-oT");

                //if (string.IsNullOrEmpty(Convert.ToString(Session["DuyuruDosya"])))
                //{
                //    List<string> dosyaList = new List<string>();
                //    dosyaList.Add(SDFSClient.FileClient.Upload(Sabitler.KullaniciAdi, file.ToByteArray(), file.FileName, true, "abis", "F4tpepP93Hhgd-oT"));
                //    Session["DuyuruDosya"] = dosyaList;
                //}
                //else
                //{
                //    var list = Session["DuyuruDosya"] as List<string>;
                //    list.Add(SDFSClient.FileClient.Upload(Sabitler.KullaniciAdi, file.ToByteArray(), file.FileName, true, "abis", "F4tpepP93Hhgd-oT"));
                //    Session["DuyuruDosya"] = list;
                //}

            }
            return Json("Dosya yüklendi");
        }
    }
}