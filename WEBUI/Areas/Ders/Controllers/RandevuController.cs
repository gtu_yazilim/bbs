﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sabis.Bolum.WebUI.Areas.Ders.Controllers
{
    public class RandevuController : Controller
    {
        // GET: Ders/Randevu
        public ActionResult Liste(int id)
        {
            return View(Sabis.Bolum.Bll.SOURCE.ABS.DERS.RANDEVUMAIN.GetirRandevuListesi(Sabitler.GetirDersDetay(id).DersGrupID));
        }

        public ActionResult _pListeDetay(int id)
        {
            return PartialView(Sabis.Bolum.Bll.GenericFunctions.GetWebApiToList<Randevu.Core.Rapor>(Sabitler.ApiAdresRandevu, "api/Randevu/GetirRandevuRapor/" + id));
        }
        public ActionResult _pRandevuOlustur(int id)
        {
            return PartialView();
        }
        public ActionResult _pRandevuDuzenle(int id)
        {
            return PartialView(Sabis.Bolum.Bll.GenericFunctions.GetWebApi<Randevu.Core.Oturum>(Sabitler.ApiAdresRandevu, "api/Admin/Getir/" + id));
        }
        public JsonResult KaydetYeniRandevu(Randevu.Core.Oturum yeniOturum, List<Randevu.Core.Ayar> ayarList, int FKPayID, int FKDersGrupID)
        {
            Randevu.Core.Oturum resultRandevu = (Randevu.Core.Oturum)Sabis.Bolum.Bll.GenericFunctions.PostWebApiReturn<Randevu.Core.Oturum>(yeniOturum, Sabitler.ApiAdresRandevu, "api/Admin/Olustur");

            foreach (var item in ayarList)
            {
                item.FKRandevuOturumID = resultRandevu.ID;
            }

            var resultAyar = Sabis.Bolum.Bll.GenericFunctions.PostWebApiList<Randevu.Core.Ayar>(ayarList, Sabitler.ApiAdresRandevu, "api/Admin/AyarEkle");

            return Json(Sabis.Bolum.Bll.SOURCE.ABS.DERS.RANDEVUMAIN.EkleYeniRandevu(FKPayID, FKDersGrupID, resultRandevu.ID));
        }
        public JsonResult GuncelleRandevu(Randevu.Core.Oturum oturumBilgi)
        {
            return Json(Sabis.Bolum.Bll.GenericFunctions.PostWebApi<Randevu.Core.Oturum>(oturumBilgi, Sabitler.ApiAdresRandevu, "api/Admin/Duzenle"));
        }
        public JsonResult TarihAralikEkle(List<Randevu.Core.Ayar> yeniAyar)
        {
            var resultRandevu = Sabis.Bolum.Bll.GenericFunctions.PostWebApiList<Randevu.Core.Ayar>(yeniAyar, Sabitler.ApiAdresRandevu, "api/Admin/AyarEkle");
            return Json(resultRandevu);
        }
        public JsonResult TarihAralikGuncelle(Randevu.Core.Ayar ayarBilgi)
        {
            return Json(Sabis.Bolum.Bll.GenericFunctions.PostWebApi<Randevu.Core.Ayar>(ayarBilgi, Sabitler.ApiAdresRandevu, "api/Admin/AyarDuzenle/" + ayarBilgi.FKRandevuOturumID));
        }
        public JsonResult TarihAralikSil(int FKRandevuOturumID, int AyarID)
        {
            Randevu.Core.Ayar ayarSil = new Randevu.Core.Ayar();
            ayarSil.FKRandevuOturumID = FKRandevuOturumID;
            ayarSil.ID = AyarID;
            return Json(Sabis.Bolum.Bll.GenericFunctions.PostWebApi<Randevu.Core.Ayar>(ayarSil, Sabitler.ApiAdresRandevu, "api/Admin/AyarSil/" + FKRandevuOturumID + "?ayarID=" + AyarID));
        }
        public JsonResult RandevuAta(int id, int dersGrupHocaID)
        {
            //Bll.Ders.DersRepository dersBLL = new Bll.Ders.DersRepository();
            var dersDetay = Sabitler.GetirDersDetay(dersGrupHocaID);
            var randevuListe = (List<Randevu.Core.Rapor>)Sabis.Bolum.Bll.GenericFunctions.GetWebApiToList<Randevu.Core.Rapor>(Sabitler.ApiAdresRandevu, "api/Randevu/GetirRandevuRapor/" + id);

            List<Sabis.Bolum.Core.ABS.OGRENCI.OgrenciListesiVM> ogrenciListesi = Sabis.Bolum.Bll.SOURCE.ABS.OGRENCI.OGRENCIMAIN.GetirDersOgrenciListesi(Sabitler.GetirDersDetay(dersGrupHocaID).DersGrupID);

            //var listRandevuOlmayan = ogrenciListesi.Select(x => x.KullaniciID).Except().ToList();

            var randevuAlanlarIDList = randevuListe.Select(y => y.KullaniciID).OrderBy(x=> x).ToList();


            var ogrIDList = ogrenciListesi.Select(x=> x.KullaniciID).ToList();

            var listRandevuOlmayan = ogrIDList.Where(x => !randevuAlanlarIDList.Contains(x.Value)).Select(x=> x).ToList();

            var bosRandevuListe = (List<Randevu.Core.MusaitAralik>)Sabis.Bolum.Bll.GenericFunctions.GetWebApiToList<Randevu.Core.MusaitAralik>(Sabitler.ApiAdresRandevu, "api/Randevu/GetirBosRandevuTumu/" + id);
            bosRandevuListe = bosRandevuListe.Where(x => x.Adet > 0).ToList();
            if (bosRandevuListe.Count < listRandevuOlmayan.Count())
            {
                return Json("false");
            }
            for (int i = 0; i < listRandevuOlmayan.Count; i++)
            {
                KayitModel kayit = new KayitModel();
                var bosluk = bosRandevuListe[i];
                var kullaniciID = listRandevuOlmayan[i];
                kayit.RandevuID = id;
                kayit.KullaniciID = kullaniciID.Value;
                kayit.Tarih = bosluk.Baslangic;
                Sabis.Bolum.Bll.GenericFunctions.PostWebApi<KayitModel>(kayit, Sabitler.ApiAdresRandevu, "api/Randevu/KaydetOtomatik");
            }
            return Json("true");
        }

        class KayitModel
        {
            public int RandevuID { get; set; }
            public int KullaniciID { get; set; }
            public DateTime Tarih { get; set; }
        }
    }
}