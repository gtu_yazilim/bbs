﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sabis.Bolum.WebUI.Areas.Ders.Controllers
{
    public class DevamsizOgrenciController : Controller
    {
        public ActionResult Liste()
        {
            return View();
        }

        public JsonResult GetirOgrenciListesi(int id)
        {
            return Json(Sabis.Bolum.Bll.SOURCE.ABS.DERS.YOKLAMAMAIN.GetirDevamsizOgrenciListesi(Sabitler.GetirDersDetay(id).DersGrupID).OrderBy(x => x.Numara).ToList(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult DevamsizOgrenciKaydet(List<Sabis.Bolum.Core.ABS.NOT.NotKayitVM> liste)
        {
            try
            {
                Sabis.Bolum.Bll.SOURCE.ABS.NOT.NOTMAIN.NotKaydet(liste, Sabitler.IPAdresi, Sabitler.KullaniciAdi);
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(false);
            }
        }
    }
}