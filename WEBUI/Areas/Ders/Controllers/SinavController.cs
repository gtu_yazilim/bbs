﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Sabis.Bolum.Bll.SOURCE.ABS.DERS;
using Sabis.Bolum.Core.ABS.DERS.SINAV;
using Sabis.Bolum.WebUI.Helpers;
using System.Web.Hosting;
using System.IO;
using System.Configuration;
using Sabis.Bolum.Core.ABS.DERS.OSINAV;
using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Bolum.Core.ABS.DERS;
using Sabis.Bolum.Bll.SOURCE.ABS.GENEL;
using Sabis.Bolum.WebUI.Areas.Ders.Models;
using Sabis.Bolum.WebUI.Models;
using Newtonsoft.Json;
using System.Diagnostics;
namespace Sabis.Bolum.WebUI.Areas.Ders.Controllers
{
    [AllowAnonymous]
    public class SinavController : Controller
    {
        public ActionResult Liste()
        {
            return View();
        }
        // GET: Ders/Sinav
        public ActionResult SinavSorulari()
        {
            return View();
        }
        public ActionResult _SinavDetay()
        {
            return PartialView();
        }

        public ActionResult _SoruIslem(int id, int P)
        {
            try
            {
                var sinavbilgi = SINAVMAIN.SinavBilgi(id, P);

                if (sinavbilgi != null)
                {
                    var paysorular = SINAVMAIN.GetirPaySinavSoruListesi2(sinavbilgi.SinavID, id, P, Sabitler.Yil, Sabitler.Donem);
                    var mazeretsorular = SINAVMAIN.GetirPayMazeretSinavSoruListesi(sinavbilgi.SinavID, id, P, Sabitler.Yil, Sabitler.Donem);
                    return View(Tuple.Create(sinavbilgi, paysorular, mazeretsorular));
                }
                return View();
            }
            catch (Exception error)
            {
                Sabitler.LogException(error);
                return View();
            }
        }
        [HttpPost]
        public ActionResult _SoruIslem([Bind(Prefix = "Item1")]OSinavVM Model1, [Bind(Prefix = "Item2")]List<PaylarOlcmeVM> Model2)
        {
            if (Model1.Baslangic >= Model1.Bitis)
            {
                ModelState.AddModelError("Sınav Tarihi", "Sınav Başlangıç ve Bitiş Tarihlerini Düzeltiniz.");
            }
            if (Model1.Baslangic >= Model1.MazeretBaslangic)
            {
                ModelState.AddModelError("Sınav Tarihi", "Mazeret Sınavının Başlangıç Tarihi Sınav Tarihinden Önce Olamaz.");
            }
            if (Model1.Bitis >= Model1.MazeretBitis)
            {
                ModelState.AddModelError("Sınav Tarihi", "Mazeret Sınavının Başlangıç Tarihi Sınav Bitiş Tarihinden Sonra Olmalıdır.");
            }
            if (Model1.MazeretBaslangic <= Model1.Bitis)
            {
                ModelState.AddModelError("Sınav Tarihi", "Mazeret Sınavının Başlangıç Tarihi Sınav Bitiş Tarihinden Sonra Olmalıdır.");
            }
            if (ModelState.IsValid)
            {
                SINAVMAIN.SinavIslemSinavKaydet(Model1);
            }
            var sinavbilgi = SINAVMAIN.SinavBilgi(Model1.HocaID, Model1.PayID);
            var paysorular = SINAVMAIN.GetirPaySinavSoruListesi2(sinavbilgi.SinavID, Model1.HocaID, Model1.PayID, Sabitler.Yil, Sabitler.Donem);
            var mazeretsorular = SINAVMAIN.GetirPayMazeretSinavSoruListesi(sinavbilgi.SinavID, Model1.HocaID, Model1.PayID, Sabitler.Yil, Sabitler.Donem);
            return View(Tuple.Create(sinavbilgi, paysorular, mazeretsorular));
        }
        public ActionResult _pSinavDokumanlari(int FKPaylarID, int FKDersGrupID)
        {
            try
            {
                ViewBag.PayID = FKPaylarID;
                ViewBag.DersGrupID = FKDersGrupID;
                return PartialView(SINAVMAIN.GetirSinavDokumanListesi(FKPaylarID));
            }
            catch (Exception error)
            {
                Sabitler.LogException(error);
                return PartialView();
            }
        }

        public ActionResult SoruIslem(int id, int P)
        {
            try
            {
                var sinavbilgi = SINAVMAIN.SinavBilgi(id, P);
                
                if (sinavbilgi != null)
                {
                    var paysorular = SINAVMAIN.GetirPaySinavSoruListesi2(sinavbilgi.SinavID, id, P, Sabitler.Yil, Sabitler.Donem);
                    var mazeretsorular = SINAVMAIN.GetirPayMazeretSinavSoruListesi(sinavbilgi.SinavID, id, P, Sabitler.Yil, Sabitler.Donem);
                    return View(Tuple.Create(sinavbilgi, paysorular, mazeretsorular));
                }
                return View();
            }
            catch (Exception error)
            {
                Sabitler.LogException(error);
                return View();
            }
        }

        [HttpPost]
        public ActionResult SoruIslem([Bind(Prefix = "Item1")]OSinavVM Model1, [Bind(Prefix = "Item2")]List<PaylarOlcmeVM> Model2)
        {
            if (Model1.Baslangic >= Model1.Bitis )
            {
                ModelState.AddModelError("Sınav Tarihi", "Sınav Başlangıç ve Bitiş Tarihlerini Düzeltiniz.");
            }
            if (Model1.Baslangic >= Model1.MazeretBaslangic)
            {
                ModelState.AddModelError("Sınav Tarihi", "Mazeret Sınavının Başlangıç Tarihi Sınav Tarihinden Önce Olamaz.");
            }
            if (Model1.Bitis >= Model1.MazeretBitis)
            {
                ModelState.AddModelError("Sınav Tarihi", "Mazeret Sınavının Başlangıç Tarihi Sınav Bitiş Tarihinden Sonra Olmalıdır.");
            }
            if (Model1.MazeretBaslangic <= Model1.Bitis)
            {
                ModelState.AddModelError("Sınav Tarihi", "Mazeret Sınavının Başlangıç Tarihi Sınav Bitiş Tarihinden Sonra Olmalıdır.");
            }
            if (ModelState.IsValid)
            {
                SINAVMAIN.SinavIslemSinavKaydet(Model1);
            }
            var sinavbilgi = SINAVMAIN.SinavBilgi(Model1.HocaID, Model1.PayID);
            var paysorular = SINAVMAIN.GetirPaySinavSoruListesi2(sinavbilgi.SinavID, Model1.HocaID, Model1.PayID, Sabitler.Yil, Sabitler.Donem);
            var mazeretsorular = SINAVMAIN.GetirPayMazeretSinavSoruListesi(sinavbilgi.SinavID, Model1.HocaID, Model1.PayID, Sabitler.Yil, Sabitler.Donem);
            return View(Tuple.Create(sinavbilgi, paysorular, mazeretsorular));
        }

        public ActionResult SinavOnizleme(int sinavID, bool mazeret)
        {
            return View(SINAVMAIN.SinavOnizleme(sinavID,mazeret));
        }
        public ActionResult SinavOturumlari(int id, int SinavID, bool k, int? Value)
        {
            var model = new SinavOturumVM();
            model.PayID = SINAVMAIN.SinavBilgi_Pay(SinavID);
            model.grupHocaID = id;
            model.SinavID = SinavID;
            model.SinavAd = SINAVMAIN.SinavAd(SinavID);
            model.GrupID = Value;
            model.koordinator = k;
            var grupid = Sabitler.GetirDersDetay(id).DersGrupID;
            model.Gruplar = SINAVMAIN.Gruplar(id, Sabitler.Yil, Sabitler.Donem).Select(x => new SelectListItem() { Text = x.text, Value = x.value.ToString(), Selected = x.value ==  grupid});
            if (k)
            {            
                if (Value == null)
                {
                    
                    model.Oturumlar = SINAVMAIN.Oturumlar(SinavID, grupid, null);
                }
                else
                {
                    model.Oturumlar = SINAVMAIN.Oturumlar(SinavID, Value,null);
                }       
            }
            else
            {
                model.Oturumlar = SINAVMAIN.Oturumlar(SinavID, grupid, null);
            }
            return View(model);
        }
        [HttpPost]
        public ActionResult SinavOturumlari(SinavOturumVM model)
        {
            model.grupHocaID = model.id;
            model.SinavID = model.SinavID;
            model.GrupID = model.Value;
            model.koordinator = model.k;
            if (model.k)
            {                       
                model.Gruplar = SINAVMAIN.Gruplar(model.id, Sabitler.Yil, Sabitler.Donem).Select(x => new SelectListItem() { Text = x.text, Value = x.value.ToString() });
                if (model.Value == null)
                {
                    //model.Oturumlar = SINAVMAIN.Oturumlar(model.SinavID, Convert.ToInt32(model.Gruplar.FirstOrDefault().Value),null);
                    model.Oturumlar = SINAVMAIN.Oturumlar(model.SinavID, model.Value, null);
                }
                else
                {
                    model.Oturumlar = SINAVMAIN.Oturumlar(model.SinavID, model.Value,model.OgrenciNo);
                }
            }
            else
            {
                model.Gruplar = SINAVMAIN.Gruplar(model.id, Sabitler.Yil, Sabitler.Donem).Select(x => new SelectListItem() { Text = x.text, Value = x.value.ToString() });
                model.Oturumlar = SINAVMAIN.Oturumlar(model.SinavID, model.Value, model.OgrenciNo);
            }
            return View(model);

        }

        public ActionResult TumSorunlar(int id, int SinavID)
        {
            var model = new SinavOturumVM();
            model.grupHocaID = id;
            model.SinavID = SinavID;
            model.SinavAd = SINAVMAIN.SinavAd(SinavID);
            model.Oturumlar = SINAVMAIN.TumSorunBildirenOturumlar(SinavID);
            return View(model);
        }
        public ActionResult SorunBildirimi(int bid)
        {
            return View(SINAVMAIN.SorunBildirimi(bid));
        }
        public JsonResult BildirimKaydet(int ID, string Cevap)
        {
            return Json(SINAVMAIN.BildirimKaydet(ID,Cevap));
        }

        public ActionResult OturumDetay(int eid)
        {
            return View(SINAVMAIN.OturumDetay(eid));
        }
        public JsonResult OturumTemizle(int EID, bool mazeret)
        {
            if (EID != 0)
            {
                return Json(SINAVMAIN.OturumTemizle(EID,mazeret));
            }
            return Json(false);
        }

        public JsonResult SinavNotYenidenHesapla(int SinavID)
        {
            return Json(SINAVMAIN.SinavNotYenidenHesapla(SinavID));
        }
        public ActionResult TamamlanmayanOturumBitir()
        {
            SINAVMAIN.TamamlanmayanOturumBitir();
            return View();
        }
        public ActionResult TamamlanmayanOturumBitirMazeret()
        {
            SINAVMAIN.TamamlanmayanOturumBitirMazeret();
            return View();
        }
        public JsonResult MazeretHakVer(int sinavID, int ogrenciID, int EID, int yazilmaid)
        {
            return Json(SINAVMAIN.MazeretHakVer(sinavID, ogrenciID,EID,yazilmaid,Sabitler.Yil,Sabitler.Donem));
        }
        public JsonResult MazeretHakSil(int sinavID, int ogrenciID, int EID, int yazilmaid)
        {
            return Json(SINAVMAIN.MazeretHakSil(sinavID, ogrenciID, EID, yazilmaid, Sabitler.Yil, Sabitler.Donem));
        }
        public JsonResult MazeretHakVer2(int sinavID, int ogrenciID, int FKDersGrupID)
        {
            return Json(SINAVMAIN.MazeretHakVer2(sinavID, ogrenciID, FKDersGrupID, Sabitler.Yil, Sabitler.Donem));
        }
        public JsonResult SinavYayinla(int sinavid)
        {
            return Json(SINAVMAIN.SinavYayinla(sinavid));
        }
        public JsonResult SinavYayindanKaldir(int sinavid)
        {
            return Json(SINAVMAIN.SinavYayindanKaldir(sinavid));
        }

        public ActionResult YenidenDegerlendir(int id, int payID)
        {
            var FKDersGrupID = DERSMAIN.GetirDersDetay(id).DersGrupID;
            SINAVMAIN.YenidenDegerlendir(payID, FKDersGrupID, Request.UserHostAddress.ToString());

            return RedirectToAction("Grup", "Detay", new { id = id});

        }
        public ActionResult YenidenDegerlendir_Tumu(int id)
        {
            using (UYSv2Entities db = new UYSv2Entities())
            {
                var FKDersPlanAnaID = db.OGRDersGrupHoca.FirstOrDefault(x => x.ID == id).OGRDersGrup.FKDersPlanAnaID;
                var liste = db.OSinavGrup.Where(x => x.FKDersPlanAnaID == FKDersPlanAnaID).OrderBy(x => x.FKDersGrupID).ToList();
                foreach (var item in liste)
                {
                    SINAVMAIN.YenidenDegerlendir(item.PayID, item.FKDersGrupID, "10.9.16.87");
                }
                return RedirectToAction("Grup", "Detay", new { id = id });
            }
                
        }
        public JsonResult GetirOnlineSinavTuru(int hocaid, int payid)
        {
            return Json(SINAVMAIN.GetirOnlineSinavTuru(hocaid,payid));
        }
        public ActionResult _pGrupPaylar(int id)
        {
            return PartialView(PAYMAIN.SinavGetirPayBilgileriv2(id, Sabitler.KullaniciAdi, Sabitler.IPAdresi));
        }

        public ActionResult _pSinavSoruListesi(int id, int P, string dil = "tr") // FKDersGrupHocaID, FKPaylarID
        {
            int enumDil = Convert.ToInt32(dil.Replace("tr", "1").Replace("en", "2"));
            return PartialView(SINAVMAIN.GetirPaySinavSoruListesi(id, P, Sabitler.Yil, Sabitler.Donem, enumDil));
        }

        public JsonResult GetirSinavTuru(int id, int P)
        {
            var list = SINAVMAIN.GetirPaySinavSoruListesi(id, P, Sabitler.Yil, Sabitler.Donem, 1);

            if (list.Count != 0)
            {
                int? enumSinavTuru = SINAVMAIN.GetirPaySinavSoruListesi(id, P, Sabitler.Yil, Sabitler.Donem, 1).FirstOrDefault().EnumSinavTuru;
                if (enumSinavTuru == null)
                {
                    enumSinavTuru = 0;
                }
                return Json(Sabis.Enum.Utils.GetEnumDescription((Sabis.Bolum.Core.ENUM.ABS.EnumSinavTuru)enumSinavTuru));
            }
            else
            {
                return Json("");
            }
        }

        public JsonResult GetirSinavID(int FKPaylarID, int FKDersGrupID, int FKDersPlanID, int FKDersPlanAnaID)
        {
            return Json(SINAVMAIN.GetirSinavID(FKPaylarID, FKDersGrupID, FKDersPlanID, FKDersPlanAnaID));
        }

        //public ActionResult PaySinavSoruListesiJson(int id) //id = FKPaylarID
        //{
        //    return Json(SINAVMAIN.GetirPaySinavSoruListesi(id, Sabitler.Yil));
        //}
        public JsonResult SinavOlustur(int soruSayisi, int FKPaylarID, int FKKoordinatorID, int FKDersGrupID, int dersGrupHocaID, int sinavTuru, int? klasikSoruSayisi, int OnlineAktiviteTip)
        {
            try
            {
                SINAVMAIN.SinavOlustur(soruSayisi, FKPaylarID, FKKoordinatorID, Sabitler.Yil, Sabitler.Donem, FKDersGrupID, sinavTuru, Sabitler.GetirDersDetay(dersGrupHocaID).FakulteID, Sabitler.KullaniciAdi, Sabitler.IPAdresi, klasikSoruSayisi, OnlineAktiviteTip);
                return Json("true");
            }
            catch (Exception error)
            {
                Sabitler.LogException(error);
                return Json("false");
            }
        }
        public ActionResult OSoruEkle(int id, int SinavID, int SoruNo,int olcmeid, int t, bool m)
        {
            var sinav = Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.SinavGetir(SinavID);
            if (sinav != null)
            {
                var payid = SINAVMAIN.OlcmedenPayGetir(olcmeid);

                var soru = new OSinavSoruEkle();
                soru.PaylarOlcmeID = olcmeid;
                soru.SinavID = SinavID;
                soru.HocaID = id;
                //soru.SoruNo = Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.SoruNoBul(SinavID);
                soru.SoruNo = SoruNo;
                soru.SinavAd = sinav.Ad + " - " + soru.SoruNo + ". Soru";
                //soru.PaylarOlcmeID = PaylarOlcmeID;
                //soru.PayOlcmeSoruNo = PayOlcmeSoruNo;
                soru.PayID = payid;
                soru.GrupHocaID = id;
                soru.SinavTur = t;
                soru.MazeretSoru = m;
                return View(soru);
            }
            return View();
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult OSoruEkle(OSinavSoruEkle model)
        {
            if (model.DogruCevap == 0)
            {
                ModelState.AddModelError("Doğru Seçenek", "Doğru cevabı işaretlemeniz gereklidir.");
            }
            if (ModelState.IsValid)
            {
                SINAVMAIN.OSoruEkle(model, Sabitler.KullaniciAdi, Sabitler.KullaniciID);

                //return RedirectToAction("SoruIslem","Sinav", new { id = model.GrupHocaID, P = model.PayID});
                return RedirectToAction("Sinav", "Detay", new { id = model.GrupHocaID, P = model.PayID, tab = 5});
            }
            return View(model);
        }
        public ActionResult OSoruDuzenle(int id, int SoruID, int PayID)
        {
            return View(SINAVMAIN.OnlineSinavSoruGetir(SoruID,PayID,id));
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult OSoruDuzenle(OSinavSoruEkle model)
        {
            if (model.DogruCevap == 0)
            {
                ModelState.AddModelError("Doğru Seçenek", "Doğru cevabı işaretlemeniz gereklidir.");
            }
            if (ModelState.IsValid)
            {
                SINAVMAIN.OSoruDuzenle(model, Sabitler.KullaniciAdi, Sabitler.KullaniciID);
                return RedirectToAction("SoruIslem", "Sinav", new { id = model.GrupHocaID, P = model.PayID });
            }
            return View(model);
        }
        [HttpGet]
        public ActionResult OSoruSil(int id, int SoruID, int PayID)
        {
            ViewBag.HocaID = id;
            return View(SINAVMAIN.OnlineSinavSoruGetir(SoruID, PayID, id));
        }
        [HttpPost]
        public ActionResult OSoruSil(OSinavSoruEkle model)
        {
            SINAVMAIN.OSoruSil(model.SoruID);
            return RedirectToAction("SoruIslem", "Sinav", new { id = model.GrupHocaID, P = model.PayID });
        }

        public JsonResult ImageUpload()
        {
            var httpRequest = HttpContext.Request;
            DokumanVM yeniDokuman = new DokumanVM();
            foreach (var item in httpRequest.Files)
            {
                Dictionary<string, string> fileAttributes = new Dictionary<string, string>();
                var file = Request.Files[item.ToString()];

                yeniDokuman.DosyaAdi = Path.GetFileNameWithoutExtension(file.FileName);

                yeniDokuman.DosyaTuru = Path.GetExtension(file.FileName);

                yeniDokuman.DosyaBoyutu = file.ContentLength;

                if (!Sabitler.DosyaUzantiKontrol(yeniDokuman.DosyaTuru))
                {
                    continue;
                }

                if (yeniDokuman.DosyaTuru == ".mp4" || yeniDokuman.DosyaTuru == ".flv" || yeniDokuman.DosyaTuru == ".mov")
                {
                    yeniDokuman.DosyaKey = SDFSClient.FileClient.UploadPublic(Sabitler.KullaniciAdi, file.ToByteArray(), file.FileName, "abis", "F4tpepP93Hhgd-oT", fileAttributes);
                }
                else
                {
                    yeniDokuman.DosyaKey = SDFSClient.FileClient.Upload(Sabitler.KullaniciAdi, file.ToByteArray(), file.FileName, false, "abis", "F4tpepP93Hhgd-oT", fileAttributes);
                }
                yeniDokuman.DosyaBoyutu = file.ContentLength;
                yeniDokuman.DosyaAdres = SDFSClient.FileClient.GetUrl(yeniDokuman.DosyaKey);

            }
            return Json(yeniDokuman);
        }
        #region Online Sınav
        public JsonResult OnlineSinavOlustur(OnlineSinavModel model)
        {
            return Json(SINAVMAIN.OnlineSinavOlustur(model.Ad, model.Aciklama, Convert.ToDateTime(model.Baslangic), Convert.ToDateTime(model.Bitis), Convert.ToInt32(model.Sure), Convert.ToInt32(model.SoruSayisi), true, Sabitler.Yil, Sabitler.Donem, Convert.ToInt32(model.FKPaylarID), Convert.ToInt32(model.FKKoordinatorID), Convert.ToInt32(model.FKDersGrupID), Convert.ToInt32(model.dersGrupHocaID), Convert.ToInt32(model.sinavTuru), Sabitler.KullaniciAdi, Sabitler.KullaniciID,Convert.ToBoolean(model.Mazeret),Convert.ToDateTime(model.MazeretBaslangic),Convert.ToDateTime(model.MazeretBitis),Convert.ToInt32(model.MazeretSure),Convert.ToInt32(model.EklemeTuru)));
        }
        //public JsonResult OnlineSinavOlustur(string Ad, string Aciklama, string Baslangic, string Bitis, string Sure, string SoruSayisi, string Yayinlandi, string FKPaylarID, string FKKoordinatorID, string FKDersGrupID, string dersGrupHocaID, string sinavTuru, string Ekleyen, string EkleyenID, string Mazeret, string MazeretBaslangic, string MazeretBitis, string MazeretSure)
        //{
        //    return null;
        //    //return Json(SINAVMAIN.OnlineSinavOlustur(Ad, Aciklama, Convert.ToDateTime(Baslangic), Convert.ToDateTime(Bitis), Convert.ToInt32(Sure), Convert.ToInt32(SoruSayisi), true, Sabitler.Yil, Sabitler.Donem, Convert.ToInt32(FKPaylarID), Convert.ToInt32(FKKoordinatorID), Convert.ToInt32(FKDersGrupID), Convert.ToInt32(dersGrupHocaID), Convert.ToInt32(sinavTuru), Sabitler.KullaniciAdi, Sabitler.KullaniciID));
        //}
        public JsonResult OnlineTest(OnlineSinavModel model)
        {
            return Json(null);
        }
        public JsonResult OnlineSinavDuzenle(int SinavID, string Baslangic, string Bitis, int Sure, int FKPaylarID, int FKKoordinatorID, int FKDersGrupID, int dersGrupHocaID, bool DegerlendirmeGruplarinaEkle)
        {
            return Json(SINAVMAIN.OnlineSinavDuzenle(SinavID,Baslangic,Bitis,Sure, FKPaylarID, FKKoordinatorID, FKDersGrupID,dersGrupHocaID, DegerlendirmeGruplarinaEkle,Sabitler.Yil,Sabitler.Donem));
        }
        public JsonResult HocaGrupYetki(int FKPaylarID, int FKKoordinatorID, int FKDersGrupID, int dersGrupHocaID)
        {
            var model = SINAVMAIN.HocaGrupYetki(FKPaylarID, FKKoordinatorID, FKDersGrupID, dersGrupHocaID,Sabitler.Yil,Sabitler.Donem);
            var degerlendirmegrubusayisi = 0;
            var koordinatorgrubusayisi = 0;
            if (model.GruplarDegerlendirme != null)
            {
                degerlendirmegrubusayisi = model.GruplarDegerlendirme.Count();
            }
            if (model.GruplarKoordinator != null)
            {
                koordinatorgrubusayisi = model.GruplarKoordinator.Count();
            }
            return Json(new { dgs = degerlendirmegrubusayisi, kgs = koordinatorgrubusayisi, degerlendirmegruplari = model.GruplarDegerlendirme, tumgruplar = model.GruplarKoordinator }, JsonRequestBehavior.AllowGet);
            //return Json(SINAVMAIN.HocaGrupYetki(FKPaylarID, FKKoordinatorID, FKDersGrupID, dersGrupHocaID));
        }
        public ActionResult HocaDegerlendirmeGrup(int FKPaylarID, int FKKoordinatorID, int FKDersGrupID, int dersGrupHocaID)
        {
            var model = SINAVMAIN.HocaGrupYetki(FKPaylarID, FKKoordinatorID, FKDersGrupID, dersGrupHocaID,Sabitler.Yil,Sabitler.Donem);
            var degerlendirmegrubusayisi = 0;
            var koordinatorgrubusayisi = 0;
            if (model.GruplarDegerlendirme != null)
            {
                degerlendirmegrubusayisi = model.GruplarDegerlendirme.Count();
            }
            if (model.GruplarKoordinator != null)
            {
                koordinatorgrubusayisi = model.GruplarKoordinator.Count();
            }
            return View(model);
        }
        
        public ActionResult KoordinatorTumGruplar(int FKPaylarID, int FKKoordinatorID, int FKDersGrupID, int dersGrupHocaID)
        {
            var model = SINAVMAIN.HocaGrupYetki(FKPaylarID, FKKoordinatorID, FKDersGrupID, dersGrupHocaID,Sabitler.Yil,Sabitler.Donem);
            var degerlendirmegrubusayisi = 0;
            var koordinatorgrubusayisi = 0;
            if (model.GruplarDegerlendirme != null)
            {
                degerlendirmegrubusayisi = model.GruplarDegerlendirme.Count();
            }
            if (model.GruplarKoordinator != null)
            {
                koordinatorgrubusayisi = model.GruplarKoordinator.Count();
            }
            return View(model);
        }

        public JsonResult OnlineSinavGetir(int FKDersGrupHocaID, int PayID)
        {
            var model = SINAVMAIN.OnlineSinavGetir(FKDersGrupHocaID, PayID);
            if (model !=null)
            {
                var SinavID = model.SinavID;
                var Ad = model.Ad;
                var EklemeTipi = model.EklemeTuru;
                var Baslangic = model.Baslangic.ToString();
                var Bitis = model.Bitis.ToString();
                var Sure = model.Sure;
                var SoruSayisi = model.SoruSayisi;
                var Yayinlandi = model.Yayinlandi;
                var payID = model.PayID;
                var Mazeret = model.Mazeret;
                var MazeretBaslangic = model.MazeretBaslangic.ToString();
                var MazeretBitis = model.MazeretBitis.ToString();
                var MazeretSure = model.MazeretSure;

                return Json(new { sinavid = SinavID, ad = Ad, baslangic = Baslangic, bitis = Bitis, sure = Sure, sorusayisi = SoruSayisi, yayinlandi = Yayinlandi, payid = payID, mazeret = Mazeret, mazeretbaslangic = MazeretBaslangic, mazeretbitis = MazeretBitis ,mazeretsure = MazeretSure, eklemetipi = EklemeTipi }, JsonRequestBehavior.AllowGet);
            }
            return Json(null);
            
        }
        public ActionResult _OnlineSinavSoruEkle(int id, int sinavid, int PaylarOlcmeID, int PayOlcmeSoruNo,int payID, int gHocaID)
        {
            var sinav = Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.SinavGetir(sinavid);
            if (sinav != null)
            {
                var soru = new OSinavSoruEkle();
                soru.SinavID = sinav.ID;
                soru.HocaID = id;
                soru.SoruNo = Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.SoruNoBul(sinavid);
                soru.SinavAd = sinav.Ad + " - " + soru.SoruNo + ". Soru";
                soru.PaylarOlcmeID = PaylarOlcmeID;
                soru.PayOlcmeSoruNo = PayOlcmeSoruNo;
                soru.PayID = payID;
                soru.GrupHocaID = gHocaID;
                return View(soru);
            }
            return View();
        }

        [HttpGet]
        public ActionResult OnlineSinavSoruEkle(int id,int sinavid, int PaylarOlcmeID, int PayOlcmeSoruNo, int payID, int gHocaID)
        {
            var sinav = Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.SinavGetir(sinavid);
            if (sinav != null)
            {
                var soru = new OSinavSoruEkle();
                soru.SinavID = sinav.ID;
                soru.HocaID = id;
                soru.SoruNo = Sabis.Bolum.Bll.SOURCE.ABS.DERS.OSINAV.SoruNoBul(sinavid);
                soru.SinavAd = sinav.Ad + " - " + soru.SoruNo + ". Soru";
                soru.PaylarOlcmeID = PaylarOlcmeID;
                soru.PayOlcmeSoruNo = PayOlcmeSoruNo;
                soru.PayID = payID;
                soru.GrupHocaID = gHocaID;
                return View(soru);
            }
            return View();
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult _OnlineSinavSoruEkle(OSinavSoruEkle model)
        {
            if (model.DogruCevap == 0)
            {
                ModelState.AddModelError("Doğru Seçenek","Doğru cevabı işaretlemeniz gereklidir.");
            }
            if (ModelState.IsValid)
            {
                try
                {
                    SINAVMAIN.OnlineSinavSoruEkle(model, Sabitler.KullaniciAdi, Sabitler.KullaniciID);
                    return RedirectToAction("SinavSorulari", "Sinav", new { @id = model.GrupHocaID ,@P= model.PayID });
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            return View(model);
        }
        [HttpGet]
        public ActionResult _OnlineSinavSoruDuzenle(int id,int soruid, int payID, int gHocaID)
        {
            return View(SINAVMAIN.OnlineSinavSoruGetir(soruid, payID,gHocaID));
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult _OnlineSinavSoruDuzenle(OSinavSoruEkle model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    SINAVMAIN.OnlineSinavSoruDuzenle(model, Sabitler.KullaniciAdi, Sabitler.KullaniciID);
                    return RedirectToAction("SinavSorulari", "Sinav", new { @id = model.GrupHocaID, @P = model.PayID });
                }
                catch (Exception)
                {

                    throw;
                }
            }
            return View();
        }
        public JsonResult YeniOlcmeSoruEkle(int PayID, int GrupID, int KoordinatorID)
        {
            return Json(SINAVMAIN.YeniOlcmeSoruEkle(PayID, GrupID, KoordinatorID));
        }
        
        [HttpGet]
        public ActionResult _OnlineSinavSoruSil(int SoruID)
        {
            var model = SINAVMAIN.OnlineSinavSoruSil(SoruID);
            return View(model);
        }
        public ActionResult _OnlineSinavSoruSilOnay(int SoruID)
        {  
            return View(SINAVMAIN.OnlineSinavSoruSilOnay(SoruID));
        }

        //public ActionResult OnlineSinavRapor(int id, int P)
        //{
        //    var sonuc = SINAVMAIN.OgrenciSinavRapor(id, P);
        //    return View(sonuc);
        //}

        public JsonResult SoruResimSil(int id)
        {
            return Json(SINAVMAIN.SoruResimSil(id));
        }

        public JsonResult SoruCevapResimSil(int soruid,int cevapno)
        {
            return Json(SINAVMAIN.SoruCevapResimSil(soruid,cevapno));
        }
        public JsonResult SinavHakVer(int id)
        {
            return Json(SINAVMAIN.SinavHakVer(id));
        }
        #endregion
        public JsonResult SinavSil(int FKPaylarID, int FKDersGrupID, int FKKoordinatorID, int dersGrupHocaID)
        {
            return Json(SINAVMAIN.SinavSil(FKPaylarID, FKDersGrupID, FKKoordinatorID, Sabitler.Yil, Sabitler.Donem, Sabitler.KullaniciAdi, Sabitler.IPAdresi, Sabitler.GetirDersDetay(dersGrupHocaID).FakulteID));
        }

        public JsonResult GetirProgramCiktilari(int bolumID, int Yil, int EnumDilID)
        {
            return Json(Sabis.Bolum.Bll.SOURCE.EBS.BIRIM.BOLUMMAIN.GetirBolumProgramCiktilari(bolumID, Yil, EnumDilID));
        }

        public JsonResult GetirOgrenmeCiktilari(int dersPlanAnaID, int yil, int EnumDilID)
        {
            return Json(Sabis.Bolum.Bll.SOURCE.EBS.DERS.DERSMAIN.GetirDersOgrenmeCiktilari(dersPlanAnaID, yil, EnumDilID));
        }

        public JsonResult KaydetProgramCiktilari(List<int> programCiktilari, int FKPaylarOlcmeID)
        {
            return Json(SINAVMAIN.KaydetProgramCiktilari(programCiktilari, FKPaylarOlcmeID));
        }

        public JsonResult KaydetOgrenmeCiktilari(List<int> ogrenmeCiktilari, int FKPaylarOlcmeID)
        {
            return Json(SINAVMAIN.KaydetOgrenmeCiktilari(ogrenmeCiktilari, FKPaylarOlcmeID));
        }

        public JsonResult SilProgramCikti(int ID)
        {
            return Json(SINAVMAIN.ProgramCiktiSil(ID));
        }

        public JsonResult SilOgrenmeCikti(int ID)
        {
            return Json(SINAVMAIN.OgrenmeCiktiSil(ID));
        }

        public JsonResult SoruPuanGuncelle(List<Sabis.Bolum.Core.ABS.DERS.SINAV.PaylarOlcmeVM> puanList)
        {
            return Json(SINAVMAIN.SoruPuanGuncelle(puanList));
        }

        #region SINAV NOT GİRİŞ İŞLEMLERİ
        public ActionResult NotGiris()
        {
            return View();
        }
        public JsonResult OgrenciBilgileri(int listPayID, int id)
        {
            try
            {
                var jsonResult = Json(SINAVMAIN.ListeleSinavOgrenciNotlari(Sabitler.GetirDersDetay(id).DersGrupID, listPayID), JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = Int32.MaxValue;
                return jsonResult;
            }
            catch (Exception error)
            {
                Sabitler.LogException(error);
                return Json("false");
            }
        }
        public JsonResult SinavNotKaydet(List<SinavNotGirisSoruListeVM> notListe, int id, List<Sabis.Bolum.Core.ABS.DERS.ODEV.OdevAciklamaVM> aciklamaListe)
        {
            //Sabis.Bolum.Core.ABS.DERS.DERSDETAY.DersDetayVM dersDetay = Sabitler.GetirDersDetay(id);
            //return Json(SINAVMAIN.SinavNotKaydet(notListe, aciklamaListe, dersDetay.DersPlanAnaID, dersDetay.DersPlanID, dersDetay.DersGrupID, Sabitler.Yil, Sabitler.Donem, Request.UserHostAddress.ToString(), Sabitler.KullaniciAdi, dersDetay.FakulteID));
            try
            {
                Sabis.Bolum.Core.ABS.DERS.DERSDETAY.DersDetayVM dersDetay = Sabitler.GetirDersDetay(id);
                SINAVMAIN.SinavNotKaydet(notListe, aciklamaListe, dersDetay.DersPlanAnaID, dersDetay.DersPlanID, dersDetay.DersGrupID, Sabitler.Yil, Sabitler.Donem, Request.UserHostAddress.ToString(), Sabitler.KullaniciAdi, dersDetay.FakulteID);
                return Json("true");
            }
            catch (Exception error)
            {
                Sabitler.LogException(error);
                return Json("false");
            }
        }
        public static bool GetirSinavKagitDurum(int fkPaylarID, int fkDersGrupID)
        {
            return SINAVMAIN.GetirSinavKagitDurum(fkPaylarID, fkDersGrupID);
        }
        public JsonResult SinavKagidiGosterGizle(int FKPaylarID, int FKDersGrupHocaID, bool durum, int? FKOgrenciID)
        {
            return Json(SINAVMAIN.GuncelleSinavKagitDurum(FKPaylarID, FKDersGrupHocaID, durum, FKOgrenciID));
        }

        public class ExcelModel
        {
            public HttpPostedFileBase ExcelFile { get; set; }
        }
        [AllowAnonymous]
        public void ExcelIndir(int id, int payID, string ad, string dersAdi, string dersGrup, string CalismaTipi, string ogretimTur)
        {
            bool baslikEklendiMi = false;
            //var payList = pay.Split('-');
            //var payAdList = ad.Split('-');
            List<SinavOgrenciListeVM> ogrenciListesi = SINAVMAIN.ListeleSinavOgrenciNotlari(Sabitler.GetirDersDetay(id).DersGrupID, payID).ToList();
            List<PaylarOlcmeVM> soruListesi = SINAVMAIN.ListelePaySorulari(payID).OrderBy(x => x.SoruNo).ToList();

            List<string[]> arrayList = new List<string[]>();


            string[] arrayBaslik = new string[soruListesi.Count() + 2];
            if (!baslikEklendiMi)
            {
                arrayBaslik[0] = "Numara";
                arrayBaslik[1] = "Öğrenci";

                foreach (var item in soruListesi)
                {
                    arrayBaslik[soruListesi.IndexOf(item) + 2] = "Soru (" + item.SoruNo + ")";
                }
                arrayList.Add(arrayBaslik);
                baslikEklendiMi = true;
            }


            foreach (var item in ogrenciListesi)
            {
                string[] array = new string[soruListesi.Count() + 2];
                array[0] = item.Numara;
                array[1] = item.Ad + " " + item.Soyad;

                if (item.OgrenciNotListesi.Any() && item.OgrenciNotListesi.FirstOrDefault().Notu == 120)
                {
                    for (var i = 0; i < array.Length - 2; i++)
                    {
                        array[i + 2] = "GR";
                    }
                }
                if (item.OgrenciNotListesi.Any() && item.OgrenciNotListesi.FirstOrDefault().Notu == 130)
                {
                    for (var i = 0; i < array.Length - 2; i++)
                    {
                        array[i + 2] = "DZ";
                    }
                }
                else if (item.OgrenciNotListesi.Any())
                {
                    foreach (var subItem in item.OgrenciNotListesi.OrderBy(x => x.SoruNo))
                    {
                        //if (item.OgrenciNotListesi.IndexOf(subItem) < 0 || item.OgrenciNotListesi.IndexOf(subItem) > 10)
                        //{
                        //    continue;
                        //}
                        array[item.OgrenciNotListesi.IndexOf(subItem) + 2] = Convert.ToString(subItem.Notu).Replace("120", "GR").Replace("130", "DZ");
                    }
                }



                arrayList.Add(array);
            }

            using (var excelFile = new OfficeOpenXml.ExcelPackage())
            {
                OfficeOpenXml.ExcelWorksheet sheet = excelFile.Workbook.Worksheets.Add("Sayfa1");

                sheet.Cells["A1"].LoadFromArrays(arrayList);

                sheet.Cells[sheet.Dimension.Address].AutoFitColumns();

                //sheet.Protection.AllowInsertRows = false;
                //sheet.Protection.AllowSort = false;
                //sheet.Protection.AllowSelectUnlockedCells = true;
                //sheet.Protection.AllowAutoFilter = false;
                //System.Drawing.Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#B7B7B7");
                //ws.Cells["A1:B1"].Style.Fill.BackgroundColor.SetColor(colFromHex);

                //not girilecek kolonların kilidini kaldır
                //for (int i = 0; i < soruListesi.Count(); i++)
                //{
                //    sheet.Column(i + 3).Style.Locked = false;
                //    sheet.Column(i + 3).Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                //    sheet.Column(i + 3).Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(238, 208, 208));
                //}

                for (int i = 1; i <= soruListesi.Count(); i++)
                {
                    for (int j = 1; j <= ogrenciListesi.Count + 1; j++)
                    {
                        sheet.Cells[j, i + 2].Style.Locked = false;
                        sheet.Cells[j, i + 2].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        sheet.Cells[j, i + 2].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(235, 226, 226));
                        sheet.Cells[j, i + 2].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        sheet.Cells[j, i + 2].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        sheet.Cells[j, i + 2].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        sheet.Cells[j, i + 2].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                        ////ilk 2 sütunu sabitle
                        //sheet.View.FreezePanes(j, 1);
                        //sheet.View.FreezePanes(j, 2);
                    }
                    //sheet.Column(i + 3).Style.Locked = false;
                    //sheet.Column(i + 3).Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    //sheet.Column(i + 3).Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(238, 208, 208));
                }
                //sheet.View.FreezePanes(1, 3);
                sheet.View.FreezePanes(1, 3);
                //sheet.View.FreezePanes(200, 2);

                //başlık satırını kilitle
                sheet.Row(1).Style.Locked = true;
                sheet.Protection.IsProtected = true;

                //sheet.Cells["A1:H1"].Style.Locked = true;

                Byte[] fileBytes = excelFile.GetAsByteArray();

                //Download ediyor.
                System.Web.HttpContext.Current.Response.Clear();
                System.Web.HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.Private);
                System.Web.HttpContext.Current.Response.Expires = -1;
                System.Web.HttpContext.Current.Response.Buffer = true;

                System.Web.HttpContext.Current.Response.Charset = System.Text.UTF8Encoding.UTF8.WebName;
                System.Web.HttpContext.Current.Response.ContentEncoding = System.Text.UTF8Encoding.UTF8;
                System.Web.HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                System.Web.HttpContext.Current.Response.AddHeader("content-disposition", "attachment; size=" + fileBytes.Length.ToString() + ";filename=\"" + ogretimTur + " " + dersAdi + " " + dersGrup + " GRUBU " + CalismaTipi + " NOT LİSTESİ.xls" + "\"");
                System.Web.HttpContext.Current.Response.BinaryWrite(fileBytes);
                System.Web.HttpContext.Current.Response.End();

            }
        }
        public JsonResult ExcelUpload(FormCollection formCollection)
        {
            try
            {
                var ogrenciListesi = new List<SinavOgrenciListeVM>();
                if (Request != null)
                {
                    HttpPostedFileBase file = Request.Files["ExcelFile"];
                    if ((file != null) && (file.ContentLength > 0) && !string.IsNullOrEmpty(file.FileName))
                    {
                        string fileName = file.FileName;
                        string fileContentType = file.ContentType;
                        byte[] fileBytes = new byte[file.ContentLength];
                        var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));

                        using (var package = new OfficeOpenXml.ExcelPackage(file.InputStream))
                        {
                            var currentSheet = package.Workbook.Worksheets;
                            var workSheet = currentSheet.First();
                            var noOfCol = workSheet.Dimension.End.Column;
                            var noOfRow = workSheet.Dimension.End.Row;

                            for (int i = 2; i <= noOfRow; i++)
                            {
                                if (workSheet.Cells[i, 1].Value == null)
                                {
                                    continue;
                                }
                                var ogrenci = new SinavOgrenciListeVM();
                                ogrenci.Numara = workSheet.Cells[i, 1].Value.ToString();
                                ogrenci.Ad = workSheet.Cells[i, 2].Value.ToString();

                                for (int j = 3; j <= noOfCol; j++)
                                {
                                    var yeniNot = new SinavNotGirisSoruListeVM();
                                    //yeniNot.Notu = Convert.ToDouble(String.IsNullOrEmpty((string)workSheet.Cells[i, j].Value) ? workSheet.Cells[i, j].Value.ToString().Replace("GR", "120").Replace("DZ", "130") : 120.ToString());
                                    yeniNot.Notu = Convert.ToDouble(workSheet.Cells[i, j].Value != null ? workSheet.Cells[i, j].Value.ToString().Replace("GR", "120").Replace("DZ", "130") : 120.ToString());
                                    yeniNot.SoruNo = Convert.ToInt32(System.Text.RegularExpressions.Regex.Match(workSheet.Cells[1, j].Value.ToString(), @"(?<=\().+?(?=\))").Value);
                                    ogrenci.OgrenciNotListesi.Add(yeniNot);
                                }
                                ogrenciListesi.Add(ogrenci);
                            }
                        }
                    }
                }
                var jsonResult = Json(ogrenciListesi);
                jsonResult.MaxJsonLength = Int32.MaxValue;
                return jsonResult;
            }
            catch (Exception error)
            {
                Sabitler.LogException(error);
                return Json("");
            }
        }
        #endregion

        #region SINAV KAĞIT YÜKLEME İŞLEMLERİ

        public ActionResult Yukle(int id)
        {
            return View();
        }

        public ActionResult _pYukle(int id)
        {
            return PartialView();
        }

        public ActionResult _pDosyaListesi(int P, int G, int D, int DP)
        {
            return PartialView(SINAVMAIN.GetirSinavKagitListe(P, G, D, DP, Sabitler.Yil, Sabitler.Donem, Sabitler.KullaniciAdi, Sabitler.KullaniciID));
        }


        public JsonResult SilSinavKagidi(List<SinavKagidiVM> sinavKagitListesi)
        {
            return Json(SINAVMAIN.SilSinavKagidi(sinavKagitListesi, Sabitler.KullaniciAdi, Sabitler.IPAdresi));
        }

        [AllowAnonymous]
        public ActionResult _pSinavDetaylari(int P) //fkPaylarID
        {
            return PartialView(SINAVMAIN.GetirSinavDetayi(P));
        }

        public ActionResult _pGetirOgrenciSinavKagidi(string ogrenciNo, int id, int P)
        {
            var dersDetay = Sabitler.GetirDersDetay(id);
            return PartialView(model:SINAVMAIN.GetirOgrenciSinavKagidi(ogrenciNo, P, dersDetay.DersGrupID, dersDetay.DersPlanID, dersDetay.DersPlanAnaID));
        }

        public ActionResult _pSinavKagidiDuzenle(int id)
        {
            //var dersDetay = Sabitler.GetirDersDetay(id);
            return PartialView(SINAVMAIN.GetirOgrenciSinavKagitListesi(id, "", 0, 0, 0, 0));
        }

        public JsonResult GuncelleSinavSonuc(SinavKagidiVM sonuclar)
        {
            return Json(SINAVMAIN.GuncelleOgrenciSinavKagitSonucu(sonuclar));
        }

        #endregion

        #region CEVAP ANAHTARI

        public ActionResult CevapAnahtari(int id, int D, int P)
        {
            return View();
        }
        public ActionResult _CevapAnahtari(int id, int P)
        {
            return PartialView();
        }

        public ActionResult _pCevapListesi(int id) // FKSinavID
        {
            ViewBag.FKSinavID = id;
            return PartialView(SINAVMAIN.GetirCevapAnahtariListe(id));
        }

        public JsonResult EkleCevapAnahtari(int FKSinavID)
        {
            return Json(SINAVMAIN.EkleCevapAnahtari(FKSinavID));
        }

        public JsonResult SilCevapAnahtari(int FKSinavID)
        {
            return Json(SINAVMAIN.SilCevapAnahtari(FKSinavID));
        }

        public JsonResult KaydetCevapAnahtari(CevapAnahtariVM cevapAnahtari)
        {
            return Json(SINAVMAIN.KaydetCevapAnahtari(cevapAnahtari));
        }

        #endregion

        #region fileupload ayar
        FilesHelper filesHelper;
        String tempPath = "~/Content/temp/";
        String serverMapPath = "~/Content/files/";
        private string StorageRoot
        {
            get { return Path.Combine(HostingEnvironment.MapPath(serverMapPath)); }
        }
        private string UrlBase = "/Content/files/";
        String DeleteURL = "/Ders/Sinav/DeleteFile/?file=";
        String DeleteType = "GET";
        public SinavController()
        {
            filesHelper = new FilesHelper(DeleteURL, DeleteType, StorageRoot, UrlBase, tempPath, serverMapPath);
        }

        #endregion

        public ActionResult Show()
        {
            JsonFiles ListOfFiles = filesHelper.GetFileList();
            var model = new FilesViewModel()
            {
                Files = ListOfFiles.files
            };

            return View(model);
        }

        public JsonResult GuncelleSinavDurum(int FKSinavID, int Durum)
        {
            return Json(SINAVMAIN.GuncelleSinavDurum(FKSinavID, Durum));
        }
        [HttpPost]
        public JsonResult SinavDokumanUpload(int FKPaylarID, int FKDersGrupID, int enumDokumanTuru)
        {
            List<SinavDokumanVM> sinavKagitListe = new List<SinavDokumanVM>();

            var httpRequest = HttpContext.Request;

            foreach (var item in httpRequest.Files)
            {
                Dictionary<string, string> fileAttributes = new Dictionary<string, string>();
                var file = Request.Files[item.ToString()];

                SinavDokumanVM yeniDokuman = new SinavDokumanVM();
                yeniDokuman.SGTarihi = DateTime.Now;
                yeniDokuman.DosyaAdi = file.FileName;
                yeniDokuman.DosyaKey = SDFSClient.FileClient.Upload(Sabitler.KullaniciAdi, file.ToByteArray(), file.FileName, false, "abis", "F4tpepP93Hhgd-oT", fileAttributes);
                yeniDokuman.EnumDokumanTuru = enumDokumanTuru;

                sinavKagitListe.Add(yeniDokuman);
            }

            try
            {
                SINAVMAIN.KaydetSinavDokuman(sinavKagitListe, enumDokumanTuru, FKPaylarID, Sabitler.Yil, FKDersGrupID, Sabitler.KullaniciAdi, Sabitler.IPAdresi);
                return Json("true");
            }
            catch (Exception error)
            {
                Sabitler.LogException(error);
                return Json("false");
            }
        }

        public JsonResult SinavDokumanSil(int fkSinavDokumanID)
        {
            try
            {
                SINAVMAIN.SilSinavDokuman(fkSinavDokumanID, Sabitler.KullaniciAdi, Sabitler.IPAdresi);
                return Json("true");
            }
            catch (Exception error)
            {
                Sabitler.LogException(error);
                return Json("false");
            }
        }

        [HttpPost]
        public JsonResult Upload(int FKPaylarID, int FKDersGrupID)
        {

            List<SinavKagidiVM> sinavKagitListe = new List<SinavKagidiVM>();

            //var sdfsLogin = SDFSClient.AppSettings.KullaniciBilgileri(ConfigurationManager.AppSettings["UserBelge"], ConfigurationManager.AppSettings["PasswordBelge"]);

            var httpRequest = HttpContext.Request;

            foreach (var item in httpRequest.Files)
            {
                Dictionary<string, string> fileAttributes = new Dictionary<string, string>();
                var file = Request.Files[item.ToString()];

                //string uzanti = Path.GetExtension(file.FileName).ToLower();
                //if (uzanti != ".jpg" || uzanti != ".png")
                //{
                //    continue;
                //}

                SinavKagidiVM yeniKagit = new SinavKagidiVM();
                yeniKagit.Kullanici = Sabitler.KullaniciAdi;
                yeniKagit.IP = Sabitler.IPAdresi;
                yeniKagit.TarihKayit = DateTime.Now;

                

                yeniKagit.DosyaAdi = file.FileName;
                
                yeniKagit.DosyaKey = SDFSClient.FileClient.Upload(Sabitler.KullaniciAdi, file.ToByteArray(), file.FileName, false, "abis", "F4tpepP93Hhgd-oT", fileAttributes);
                yeniKagit.EnumKagitOkumaDurumu = 1;

                sinavKagitListe.Add(yeniKagit);
                
                //var test = file.ToByteArray();
                
                //string dosyaKey = SDFSClient.FileClient.Upload(Sabitler.KullaniciAdi, file.ToByteArray(), "test.jpg", false, "abis", "F4tpepP93Hhgd-oT", fileAttributes);
                
            }

            
           
            //fileAttributes.Add("dersgrupid", ID.ToString());
            


            return Json(SINAVMAIN.KaydetSinavKagidi(sinavKagitListe, FKPaylarID, Sabitler.Yil, FKDersGrupID));

        }
        public JsonResult GetFileList()
        {
            var list = filesHelper.GetFileList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult DeleteFile(string file)
        {
            filesHelper.DeleteFile(file);
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public class FilesViewModel
        {
            public ViewDataUploadFilesResult[] Files { get; set; }
        }

        #region SINAV DEĞERLENDİR
        public JsonResult SinavDegerlendir(int FKSinavID, int FKPaylarID, int FKDersGrupHocaID, int? FKSinavKagitID, bool grupNotlariSilinsinMi = true)
        {
            try
            {
                SINAVMAIN.SinavDegerlendir(FKSinavID, FKPaylarID, FKDersGrupHocaID, Sabitler.Yil, Sabitler.Donem, Sabitler.KullaniciAdi, Sabitler.IPAdresi, FKSinavKagitID, grupNotlariSilinsinMi);
                return Json("true");
            }
            catch (Exception error)
            {
                Sabitler.LogException(error);
                return Json("false");
            }
        }
        #endregion

        public JsonResult SilGrupNotlari(int FKDersGrupHocaID, int FKABSPaylarID)
        {
            List<int> payIDList = new List<int>();
            payIDList.Add(FKABSPaylarID);
            var dersDetay = Sabitler.GetirDersDetay(FKDersGrupHocaID);
            var detayNotlariSilindiMi = Sabis.Bolum.Bll.SOURCE.ABS.NOT.NOTMAIN.NotDetaylariniSilindiYap(new List<int>(), payIDList, Sabitler.IPAdresi, Sabitler.KullaniciAdi);
            var grupNotlariniSilindiMi = Sabis.Bolum.Bll.SOURCE.ABS.NOT.NOTMAIN.NotlariSilindiYap(new List<int>(), payIDList, Sabitler.IPAdresi, Sabitler.KullaniciAdi);
            return Json(grupNotlariniSilindiMi && detayNotlariSilindiMi);
        }

        //public JsonResult SilOgrenciNotlari(int FKDersGrupHocaID, int FKABSPaylarID, int FKOgrenciID)
        //{
        //    return Json(SINAVMAIN.SilOgrenciNotlari(FKDersGrupHocaID, FKABSPaylarID, FKOgrenciID));
        //}

        public JsonResult SilSinavKagitlari(int FKSinavID, int FKDersGrupID)
        {
            return Json(SINAVMAIN.SilSinavKagitlari(FKSinavID, FKDersGrupID, Sabitler.KullaniciAdi));
        }

        public ActionResult _pGetirOrtakDersGrupListesi(int FKDersGrupHocaID)
        {
            return PartialView();
        }

        public ActionResult _SinavIstatistikleri(int fkPaylarID, int fkDersGrupID)
        {
            return PartialView(SINAVMAIN.GetirSinavIstatistik(fkPaylarID, fkDersGrupID));
        }

    }
}