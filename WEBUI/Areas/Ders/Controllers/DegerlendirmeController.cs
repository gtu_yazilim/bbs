﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Sabis.Bolum.Core.ABS.DEGERLENDIRME;
using System.Text;

namespace Sabis.Bolum.WebUI.Areas.Ders.Controllers
{
    //[AllowAnonymous]
    public class DegerlendirmeController : Controller
    {
        public ActionResult Index(int ID, bool hiyerarsik = false)
        {
            DegerlendirmeViewModel result = null;
            if (hiyerarsik)
            {
                result = Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME.DegerlendirmeBL.GetirDegerlendirme(ID, true, Sabitler.KullaniciAdi, Sabitler.KullaniciID, Sabitler.IPAdresi, Sabitler.SimulasyonKullaniciAdi, Sabitler.Yil, Sabitler.DegerlendirmeDonem);
            }
            else
            {
                ViewBag.DersGrupID = Sabitler.GetirDersDetay(ID).DersGrupID;
                result = Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME.DegerlendirmeBL.GetirDegerlendirme(Sabitler.GetirDersDetay(ID).DersGrupID, false, Sabitler.KullaniciAdi, Sabitler.KullaniciID, Sabitler.IPAdresi, Sabitler.SimulasyonKullaniciAdi, Sabitler.Yil, Sabitler.DegerlendirmeDonem);
            }

            if (result.DegerlendirmeCikti != null)
            {
                VeriHazirla(result.DegerlendirmeCikti.IstatistikMutlak, result.DegerlendirmeCikti.IstatistikBagil);
            }

            if (result.OrtakGruplar == null)
            {
                result.OrtakGruplar = new List<Grup>();
            }

            if (result.DegerlendirmeCikti == null)
            {
                result.DegerlendirmeCikti = new CiktiDTO();
                result.DegerlendirmeCikti.NotList = new List<OgrNot>();
            }

            result.IslemID = 0;
            if (result.Ders == null)
            {
                result.Ders = new DersDTO();
            }
            else
            {
                result.IslemID = result.Ders.Hiyerarsik ? result.Ders.DersPlanAnaID : result.Ders.DersGrupID.Value;
            }

#if DEBUG
            result.SonHalKaldirmaYetkisiVarMi = true;
#endif

            return View(result);
        }

        [HttpPost]
        public JsonResult Hesapla(int ID, string bagil, string ust, bool hiyerarsik = false)
        {
            bagil = bagil.Replace('.', ',');
            ust = ust.Replace('.', ',');
            double bagilOrtalama = double.Parse(bagil);
            double ustSinir = double.Parse(ust);

            DegerlendirmeViewModel result = new DegerlendirmeViewModel();
            if (hiyerarsik)
            {
                result.NotificationList = new List<Notification>();
                try
                {
                    Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME.HDegerlendirmeYonetim hdeg = new Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME.HDegerlendirmeYonetim(ID, Sabitler.Yil, Sabitler.DegerlendirmeDonem, Sabitler.KullaniciAdi, Sabitler.KullaniciID, Sabitler.IPAdresi);
                    hdeg.Degerlendir(default(double?));
                    hdeg.Kaydet();
                    result.DegerlendirmeCikti = hdeg.DegerlendirmeSonuc;
                    result.Ders = hdeg.SeciliDers;
                    result.AkademikTakvim = hdeg.AkademikTakvim;
                    result.NotificationList = hdeg.NotificationList;
                    result.EnumDegerlendirmeAsama = hdeg.EnumDegerlendirmeAsama;
                    result.SonHalKaldirmaYetkisiVarMi = hdeg.SonHalKaldirmaYetkisiVarMi;
                }
                catch (Exception ex)
                {
                    result.NotificationList.Add(new Notification("HATA", ex.Message, Notification.MasterMesajTip.Hata));
                }

            }
            else
            {
                result = Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME.DegerlendirmeBL.Hesapla(bagilOrtalama, ustSinir, ID, Sabitler.KullaniciAdi, Sabitler.KullaniciID, Sabitler.IPAdresi, Sabitler.SimulasyonKullaniciAdi);
            }
            return Json(result.NotificationList);
        }

        [HttpPost]
        public JsonResult Sifirla(int ID, bool hiyerarsik = false)
        {
            DegerlendirmeViewModel result = new DegerlendirmeViewModel();
            if (hiyerarsik)
            {
                result.NotificationList = new List<Notification>();
                try
                {
                    Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME.HDegerlendirmeYonetim hdeg = new Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME.HDegerlendirmeYonetim(ID, Sabitler.Yil, Sabitler.DegerlendirmeDonem, Sabitler.KullaniciAdi, Sabitler.KullaniciID, Sabitler.IPAdresi);
                    hdeg.Sifirla();
                    result.DegerlendirmeCikti = hdeg.DegerlendirmeSonuc;
                    result.Ders = hdeg.SeciliDers;
                    result.AkademikTakvim = hdeg.AkademikTakvim;
                    result.NotificationList = hdeg.NotificationList;
                    result.EnumDegerlendirmeAsama = hdeg.EnumDegerlendirmeAsama;
                    result.SonHalKaldirmaYetkisiVarMi = hdeg.SonHalKaldirmaYetkisiVarMi;
                }
                catch (Exception ex)
                {
                    result.NotificationList.Add(new Notification("HATA", ex.Message, Notification.MasterMesajTip.Hata));
                }

            }
            else
            {
                result = Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME.DegerlendirmeBL.Sifirla(ID, Sabitler.KullaniciAdi, Sabitler.KullaniciID, Sabitler.IPAdresi, Sabitler.SimulasyonKullaniciAdi);
            }
            return Json(result.NotificationList);
        }

        [HttpPost]
        public JsonResult SonHal(int ID, bool hiyerarsik = false)
        {
            DegerlendirmeViewModel result = new DegerlendirmeViewModel();
            if (hiyerarsik)
            {
                result.NotificationList = new List<Notification>();
                try
                {
                    Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME.HDegerlendirmeYonetim hdeg = new Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME.HDegerlendirmeYonetim(ID, Sabitler.Yil, Sabitler.DegerlendirmeDonem, Sabitler.KullaniciAdi, Sabitler.KullaniciID, Sabitler.IPAdresi);
                    hdeg.Yayinla();
                    hdeg.SonHal();
                    result.DegerlendirmeCikti = hdeg.DegerlendirmeSonuc;
                    result.Ders = hdeg.SeciliDers;
                    result.AkademikTakvim = hdeg.AkademikTakvim;
                    result.NotificationList = hdeg.NotificationList;
                    result.EnumDegerlendirmeAsama = hdeg.EnumDegerlendirmeAsama;
                    result.SonHalKaldirmaYetkisiVarMi = hdeg.SonHalKaldirmaYetkisiVarMi;
                }
                catch (Exception ex)
                {
                    result.NotificationList.Add(new Notification("HATA", ex.Message, Notification.MasterMesajTip.Hata));
                }
            }
            else
            {
                result = Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME.DegerlendirmeBL.SonHal(ID, Sabitler.KullaniciAdi, Sabitler.KullaniciID, Sabitler.IPAdresi, Sabitler.SimulasyonKullaniciAdi);
            }

            return Json(result.NotificationList);
        }

        [HttpPost]
        public JsonResult SonHalSerbest(int ID)
        {
            var result = Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME.DegerlendirmeBL.SonHalSerbest(ID, Sabitler.KullaniciAdi, Sabitler.KullaniciID, Sabitler.IPAdresi, Sabitler.SimulasyonKullaniciAdi);
            return Json(result.NotificationList);
        }

        [HttpPost]
        public JsonResult SonHalKaldir(int ID, bool hiyerarsik = false)
        {
            DegerlendirmeViewModel result = new DegerlendirmeViewModel();
            if (hiyerarsik)
            {
                result.NotificationList = new List<Notification>();
                try
                {
                    Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME.HDegerlendirmeYonetim hdeg = new Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME.HDegerlendirmeYonetim(ID, Sabitler.Yil, Sabitler.DegerlendirmeDonem, Sabitler.KullaniciAdi, Sabitler.KullaniciID, Sabitler.IPAdresi);
                    hdeg.SonHalKaldir();
                    result.DegerlendirmeCikti = hdeg.DegerlendirmeSonuc;
                    result.Ders = hdeg.SeciliDers;
                    result.AkademikTakvim = hdeg.AkademikTakvim;
                    result.NotificationList = hdeg.NotificationList;
                    result.EnumDegerlendirmeAsama = hdeg.EnumDegerlendirmeAsama;
                    result.SonHalKaldirmaYetkisiVarMi = hdeg.SonHalKaldirmaYetkisiVarMi;
                }
                catch (Exception ex)
                {
                    result.NotificationList.Add(new Notification("HATA", ex.Message, Notification.MasterMesajTip.Hata));
                }
            }
            else
            {
                result = Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME.DegerlendirmeBL.SonHalKaldir(ID, Sabitler.KullaniciAdi, Sabitler.KullaniciID, Sabitler.IPAdresi, Sabitler.SimulasyonKullaniciAdi);
            }
            return Json(result.NotificationList);
        }

        [HttpPost]
        public JsonResult SonHalKaldirSerbest(int ID)
        {
            var result = Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME.DegerlendirmeBL.SonHalKaldirSerbest(ID, Sabitler.KullaniciAdi, Sabitler.KullaniciID, Sabitler.IPAdresi, Sabitler.SimulasyonKullaniciAdi);
            Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME.DegerlendirmeBL.YayindanKaldirSerbest(ID, Sabitler.KullaniciAdi, Sabitler.KullaniciID, Sabitler.IPAdresi, Sabitler.SimulasyonKullaniciAdi);
            return Json(result.NotificationList);
        }

        [HttpPost]
        public JsonResult Yayinla(int ID, bool hiyerarsik = false)
        {
            DegerlendirmeViewModel result = new DegerlendirmeViewModel();
            if (hiyerarsik)
            {
                result.NotificationList = new List<Notification>();
                try
                {
                    Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME.HDegerlendirmeYonetim hdeg = new Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME.HDegerlendirmeYonetim(ID, Sabitler.Yil, Sabitler.DegerlendirmeDonem, Sabitler.KullaniciAdi, Sabitler.KullaniciID, Sabitler.IPAdresi);
                    hdeg.Yayinla();
                    result.DegerlendirmeCikti = hdeg.DegerlendirmeSonuc;
                    result.Ders = hdeg.SeciliDers;
                    result.AkademikTakvim = hdeg.AkademikTakvim;
                    result.NotificationList = hdeg.NotificationList;
                    result.EnumDegerlendirmeAsama = hdeg.EnumDegerlendirmeAsama;
                    result.SonHalKaldirmaYetkisiVarMi = hdeg.SonHalKaldirmaYetkisiVarMi;
                }
                catch (Exception ex)
                {
                    result.NotificationList.Add(new Notification("HATA", ex.Message, Notification.MasterMesajTip.Hata));
                }

            }
            else
            {
                result = Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME.DegerlendirmeBL.Yayinla(ID, Sabitler.KullaniciAdi, Sabitler.KullaniciID, Sabitler.IPAdresi, Sabitler.SimulasyonKullaniciAdi);
            }
            return Json(result.NotificationList);
        }

        [HttpPost]
        public JsonResult YayinlaSerbest(int ID)
        {
            var result = Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME.DegerlendirmeBL.YayinlaSerbest(ID, Sabitler.KullaniciAdi, Sabitler.KullaniciID, Sabitler.IPAdresi, Sabitler.SimulasyonKullaniciAdi);
            return Json(result.NotificationList);
        }

        [HttpPost]
        public JsonResult YayindanKaldir(int ID, bool hiyerarsik = false)
        {
            DegerlendirmeViewModel result = new DegerlendirmeViewModel();
            if (hiyerarsik)
            {
                result.NotificationList = new List<Notification>();
                try
                {
                    Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME.HDegerlendirmeYonetim hdeg = new Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME.HDegerlendirmeYonetim(ID, Sabitler.Yil, Sabitler.DegerlendirmeDonem, Sabitler.KullaniciAdi, Sabitler.KullaniciID, Sabitler.IPAdresi);
                    hdeg.YayindanKaldir();
                    result.DegerlendirmeCikti = hdeg.DegerlendirmeSonuc;
                    result.Ders = hdeg.SeciliDers;
                    result.AkademikTakvim = hdeg.AkademikTakvim;
                    result.NotificationList = hdeg.NotificationList;
                    result.EnumDegerlendirmeAsama = hdeg.EnumDegerlendirmeAsama;
                    result.SonHalKaldirmaYetkisiVarMi = hdeg.SonHalKaldirmaYetkisiVarMi;
                }
                catch (Exception ex)
                {
                    result.NotificationList.Add(new Notification("HATA", ex.Message, Notification.MasterMesajTip.Hata));
                }

            }
            else
            {
                result = Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME.DegerlendirmeBL.YayindanKaldir(ID, Sabitler.KullaniciAdi, Sabitler.KullaniciID, Sabitler.IPAdresi, Sabitler.SimulasyonKullaniciAdi);
            }
            return Json(result.NotificationList);
        }

        [HttpPost]
        public JsonResult YayindanKaldirSerbest(int ID)
        {
            var result = Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME.DegerlendirmeBL.YayindanKaldirSerbest(ID, Sabitler.KullaniciAdi, Sabitler.KullaniciID, Sabitler.IPAdresi, Sabitler.SimulasyonKullaniciAdi);
            return Json(result.NotificationList);
        }

        public ActionResult BasariListesiOlustur(int ID, bool hiyerarsik = false)
        {
            var result = Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME.DegerlendirmeBL.BasariListesiRapor(ID, Sabitler.Yil, Sabitler.DegerlendirmeDonem, Sabitler.KullaniciAdi, Sabitler.KullaniciID, Sabitler.IPAdresi, Sabitler.SimulasyonKullaniciAdi, hiyerarsik);
            var rapor = new Sabis.Bolum.Rapor.BasariListesi(result, Sabitler.UniversiteAdi, Sabitler.LogoUrl);

            var sdfsLogin = SDFSClient.AppSettings.KullaniciBilgileri("UserBelge", "PasswordBelge");
            Dictionary<string, string> fileAttributes = new Dictionary<string, string>();
            if (hiyerarsik)
            {
                fileAttributes.Add("dersplananaid", ID.ToString());
            }
            else
            {
                fileAttributes.Add("grupid", ID.ToString());
            }
            fileAttributes.Add("kullanici", Sabitler.KullaniciAdi);
            // fileAttributes.Add("simulasyon", Sabitler.SimulasyonKullaniciAdi);
            string returnURL = string.Empty;
            try
            {
                Telerik.Reporting.Processing.ReportProcessor reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
                Telerik.Reporting.InstanceReportSource instanceReportSource = new Telerik.Reporting.InstanceReportSource();
                instanceReportSource.ReportDocument = rapor;
                Telerik.Reporting.Processing.RenderingResult reportResult = reportProcessor.RenderReport("PDF", instanceReportSource, null);
                string dosyaKey = SDFSClient.FileClient.Upload(Sabitler.KullaniciAdi, reportResult.DocumentBytes, "basari_belgesi_" + ID + ".pdf", false, sdfsLogin.Key, sdfsLogin.Value, fileAttributes);

                Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME.DegerlendirmeBL.BasariRaporKaydet(ID, dosyaKey, Sabitler.KullaniciAdi, Sabitler.IPAdresi);
                returnURL = SDFSClient.FileClient.GetUrl(dosyaKey, inlineHeaderEkle: true);
            }
            catch (Exception)
            {

            }

            //ExportToPDF(rapor, "Başarı Listesi-" + ID + ".pdf");
            return Json(returnURL);
            //return View();
        }

        public ActionResult BasariListesiOlusturTumGruplar(List<int> GrupIDList)
        {
            List<Tuple<int, string>> result = new List<Tuple<int, string>>();

            foreach (var item in GrupIDList)
            {
                result.Add(new Tuple<int, string>(item, RaporOlustur(item)));
            }

            return Json(result);
        }

        private string RaporOlustur(int grupID)
        {
            var result = Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME.DegerlendirmeBL.BasariListesiRapor(grupID, Sabitler.Yil, Sabitler.DegerlendirmeDonem, Sabitler.KullaniciAdi, Sabitler.KullaniciID, Sabitler.IPAdresi, Sabitler.SimulasyonKullaniciAdi, false);
            var rapor = new Sabis.Bolum.Rapor.BasariListesi(result, Sabitler.UniversiteAdi, Sabitler.LogoUrl);

            var sdfsLogin = SDFSClient.AppSettings.KullaniciBilgileri("UserBelge", "PasswordBelge");
            Dictionary<string, string> fileAttributes = new Dictionary<string, string>();
            fileAttributes.Add("grupid", grupID.ToString());
            fileAttributes.Add("kullanici", Sabitler.KullaniciAdi);
            // fileAttributes.Add("simulasyon", Sabitler.SimulasyonKullaniciAdi);
            string returnURL = string.Empty;
            try
            {
                Telerik.Reporting.Processing.ReportProcessor reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
                Telerik.Reporting.InstanceReportSource instanceReportSource = new Telerik.Reporting.InstanceReportSource();
                instanceReportSource.ReportDocument = rapor;
                Telerik.Reporting.Processing.RenderingResult reportResult = reportProcessor.RenderReport("PDF", instanceReportSource, null);
                string dosyaKey = SDFSClient.FileClient.Upload(Sabitler.KullaniciAdi, reportResult.DocumentBytes, "basari_belgesi_" + grupID + ".pdf", false, sdfsLogin.Key, sdfsLogin.Value, fileAttributes);

                Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME.DegerlendirmeBL.BasariRaporKaydet(grupID, dosyaKey, Sabitler.KullaniciAdi, Sabitler.IPAdresi);
                returnURL = SDFSClient.FileClient.GetUrl(dosyaKey, inlineHeaderEkle: true);
            }
            catch (Exception)
            {

            }

            return returnURL;
        }

        public ActionResult BasariListesiOlusturSerbest(int ID, bool butunlemeMi)
        {
            //var result = Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME.DegerlendirmeBL.BasariListesiRaporSerbest(ID, Sabitler.KullaniciAdi, Sabitler.KullaniciID, Sabitler.IPAdresi, Sabitler.SimulasyonKullaniciAdi);

            BasariListesiDTO result = null;

            if (butunlemeMi)
                result = Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME.DegerlendirmeBL.BasariListesiRaporSerbestButunleme(ID, Sabitler.KullaniciAdi, Sabitler.KullaniciID, Sabitler.IPAdresi, Sabitler.SimulasyonKullaniciAdi);
            else result = Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME.DegerlendirmeBL.BasariListesiRaporSerbestFinal(ID, Sabitler.KullaniciAdi, Sabitler.KullaniciID, Sabitler.IPAdresi, Sabitler.SimulasyonKullaniciAdi);
            

            var rapor = new Sabis.Bolum.Rapor.BasariNotListe(result, Sabitler.UniversiteAdi, Sabitler.LogoUrl);

            var sdfsLogin = SDFSClient.AppSettings.KullaniciBilgileri("UserBelge", "PasswordBelge");
            Dictionary<string, string> fileAttributes = new Dictionary<string, string>();
            fileAttributes.Add("grupid", ID.ToString());
            fileAttributes.Add("kullanici", Sabitler.KullaniciAdi);
            // fileAttributes.Add("simulasyon", Sabitler.SimulasyonKullaniciAdi);
            string returnURL = string.Empty;
            try
            {

                Telerik.Reporting.Processing.ReportProcessor reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
                Telerik.Reporting.InstanceReportSource instanceReportSource = new Telerik.Reporting.InstanceReportSource();
                instanceReportSource.ReportDocument = rapor;
                Telerik.Reporting.Processing.RenderingResult reportResult = reportProcessor.RenderReport("PDF", instanceReportSource, null);
                string dosyaKey = SDFSClient.FileClient.Upload(Sabitler.KullaniciAdi, reportResult.DocumentBytes, "basari_belgesi_" + ID + ".pdf", false, sdfsLogin.Key, sdfsLogin.Value, fileAttributes);
                Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME.DegerlendirmeBL.BasariRaporKaydet(ID, dosyaKey, Sabitler.KullaniciAdi, Sabitler.IPAdresi);
                returnURL = SDFSClient.FileClient.GetUrl(dosyaKey, inlineHeaderEkle: true);
            }
            catch (Exception)
            {

            }

            //ExportToPDF(rapor, "Başarı Listesi-" + ID + ".pdf");

            return Json(returnURL);
            //return View();
        }

        public ActionResult YardimciListeOlustur(int ID, bool hiyerarsik = false)
        {
            var result = Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME.DegerlendirmeBL.BasariListesiRapor(ID, Sabitler.Yil, Sabitler.DegerlendirmeDonem, Sabitler.KullaniciAdi, Sabitler.KullaniciID, Sabitler.IPAdresi, Sabitler.SimulasyonKullaniciAdi, false);

            var rapor = new Sabis.Bolum.Rapor.YardimciListe(result, Sabitler.UniversiteAdi, Sabitler.LogoUrl);

            ExportToPDF(rapor, "Yardımcı Liste -" + ID + ".pdf");

            return new EmptyResult();
        }

        [HttpPost]
        public JsonResult BasariRaporListele(int ID, bool hiyerarsik = false)
        {
            var result = Sabis.Bolum.Bll.SOURCE.ABS.DEGERLENDIRME.DegerlendirmeBL.BasariRaporListele(ID);

            var tmpList = new List<object>();
            foreach (var item in result)
            {
                tmpList.Add(new
                {
                    Url = SDFSClient.FileClient.GetUrl(item.DosyaKey),
                    Tarih = item.Tarih.ToString()
                });
            }

            return Json(tmpList);
        }

        private void PDFIndir(byte[] rapor, string fileName)
        {

            Response.Clear();

            Response.ContentType = "application/pdf; name=" + fileName;
            Response.AddHeader("content-disposition", "inline; filename=" + fileName);

            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;

            Response.Buffer = true;

            Response.AddHeader("Content-Length", rapor.Length.ToString());
            Response.BinaryWrite(rapor);
            Response.End();
        }

        private void ExportToPDF(Telerik.Reporting.Report reportToExport, string fileName)
        {
            Telerik.Reporting.Processing.ReportProcessor reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
            Telerik.Reporting.InstanceReportSource instanceReportSource = new Telerik.Reporting.InstanceReportSource();
            instanceReportSource.ReportDocument = reportToExport;
            Telerik.Reporting.Processing.RenderingResult result = reportProcessor.RenderReport("PDF", instanceReportSource, null);
            PDFIndir(result.DocumentBytes, fileName);
        }

        public void VeriHazirla(List<Istatistik> listMutlak, List<Istatistik> listBagil)
        {
            string dataMutlak, dataBagil, dataBirlesik, colorMutlak, colorBagil;

            StringBuilder sb = new StringBuilder();
            //Mutlak işemler
            colorMutlak = string.Empty;
            var grupMutlak = listMutlak.Where(x => x.EnumHarf < 9).OrderBy(x => x.Harf);
            int say = 0;
            sb.AppendLine("[ ['Harf', 'Adet'],");
            foreach (Istatistik item in grupMutlak)
            {
                say++;
                sb.AppendFormat("['{0}', {1}]", item.Harf, item.Adet);
                if (say < grupMutlak.Count())
                {
                    sb.Append(", ");
                }
            }
            sb.Append("]");
            dataMutlak = sb.ToString();

            sb.Clear();

            //Bağıl İşlemler
            colorBagil = string.Empty;
            var grupBagil = listBagil.Where(x => x.EnumHarf < 9).OrderBy(x => x.Harf);
            say = 0;
            sb.AppendLine("[ ['Harf', 'Adet'],");
            foreach (Istatistik item in grupBagil)
            {
                say++;
                sb.AppendFormat("['{0}', {1}]", item.Harf, item.Adet);
                if (say < grupBagil.Count())
                {
                    sb.Append(", ");
                }
            }
            sb.Append("]");
            dataBagil = sb.ToString();


            sb.Clear();

            //Birleşik İşlemler
            colorBagil = string.Empty;
            say = 0;
            sb.AppendLine("[ ['Harf', 'MutlakAdet', 'BagilAdet'],");
            foreach (Istatistik item in grupMutlak)
            {
                say++;
                string bagilAdet = grupBagil.FirstOrDefault(x => x.EnumHarf == item.EnumHarf).Adet.ToString();
                sb.AppendFormat("['{0}', {1}, {2}]", item.Harf, item.Adet, bagilAdet);
                if (say < grupBagil.Count())
                {
                    sb.Append(", ");
                }
            }
            sb.Append("]");
            dataBirlesik = sb.ToString();


            sb.Clear();
            sb = null;

            ViewBag.MutlakGrafik = dataMutlak;
            ViewBag.BagilGrafik = dataBagil;
            ViewBag.BirlesikGrafik = dataBirlesik;
        }


        [AllowAnonymous]
        public ActionResult DegerlendirmeBilgisi(int grupID)
        {
            List<Grup> result = null;
            Bll.SOURCE.ABS.DEGERLENDIRME.Data data = new Bll.SOURCE.ABS.DEGERLENDIRME.Data();

            result = data.OrtakDegerlendirmeGrupBul(grupID, false);

            ViewBag.Degerlendirici = data.GetirDegerlendirecekPersonel(grupID);

            return View(result);
        }

        public ActionResult _EksikNotGirilenDersGruplari(int fkDersPlanAnaID, int fkDersGrupID)
        {
            return PartialView(Bll.SOURCE.ABS.DEGERLENDIRME.DegerlendirmeBL.GetirEksikNotGirilenDersGruplari(fkDersPlanAnaID, fkDersGrupID, Sabitler.Yil, Sabitler.Donem));
        }

        public ActionResult _YayinlanmayanNotlar(int fkDersPlanAnaID, int fkDersGrupID)
        {
            return PartialView(Bll.SOURCE.ABS.DEGERLENDIRME.DegerlendirmeBL.GetirYayinlanmayanPayListesi(fkDersPlanAnaID, fkDersGrupID, Sabitler.Yil, Sabitler.Donem));
        }
    }
}