﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Sabis.Bolum.Core.ABS.DERS.DERSDETAY;
using Sabis.Bolum.Bll.SOURCE.ABS.GENEL;
using Sabis.Bolum.Core.ABS.DERS.HOME;
using Sabis.Bolum.Bll.SOURCE.ABS.DERS;

using Sabis.Bolum.Core.ABS.DERS.HOME;

namespace Sabis.Bolum.WebUI.Areas.Ders.Controllers
{
    //[AllowAnonymous]
    public class DetayController : Controller
    {
        private IDers _ders;
        public DetayController(IDers ders)
        {
            _ders = ders;
        }
        // GET: Ders/Detay
        #region GRUP DERS
        public ActionResult Grup(int id)
        {
            DersDetayVM ders = Sabitler.GetirDersDetay(id, Sabitler.KullaniciID);

            if (ders == null)
            {
                HttpContext.Response.Redirect("/");
            }
            else
            {
                if (!GENELMAIN.DersGrupHocaMi(id, Sabitler.KullaniciID))
                {
                    if (ders.HiyerarsikKokID.HasValue && !(Sabis.Bolum.Bll.SOURCE.EBS.GENEL.GENELMAIN.DersKoordinatoruMu(ders.HiyerarsikKokID.Value, Sabitler.KullaniciID)))
                    {
                        HttpContext.Response.Redirect("/");
                    }
                    if (!ders.HiyerarsikKokID.HasValue && !(Sabis.Bolum.Bll.SOURCE.EBS.GENEL.GENELMAIN.DersKoordinatoruMu(ders.DersPlanAnaID, Sabitler.KullaniciID)))
                    {
                        HttpContext.Response.Redirect("/");
                    }
                }

                //if (
                //    ders.EnumKokDersTipi == (int)Sabis.Enum.EnumKokDersTipi.BITIRME ||
                //    ders.EnumKokDersTipi == (int)Sabis.Enum.EnumKokDersTipi.STAJ ||
                //    ders.EnumKokDersTipi == (int)Sabis.Enum.EnumKokDersTipi.TASARIM ||
                //    ders.EnumKokDersTipi == (int)Sabis.Enum.EnumKokDersTipi.UZMANLIKALANI ||
                //    ders.EnumKokDersTipi == (int)Sabis.Enum.EnumKokDersTipi.ISYERI_UYGULAMASI ||
                //    ders.EnumKokDersTipi == (int)Sabis.Enum.EnumKokDersTipi.SEMINER ||
                //    ders.EnumKokDersTipi == (int)Sabis.Enum.EnumKokDersTipi.ENSTITU_PROJE ||
                //    ders.EnumKokDersTipi == (int)Sabis.Enum.EnumKokDersTipi.TIP_FAZ3_STAJ ||
                //    ders.EnumDegerlendirmeTip == (int)Sabis.Enum.EnumDegerlendirmeTipi.BASARI_NOT)
                //{
                try
                { 
                    TempData["EnumKokDersTipi"] = ders.EnumKokDersTipi;
                    HttpContext.Response.Redirect("/Ders/BasariNotGirisi/Grup/" + id);
                }
                catch //TODO RMZN edit: geniş zamanda buradan gelen ex. araştırılacak
                {
                    HttpContext.Response.Redirect("/");
                }
                //}
            }

            return View(Sabis.Bolum.Bll.SOURCE.ABS.DERS.DERSMAIN.GetirDersDetay(id));
        }

        public ActionResult Sinav(int id, int? tab)
        {
            var payList = PAYMAIN.GetirPayBilgileriv2(id, Sabitler.KullaniciAdi, Sabitler.IPAdresi)
                .Where(x => x.EnumCalismaTipi == (int)Sabis.Enum.EnumCalismaTip.AraSinav ||
                            x.EnumCalismaTipi == (int)Sabis.Enum.EnumCalismaTip.KisaSinav ||
                            x.EnumCalismaTipi == (int)Sabis.Enum.EnumCalismaTip.SozluSinav ||
                            x.EnumCalismaTipi == (int)Sabis.Enum.EnumCalismaTip.Final
                            ).ToList();
            ViewBag.AktifTab = tab;
            return View(payList);
        }
        
        public ActionResult Odev(int id)
        {
            var payList = PAYMAIN.GetirPayBilgileriv2(id, Sabitler.KullaniciAdi, Sabitler.IPAdresi)
                .Where(x => x.EnumCalismaTipi == (int)Sabis.Enum.EnumCalismaTip.Odev ||
                            x.EnumCalismaTipi == (int)Sabis.Enum.EnumCalismaTip.PerformanGoreviLabaratuvar ||
                            x.EnumCalismaTipi == (int)Sabis.Enum.EnumCalismaTip.PerformansGoreviAraziCalismasi ||
                            x.EnumCalismaTipi == (int)Sabis.Enum.EnumCalismaTip.PerformansGoreviAtolye ||
                            x.EnumCalismaTipi == (int)Sabis.Enum.EnumCalismaTip.PerformansGoreviSeminer ||
                            x.EnumCalismaTipi == (int)Sabis.Enum.EnumCalismaTip.PerformansGoreviUygulama
                            ).ToList();
            return View(payList);
        }

        public ActionResult _pGrupDersDetaylari(int? id, bool redirect = false)
        {
            DersDetayVM ders = Sabitler.GetirDersDetay(id.Value, Sabitler.KullaniciID);

            if (ders == null || id == null) // kullanıcının yetkisi yoksa veya id değeri null ise
            {
                HttpContext.Response.Redirect("/");
            }
            else
            {
                if (!GENELMAIN.DersGrupHocaMi(id.Value, Sabitler.KullaniciID))
                {
                    if (ders.HiyerarsikKokID.HasValue && !(Sabis.Bolum.Bll.SOURCE.EBS.GENEL.GENELMAIN.DersKoordinatoruMu(ders.HiyerarsikKokID.Value, Sabitler.KullaniciID)))
                    {
                        HttpContext.Response.Redirect("/");
                    }
                    if (!ders.HiyerarsikKokID.HasValue && !(Sabis.Bolum.Bll.SOURCE.EBS.GENEL.GENELMAIN.DersKoordinatoruMu(ders.DersPlanAnaID, Sabitler.KullaniciID)))
                    {
                        HttpContext.Response.Redirect("/");
                    }
                }

                if (
                    ders.EnumKokDersTipi == (int)Sabis.Enum.EnumKokDersTipi.BITIRME ||
                    ders.EnumKokDersTipi == (int)Sabis.Enum.EnumKokDersTipi.STAJ ||
                    ders.EnumKokDersTipi == (int)Sabis.Enum.EnumKokDersTipi.TASARIM ||
                    ders.EnumKokDersTipi == (int)Sabis.Enum.EnumKokDersTipi.UZMANLIKALANI ||
                    ders.EnumKokDersTipi == (int)Sabis.Enum.EnumKokDersTipi.ISYERI_UYGULAMASI ||
                    ders.EnumKokDersTipi == (int)Sabis.Enum.EnumKokDersTipi.SEMINER ||
                    ders.EnumKokDersTipi == (int)Sabis.Enum.EnumKokDersTipi.ENSTITU_PROJE ||
                    ders.EnumKokDersTipi == (int)Sabis.Enum.EnumKokDersTipi.TIP_FAZ3_STAJ)
                {
                    if (redirect == true)
                    {
                        TempData["EnumKokDersTipi"] = ders.EnumKokDersTipi;
                        HttpContext.Response.Redirect("/Ders/BasariNotGirisi/Grup/" + id);
                    }
                }
            }

            return PartialView(ders);
        }

        public ActionResult _pNotGirisTarihAraliklari(int id)
        {
            return PartialView(DERSMAIN.GetirDersSinavTarihleri(id, Sabitler.Yil, Sabitler.Donem, Sabitler.UniversiteID));
        }

        public ActionResult _pGrupPaylar(int id)
        {
            if (Sabitler.Donem == (int)Sabis.Enum.EnumDonem.YAZ)
            {
                PAYMAIN.YazOkuluPayAktar(id);
            }
            return PartialView(PAYMAIN.GetirPayBilgileriv2(id, Sabitler.KullaniciAdi, Sabitler.IPAdresi));
        }

        public ActionResult SecilenleriKaydet(string secilenPayID, string secilenPayAD, string secilenDurum, string EnumCalismaTipi)
        {
            TempData["degerlerAd"] = secilenPayAD;
            TempData["degerler"] = secilenPayID;
            TempData["degerDurum"] = secilenDurum.ToLower().Replace("false", "disabled").Replace("true", "");
            TempData["EnumCalismaTip"] = EnumCalismaTipi;
            return Json("");
        }

        public JsonResult DersYayinlaJson(int payID, int dersGrupID)
        {
            return Json(PAYMAIN.YayinlaPay(payID, dersGrupID, Sabitler.KullaniciAdi, true));
        }
        public JsonResult DersYayindanKaldirJson(int payID, int dersGrupID)
        {
            return Json(PAYMAIN.YayindanKaldirPay(payID, dersGrupID));
        }
        #endregion

        #region ANA DERS

        public ActionResult Ana()
        {
            return View();
        }

        public ActionResult _pAnaDersDetaylari(int? id)
        {
            List<UstDersViewModel> ders =_ders.ListelePersonelDersleriv2(Sabitler.KullaniciID, Sabitler.Yil, Sabitler.Donem);


            UstDersViewModel dp = null;
            if (ders == null || id == null) // kullanıcının yetkisi yoksa veya id değeri null ise
            {
                HttpContext.Response.Redirect("/");
            }
            else
            {
                if (ders.FirstOrDefault(d => d.DersPlanAnaID == id) != null)
                {
                    dp = ders.FirstOrDefault(d => d.DersPlanAnaID == id);
                }
                else
                {
                    foreach (var item in ders)
                    {
                        if (item.AltDersList != null)
                        {
                            foreach (var itemAlt in item.AltDersList)
                            {
                                if (itemAlt.DersPlanAnaID == id)
                                {
                                    dp = itemAlt;
                                }
                            }
                        }
                    }
                }
            }

            return PartialView(dp);
        }

        public ActionResult _pAnaAltDersler(int id)
        {
            try
            {
                return PartialView(Sabis.Bolum.Bll.SOURCE.ABS.DERS.DERSMAIN.ListeleAnaDersDetay(Sabitler.KullaniciID, Sabitler.Yil, Sabitler.Donem, id));
            }
            catch (Exception hata)
            {
                throw hata;
            }
        }

        public ActionResult _pAnaPaylar(int id)
        {
            return PartialView(Sabis.Bolum.Bll.SOURCE.ABS.DERS.PAYMAIN.ListelePayHiyerarsik(id, Sabitler.Yil, Sabitler.Donem));
        }

        public JsonResult AnaAltDersVeGruplariPayIslem(PayVM payBilgi, bool durum)
        {
            if (durum)
            {
                Sabis.Bolum.Bll.SOURCE.ABS.DERS.PAYMAIN.PayYayinlaDersplanAnaAltDersVeGruplari(payBilgi);
            }
            else
            {
                Sabis.Bolum.Bll.SOURCE.ABS.DERS.PAYMAIN.PayYayindanKaldirDersPlanAnaAltDersVeGruplari(payBilgi);
            }
            return Json("true");
        }
        #endregion
    }
}