﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Sabis.Bolum.Bll.SOURCE.ABS.NOT;
using System.IO;
using OfficeOpenXml;

namespace Sabis.Bolum.WebUI.Areas.Ders.Controllers
{
    public class ExcelNotGirisController : Controller
    {
        // GET: Ders/ExcelNotGiris
        public ActionResult Index(int id)
        {
            return View(HIYERARSIKMAIN.PayGetir(id, Sabitler.Yil, Sabitler.Donem));
        }

        public ActionResult _pGetirDersList(int id)
        {
            return PartialView(Sabis.Bolum.Bll.SOURCE.ABS.DERS.DERSMAIN.ListeleAnaDersDetay(Sabitler.KullaniciID, Sabitler.Yil, Sabitler.Donem, id));
        }

        public JsonResult GetirPayList(int id)
        {
            return Json(HIYERARSIKMAIN.PayGetir(id, Sabitler.Yil, Sabitler.Donem));
        }

        public ActionResult _pExcelYukle()
        {
            return PartialView();
        }

        public JsonResult PayEslestir(List<PayIDList> secilenPaylar, int FKDersPlanAnaID)
        {

            var ogrenciListe = HIYERARSIKMAIN.GetirOgrenciYazilmaByDersPlanAnaID(FKDersPlanAnaID, Sabitler.Yil, Sabitler.Donem);
            //var payDict = Sabis.Bolum.Bll.SOURCE.ABS.DERS.PAYMAIN.PayGetir(secilenPaylar.Select(x => x.payID.Value).ToList());
            List<Sabis.Bolum.Core.ABS.NOT.NotKayitVM> liste = new List<Sabis.Bolum.Core.ABS.NOT.NotKayitVM>();

            var fileBytes = (byte[])Session["ExcelFile"];
            using (var reader = new MemoryStream(fileBytes))
            {
                using (var package = new ExcelPackage(reader))
                {
                    var currentSheet = package.Workbook.Worksheets;
                    var workSheet = currentSheet.First();
                    var noOfCol = workSheet.Dimension.End.Column;
                    var noOfRow = workSheet.Dimension.End.Row;

                    foreach (var item in secilenPaylar.Where(x => x.payID > 0))
                    {
                        if (item.sira == 0)
                        {
                            continue;
                        }
                        for (int i = 2; i <= noOfRow; i++)
                        {
                            Sabis.Bolum.Core.ABS.NOT.NotKayitVM yeniNot = new Sabis.Bolum.Core.ABS.NOT.NotKayitVM();
                            if (workSheet.Cells[i, item.sira].Any() && workSheet.Cells[i, item.sira].Any())
                            {
                                //Sabis.Core.Utils.OgrenciNo.OgrenciNoKullaniciAdiCevir(workSheet.Cells[i, 1].Value.ToString());


                                string numara = workSheet.Cells[i, secilenPaylar.FirstOrDefault().sira].Value.ToString();
                                //string numara = workSheet.Cells[i, 2].Value.ToString();
                                if (workSheet.Cells[i, item.sira].Value != null)
                                {
                                    yeniNot.Notu = workSheet.Cells[i, item.sira].Value.ToString();
                                    if (String.IsNullOrEmpty(yeniNot.Notu))
                                    {
                                        continue;
                                    }
                                }
                                else
                                {
                                    continue;
                                }

                                var yazilma = ogrenciListe.FirstOrDefault(x => x.Numara == numara.Insert(4, "."));
                                yeniNot.OgrenciID = yazilma.OgrenciID;
                                yeniNot.PayID = item.payID;
                                yeniNot.KaydedenKulanici = Sabitler.KullaniciAdi;
                                yeniNot.Yil = Sabitler.Yil;
                                yeniNot.Donem = Sabitler.Donem;
                                yeniNot.DersPlanID = item.FKDersplanID;
                                yeniNot.DersPlanAnaID = item.FKDersPlanAnaID;
                                yeniNot.DersGrupID = item.FKDersGrupID;

                                //Core.ViewModel.NotGiris.NotListeDTO yeniNot = new Core.ViewModel.NotGiris.NotListeDTO();
                                //yeniNot.Numara = workSheet.Cells[i, item.sira].Value.ToString();
                                //yeniNot.Notu = workSheet.Cells[i, item.sira].Value.ToString();
                                //notListe.Add(yeniNot);
                            }
                            if (yeniNot.OgrenciID != 0 && yeniNot.DersPlanAnaID > 0)
                            {
                                liste.Add(yeniNot);
                            }
                        }
                    }
                }
            }

            Session["ExcelFile"] = null;
            var notKayitSonuc = NOTMAIN.NotKaydet(liste, Sabitler.IPAdresi, Sabitler.KullaniciAdi);
            //TODO
            return Json("Ok", JsonRequestBehavior.AllowGet);
        }

        public class PayIDList
        {
            public int payID { get; set; }
            public int sira { get; set; }
            public int FKDersPlanAnaID { get; set; }
            public int? FKDersplanID { get; set; }
            public int? FKDersGrupID { get; set; }
        }
        public class ExcelFileModel
        {
            public HttpPostedFileBase ExcelFile { get; set; }
        }
        public class KolonList
        {
            public int SiraNo { get; set; }
            public string KolonAdi { get; set; }
        }
        public JsonResult ExcelUpload(FormCollection formCollection)
        {


            //Session["NotGirisExcelDosya"] = Request.Files["ExcelFile"];


            //List <string> kolonListesi = new List<string>();
            List<KolonList> kolonListesi = new List<KolonList>();
            //var ogrenciListesi = new List<OgrenciListesiViewModel>();
            if (Request != null)
            {
                HttpPostedFileBase file = Request.Files["ExcelFile"];
                Session["ExcelFile"] = file.ToByteArray();

                if ((file != null) && (file.ContentLength > 0) && !string.IsNullOrEmpty(file.FileName))
                {
                    string fileName = file.FileName;
                    string fileContentType = file.ContentType;
                    byte[] fileBytes = new byte[file.ContentLength];
                    var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));

                    using (var package = new OfficeOpenXml.ExcelPackage(file.InputStream))
                    {
                        var currentSheet = package.Workbook.Worksheets;
                        var workSheet = currentSheet.First();
                        var noOfCol = workSheet.Dimension.End.Column;
                        var noOfRow = workSheet.Dimension.End.Row;



                        for (int i = 1; i <= noOfCol; i++)
                        {
                            KolonList yeniKolon = new KolonList();
                            yeniKolon.SiraNo = i;
                            yeniKolon.KolonAdi = workSheet.Cells[1, i].Value.ToString();
                            kolonListesi.Add(yeniKolon);
                        }


                        //workSheet.Cells[1, 2];
                        for (int i = 2; i <= noOfRow; i++)
                        {

                            //var ogrenci = new OgrenciListesiViewModel();
                            //ogrenci.Numara = workSheet.Cells[i, 1].Value.ToString();
                            //ogrenci.Ad = workSheet.Cells[i, 2].Value.ToString();

                            //for (int j = 3; j <= noOfCol; j++)
                            //{
                            //    var yeniNot = new OgrenciNotlariViewModel();
                            //    yeniNot.Notu = workSheet.Cells[i, j].Value != null ? workSheet.Cells[i, j].Value.ToString() : "";
                            //    yeniNot.PayID = Convert.ToInt32(System.Text.RegularExpressions.Regex.Match(workSheet.Cells[1, j].Value.ToString(), @"(?<=\().+?(?=\))").Value);

                            //    ogrenci.OgrenciNotListesi.Add(yeniNot);
                            //}
                            //ogrenciListesi.Add(ogrenci);
                        }
                    }
                }
            }
            return Json(kolonListesi);
        }
    }

    public static class HttpPostedFileBaseExtensions
    {
        public static Byte[] ToByteArray(this HttpPostedFileBase value)
        {
            if (value == null)
                return null;
            var array = new Byte[value.ContentLength];
            value.InputStream.Position = 0;
            value.InputStream.Read(array, 0, value.ContentLength);
            return array;
        }
    }
}