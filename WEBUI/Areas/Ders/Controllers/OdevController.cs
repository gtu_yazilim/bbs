﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Sabis.Bolum.Core.ABS.DERS.ODEV;
using Sabis.Bolum.Bll.SOURCE.ABS.DERS;
using Ionic.Zip;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using Sabis.Bolum.Core.ABS.DERS.DERSDETAY;

namespace Sabis.Bolum.WebUI.Areas.Ders.Controllers
{
    [AllowAnonymous]
    public class OdevController : Controller
    {
        // GET: Ders/Odev
        public ActionResult Duzenle()
        {
            return View();
        }

        public ActionResult _pDuzenle()
        {
            return PartialView();
        }

        public ActionResult Guncelle(int id, int P)
        {
            return View(ODEVMAIN.GetirOdev(P, id, Sabitler.KullaniciID));
        }

        public ActionResult _pGuncelle(int id, int P)
        {
            return View(ODEVMAIN.GetirOdev(P, id, Sabitler.KullaniciID));
        }

        public JsonResult YuklemeHakkiVer(int odevDosyaID, int fkDersGrupHocaID, string aciklama)
        {
            var dersDetay = Sabitler.GetirDersDetay(fkDersGrupHocaID);

            DuyuruVM Duyuru = new DuyuruVM();
            Duyuru.FKDersGrupID = dersDetay.DersGrupID;
            Duyuru.FKPersonelID = Sabitler.KullaniciID;
            Duyuru.EkleyenIP = Sabitler.IPAdresi;
            Duyuru.Konu = "Ödeviniz için yeni yükleme hakkı verildi.";
            Duyuru.Mesaj = dersDetay.DersAd + " dersindeki ödeviniz için yeni ödev yükleme hakkı tanımlandı. Açıklama: " + aciklama;
            Duyuru.Tarih = DateTime.Now;
            Duyuru.Yil = Sabitler.Yil;
            Duyuru.Donem = Sabitler.Donem;


            return Json(ODEVMAIN.YuklemeHakkiVer(odevDosyaID, Sabitler.KullaniciAdi, Sabitler.IPAdresi, fkDersGrupHocaID, Duyuru));
        }

        public JsonResult OdevOlustur(OdevVM odev)
        {
            return Json(ODEVMAIN.OdevOlustur(odev, Sabitler.Yil, Sabitler.Donem, Sabitler.KullaniciID));
        }

        public ActionResult _pGetirOdev(int id, int P) //dersGrupHocaID, PayID
        {
            return PartialView(ODEVMAIN.GetirOdev(P, id, Sabitler.KullaniciID));
        }

        public JsonResult OdevGuncelle(OdevVM odev)
        {
            return Json(ODEVMAIN.OdevGuncelle(odev));
        }

        public JsonResult OdevSil(int id, int P)
        {
            return Json(ODEVMAIN.OdevSil(id, P, Sabitler.KullaniciAdi, Sabitler.IPAdresi));
        }

        public JsonResult OdevTuruDegistir(int fkAbsPaylarID, int fkDersGrupID, int enumSinavTuru)
        {
            return Json(ODEVMAIN.OdevTuruDegistir(fkAbsPaylarID, fkDersGrupID, enumSinavTuru));
        }

        public ActionResult _pOdevGrupListesi(int id)
        {
            return PartialView(ODEVMAIN.GetirOdevGrupListesi(id));
        }

        public JsonResult GrupEkleSil(int FKOdevID, int FKDersGrupID, bool durum)
        {
            return Json(ODEVMAIN.GrupEkleSil(FKOdevID, FKDersGrupID, durum));
        }

        public JsonResult YeniSoruEkle(int fkAbsPaylarID)
        {
            return Json(ODEVMAIN.YeniSoruEkle(fkAbsPaylarID, Sabitler.KullaniciID));
        }
        public JsonResult SoruSil(int fkAbsPaylarOlcmeID)
        {
            return Json(ODEVMAIN.SoruSil(fkAbsPaylarOlcmeID, Sabitler.KullaniciAdi, Sabitler.IPAdresi));
        }

        public JsonResult SoruPuanGuncelle(int fkAbsPaylarOlcmeID, double SoruPuan)
        {
            return Json(ODEVMAIN.SoruPuanGuncelle(fkAbsPaylarOlcmeID, SoruPuan));
        }

        public ActionResult Indir(string key)
        {
            var url = SDFSClient.FileClient.GetUrl(key);
            return Redirect(url);
        }
        [AllowAnonymous]
        public FileStreamResult TopluIndir(int id, int P) //string[][] dosyaUrlList
        {
            var dersDetay = Sabitler.GetirDersDetay(id);
            string grupTamAdi = dersDetay.DersAd + "-" + Sabis.Enum.Utils.GetEnumDescription((Sabis.Enum.EnumOgretimTur)dersDetay.EnumOgretimTur) + "-" + dersDetay.GrupAd + "-grubu";
            //grupTamAdi = grupTamAdi.ToLower().Replace("ı", "i").Replace("ğ", "g").Replace("ü", "u").Replace("ş", "s").Replace("ö", "o").Replace("ç", "c").Replace(" ", "-").Replace(".", "");

            string folderName = "Odevler-" + grupTamAdi;

            var odev = ODEVMAIN.GetirOdev(P, id, Sabitler.KullaniciID);
            Stream fileMS = new MemoryStream();

            using (ZipFile zip = new ZipFile())
            {
                foreach (var dosya in odev.OdevOgrenciListesi)
                {
                    byte[] fileBytes;
                    using (WebClient wc = new WebClient())
                    {
                        Uri myUri = new Uri(SDFSClient.FileClient.GetUrl(dosya.DosyaKey), UriKind.Absolute);

                        var newUri = new UriBuilder(myUri);
                        newUri.Host = "dl.local.apollo.gtu.edu.tr";

                        fileBytes = wc.DownloadData(newUri.ToString());
                    }
                    Stream stream = new MemoryStream(fileBytes);
                    zip.AddEntry(folderName + "/" + dosya.OgrenciNumara + " - " + dosya.OgrenciAdSoyad + " - " + dosya.DosyaAdi, stream);
                }
                zip.Save(fileMS);

                fileMS.Seek(0, SeekOrigin.Begin);
                fileMS.Flush();
            }
            return File(fileMS, "application/zip", folderName + ".zip");
        }
    }
}