﻿using System.Web.Mvc;

namespace Sabis.Bolum.WebUI.Areas.Ders
{
    public class DersAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Ders";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Ders_default",
                "Ders/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new[] { "Sabis.Bolum.WebUI.Areas.Ders.Controllers" }
            );
        }
    }
}