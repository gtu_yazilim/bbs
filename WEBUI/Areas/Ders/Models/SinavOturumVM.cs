﻿using Sabis.Bolum.Core.ABS.DERS.OSINAV;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sabis.Bolum.WebUI.Areas.Ders.Models
{
    public class SinavOturumVM
    {
        public int grupHocaID { get; set; }
        public int SinavID { get; set; }
        public string SinavAd { get; set; }
        public int? GrupID { get; set; }
        public int PayID { get; set; }
        public bool koordinator { get; set; }
        public List<SinavOturumDto> Oturumlar { get; set; }
        public IEnumerable<SelectListItem> Gruplar { get; set; }

        public int OgrenciNumara { get; set; }
        public string OgrenciNo { get; set; }
        public int id { get; set; }
        public bool k { get; set; }
        public int? Value { get; set; }

        public bool SorunBildir { get; set; }
    }
}