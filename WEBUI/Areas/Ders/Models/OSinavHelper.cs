﻿using Sabis.Abs.Bll.DATA;
using Sabis.Abs.Core.ABS.DERS.OSINAV;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sabis.Abs.WebUI.Areas.Ders.Models
{
    public static class OSinavHelper
    {
        //public static string PayAdiGetir(int? payID)
        //{
            
        //    UYSv2Entities db = new UYSv2Entities();
        //    var pay = db.ABSPaylar.Where(p => p.ID == payID).FirstOrDefault();
        //    return pay.Sira + ". " + Sabis.Enum.Utils.GetEnumDescription((Sabis.Enum.EnumCalismaTip)pay.EnumCalismaTip);
        //}
        //public static List<OSinavHocaGruplar> HocayaAitSiniflar(int GroupID, int payid)
        //{
        //    UYSv2Entities db = new UYSv2Entities();
        //    var cid = db.OGRDersGrup.Where(g => g.ID == GroupID).FirstOrDefault().FKDersPlanID;
        //    var mcid = db.OGRDersPlan.Where(c => c.ID == cid).FirstOrDefault().FKDersPlanAnaID;
        //    var personelid = db.OGRDersGrupHoca.Where(h => h.FKDersGrupID == GroupID).FirstOrDefault().FKPersonelID;
        //    var pid = payid;

        //    var sorgu = (from grup in db.OGRDersGrup
        //                 join anaders in db.OGRDersPlanAna on grup.FKDersPlanAnaID equals anaders.ID
        //                 join gruphoca in db.OGRDersGrupHoca on grup.ID equals gruphoca.FKDersGrupID            
        //                 join sg in db.OSinavGrup on grup.ID equals sg.FKDersGrupID into tbl1
        //                 from sg in tbl1.Where(f => f.PayID == pid).DefaultIfEmpty()
        //                 where
        //                     anaders.ID == mcid &&
        //                     gruphoca.FKPersonelID == personelid &&
        //                     grup.OgretimYili == Sabitler.Yil &&
        //                     grup.EnumDonem == Sabitler.Donem &&
        //                     grup.Acik == true &&
        //                     grup.Silindi == false
        //                 select new OSinavHocaGruplar
        //                 {
        //                     FKDersGrupID = gruphoca.FKDersGrupID,
        //                     FKPersonelID = gruphoca.FKPersonelID,
        //                     GrupAd = grup.GrupAd,
        //                     OgretimTuru = grup.EnumOgretimTur,
        //                     Selected = sg.FKDersGrupID == null ? false : true,
        //                     SelectedOld  = sg.FKDersGrupID == null ? false : true
        //                 }).ToList();
        //    return sorgu;
        //}
        //public static void SinavGrupEkle(OSinavEkle vm, int? sinavID)
        //{
        //    UYSv2Entities db = new UYSv2Entities();

        //    var dersID = db.OGRDersGrup.Where(g => g.ID == vm.GrupID).FirstOrDefault().FKDersPlanID;
        //    var anadersID = db.OGRDersPlan.Where(d => d.ID == dersID).FirstOrDefault().FKDersPlanAnaID;

        //    var gruplar = vm.hocagruplar.Where(x => x.Selected && !x.SelectedOld).Select(x =>
        //                  new OSinavGrup()
        //                  {
        //                      SinavID = sinavID.Value,
        //                      FKDersPlanAnaID = anadersID.Value,
        //                      FKDersPlanID = dersID.Value,
        //                      FKDersGrupID = x.FKDersGrupID.Value,
        //                      PayID = vm.PayID
        //                  }
        //                   ).ToList();

        //    if (gruplar.Count() > 0)
        //    {
        //        db.OSinavGrup.AddRange(gruplar);
        //        db.SaveChanges();
        //    }
        //}

        //public static int SoruNoBul(int sinavid)
        //{
        //    UYSv2Entities db = new UYSv2Entities();

        //    var query = from s in db.OSinavSoru
        //                where
        //                s.SinavID == sinavid &&
        //                !s.Silindi.Value
        //                select s.SoruNo;
        //    if (query.Count() > 0)
        //    {
        //        return query.Max() + 1;
        //    }
        //    return 1;
        //}
    }
}