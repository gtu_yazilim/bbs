﻿using Sabis.Bolum.Core.ABS.DERS.PROJE;
using Sabis.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sabis.Bolum.Core.ABS.NOT;

namespace Sabis.Bolum.WebUI.Areas.Ders.Models
{
    public class OgrenciBasariNotGirisVM : OgrenciListesiVM
    {
        public OgrenciBasariNotGirisVM()
        {
            //BasariNotlari = Sabis.Enum.Utils.ConvertEnumToList(typeof(EnumHarfBasari))
                //.Where(d => d.ID == (int)Sabis.Enum.EnumHarfBasari.BELIRSIZ ||
                //            d.ID == (int)Sabis.Enum.EnumHarfBasari.AA);//new EnumDTO[] { };
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder(base.ToString());
            foreach (EnumDTO ed in BasariNotlari)
                sb.AppendLine(ed.ToString());

            return sb.ToString();
        }

        public IEnumerable<EnumDTO> BasariNotlari { get; set; }
        public string YonetmelikAciklama { get; set; }
    }
}
