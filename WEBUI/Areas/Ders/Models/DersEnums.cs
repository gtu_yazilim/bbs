﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sabis.Bolum.WebUI.Areas.Ders.Models
{
    public class DersEnums
    {
        public enum Mailsonuc
        {
            basarili_gonderildi = 2,
            ders_hoca_id_bos_geldi = -2,
            islem_hata = -5
        }
    }
}