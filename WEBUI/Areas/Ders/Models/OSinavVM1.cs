﻿using Sabis.Abs.Bll.DATA;
using Sabis.Abs.Core.ABS.DERS.DERSDETAY;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sabis.Abs.WebUI.Areas.Ders.Models
{
    public class OSinavVM1
    {  
        public int SinavID { get; set; }
        [Required]
        public string Ad { get; set; }
        public string Aciklama { get; set; }
        [Required]
        public Nullable<System.DateTime> Baslangic { get; set; }
        [Required]
        public Nullable<System.DateTime> Bitis { get; set; }
        [Required]
        public int? Sure { get; set; }
        [Required]
        public int? SoruSayisi { get; set; }
        public bool Yayinlandi { get; set; }
        public string Ekleyen { get; set; }
        public Nullable<int> EkleyenID { get; set; }
        public Nullable<System.DateTime> EklemeTarihi { get; set; }
        public Nullable<int> GuncelleyenID { get; set; }
        public string Guncelleyen { get; set; }
        public Nullable<System.DateTime> GuncellemeTarihi { get; set; }


        public int GrupID { get; set; }
        public int PayID { get; set; }

        [Required]
        public IEnumerable<HocaGruplar> hocagruplar { get; set; }
    }
    public class HocaGruplar
    {
        [Required]
        public int? FKDersGrupID { get; set; }
        public int? FKPersonelID { get; set; }
        public string GrupAd { get; set; }
        public int OgretimTuru { get; set; }
        [Required]
        public bool Selected { get; set; }

        public bool SelectedOld { get; set; }
    }
    public class OSinavGrup_Pay
    {
        public PayBilgileriVM paybilgi { get; set; }
        public OSinavGrup sinavgrup { get; set; }
    }
}