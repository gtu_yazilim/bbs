﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Sabis.Bolum.Bll.SOURCE.ADMIN;

namespace Sabis.Bolum.WebUI.Areas.Admin.Controllers
{
    [AllowAnonymous]
    public class IstatistikController : Controller
    {
        // GET: Admin/Istatistik
        public ActionResult Index()
        {
            return View(ISTATISTIKMAIN.GetirIstatistikler(Sabitler.Yil, Sabitler.Donem));
        }

        public ActionResult Dokuman()
        {
            return View(ISTATISTIKMAIN.GetirDokumanRapor(Sabitler.Yil, Sabitler.Donem));
        }
    }
}