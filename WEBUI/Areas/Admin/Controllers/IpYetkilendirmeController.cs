﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Sabis.Bolum.Core.ADMIN;
using Sabis.Bolum.Bll.SOURCE.ADMIN;

namespace Sabis.Bolum.WebUI.Areas.Admin.Controllers
{
    public class IpYetkilendirmeController : Controller
    {
        // GET: Admin/IpYetkilendirme
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult _pGetirIPYetkiListesi()
        {
            return PartialView(IPMAIN.GetirIPYetkiListesi());
        }

        public ActionResult _pYeniYetkiEkle()
        {
            return PartialView();
        }
        public JsonResult EkleIPYetki(IPYetkiVM yeniYetki)
        {
            return Json(IPMAIN.EkleIPYetki(yeniYetki));
        }

        public JsonResult SilIPYetki(int id)
        {
            return Json(IPMAIN.SilIpYetki(id));
        }
    }
}