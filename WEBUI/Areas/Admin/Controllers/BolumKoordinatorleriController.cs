﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Sabis.Bolum.Core.EBS.BIRIM;
using Sabis.Bolum.Bll.SOURCE.EBS.BIRIM;

namespace Sabis.Bolum.WebUI.Areas.Admin.Controllers
{
    public class BolumKoordinatorleriController : Controller
    {
        // GET: Admin/BolumKoordinatorleri
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult _pBolumKoordinatorEkle()
        {
            return PartialView();
        }

        public JsonResult GetirKoordinatorListesi(int fakulteID)
        {
            var koordinatorListe = BOLUMMAIN.GetirBolumKoordinatorListesi(fakulteID);

            koordinatorListe = koordinatorListe.Select(x => new BolumKoordinatorVM
            {
                ID = x.ID,
                YetkiGrupID = x.YetkiGrupID,
                YetkiAd = x.YetkiGrupID.ToString().Replace("2", "Fakülte Yetkilisi").Replace("3", "Bölüm Yetkilisi").Replace("10", "Bölüm Başkanı").Replace("13", "Ana Bilim Dalı Başkanı"),
                AdSoyad = x.AdSoyad,
                FKPersonelID = x.FKPersonelID,
                YetkiBaslangicTarihi = x.YetkiBaslangicTarihi,
                YetkiBitisTarihi = x.YetkiBitisTarihi,
                Zaman = x.Zaman,
                FakulteID = x.FakulteID,
                BolumID = x.BolumID,
                FKProgramBirimID = x.FKProgramBirimID,
                YetkiliBirimAd = Sabis.Client.BirimIslem.Instance.getirBagliFakulte(x.FakulteID.Value).BirimAdi + " / " + Convert.ToString(x.BolumID.HasValue && x.BolumID != 0 ? Sabis.Client.BirimIslem.Instance.getirBagliBolum(x.BolumID.Value).BirimAdi : ""),
                IdariMi = x.IdariMi
            }).ToList();

            return Json(koordinatorListe);
        }

        public JsonResult GuncelleBolumKoordinator(int? id, int FKPersonelID, int? BolumID)
        {
            return Json(BOLUMMAIN.GuncelleBolumKoordinator(id, FKPersonelID, BolumID, Sabitler.KullaniciAdi));
        }

        public JsonResult EkleBolumKoordinator(int FakulteID, int BolumID, int? ProgramID, int? AnaBilimDaliID, int FKPersonelID)
        {
            return Json(BOLUMMAIN.EkleBolumKoordinator(FakulteID, BolumID, ProgramID, AnaBilimDaliID, FKPersonelID, Sabitler.KullaniciAdi));
        }

        public JsonResult SilBolumKoordinator(int id)
        {
            return Json(BOLUMMAIN.SilBolumKoordinator(id));
        }
    }
}