﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sabis.Bolum.WebUI.Areas.Admin.Controllers
{
    [AllowAnonymous]
    public class SimulasyonController : Controller
    {
        // GET: Admin/Simulasyon
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult SimuleEt(string kullaniciAdi)
        {
            Session["simulasyon"] = kullaniciAdi;
            Session["simuleEden"] = (User.Identity as SSOClient.AuthModel.SSOGenericIdentity).SSOTicket.KullaniciAdi;
            return Json("");
        }

        public JsonResult SimulasyonSonlandir()
        {
            Session["simulasyon"] = null;
            Session["simuleEden"] = null;
            return Json("");
        }
        [AllowAnonymous]
        public JsonResult DilDegistir(string lang)
        {
            HttpCookie cookie = Request.Cookies["dil"];
            if (cookie != null)
            {
                Response.Cookies["dil"].Value = lang;
                HttpCookie cookie1 = Request.Cookies["dil"];
            }
            return Json("");
        }
    }
}