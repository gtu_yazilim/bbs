﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sabis.Bolum.WebUI.Areas.Admin.Controllers
{
    [AllowAnonymous]
    public class DersKoordinatorleriController : Controller
    {
        // GET: Admin/DersKoordinator
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetirBirimList(int? ustBirimID)
        {
            return Json(Sabis.Bolum.Bll.SOURCE.GENEL.GENELMAIN.GetirBirimAgaci(ustBirimID));
        }

        public JsonResult GetirBirimDersList(int FKBirimID, int? FKProgramID)
        {
            return Json(Sabis.Bolum.Bll.SOURCE.EBS.BIRIM.BOLUMMAIN.GetirBolumDersListesi(FKBirimID, Sabitler.EBSYil, Sabitler.EBSDonem, Sabitler.KullaniciID, FKProgramID));
        }
        public JsonResult GetirBirimDersListDonemBazli(int FKBirimID, int? FKProgramID)
        {
            return Json(Sabis.Bolum.Bll.SOURCE.EBS.BIRIM.BOLUMMAIN.GetirBolumDersListesiDonemBazli(FKBirimID, Sabitler.EBSYil, Sabitler.EBSDonem, Sabitler.KullaniciID, FKProgramID));
        }

        public JsonResult GetirPersonelBilgi(string aranan, bool kullaniciAdiGetir = false)
        {
            if (kullaniciAdiGetir)
            {
                if (Sabitler.KullaniciID == 1108 || Sabitler.KullaniciID == 973 || Sabitler.KullaniciID == 348)
                {
                    return Json(Sabis.Bolum.Bll.SOURCE.ABS.PERSONEL.PERSONELMAIN.AraPersonel(aranan, Sabis.Client.BirimIslem.Instance.getirAltBirimler(339).Select(x=> x.ID).ToList(), Sabitler.Yil, Sabitler.Donem).Select(c => new { Value = c.KullaniciAdi, Text = c.UnvanAdSoyad }), JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(Sabis.Bolum.Bll.SOURCE.ABS.PERSONEL.PERSONELMAIN.AraPersonel(aranan, new List<int>(), Sabitler.EBSYil, Sabitler.EBSDonem).Select(c => new { Value = c.KullaniciAdi, Text = c.UnvanAdSoyad }), JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(Sabis.Bolum.Bll.SOURCE.ABS.PERSONEL.PERSONELMAIN.AraPersonel(aranan, new List<int>(), Sabitler.EBSYil, Sabitler.EBSDonem).Select(c => new { Value = c.ID, Text = c.UnvanAdSoyad }), JsonRequestBehavior.AllowGet);
            }
            
        }

        public JsonResult DersKoordinatorDegistir(int FKDersPlanAnaID, int FKPersonelID)
        {
            return Json(Sabis.Bolum.Bll.SOURCE.EBS.DERS.DERSMAIN.GuncelleDersKoordinator(FKDersPlanAnaID, FKPersonelID, Sabitler.KullaniciAdi));
        }
    }
}