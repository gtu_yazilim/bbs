﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sabis.Bolum.WebUI.Areas.Anket.Controllers
{
    public class ListeController : Controller
    {
        private bool OylamaAcik()
        {
            return DateTime.Now < new DateTime(2017, 7, 1, 23, 59, 59);
        }

        // GET: Anket/Liste
        public ActionResult Index()
        {
            ViewBag.OylamaAcik = OylamaAcik();
            return View();
        }
        //TabloYok
        //public ActionResult _Liste()
        //{
        //    return View(Sabis.Bolum.Bll.SOURCE.ABS.ANKET.ANKETMAIN.GetirAnketler(Sabitler.KullaniciID));
        //}

        public ActionResult _Oyla(int HedefPersonelID)
        {
            ViewBag.HedefPersonelID = HedefPersonelID;

            return View();
        }

        //TabloYok
        //[HttpPost]
        //public ActionResult Kaydet(FormCollection form)
        //{
        //    if (!OylamaAcik())
        //    {
        //        Sabis.Core.MessageDTO resultErr = Sabis.Core.MessageDTO.error_Add("Oylama Kapalıdır!");
        //        return Json(resultErr);
        //    }
        //    Sabis.Core.MessageDTO result = Sabis.Core.MessageDTO.success_Add();

        //    int hedefPersonelID = Convert.ToInt32(form.Get("HedefPersonelID"));

        //    if (Sabis.Bolum.Bll.SOURCE.ABS.ANKET.ANKETMAIN.GetirAnketler(Sabitler.KullaniciID).Any(x => x.Item1 == hedefPersonelID && x.Item4))
        //    {
        //        result = Sabis.Core.MessageDTO.error_Add("Bu kişiyi oylama yetkiniz yok ya da zaten oyladınız");
        //    }

        //    int puanCounter = 5;
        //    int[] puanlar = new int[5];
        //    foreach (var item in form.AllKeys)
        //    {
        //        int sira;
        //        if (int.TryParse(item.Replace("S", ""), out sira))
        //        {
        //            try
        //            {
        //                int tmpPuan = Convert.ToInt32(form[item]);
        //                if (tmpPuan < -1 || tmpPuan > 4)
        //                {
        //                    result = Sabis.Core.MessageDTO.error_Add("Hedef aralıktan başka değer girilmiş");
        //                }
        //                puanlar[sira - 1] = tmpPuan;
        //                puanCounter--;
        //            }
        //            catch (Exception)
        //            {
        //                result = Sabis.Core.MessageDTO.error_Add("Kaydetme esnasında beklenmedik bir hata oluştu");
        //            }
        //        }
        //    }

        //    if (puanCounter != 0)
        //    {
        //        result = Sabis.Core.MessageDTO.error_Add("Eksik soru cevaplanmış");
        //    }

        //    if (result.MessageType == "success")
        //    {
        //        Sabis.Bolum.Bll.SOURCE.ABS.ANKET.ANKETMAIN.KaydetAnketPuanlari(Sabitler.KullaniciID, hedefPersonelID, puanlar, Sabitler.AnketYil, Sabitler.AnketDonem);
        //    }

        //    return Json(result);
        //}
    }
}