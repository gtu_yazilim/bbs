﻿function queryString() {
    var qs = location.search.substring(1, location.search.length).replace(/(%20|\+)/g, " ");
    if (arguments.length == 0 || qs == "") return qs; else qs = "&" + qs + "&";
    return qs.substring(qs.indexOf("=", qs.indexOf("&" + arguments[0] + "=") + 1) + 1, qs.indexOf("&", qs.indexOf("&" + arguments[0] + "=") + 1));
}

function boolReplace(value)
{
    if (value == true || value == "true" || value == "True" || value == "TRUE") {
        return "checked";
    }
    else {
        return "";
    }
}

//$(document).on("keyup", ".personelAra", function () {
//    blockStatus = true;
//    $(this).autocomplete({
//        source: function (request, response) {
//            $.post("/Admin/DersKoordinatorleri/GetirPersonelBilgi", { aranan: request.term, kullaniciAdiGetir: $(this).attr("kullaniciadigetir") })
//                .success(function (data) {
//                    response($.map(data, function (item) {return {label: item.Text,value: item.Value}}));
//                    delete blockStatus;
//                });
//        },
//        messages: {
//            noResults: '',
//            results: function () { }
//        },
//        select: function (event, ui) {
//            $(this).val(ui.item.value);
//            //$(this).parent().next().children("button").attr("data-personelid", ui.item.value);
//            return ui.item.value;
//        },
//        minLength: 3
//    });
//});

//var sumTxt = function (selector) {
//    var sum = 0;
//    $(selector).each(function () {
//        sum += Number($(this).val());
//    });
//    return sum;
//}

var sumTxt = function (selector) {
    var sum = 0;
    $(selector).each(function () {
        //sum += Number($(this).val());
        var val = $(this).val().replace(',', ".");
        sum += parseFloat(val);
    });
    return sum;
}


window.htmlentities = {

    encode: function (str) {
        var buf = [];

        for (var i = str.length - 1; i >= 0; i--) {
            buf.unshift(['&#', str[i].charCodeAt(), ';'].join(''));
        }

        return buf.join('');
    },

    decode: function (str) {
        return str.replace(/&#(\d+);/g, function (match, dec) {
            return String.fromCharCode(dec);
        });
    }
};

//function minmax(value, min, max) 
//{
//    if(parseInt(value) < min || isNaN(parseInt(value))) 
//        return 0; 
//    else if(parseInt(value) > max) 
//        return 100; 
//    else return value;
//}

//function parseJsonDate(jsonDateString) {
//    return new Date(parseInt(jsonDateString.replace('/Date(', '')));
//}


$(function () {
    $('[data-toggle="tooltip"]').tooltip({
        position: { my: "top+15 center", at: "top center" }
    });
});


$.getURL = function (deger) {
    var urlBilgi = new RegExp('[\?&]' + deger + '=([^&#]*)').exec(window.location.href);
    return urlBilgi[1] || 0;
}

$(document).on("click", "[data-toggle*='modal']", function () { //, .modal, *[data-toggle='modal'], *[data-toggle='tooltip modal']
    //$("#modalBody").html('<img src="/Content/assets/global/img/loading-spinner-grey.gif" alt="" class="loading"><span> &nbsp;&nbsp;Açılıyor... </span>');
    $('#modalTitle').html($(this).attr("data-title"));
    $('#modalBody').load($(this).attr("data-source"));
    if ($(this).attr("data-size") != "") {
        $('#ModalContent').removeClass("modal-full").css('width', $(this).attr("data-size") + "%");
    }
    $('#GeneralModalBox').modal();
});

$('#GlobalModalBoxBody').on('hidden', function () {
    $(this).removeData('modal');
});

//$(".tooltips").tooltip();

$(document).ajaxStart(function (e) {
    //console.log(typeof blockStatus);
    if (typeof blockStatus != "boolean") {
        $.blockUI({ message: "Lütfen bekleyin...", css: { fontSize: '30px' } });
    }
});

$(document).ajaxComplete(function (event, xhr, settings) {
    //$("html").unblock();
    $.unblockUI();
});

toastr.options.onShown = function () { $("#pop")[0].play(); }
toastr.options.preventDuplicates = true;
toastr.options.closeButton = true;

function checkAll(bx, name) {
    var cbs = document.getElementsByTagName('input');
    for (var i = 0; i < cbs.length; i++) {
        if (cbs[i].type == 'checkbox' && (cbs[i].name == name || cbs[i].className == name)) {
            cbs[i].checked = bx.checked;
        }
    }
}

var tableToExcel = (function () {
    var uri = 'data:application/vnd.ms-excel;base64,'
      , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><?xml version="1.0" encoding="UTF-8" standalone="yes"?><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
      , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
      , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
    return function (btnID, table, name, filename) {
        if (!table.nodeType) table = document.getElementById(table)
        var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

        document.getElementById(btnID).href = uri + base64(format(template, ctx));
        document.getElementById(btnID).download = filename;
        //window.location.href = uri + base64(format(template, ctx))
        //document.getElementById(btnID).click();

    }
})();

String.prototype.capitalize = function (all) {
    if (all) {
        return this.split(' ').map(e=>e.capitalize()).join(' ');
    } else {
        return this.charAt(0).toUpperCase() + this.slice(1);
    }
}

function JSONdatefixer(value) {
    var tarih = new Date(parseInt(value.replace(/(^.*\()|([+-].*$)/g, '')));
    return tarih.getDate() + "." + Number(tarih.getMonth() + 1) + "." + tarih.getFullYear() + " " + Number(tarih.getHours() - 3) + ":00";
}

//function GetirAutoComplete(element, url, target) {
//    $(this).autocomplete({
//        source: function (request, response) {
//            $.ajax({
//                url: '/Admin/DersKoordinator/GetirPersonelBilgi',
//                data: { aranan: request.term },
//                dataType: 'json',
//                type: 'POST',
//                success: function (data) {
//                    response($.map(data, function (item) {
//                        return {
//                            label: item.Text,
//                            value: item.Value
//                        }
//                    }));
//                }
//            })
//        },
//        messages: {
//            noResults: '',
//            results: function () { }
//        },
//        select: function (event, ui) {
//            $(this).val(ui.item.label);
//            $(this).parent().next().children("button").attr("data-personelid", ui.item.value);
//            return false;
//        },
//        minLength: 3
//    });
//}