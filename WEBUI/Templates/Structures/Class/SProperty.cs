﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Sabis.Abs.WebUI.Templates.Structures.Class
{
    [Serializable]
    public class SProperty
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string TypeName { get; set; }
        public SProperty(object resource, PropertyInfo propertyInfo, SFormatProvider format)
        {
            Name = propertyInfo.Name;
            Value = propertyInfo.GetValue(resource, null) != null ?
                String.Format(format.TextFormat, propertyInfo.GetValue(resource, null)) :
                format.NullValue;
            TypeName = propertyInfo.PropertyType.FullName;
        }
        public Type PropertyType()
        {
            return Type.GetType(TypeName);
        }
    }
}