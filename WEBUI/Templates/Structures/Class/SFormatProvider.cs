﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sabis.Abs.WebUI.Templates.Structures.Class
{
    public class SFormatProvider
    {
        public string TextFormat { get; set; }
        public string NullValue { get; set; }
        public SFormatProvider(string textFormat = "{0}", string nullValue = "")
        {
            TextFormat = textFormat;
            NullValue = nullValue;
        }
    }
}