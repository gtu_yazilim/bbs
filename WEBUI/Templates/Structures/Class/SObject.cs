﻿using Sabis.Abs.WebUI.Templates.Structures.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;

namespace Sabis.Abs.WebUI.Templates.Structures.Class
{
    [Serializable]
    public class SObject
    {
        public SObject()
        {
            Properties = new List<SProperty>();
        }
        public List<SProperty> Properties { get; set; }

        public static SObject CreateWith(object resource, SPropertiesType propertiesType, Dictionary<string, SFormatProvider> dictFormat = null)
        {
            Type type = resource.GetType();

            List<PropertyInfo> props = type.GetProperties()
                .Where(m => !(m.GetGetMethod().IsVirtual))
                .ToList();

            if (propertiesType == SPropertiesType.Primitive)
            {
                props = props
                .Where(m =>
                    m.PropertyType.IsPrimitive ||
                    m.PropertyType.IsEnum ||
                    m.PropertyType == typeof(String) ||
                    m.PropertyType == typeof(Decimal))
                .ToList();
            }

            SObject newObject = new SObject();

            foreach (var prop in props)
            {
                SFormatProvider format = (dictFormat != null && dictFormat.ContainsKey(prop.PropertyType.FullName)) ?
                    dictFormat[prop.PropertyType.FullName] :
                    new SFormatProvider();

                SProperty newProperty = new SProperty(resource, prop, format);
                newObject.Properties.Add(newProperty);
            }

            return newObject;
        }
    }
}