﻿using System.Web;
using System.Web.Mvc;

using Exceptionless;

namespace Sabis.Bolum.WebUI
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            //filters.Add(new LogException());
            filters.Add(new SSOClient.SSOAuthorizeAttribute());
          
        }
    }

    //public class LogException : HandleErrorAttribute
    //{
    //    public override void OnException(ExceptionContext filterContext)
    //    {
    //        filterContext.Exception.ToExceptionless().SetException(filterContext.Exception).SetUserDescription(Sabitler.KullaniciAdi + "@sakarya.edu.tr", Sabitler.UnvanAdSoyad).SetUserIdentity(new Exceptionless.Models.Data.UserInfo
    //        {
    //            Identity = Sabitler.KullaniciAdi,
    //            Name = Sabitler.KullaniciAdi,
    //        }).Submit();
    //    }
    //}
}
