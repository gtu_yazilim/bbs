﻿using System.Web;
using System.Web.Optimization;

namespace Sabis.Bolum.WebUI
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.UseCdn = true;
            #region Global
            bundles.Add(new StyleBundle("~/bundles/global-styles").Include(
                "~/Content/assets/global/plugins/font-awesome/css/font-awesome.min.css",
                "~/Content/assets/global/plugins/simple-line-icons/simple-line-icons.min.css",
                "~/Content/assets/global/plugins/bootstrap/css/bootstrap.min.css",
                //"~/Content/assets/global/plugins/uniform/css/uniform.default.css",
                "~/Content/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css",
                "~/Content/assets/global/css/components-md.min.css",
                "~/Content/assets/global/css/plugins-md.min.css",
                "~/Content/assets/layouts/layout3/css/layout.min.css",
                "~/Content/assets/layouts/layout3/css/themes/default.min.css",
                "~/Content/assets/layouts/layout3/css/custom.min.css",
                "~/Content/assets/global/plugins/bootstrap-toastr/toastr.min.css",
                "~/Content/custom.css"
            ));

            bundles.Add(new ScriptBundle("~/bundles/global-scripts").Include(
                "~/Content/assets/global/plugins/bootstrap-toastr/toastr.min.js",
                "~/Content/assets/global/plugins/jquery-bootpag/jquery.bootpag.min.js",
                "~/Content/assets/global/plugins/js.cookie.min.js",
                "~/Content/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js",
                "~/Content/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js",
                "~/Content/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js",
                //"~/Content/assets/global/plugins/uniform/jquery.uniform.min.js",
                "~/Content/assets/global/scripts/app.min.js",
                "~/Content/assets/layouts/layout/scripts/layout.min.js",
                "~/Content/assets/layouts/global/scripts/quick-sidebar.min.js",
                "~/Content/assets/pages/scripts/ui-general.min.js",
                "~/Content/assets/global/plugins/jquery.blockui.min.js",
                "~/Content/assets/global/plugins/jquery-isotope/isotope.pkgd.min.js",
                "~/Content/assets/global/plugins/jquery-isotope/jquery.pulsate.min.js"
            ));
            #endregion

            #region jQuery FileUpload
            bundles.Add(new ScriptBundle("~/bundles/jquery-fileupload").Include(
                "~/Content/assets/global/plugins/jquery-fileupload/js/vendor/jquery.ui.widget.js",
                "~/Content/assets/global/plugins/jquery-fileupload/js/jquery.iframe-transport.js",
                "~/Content/assets/global/plugins/jquery-fileupload/js/jquery.fileupload.js"
            ));
            
            bundles.Add(new StyleBundle("~/bundles/jquery-fileupload-styles").Include(
                "~/Content/assets/global/plugins/jquery-fileupload/css/jquery.fileupload.css",
                "~/Content/assets/global/plugins/jquery-fileupload/css/jquery.fileupload-ui.css"
            ));
            #endregion

            BundleTable.EnableOptimizations = true;
        }
    }
}
