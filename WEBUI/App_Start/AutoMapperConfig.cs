﻿using AutoMapper;
using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Bolum.Core.ABS.DERS.SANALSINIF.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabis.Bolum.WebUI.App_Start
{
    public class AutoMapperConfig
    {
        public static MapperConfiguration GetConfig()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<SanalSinif, SanalSinifDto>();
                cfg.CreateMap<SanalSinifDto, SanalSinif>();
                cfg.CreateMap<SanalSinifGrupDto, SanalSinifGrup>();
                cfg.CreateMap<SanalSinifGrup, SanalSinifGrupDto>();
                cfg.CreateMap<Core.ABS.NOT.OgrenciListesiVM, WebUI.Areas.Ders.Models.OgrenciBasariNotGirisVM>();

            });
            return config;
        }
    }
}
