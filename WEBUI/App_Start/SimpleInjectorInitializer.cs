[assembly: WebActivator.PostApplicationStartMethod(typeof(Sabis.Bolum.WebUI.App_Start.SimpleInjectorInitializer), "Initialize")]

namespace Sabis.Bolum.WebUI.App_Start
{
    using System.Reflection;
    using System.Web.Mvc;

    using SimpleInjector;
    using SimpleInjector.Extensions;
    using SimpleInjector.Integration.Web;
    using SimpleInjector.Integration.Web.Mvc;
    using Core.ABS.DERS.SANALSINIF.Interface;
    using Bll.SOURCE.ABS.DERS;

    using Core.ABS.DERS.HOME;


    public static class SimpleInjectorInitializer
    {
        /// <summary>Initialize the container and register it as MVC3 Dependency Resolver.</summary>
        public static void Initialize()
        {
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();           
            InitializeContainer(container);
            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());           
            container.Verify();          
            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }
     
        private static void InitializeContainer(Container container)
        {
            var mapConfig = AutoMapperConfig.GetConfig();// Sabis.Bolum.Bll.DATA.AutoMapperConfig.GetConfig();

            container.RegisterSingleton<AutoMapper.MapperConfiguration>(mapConfig);
            container.Register<AutoMapper.IMapper>(() => mapConfig.CreateMapper(container.GetInstance));

            container.Register<ISanalSinif, SanalSinifMain>();
            container.Register<IDers, DERSMAIN>();

            //container.Register<IOgrenciSinavBasla, OgrenciSinavBasla>();
        }
    }
}