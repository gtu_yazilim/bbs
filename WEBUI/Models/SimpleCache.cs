﻿using System;
using System.Collections.Generic;

namespace Sabis.Bolum.WebUI.Models
{
    internal sealed class SimpleCache
    {
        private static readonly Dictionary<string, CacheItem> Cache = new Dictionary<string, CacheItem>();

        public void AddOrUpdate(string key, object value)
        {
            var aa = this[key];
            if (aa != null) Remove(key);

            Cache.Add(key, new CacheItem() { Value = value, Created = DateTime.Now.AddHours(1).Ticks });

        }

        public void Remove(string key)
        {
            Cache.Remove(key);
        }



        public object this[string key]
        {
            get
            {
                if (!Cache.ContainsKey(key)) return null;

                if ((Cache[key].Created + 30000) > DateTime.Now.Ticks)
                {
                    return Cache[key].Value;
                }
                Cache.Remove(key);
                return null;
            }
        }



        internal sealed class CacheItem
        {
            public object Value { get; set; }
            public long Created { get; set; }
        }
    }
}