﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Reflection;

namespace PerculusSDK.Helpers
{
    public enum VCReplay
    {
        [Display(Name = "Kapalı")]
        Close = 0,

        [Display(Name = "Açık")]
        Open = 1
    }

    public enum VCStatus
    {
        [Display(Name = "Ders Henüz Başlamadı")]
        Baslamadi = -1,
        [Display(Name = "Ders Aktif")]
        Basladi = 0,
        [Display(Name = "Dersin Süresi Geçmiş")]
        Bitti = 1
    }

    public enum ProviderType
    {
        Perculus = 1,
        Connect = 2,
        BBB = 3
    }

    public static class VcEnumHelper
    {

        //http://stackoverflow.com/a/7111222
        [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
        public class MultipleButtonAttribute : ActionNameSelectorAttribute
        {
            public string Name { get; set; }
            public string Argument { get; set; }

            public override bool IsValidName(ControllerContext controllerContext, string actionName, MethodInfo methodInfo)
            {
                var isValidName = false;
                var keyValue = string.Format("{0}:{1}", Name, Argument);
                var value = controllerContext.Controller.ValueProvider.GetValue(keyValue);

                if (value != null)
                {
                    controllerContext.Controller.ControllerContext.RouteData.Values[Name] = Argument;
                    isValidName = true;
                }

                return isValidName;
            }
        }

    }
}
