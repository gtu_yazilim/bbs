﻿using System;
using PerculusSDK;
using PerculusSDK.ServiceReference1;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sabis.Bolum.Core.ABS.DERS.SANALSINIF.Models;
using System.Web.Configuration;
using Sabis.Bolum.Core.ABS.DERS.SANALSINIF.Dto;
using System.Text.RegularExpressions;

namespace PerculusSDK.Helpers
{
    public static class VCHelper
    {
        public static string AttemptVcLass()
        {
            return null;
        }

        public static SSDurum CheckDate(DateTime startDate, int duration)
        {
            //ders başlamadan 30 dk önce ve bittikten 10 dakika sonraya kadar giriş yapılabilir
            int beforeStart = Convert.ToInt32(WebConfigurationManager.AppSettings["VC:BeforeStartTime"]);
            int afterEnd = Convert.ToInt32(WebConfigurationManager.AppSettings["VC:AfterEndTime"]);
            var sDate = startDate.AddMinutes(-beforeStart);
            var eDate = startDate.AddMinutes(duration + afterEnd);
            var now = DateTime.Now;
            if (now < sDate)
            {
                return SSDurum.Baslamadi;
            }
            if (sDate <= now && now <= eDate)
            {
                return SSDurum.Basladi;
            }
            return SSDurum.Bitti;
        }

        public static string ShowReplay(int? vcKEy, KullaniciTipEnum userType,string ad, string soyad,string kullaniciadi,int sunucuid, ref string errorMessage)
        {
            if (vcKEy.HasValue)
            {
                try
                {
                   

                    PercSdk.Instance.RoomService.PublishReplay(vcKEy.Value);
                    var participant = new Participant();
                    participant.FirstName = TurkishName2(ad);
                    participant.LastName = TurkishName2(soyad);
                    participant.Role = UserTypeConverter(userType);
                    participant.Email = kullaniciadi + "@sakarya.edu.tr";

                    switch (sunucuid)
                    {
                        case 1: return PercSdk.Instance.RoomService.RegisterAndGetURL(participant, vcKEy.Value); break;
                        case 2: return PercSdk.Instance.RoomService2.RegisterAndGetURL(participant, vcKEy.Value); break;
                        case 3: return PercSdk.Instance.RoomService3.RegisterAndGetURL(participant, vcKEy.Value); break;
                        case 4: return PercSdk.Instance.RoomService4.RegisterAndGetURL(participant, vcKEy.Value); break;
                        default:
                            return PercSdk.Instance.RoomService2.RegisterAndGetURL(participant, vcKEy.Value); break;
                            break;
                    }

                    //return PercSdk.Instance.RoomService.RegisterAndGetURL(participant, vcKEy.Value);
                }
                catch (Exception ex)
                {
                    errorMessage = ex.Message;
                    return null;
                }
            }
            return null;
        }
        public static string TurkishName2(string phrase)
        {
            int maxLength = 100;
            string str = phrase.ToLower();
            int i = str.IndexOfAny(new char[] { 'ş', 'ç', 'ö', 'ğ', 'ü', 'ı' });
            //if any non-english charr exists,replace it with proper char
            if (i > -1)
            {
                StringBuilder outPut = new StringBuilder(str);
                outPut.Replace('ö', 'o');
                outPut.Replace('ç', 'c');
                outPut.Replace('ş', 's');
                outPut.Replace('ı', 'i');
                outPut.Replace('ğ', 'g');
                outPut.Replace('ü', 'u');
                str = outPut.ToString();
            }
            // if there are other invalid chars, convert them into blank spaces
            str = Regex.Replace(str, @"[^a-z0-9\s-]", "");
            // convert multiple spaces and hyphens into one space       
            str = Regex.Replace(str, @"[\s-]+", " ").Trim();
            // cut and trim string
            str = str.Substring(0, str.Length <= maxLength ? str.Length : maxLength).Trim();
            // add hyphens
            str = Regex.Replace(str, @"\s", "-");
            return str;
        }

        public static string VcStart(SanalSinifDto vc, KullaniciTipEnum userType, ref string errorMessage, string ad, string soyad, string kullaniciadi, int sunucuid)
        {
            try
            {
                var rm = VcToRoom(vc);

                if (rm.ROOMID <= 0)
                {
                    //rm = PercSdk.Instance.RoomService.Save(rm, PercHelper.GetModuleSettings());
                    switch (sunucuid)
                    {
                        case 1: rm = PercSdk.Instance.RoomService.Save(rm, PercHelper.GetModuleSettings()); break;
                        case 2: rm = PercSdk.Instance.RoomService2.Save(rm, PercHelper.GetModuleSettings()); break;
                        case 3: rm = PercSdk.Instance.RoomService3.Save(rm, PercHelper.GetModuleSettings()); break;
                        case 4: rm = rm = PercSdk.Instance.RoomService4.Save(rm, PercHelper.GetModuleSettings()); break;
                        default: rm = PercSdk.Instance.RoomService.Save(rm, PercHelper.GetModuleSettings()); break;
                    }
                    vc.SSinifKey = rm.ROOMID;
                }
                if (rm.ROOMID <= 0)
                {
                    // error
                    Console.Out.WriteLine("error:" + PercSdk.Instance.RoomService.GetLastError());
                }

                // add attendee
                if (rm.ROOMID > 0)
                {
                    return RegisterAttendee(userType, rm.ROOMID, ad, soyad, kullaniciadi, sunucuid);
                }
                return null;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return null;
            }
        }

        public static string RegisterAttendee(KullaniciTipEnum userType, int roomid, string ad, string soyad, string kullaniciadi,int sunucuid)
        {


            var participant = new Participant();
            participant.FirstName = TurkishName2(ad);
            participant.LastName = TurkishName2(soyad);
            participant.Role = UserTypeConverter(userType);
            participant.Email = kullaniciadi + "@sakarya.edu.tr";
            switch (sunucuid)
            {
                case 1: return PercSdk.Instance.RoomService.RegisterAndGetURL(participant, roomid); break;
                case 2: return PercSdk.Instance.RoomService2.RegisterAndGetURL(participant, roomid); break;
                case 3: return PercSdk.Instance.RoomService3.RegisterAndGetURL(participant, roomid); break;
                case 4: return PercSdk.Instance.RoomService4.RegisterAndGetURL(participant, roomid); break;
                default:
                    return PercSdk.Instance.RoomService.RegisterAndGetURL(participant, roomid); break;
                    break;
            }

            //return PercSdk.Instance.RoomService.RegisterAndGetURL(participant, roomid);
        }

        private static Room VcToRoom(SanalSinifDto vc)
        {
            return new Room
            {
                ROOMID = vc.SSinifKey ?? 0,
                BEGINDATE = vc.Baslangic,
                SESSIONNAME = vc.Ad,
                DESCRIPTION = "",
                FORMENROLL_CAPACITY = 50,
                SEND_ICS = true,
                USERRIGHTSCHEMA = "Standart",
                DURATION = vc.Sure,
                CATEGORY = "11003",
                PERMITTEDSYSTEMLAYOUTS = "elearning,meeting,one2one,webinar,layout-twovideo,layout-wide-1,layout-videoconf",
                COLORCODE = "#cccccc", // gray background
                LANGUAGEFILE = "tr-TR" // english: en-US, turkish: tr-TR
            };
        }

        private static string UserTypeConverter(KullaniciTipEnum usertype)
        {
            switch (usertype)
            {
                case KullaniciTipEnum.Hoca:
                    return Participant.PARTICIPANT_INSTRUCTOR;
                case KullaniciTipEnum.Ogrenci:
                    return Participant.PARTICIPANT_USER;
                default:
                    return Participant.PARTICIPANT_USER;
            }
        }
    }
    public class PercSdk
    {
        private static PercSdk _instance;
        private static readonly object Padlock = new object();
        private readonly string _perculusApiPassword = WebConfigurationManager.AppSettings["VC:password"];
        private readonly string _perculusApiUsername = WebConfigurationManager.AppSettings["VC:username"];
        private readonly string _perculusBaseUri = WebConfigurationManager.AppSettings["VC:Base_Url"];
        public readonly RoomService RoomService;
        public readonly RoomService RoomService2;
        public readonly RoomService RoomService3;
        public readonly RoomService RoomService4;

        private PercSdk()
        {
            RoomService = new RoomService();
            RoomService.Initialize(new Uri("http://www.etoplanti.sakarya.edu.tr/"), "system", "perculus07");
            RoomService2 = new RoomService();
            RoomService2.Initialize(new Uri("http://etoplanti2.sakarya.edu.tr/"), "system", "perculus07");
            RoomService3 = new RoomService();
            RoomService3.Initialize(new Uri("http://etoplanti3.sakarya.edu.tr/"), "system", "perculus07");
            RoomService4 = new RoomService();
            RoomService4.Initialize(new Uri("http://etoplanti4.sakarya.edu.tr/"), "system", "perculus07");
        }

        public static PercSdk Instance
        {
            get
            {
                lock (Padlock)
                {
                    if (_instance == null)
                    {
                        _instance = new PercSdk();
                    }
                    return _instance;
                }
            }
        }
    }
}
