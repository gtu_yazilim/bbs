﻿using PerculusSDK.Reports;
using PerculusSDK.ServiceReference1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace PerculusSDK
{
    public class ReportService
    {
        private ReportsSoapClient _rClient = null;
        private Reports.AuthHeader _aHeader = null;

        private Dictionary<string, string> _vcParameters = new Dictionary<string, string>();
        public Uri ApiUri
        {
            get
            {
                UriBuilder uriBuilder = new UriBuilder(PerculusBaseUri);
                uriBuilder.Path = "api/v2/reports.asmx";
                return uriBuilder.Uri;
            }
        }
        public Uri PerculusBaseUri { get; set; }

        private string _lastError;

        private ReportsSoapClient getReportsApi()
        {
            if (_rClient == null)
            {
                _rClient = new Reports.ReportsSoapClient(new BasicHttpBinding(), new EndpointAddress(ApiUri.ToString()));
                //_rClient = new Reports.ReportsSoapClient(new BasicHttpBinding(), new EndpointAddress("http://app.perculus.com/api/v2/reports.asmx"));
               
            }

            return _rClient;
        }
        private Reports.AuthHeader getAuthHeader()
        {
            return _aHeader;
        }
        public Reports.WSResponse GetDurationActive(int roomId)
        {
            try
            {
                var reportsApi = getReportsApi();
                var result = reportsApi.RoomList(getAuthHeader(), roomId);
                return result;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public bool Initialized { get; private set; }
        public bool Initialize(Uri perculusBaseUri, string apiUsername, string apiPassword)
        {
            if (Initialized)
            {
                _lastError = "Service is already initialized.";
                return false;
            }
            _aHeader = new Reports.AuthHeader()
            {
                Username = apiUsername,
                Password = apiPassword
            };

            PerculusBaseUri = perculusBaseUri;
            return true;
        }
        //public Reports.WSResponse GetCustomReport()
        //{
        //    try
        //    {
        //        var reportsApi = getReportsApi();
        //        var result = reportsApi.RunCustomReport(getAuthHeader(), 1, new ReportParameter("ROOMID", 123));
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}
    }
}
