﻿using Sabis.Bolum.Core.dto.Yoklama;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Sabis.Bolum.WebAPI.Controllers
{
    public class YoklamaController : ApiController
    {
        [HttpPost]
        public IHttpActionResult PinUret(int gecerlilikSuresi, int personelID, List<int> dersProgramIDList)
        {
            return Ok(Bll.SOURCE.ABS.Yoklama.YoklamaIslem.PinUret(gecerlilikSuresi, personelID, dersProgramIDList));
        }

        [HttpPost]
        public IHttpActionResult Kaydet(int ogrenciID, List<int> dersProgramIDList, string ip, string kullanici)
        {
            return Ok(Bll.SOURCE.ABS.Yoklama.YoklamaIslem.Kaydet(ogrenciID, dersProgramIDList, ip, kullanici));
        }

        [HttpPost]
        public IHttpActionResult KaydetListe(YoklamaKayitDto kayitDto, string ip, string kullanici)
        {
            return Ok(Bll.SOURCE.ABS.Yoklama.YoklamaIslem.KaydetListe(kayitDto, ip, kullanici));
        }

        [HttpPost]
        public IHttpActionResult SilListe(YoklamaKayitDto kayitDto, string ip, string kullanici)
        {
            return Ok(Bll.SOURCE.ABS.Yoklama.YoklamaIslem.SilListe(kayitDto, ip, kullanici));
        }

        [HttpPost]
        public IHttpActionResult Sil(int ogrenciID, List<int> dersProgramIDList, string ip, string kullanici)
        {
            return Ok(Bll.SOURCE.ABS.Yoklama.YoklamaIslem.Sil(ogrenciID, dersProgramIDList, ip, kullanici));
        }

        [HttpPost]
        public IHttpActionResult Listele(int dersGrupHocaID, List<int> dersProgramIDList)
        {
            return Ok(Bll.SOURCE.ABS.Yoklama.YoklamaIslem.Listele(dersGrupHocaID, dersProgramIDList));
        }

        [HttpGet]
        public IHttpActionResult ListeleKatilimOrani(int dersGrupHocaID)
        {
            return Ok(Bll.SOURCE.ABS.Yoklama.YoklamaIslem.ListeleKatilimOrani(dersGrupHocaID));
        }

        [HttpGet]
        public IHttpActionResult ListeleOgrenciKatilimOrani(int ogrenciID, int dersGrupID)
        {
            return Ok(Bll.SOURCE.ABS.Yoklama.YoklamaIslem.ListeleOgrenciKatilimOrani(ogrenciID, dersGrupID));
        }
    }
}
