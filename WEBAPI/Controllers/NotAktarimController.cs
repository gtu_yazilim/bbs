﻿using Sabis.Bolum.Bll.SOURCE.ABS.NOT;
using Sabis.Bolum.Core.ABS.NOT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Sabis.Bolum.WebAPI.Controllers
{
    public class NotAktarimController : ApiController
    {
        [HttpPost]
        public IHttpActionResult Aktar([FromBody]List<NotKayitVM> notList, int yil, int donem)
        {
            //return Ok(NOTAKTARIM.Aktar(notList, yil, donem));
            return Ok();
        }
    }
}
