﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using Sabis.Bolum.Bll.SOURCE.ABS.NOT;
using Sabis.Bolum.Core.ABS.NOT;
using System.Web.Http.Description;
using Sabis.Bolum.Core.ABS.DEGERLENDIRME;

namespace Sabis.Bolum.WebAPI.Controllers
{
    public class NotController : ApiController
    {

        [HttpGet]
        public IHttpActionResult ListeleOgrenciNotlari(int dersGrupID, string listPayID)
        {
            return Ok(NOTMAIN.ListeleOgrenciNotlari(dersGrupID, listPayID));
        }

        [HttpPost]
        public IHttpActionResult ListeleOgrenciNotlariv2(NotGirisListParamModel model)
        {
            return Ok(NOTMAIN.ListeleOgrenciNotlariv2(model));
        }

        [HttpPost]
        public IHttpActionResult NotKaydet(List<NotKayitVM> ogrenciNot)
        {
            try
            {
                return Ok(NOTMAIN.NotKaydet(ogrenciNot));
            }
            catch (Exception ex)
            {
                return NotFound();
            }
        }
      

        [HttpGet]
        [ResponseType(typeof(List<OgrenciListesiVM>))]
        public IHttpActionResult ListeleBasariNotlar(int dersGrupID, int personelID, bool butunlemeMi)
        {
            return Ok(BASARINOTMAIN.GetirBasariNotListesi(dersGrupID, personelID, butunlemeMi));
        }

        [HttpPost]
        [ResponseType(typeof(BasariNotKayitVM))]
        public IHttpActionResult KaydetBasariNotSerbest(BasariNotKayitVM basariNotKayit, string kullaniciAdi, string IPAdresi)
        {
            try
            {
                return Ok(BASARINOTMAIN.KaydetBasariNot(basariNotKayit, kullaniciAdi, IPAdresi));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message));
            }

        }
    }
}
