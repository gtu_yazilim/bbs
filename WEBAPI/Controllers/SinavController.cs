﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using Sabis.Bolum.Bll.SOURCE.ABS.DERS;

namespace Sabis.Bolum.WebAPI.Controllers
{
    public class SinavController : ApiController
    {
        [HttpGet]
        public IHttpActionResult GetirOgrenciSinavKagitlari(int FKOgrenciID, int FKABSPaylarID)
        {
            return Ok(Sabis.Bolum.Bll.SOURCE.ABS.DERS.SINAVMAIN.GetirOgrenciSinavKagitListeByOgrenciID(FKOgrenciID, FKABSPaylarID));
        }

        [HttpGet]
        public IHttpActionResult GetirOgrenciGrupSinavKagitlari(int DersGrupID, int OgrenciID)
        {
            return Ok(Sabis.Bolum.Bll.SOURCE.ABS.DERS.SINAVMAIN.GetirOgrenciSinavKagitListeByDersGrupID(DersGrupID, OgrenciID));
        }

        [HttpPost]
        public IHttpActionResult KaydetOgrenciSinavNotlari(int FKDersPlanAnaID, int FKDersPlanID, int FKDersGrupID, int FKAbsPaylarID, int sinavSoruSayisi, int yil, int enumDonem, string kullanici, string IP, List<Sabis.Bolum.Core.ABS.DERS.SINAV.SinavNotGirisSoruListeVM> OgrenciNotListesi)
        {
            if (!SINAVMAIN.GetirSinavDetayi(FKAbsPaylarID).SinavOlusturulmusMu)
            {
                //SINAVMAIN.SinavOlustur(sinavSoruSayisi, FKAbsPaylarID, Sabis.Bolum.Bll.SOURCE.EBS.DERS.DERSMAIN.GetirKoordinatorIDByDersPlanAnaID(FKDersPlanAnaID), yil, enumDonem, 0, 2, null, kullanici, IP, null);
            }

            if (SINAVMAIN.GetirSinavDetayi(FKAbsPaylarID).SoruSayisi == sinavSoruSayisi)
            {
                try
                {
                    SINAVMAIN.SinavNotKaydet(OgrenciNotListesi, new List<Sabis.Bolum.Core.ABS.DERS.ODEV.OdevAciklamaVM>(), FKDersPlanAnaID, FKDersPlanID, FKDersGrupID, yil, enumDonem, IP, kullanici, null, (int)Sabis.Bolum.Core.ENUM.ABS.EnumSinavTuru.TEST);
                    return Ok();
                }
                catch (Exception)
                {
                    return Ok("hata");
                }
                
            }
            else
            {
                return Ok("hata");
            }
        }

        #region Online Sınav
        [HttpGet]
        public IHttpActionResult OgrenciSinavListe(int FKDersGrupID, int FKOgrenciID)
        {
            return Ok(Sabis.Bolum.Bll.SOURCE.ABS.DERS.SINAVMAIN.OgrenciSinavListe(FKDersGrupID, FKOgrenciID));
        }

        [HttpGet]
        public IHttpActionResult OgrenciSinavListe(int FKOgrenciID, int yil, int donem)
        {
            return Ok(Sabis.Bolum.Bll.SOURCE.ABS.DERS.SINAVMAIN.OgrenciSinavListe(FKOgrenciID, yil, donem));
        }
        #endregion
    }
}
