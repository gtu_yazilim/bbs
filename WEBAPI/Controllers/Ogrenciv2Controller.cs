﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Sabis.Bolum.WebAPI.Controllers
{
    public class Ogrenciv2Controller : ApiController
    {
        [HttpGet]
        public IHttpActionResult ListeleDersGrupOgrenci(int dersGrupID)
        {
            return Ok(Sabis.Bolum.Bll.SOURCE.ABS.DERS.DersIslem.GetirDersOgrenciListesi(dersGrupID));
        }

    }
}
