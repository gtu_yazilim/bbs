﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Sabis.Bolum.WebAPI.Controllers
{
    public class ProgramController : ApiController
    {
        [HttpGet]
        public IHttpActionResult ListelePersonelDersProgrami(int personelID, int yil, int donem, DateTime baslangic, DateTime bitis)
        {
            return Ok(Bll.SOURCE.ABS.DERS.DERSPROGRAMI.GetirOgretimElemaniDersProgramiAralik(personelID, yil, donem, baslangic, bitis));
        }

        [HttpGet]
        public IHttpActionResult GetirGrupDersProgrami(int personelID, int dersGrupID)
        {
            return Ok(Bll.SOURCE.ABS.DERS.DERSPROGRAMI.GetirGrupDersProgramiv2(personelID, dersGrupID));
        }

        [HttpGet]
        public IHttpActionResult GetirGrupDersProgramiAralik(int personelID, int dersGrupID, DateTime baslangic, DateTime bitis)
        {
            return Ok(Bll.SOURCE.ABS.DERS.DERSPROGRAMI.GetirGrupDersProgramiv2Aralik(personelID, dersGrupID, baslangic, bitis));
        }
    }
}
