﻿using Sabis.Bolum.Bll.SOURCE.ABS.DERS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Sabis.Bolum.WebAPI.Controllers
{
    public class DuyuruController : ApiController
    {
        [HttpGet]
        public IHttpActionResult ListeleDuyuru(int dersGrupID, int personelID)
        {
            return Ok(DUYURUMAIN.GetirDuyuruListesi(dersGrupID, personelID));
        }

        [HttpPost]
        public IHttpActionResult KaydetDuyuru(Core.ABS.DERS.DERSDETAY.DuyuruVM duyuru, int DersGrupHocaID)
        {
            try
            {
                DUYURUMAIN.KaydetDuyuru(duyuru, DersGrupHocaID);
                return Ok("Başarılı");

            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }
        }
    }
}
