﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Sabis.Bolum.WebAPI.Controllers
{
    public class PersonelController : ApiController
    {
        [HttpGet]
        public IHttpActionResult GetirPersonelID(int kullaniciID)
        {
            return Ok(Sabis.Bolum.Bll.SOURCE.ABS.PERSONEL.PERSONELMAIN.GetirPersonelID(kullaniciID));
        }
    }
}
