﻿using Sabis.Bolum.Bll.SOURCE.ABS.DERS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using Sabis.Bolum.Core.ABS.DERS.HOME;

namespace Sabis.Bolum.WebAPI.Controllers
{
    public class Dersv2Controller : ApiController
    {
        private IDers _ders;
        [HttpGet]
        public IHttpActionResult ListelePersonelDersleri(int PersonelID, int Yil, int Donem)
        {
            return Ok(_ders.ListelePersonelDersleriv2(PersonelID, Yil, Donem));
        }

        [HttpGet]
        public IHttpActionResult GetirDersDetay(int DersGrupHocaID)
        {
            return Ok(Sabis.Bolum.Bll.SOURCE.ABS.DERS.DERSMAIN.GetirDersDetay(DersGrupHocaID));
        }
    }
}
