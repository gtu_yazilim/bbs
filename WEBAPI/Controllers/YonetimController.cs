﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using Sabis.Bolum.Bll.SOURCE.ADMIN;
using Sabis.Bolum.Core.ADMIN;

namespace Sabis.Bolum.WebAPI.Controllers
{
    public class YonetimController : ApiController
    {
        [HttpGet]
        public IHttpActionResult ListeleIPYetkileri()
        {
            return Ok(IPMAIN.GetirIPYetkiListesi());
        }

        public IHttpActionResult GetirIPYetki(int ID)
        {
            return Ok(IPMAIN.GetirIPYetki(ID));
        }

        [HttpGet]
        public IHttpActionResult SilIpYetki(int ID)
        {
            return Ok(IPMAIN.SilIpYetki(ID));
        }

        [HttpGet]
        public IHttpActionResult ListeleHafta(int Yil, int Donem)
        {
            //return Ok(HAFTAMAIN.ListeleHafta(Yil, Donem));
            return Ok();
        }

        [HttpPost]
        public IHttpActionResult EkleHafta(HaftaVM hafta)
        {
            //HAFTAMAIN.EkleHafta(hafta);
            return Ok();
        }

        [HttpPost]
        public IHttpActionResult GuncelleHafta(HaftaVM hafta)
        {
            //HAFTAMAIN.GuncelleHafta(hafta);
            return Ok();
        }

        [HttpGet]
        public IHttpActionResult SilHafta(int ID)
        {
            //HAFTAMAIN.SilHafta(ID);
            return Ok();
        }
    }
}
