﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using Sabis.Bolum.Bll.SOURCE.ABS.DERS;
using Sabis.Bolum.Core.ABS.DERS.DERSDETAY;

namespace Sabis.Bolum.WebAPI.Controllers
{
    public class PayController : ApiController
    {
        [HttpGet]
        public IHttpActionResult ListelePay(int dersplanID, int yil, int donem)
        {
            return Ok("kullanım dışı");
        }

        [HttpPost]
        public IHttpActionResult KaydetPay(PayVM pay)
        {
            return Ok("kullanım dışı");
        }

        [HttpGet]
        public IHttpActionResult SilPay(int payID)
        {
            return Ok("kullanım dışı");
        }
        public IHttpActionResult ListelePayHiyerarsik(int dersPlanAnaID, int yil, int donem)
        {
            return Ok(PAYMAIN.ListelePayHiyerarsik(dersPlanAnaID, yil, donem));
        }
    }
}
