﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using Sabis.Bolum.Bll.DATA.UysModel;
using Sabis.Bolum.Bll.SOURCE.ABS.DERS;
using Sabis.Bolum.Bll.SOURCE.ABS.OGRENCI;
using Sabis.Bolum.Core.ABS.DERS.DERSDETAY;
using Sabis.Bolum.Core.ABS.DERS.HOME;

namespace Sabis.Bolum.WebAPI.Controllers
{
    public class DersController : ApiController
    {
        private IDers _ders;
        [HttpGet]
        public IHttpActionResult ListelePersonelDersleri(int PersonelID, int Yil, int Donem)
        {
            return Ok(DERSMAIN.ListelePersonelDersleri(PersonelID, Yil, Donem));
        }

        [HttpGet]
        public IHttpActionResult ListelePersonelDersleriv2(int PersonelID, int Yil, int Donem)
        {
            return Ok(_ders.ListelePersonelDersleriv2(PersonelID, Yil, Donem));
        }

        [HttpGet]
        public IHttpActionResult ListeleAnaDersDetay(int PersonelID, int Yil, int Donem, int DersPlanAnaID)
        {
            return Ok(DERSMAIN.ListeleAnaDersDetay(PersonelID, Yil, Donem, DersPlanAnaID));
        }

        [HttpGet]
        public IHttpActionResult GetirDersPlanAna(int dpAnaID)
        {
            return Ok(DERSMAIN.GetirDersPlanAna(dpAnaID));
        }

        [HttpGet]
        public IHttpActionResult ListelePaylarVeNotBilgileriv2(int DersGrupHocaID, string kullanici = "", string IP = "")
        {
            return Ok(DERSMAIN.ListelePaylarVeNotBilgileriv2(DersGrupHocaID, kullanici, IP));
        }

        [HttpGet]
        public IHttpActionResult ListeleDersinOgrencileri(int dersGrupID)
        {
            return Ok(OGRENCIMAIN.GetirDersOgrenciListesi(dersGrupID));
        }

        [HttpGet]
        public IHttpActionResult ListeleDevamTakipV2(int dersGrupHocaID, string dersTarihi)
        {
            return Ok(YOKLAMAMAIN.GetirYoklamaListe(dersGrupHocaID, DateTime.Parse(dersTarihi)));
        }

        [HttpPost]
        public IHttpActionResult KaydetYoklama(List<Sabis.Bolum.Core.ABS.DERS.DERSDETAY.YoklamaVM> yoklama)
        {
            try
            {
                YOKLAMAMAIN.KaydetYoklama(yoklama);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }

            return Ok();
        }

        [HttpPost]
        public IHttpActionResult KaydetYoklamaV3(int dersGrupHocaID, DateTime dersTarihi, Dictionary<int, bool> devamListe)
        {
            //http://apiabs.apollo.gtu.edu.tr/api/Ders/KaydetYoklamaV3?dersGrupHocaID=471127&dersTarihi=23.02.2016
            try
            {
                YOKLAMAMAIN.KaydetYoklamaV3(dersGrupHocaID, dersTarihi, devamListe);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }

            return Ok();
        }

        [HttpGet]
        public IHttpActionResult ListeleDersinHaftaHaftaKonulari(int DersPlanAnaID, int yil, int dil)
        {
            return Ok("kullanım dışı");
        }

        [HttpGet]
        public IHttpActionResult ListeleDokumanlar(int DersPlanAnaID, int yil, int personelID)
        {
            return Ok("kullanım dışı");
        }

        [HttpGet]
        public IHttpActionResult ListeleDevamsizOgrenciListesi(int DersGrupID)
        {
            return Ok(YOKLAMAMAIN.GetirDevamsizOgrenciListesi(DersGrupID));
        }

        [HttpGet]
        public IHttpActionResult ListeleKatilimOrani(int dersGrupHocaID)
        {
            return Ok(YOKLAMAMAIN.ListeleKatilimOraniV2(dersGrupHocaID));
        }

        [HttpPost]
        public IHttpActionResult DokumanYukle()
        {
            return Ok("kullanım dışı");
        }

        [HttpGet]
        public IHttpActionResult ListeleDuyuru(int dersGrupID, int personelID)
        {
            return Ok(DUYURUMAIN.GetirDuyuruListesi(dersGrupID, personelID));
        }

        [HttpPost]
        public IHttpActionResult KaydetDuyuru(DuyuruVM duyuru, int fkDersGrupHocaID)
        {
            try
            {
                DUYURUMAIN.KaydetDuyuru(duyuru, fkDersGrupHocaID);
                return Ok("Başarılı");

            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }
        }

        //[HttpGet]
        //public IHttpActionResult ListeleDersProgramiOgretimUyesi(int personelID, int yil, int donem, int? FKDersGrupHocaID)
        //{
        //    return Ok(DERSPROGRAMI.GetirOgretimElemaniDersProgrami(personelID, yil, donem, FKDersGrupHocaID));
        //}

        [HttpPost]
        public IHttpActionResult PayYayinla(PayVM pay)
        {
            //try
            //{
            //    PAYMAIN.YayinlaPay(pay.ID.Value, pay.FKDersGrupID.Value,  , true);
            //    return Ok();
            //}
            //catch (Exception ex)
            //{
            //    return InternalServerError(ex);
            //}
            throw new NotImplementedException();
        }

        [HttpPost]
        public IHttpActionResult PayYayinlaDersplanAnaAltDersVeGruplari(PayVM pay)
        {
            try
            {
                PAYMAIN.PayYayinlaDersplanAnaAltDersVeGruplari(pay);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        public IHttpActionResult PayYayinlaDersplanAnaUstDers(PayVM pay)
        {
            try
            {
                PAYMAIN.PayYayinlaDersplanAnaUstDers(pay);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        public IHttpActionResult PayYayindanKaldirDersPlanAnaUstDers(PayVM pay)
        {
            try
            {
                PAYMAIN.PayYayindanKaldirDersPlanAnaUstDers(pay);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        public IHttpActionResult PayYayindanKaldirDersPlanAnaAltDersVeGruplari(PayVM pay)
        {
            try
            {
                PAYMAIN.PayYayindanKaldirDersPlanAnaAltDersVeGruplari(pay);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        public IHttpActionResult PayYayindanKaldir(PayVM pay)
        {
            try
            {
                PAYMAIN.YayindanKaldirPay(pay.ID.Value, pay.FKDersGrupID.Value);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        public IHttpActionResult GetirPayNotAdet(int payID, int yil, int donem, int grupID = 0)
        {
            try
            {
                var result = PAYMAIN.GetirPayNotAdet(payID, yil, donem, grupID);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
