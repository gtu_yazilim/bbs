﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace Sabis.Bolum.WebAPI.Controllers
{
    public class SSinifController : ApiController
    {
        [System.Web.Http.HttpGet]
        public IHttpActionResult ListeleSanalSinif(int grupID, int ogrenciID)
        {
            return Ok(Sabis.Bolum.Bll.SOURCE.ABS.DERS.SanalSinifApiData.SanalSinifListele(grupID, ogrenciID));
        }

        [System.Web.Http.HttpGet]
        public IHttpActionResult OgrenciSanalSinifKatil(int sanalSinifID, int ogrenciID)
        {
            return Ok(Sabis.Bolum.Bll.SOURCE.ABS.DERS.SanalSinifApiData.OgrenciSanalSinifKatil(sanalSinifID, ogrenciID));
        }

        [System.Web.Http.HttpGet]
        public IHttpActionResult ListeleSanalSinif(int ogrenciID, int yil, int donem)
        {
            return Ok(Sabis.Bolum.Bll.SOURCE.ABS.DERS.SanalSinifApiData.SanalSinifListele(ogrenciID, yil, donem));
        }
    }
}