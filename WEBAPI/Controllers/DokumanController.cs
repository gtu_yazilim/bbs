﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using Sabis.Bolum.Bll.SOURCE.ABS.DERS;

namespace Sabis.Bolum.WebAPI.Controllers
{
    public class DokumanController : ApiController
    {
        [HttpGet]
        public IHttpActionResult GetirDokumanListesi(int fkDersGrupID)
        {
            return Ok(DOKUMANMAIN.GetirDokumanListesiByDersGrupID(fkDersGrupID));
        }

        [HttpGet]
        public IHttpActionResult GetirDokumanListesi(int DersGrupID, int PersonelID)
        {
            return Ok(DOKUMANMAIN.GetirDokumanListesiByDersGrupID(DersGrupID, PersonelID));
        }

        [HttpGet]
        public IHttpActionResult GetirEBSDokumanListesi(int fkDersPlanAnaID, int yil)
        {
            return Ok(DOKUMANMAIN.GetirEBSDokumanListesi(fkDersPlanAnaID, yil));
        }
    }
}
