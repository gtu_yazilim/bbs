﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Sabis.Bolum.WebAPI.Controllers
{
    public class Payv2Controller : ApiController
    {
        [HttpGet]
        public IHttpActionResult ListelePay(int dersGrupHocaID, string kullaniciAdi, string ip)
        {
            return Ok(Sabis.Bolum.Bll.SOURCE.ABS.DERS.PAYMAIN.GetirPayBilgileriv2(dersGrupHocaID, kullaniciAdi, ip));
        }

        [HttpGet]
        public IHttpActionResult ListelePayHiyerarsik(int dersPlanAnaID, int yil, int donem)
        {
            return Ok(Sabis.Bolum.Bll.SOURCE.ABS.DERS.PAYMAIN.ListelePayHiyerarsik(dersPlanAnaID, yil, donem));
        }
    }
}
