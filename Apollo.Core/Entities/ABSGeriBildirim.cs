namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.ABSGeriBildirim")]
    public partial class ABSGeriBildirim
    {
        [Key]
        [Column(Order = 0)]
        public int ID { get; set; }

        [Key]
        [Column(Order = 1)]
        public DateTime TarihKayit { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int OgrenciID { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AktiviteID { get; set; }

        [Key]
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int GrupID { get; set; }

        [Key]
        [Column(Order = 5)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EnumModulTipi { get; set; }

        [Key]
        [Column(Order = 6)]
        [StringLength(100)]
        public string Baslik { get; set; }

        [Key]
        [Column(Order = 7)]
        public string Icerik { get; set; }

        [Key]
        [Column(Order = 8)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EnumGeriBildirimDurum { get; set; }

        public string Cevap { get; set; }

        [StringLength(50)]
        public string DosyaKey { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
