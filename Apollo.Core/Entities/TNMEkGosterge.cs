namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.TNMEkGosterge")]
    public partial class TNMEkGosterge
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? Kod { get; set; }

        public int DereceAlt { get; set; }

        public int DereceUst { get; set; }

        public int YilAlt { get; set; }

        public int YilUst { get; set; }

        public int AltIdariGorevID { get; set; }

        public int UstIdariGorevID { get; set; }

        public int EkGosterge { get; set; }

        public int? FKKadroUnvaniID { get; set; }

        public virtual TNMKadroUnvani TNMKadroUnvani { get; set; }
    }
}
