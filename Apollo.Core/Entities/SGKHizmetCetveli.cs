namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.SGKHizmetCetveli")]
    public partial class SGKHizmetCetveli
    {
        public int ID { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? Zaman { get; set; }

        [StringLength(11)]
        public string Tckn { get; set; }

        public long? kayitNo { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? baslamaTarihi { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? bitisTarihi { get; set; }

        [StringLength(200)]
        public string gorev { get; set; }

        public int? unvanKod { get; set; }

        [StringLength(200)]
        public string yevmiye { get; set; }

        [StringLength(200)]
        public string ucret { get; set; }

        [StringLength(200)]
        public string hizmetSinifi { get; set; }

        public int? kadroDerece { get; set; }

        public int? odemeDerece { get; set; }

        public int? odemeKademe { get; set; }

        public int? odemeEkgosterge { get; set; }

        public int? kazanilmisHakAyligiDerece { get; set; }

        public int? kazanilmisHakAyligiKademe { get; set; }

        public int? kazanilmisHakAyligiEkGosterge { get; set; }

        public int? emekliDerece { get; set; }

        public int? emekliKademe { get; set; }

        public int? emekliEkgosterge { get; set; }

        public int? sebepKod { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? kurumOnayTarihi { get; set; }

        [StringLength(250)]
        public string SGKDurumAciklama { get; set; }

        public int? ENUMSGKDurum { get; set; }

        public int? FKPersonelBilgisiID { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }
    }
}
