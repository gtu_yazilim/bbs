namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.IzinSakli")]
    public partial class IzinSakli
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public IzinSakli()
        {
            IzinSakliDetay = new HashSet<IzinSakliDetay>();
            IzinSakliToplam = new HashSet<IzinSakliToplam>();
        }

        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        public string Kullanici { get; set; }

        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int InKod { get; set; }

        public int FormNo { get; set; }

        public int FormTipi { get; set; }

        [Required]
        [StringLength(50)]
        public string EvrakNo { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? EvrakTarihi { get; set; }

        [Required]
        [StringLength(50)]
        public string Konu { get; set; }

        [Required]
        [StringLength(50)]
        public string Aciklama { get; set; }

        [Required]
        [StringLength(50)]
        public string YOKEvrakNo { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? YOKEvrakTarihi { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? YOKKurulTarihi { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? GecerlilikTarihi { get; set; }

        [Required]
        [StringLength(50)]
        public string UYKEvrakNo { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? UYKEvrakTarihi { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? UYKToplantiTarihi { get; set; }

        public int UYKToplantiNo { get; set; }

        public int UYKKararNo { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? OnayTarihi { get; set; }

        public int KullaniciKod { get; set; }

        public DateTime IslemTarihi { get; set; }

        public int ENUMKadroTipi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IzinSakliDetay> IzinSakliDetay { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IzinSakliToplam> IzinSakliToplam { get; set; }
    }
}
