namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.Docentlik")]
    public partial class Docentlik
    {
        public int ID { get; set; }

        [StringLength(255)]
        public string Ad { get; set; }

        [StringLength(255)]
        public string Soyad { get; set; }

        [StringLength(255)]
        public string Unvan { get; set; }

        [StringLength(255)]
        public string Birim { get; set; }

        public DateTime? DocentTarih { get; set; }

        public DateTime? ProfesorlukTarih { get; set; }

        [StringLength(255)]
        public string BilimDali { get; set; }

        [StringLength(255)]
        public string Aciklama { get; set; }
    }
}
