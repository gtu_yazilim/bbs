namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRTNMBelge")]
    public partial class OGRTNMBelge
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? InKod { get; set; }

        [StringLength(50)]
        public string OgrBelgeSayi { get; set; }

        [StringLength(50)]
        public string YuzdeOnSayi { get; set; }

        [StringLength(50)]
        public string DisiplinCezaSayi { get; set; }

        [StringLength(50)]
        public string HazirlikSayi { get; set; }

        [StringLength(50)]
        public string YazOkuluSayi { get; set; }

        [StringLength(50)]
        public string YosSayi { get; set; }

        [StringLength(50)]
        public string YuksekOnurAileSayi { get; set; }

        [StringLength(500)]
        public string BelgeBaslik { get; set; }

        [StringLength(500)]
        public string BelgeBaslikEng { get; set; }

        [StringLength(250)]
        public string YetkiliUnvan { get; set; }

        [StringLength(250)]
        public string YetkiliUnvanEng { get; set; }

        [StringLength(500)]
        public string YetkiliAd { get; set; }

        [StringLength(250)]
        public string YosYetkiliUnvan { get; set; }

        [StringLength(250)]
        public string YosYetkiliUnvanEng { get; set; }

        [StringLength(500)]
        public string YosYetkiliAd { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public int? FKFakulteBirimID { get; set; }

        public string ImzaUrl { get; set; }

        public virtual Birimler Birimler { get; set; }
    }
}
