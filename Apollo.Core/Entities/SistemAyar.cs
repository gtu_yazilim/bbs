namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.SistemAyar")]
    public partial class SistemAyar
    {
        public int ID { get; set; }

        [StringLength(12)]
        public string CevrimBasTarih { get; set; }

        [StringLength(12)]
        public string CevrimBitTarih { get; set; }

        public byte? CevrimGecerliOlduguYer { get; set; }

        [StringLength(255)]
        public string MYOAyriDegenlendirilenler { get; set; }

        [Column(TypeName = "text")]
        public string OzelYetkililer { get; set; }

        public int? AktifYil { get; set; }

        public int? DuzenlenenYil { get; set; }

        public byte? SecenekAdet { get; set; }

        [StringLength(15)]
        public string Ekleyen { get; set; }

        public DateTime? Zaman { get; set; }
    }
}
