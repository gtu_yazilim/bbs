namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRMazeret")]
    public partial class OGRMazeret
    {
        public int ID { get; set; }

        public int OgretimYili { get; set; }

        public int EnumDonem { get; set; }

        public int FKDersPlanAnaID { get; set; }

        public int FKDersPlanID { get; set; }

        public int FKDersGrupID { get; set; }

        public int? FKHocaID { get; set; }

        public int FKOgrenciID { get; set; }

        public int? SinavMekanID { get; set; }

        public string SinavMekani { get; set; }

        public DateTime? SinavZamani { get; set; }

        public DateTime? YKKTarih { get; set; }

        [StringLength(50)]
        public string YKKNo { get; set; }

        public int? EnumMazeretDurum { get; set; }

        [Required]
        public string Makina { get; set; }

        public DateTime Zaman { get; set; }

        [Required]
        public string Kullanici { get; set; }

        public string SGKullanici { get; set; }

        public DateTime? SGZaman { get; set; }

        public bool Silindi { get; set; }

        [StringLength(250)]
        public string Aciklama { get; set; }

        public virtual MekanV2 MekanV2 { get; set; }

        public virtual OGRDersGrup OGRDersGrup { get; set; }

        public virtual OGRDersPlan OGRDersPlan { get; set; }

        public virtual OGRDersPlanAna OGRDersPlanAna { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }
    }
}
