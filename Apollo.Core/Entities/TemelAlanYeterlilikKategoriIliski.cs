namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.TemelAlanYeterlilikKategoriIliski")]
    public partial class TemelAlanYeterlilikKategoriIliski
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TemelAlanYeterlilikKategoriIliski()
        {
            DilTemelAlanYeterlilik = new HashSet<DilTemelAlanYeterlilik>();
        }

        public int ID { get; set; }

        public int? EnumDuzey { get; set; }

        public byte? EnumAlanTur { get; set; }

        public int? FKTemelAlanID { get; set; }

        public int? FKYeterlilikAltKategoriID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DilTemelAlanYeterlilik> DilTemelAlanYeterlilik { get; set; }

        public virtual TemelAlan TemelAlan { get; set; }

        public virtual YeterlilikAltKategori YeterlilikAltKategori { get; set; }
    }
}
