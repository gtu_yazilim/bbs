namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ytk.KullaniciGuvenlik")]
    public partial class KullaniciGuvenlik
    {
        public int ID { get; set; }

        [Required]
        [StringLength(255)]
        public string Sifre { get; set; }

        public int FKKullaniciID { get; set; }

        public DateTime? DogumTarihi { get; set; }

        [StringLength(30)]
        public string AnneKizlikSoyadi { get; set; }

        [StringLength(80)]
        public string GizliSoru { get; set; }

        [StringLength(80)]
        public string GizliCevap { get; set; }

        [StringLength(254)]
        public string Email { get; set; }

        public bool EmailOnay { get; set; }

        public DateTime? EmailGirisTarihi { get; set; }

        public int EnumGizliSoru { get; set; }

        public virtual Kullanici Kullanici { get; set; }
    }
}
