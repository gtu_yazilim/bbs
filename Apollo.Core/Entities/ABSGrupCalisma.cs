namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.ABSGrupCalisma")]
    public partial class ABSGrupCalisma
    {
        public int ID { get; set; }

        public int FK_PersonelID { get; set; }

        public int FK_OgrenciID { get; set; }

        public int FK_DersGrupID { get; set; }

        public int Yil { get; set; }

        public int Donem { get; set; }

        [StringLength(255)]
        public string TezAdi { get; set; }

        [StringLength(255)]
        public string Konu { get; set; }

        [StringLength(255)]
        public string TezLinki { get; set; }

        [Required]
        [StringLength(50)]
        public string EkleyenKullanici { get; set; }

        public DateTime EklenmeTarihi { get; set; }

        public virtual OGRDersGrup OGRDersGrup { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }
    }
}
