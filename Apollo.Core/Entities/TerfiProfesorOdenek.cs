namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.TerfiProfesorOdenek")]
    public partial class TerfiProfesorOdenek
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int FKTCNo { get; set; }

        public int InKod { get; set; }

        public int BelgeNo { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime BaslangicTarihi { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime BitisTarihi { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime Tarih { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime OnayTarihi { get; set; }

        [Required]
        [StringLength(50)]
        public string EvrakNo { get; set; }

        [StringLength(50)]
        public string SicilNo { get; set; }

        [StringLength(50)]
        public string AdiSoyadi { get; set; }

        [StringLength(50)]
        public string Unvani { get; set; }

        [StringLength(50)]
        public string ProfAtanmaTarihi { get; set; }

        [StringLength(50)]
        public string EKidemYili { get; set; }

        [StringLength(50)]
        public string EEkGosterge { get; set; }

        [StringLength(50)]
        public string EMakamTazminati { get; set; }

        [StringLength(50)]
        public string EUniversiteOdenegi { get; set; }

        [StringLength(50)]
        public string YKidemYili { get; set; }

        [StringLength(50)]
        public string YEkGosterge { get; set; }

        [StringLength(50)]
        public string YMakamTazminati { get; set; }

        [StringLength(50)]
        public string YUniversiteOdenegi { get; set; }

        [StringLength(50)]
        public string IslemKod { get; set; }

        [StringLength(50)]
        public string GorevYeri { get; set; }

        public int KullaniciKod { get; set; }

        public DateTime IslemTarihi { get; set; }
    }
}
