namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.WebDersTemel")]
    public partial class WebDersTemel
    {
        [Key]
        public int InKod { get; set; }

        public int? AnaDersPlanID { get; set; }

        public byte? FormDil { get; set; }

        [Column(TypeName = "text")]
        public string OnKosul { get; set; }

        [StringLength(1)]
        public string Dil { get; set; }

        [Column(TypeName = "text")]
        public string Koordinator { get; set; }

        [Column(TypeName = "text")]
        public string DersiVeren { get; set; }

        [Column(TypeName = "text")]
        public string DersYardimci { get; set; }

        [Column(TypeName = "text")]
        public string DersinAmaci { get; set; }

        [Column(TypeName = "text")]
        public string DersIcerigi { get; set; }

        [Column(TypeName = "text")]
        public string DersNotu { get; set; }

        [Column(TypeName = "text")]
        public string DersKaynak { get; set; }

        [Column(TypeName = "text")]
        public string H1 { get; set; }

        [Column(TypeName = "text")]
        public string H2 { get; set; }

        [Column(TypeName = "text")]
        public string H3 { get; set; }

        [Column(TypeName = "text")]
        public string H4 { get; set; }

        [Column(TypeName = "text")]
        public string H5 { get; set; }

        [Column(TypeName = "text")]
        public string H6 { get; set; }

        [Column(TypeName = "text")]
        public string H7 { get; set; }

        [Column(TypeName = "text")]
        public string H8 { get; set; }

        [Column(TypeName = "text")]
        public string H9 { get; set; }

        [Column(TypeName = "text")]
        public string H10 { get; set; }

        [Column(TypeName = "text")]
        public string H11 { get; set; }

        [Column(TypeName = "text")]
        public string H12 { get; set; }

        [Column(TypeName = "text")]
        public string H13 { get; set; }

        [Column(TypeName = "text")]
        public string H14 { get; set; }

        [StringLength(255)]
        public string OH1 { get; set; }

        [StringLength(255)]
        public string OH2 { get; set; }

        [StringLength(255)]
        public string OH3 { get; set; }

        [StringLength(255)]
        public string OH4 { get; set; }

        [StringLength(255)]
        public string OH5 { get; set; }

        [StringLength(255)]
        public string OH6 { get; set; }

        [StringLength(255)]
        public string OH7 { get; set; }

        [StringLength(255)]
        public string OH8 { get; set; }

        [StringLength(255)]
        public string OH9 { get; set; }

        [StringLength(255)]
        public string OH10 { get; set; }

        [StringLength(255)]
        public string OH11 { get; set; }

        [StringLength(255)]
        public string OH12 { get; set; }

        [StringLength(255)]
        public string OH13 { get; set; }

        [StringLength(255)]
        public string OH14 { get; set; }

        public short? Yil1 { get; set; }

        [StringLength(30)]
        public string Ekleyen { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? Zaman { get; set; }

        [Column(TypeName = "text")]
        public string H15 { get; set; }

        [Column(TypeName = "text")]
        public string H18 { get; set; }

        [Column(TypeName = "text")]
        public string H16 { get; set; }

        [Column(TypeName = "text")]
        public string H17 { get; set; }

        [Column(TypeName = "text")]
        public string H19 { get; set; }

        [Column(TypeName = "text")]
        public string H20 { get; set; }

        [Column(TypeName = "text")]
        public string H21 { get; set; }

        [Column(TypeName = "text")]
        public string H22 { get; set; }

        [Column(TypeName = "text")]
        public string H23 { get; set; }

        [Column(TypeName = "text")]
        public string H24 { get; set; }

        [Column(TypeName = "text")]
        public string H25 { get; set; }

        [Column(TypeName = "text")]
        public string H26 { get; set; }

        [Column(TypeName = "text")]
        public string H27 { get; set; }

        [Column(TypeName = "text")]
        public string H28 { get; set; }

        [Column(TypeName = "text")]
        public string H29 { get; set; }

        [Column(TypeName = "text")]
        public string H30 { get; set; }

        [Column(TypeName = "text")]
        public string H31 { get; set; }

        [Column(TypeName = "text")]
        public string H32 { get; set; }

        [Column(TypeName = "text")]
        public string H33 { get; set; }

        [Column(TypeName = "text")]
        public string H34 { get; set; }

        [Column(TypeName = "text")]
        public string H35 { get; set; }

        [Column(TypeName = "text")]
        public string H36 { get; set; }

        [StringLength(255)]
        public string OH15 { get; set; }

        [StringLength(255)]
        public string OH16 { get; set; }

        [StringLength(255)]
        public string OH17 { get; set; }

        [StringLength(255)]
        public string OH18 { get; set; }

        [StringLength(255)]
        public string OH19 { get; set; }

        [StringLength(255)]
        public string OH20 { get; set; }

        [StringLength(255)]
        public string OH21 { get; set; }

        [StringLength(255)]
        public string OH22 { get; set; }

        [StringLength(255)]
        public string OH23 { get; set; }

        [StringLength(255)]
        public string OH24 { get; set; }

        [StringLength(255)]
        public string OH25 { get; set; }

        [StringLength(255)]
        public string OH26 { get; set; }

        [StringLength(255)]
        public string OH27 { get; set; }

        [StringLength(255)]
        public string OH28 { get; set; }

        [StringLength(255)]
        public string OH29 { get; set; }

        [StringLength(255)]
        public string OH30 { get; set; }

        [StringLength(255)]
        public string OH31 { get; set; }

        [StringLength(255)]
        public string OH32 { get; set; }

        [StringLength(255)]
        public string OH33 { get; set; }

        [StringLength(255)]
        public string OH34 { get; set; }

        [StringLength(255)]
        public string OH35 { get; set; }

        [StringLength(255)]
        public string OH36 { get; set; }
    }
}
