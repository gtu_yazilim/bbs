namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.WebYetkililer")]
    public partial class WebYetkililer
    {
        [Key]
        public int InKod { get; set; }

        public int YetkiGrupID { get; set; }

        [StringLength(50)]
        public string UserName { get; set; }

        [StringLength(2)]
        public string FakKod { get; set; }

        [StringLength(2)]
        public string BolKod { get; set; }

        [StringLength(12)]
        public string YetkiTarih1 { get; set; }

        [StringLength(12)]
        public string YetkiTarih2 { get; set; }

        [StringLength(15)]
        public string Ekleyen { get; set; }

        public DateTime? Zaman { get; set; }

        public int? FKFakulteBirimID { get; set; }

        public int? FKBolumBirimID { get; set; }

        public int? FKProgramBirimID { get; set; }
    }
}
