namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ytk.SifreDogrulamaServisYetki")]
    public partial class SifreDogrulamaServisYetki
    {
        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string UygulamaAdi { get; set; }

        [Required]
        [StringLength(50)]
        public string UygulamaSifre { get; set; }

        [Required]
        [StringLength(50)]
        public string IP { get; set; }

        public DateTime Tarih { get; set; }

        public bool AktifMi { get; set; }
    }
}
