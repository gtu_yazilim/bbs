namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.BYOK")]
    public partial class BYOK
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? InKod { get; set; }

        [StringLength(11)]
        public string TCNo { get; set; }

        [Required]
        [StringLength(50)]
        public string Adi { get; set; }

        [Required]
        [StringLength(50)]
        public string Soyadi { get; set; }

        [StringLength(5)]
        public string CuzdanSeri { get; set; }

        [StringLength(10)]
        public string CuzdanNo { get; set; }

        [StringLength(50)]
        public string OncekiSoyadi { get; set; }

        [StringLength(50)]
        public string BabaAdi { get; set; }

        [StringLength(50)]
        public string AnaAdi { get; set; }

        [StringLength(100)]
        public string DogumYeri { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? DogumTarihi { get; set; }

        [StringLength(50)]
        public string NKMahalleKoy { get; set; }

        [StringLength(6)]
        public string NKCiltNo { get; set; }

        [StringLength(7)]
        public string NKAileSirano { get; set; }

        [StringLength(6)]
        public string NKSirano { get; set; }

        [StringLength(50)]
        public string VerildigiYer { get; set; }

        [StringLength(25)]
        public string VerilisNedeni { get; set; }

        public int? KayitNo { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? VerilisTarihi { get; set; }

        [StringLength(50)]
        public string KizlikSoyadi { get; set; }

        [Column(TypeName = "image")]
        public byte[] Resim { get; set; }

        [StringLength(100)]
        public string Okulu { get; set; }

        [StringLength(15)]
        public string Numarasi { get; set; }

        public int? Sinifi { get; set; }

        public bool? BursDurumu { get; set; }

        [StringLength(250)]
        public string Aciklama1 { get; set; }

        public bool? CocukOzMu { get; set; }

        [StringLength(250)]
        public string Aciklama2 { get; set; }

        public int? KullaniciKod { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? IslemTarihi { get; set; }

        public int? ENUMYakinlikDerecesi { get; set; }

        public int? ENUMMedeniHali { get; set; }

        public int? ENUMCinsiyeti { get; set; }

        public int? ENUMKanGrubu { get; set; }

        public int? ENUMDin { get; set; }

        public int? ENUMMesguliyeti { get; set; }

        public int FKPersonelBilgisiID { get; set; }

        public int? FKUyrukID { get; set; }

        public int? FKDogUlkeID { get; set; }

        public int? FKDogIlID { get; set; }

        public int? FKDogIlceID { get; set; }

        public int? FKNKUlkeID { get; set; }

        public int? FKNKIlID { get; set; }

        public int? FKNKIlceID { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }
    }
}
