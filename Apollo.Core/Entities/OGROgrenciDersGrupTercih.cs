namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGROgrenciDersGrupTercih")]
    public partial class OGROgrenciDersGrupTercih
    {
        [Key]
        [Column(Order = 0)]
        public int ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string Kullanici { get; set; }

        [Key]
        [Column(Order = 2)]
        public DateTime TarihKayit { get; set; }

        public DateTime? TarihGuncelleme { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FKOgrenciID { get; set; }

        [Key]
        [Column(Order = 4)]
        [StringLength(3)]
        public string GrupAd { get; set; }

        [Key]
        [Column(Order = 5)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Yil { get; set; }
    }
}
