namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yoksis.YokApiSorguLog")]
    public partial class YokApiSorguLog
    {
        public int ID { get; set; }

        public DateTime Tarih { get; set; }

        [Required]
        [StringLength(50)]
        public string SorgulayanKullanici { get; set; }

        [Required]
        [StringLength(50)]
        public string SorgulayanIP { get; set; }

        [Required]
        [StringLength(50)]
        public string UygulamaAdi { get; set; }

        [Required]
        [StringLength(50)]
        public string TCKimlikNo { get; set; }

        [Required]
        [StringLength(50)]
        public string SorguTipi { get; set; }

        [Required]
        [StringLength(50)]
        public string UygulamaSunucuIP { get; set; }

        public string Sonuc { get; set; }
    }
}
