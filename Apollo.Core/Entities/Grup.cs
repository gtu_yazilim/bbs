namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ytk.Grup")]
    public partial class Grup
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Grup()
        {
            GrupKullanici = new HashSet<GrupKullanici>();
            RolGrup = new HashSet<RolGrup>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(100)]
        public string Ad { get; set; }

        public int? FKTNMGorevUnvaniID { get; set; }

        public int? EnumKullaniciTip { get; set; }

        public virtual TNMGorevUnvani TNMGorevUnvani { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GrupKullanici> GrupKullanici { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RolGrup> RolGrup { get; set; }
    }
}
