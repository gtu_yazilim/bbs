namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRKurulKarar")]
    public partial class OGRKurulKarar
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OGRKurulKarar()
        {
            OGROgrenciNot = new HashSet<OGROgrenciNot>();
        }

        public int ID { get; set; }

        public DateTime TarihKayit { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        public DateTime TarihKarar { get; set; }

        [Required]
        [StringLength(50)]
        public string KararNo { get; set; }

        [StringLength(50)]
        public string DokumanID { get; set; }

        public int FKBirimID { get; set; }

        public virtual Birimler Birimler { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGROgrenciNot> OGROgrenciNot { get; set; }
    }
}
