namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRCapDersPlan")]
    public partial class OGRCapDersPlan
    {
        [Key]
        [Column(Order = 0)]
        public int ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FKCapTanimID { get; set; }

        [Key]
        [Column(Order = 2)]
        public DateTime TarihKayit { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(50)]
        public string Kullanici { get; set; }

        [Key]
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Yil { get; set; }

        [Key]
        [Column(Order = 5)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FKAnaDersPlanAnaID { get; set; }

        [Key]
        [Column(Order = 6)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FKCapDersPlanAnaID { get; set; }

        [Key]
        [Column(Order = 7)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FKAnaDersPlanID { get; set; }

        [Key]
        [Column(Order = 8)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FKCapDersPlanID { get; set; }

        [Key]
        [Column(Order = 9)]
        public bool Silindi { get; set; }

        public DateTime? TarihSilinme { get; set; }

        [StringLength(4000)]
        public string Aciklama { get; set; }

        public int? FKAnaDersPlanAnaID_tmp { get; set; }

        public int? FKCapDersPlanAnaID_tmp { get; set; }

        public int? FKAnaDersPlanID_tmp { get; set; }

        public int? FKCapDersPlanID_tmp { get; set; }
    }
}
