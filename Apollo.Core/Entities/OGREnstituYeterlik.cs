namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGREnstituYeterlik")]
    public partial class OGREnstituYeterlik
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? InKod { get; set; }

        public DateTime? Tarih { get; set; }

        public int? EnumEnstituYeterlikDurum { get; set; }

        public int? FKOgrenciID { get; set; }

        public int? BNot { get; set; }

        public DateTime? SonTarih { get; set; }

        public int? FKMekanID { get; set; }

        public TimeSpan? Saat { get; set; }

        [StringLength(10)]
        public string YKNo { get; set; }

        public DateTime? YKTarih { get; set; }

        public int? FKDanismanID { get; set; }

        public int? FKOgrEleman1ID { get; set; }

        public int? FKOgrEleman2ID { get; set; }

        public int? FKOgrEleman3ID { get; set; }

        public int? FKOgrEleman4ID { get; set; }

        [StringLength(100)]
        public string AdSoyad { get; set; }

        [StringLength(200)]
        public string Unv { get; set; }

        [StringLength(100)]
        public string AdSoyad1 { get; set; }

        [StringLength(200)]
        public string Unv1 { get; set; }

        [StringLength(100)]
        public string AdSoyad2 { get; set; }

        [StringLength(200)]
        public string Unv2 { get; set; }

        [StringLength(100)]
        public string AdSoyad3 { get; set; }

        [StringLength(200)]
        public string Unv3 { get; set; }

        [StringLength(100)]
        public string AdSoyad4 { get; set; }

        [StringLength(200)]
        public string Unv4 { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public virtual Mekan Mekan { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi1 { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi2 { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi3 { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi4 { get; set; }
    }
}
