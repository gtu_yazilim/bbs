namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.ABSProjeRapor")]
    public partial class ABSProjeRapor
    {
        [Key]
        [Column(Order = 0)]
        public int ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FKProjeOgrenciID { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int RaporNo { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int RaporDurum { get; set; }

        [StringLength(100)]
        public string DosyaKey { get; set; }

        [StringLength(4000)]
        public string HocaAciklama { get; set; }

        [Key]
        [Column(Order = 4)]
        public DateTime TarihHocaCevap { get; set; }

        [Key]
        [Column(Order = 5)]
        public DateTime TarihKatilim { get; set; }
    }
}
