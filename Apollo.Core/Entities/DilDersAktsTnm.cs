namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.DilDersAktsTnm")]
    public partial class DilDersAktsTnm
    {
        public int ID { get; set; }

        public int? FKDersAktsTnmID { get; set; }

        [Column(TypeName = "text")]
        public string Icerik { get; set; }

        public byte? EnumDilID { get; set; }

        [StringLength(50)]
        public string Ekleyen { get; set; }

        public DateTime? Zaman { get; set; }

        public virtual DersAktsTnm DersAktsTnm { get; set; }
    }
}
