namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("deg.DegisimDonem")]
    public partial class DegisimDonem
    {
        public int ID { get; set; }

        [StringLength(255)]
        public string DonemAd { get; set; }

        [StringLength(255)]
        public string DonemAdEn { get; set; }

        public string SozlesmeMetni { get; set; }
    }
}
