namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("basvuru.MuafiyetSinavBasvuru")]
    public partial class MuafiyetSinavBasvuru
    {
        public int ID { get; set; }

        public int FKOgrenciID { get; set; }

        public DateTime TarihKayit { get; set; }

        public DateTime? TarihGuncelleme { get; set; }

        [Required]
        [StringLength(500)]
        public string Kullanici { get; set; }

        [Required]
        [StringLength(50)]
        public string IP { get; set; }

        public bool Iptal { get; set; }

        public int FKMuafiyetSinavTanimID { get; set; }

        public int? FKMuafiyetSinavOturumID { get; set; }

        public int SiraNo { get; set; }

        public virtual MuafiyetSinavOturum MuafiyetSinavOturum { get; set; }

        public virtual MuafiyetSinavTanim MuafiyetSinavTanim { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
