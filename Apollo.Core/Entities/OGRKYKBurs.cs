namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRKYKBurs")]
    public partial class OGRKYKBurs
    {
        public int ID { get; set; }

        public long BursNo { get; set; }

        public DateTime? BursBaslamaTarihi { get; set; }

        public DateTime? BursBitisTarihi { get; set; }

        public DateTime? BursIptalTarihi { get; set; }

        public DateTime? BasarisizlikKYKBildirimTarihi { get; set; }

        public int? EnumBurs { get; set; }

        public int FKOgrenciID { get; set; }

        public DateTime? KayitTarihi { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        public DateTime? GuncellenmeTarihi { get; set; }

        [StringLength(50)]
        public string GuncelleyenKullanici { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
