namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("mkn.MekanCihazlar")]
    public partial class MekanCihazlar
    {
        public int ID { get; set; }

        public int? FKMekanID { get; set; }

        [StringLength(100)]
        public string CihazAd { get; set; }

        [StringLength(200)]
        public string CihazOzellik { get; set; }

        [Column(TypeName = "image")]
        public byte[] Foto { get; set; }

        public virtual Mekan Mekan { get; set; }
    }
}
