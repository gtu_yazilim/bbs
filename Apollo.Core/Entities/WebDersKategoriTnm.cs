namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.WebDersKategoriTnm")]
    public partial class WebDersKategoriTnm
    {
        [Key]
        public int InKod { get; set; }

        public byte? FormDil { get; set; }

        public int? KategoriNo { get; set; }

        [StringLength(10)]
        public string Kategori { get; set; }

        public byte? Sira { get; set; }

        [StringLength(150)]
        public string Ad { get; set; }

        public short? Yil1 { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? Zaman { get; set; }
    }
}
