namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.ABSOrtakDegerlendirme")]
    public partial class ABSOrtakDegerlendirme
    {
        [Key]
        [Column(Order = 0)]
        public int ID { get; set; }

        [Key]
        [Column(Order = 1)]
        public DateTime TarihKayit { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(50)]
        public string Kullanici { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FKDersPlanAnaID { get; set; }

        [Key]
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FKPersonelID { get; set; }

        public int? Yil { get; set; }

        public int? Donem { get; set; }

        [Key]
        [Column(Order = 5)]
        public bool Silindi { get; set; }
    }
}
