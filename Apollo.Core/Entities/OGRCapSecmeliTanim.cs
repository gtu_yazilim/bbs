namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRCapSecmeliTanim")]
    public partial class OGRCapSecmeliTanim
    {
        [Key]
        [Column(Order = 0)]
        public int ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FKCapTanimID { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FKAnaEtiketID { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FKCapEtiketID { get; set; }

        [Key]
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Adet { get; set; }

        public int? Yil { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [Key]
        [Column(Order = 5)]
        public DateTime TarihKayit { get; set; }

        [Key]
        [Column(Order = 6)]
        public bool Silindi { get; set; }

        [Key]
        [Column(Order = 7)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FKSaatBilgiID { get; set; }

        public int? FKCapSaatBilgiID { get; set; }

        public int? FKAnaEtiketID_tmp { get; set; }

        public int? FKCapEtiketID_tmp { get; set; }
    }
}
