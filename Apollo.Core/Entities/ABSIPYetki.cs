namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.ABSIPYetki")]
    public partial class ABSIPYetki
    {
        public int ID { get; set; }

        [StringLength(50)]
        public string IPAdresi { get; set; }

        [StringLength(50)]
        public string AdSoyad { get; set; }

        [StringLength(50)]
        public string BirimBolum { get; set; }

        [StringLength(50)]
        public string Aciklama { get; set; }

        [StringLength(50)]
        public string KullaniciAdi { get; set; }

        [Required]
        [StringLength(50)]
        public string YetkiVeren { get; set; }

        public DateTime Tarih { get; set; }

        public bool YetkiVar { get; set; }

        public bool SonHalKaldirmaVarMi { get; set; }

        public bool? SimuleYetkisiVarMi { get; set; }
    }
}
