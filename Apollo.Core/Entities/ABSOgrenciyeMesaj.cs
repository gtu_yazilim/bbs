namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.ABSOgrenciyeMesaj")]
    public partial class ABSOgrenciyeMesaj
    {
        public int ID { get; set; }

        public int FKPersonelID { get; set; }

        public int FKOgrenciID { get; set; }

        [Required]
        public string Mesaj { get; set; }

        public DateTime GonderilmeTarihi { get; set; }

        [Required]
        [StringLength(50)]
        public string GonderenIPAdresi { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }
    }
}
