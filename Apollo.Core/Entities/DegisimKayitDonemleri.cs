namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("deg.DegisimKayitDonemleri")]
    public partial class DegisimKayitDonemleri
    {
        public int ID { get; set; }

        public int FKDegisimKayitID { get; set; }

        public int EnumDonem { get; set; }

        public virtual DegisimKayit DegisimKayit { get; set; }
    }
}
