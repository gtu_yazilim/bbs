namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.DilDersBilgi")]
    public partial class DilDersBilgi
    {
        public int ID { get; set; }

        public int? FKDersBilgiID { get; set; }

        [Column(TypeName = "text")]
        public string IcerikOnKosul { get; set; }

        [Column(TypeName = "text")]
        public string IcerikOnerilenSecmeli { get; set; }

        [Column(TypeName = "text")]
        public string IcerikDersYardimcilar { get; set; }

        [Column(TypeName = "text")]
        public string IcerikDersAmac { get; set; }

        [Column(TypeName = "text")]
        public string IcerikDers { get; set; }

        [Column(TypeName = "text")]
        public string IcerikDersNot { get; set; }

        [Column(TypeName = "text")]
        public string IcerikDersKaynak { get; set; }

        public byte? EnumDilID { get; set; }

        [StringLength(50)]
        public string Ekleyen { get; set; }

        public DateTime? Zaman { get; set; }
    }
}
