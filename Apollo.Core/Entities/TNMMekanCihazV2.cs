namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("mek.TNMMekanCihazV2")]
    public partial class TNMMekanCihazV2
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TNMMekanCihazV2()
        {
            MekanCihazV2 = new HashSet<MekanCihazV2>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(200)]
        public string CihazAd { get; set; }

        [StringLength(300)]
        public string Aciklama { get; set; }

        [Column(TypeName = "image")]
        public byte[] Fotograf { get; set; }

        public bool Aktif { get; set; }

        [Required]
        [StringLength(50)]
        public string Kullanici { get; set; }

        public DateTime Zaman { get; set; }

        public DateTime? SGZaman { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MekanCihazV2> MekanCihazV2 { get; set; }
    }
}
