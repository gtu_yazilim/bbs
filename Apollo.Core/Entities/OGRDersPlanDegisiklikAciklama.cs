namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRDersPlanDegisiklikAciklama")]
    public partial class OGRDersPlanDegisiklikAciklama
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(500)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? InKod { get; set; }

        [StringLength(350)]
        public string Aciklama { get; set; }

        public int? FKOgrenciID { get; set; }

        public int? FKDersPlanID { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public virtual OGRDersPlan OGRDersPlan { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
