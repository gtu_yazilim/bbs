namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGREnstituBasvuruTercih")]
    public partial class OGREnstituBasvuruTercih
    {
        public int ID { get; set; }

        [StringLength(11)]
        public string TcKimlikNo { get; set; }

        public int? BasvuruSeviye { get; set; }

        public int? BilimSinavi { get; set; }

        public int? SanatSinavi { get; set; }

        public double? HesaplananPuan { get; set; }

        public int? Yil { get; set; }

        public int? Donem { get; set; }

        public DateTime? KayitTarih { get; set; }

        public int? FKBirimKontenjanID { get; set; }

        public int FKKimlikID { get; set; }

        public int? Sira { get; set; }

        public int? EnumListeTipi { get; set; }

        public string Aciklama { get; set; }

        public bool KayitOldu { get; set; }

        public int YedekListeTipi { get; set; }

        public int? GeciciOgrenciNo { get; set; }

        [StringLength(20)]
        public string Makina { get; set; }

        public DateTime? SGZamani { get; set; }

        public virtual OGREnstituBasvuruKimlik OGREnstituBasvuruKimlik { get; set; }

        public virtual OGREnstituKontenjan OGREnstituKontenjan { get; set; }
    }
}
