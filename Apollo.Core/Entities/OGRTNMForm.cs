namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRTNMForm")]
    public partial class OGRTNMForm
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? InKod { get; set; }

        public int? FormNo { get; set; }

        [StringLength(250)]
        public string MenuAd { get; set; }

        [StringLength(500)]
        public string FormAd { get; set; }

        [StringLength(500)]
        public string MenuKisaYol { get; set; }

        [StringLength(500)]
        public string Aciklama { get; set; }
    }
}
