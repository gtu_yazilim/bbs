namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.WebDersCiktiYontem")]
    public partial class WebDersCiktiYontem
    {
        [Key]
        [Column(Order = 0)]
        public int InKod { get; set; }

        public byte? FormDil { get; set; }

        public byte? YontemTur { get; set; }

        [StringLength(2)]
        public string YontemNo { get; set; }

        [StringLength(150)]
        public string YontemAd { get; set; }

        [StringLength(150)]
        public string YontemUzunAd { get; set; }

        public byte? Sira { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short Yil1 { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? Zaman { get; set; }
    }
}
