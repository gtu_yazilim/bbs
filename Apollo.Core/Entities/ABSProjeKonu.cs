namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.ABSProjeKonu")]
    public partial class ABSProjeKonu
    {
        [Key]
        [Column(Order = 0)]
        public int ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ProjeID { get; set; }

        [StringLength(4000)]
        public string KonuBaslik { get; set; }

        [Key]
        [Column(Order = 2)]
        public string Konu { get; set; }

        public int? Kapasite { get; set; }

        public int? OgrenciSayisi { get; set; }
    }
}
