namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ytk.KullaniciCihaz")]
    public partial class KullaniciCihaz
    {
        public int ID { get; set; }

        public int FKKullaniciID { get; set; }

        [Required]
        [StringLength(50)]
        public string CihazID { get; set; }

        public DateTime Tarih { get; set; }

        public bool Aktif { get; set; }

        public DateTime? TarihPasif { get; set; }

        public DateTime TarihKayit { get; set; }

        [Required]
        public string BildirimID { get; set; }

        public bool BildirimAcik { get; set; }

        public int EnumMobilCihazTipi { get; set; }

        public virtual Kullanici Kullanici { get; set; }
    }
}
