namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRAkademikTakvim")]
    public partial class OGRAkademikTakvim
    {
        public int ID { get; set; }

        public DateTime TarihKayit { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        public int EnumAkademikTarih { get; set; }

        public int FKBirimID { get; set; }

        public DateTime TarihBaslangic { get; set; }

        public DateTime? TarihBitis { get; set; }

        public int Yil { get; set; }

        public int EnumDonem { get; set; }

        [StringLength(250)]
        public string Aciklama { get; set; }

        public virtual Birimler Birimler { get; set; }
    }
}
