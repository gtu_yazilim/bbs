namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.WebYetkiler")]
    public partial class WebYetkiler
    {
        [Key]
        public int InKod { get; set; }

        [StringLength(100)]
        public string YetkiAdi { get; set; }

        public byte? YetkiYeri { get; set; }
    }
}
