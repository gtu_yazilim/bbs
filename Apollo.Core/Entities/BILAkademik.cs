namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.BILAkademik")]
    public partial class BILAkademik
    {
        public int id { get; set; }

        [StringLength(2)]
        public string YT { get; set; }

        public int? YT1 { get; set; }

        [StringLength(255)]
        public string AU { get; set; }

        [StringLength(255)]
        public string TI { get; set; }

        [StringLength(255)]
        public string SO { get; set; }

        [StringLength(255)]
        public string CT { get; set; }

        [StringLength(100)]
        public string CY { get; set; }

        [StringLength(100)]
        public string CL { get; set; }

        public double? TC { get; set; }

        [StringLength(50)]
        public string SN { get; set; }

        [StringLength(50)]
        public string BN { get; set; }

        [StringLength(255)]
        public string JI { get; set; }

        [StringLength(30)]
        public string PD { get; set; }

        [StringLength(4)]
        public string PY { get; set; }

        public double? VL { get; set; }

        public double? BP { get; set; }

        public double? EP { get; set; }

        [StringLength(50)]
        public string DI { get; set; }

        [StringLength(50)]
        public string UT { get; set; }

        [StringLength(255)]
        public string link { get; set; }

        [StringLength(25)]
        public string kullanici { get; set; }

        public DateTime? tarih { get; set; }

        [StringLength(11)]
        public string y1 { get; set; }

        [StringLength(11)]
        public string y2 { get; set; }

        [StringLength(11)]
        public string y3 { get; set; }

        [StringLength(11)]
        public string y4 { get; set; }

        [StringLength(11)]
        public string y5 { get; set; }

        [StringLength(11)]
        public string y6 { get; set; }

        [StringLength(11)]
        public string y7 { get; set; }

        [StringLength(11)]
        public string y8 { get; set; }

        [StringLength(11)]
        public string y9 { get; set; }

        [StringLength(11)]
        public string y10 { get; set; }

        public DateTime? tarih1 { get; set; }

        public DateTime? tarih2 { get; set; }

        public byte? kisi { get; set; }

        public bool? sau { get; set; }
    }
}
