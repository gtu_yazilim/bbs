namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRTNMEskiRektor")]
    public partial class OGRTNMEskiRektor
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? InKod { get; set; }

        [StringLength(250)]
        public string Unvan { get; set; }

        [StringLength(250)]
        public string Makam { get; set; }

        [StringLength(500)]
        public string AdSoyad { get; set; }

        public DateTime? BaslamaTarihi { get; set; }

        public DateTime? BitisTarihi { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }
    }
}
