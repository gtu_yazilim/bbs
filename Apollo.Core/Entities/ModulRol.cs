namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ytk.ModulRol")]
    public partial class ModulRol
    {
        public int ID { get; set; }

        public int FKModulID { get; set; }

        public int FKRolID { get; set; }

        public virtual Modul Modul { get; set; }

        public virtual Rol Rol { get; set; }
    }
}
