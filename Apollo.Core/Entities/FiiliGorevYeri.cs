namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.FiiliGorevYeri")]
    public partial class FiiliGorevYeri
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? InKod { get; set; }

        public DateTime? GorevBaslangicTarihi { get; set; }

        public DateTime? GorevBitisTarihi { get; set; }

        public bool? GorevSureliMi { get; set; }

        public bool? KadroSahsaBagliMi { get; set; }

        public bool? KadroStatuUcretliMi { get; set; }

        public int? KullaniciKod { get; set; }

        public DateTime? IslemTarihi { get; set; }

        public int? ENUMSozlesmeStatu { get; set; }

        public int? ENUMSozlesmeDersStatu { get; set; }

        public int FKPersonelBilgisiID { get; set; }

        public int? FKGorYerUlkeID { get; set; }

        public int? FKGorYerUniversiteID { get; set; }

        public int? FKGorYerFakulteID { get; set; }

        public int? FKGorYerBolumID { get; set; }

        public int? FKGorYerAnaBilimDaliID { get; set; }

        public int? FKKanunMaddeNoID { get; set; }

        public virtual Birimler Birimler { get; set; }

        public virtual Birimler Birimler1 { get; set; }

        public virtual Birimler Birimler2 { get; set; }

        public virtual Birimler Birimler3 { get; set; }

        public virtual TNMKanunMaddeNo TNMKanunMaddeNo { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }
    }
}
