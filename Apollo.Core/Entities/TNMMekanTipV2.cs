namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("mek.TNMMekanTipV2")]
    public partial class TNMMekanTipV2
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TNMMekanTipV2()
        {
            MekanV2 = new HashSet<MekanV2>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(200)]
        public string MekanTip { get; set; }

        public int? Sira { get; set; }

        public bool Aktif { get; set; }

        [Required]
        [StringLength(50)]
        public string Kullanici { get; set; }

        public DateTime Zaman { get; set; }

        public DateTime? SGZaman { get; set; }

        public int? Kapsayici { get; set; }

        public string PolygonOzellikleriJson { get; set; }

        public bool? AcilistaGoster { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MekanV2> MekanV2 { get; set; }
    }
}
