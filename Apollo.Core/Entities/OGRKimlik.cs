namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRKimlik")]
    public partial class OGRKimlik
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OGRKimlik()
        {
            ABSBasariNot = new HashSet<ABSBasariNot>();
            ABSDanismanOnay = new HashSet<ABSDanismanOnay>();
            ABSDevamTakipKontrol = new HashSet<ABSDevamTakipKontrol>();
            ABSDevamTakipV2 = new HashSet<ABSDevamTakipV2>();
            ABSGrupCalisma = new HashSet<ABSGrupCalisma>();
            ABSOgrenciDanismanlik = new HashSet<ABSOgrenciDanismanlik>();
            ABSOgrenciIleGorusme = new HashSet<ABSOgrenciIleGorusme>();
            ABSOgrenciNot = new HashSet<ABSOgrenciNot>();
            ABSOgrenciNotDetay = new HashSet<ABSOgrenciNotDetay>();
            ABSOgrenciyeMesaj = new HashSet<ABSOgrenciyeMesaj>();
            DanismanlikBilgileri = new HashSet<DanismanlikBilgileri>();
            OgrenciDersCevap = new HashSet<OgrenciDersCevap>();
            CapYandalTercih = new HashSet<CapYandalTercih>();
            EnkaBarinmaBursu = new HashSet<EnkaBarinmaBursu>();
            GecisTercih = new HashSet<GecisTercih>();
            HazirlikBasvuru = new HashSet<HazirlikBasvuru>();
            KitapBursu = new HashSet<KitapBursu>();
            MuafiyetSinavBasvuru = new HashSet<MuafiyetSinavBasvuru>();
            MuafiyetSinavTIN = new HashSet<MuafiyetSinavTIN>();
            OgrenciEtkinlikBilgiFormu = new HashSet<OgrenciEtkinlikBilgiFormu>();
            OnlineHarcBorclari = new HashSet<OnlineHarcBorclari>();
            OnlineHarcOdemeleri = new HashSet<OnlineHarcOdemeleri>();
            DegisimKayit = new HashSet<DegisimKayit>();
            AdresBilgisi = new HashSet<AdresBilgisi>();
            TelefonBilgisi = new HashSet<TelefonBilgisi>();
            FormasyonBasvuru = new HashSet<FormasyonBasvuru>();
            OBSIliskiKesme = new HashSet<OBSIliskiKesme>();
            OBSMezuniyetAnketi = new HashSet<OBSMezuniyetAnketi>();
            OGRArsiv = new HashSet<OGRArsiv>();
            OGRBelgeSayiOnur = new HashSet<OGRBelgeSayiOnur>();
            OGRDanismanGorusme = new HashSet<OGRDanismanGorusme>();
            OGRDanismanlik = new HashSet<OGRDanismanlik>();
            OGRDanismanMesaj = new HashSet<OGRDanismanMesaj>();
            OGRDegisim = new HashSet<OGRDegisim>();
            OGRDersPlanDegisiklikAciklama = new HashSet<OGRDersPlanDegisiklikAciklama>();
            OGRDiplomaNo = new HashSet<OGRDiplomaNo>();
            OGRDisiplinCeza = new HashSet<OGRDisiplinCeza>();
            OGRDondurmaCeza = new HashSet<OGRDondurmaCeza>();
            OGRDuplicateNo = new HashSet<OGRDuplicateNo>();
            OGREgitimBilgileri = new HashSet<OGREgitimBilgileri>();
            OGREnstituBilimselHazirlik = new HashSet<OGREnstituBilimselHazirlik>();
            OGREnstituDanisman = new HashSet<OGREnstituDanisman>();
            OGREnstituDonemAtlatma = new HashSet<OGREnstituDonemAtlatma>();
            OGREnstituIngilizceHazirlik = new HashSet<OGREnstituIngilizceHazirlik>();
            OGREnstituKomitelerJuriler = new HashSet<OGREnstituKomitelerJuriler>();
            OGREnstituSeminer = new HashSet<OGREnstituSeminer>();
            OGREnstituTezIzlemeKomitesi = new HashSet<OGREnstituTezIzlemeKomitesi>();
            OGREnstituTezKonusu = new HashSet<OGREnstituTezKonusu>();
            OGREnstituTezTizTosSinavi = new HashSet<OGREnstituTezTizTosSinavi>();
            OGREnstituTIZ = new HashSet<OGREnstituTIZ>();
            OGREnstituTOS = new HashSet<OGREnstituTOS>();
            OGREnstituYayin = new HashSet<OGREnstituYayin>();
            OGREnstituYeterlik = new HashSet<OGREnstituYeterlik>();
            OGRGeciciOgrenciNumaralari = new HashSet<OGRGeciciOgrenciNumaralari>();
            OGRHarcBankadanGelen = new HashSet<OGRHarcBankadanGelen>();
            OGRHarcBankayaGiden = new HashSet<OGRHarcBankayaGiden>();
            OGRHarcBilgi = new HashSet<OGRHarcBilgi>();
            OGRHarcDersUcreti = new HashSet<OGRHarcDersUcreti>();
            OGRHarcEskiBorc = new HashSet<OGRHarcEskiBorc>();
            OGRHarcEskiBorcSilinen = new HashSet<OGRHarcEskiBorcSilinen>();
            OGRHarcIade = new HashSet<OGRHarcIade>();
            OGRHarcKrediUcretlendirmesiBorc = new HashSet<OGRHarcKrediUcretlendirmesiBorc>();
            OGRHarcOdeme = new HashSet<OGRHarcOdeme>();
            OGRHarcYuzdeOn = new HashSet<OGRHarcYuzdeOn>();
            OGRHazirlik = new HashSet<OGRHazirlik>();
            OGRKayitBelgeleri = new HashSet<OGRKayitBelgeleri>();
            OGRKayitDondurma = new HashSet<OGRKayitDondurma>();
            ABSGeriBildirim = new HashSet<ABSGeriBildirim>();
            OGRKimlikFarkliGelis = new HashSet<OGRKimlikFarkliGelis>();
            Kullanici1 = new HashSet<Kullanici>();
            OGROgrenciHatirlatma = new HashSet<OGROgrenciHatirlatma>();
            OGROgrenciSorun = new HashSet<OGROgrenciSorun>();
            OGROgrenciYazilma = new HashSet<OGROgrenciYazilma>();
            OGRKimlik1 = new HashSet<OGRKimlik>();
            OGRKimlikDetay = new HashSet<OGRKimlikDetay>();
            OGRKYKBurs = new HashSet<OGRKYKBurs>();
            OGRKYKOgrenim = new HashSet<OGRKYKOgrenim>();
            OGRMazeret = new HashSet<OGRMazeret>();
            OGROgrenciDersPlan = new HashSet<OGROgrenciDersPlan>();
            OGROgrenciEtiket = new HashSet<OGROgrenciEtiket>();
            OGROgrenciGecmis = new HashSet<OGROgrenciGecmis>();
            OGROgrenciNot = new HashSet<OGROgrenciNot>();
            OGROgrenciOrtalama = new HashSet<OGROgrenciOrtalama>();
            OGROgrenciTemsilci = new HashSet<OGROgrenciTemsilci>();
            OgrOgrenciYonetmelik = new HashSet<OgrOgrenciYonetmelik>();
            OGROzelOgrenciExceldenTranskript = new HashSet<OGROzelOgrenciExceldenTranskript>();
            OGRStaj = new HashSet<OGRStaj>();
            OGRTekDers = new HashSet<OGRTekDers>();
            OGRYazilmaDanismanOnay = new HashSet<OGRYazilmaDanismanOnay>();
            OOdevOgrenci = new HashSet<OOdevOgrenci>();
            OsymOgrenciBasari = new HashSet<OsymOgrenciBasari>();
            OGRSakatlikBilgi = new HashSet<OGRSakatlikBilgi>();
            YOKAktarim = new HashSet<YOKAktarim>();
            YoksisAktarimDurumu = new HashSet<YoksisAktarimDurumu>();
        }

        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(100)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public int? InKod { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        [Required]
        [StringLength(11)]
        public string Numara { get; set; }

        public int? GirisYil { get; set; }

        public DateTime? KayitTarih { get; set; }

        [Required]
        [StringLength(2)]
        public string Sinif { get; set; }

        [StringLength(11)]
        public string AnadalNo { get; set; }

        public int? FKAnadalID { get; set; }

        public int AktifYariyil { get; set; }

        [StringLength(10)]
        public string DanismanSicilNo { get; set; }

        public int EnumOgrDurum { get; set; }

        public int? EnumAskerlikDurumu { get; set; }

        public int EnumKayitNedeni { get; set; }

        public int? EnumHarcDurum { get; set; }

        public int? EnumSinirsizSinav { get; set; }

        public int? EnumMezuniyetAsamasi { get; set; }

        public int? EnumOgrencilikAsamasi { get; set; }

        public int? EnumOgrencilikStatusu { get; set; }

        public int? FKDanismanID { get; set; }

        [StringLength(100)]
        public string foto { get; set; }

        [StringLength(50)]
        public string Asoy { get; set; }

        [StringLength(100)]
        public string BilimDali { get; set; }

        public int? FKBilimID { get; set; }

        public int? ToplamAKTS { get; set; }

        public double? ToplamAgirlik { get; set; }

        public double? GenelOrtalama { get; set; }

        [StringLength(7)]
        public string OgrenciID { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public DateTime? SGZaman { get; set; }

        public int? ArtiYariyil { get; set; }

        public int? EksiYariyil { get; set; }

        public int? EnumStajAsamasi { get; set; }

        public int FKBolumPrBirimID { get; set; }

        public int? EnumKayitDonem { get; set; }

        public int FKKisiID { get; set; }

        public int? FKFiiliPrID { get; set; }

        [StringLength(255)]
        public string KullaniciAdi { get; set; }

        public int? EnumYOKKayitNedeni { get; set; }

        public bool SilindiMi { get; set; }

        public int? HarcYariyil { get; set; }

        public int? AzamiYariyil { get; set; }

        public int FKBirimID { get; set; }

        public int FKKayitBirimID { get; set; }

        public int FKDiplomaBirimID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSBasariNot> ABSBasariNot { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSDanismanOnay> ABSDanismanOnay { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSDevamTakipKontrol> ABSDevamTakipKontrol { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSDevamTakipV2> ABSDevamTakipV2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSGrupCalisma> ABSGrupCalisma { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSOgrenciDanismanlik> ABSOgrenciDanismanlik { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSOgrenciIleGorusme> ABSOgrenciIleGorusme { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSOgrenciNot> ABSOgrenciNot { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSOgrenciNotDetay> ABSOgrenciNotDetay { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSOgrenciyeMesaj> ABSOgrenciyeMesaj { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DanismanlikBilgileri> DanismanlikBilgileri { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OgrenciDersCevap> OgrenciDersCevap { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CapYandalTercih> CapYandalTercih { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EnkaBarinmaBursu> EnkaBarinmaBursu { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GecisTercih> GecisTercih { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HazirlikBasvuru> HazirlikBasvuru { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<KitapBursu> KitapBursu { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MuafiyetSinavBasvuru> MuafiyetSinavBasvuru { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MuafiyetSinavTIN> MuafiyetSinavTIN { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OgrenciEtkinlikBilgiFormu> OgrenciEtkinlikBilgiFormu { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OnlineHarcBorclari> OnlineHarcBorclari { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OnlineHarcOdemeleri> OnlineHarcOdemeleri { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DegisimKayit> DegisimKayit { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AdresBilgisi> AdresBilgisi { get; set; }

        public virtual Birimler Birimler { get; set; }

        public virtual Birimler Birimler1 { get; set; }

        public virtual Birimler Birimler2 { get; set; }

        public virtual Birimler Birimler3 { get; set; }

        public virtual Kisi Kisi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TelefonBilgisi> TelefonBilgisi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FormasyonBasvuru> FormasyonBasvuru { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OBSIliskiKesme> OBSIliskiKesme { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OBSMezuniyetAnketi> OBSMezuniyetAnketi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRArsiv> OGRArsiv { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRBelgeSayiOnur> OGRBelgeSayiOnur { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRDanismanGorusme> OGRDanismanGorusme { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRDanismanlik> OGRDanismanlik { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRDanismanMesaj> OGRDanismanMesaj { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRDegisim> OGRDegisim { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRDersPlanDegisiklikAciklama> OGRDersPlanDegisiklikAciklama { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRDiplomaNo> OGRDiplomaNo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRDisiplinCeza> OGRDisiplinCeza { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRDondurmaCeza> OGRDondurmaCeza { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRDuplicateNo> OGRDuplicateNo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREgitimBilgileri> OGREgitimBilgileri { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituBilimselHazirlik> OGREnstituBilimselHazirlik { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituDanisman> OGREnstituDanisman { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituDonemAtlatma> OGREnstituDonemAtlatma { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituIngilizceHazirlik> OGREnstituIngilizceHazirlik { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituKomitelerJuriler> OGREnstituKomitelerJuriler { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituSeminer> OGREnstituSeminer { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituTezIzlemeKomitesi> OGREnstituTezIzlemeKomitesi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituTezKonusu> OGREnstituTezKonusu { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituTezTizTosSinavi> OGREnstituTezTizTosSinavi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituTIZ> OGREnstituTIZ { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituTOS> OGREnstituTOS { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituYayin> OGREnstituYayin { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituYeterlik> OGREnstituYeterlik { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRGeciciOgrenciNumaralari> OGRGeciciOgrenciNumaralari { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRHarcBankadanGelen> OGRHarcBankadanGelen { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRHarcBankayaGiden> OGRHarcBankayaGiden { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRHarcBilgi> OGRHarcBilgi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRHarcDersUcreti> OGRHarcDersUcreti { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRHarcEskiBorc> OGRHarcEskiBorc { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRHarcEskiBorcSilinen> OGRHarcEskiBorcSilinen { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRHarcIade> OGRHarcIade { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRHarcKrediUcretlendirmesiBorc> OGRHarcKrediUcretlendirmesiBorc { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRHarcOdeme> OGRHarcOdeme { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRHarcYuzdeOn> OGRHarcYuzdeOn { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRHazirlik> OGRHazirlik { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRKayitBelgeleri> OGRKayitBelgeleri { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRKayitDondurma> OGRKayitDondurma { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSGeriBildirim> ABSGeriBildirim { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRKimlikFarkliGelis> OGRKimlikFarkliGelis { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Kullanici> Kullanici1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGROgrenciHatirlatma> OGROgrenciHatirlatma { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGROgrenciSorun> OGROgrenciSorun { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGROgrenciYazilma> OGROgrenciYazilma { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRKimlik> OGRKimlik1 { get; set; }

        public virtual OGRKimlik OGRKimlik2 { get; set; }

        public virtual OGRTNMBilimDali OGRTNMBilimDali { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRKimlikDetay> OGRKimlikDetay { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRKYKBurs> OGRKYKBurs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRKYKOgrenim> OGRKYKOgrenim { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRMazeret> OGRMazeret { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGROgrenciDersPlan> OGROgrenciDersPlan { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGROgrenciEtiket> OGROgrenciEtiket { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGROgrenciGecmis> OGROgrenciGecmis { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGROgrenciNot> OGROgrenciNot { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGROgrenciOrtalama> OGROgrenciOrtalama { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGROgrenciTemsilci> OGROgrenciTemsilci { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OgrOgrenciYonetmelik> OgrOgrenciYonetmelik { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGROzelOgrenciExceldenTranskript> OGROzelOgrenciExceldenTranskript { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRStaj> OGRStaj { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRTekDers> OGRTekDers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRYazilmaDanismanOnay> OGRYazilmaDanismanOnay { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OOdevOgrenci> OOdevOgrenci { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OsymOgrenciBasari> OsymOgrenciBasari { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRSakatlikBilgi> OGRSakatlikBilgi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<YOKAktarim> YOKAktarim { get; set; }

        public virtual YOKBildirilenOgrenciler YOKBildirilenOgrenciler { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<YoksisAktarimDurumu> YoksisAktarimDurumu { get; set; }
    }
}
