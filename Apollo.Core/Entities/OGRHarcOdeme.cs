namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRHarcOdeme")]
    public partial class OGRHarcOdeme
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(500)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? InKod { get; set; }

        public double AnaPara { get; set; }

        public double Faiz { get; set; }

        public DateTime Tarih { get; set; }

        public int Yil { get; set; }

        public int AitYil { get; set; }

        public int EnumDonemOdeme { get; set; }

        public int EnumDonemAit { get; set; }

        public int FKOgrenciID { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        [StringLength(250)]
        public string Aciklama { get; set; }

        public int? FKOdemeID { get; set; }

        public bool Silindi { get; set; }

        public virtual OnlineHarcOdemeleri OnlineHarcOdemeleri { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
