namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("basvuru.MuafiyetSinavTanim")]
    public partial class MuafiyetSinavTanim
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MuafiyetSinavTanim()
        {
            MuafiyetSinavBasvuru = new HashSet<MuafiyetSinavBasvuru>();
            MuafiyetSinavOturum = new HashSet<MuafiyetSinavOturum>();
        }

        public int ID { get; set; }

        public int Yil { get; set; }

        public int EnumDonem { get; set; }

        public int Sira { get; set; }

        public DateTime TarihKayit { get; set; }

        public DateTime? TarihGuncelleme { get; set; }

        [Required]
        [StringLength(500)]
        public string Kullanici { get; set; }

        [Required]
        [StringLength(50)]
        public string IP { get; set; }

        public DateTime TarihBasvuruBaslangic { get; set; }

        public DateTime TarihBasvuruBitis { get; set; }

        public DateTime TarihSinavBaslangic { get; set; }

        public DateTime TarihSinavBitis { get; set; }

        public bool RandevuSecimi { get; set; }

        public int? FinalDersplanAnaID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MuafiyetSinavBasvuru> MuafiyetSinavBasvuru { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MuafiyetSinavOturum> MuafiyetSinavOturum { get; set; }
    }
}
