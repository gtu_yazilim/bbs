namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.DuzeyYeterlilik")]
    public partial class DuzeyYeterlilik
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DuzeyYeterlilik()
        {
            DilDuzeyYeterlilik = new HashSet<DilDuzeyYeterlilik>();
        }

        public int ID { get; set; }

        public byte? EnumDuzey { get; set; }

        public int? FKYeterlilikAltKategoriID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DilDuzeyYeterlilik> DilDuzeyYeterlilik { get; set; }

        public virtual YeterlilikAltKategori YeterlilikAltKategori { get; set; }
    }
}
