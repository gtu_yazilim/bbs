namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGROgrenciNot")]
    public partial class OGROgrenciNot
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OGROgrenciNot()
        {
            OGROgrenciDersPlan = new HashSet<OGROgrenciDersPlan>();
            OGROgrenciNot1 = new HashSet<OGROgrenciNot>();
            OGROgrenciYazilma = new HashSet<OGROgrenciYazilma>();
        }

        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(500)]
        public string Kullanici { get; set; }

        [StringLength(500)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? InKod { get; set; }

        public int? EskiNotInKod { get; set; }

        public int? AlisSayi { get; set; }

        public int? Yariyil { get; set; }

        public int? Yil { get; set; }

        public int? AktifAlisSayisi { get; set; }

        public int? EnumSinav { get; set; }

        public int? EnumOgrenciMazeretNot { get; set; }

        public int? EnumOgrenciBasariNot { get; set; }

        public int? EnumOgrenciNihaiNot { get; set; }

        public int? EnumOgrenciNotDurum { get; set; }

        public int? EnumOgrenciNotEnSon { get; set; }

        public int? EnumDonem { get; set; }

        public int? FKOgrenciID { get; set; }

        public int? FKDersPlanAnaID { get; set; }

        public int? FKDersPlanID { get; set; }

        public int? FKDersGrupID { get; set; }

        public int? FKOgrenciNotID { get; set; }

        public int? FKABSBasariNot { get; set; }

        public int? FKBarkodOkuma { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public double? YuzlukNot { get; set; }

        public double? DortlukNot { get; set; }

        public int? ArsivID { get; set; }

        public int? ArsivFKNotID { get; set; }

        public int? FKOgrenciPlanID { get; set; }

        public int? FKKurulKararID { get; set; }

        public bool Silindi { get; set; }

        public virtual ABSBasariNot ABSBasariNot { get; set; }

        public virtual OGRBarkodOkuma OGRBarkodOkuma { get; set; }

        public virtual OGRDersGrup OGRDersGrup { get; set; }

        public virtual OGRDersPlan OGRDersPlan { get; set; }

        public virtual OGRDersPlanAna OGRDersPlanAna { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }

        public virtual OGRKurulKarar OGRKurulKarar { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGROgrenciDersPlan> OGROgrenciDersPlan { get; set; }

        public virtual OGROgrenciDersPlan OGROgrenciDersPlan1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGROgrenciNot> OGROgrenciNot1 { get; set; }

        public virtual OGROgrenciNot OGROgrenciNot2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGROgrenciYazilma> OGROgrenciYazilma { get; set; }
    }
}
