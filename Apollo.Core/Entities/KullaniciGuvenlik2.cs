namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ytk.KullaniciGuvenlik2")]
    public partial class KullaniciGuvenlik2
    {
        public int ID { get; set; }

        public int FKKullaniciID { get; set; }

        [Required]
        [StringLength(250)]
        public string Bilgi { get; set; }

        public DateTime TarihEkleme { get; set; }

        public DateTime TarihGuncelleme { get; set; }

        public virtual Kullanici Kullanici { get; set; }
    }
}
