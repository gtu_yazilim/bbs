namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.OSinavCevap")]
    public partial class OSinavCevap
    {
        [Key]
        [Column(Order = 0)]
        public int CevapID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SinavID { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SoruID { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EID { get; set; }

        [Key]
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int OgrenciID { get; set; }

        public DateTime? CevapTarih { get; set; }

        public string CevapText { get; set; }

        public int? Cevap { get; set; }

        [StringLength(50)]
        public string CevapIP { get; set; }

        [Key]
        [Column(Order = 5)]
        public DateTime EklenmeTarihi { get; set; }

        public int? EkleyenID { get; set; }

        public bool? CevapDogru { get; set; }

        [Key]
        [Column(Order = 6)]
        public bool Silindi { get; set; }

        [Key]
        [Column(Order = 7)]
        public bool MazeretSoru { get; set; }

        [Key]
        [Column(Order = 8)]
        public bool Iptal { get; set; }

        public virtual OSinav OSinav { get; set; }
    }
}
