namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRDiplomaNo")]
    public partial class OGRDiplomaNo
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(500)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? InKod { get; set; }

        [StringLength(50)]
        public string DiplomaNo1 { get; set; }

        [StringLength(50)]
        public string DiplomaNo2 { get; set; }

        [StringLength(50)]
        public string DiplomaNo3 { get; set; }

        [StringLength(50)]
        public string DiplomaNo4 { get; set; }

        [StringLength(50)]
        public string DiplomaNo5 { get; set; }

        public DateTime? VerilisTarihi { get; set; }

        public int? FKOgrenciID { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public bool Iptal { get; set; }

        public int? FKBelgeDogrulamaID { get; set; }

        [StringLength(50)]
        public string DosyaKey { get; set; }

        [StringLength(500)]
        public string DiplomaAd { get; set; }

        [StringLength(500)]
        public string DiplomaSoyad { get; set; }

        public string DiplomaIcerikJson { get; set; }

        public int EnumDiplomaDurum { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
