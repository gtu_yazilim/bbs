namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("gen.Sayilar")]
    public partial class Sayilar
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        public int? Kategori { get; set; }

        [StringLength(100)]
        public string AciklamaTR { get; set; }

        [StringLength(100)]
        public string AciklamaEN { get; set; }

        [StringLength(20)]
        public string sira { get; set; }

        [StringLength(20)]
        public string deger1 { get; set; }

        [StringLength(20)]
        public string deger2 { get; set; }

        [StringLength(20)]
        public string deger3 { get; set; }

        [StringLength(20)]
        public string deger4 { get; set; }

        [StringLength(20)]
        public string deger0 { get; set; }

        [StringLength(20)]
        public string deger5 { get; set; }
    }
}
