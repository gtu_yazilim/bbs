namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("randevu.RandevuOturum")]
    public partial class RandevuOturum
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public RandevuOturum()
        {
            RandevuAyar = new HashSet<RandevuAyar>();
            RandevuKayit = new HashSet<RandevuKayit>();
            PayRandevu = new HashSet<PayRandevu>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(250)]
        public string Ad { get; set; }

        public DateTime TarihKayit { get; set; }

        public int? FKMekanID { get; set; }

        public string Aciklama { get; set; }

        public DateTime SonKayitTarihi { get; set; }

        public DateTime BaslangicTarihi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RandevuAyar> RandevuAyar { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RandevuKayit> RandevuKayit { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PayRandevu> PayRandevu { get; set; }
    }
}
