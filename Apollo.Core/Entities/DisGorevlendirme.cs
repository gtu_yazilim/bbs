namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.DisGorevlendirme")]
    public partial class DisGorevlendirme
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        public string Kullanici { get; set; }

        public string Makina { get; set; }

        public int? KayitNo { get; set; }

        public int? SiraNo { get; set; }

        public int FKPersonelBilgisiID { get; set; }

        public int? FKAkademikUnvanID { get; set; }

        public int? FKKadroBirimiID { get; set; }

        public int? FKUlkeID { get; set; }

        public int? FKIlID { get; set; }

        public int? FKIlceID { get; set; }

        [StringLength(100)]
        public string UlkeSehir { get; set; }

        public string KurumAdi { get; set; }

        public string BirimAdi { get; set; }

        public string KonferansAdi { get; set; }

        [StringLength(20)]
        public string KonferansDonemi { get; set; }

        public string BildiriAdi { get; set; }

        public DateTime? BaslangicTarihi { get; set; }

        public DateTime? BitisTarihi { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? GoreveBaslamaTarihi { get; set; }

        public bool? Yolluklu { get; set; }

        public bool? Yevmiyeli { get; set; }

        public bool? Maasli { get; set; }

        public bool? Izinli { get; set; }

        public string Aciklama { get; set; }

        [StringLength(250)]
        public string Durum { get; set; }

        [StringLength(50)]
        public string Sure { get; set; }

        public int FKKanunMaddeNoID { get; set; }

        public int? ENUMGorevSuresi { get; set; }

        [StringLength(255)]
        public string KayitEdildigiAy { get; set; }

        public bool? YOKeGonderildiMi { get; set; }

        public int? ENUMGorevlendirmeYeri { get; set; }

        public bool? OnRapor { get; set; }

        [StringLength(255)]
        public string OnRaporNo { get; set; }

        public bool? DonusRaporu { get; set; }

        [StringLength(255)]
        public string DonusRaporuNo { get; set; }

        public DateTime? YOKYaziTarihi { get; set; }

        [StringLength(20)]
        public string YOKYaziEvrakNo { get; set; }

        [StringLength(20)]
        public string YOKYaziSayisi { get; set; }

        public DateTime? UYKTarih { get; set; }

        [StringLength(20)]
        public string UYKNo { get; set; }

        public DateTime? FYKTarih { get; set; }

        [StringLength(20)]
        public string FYKNo { get; set; }

        public int? FKDisKurumID { get; set; }

        [StringLength(50)]
        public string Ad { get; set; }

        [StringLength(50)]
        public string Soyad { get; set; }

        public double? DestekTutari { get; set; }

        public int? ENUMGorevlendirmeDurumu { get; set; }

        public DateTime? IptalTarihi { get; set; }

        public int ENUMGidenGelen { get; set; }

        public bool Silindi { get; set; }

        public virtual Birimler Birimler { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }

        public virtual TNMDisKurum TNMDisKurum { get; set; }

        public virtual TNMDisKurum TNMDisKurum1 { get; set; }

        public virtual TNMKanunMaddeNo TNMKanunMaddeNo { get; set; }

        public virtual TNMUnvan TNMUnvan { get; set; }
    }
}
