namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.DersKoordinator")]
    public partial class DersKoordinator
    {
        public int ID { get; set; }

        public int FKPersonelID { get; set; }

        public int? FKDersBilgiID { get; set; }

        public int FKDersPlanAnaID { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime BaslangicTarih { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? BitisTarih { get; set; }

        [StringLength(30)]
        public string Ekleyen { get; set; }

        public DateTime? Zaman { get; set; }

        public bool Silindi { get; set; }

        public virtual DersBilgi DersBilgi { get; set; }

        public virtual OGRDersPlanAna OGRDersPlanAna { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }
    }
}
