namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.WebBolumYeterlilik")]
    public partial class WebBolumYeterlilik
    {
        [Key]
        [Column(Order = 0)]
        public int InKod { get; set; }

        [StringLength(2)]
        public string FakKod { get; set; }

        [StringLength(2)]
        public string BolKod { get; set; }

        public byte? FormDil { get; set; }

        public byte? OgretimSeviye { get; set; }

        public int? YetId { get; set; }

        [Column(TypeName = "text")]
        public string YetIcerik { get; set; }

        public byte? Dil { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short Yil1 { get; set; }

        [StringLength(20)]
        public string Ekleyen { get; set; }

        public DateTime? Tarih { get; set; }

        public int? FKFakulteBirimID { get; set; }

        public int? FKBolumBirimID { get; set; }

        public int? FKProgramBirimID { get; set; }
    }
}
