namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.TNMSendika")]
    public partial class TNMSendika
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TNMSendika()
        {
            Sendika = new HashSet<Sendika>();
        }

        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        [StringLength(4)]
        public string KonfederasyonKodu { get; set; }

        [Required]
        [StringLength(4)]
        public string SendikaKodu { get; set; }

        [Required]
        [StringLength(100)]
        public string Ad { get; set; }

        [Required]
        [StringLength(20)]
        public string KisaAd { get; set; }

        [StringLength(50)]
        public string Adres1 { get; set; }

        [StringLength(50)]
        public string Adres2 { get; set; }

        [StringLength(10)]
        public string BankaSubeKodu { get; set; }

        [StringLength(26)]
        public string BankaIBAN { get; set; }

        [StringLength(2)]
        public string EKod1 { get; set; }

        [StringLength(2)]
        public string EKod2 { get; set; }

        [StringLength(2)]
        public string EKod3 { get; set; }

        [StringLength(2)]
        public string EKod4 { get; set; }

        [StringLength(5)]
        public string GelirKodu { get; set; }

        public float KesintiOrani { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Sendika> Sendika { get; set; }
    }
}
