namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("randevu.RandevuKayit")]
    public partial class RandevuKayit
    {
        public int ID { get; set; }

        public int FKRandevuOturumID { get; set; }

        public int FKKullaniciID { get; set; }

        public DateTime TarihRandevu { get; set; }

        public int Sira { get; set; }

        public DateTime TarihKayit { get; set; }

        public bool Iptal { get; set; }

        public DateTime? TarihIptal { get; set; }

        public int FKAyarID { get; set; }

        public bool OtomatikAtama { get; set; }

        public virtual RandevuAyar RandevuAyar { get; set; }

        public virtual Kullanici Kullanici { get; set; }

        public virtual RandevuOturum RandevuOturum { get; set; }
    }
}
