namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ytk.SifreDogrulamaServisLog")]
    public partial class SifreDogrulamaServisLog
    {
        public int ID { get; set; }

        public DateTime Tarih { get; set; }

        [Required]
        [StringLength(50)]
        public string KullaniciAdi { get; set; }

        [Required]
        [StringLength(50)]
        public string UygulamaAdi { get; set; }

        [Required]
        [StringLength(50)]
        public string IP { get; set; }

        public bool? Sonuc { get; set; }

        public string LogData { get; set; }
    }
}
