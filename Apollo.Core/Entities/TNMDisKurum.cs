namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.TNMDisKurum")]
    public partial class TNMDisKurum
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TNMDisKurum()
        {
            DisGorevlendirme = new HashSet<DisGorevlendirme>();
            DisGorevlendirme1 = new HashSet<DisGorevlendirme>();
            TNMDisKurum1 = new HashSet<TNMDisKurum>();
        }

        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(255)]
        public string KurumAd { get; set; }

        public int? FKUstID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DisGorevlendirme> DisGorevlendirme { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DisGorevlendirme> DisGorevlendirme1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TNMDisKurum> TNMDisKurum1 { get; set; }

        public virtual TNMDisKurum TNMDisKurum2 { get; set; }
    }
}
