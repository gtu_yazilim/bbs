namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRBelgeSayiOnur")]
    public partial class OGRBelgeSayiOnur
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(500)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? InKod { get; set; }

        public int? BelgeSayi { get; set; }

        public int? Yil { get; set; }

        [StringLength(5)]
        public string Ortalama { get; set; }

        public int? EnumBelgeTuru { get; set; }

        public int? EnumDonem { get; set; }

        public int? FKOgrenciID { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public int? FKFakulteBirimID { get; set; }

        public virtual Birimler Birimler { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
