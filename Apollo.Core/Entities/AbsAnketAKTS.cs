namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("obs.AbsAnketAKTS")]
    public partial class AbsAnketAKTS
    {
        [Key]
        [Column(Order = 0)]
        public int ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FKOgrenciID { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FKGrupID { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(2)]
        public string GrupAd { get; set; }

        [Key]
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Yil { get; set; }

        [Key]
        [Column(Order = 5)]
        public byte Donem { get; set; }

        [Key]
        [Column(Order = 6)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short DerseDevam { get; set; }

        [Key]
        [Column(Order = 7)]
        public byte OnHazirlikVarYok { get; set; }

        [Key]
        [Column(Order = 8)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short OnHazirlikAdet { get; set; }

        [Key]
        [Column(Order = 9)]
        public byte OdevVarYok { get; set; }

        [Key]
        [Column(Order = 10)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short OdevAdet { get; set; }

        [Key]
        [Column(Order = 11)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short OdevSaati { get; set; }

        [Key]
        [Column(Order = 12)]
        public byte SunumVarYok { get; set; }

        [Key]
        [Column(Order = 13)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short SunumAdet { get; set; }

        [Key]
        [Column(Order = 14)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short SunumSaat { get; set; }

        [Key]
        [Column(Order = 15)]
        public byte AraSinavVarYok { get; set; }

        [Key]
        [Column(Order = 16)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short AraSinavAdet { get; set; }

        [Key]
        [Column(Order = 17)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short AraSinavSaat { get; set; }

        [Key]
        [Column(Order = 18)]
        public byte ProjeVarYok { get; set; }

        [Key]
        [Column(Order = 19)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short ProjeAdet { get; set; }

        [Key]
        [Column(Order = 20)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short ProjeSaat { get; set; }

        [Key]
        [Column(Order = 21)]
        public byte LaboratuarVarYok { get; set; }

        [Key]
        [Column(Order = 22)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short LaboratuarAdet { get; set; }

        [Key]
        [Column(Order = 23)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short LaboratuarSaat { get; set; }

        [Key]
        [Column(Order = 24)]
        public byte AraziCalismasiVarYok { get; set; }

        [Key]
        [Column(Order = 25)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short AraziCalismasiAdet { get; set; }

        [Key]
        [Column(Order = 26)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short AraziCalismasiSaat { get; set; }

        [Key]
        [Column(Order = 27)]
        public byte YariyilSonuSinavVarYok { get; set; }

        [Key]
        [Column(Order = 28)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short YariyilSonuSinavSaat { get; set; }

        [Key]
        [Column(Order = 29, TypeName = "smalldatetime")]
        public DateTime Tarih { get; set; }
    }
}
