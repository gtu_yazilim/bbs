namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.TNMSistemAyar")]
    public partial class TNMSistemAyar
    {
        public int ID { get; set; }

        public int? FKUlkeID { get; set; }

        public int? FKUniversiteID { get; set; }

        public int? FKIlID { get; set; }

        [Column(TypeName = "text")]
        public string DagitimAka { get; set; }

        [Column(TypeName = "text")]
        public string DagitimIdr { get; set; }

        public int? FKAkaFakulteID { get; set; }

        public int? FKAkaBolumID { get; set; }

        public int? FKAkaAnaBilimDaliID { get; set; }

        public int? FKIdrFakulteID { get; set; }

        public int? FKIdrBolumID { get; set; }

        public int? FKIdrAnaBilimDaliID { get; set; }

        [StringLength(50)]
        public string MailServer { get; set; }

        [StringLength(50)]
        public string DNSSonEki { get; set; }
    }
}
