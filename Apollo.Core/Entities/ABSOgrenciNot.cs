namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.ABSOgrenciNot")]
    public partial class ABSOgrenciNot
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ABSOgrenciNot()
        {
            ABSOgrenciNotDetay = new HashSet<ABSOgrenciNotDetay>();
        }

        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        public string Kullanici { get; set; }

        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public decimal? InKod { get; set; }

        public int? Notu { get; set; }

        public int Yil { get; set; }

        public int FKOgrenciID { get; set; }

        public int? FKDersPlanID { get; set; }

        public int FKDersPlanAnaID { get; set; }

        public int? FKDersGrupID { get; set; }

        public int EnumDonem { get; set; }

        public int? FKAbsPaylarID { get; set; }

        public Guid? UZEMID { get; set; }

        public double NotDeger { get; set; }

        public int? FKDegerlendirmeID { get; set; }

        public bool Silindi { get; set; }

        public DateTime? SilindiTarih { get; set; }

        [StringLength(50)]
        public string SilenKullanici { get; set; }

        [StringLength(50)]
        public string SilenIP { get; set; }

        public Guid? GUID { get; set; }

        public virtual ABSDegerlendirme ABSDegerlendirme { get; set; }

        public virtual ABSPaylar ABSPaylar { get; set; }

        public virtual OGRDersGrup OGRDersGrup { get; set; }

        public virtual OGRDersPlan OGRDersPlan { get; set; }

        public virtual OGRDersPlanAna OGRDersPlanAna { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSOgrenciNotDetay> ABSOgrenciNotDetay { get; set; }
    }
}
