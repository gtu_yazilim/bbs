namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.WebAlanDersIliski")]
    public partial class WebAlanDersIliski
    {
        [Key]
        public int InKod { get; set; }

        public int? AlanTipKod { get; set; }

        [StringLength(3)]
        public string BolKodAd { get; set; }

        [StringLength(5)]
        public string DersKod { get; set; }

        public short? Yil1 { get; set; }

        [StringLength(30)]
        public string Ekleyen { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? Zaman { get; set; }
    }
}
