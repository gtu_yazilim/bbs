namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.YoksisAktarimTalep")]
    public partial class YoksisAktarimTalep
    {
        public int ID { get; set; }

        [Required]
        [StringLength(100)]
        public string Kullanici { get; set; }

        [Required]
        [StringLength(500)]
        public string Talep { get; set; }

        public int OgrenciSayisi { get; set; }

        public DateTime TalepZamani { get; set; }

        public Guid TalepGuid { get; set; }
    }
}
