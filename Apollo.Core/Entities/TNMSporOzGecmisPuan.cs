namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.TNMSporOzGecmisPuan")]
    public partial class TNMSporOzGecmisPuan
    {
        public int ID { get; set; }

        public int FKBransID { get; set; }

        [Required]
        [StringLength(255)]
        public string Aciklama { get; set; }

        public int Puan { get; set; }

        public bool? Iptal { get; set; }

        public int? Yil { get; set; }

        public virtual TNMSporBranslari TNMSporBranslari { get; set; }
    }
}
