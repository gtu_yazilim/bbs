namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.SanalSinifProvider")]
    public partial class SanalSinifProvider
    {
        [Key]
        [Column(Order = 0)]
        public int PId { get; set; }

        [Key]
        [Column(Order = 1)]
        public byte Type { get; set; }

        [Key]
        [Column(Order = 2)]
        public byte Order { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(4000)]
        public string Data { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        [Key]
        [Column(Order = 4)]
        public bool Enable { get; set; }

        [Key]
        [Column(Order = 5)]
        public bool Shared { get; set; }

        [Key]
        [Column(Order = 6)]
        public bool Frame { get; set; }

        [Key]
        [Column(Order = 7)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CUId { get; set; }

        [Key]
        [Column(Order = 8)]
        public DateTime CDate { get; set; }

        [Key]
        [Column(Order = 9)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int UUId { get; set; }

        [Key]
        [Column(Order = 10)]
        public DateTime UDate { get; set; }
    }
}
