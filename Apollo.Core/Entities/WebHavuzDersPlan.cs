namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.WebHavuzDersPlan")]
    public partial class WebHavuzDersPlan
    {
        [Key]
        public int InKod { get; set; }

        [Required]
        [StringLength(2)]
        public string FakKod { get; set; }

        [Required]
        [StringLength(2)]
        public string BolKod { get; set; }

        public int HavuzDersID { get; set; }

        public int Yariyil { get; set; }

        public short? Zorunlu { get; set; }

        public byte? GosterimSekli { get; set; }

        public int? Yil1 { get; set; }

        [Required]
        [StringLength(50)]
        public string Ekleyen { get; set; }

        public DateTime Zaman { get; set; }

        [StringLength(50)]
        public string BolKodAd { get; set; }

        [StringLength(50)]
        public string DersKod { get; set; }

        public int? FKFakulteBirimID { get; set; }

        public int? FKBolumBirimID { get; set; }

        public int? FKProgramBirimID { get; set; }
    }
}
