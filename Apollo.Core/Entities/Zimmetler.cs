namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.Zimmetler")]
    public partial class Zimmetler
    {
        public int ID { get; set; }

        [StringLength(255)]
        public string FisNumarasi { get; set; }

        [StringLength(255)]
        public string MalzemeKodu { get; set; }

        [StringLength(255)]
        public string Tarih { get; set; }

        [StringLength(255)]
        public string MalzemeAdi { get; set; }

        [StringLength(255)]
        public string Birim { get; set; }

        [StringLength(255)]
        public string SicilNo { get; set; }

        [StringLength(255)]
        public string AmbarAdi { get; set; }

        public int? Miktar { get; set; }

        [StringLength(255)]
        public string BirimFiyat { get; set; }

        [StringLength(255)]
        public string ZimmetSahibi { get; set; }

        [StringLength(255)]
        public string KurumKodu { get; set; }

        [StringLength(255)]
        public string BirimAdi { get; set; }
    }
}
