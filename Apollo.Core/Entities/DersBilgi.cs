namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.DersBilgi")]
    public partial class DersBilgi
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DersBilgi()
        {
            DersKoordinator = new HashSet<DersKoordinator>();
        }

        public int ID { get; set; }

        public int? FKDersPlanAnaID { get; set; }

        public byte? FKDilID { get; set; }

        public short? Yil { get; set; }

        [StringLength(30)]
        public string Ekleyen { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? Zaman { get; set; }

        public virtual Dil Dil { get; set; }

        public virtual OGRDersPlanAna OGRDersPlanAna { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DersKoordinator> DersKoordinator { get; set; }
    }
}
