namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.DersAlanTnm")]
    public partial class DersAlanTnm
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DersAlanTnm()
        {
            DersAlaniIliski = new HashSet<DersAlaniIliski>();
            DilDersAlanTnm = new HashSet<DilDersAlanTnm>();
        }

        public int ID { get; set; }

        public int? AktsNo { get; set; }

        [StringLength(10)]
        public string Yil { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DersAlaniIliski> DersAlaniIliski { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DilDersAlanTnm> DilDersAlanTnm { get; set; }
    }
}
