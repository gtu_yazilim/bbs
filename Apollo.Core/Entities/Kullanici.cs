namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ytk.Kullanici")]
    public partial class Kullanici
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Kullanici()
        {
            Bildirim = new HashSet<Bildirim>();
            Mesaj = new HashSet<Mesaj>();
            RandevuKayit = new HashSet<RandevuKayit>();
            EPostaOnay = new HashSet<EPostaOnay>();
            GrupKullanici = new HashSet<GrupKullanici>();
            HesapYetki = new HashSet<HesapYetki>();
            HesapYetki1 = new HashSet<HesapYetki>();
            IslemLog = new HashSet<IslemLog>();
            KullaniciAlias = new HashSet<KullaniciAlias>();
            KullaniciCihaz = new HashSet<KullaniciCihaz>();
            KullaniciDetay = new HashSet<KullaniciDetay>();
            KullaniciGuvenlik = new HashSet<KullaniciGuvenlik>();
            KullaniciGuvenlik2 = new HashSet<KullaniciGuvenlik2>();
            KullaniciHesapAyar = new HashSet<KullaniciHesapAyar>();
            KullaniciToken = new HashSet<KullaniciToken>();
            KullaniciTokenHistory = new HashSet<KullaniciTokenHistory>();
            KullaniciTOTP = new HashSet<KullaniciTOTP>();
            ModulIPGuvenlik = new HashSet<ModulIPGuvenlik>();
            RolKullanici = new HashSet<RolKullanici>();
            SifreSifirlaAktivasyon = new HashSet<SifreSifirlaAktivasyon>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(255)]
        public string KullaniciAdi { get; set; }

        public int Tip { get; set; }

        public bool Durum { get; set; }

        public int? cawis_uid { get; set; }

        public bool GoogleAc { get; set; }

        public int? FKOgrenciID { get; set; }

        public int? FKPersonelID { get; set; }

        public int EnumDil { get; set; }

        public DateTime Tarih { get; set; }

        public int? FKKisiID { get; set; }

        public int? FKSorumluKullaniciID { get; set; }

        public virtual Kisi Kisi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Bildirim> Bildirim { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Mesaj> Mesaj { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RandevuKayit> RandevuKayit { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EPostaOnay> EPostaOnay { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GrupKullanici> GrupKullanici { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HesapYetki> HesapYetki { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HesapYetki> HesapYetki1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IslemLog> IslemLog { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<KullaniciAlias> KullaniciAlias { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<KullaniciCihaz> KullaniciCihaz { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<KullaniciDetay> KullaniciDetay { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<KullaniciGuvenlik> KullaniciGuvenlik { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<KullaniciGuvenlik2> KullaniciGuvenlik2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<KullaniciHesapAyar> KullaniciHesapAyar { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<KullaniciToken> KullaniciToken { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<KullaniciTokenHistory> KullaniciTokenHistory { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<KullaniciTOTP> KullaniciTOTP { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ModulIPGuvenlik> ModulIPGuvenlik { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RolKullanici> RolKullanici { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SifreSifirlaAktivasyon> SifreSifirlaAktivasyon { get; set; }
    }
}
