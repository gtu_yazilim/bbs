namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRDegisim")]
    public partial class OGRDegisim
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? InKod { get; set; }

        public int? Yil { get; set; }

        public DateTime? YonKurTarih { get; set; }

        [StringLength(50)]
        public string YonKurNo { get; set; }

        public int? EnumDonem { get; set; }

        public int? FKUlkeID { get; set; }

        public int? FKUniversiteID { get; set; }

        public int? FKOgrenciID { get; set; }

        public int? EnumDegisimTipi { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public virtual Birimler Birimler { get; set; }

        public virtual TNMUlke TNMUlke { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
