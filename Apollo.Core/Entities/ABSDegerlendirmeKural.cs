namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.ABSDegerlendirmeKural")]
    public partial class ABSDegerlendirmeKural
    {
        public int ID { get; set; }

        public DateTime TarihKayit { get; set; }

        [Required]
        [StringLength(50)]
        public string Kullanici { get; set; }

        public int FKDersPlanAnaID { get; set; }

        public int Yil { get; set; }

        public int EnumDonem { get; set; }

        public int MinFinal { get; set; }

        public int MinYilIci { get; set; }

        public int MinOrtalama { get; set; }

        public int MinKurulOrtalamaTip { get; set; }

        public int MinYilIciOrtalamaTip { get; set; }

        public bool YiliciDegerlendirTip { get; set; }

        public virtual OGRDersPlanAna OGRDersPlanAna { get; set; }
    }
}
