namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGROgrenciTemsilci")]
    public partial class OGROgrenciTemsilci
    {
        public int ID { get; set; }

        public int? FKFakulteID { get; set; }

        public int? FKBolumID { get; set; }

        public int? FKOgrenciID { get; set; }

        public int? EnumTemsilciID { get; set; }

        [StringLength(50)]
        public string TelefonNo { get; set; }

        public int? Yil { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public DateTime? SGZaman { get; set; }

        public bool? Aktif { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
