namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRUniversiteDisiGorevli")]
    public partial class OGRUniversiteDisiGorevli
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? Sicil { get; set; }

        [StringLength(100)]
        public string TC { get; set; }

        [StringLength(250)]
        public string Unvan { get; set; }

        [StringLength(500)]
        public string Ad { get; set; }

        [StringLength(500)]
        public string Soyad { get; set; }

        [StringLength(250)]
        public string GorevYeri { get; set; }

        [StringLength(250)]
        public string Email { get; set; }

        public int? FKGorevIlID { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public virtual TNMIl TNMIl { get; set; }
    }
}
