namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRHarcBilgi")]
    public partial class OGRHarcBilgi
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(500)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? InKod { get; set; }

        [StringLength(100)]
        public string OgrenimKrediNo { get; set; }

        public DateTime? OgrenimBaslamaTarihi { get; set; }

        public DateTime? OgrenimBitisTarihi { get; set; }

        public DateTime? OgrenimIptalTarihi { get; set; }

        public DateTime? HarcMuafBaslamaTarihi { get; set; }

        public DateTime? HarcMuafBitisTarihi { get; set; }

        public DateTime? HarcMuafIptalTarihi { get; set; }

        public int? EnumHarcMuafiyeti { get; set; }

        [StringLength(100)]
        public string HarcMuafAciklama { get; set; }

        [StringLength(100)]
        public string BursNo { get; set; }

        public DateTime? BursBaslamaTarihi { get; set; }

        public DateTime? BursBitisTarihi { get; set; }

        public DateTime? BursIptalTarihi { get; set; }

        public int? EnumBurs { get; set; }

        public int? EnumHarcZitTarifeNedeni { get; set; }

        public int? EnumZitTarifeOgretimTuru { get; set; }

        public DateTime? ZitTarifeBaslamaTarihi { get; set; }

        public DateTime? ZitTarifeBitisTarihi { get; set; }

        public DateTime? ZitTarifeIptalTarihi { get; set; }

        [StringLength(100)]
        public string ZitTarifeAciklama { get; set; }

        public int? EnumProtokolKurum { get; set; }

        public int? ProtokolIndirimOrani { get; set; }

        public DateTime? ProtokolIndirimBasTarihi { get; set; }

        public DateTime? ProtokolIndirimBitTarihi { get; set; }

        public DateTime? ProtokolIndirimIptalTarihi { get; set; }

        [StringLength(250)]
        public string ProtokolIndirimAciklama { get; set; }

        [StringLength(15)]
        public string UAOBurslulukNo { get; set; }

        public int FKOgrenciID { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
