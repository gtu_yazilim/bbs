namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGROgrenciEtiket")]
    public partial class OGROgrenciEtiket
    {
        public int ID { get; set; }

        public int FKOgrenciID { get; set; }

        public int FKEtiketID { get; set; }

        public DateTime? TarihBaslangic { get; set; }

        public DateTime? TarihBitis { get; set; }

        [Required]
        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public DateTime TarihKayit { get; set; }

        public DateTime? TarihGuncelleme { get; set; }

        public bool Silindi { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }

        public virtual OGRTNMOgrenciEtiket OGRTNMOgrenciEtiket { get; set; }
    }
}
