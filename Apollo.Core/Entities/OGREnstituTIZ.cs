namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGREnstituTIZ")]
    public partial class OGREnstituTIZ
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? InKod { get; set; }

        public DateTime? Tarih { get; set; }

        public int? EnumEnstituYeterlikDurum { get; set; }

        public int? FKOgrenciID { get; set; }

        public DateTime? SonTarih { get; set; }

        public int? FKMekanID { get; set; }

        public TimeSpan? Saat { get; set; }

        public int? BNot { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public virtual Mekan Mekan { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
