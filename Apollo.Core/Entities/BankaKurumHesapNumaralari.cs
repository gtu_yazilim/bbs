namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("bnk.BankaKurumHesapNumaralari")]
    public partial class BankaKurumHesapNumaralari
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BankaKurumHesapNumaralari()
        {
            BankaWebServisHareketi = new HashSet<BankaWebServisHareketi>();
        }

        public int ID { get; set; }

        [StringLength(10)]
        public string MusteriNo { get; set; }

        [StringLength(10)]
        public string SubeKodu { get; set; }

        [StringLength(10)]
        public string HesapNo { get; set; }

        public bool AktifMi { get; set; }

        [StringLength(26)]
        public string IBAN { get; set; }

        public DateTime? HesapAcilisTarihi { get; set; }

        public DateTime? HesapKapanisTarihi { get; set; }

        [StringLength(100)]
        public string Aciklama { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BankaWebServisHareketi> BankaWebServisHareketi { get; set; }
    }
}
