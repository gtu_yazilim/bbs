namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.uyelik")]
    public partial class uyelik
    {
        public int ID { get; set; }

        [StringLength(11)]
        public string tc_no { get; set; }

        [StringLength(255)]
        public string kurum { get; set; }

        public int? baslangic { get; set; }

        public int? bitis { get; set; }

        public int? FKuyelikID { get; set; }

        [StringLength(20)]
        public string kullanici { get; set; }

        public DateTime? son_giris_tar { get; set; }

        public virtual SBTuyelik SBTuyelik { get; set; }
    }
}
