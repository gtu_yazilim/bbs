namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.Sendika")]
    public partial class Sendika
    {
        public int ID { get; set; }

        public DateTime Zaman { get; set; }

        [Required]
        [StringLength(50)]
        public string Kullanici { get; set; }

        [Required]
        [StringLength(50)]
        public string Makina { get; set; }

        [StringLength(10)]
        public string SendikaUyeNo { get; set; }

        public DateTime? SendikaKayitTarihi { get; set; }

        public DateTime? SendikaAyrilisTarihi { get; set; }

        [StringLength(250)]
        public string Aciklama { get; set; }

        public int FKPersonelBilgisiID { get; set; }

        public int FKSendikaID { get; set; }

        public int? ENUMVazife { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }

        public virtual TNMSendika TNMSendika { get; set; }
    }
}
