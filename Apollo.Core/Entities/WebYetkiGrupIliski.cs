namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.WebYetkiGrupIliski")]
    public partial class WebYetkiGrupIliski
    {
        [Key]
        public int InKod { get; set; }

        public int? YetkiGrupID { get; set; }

        public int? YetkiID { get; set; }

        [StringLength(50)]
        public string Ekleyen { get; set; }

        public DateTime? Zaman { get; set; }
    }
}
