namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGROgrenciDersPlan")]
    public partial class OGROgrenciDersPlan
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OGROgrenciDersPlan()
        {
            OGROgrenciDersPlan1 = new HashSet<OGROgrenciDersPlan>();
            OGROgrenciDersPlan11 = new HashSet<OGROgrenciDersPlan>();
            OGROgrenciDersPlan12 = new HashSet<OGROgrenciDersPlan>();
            OGROgrenciNot1 = new HashSet<OGROgrenciNot>();
            OGROgrenciYazilma1 = new HashSet<OGROgrenciYazilma>();
        }

        public int ID { get; set; }

        public int FKOgrenciID { get; set; }

        public int FKDersPlanAnaID { get; set; }

        public int? FKProgramID { get; set; }

        public int? FKDersPlanID { get; set; }

        public int? OgretimYili { get; set; }

        public int? EnumDonem { get; set; }

        public int? Yariyil { get; set; }

        public int? FKOgrenciNotID { get; set; }

        public int? EnumPlanIslem { get; set; }

        public int? FKIntibakID { get; set; }

        public int? FKYazilmaID { get; set; }

        public int? FKOgrenciDersPlanID { get; set; }

        public bool OrtalamayaGirer { get; set; }

        public bool Aktif { get; set; }

        public DateTime TarihKayit { get; set; }

        public DateTime? TarihGuncelleme { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string IP { get; set; }

        public int? FKKurulKararID { get; set; }

        public bool Silindi { get; set; }

        public int EnumDersSureTip { get; set; }

        public int? FKReferansDersPlanID { get; set; }

        public int? FKIkameNotOgrenciDersPlanID { get; set; }

        public int? FKReferansOgrenciDersPlanID { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGROgrenciDersPlan> OGROgrenciDersPlan1 { get; set; }

        public virtual OGROgrenciDersPlan OGROgrenciDersPlan2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGROgrenciDersPlan> OGROgrenciDersPlan11 { get; set; }

        public virtual OGROgrenciDersPlan OGROgrenciDersPlan3 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGROgrenciDersPlan> OGROgrenciDersPlan12 { get; set; }

        public virtual OGROgrenciDersPlan OGROgrenciDersPlan4 { get; set; }

        public virtual OGROgrenciNot OGROgrenciNot { get; set; }

        public virtual OGROgrenciYazilma OGROgrenciYazilma { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGROgrenciNot> OGROgrenciNot1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGROgrenciYazilma> OGROgrenciYazilma1 { get; set; }
    }
}
