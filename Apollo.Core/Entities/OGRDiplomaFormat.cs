namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRDiplomaFormat")]
    public partial class OGRDiplomaFormat
    {
        public int ID { get; set; }

        public int FKRaporTasarimID { get; set; }

        public int FKRaporTasarimArkaID { get; set; }

        public DateTime TarihKayit { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        public int FKBirimID { get; set; }

        public DateTime TarihBaslangic { get; set; }

        public DateTime TarihBitis { get; set; }

        public DateTime? TarihSilinme { get; set; }

        public bool Aktif { get; set; }

        public int EnumDiplomaBelgeTuru { get; set; }

        public virtual Birimler Birimler { get; set; }

        public virtual RaporTasarim RaporTasarim { get; set; }

        public virtual RaporTasarim RaporTasarim1 { get; set; }
    }
}
