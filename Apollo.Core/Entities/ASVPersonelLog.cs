namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.ASVPersonelLog")]
    public partial class ASVPersonelLog
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        public string Kullanici { get; set; }

        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? IslemKod { get; set; }

        public DateTime? ArsivTarihi { get; set; }

        [StringLength(20)]
        public string ArsivEvrakNo { get; set; }

        [Required]
        [StringLength(20)]
        public string AyrilmaMi { get; set; }

        [Column(TypeName = "text")]
        public string Aciklama { get; set; }

        public short? KullaniciKod { get; set; }

        public DateTime? IslemTarihi { get; set; }

        public int FKPersonelBilgisiID { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }
    }
}
