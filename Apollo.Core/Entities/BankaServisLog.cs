namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("bnk.BankaServisLog")]
    public partial class BankaServisLog
    {
        public int ID { get; set; }

        [StringLength(100)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Ip { get; set; }

        public long? Tc { get; set; }

        [StringLength(20)]
        public string OgrenciNo { get; set; }

        public DateTime? Date { get; set; }

        [StringLength(50)]
        public string Level { get; set; }

        [StringLength(255)]
        public string Logger { get; set; }

        [StringLength(255)]
        public string Action { get; set; }

        public string Message { get; set; }

        [StringLength(50)]
        public string DatabaseName { get; set; }
    }
}
