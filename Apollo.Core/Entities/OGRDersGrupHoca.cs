namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRDersGrupHoca")]
    public partial class OGRDersGrupHoca
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OGRDersGrupHoca()
        {
            OGRDersProgramiv2 = new HashSet<OGRDersProgramiv2>();
        }

        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(500)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? InKod { get; set; }

        public int? Saat { get; set; }

        public int? Uygulama { get; set; }

        [StringLength(150)]
        public string Detay { get; set; }

        [StringLength(50)]
        public string PersonelSicilNo { get; set; }

        [StringLength(50)]
        public string PersonelTCNo { get; set; }

        public int? EnumEBSOnay { get; set; }

        public int? EnumDersPlanIliskiNo { get; set; }

        public int? EnumDersGrupDil { get; set; }

        public int? EnumDonem { get; set; }

        public int? FKDersGrupID { get; set; }

        public int? FKPersonelID { get; set; }

        public int? EnumDersHocaUnvan { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public DateTime? TarihBaslangic { get; set; }

        public DateTime? TarihBitis { get; set; }

        public bool Aktif { get; set; }

        public virtual OGRDersGrup OGRDersGrup { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRDersProgramiv2> OGRDersProgramiv2 { get; set; }
    }
}
