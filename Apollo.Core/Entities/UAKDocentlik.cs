namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.UAKDocentlik")]
    public partial class UAKDocentlik
    {
        public int ID { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? EvrakTarihi { get; set; }

        public int? EvrakSayi { get; set; }

        public int FKTemelAlanID { get; set; }

        public int FKBilimAlaniID { get; set; }

        public int SinavDonemiYil { get; set; }

        public int SinavDonemiAy { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? UnvanAldigiTarih { get; set; }
    }
}
