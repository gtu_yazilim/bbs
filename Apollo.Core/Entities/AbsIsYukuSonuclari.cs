namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("obs.AbsIsYukuSonuclari")]
    public partial class AbsIsYukuSonuclari
    {
        public int ID { get; set; }

        public int? FK_GrupID { get; set; }

        public int? Yil { get; set; }

        public double? DerseDevamYuzdesi { get; set; }

        public int? OnHazirlikSifir { get; set; }

        public int? OnHazirlikBir { get; set; }

        public int? OnHazirlikIki { get; set; }

        public double? OnHazirlikSaatOrt { get; set; }

        public int? OdevVarmiSifir { get; set; }

        public int? OdevVarmiBir { get; set; }

        public int? OdevVarmiIki { get; set; }

        public double? OdevAdetOrt { get; set; }

        public double? OdevSaatOrt { get; set; }

        public int? SunumVarmiSifir { get; set; }

        public int? SunumVarmiBir { get; set; }

        public int? SunumVarmiIki { get; set; }

        public double? SunumAdetOrt { get; set; }

        public double? SunumSaatOrt { get; set; }

        public int? AraSinavVarmiSifir { get; set; }

        public int? AraSinavVarmiBir { get; set; }

        public int? AraSinavVarmiIki { get; set; }

        public double? AraSinavAdetOrt { get; set; }

        public double? AraSinavSaatOrt { get; set; }

        public int? ProjeVarmiSifir { get; set; }

        public int? ProjeVarmiBir { get; set; }

        public int? ProjeVarmiIki { get; set; }

        public double? ProjeAdetOrt { get; set; }

        public double? ProjeSaatOrt { get; set; }

        public int? LabVarmiSifir { get; set; }

        public int? LabVarmiBir { get; set; }

        public int? LabVarmiIki { get; set; }

        public double? LabAdetOrt { get; set; }

        public double? LabSaatOrt { get; set; }

        public int? AraziVarmiSifir { get; set; }

        public int? AraziVarmiBir { get; set; }

        public int? AraziVarmiIki { get; set; }

        public double? AraziAdetOrt { get; set; }

        public double? AraziSaatOrt { get; set; }

        public int? YariyilVarmiSifir { get; set; }

        public int? YariyilVarmiBir { get; set; }

        public int? YariyilVarmiIki { get; set; }

        public double? YariyilSaatOrt { get; set; }
    }
}
