namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("basvuru.YazOkuluOnKayitDersler")]
    public partial class YazOkuluOnKayitDersler
    {
        public int ID { get; set; }

        public int FKYazOkuluOnKayitID { get; set; }

        public int FKDersGrupID { get; set; }

        public bool Silindi { get; set; }

        public DateTime? TarihSilindi { get; set; }

        public virtual YazOkuluOnKayit YazOkuluOnKayit { get; set; }

        public virtual OGRDersGrup OGRDersGrup { get; set; }
    }
}
