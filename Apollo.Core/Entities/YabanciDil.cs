namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.YabanciDil")]
    public partial class YabanciDil
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? InKod { get; set; }

        public int FKPersonelBilgisiID { get; set; }

        public double SinavPuani { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? SinavTarihi { get; set; }

        public bool Resmi { get; set; }

        public int? KullaniciKod { get; set; }

        public DateTime? IslemTarihi { get; set; }

        public int ENUMDilTuru { get; set; }

        public int ENUMSinavTuru { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }
    }
}
