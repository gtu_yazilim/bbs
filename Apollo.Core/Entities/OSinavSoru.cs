namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.OSinavSoru")]
    public partial class OSinavSoru
    {
        [Key]
        [Column(Order = 0)]
        public int SoruID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SinavID { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SoruNo { get; set; }

        [Key]
        [Column(Order = 3)]
        public string Baslik { get; set; }

        [Key]
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DogruCevap { get; set; }

        [Key]
        [Column(Order = 5)]
        public string Cevap1 { get; set; }

        [Key]
        [Column(Order = 6)]
        public string Cevap2 { get; set; }

        [Key]
        [Column(Order = 7)]
        public string Cevap3 { get; set; }

        [Key]
        [Column(Order = 8)]
        public string Cevap4 { get; set; }

        [Key]
        [Column(Order = 9)]
        public string Cevap5 { get; set; }

        [StringLength(255)]
        public string Cevap1DosyaKey { get; set; }

        [StringLength(255)]
        public string Cevap2DosyaKey { get; set; }

        [StringLength(255)]
        public string Cevap3DosyaKey { get; set; }

        [StringLength(255)]
        public string Cevap4DosyaKey { get; set; }

        [StringLength(255)]
        public string Cevap5DosyaKey { get; set; }

        [Key]
        [Column(Order = 10)]
        public bool Silindi { get; set; }

        [Key]
        [Column(Order = 11)]
        public bool Iptal { get; set; }

        [Key]
        [Column(Order = 12)]
        [StringLength(50)]
        public string Ekleyen { get; set; }

        [Key]
        [Column(Order = 13)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EkleyenID { get; set; }

        [Key]
        [Column(Order = 14)]
        public DateTime EklemeTarihi { get; set; }

        [Key]
        [Column(Order = 15)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int GuncelleyenID { get; set; }

        [Key]
        [Column(Order = 16)]
        [StringLength(50)]
        public string Guncelleyen { get; set; }

        [Key]
        [Column(Order = 17)]
        public DateTime GuncellemeTarihi { get; set; }

        public int? OlcmeID { get; set; }

        [Key]
        [Column(Order = 18)]
        public bool MazeretSoru { get; set; }

        public virtual ABSPaylarOlcme ABSPaylarOlcme { get; set; }

        public virtual OSinav OSinav { get; set; }
    }
}
