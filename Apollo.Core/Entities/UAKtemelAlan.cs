namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.UAKtemelAlan")]
    public partial class UAKtemelAlan
    {
        public int ID { get; set; }

        [StringLength(11)]
        public string tc_no { get; set; }

        public int? FKtemelAlanID { get; set; }

        [StringLength(8)]
        public string FKbilimAlanID { get; set; }

        [StringLength(5)]
        public string anahtar1 { get; set; }

        [StringLength(5)]
        public string anahtar2 { get; set; }

        [StringLength(5)]
        public string anahtar3 { get; set; }

        [StringLength(5)]
        public string anahtar4 { get; set; }

        [StringLength(5)]
        public string anahtar5 { get; set; }

        [StringLength(20)]
        public string kullanici { get; set; }

        public DateTime? son_giris_tar { get; set; }

        public virtual SBTanahtarSozcukler SBTanahtarSozcukler { get; set; }

        public virtual SBTanahtarSozcukler SBTanahtarSozcukler1 { get; set; }

        public virtual SBTanahtarSozcukler SBTanahtarSozcukler2 { get; set; }

        public virtual SBTanahtarSozcukler SBTanahtarSozcukler3 { get; set; }

        public virtual SBTanahtarSozcukler SBTanahtarSozcukler4 { get; set; }

        public virtual SBTbilimAlanlari SBTbilimAlanlari { get; set; }

        public virtual SBTtemelAlanlar SBTtemelAlanlar { get; set; }
    }
}
