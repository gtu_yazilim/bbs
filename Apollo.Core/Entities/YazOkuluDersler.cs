namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.YazOkuluDersler")]
    public partial class YazOkuluDersler
    {
        public int ID { get; set; }

        public int? FKFakulteID { get; set; }

        public int? FKProgramID { get; set; }

        public int? FKDersPlanAnaID { get; set; }

        public int? EnumDersTipi { get; set; }

        [StringLength(50)]
        public string EkleyenKullanici { get; set; }

        public DateTime? Zaman { get; set; }

        public int? FKBirimID { get; set; }

        public int? Yil { get; set; }

        public virtual OGRDersPlanAna OGRDersPlanAna { get; set; }
    }
}
