namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRKYKBursAktarma")]
    public partial class OGRKYKBursAktarma
    {
        public int ID { get; set; }

        [Required]
        [StringLength(11)]
        public string TCNO { get; set; }

        public long? KlavuzKodu { get; set; }

        public long BursNo { get; set; }

        public DateTime? BursBaslamaTarihi { get; set; }

        public DateTime? BursBitisTarihi { get; set; }

        public int? BursBitisYili { get; set; }

        public DateTime? BursIptalTarihi { get; set; }

        public int? EnumBurs { get; set; }

        public int? FKOgrenciID { get; set; }

        public bool? Aktarildi { get; set; }

        [StringLength(100)]
        public string Aciklama { get; set; }
    }
}
