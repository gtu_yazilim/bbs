namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.TNMMakamTazminati")]
    public partial class TNMMakamTazminati
    {
        public short ID { get; set; }

        public DateTime? Zaman { get; set; }

        public string Kullanici { get; set; }

        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public short? Kod { get; set; }

        public byte DereceAlt { get; set; }

        public byte DereceUst { get; set; }

        public byte YilAlt { get; set; }

        public byte YilUst { get; set; }

        public short AltIdariGorevID { get; set; }

        public short UstIdariGorevID { get; set; }

        public short MakamTazminati { get; set; }

        public int? FKKadroUnvaniID { get; set; }

        public virtual TNMKadroUnvani TNMKadroUnvani { get; set; }
    }
}
