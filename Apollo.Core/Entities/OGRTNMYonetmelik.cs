namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRTNMYonetmelik")]
    public partial class OGRTNMYonetmelik
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OGRTNMYonetmelik()
        {
            OgrOgrenciYonetmelik = new HashSet<OgrOgrenciYonetmelik>();
        }

        public int ID { get; set; }

        public string YonetmelikAdi { get; set; }

        public string Aciklama { get; set; }

        public int BaslangicYili { get; set; }

        public int BitisYili { get; set; }

        public int EnumFakulteTipi { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public string Kullanici { get; set; }

        public string Makina { get; set; }

        public string SilenKullanici { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? SilinmeZamani { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? Zaman { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OgrOgrenciYonetmelik> OgrOgrenciYonetmelik { get; set; }
    }
}
