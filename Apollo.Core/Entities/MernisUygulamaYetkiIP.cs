namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("mernis.MernisUygulamaYetkiIP")]
    public partial class MernisUygulamaYetkiIP
    {
        public int ID { get; set; }

        public int FKUygulamaID { get; set; }

        [Required]
        [StringLength(50)]
        public string IP { get; set; }

        [Required]
        [StringLength(50)]
        public string Kullanici { get; set; }

        public DateTime TarihKayit { get; set; }

        public bool Silindi { get; set; }

        public virtual MernisUygulamaYetki MernisUygulamaYetki { get; set; }
    }
}
