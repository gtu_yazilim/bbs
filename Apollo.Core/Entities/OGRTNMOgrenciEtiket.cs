namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRTNMOgrenciEtiket")]
    public partial class OGRTNMOgrenciEtiket
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OGRTNMOgrenciEtiket()
        {
            OGROgrenciEtiket = new HashSet<OGROgrenciEtiket>();
            OGROgrenciEtiketIslem = new HashSet<OGROgrenciEtiketIslem>();
        }

        public int ID { get; set; }

        public DateTime Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        [Required]
        [StringLength(300)]
        public string Etiket { get; set; }

        [StringLength(300)]
        public string EtiketEng { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        [StringLength(50)]
        public string Renk { get; set; }

        public bool KullaniciGorebilir { get; set; }

        public bool KullaniciEkleyebilir { get; set; }

        public bool KullaniciSilebilir { get; set; }

        [StringLength(1000)]
        public string Mesaj { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGROgrenciEtiket> OGROgrenciEtiket { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGROgrenciEtiketIslem> OGROgrenciEtiketIslem { get; set; }
    }
}
