namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.WebAlanTipTnm")]
    public partial class WebAlanTipTnm
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int InKod { get; set; }

        public byte FormDil { get; set; }

        public byte AlanTipKod { get; set; }

        [Required]
        [StringLength(50)]
        public string AlanTipAd { get; set; }

        public DateTime Zaman { get; set; }
    }
}
