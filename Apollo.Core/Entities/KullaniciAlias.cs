namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ytk.KullaniciAlias")]
    public partial class KullaniciAlias
    {
        public int ID { get; set; }

        public int FKKullaniciID { get; set; }

        [Required]
        [StringLength(255)]
        public string Alias { get; set; }

        public DateTime Tarih { get; set; }

        public int FKDomainID { get; set; }

        public bool AnaMi { get; set; }

        public virtual Kullanici Kullanici { get; set; }

        public virtual TNMDomain TNMDomain { get; set; }
    }
}
