namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("mek.MekanV2")]
    public partial class MekanV2
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MekanV2()
        {
            MekanCihazV2 = new HashSet<MekanCihazV2>();
            MekanFotoV2 = new HashSet<MekanFotoV2>();
            MekanKisiV2 = new HashSet<MekanKisiV2>();
            MekanV21 = new HashSet<MekanV2>();
            OGRDersProgramiv2 = new HashSet<OGRDersProgramiv2>();
            OGREnstituTezTizTosSinavi = new HashSet<OGREnstituTezTizTosSinavi>();
            OGRMazeret = new HashSet<OGRMazeret>();
            SinavMekan = new HashSet<SinavMekan>();
        }

        public int ID { get; set; }

        public int? UstMekanID { get; set; }

        public int FKMekanTipID { get; set; }

        public int? FKBirimID { get; set; }

        public int? FKIlID { get; set; }

        public int? FKIlceID { get; set; }

        public int? FKSorumluKisiID { get; set; }

        [StringLength(200)]
        public string MekanAd { get; set; }

        [StringLength(300)]
        public string MekanKisaAd { get; set; }

        [StringLength(500)]
        public string MekanUzunAd { get; set; }

        [StringLength(50)]
        public string MekanKod { get; set; }

        [StringLength(300)]
        public string AcikAdres { get; set; }

        public string Aciklama { get; set; }

        public int? Kat { get; set; }

        public int? Alan { get; set; }

        public int? ToplamKapasite { get; set; }

        public int? SinavKapasite { get; set; }

        public bool Aktif { get; set; }

        [Required]
        [StringLength(50)]
        public string Kullanici { get; set; }

        public DateTime Zaman { get; set; }

        public DateTime? SGZaman { get; set; }

        public int? EskiID { get; set; }

        public double? Latitude { get; set; }

        public double? Longitude { get; set; }

        public bool Silindi { get; set; }

        public string PolygonJson { get; set; }

        public virtual Birimler Birimler { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MekanCihazV2> MekanCihazV2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MekanFotoV2> MekanFotoV2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MekanKisiV2> MekanKisiV2 { get; set; }

        public virtual Mekan Mekan { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MekanV2> MekanV21 { get; set; }

        public virtual MekanV2 MekanV22 { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }

        public virtual TNMMekanTipV2 TNMMekanTipV2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRDersProgramiv2> OGRDersProgramiv2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituTezTizTosSinavi> OGREnstituTezTizTosSinavi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRMazeret> OGRMazeret { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SinavMekan> SinavMekan { get; set; }
    }
}
