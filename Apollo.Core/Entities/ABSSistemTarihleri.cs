namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.ABSSistemTarihleri")]
    public partial class ABSSistemTarihleri
    {
        public int ID { get; set; }

        public int? EnumTarih { get; set; }

        public DateTime? AcilisTarihi { get; set; }

        public DateTime? KapanisTarihi { get; set; }

        [StringLength(50)]
        public string FakulteKod { get; set; }

        [StringLength(50)]
        public string BolumKod { get; set; }

        [StringLength(50)]
        public string Aciklama { get; set; }

        public int? FK_FakulteID { get; set; }

        public int? FK_BolumID { get; set; }

        public int? EnumDonem { get; set; }

        public int? Yil { get; set; }

        [StringLength(50)]
        public string Ekleyen { get; set; }

        public DateTime? EklenmeTarihi { get; set; }

        public virtual Birimler Birimler { get; set; }

        public virtual Birimler Birimler1 { get; set; }
    }
}
