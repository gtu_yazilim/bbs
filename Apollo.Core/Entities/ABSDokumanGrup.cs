namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.ABSDokumanGrup")]
    public partial class ABSDokumanGrup
    {
        [Key]
        [Column(Order = 0)]
        public int ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FKABSDokumanID { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FKOGRDersGrupID { get; set; }

        [Key]
        [Column(Order = 3)]
        public DateTime SGTarih { get; set; }

        [Key]
        [Column(Order = 4)]
        [StringLength(30)]
        public string SGKullanici { get; set; }

        [Key]
        [Column(Order = 5)]
        [StringLength(20)]
        public string SGIP { get; set; }

        [Key]
        [Column(Order = 6)]
        public bool Silindi { get; set; }

        public virtual ABSDokuman ABSDokuman { get; set; }

        public virtual OGRDersGrup OGRDersGrup { get; set; }
    }
}
