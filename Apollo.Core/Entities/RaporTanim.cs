namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("rapor.RaporTanim")]
    public partial class RaporTanim
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public RaporTanim()
        {
            RaporTasarim1 = new HashSet<RaporTasarim1>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(250)]
        public string RaporAd { get; set; }

        public DateTime TarihKayit { get; set; }

        [StringLength(250)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string IP { get; set; }

        public int? EnumRaporBelgeTipi { get; set; }

        public DateTime? TarihSilinme { get; set; }

        public bool Silindi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RaporTasarim1> RaporTasarim1 { get; set; }
    }
}
