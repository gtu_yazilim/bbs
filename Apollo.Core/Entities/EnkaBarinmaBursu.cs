namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("basvuru.EnkaBarinmaBursu")]
    public partial class EnkaBarinmaBursu
    {
        public int ID { get; set; }

        public int EnumAnneMaas { get; set; }

        public int EnumBabaMaas { get; set; }

        public int EnumEmekliMaas { get; set; }

        public int EnumAnneCalismaTipi { get; set; }

        public int EnumBabaCalismaTipi { get; set; }

        public int OkuyanKardesSayisi { get; set; }

        [Required]
        [StringLength(500)]
        public string OkuyanKardesOgretimDuzeyi { get; set; }

        public int BaskaKurumBurs { get; set; }

        public bool SehitGaziYakini { get; set; }

        public bool AnneBabaAyri { get; set; }

        public bool AnneOlmus { get; set; }

        public bool BabaOlmus { get; set; }

        [Required]
        [StringLength(1000)]
        public string IkametAdres { get; set; }

        [Required]
        [StringLength(50)]
        public string GSM { get; set; }

        [Required]
        [StringLength(50)]
        public string VeliGSM { get; set; }

        [Required]
        [StringLength(250)]
        public string EPosta { get; set; }

        [Required]
        [StringLength(50)]
        public string IBAN { get; set; }

        [Required]
        [StringLength(150)]
        public string BankaAdi { get; set; }

        public DateTime TarihKayit { get; set; }

        public DateTime? TarihGuncelleme { get; set; }

        [Required]
        [StringLength(50)]
        public string Kullanici { get; set; }

        [Required]
        [StringLength(50)]
        public string IP { get; set; }

        public bool Iptal { get; set; }

        public int Yil { get; set; }

        public int FKOgrenciID { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
