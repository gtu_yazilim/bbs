namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.WebDersYerTipTnm")]
    public partial class WebDersYerTipTnm
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int InKod { get; set; }

        public byte FormDil { get; set; }

        public byte DersYerTipKod { get; set; }

        [Required]
        [StringLength(50)]
        public string DersYerTipAd { get; set; }

        public DateTime Zaman { get; set; }
    }
}
