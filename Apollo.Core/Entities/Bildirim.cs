namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("msg.Bildirim")]
    public partial class Bildirim
    {
        public int ID { get; set; }

        public DateTime TarihKayit { get; set; }

        public int FKKullaniciID { get; set; }

        [Required]
        public string Metin { get; set; }

        public bool OkunduMu { get; set; }

        public bool SilindiMi { get; set; }

        public int FKMesajID { get; set; }

        public virtual Kullanici Kullanici { get; set; }

        public virtual Mesaj Mesaj { get; set; }
    }
}
