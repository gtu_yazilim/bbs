namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRSaglik")]
    public partial class OGRSaglik
    {
        public int ID { get; set; }

        public int FKKisiID { get; set; }

        public int FKOgrenciID { get; set; }

        public int? EnumEngelTipi { get; set; }

        public int? EngelOrani { get; set; }

        [StringLength(500)]
        public string Aciklama { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public DateTime? Zaman { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public virtual Kisi Kisi { get; set; }
    }
}
