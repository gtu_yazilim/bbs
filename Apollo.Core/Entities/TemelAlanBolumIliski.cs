namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.TemelAlanBolumIliski")]
    public partial class TemelAlanBolumIliski
    {
        public int ID { get; set; }

        public int? FKTemelAlanID { get; set; }

        public byte? OgretimSeviye { get; set; }

        public byte? EnumAlanTur { get; set; }

        public int? FKBolumBirimID { get; set; }

        public int? FKProgramBirimID { get; set; }

        public virtual TemelAlan TemelAlan { get; set; }

        public virtual Birimler Birimler { get; set; }
    }
}
