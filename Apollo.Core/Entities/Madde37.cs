namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.Madde37")]
    public partial class Madde37
    {
        public int ID { get; set; }

        public int? FKPersonelID { get; set; }

        public int? FKAkademikUnvanID { get; set; }

        public int? FKBirimID { get; set; }

        public int? FKGorevlendirildigiKurumID { get; set; }

        [StringLength(255)]
        public string SicilNo { get; set; }

        [StringLength(255)]
        public string KadroUnvani { get; set; }

        [StringLength(255)]
        public string Adi { get; set; }

        [StringLength(255)]
        public string Soyadi { get; set; }

        [StringLength(255)]
        public string KadroBirimi { get; set; }

        [StringLength(255)]
        public string GorevlendirildigiKurum { get; set; }

        public DateTime? UYKTarihi { get; set; }

        [StringLength(50)]
        public string UYKNo { get; set; }

        [Column(TypeName = "text")]
        public string Aciklama { get; set; }

        [StringLength(255)]
        public string Durumu { get; set; }
    }
}
