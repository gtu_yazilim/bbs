namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class AuditLogs
    {
        [Key]
        [Column(Order = 0)]
        public long AuditLogId { get; set; }

        public string UserName { get; set; }

        [Key]
        [Column(Order = 1)]
        public DateTime EventDateUTC { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EventType { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(512)]
        public string TypeFullName { get; set; }

        [Key]
        [Column(Order = 4)]
        [StringLength(256)]
        public string RecordId { get; set; }
    }
}
