namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.DersCiktiYontem")]
    public partial class DersCiktiYontem
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DersCiktiYontem()
        {
            DersCiktiYontemIliski = new HashSet<DersCiktiYontemIliski>();
            DilDersCiktiYontem = new HashSet<DilDersCiktiYontem>();
        }

        public int ID { get; set; }

        public byte? YontemTur { get; set; }

        [StringLength(2)]
        public string YontemNo { get; set; }

        public short Yil { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DersCiktiYontemIliski> DersCiktiYontemIliski { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DilDersCiktiYontem> DilDersCiktiYontem { get; set; }
    }
}
