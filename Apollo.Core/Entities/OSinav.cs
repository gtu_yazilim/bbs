namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.OSinav")]
    public partial class OSinav
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OSinav()
        {
            OSinavGrup = new HashSet<OSinavGrup>();
            OSinavDosya = new HashSet<OSinavDosya>();
            OSinavSoru = new HashSet<OSinavSoru>();
            OSinavCevap = new HashSet<OSinavCevap>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(255)]
        public string Ad { get; set; }

        public int EklemeTuru { get; set; }

        [StringLength(1000)]
        public string Aciklama { get; set; }

        public DateTime Baslangic { get; set; }

        public DateTime Bitis { get; set; }

        public int Sure { get; set; }

        public int SoruSayisi { get; set; }

        public int Yayinlandi { get; set; }

        [Required]
        [StringLength(50)]
        public string Ekleyen { get; set; }

        public int EkleyenID { get; set; }

        public DateTime EklemeTarihi { get; set; }

        public int? GuncelleyenID { get; set; }

        [StringLength(50)]
        public string Guncelleyen { get; set; }

        public DateTime? GuncellemeTarihi { get; set; }

        public bool Silindi { get; set; }

        public bool MazeretSinav { get; set; }

        public DateTime? TarihMazeretBaslangic { get; set; }

        public DateTime? TarihMazeretBitis { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OSinavGrup> OSinavGrup { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OSinavDosya> OSinavDosya { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OSinavSoru> OSinavSoru { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OSinavCevap> OSinavCevap { get; set; }
    }
}
