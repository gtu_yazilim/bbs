namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("mernis.MernisUygulamaYetki")]
    public partial class MernisUygulamaYetki
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MernisUygulamaYetki()
        {
            MernisUygulamaYetkiIP = new HashSet<MernisUygulamaYetkiIP>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string UygulamaAdi { get; set; }

        [Required]
        [StringLength(50)]
        public string UygulamaSifresi { get; set; }

        [Required]
        [StringLength(50)]
        public string Controller { get; set; }

        [StringLength(50)]
        public string Action { get; set; }

        [Required]
        [StringLength(50)]
        public string Kullanici { get; set; }

        public bool Silindi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MernisUygulamaYetkiIP> MernisUygulamaYetkiIP { get; set; }
    }
}
