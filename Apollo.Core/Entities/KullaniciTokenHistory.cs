namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ytk.KullaniciTokenHistory")]
    public partial class KullaniciTokenHistory
    {
        public int ID { get; set; }

        public int FKKullaniciID { get; set; }

        [Required]
        [StringLength(50)]
        public string AccessToken { get; set; }

        [Required]
        [StringLength(50)]
        public string RefreshToken { get; set; }

        public DateTime ExpireDate { get; set; }

        public DateTime Date { get; set; }

        public virtual Kullanici Kullanici { get; set; }
    }
}
