namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.DereceIlerletme")]
    public partial class DereceIlerletme
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DereceIlerletme()
        {
            DereceIlerletmeDetay = new HashSet<DereceIlerletmeDetay>();
        }

        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public int? BelgeNo { get; set; }

        public DateTime? BaslangicTarihi { get; set; }

        public DateTime? OnayTarihi { get; set; }

        [StringLength(10)]
        public string EvrakNo { get; set; }

        [StringLength(250)]
        public string Aciklama { get; set; }

        public int ENUMKadroTipi { get; set; }

        public int? FKUlkeID { get; set; }

        public int? FKUniversiteID { get; set; }

        public int? FKFakulteID { get; set; }

        public int? FKSebepID { get; set; }

        public virtual Birimler Birimler { get; set; }

        public virtual Birimler Birimler1 { get; set; }

        public virtual TNMUlke TNMUlke { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DereceIlerletmeDetay> DereceIlerletmeDetay { get; set; }
    }
}
