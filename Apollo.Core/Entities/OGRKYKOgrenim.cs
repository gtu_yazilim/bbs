namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRKYKOgrenim")]
    public partial class OGRKYKOgrenim
    {
        public int ID { get; set; }

        public long OgrenimKrediNo { get; set; }

        public DateTime? OgrenimBaslamaTarihi { get; set; }

        public DateTime? OgrenimBitisTarihi { get; set; }

        public DateTime? OgrenimIptalTarihi { get; set; }

        public int FKOgrenciID { get; set; }

        public DateTime? KayitTarihi { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        public DateTime? GuncellenmeTarihi { get; set; }

        [StringLength(50)]
        public string GuncelleyenKullanici { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
