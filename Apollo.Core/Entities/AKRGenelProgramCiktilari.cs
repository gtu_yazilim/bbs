namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.AKRGenelProgramCiktilari")]
    public partial class AKRGenelProgramCiktilari
    {
        [Key]
        [Column(Order = 0)]
        public int ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FKDersPlanAnaID { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FKDersGrupID { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EnumOgretimTur { get; set; }

        [Key]
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Yil { get; set; }

        [Key]
        [Column(Order = 5)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Donem { get; set; }

        public int? FKProgramCiktiID { get; set; }

        [Key]
        [Column(Order = 6)]
        [StringLength(10)]
        public string ProgramCikti { get; set; }

        [Key]
        [Column(Order = 7)]
        public double CiktiPuanOrtalama { get; set; }

        [Key]
        [Column(Order = 8)]
        public DateTime SGTarih { get; set; }

        [Key]
        [Column(Order = 9)]
        [StringLength(30)]
        public string SGKullanici { get; set; }

        [Key]
        [Column(Order = 10)]
        [StringLength(20)]
        public string SGIP { get; set; }

        public virtual BolumYeterlilik BolumYeterlilik { get; set; }

        public virtual OGRDersGrup OGRDersGrup { get; set; }
    }
}
