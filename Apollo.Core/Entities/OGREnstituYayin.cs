namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGREnstituYayin")]
    public partial class OGREnstituYayin
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public int? EnumYayinTip { get; set; }

        public int? FKOgrenciID { get; set; }

        [StringLength(255)]
        public string AU { get; set; }

        [StringLength(255)]
        public string TI { get; set; }

        [StringLength(255)]
        public string SO { get; set; }

        [StringLength(30)]
        public string PD { get; set; }

        [StringLength(4)]
        public string PY { get; set; }

        [StringLength(50)]
        public string DI { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
