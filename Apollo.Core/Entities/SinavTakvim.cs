namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.SinavTakvim")]
    public partial class SinavTakvim
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SinavTakvim()
        {
            SinavMekan = new HashSet<SinavMekan>();
        }

        public int ID { get; set; }

        public int FKDersPlanID { get; set; }

        public int Sure { get; set; }

        public DateTime BaslamaTarihSaati { get; set; }

        public DateTime BitisTarihSaati { get; set; }

        [StringLength(250)]
        public string Aciklama { get; set; }

        [StringLength(50)]
        public string EkleyenKullaniciAd { get; set; }

        public int EnumCalismaTip { get; set; }

        public virtual OGRDersPlan OGRDersPlan { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SinavMekan> SinavMekan { get; set; }
    }
}
