namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.WebBolumDuzeyYeterlilik")]
    public partial class WebBolumDuzeyYeterlilik
    {
        public int ID { get; set; }

        public byte? Duzey { get; set; }

        public byte? Kategori { get; set; }

        [Column(TypeName = "text")]
        public string Aciklama { get; set; }

        public byte? Sira { get; set; }

        public byte? FormDil { get; set; }
    }
}
