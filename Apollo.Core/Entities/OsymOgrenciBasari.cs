namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("osym.OsymOgrenciBasari")]
    public partial class OsymOgrenciBasari
    {
        public int ID { get; set; }

        public int? FKOgrenciID { get; set; }

        [StringLength(11)]
        public string TCNO { get; set; }

        public int? GirisYil { get; set; }

        public string YABANCI_DIL { get; set; }

        public string OKUL_KOD { get; set; }

        public string OKUL_TUR { get; set; }

        public string DIPLOMA_NOTU { get; set; }

        public string ONCEKI_YIL_YERLESME_DURUMU { get; set; }

        public string LYS1_MATEMATIK_DOGRU { get; set; }

        public string LYS1_MATEMATIK_YANLIS { get; set; }

        public string LYS1_GEOMETRI_DOGRU { get; set; }

        public string LYS1_GEOMETRI_YANLIS { get; set; }

        public string LYS2_FIZIK_DOGRU { get; set; }

        public string LYS2_FIZIK_YANLIS { get; set; }

        public string LYS2_KIMYA_DOGRU { get; set; }

        public string LYS2_KIMYA_YANLIS { get; set; }

        public string LYS2_BIYOLOJI_DOGRU { get; set; }

        public string LYS2_BIYOLOJI_YANLIS { get; set; }

        public string LYS3_EDEBIYAT_DOGRU { get; set; }

        public string LYS3_EDEBIYAT_YANLIS { get; set; }

        public string LYS3_COGRAFYA1_DOGRU { get; set; }

        public string LYS3_COGRAFYA1_YANLIS { get; set; }

        public string LYS4_TARIH_DOGRU { get; set; }

        public string LYS4_TARIH_YANLIS { get; set; }

        public string LYS4_COGRAFYA2_DOGRU { get; set; }

        public string LYS4_COGRAFYA2_YANLIS { get; set; }

        public string LYS4_FELSEFE_DOGRU { get; set; }

        public string LYS4_FELSEFE_YANLIS { get; set; }

        public string LYS5_DIL_DOGRU { get; set; }

        public string LYS5_DIL_YANLIS { get; set; }

        public string MF1 { get; set; }

        public string MF1_BASARI_SIRASI { get; set; }

        public string MF2 { get; set; }

        public string MF2_BASARI_SIRASI { get; set; }

        public string MF3 { get; set; }

        public string MF3_BASARI_SIRASI { get; set; }

        public string MF4 { get; set; }

        public string MF4_BASARI_SIRASI { get; set; }

        public string MF1_YERLESTIRME { get; set; }

        public string MF1_YERLESTIRME_BASARI_SIRASI { get; set; }

        public string MF2_YERLESTIRME { get; set; }

        public string MF2_YERLESTIRME_BASARI_SIRASI { get; set; }

        public string MF3_YERLESTIRME { get; set; }

        public string MF3_YERLESTIRME_BASARI_SIRASI { get; set; }

        public string MF4_YERLESTIRME { get; set; }

        public string MF4_YERLESTIRME_BASARI_SIRASI { get; set; }

        public string TM1 { get; set; }

        public string TM1_BASARI_SIRASI { get; set; }

        public string TM2 { get; set; }

        public string TM2_BASARI_SIRASI { get; set; }

        public string TM3 { get; set; }

        public string TM3_BASARI_SIRASI { get; set; }

        public string TM1_YERLESTIRME { get; set; }

        public string TM1_YERLESTIRME_BASARI_SIRASI { get; set; }

        public string TM2_YERLESTIRME { get; set; }

        public string TM2_YERLESTIRME_BASARI_SIRASI { get; set; }

        public string TM3_YERLESTIRME { get; set; }

        public string TM3_YERLESTIRME_BASARI_SIRASI { get; set; }

        public string TS1 { get; set; }

        public string TS1_BASARI_SIRASI { get; set; }

        public string TS2 { get; set; }

        public string TS2_BASARI_SIRASI { get; set; }

        public string TS1_YERLESTIRME { get; set; }

        public string TS1_YERLESTIRME_BASARI_SIRASI { get; set; }

        public string TS2_YERLESTIRME { get; set; }

        public string TS2_YERLESTIRME_BASARI_SIRASI { get; set; }

        public string DIL1_YERLESTIRME { get; set; }

        public string DIL1_YERLESTIRME_BASARI_SIRASI { get; set; }

        public string DIL2_YERLESTIRME { get; set; }

        public string DIL2_YERLESTIRME_BASARI_SIRASI { get; set; }

        public string DIL3_YERLESTIRME { get; set; }

        public string DIL3_YERLESTIRME_BASARI_SIRASI { get; set; }

        public string YGS1_YERLESTIRME { get; set; }

        public string YGS1_YERLESTIRME_BASARI_SIRASI { get; set; }

        public string YGS2_YERLESTIRME { get; set; }

        public string YGS2_YERLESTIRME_BASARI_SIRASI { get; set; }

        public string YGS3_YERLESTIRME { get; set; }

        public string YGS3_YERLESTIRME_BASARI_SIRASI { get; set; }

        public string YGS4_YERLESTIRME { get; set; }

        public string YGS4_YERLESTIRME_BASARI_SIRASI { get; set; }

        public string YGS5_YERLESTIRME { get; set; }

        public string YGS5_YERLESTIRME_BASARI_SIRASI { get; set; }

        public string YGS6_YERLESTIRME { get; set; }

        public string YGS6_YERLESTIRME_BASARI_SIRASI { get; set; }

        public int? YGS_TURKCE_DOGRU { get; set; }

        public int? YGS_TURKCE_YANLIS { get; set; }

        public int? YGS_SOSYAL_BILIMLER_DOGRU { get; set; }

        public int? YGS_SOSYAL_BILIMLER_YANLIS { get; set; }

        public int? YGS_TEMEL_MATEMATIK_DOGRU { get; set; }

        public int? YGS_TEMEL_MATEMATIK_YANLIS { get; set; }

        public int? YGS_FEN_BILIMLERI_DOGRU { get; set; }

        public int? YGS_FEN_BILIMLERI_YANLIS { get; set; }

        public bool? oss_yerlestirilmis_mi { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
