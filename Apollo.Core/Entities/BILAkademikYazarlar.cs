namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.BILAkademikYazarlar")]
    public partial class BILAkademikYazarlar
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id { get; set; }

        [StringLength(11)]
        public string tc_no { get; set; }

        [StringLength(4)]
        public string py { get; set; }

        public int? YT1 { get; set; }

        public int? yayin_id { get; set; }

        public short? fak_kod { get; set; }

        public short? bol_kod { get; set; }

        public int? puan { get; set; }

        public byte? kisi { get; set; }

        [StringLength(1000)]
        public string yayin { get; set; }

        [StringLength(2)]
        public string YT { get; set; }

        public int? sayi { get; set; }

        [StringLength(60)]
        public string YT1_aciklama { get; set; }

        public bool? YT1a { get; set; }
    }
}
