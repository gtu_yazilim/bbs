namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.TNMAciklama")]
    public partial class TNMAciklama
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int AciklamaID { get; set; }

        [StringLength(250)]
        public string AciklamaKisaltma { get; set; }

        [Column(TypeName = "text")]
        public string AciklamaAd { get; set; }

        public bool AkademikGorsun { get; set; }

        public int? KullaniciKod { get; set; }

        public DateTime? IslemTarihi { get; set; }
    }
}
