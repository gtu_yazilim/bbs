namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ytk.TNMDomain")]
    public partial class TNMDomain
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TNMDomain()
        {
            KullaniciAlias = new HashSet<KullaniciAlias>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(200)]
        public string Domain { get; set; }

        public bool BirincilMi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<KullaniciAlias> KullaniciAlias { get; set; }
    }
}
