namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.ABSDegerlendirmeDosya")]
    public partial class ABSDegerlendirmeDosya
    {
        public int ID { get; set; }

        public DateTime TarihKayit { get; set; }

        [Required]
        [StringLength(200)]
        public string DosyaKey { get; set; }

        public int FKDersGrupID { get; set; }

        [StringLength(100)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string IP { get; set; }

        public int FKDegerlendirmeID { get; set; }

        public virtual ABSDegerlendirme ABSDegerlendirme { get; set; }

        public virtual OGRDersGrup OGRDersGrup { get; set; }
    }
}
