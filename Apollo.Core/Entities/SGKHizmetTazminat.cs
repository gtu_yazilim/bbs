namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.SGKHizmetTazminat")]
    public partial class SGKHizmetTazminat
    {
        public int ID { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? Zaman { get; set; }

        public int? kayitNo { get; set; }

        [StringLength(11)]
        public string Tckn { get; set; }

        public int? unvanKod { get; set; }

        public int? makam { get; set; }

        public int? gorev { get; set; }

        public int? temsil { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? tazminatTarihi { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? tazminatBitisTarihi { get; set; }

        public int? kadrosuzluk { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? kurumOnayTarihi { get; set; }

        [StringLength(250)]
        public string SGKDurumAciklama { get; set; }

        public int? ENUMSGKDurum { get; set; }

        public int? FKPersonelBilgisiID { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }
    }
}
