namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.KaraListe")]
    public partial class KaraListe
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        public string Kullanici { get; set; }

        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        [Required]
        [StringLength(11)]
        public string TCNo { get; set; }

        [Required]
        [StringLength(50)]
        public string Adi { get; set; }

        [Required]
        [StringLength(50)]
        public string Soyadi { get; set; }

        public bool Vekil { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? CikarilmaTarihi { get; set; }

        public byte CezaKitabi { get; set; }

        [Required]
        [StringLength(10)]
        public string CezaMaddesi { get; set; }

        [Required]
        [StringLength(50)]
        public string FakulteAd { get; set; }

        [Required]
        [StringLength(20)]
        public string YOKYazisi { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? YOKTarihi { get; set; }

        public byte ENUMPersonelTipi { get; set; }

        public short FKKadroUnvaniID { get; set; }

        public int FKUlkeID { get; set; }

        public int FKUniversiteID { get; set; }

        public int FKFakulteID { get; set; }

        public virtual TNMFakulte TNMFakulte { get; set; }

        public virtual TNMUniversite TNMUniversite { get; set; }
    }
}
