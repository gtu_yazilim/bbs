namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("osym.OgretimUyeSayilar")]
    public partial class OgretimUyeSayilar
    {
        public int ID { get; set; }

        [StringLength(255)]
        public string Kod { get; set; }

        [StringLength(255)]
        public string ProgramAdi { get; set; }

        public int? Prof { get; set; }

        public int? Doc { get; set; }

        public int? YDoc { get; set; }
    }
}
