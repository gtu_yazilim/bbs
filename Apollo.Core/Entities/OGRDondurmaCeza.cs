namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRDondurmaCeza")]
    public partial class OGRDondurmaCeza
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(500)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? InKod { get; set; }

        [StringLength(100)]
        public string Karar { get; set; }

        public int? Yil { get; set; }

        public DateTime? YonKurTarih { get; set; }

        [StringLength(50)]
        public string YonKurNo { get; set; }

        public int? EnumDonem { get; set; }

        public int? EnumCezaTur { get; set; }

        public int? EnumIslemTip { get; set; }

        public int? EnumCezaKaldir { get; set; }

        public int? FKOgrenciID { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
