namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.OgrenimBilgisi")]
    public partial class OgrenimBilgisi
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public int? KayitNo { get; set; }

        [StringLength(50)]
        public string MahalleKoy { get; set; }

        [StringLength(150)]
        public string OkulAdi { get; set; }

        [StringLength(150)]
        public string BolumAdi { get; set; }

        [StringLength(100)]
        public string UzmanlikAlani { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? KayitTarihi { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? MezuniyetTarihi { get; set; }

        public int OgrenimSuresi { get; set; }

        public bool? Hazirlik { get; set; }

        public bool IseBaslarkenki { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? KurumOnayTarihi { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? DenklikTarihi { get; set; }

        [StringLength(100)]
        public string DenklikOkul { get; set; }

        [StringLength(100)]
        public string DenklikBolum { get; set; }

        [StringLength(250)]
        public string SGKDurumAciklama { get; set; }

        public int ENUMOgrenimTuru { get; set; }

        public int? ENUMOgrenimYeri { get; set; }

        public int? ENUMSGKDurum { get; set; }

        public int FKPersonelBilgisiID { get; set; }

        public int? FKUlkeID { get; set; }

        public long? FKUniversiteID { get; set; }

        public long? FKFakulteID { get; set; }

        public long? FKBolumID { get; set; }

        public long? FKAnaBilimDaliID { get; set; }

        public int? FKIlID { get; set; }

        public int? FKIlceID { get; set; }

        public bool Durum { get; set; }

        public virtual TNMIl TNMIl { get; set; }

        public virtual TNMIlce TNMIlce { get; set; }

        public virtual TNMUlke TNMUlke { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }
    }
}
