namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRDersOnSart")]
    public partial class OGRDersOnSart
    {
        public int ID { get; set; }

        public DateTime TarihKayit { get; set; }

        public int FKDersPlanAnaID { get; set; }

        public int OnSartFKDersPlanAnaID { get; set; }

        public int Yil { get; set; }

        public int Donem { get; set; }

        public int FKBirimID { get; set; }

        public bool Silindi { get; set; }
    }
}
