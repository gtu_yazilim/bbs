namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRDisiplinCeza")]
    public partial class OGRDisiplinCeza
    {
        public int ID { get; set; }

        public DateTime? KayitTarihi { get; set; }

        [StringLength(500)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string IP { get; set; }

        public string Karar { get; set; }

        public int Yil { get; set; }

        public DateTime? YonKurTarih { get; set; }

        [StringLength(50)]
        public string YonKurNo { get; set; }

        public int EnumDonem { get; set; }

        public int EnumCezaTur { get; set; }

        public bool CezaKaldirdiMi { get; set; }

        public int FKOgrenciID { get; set; }

        public DateTime? SGTarihi { get; set; }

        [Required]
        [StringLength(50)]
        public string SGKullanici { get; set; }

        public bool Silindi { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
