namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ytk.LoginGuvenlik")]
    public partial class LoginGuvenlik
    {
        public int ID { get; set; }

        public DateTime Tarih { get; set; }

        public bool BasariliMi { get; set; }

        [StringLength(100)]
        public string KullaniciAdi { get; set; }

        [StringLength(50)]
        public string IP { get; set; }
    }
}
