namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.TNMIdariOdenekler")]
    public partial class TNMIdariOdenekler
    {
        public short ID { get; set; }

        public DateTime? Zaman { get; set; }

        public string Kullanici { get; set; }

        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public short? InKod { get; set; }

        [Required]
        [StringLength(100)]
        public string KadroUnvaniAd { get; set; }

        public short GorevAyligiDereceAlt { get; set; }

        public short GorevAyligiDereceUst { get; set; }

        public short KidemYiliAlt { get; set; }

        public short KidemYiliUst { get; set; }

        public short OgrenimStatusu { get; set; }

        public short IsGucluguZammi { get; set; }

        public short IsRiskiZammi { get; set; }

        public short TeminGucluguZammiCetvel { get; set; }

        public short TeminGucluguZammiNot { get; set; }

        public short MaliSorumlulukZammi { get; set; }

        public float TazminatOrani { get; set; }

        public short EkTazminat { get; set; }

        public short EkGosterge { get; set; }

        public short MakamTazminati { get; set; }

        public short GorevTazminati { get; set; }

        [StringLength(250)]
        public string Aciklama { get; set; }

        public short? KullaniciKod { get; set; }

        public DateTime? IslemTarihi { get; set; }

        public int FKKadroUnvaniID { get; set; }

        public virtual TNMKadroUnvani TNMKadroUnvani { get; set; }
    }
}
