namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRArsiv")]
    public partial class OGRArsiv
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(500)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? InKod { get; set; }

        public int? Yil { get; set; }

        public DateTime? Tarih { get; set; }

        [StringLength(100)]
        public string YKNo { get; set; }

        public string Aciklama { get; set; }

        [StringLength(50)]
        public string Ortalama { get; set; }

        public double? Ortalama4 { get; set; }

        public double? Ortalama100 { get; set; }

        public int? EnumArsivDurum { get; set; }

        public int? EnumDonem { get; set; }

        public int? FKOgrenciID { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public bool Silindi { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
