namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRHarcEskiBorcSilinen")]
    public partial class OGRHarcEskiBorcSilinen
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(500)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public DateTime? Tarih { get; set; }

        public double? Miktar { get; set; }

        public int? Yil { get; set; }

        public int? EnumDonem { get; set; }

        public int? EnumHarcAtamaNedeni { get; set; }

        public int? FKOgrenciID { get; set; }

        [StringLength(250)]
        public string Aciklama { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
