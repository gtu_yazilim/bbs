namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("deg.DegisimKayit")]
    public partial class DegisimKayit
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DegisimKayit()
        {
            DegisimKayitDonemleri = new HashSet<DegisimKayitDonemleri>();
        }

        public int ID { get; set; }

        public int? Yil { get; set; }

        public DateTime? YonKurTarih { get; set; }

        [StringLength(50)]
        public string YonKurNo { get; set; }

        public int? EnumDonem { get; set; }

        public int? FKUlkeID { get; set; }

        public int? FKUniversiteID { get; set; }

        public int? FKOgrenciID { get; set; }

        public int? FKPersonelID { get; set; }

        public int? EnumDegisimTipi { get; set; }

        public int? FKDegisimYerlesmeID { get; set; }

        public DateTime? FaaliyetBaslangicTarih { get; set; }

        public DateTime? FaaliyetBitisTarih { get; set; }

        public int? EnumGelenGiden { get; set; }

        public int? EnumHareketTipi { get; set; }

        public bool? Arsiv { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        public int? EnumOnay { get; set; }

        public string Aciklama { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public string UniversiteAdi { get; set; }

        public virtual Birimler Birimler { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }

        public virtual TNMUlke TNMUlke { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DegisimKayitDonemleri> DegisimKayitDonemleri { get; set; }
    }
}
