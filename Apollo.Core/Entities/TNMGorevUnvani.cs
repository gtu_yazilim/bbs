namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.TNMGorevUnvani")]
    public partial class TNMGorevUnvani
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TNMGorevUnvani()
        {
            IdariGorev = new HashSet<IdariGorev>();
            Grup = new HashSet<Grup>();
            TNMIdariGorev = new HashSet<TNMIdariGorev>();
        }

        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int UnvanID { get; set; }

        [StringLength(50)]
        public string UnvanKisaltma { get; set; }

        [StringLength(50)]
        public string UnvanAd { get; set; }

        [StringLength(50)]
        public string UnvanEngAd { get; set; }

        [StringLength(50)]
        public string UnvanEngKisaltma { get; set; }

        public int UnvanSeviye { get; set; }

        public int EnumGorevUnvaniTur { get; set; }

        public int? KullaniciKod { get; set; }

        public DateTime? IslemTarihi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IdariGorev> IdariGorev { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Grup> Grup { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TNMIdariGorev> TNMIdariGorev { get; set; }
    }
}
