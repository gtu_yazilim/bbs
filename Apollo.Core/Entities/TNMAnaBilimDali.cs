namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.TNMAnaBilimDali")]
    public partial class TNMAnaBilimDali
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TNMAnaBilimDali()
        {
            IzinSakliDetay = new HashSet<IzinSakliDetay>();
            IzinSakliDetay1 = new HashSet<IzinSakliDetay>();
            IzinSakliToplam = new HashSet<IzinSakliToplam>();
            IzinSakliToplam1 = new HashSet<IzinSakliToplam>();
        }

        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? UlkeKod { get; set; }

        public int? UniversiteKod { get; set; }

        public int? FakulteKod { get; set; }

        public int? BolumKod { get; set; }

        public int? AnaBilimDaliKod { get; set; }

        [Required]
        [StringLength(100)]
        public string AnaBilimDaliAd { get; set; }

        public bool AkademikGorsun { get; set; }

        [StringLength(20)]
        public string ACYokEvrakNo { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? ACYokEvrakTarihi { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? ACYokKurulTarihi { get; set; }

        [StringLength(20)]
        public string ACResmiGazeteSayisi { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? ACResmiGazeteTarihi { get; set; }

        public bool ACResmiGazeteMukerrerMi { get; set; }

        public bool Durum { get; set; }

        public int FKUlkeID { get; set; }

        public int FKUniversiteID { get; set; }

        public int FKFakulteID { get; set; }

        public int FKBolumID { get; set; }

        public int? KullaniciKod { get; set; }

        public DateTime? IslemTarihi { get; set; }

        public int? FKYoksisID { get; set; }

        public int? FKBirimID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IzinSakliDetay> IzinSakliDetay { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IzinSakliDetay> IzinSakliDetay1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IzinSakliToplam> IzinSakliToplam { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IzinSakliToplam> IzinSakliToplam1 { get; set; }

        public virtual TNMBolum TNMBolum { get; set; }

        public virtual TNMFakulte TNMFakulte { get; set; }

        public virtual TNMUniversite TNMUniversite { get; set; }
    }
}
