namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.OOdevOgrenci")]
    public partial class OOdevOgrenci
    {
        [Key]
        [Column(Order = 0)]
        public int ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FKOOdevID { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FKOgrenciID { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(50)]
        public string DosyaAdi { get; set; }

        [Key]
        [Column(Order = 4)]
        [StringLength(50)]
        public string DosyaKey { get; set; }

        [Key]
        [Column(Order = 5)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DosyaBoyutu { get; set; }

        [Key]
        [Column(Order = 6)]
        [StringLength(10)]
        public string DosyaTuru { get; set; }

        [Key]
        [Column(Order = 7)]
        public DateTime YuklemeTarihi { get; set; }

        [Key]
        [Column(Order = 8)]
        public bool Silindi { get; set; }

        public DateTime? SilinmeTarihi { get; set; }

        [Key]
        [Column(Order = 9)]
        [StringLength(50)]
        public string SGKullanici { get; set; }

        [Key]
        [Column(Order = 10)]
        [StringLength(50)]
        public string SGIP { get; set; }

        [StringLength(3000)]
        public string Aciklama { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
