namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRTnmAFKanunlari")]
    public partial class OGRTnmAFKanunlari
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OGRTnmAFKanunlari()
        {
            OGRKimlikFarkliGelis = new HashSet<OGRKimlikFarkliGelis>();
        }

        public int ID { get; set; }

        [StringLength(25)]
        public string KanunSayisi { get; set; }

        public DateTime? YururlukTarihi { get; set; }

        [StringLength(25)]
        public string ResmiGazeteNo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRKimlikFarkliGelis> OGRKimlikFarkliGelis { get; set; }
    }
}
