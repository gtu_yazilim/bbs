namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("mkn.MekanBinalar")]
    public partial class MekanBinalar1
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MekanBinalar1()
        {
            Mekan = new HashSet<Mekan>();
        }

        public int ID { get; set; }

        [StringLength(50)]
        public string kodBina { get; set; }

        [StringLength(2)]
        public string FakKod { get; set; }

        [StringLength(2)]
        public string BolKod { get; set; }

        public byte? Kat { get; set; }

        public byte? Kapi { get; set; }

        public bool? Asansor { get; set; }

        public bool? Jenerator { get; set; }

        public bool? Klima { get; set; }

        public int? En { get; set; }

        public int? Boy { get; set; }

        public int? Toplam { get; set; }

        public int? AcikAlan { get; set; }

        public int? ToplamAlan { get; set; }

        [StringLength(256)]
        public string Aciklama { get; set; }

        public int? idari { get; set; }

        public int? derslik { get; set; }

        public int? laboratuvar { get; set; }

        public int? sosyal { get; set; }

        public int? spor { get; set; }

        public int? ofis { get; set; }

        [StringLength(100)]
        public string fakAd { get; set; }

        public bool? goster { get; set; }

        public int? FKKampusID { get; set; }

        public int? FKIslevID { get; set; }

        public int? FKFakulteID { get; set; }

        [Column(TypeName = "image")]
        public byte[] foto { get; set; }

        public int? anfi { get; set; }

        public int? kantin { get; set; }

        public int? toplanti_odasi { get; set; }

        public int? ogretimElOfis { get; set; }

        public int? ortak { get; set; }

        public int? net_kapali_alan { get; set; }

        public int? brut_alan { get; set; }

        [StringLength(10)]
        public string kod { get; set; }

        public int? UnvKod { get; set; }

        [StringLength(100)]
        public string Kullanici { get; set; }

        public DateTime? Zaman { get; set; }

        public virtual Birimler Birimler { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Mekan> Mekan { get; set; }

        public virtual TNMMekanIslevler TNMMekanIslevler { get; set; }

        public virtual TNMMekanKampusler TNMMekanKampusler { get; set; }
    }
}
