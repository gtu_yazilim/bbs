namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRMezunBilgi")]
    public partial class OGRMezunBilgi
    {
        public int ID { get; set; }

        public int? FKOgrenciID { get; set; }

        [StringLength(100)]
        public string ePosta { get; set; }

        [StringLength(100)]
        public string web { get; set; }

        [StringLength(20)]
        public string telefon { get; set; }

        [StringLength(20)]
        public string gsm { get; set; }

        public bool? calisiyor { get; set; }

        public bool? alaninda { get; set; }

        [StringLength(100)]
        public string kurum { get; set; }

        [StringLength(100)]
        public string adres { get; set; }

        [StringLength(100)]
        public string unvan { get; set; }

        [StringLength(300)]
        public string aciklama { get; set; }

        [StringLength(30)]
        public string kullanici { get; set; }

        public DateTime? zaman { get; set; }

        [Column(TypeName = "image")]
        public byte[] foto { get; set; }
    }
}
