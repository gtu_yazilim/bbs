namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ytk.KullaniciHesapAyar")]
    public partial class KullaniciHesapAyar
    {
        public int ID { get; set; }

        public int FKKullaniciID { get; set; }

        public bool GmailIzin { get; set; }

        public string MenuDizilim { get; set; }

        [StringLength(128)]
        public string GmailToken { get; set; }

        public virtual Kullanici Kullanici { get; set; }
    }
}
