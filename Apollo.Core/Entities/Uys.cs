namespace Apollo.Core.Entities
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Uys : DbContext
    {
        public Uys()
            : base("name=Uys")
        {
        }

        public virtual DbSet<ABSBasariNot> ABSBasariNot { get; set; }
        public virtual DbSet<ABSBirimSorumlu> ABSBirimSorumlu { get; set; }
        public virtual DbSet<ABSCevapAnahtari> ABSCevapAnahtari { get; set; }
        public virtual DbSet<ABSDanismanOnay> ABSDanismanOnay { get; set; }
        public virtual DbSet<ABSDegerlendirme> ABSDegerlendirme { get; set; }
        public virtual DbSet<ABSDegerlendirmeDosya> ABSDegerlendirmeDosya { get; set; }
        public virtual DbSet<ABSDegerlendirmeKural> ABSDegerlendirmeKural { get; set; }
        public virtual DbSet<ABSDevamTakipKontrol> ABSDevamTakipKontrol { get; set; }
        public virtual DbSet<ABSDevamTakipV2> ABSDevamTakipV2 { get; set; }
        public virtual DbSet<ABSDokuman> ABSDokuman { get; set; }
        public virtual DbSet<ABSGrupCalisma> ABSGrupCalisma { get; set; }
        public virtual DbSet<ABSIPYetki> ABSIPYetki { get; set; }
        public virtual DbSet<ABSMesaj> ABSMesaj { get; set; }
        public virtual DbSet<ABSOgrenciDanismanlik> ABSOgrenciDanismanlik { get; set; }
        public virtual DbSet<ABSOgrenciIleGorusme> ABSOgrenciIleGorusme { get; set; }
        public virtual DbSet<ABSOgrenciNot> ABSOgrenciNot { get; set; }
        public virtual DbSet<ABSOgrenciNotDetay> ABSOgrenciNotDetay { get; set; }
        public virtual DbSet<ABSOgrenciyeMesaj> ABSOgrenciyeMesaj { get; set; }
        public virtual DbSet<ABSPaylar> ABSPaylar { get; set; }
        public virtual DbSet<ABSPaylarOlcme> ABSPaylarOlcme { get; set; }
        public virtual DbSet<ABSPaylarOlcmeOgrenme> ABSPaylarOlcmeOgrenme { get; set; }
        public virtual DbSet<ABSPaylarOlcmeProgram> ABSPaylarOlcmeProgram { get; set; }
        public virtual DbSet<ABSRolYetki> ABSRolYetki { get; set; }
        public virtual DbSet<ABSSistemTarihleri> ABSSistemTarihleri { get; set; }
        public virtual DbSet<ABSTNMHafta> ABSTNMHafta { get; set; }
        public virtual DbSet<ABSYayinlananBasariNotlar> ABSYayinlananBasariNotlar { get; set; }
        public virtual DbSet<ABSYayinlananPayNotlar> ABSYayinlananPayNotlar { get; set; }
        public virtual DbSet<ABSYetki> ABSYetki { get; set; }
        public virtual DbSet<ABSYetkiliKullanici> ABSYetkiliKullanici { get; set; }
        public virtual DbSet<ABSYetkiModul> ABSYetkiModul { get; set; }
        public virtual DbSet<ABSYetkliRolleri> ABSYetkliRolleri { get; set; }
        public virtual DbSet<DanismanlikBilgileri> DanismanlikBilgileri { get; set; }
        public virtual DbSet<OSinav> OSinav { get; set; }
        public virtual DbSet<OSinavGrup> OSinavGrup { get; set; }
        public virtual DbSet<DersIcerikPuani> DersIcerikPuani { get; set; }
        public virtual DbSet<OgrenciDersCevap> OgrenciDersCevap { get; set; }
        public virtual DbSet<CapYandalKontenjan> CapYandalKontenjan { get; set; }
        public virtual DbSet<CapYandalTercih> CapYandalTercih { get; set; }
        public virtual DbSet<EnkaBarinmaBursu> EnkaBarinmaBursu { get; set; }
        public virtual DbSet<GecisKontenjan> GecisKontenjan { get; set; }
        public virtual DbSet<GecisTercih> GecisTercih { get; set; }
        public virtual DbSet<HazirlikBasvuru> HazirlikBasvuru { get; set; }
        public virtual DbSet<KitapBursu> KitapBursu { get; set; }
        public virtual DbSet<KitapBursuKardesBilgisi> KitapBursuKardesBilgisi { get; set; }
        public virtual DbSet<MuafiyetSinavBasvuru> MuafiyetSinavBasvuru { get; set; }
        public virtual DbSet<MuafiyetSinavBatchKey> MuafiyetSinavBatchKey { get; set; }
        public virtual DbSet<MuafiyetSinavOturum> MuafiyetSinavOturum { get; set; }
        public virtual DbSet<MuafiyetSinavSalonTanim> MuafiyetSinavSalonTanim { get; set; }
        public virtual DbSet<MuafiyetSinavTanim> MuafiyetSinavTanim { get; set; }
        public virtual DbSet<MuafiyetSinavTIN> MuafiyetSinavTIN { get; set; }
        public virtual DbSet<OgrenciEtkinlikBilgiFormu> OgrenciEtkinlikBilgiFormu { get; set; }
        public virtual DbSet<YazOkuluOnKayit> YazOkuluOnKayit { get; set; }
        public virtual DbSet<YazOkuluOnKayitDersler> YazOkuluOnKayitDersler { get; set; }
        public virtual DbSet<BankaKurumHesapNumaralari> BankaKurumHesapNumaralari { get; set; }
        public virtual DbSet<BankaOdemeler> BankaOdemeler { get; set; }
        public virtual DbSet<BankaServisLog> BankaServisLog { get; set; }
        public virtual DbSet<BankaWebServisHareketi> BankaWebServisHareketi { get; set; }
        public virtual DbSet<OnlineHarcBorclari> OnlineHarcBorclari { get; set; }
        public virtual DbSet<OnlineHarcOdemeIptalleri> OnlineHarcOdemeIptalleri { get; set; }
        public virtual DbSet<OnlineHarcOdemeleri> OnlineHarcOdemeleri { get; set; }
        public virtual DbSet<C__EFMigrationsHistory> C__EFMigrationsHistory { get; set; }
        public virtual DbSet<DbId> DbId { get; set; }
        public virtual DbSet<sysdiagrams> sysdiagrams { get; set; }
        public virtual DbSet<UKKullanicilar> UKKullanicilar { get; set; }
        public virtual DbSet<UKModuller> UKModuller { get; set; }
        public virtual DbSet<DegisimDonem> DegisimDonem { get; set; }
        public virtual DbSet<DegisimKayit> DegisimKayit { get; set; }
        public virtual DbSet<DegisimKayitDonemleri> DegisimKayitDonemleri { get; set; }
        public virtual DbSet<BelgeDogrulama> BelgeDogrulama { get; set; }
        public virtual DbSet<BolumBilgi> BolumBilgi { get; set; }
        public virtual DbSet<BolumYeterlilik> BolumYeterlilik { get; set; }
        public virtual DbSet<BolumYeterlilikKategoriIliski> BolumYeterlilikKategoriIliski { get; set; }
        public virtual DbSet<DersAktsTnm> DersAktsTnm { get; set; }
        public virtual DbSet<DersAktsYukIliski> DersAktsYukIliski { get; set; }
        public virtual DbSet<DersAlaniIliski> DersAlaniIliski { get; set; }
        public virtual DbSet<DersAlanTnm> DersAlanTnm { get; set; }
        public virtual DbSet<DersBilgi> DersBilgi { get; set; }
        public virtual DbSet<DersCikti> DersCikti { get; set; }
        public virtual DbSet<DersCiktiYontem> DersCiktiYontem { get; set; }
        public virtual DbSet<DersCiktiYontemIliski> DersCiktiYontemIliski { get; set; }
        public virtual DbSet<DersHaftaIliski> DersHaftaIliski { get; set; }
        public virtual DbSet<DersKategoriIliski> DersKategoriIliski { get; set; }
        public virtual DbSet<DersKategoriTnm> DersKategoriTnm { get; set; }
        public virtual DbSet<DersKonu> DersKonu { get; set; }
        public virtual DbSet<DersKoordinator> DersKoordinator { get; set; }
        public virtual DbSet<DersPlanAnaEtiket> DersPlanAnaEtiket { get; set; }
        public virtual DbSet<DersPlanEtiket> DersPlanEtiket { get; set; }
        public virtual DbSet<DersYeterlilikIliski> DersYeterlilikIliski { get; set; }
        public virtual DbSet<Dil> Dil { get; set; }
        public virtual DbSet<DilBolumBilgi> DilBolumBilgi { get; set; }
        public virtual DbSet<DilBolumYeterlilik> DilBolumYeterlilik { get; set; }
        public virtual DbSet<DilDersAktsTnm> DilDersAktsTnm { get; set; }
        public virtual DbSet<DilDersAlanTnm> DilDersAlanTnm { get; set; }
        public virtual DbSet<DilDersBilgi> DilDersBilgi { get; set; }
        public virtual DbSet<DilDersCikti> DilDersCikti { get; set; }
        public virtual DbSet<DilDersCiktiYontem> DilDersCiktiYontem { get; set; }
        public virtual DbSet<DilDersKategoriTnm> DilDersKategoriTnm { get; set; }
        public virtual DbSet<DilDersKonu> DilDersKonu { get; set; }
        public virtual DbSet<DilDuzeyYeterlilik> DilDuzeyYeterlilik { get; set; }
        public virtual DbSet<DilTemelAlan> DilTemelAlan { get; set; }
        public virtual DbSet<DilTemelAlanYeterlilik> DilTemelAlanYeterlilik { get; set; }
        public virtual DbSet<DilYeterlilikAltKategori> DilYeterlilikAltKategori { get; set; }
        public virtual DbSet<DilYeterlilikAnaKategori> DilYeterlilikAnaKategori { get; set; }
        public virtual DbSet<DuzeyYeterlilik> DuzeyYeterlilik { get; set; }
        public virtual DbSet<SistemAyar> SistemAyar { get; set; }
        public virtual DbSet<TemelAlan> TemelAlan { get; set; }
        public virtual DbSet<TemelAlanBolumIliski> TemelAlanBolumIliski { get; set; }
        public virtual DbSet<TemelAlanYeterlilikKategoriIliski> TemelAlanYeterlilikKategoriIliski { get; set; }
        public virtual DbSet<WebAlanDersIliski> WebAlanDersIliski { get; set; }
        public virtual DbSet<WebAlanTipTnm> WebAlanTipTnm { get; set; }
        public virtual DbSet<WebBolumAmacHedef> WebBolumAmacHedef { get; set; }
        public virtual DbSet<WebBolumDuzeyYeterlilik> WebBolumDuzeyYeterlilik { get; set; }
        public virtual DbSet<WebBolumKategoriIliski> WebBolumKategoriIliski { get; set; }
        public virtual DbSet<WebBolumKategoriTnm> WebBolumKategoriTnm { get; set; }
        public virtual DbSet<WebBolumTemelAlan> WebBolumTemelAlan { get; set; }
        public virtual DbSet<WebBolumTemelAlanIliski> WebBolumTemelAlanIliski { get; set; }
        public virtual DbSet<WebBolumYeterlilik> WebBolumYeterlilik { get; set; }
        public virtual DbSet<WebDersAktsTnm> WebDersAktsTnm { get; set; }
        public virtual DbSet<WebDersAktsYuk> WebDersAktsYuk { get; set; }
        public virtual DbSet<WebDersCiktilari> WebDersCiktilari { get; set; }
        public virtual DbSet<WebDersCiktiYontem> WebDersCiktiYontem { get; set; }
        public virtual DbSet<WebDersCiktiYontemIliski> WebDersCiktiYontemIliski { get; set; }
        public virtual DbSet<WebDersKategoriA> WebDersKategoriA { get; set; }
        public virtual DbSet<WebDersKategoriB> WebDersKategoriB { get; set; }
        public virtual DbSet<WebDersKategoriTnm> WebDersKategoriTnm { get; set; }
        public virtual DbSet<WebDersPaylarTnm> WebDersPaylarTnm { get; set; }
        public virtual DbSet<WebDersSosyalYeterlilikIliski> WebDersSosyalYeterlilikIliski { get; set; }
        public virtual DbSet<WebDersTemel> WebDersTemel { get; set; }
        public virtual DbSet<WebDersTipHocalar> WebDersTipHocalar { get; set; }
        public virtual DbSet<WebDersTipTnm> WebDersTipTnm { get; set; }
        public virtual DbSet<WebDersYerTipTnm> WebDersYerTipTnm { get; set; }
        public virtual DbSet<WebDersYeterlilikIliski> WebDersYeterlilikIliski { get; set; }
        public virtual DbSet<WebDuzeyYeterlilikler> WebDuzeyYeterlilikler { get; set; }
        public virtual DbSet<WebHavuzDersPlan> WebHavuzDersPlan { get; set; }
        public virtual DbSet<WebHavuzDersPlanAna> WebHavuzDersPlanAna { get; set; }
        public virtual DbSet<WebOrtalamaTipTnm> WebOrtalamaTipTnm { get; set; }
        public virtual DbSet<WebSistemAyar> WebSistemAyar { get; set; }
        public virtual DbSet<WebTipKurulDersler> WebTipKurulDersler { get; set; }
        public virtual DbSet<WebYetkiGrup> WebYetkiGrup { get; set; }
        public virtual DbSet<WebYetkililer> WebYetkililer { get; set; }
        public virtual DbSet<WebZorunluTipTnm> WebZorunluTipTnm { get; set; }
        public virtual DbSet<YazOkuluDersler> YazOkuluDersler { get; set; }
        public virtual DbSet<YeterlilikAltKategori> YeterlilikAltKategori { get; set; }
        public virtual DbSet<YeterlilikAnaKategori> YeterlilikAnaKategori { get; set; }
        public virtual DbSet<Yetkililer> Yetkililer { get; set; }
        public virtual DbSet<AdresBilgisi> AdresBilgisi { get; set; }
        public virtual DbSet<BirimKurulusDayanagi> BirimKurulusDayanagi { get; set; }
        public virtual DbSet<Birimler> Birimler { get; set; }
        public virtual DbSet<IBANBilgisi> IBANBilgisi { get; set; }
        public virtual DbSet<Kisi> Kisi { get; set; }
        public virtual DbSet<mekanBinalar> mekanBinalar { get; set; }
        public virtual DbSet<Nufus> Nufus { get; set; }
        public virtual DbSet<osymSiralama> osymSiralama { get; set; }
        public virtual DbSet<osymUniversite> osymUniversite { get; set; }
        public virtual DbSet<osymYoksis> osymYoksis { get; set; }
        public virtual DbSet<RaporTasarim> RaporTasarim { get; set; }
        public virtual DbSet<Sayilar> Sayilar { get; set; }
        public virtual DbSet<TelefonBilgisi> TelefonBilgisi { get; set; }
        public virtual DbSet<TNMIl> TNMIl { get; set; }
        public virtual DbSet<TNMIlce> TNMIlce { get; set; }
        public virtual DbSet<TNMUlke> TNMUlke { get; set; }
        public virtual DbSet<YokBirimler> YokBirimler { get; set; }
        public virtual DbSet<YoksisUlkeler> YoksisUlkeler { get; set; }
        public virtual DbSet<MekanCihazV2> MekanCihazV2 { get; set; }
        public virtual DbSet<MekanFotoV2> MekanFotoV2 { get; set; }
        public virtual DbSet<MekanKisiV2> MekanKisiV2 { get; set; }
        public virtual DbSet<MekanV2> MekanV2 { get; set; }
        public virtual DbSet<TNMMekanCihazV2> TNMMekanCihazV2 { get; set; }
        public virtual DbSet<TNMMekanTipV2> TNMMekanTipV2 { get; set; }
        public virtual DbSet<MernisSorguLog> MernisSorguLog { get; set; }
        public virtual DbSet<MernisUygulamaYetki> MernisUygulamaYetki { get; set; }
        public virtual DbSet<MernisUygulamaYetkiIP> MernisUygulamaYetkiIP { get; set; }
        public virtual DbSet<BarinmaKisiler> BarinmaKisiler { get; set; }
        public virtual DbSet<Mekan> Mekan { get; set; }
        public virtual DbSet<MekanBinalar1> MekanBinalar1 { get; set; }
        public virtual DbSet<MekanCihazlar> MekanCihazlar { get; set; }
        public virtual DbSet<MekanFoto> MekanFoto { get; set; }
        public virtual DbSet<MekanKisiler> MekanKisiler { get; set; }
        public virtual DbSet<TNMMekanIslevler> TNMMekanIslevler { get; set; }
        public virtual DbSet<TNMMekanKampusler> TNMMekanKampusler { get; set; }
        public virtual DbSet<TNMMekanTip> TNMMekanTip { get; set; }
        public virtual DbSet<Bildirim> Bildirim { get; set; }
        public virtual DbSet<Mesaj> Mesaj { get; set; }
        public virtual DbSet<FormasyonBasvuru> FormasyonBasvuru { get; set; }
        public virtual DbSet<OBSIliskiKesme> OBSIliskiKesme { get; set; }
        public virtual DbSet<OBSMezuniyetAnketi> OBSMezuniyetAnketi { get; set; }
        public virtual DbSet<OBSOnKayit> OBSOnKayit { get; set; }
        public virtual DbSet<OBSOnKayitAdres> OBSOnKayitAdres { get; set; }
        public virtual DbSet<OBSOnKayitAyar> OBSOnKayitAyar { get; set; }
        public virtual DbSet<OBSOnKayitAyarHazirlik> OBSOnKayitAyarHazirlik { get; set; }
        public virtual DbSet<OBSYazilmaAyar> OBSYazilmaAyar { get; set; }
        public virtual DbSet<OGRAkademikTakvim> OGRAkademikTakvim { get; set; }
        public virtual DbSet<OGRArsiv> OGRArsiv { get; set; }
        public virtual DbSet<OGRAskerlik> OGRAskerlik { get; set; }
        public virtual DbSet<OGRBarkodOkuma> OGRBarkodOkuma { get; set; }
        public virtual DbSet<OGRBelgeAyar> OGRBelgeAyar { get; set; }
        public virtual DbSet<OGRBelgeIsoNo> OGRBelgeIsoNo { get; set; }
        public virtual DbSet<OGRBelgeSayi> OGRBelgeSayi { get; set; }
        public virtual DbSet<OGRBelgeSayiOnur> OGRBelgeSayiOnur { get; set; }
        public virtual DbSet<OGRCapYandalTanim> OGRCapYandalTanim { get; set; }
        public virtual DbSet<OGRDanismanGorusme> OGRDanismanGorusme { get; set; }
        public virtual DbSet<OGRDanismanlik> OGRDanismanlik { get; set; }
        public virtual DbSet<OGRDanismanMesaj> OGRDanismanMesaj { get; set; }
        public virtual DbSet<OGRDegisim> OGRDegisim { get; set; }
        public virtual DbSet<OGRDersGrup> OGRDersGrup { get; set; }
        public virtual DbSet<OGRDersGrupHoca> OGRDersGrupHoca { get; set; }
        public virtual DbSet<OGRDersOnSart> OGRDersOnSart { get; set; }
        public virtual DbSet<OGRDersPlan> OGRDersPlan { get; set; }
        public virtual DbSet<OGRDersPlanAna> OGRDersPlanAna { get; set; }
        public virtual DbSet<OGRDersPlanDegisiklikAciklama> OGRDersPlanDegisiklikAciklama { get; set; }
        public virtual DbSet<OGRDersPlanFiltre> OGRDersPlanFiltre { get; set; }
        public virtual DbSet<OGRDersProgramiv2> OGRDersProgramiv2 { get; set; }
        public virtual DbSet<OGRDiplomaFormat> OGRDiplomaFormat { get; set; }
        public virtual DbSet<OGRDiplomaNo> OGRDiplomaNo { get; set; }
        public virtual DbSet<OGRDiplomaUnvan> OGRDiplomaUnvan { get; set; }
        public virtual DbSet<OGRDisiplinCeza> OGRDisiplinCeza { get; set; }
        public virtual DbSet<OGRDondurmaCeza> OGRDondurmaCeza { get; set; }
        public virtual DbSet<OGRDosya> OGRDosya { get; set; }
        public virtual DbSet<OGRDuplicateNo> OGRDuplicateNo { get; set; }
        public virtual DbSet<OGREgitimBilgileri> OGREgitimBilgileri { get; set; }
        public virtual DbSet<OGREnstituBasvuruKimlik> OGREnstituBasvuruKimlik { get; set; }
        public virtual DbSet<OGREnstituBasvuruTercih> OGREnstituBasvuruTercih { get; set; }
        public virtual DbSet<OGREnstituBilimselHazirlik> OGREnstituBilimselHazirlik { get; set; }
        public virtual DbSet<OGREnstituDanisman> OGREnstituDanisman { get; set; }
        public virtual DbSet<OGREnstituDonemAtlatma> OGREnstituDonemAtlatma { get; set; }
        public virtual DbSet<OGREnstituEkSure> OGREnstituEkSure { get; set; }
        public virtual DbSet<OGREnstituIngilizceHazirlik> OGREnstituIngilizceHazirlik { get; set; }
        public virtual DbSet<OGREnstituKimlik> OGREnstituKimlik { get; set; }
        public virtual DbSet<OGREnstituKomiteJuriUyeleri> OGREnstituKomiteJuriUyeleri { get; set; }
        public virtual DbSet<OGREnstituKomitelerJuriler> OGREnstituKomitelerJuriler { get; set; }
        public virtual DbSet<OGREnstituKontenjan> OGREnstituKontenjan { get; set; }
        public virtual DbSet<OGREnstituSeminer> OGREnstituSeminer { get; set; }
        public virtual DbSet<OGREnstituTezIzlemeKomitesi> OGREnstituTezIzlemeKomitesi { get; set; }
        public virtual DbSet<OGREnstituTezKonusu> OGREnstituTezKonusu { get; set; }
        public virtual DbSet<OGREnstituTezTizTosSinavi> OGREnstituTezTizTosSinavi { get; set; }
        public virtual DbSet<OGREnstituTIZ> OGREnstituTIZ { get; set; }
        public virtual DbSet<OGREnstituTOS> OGREnstituTOS { get; set; }
        public virtual DbSet<OGREnstituYayin> OGREnstituYayin { get; set; }
        public virtual DbSet<OGREnstituYeterlik> OGREnstituYeterlik { get; set; }
        public virtual DbSet<OGRGeciciOgrenciNumaralari> OGRGeciciOgrenciNumaralari { get; set; }
        public virtual DbSet<OGRGeribildirim> OGRGeribildirim { get; set; }
        public virtual DbSet<OGRHarcBankadanGelen> OGRHarcBankadanGelen { get; set; }
        public virtual DbSet<OGRHarcBankayaGiden> OGRHarcBankayaGiden { get; set; }
        public virtual DbSet<OGRHarcBasvuru> OGRHarcBasvuru { get; set; }
        public virtual DbSet<OGRHarcBilgi> OGRHarcBilgi { get; set; }
        public virtual DbSet<OGRHarcDersUcreti> OGRHarcDersUcreti { get; set; }
        public virtual DbSet<OGRHarcEskiBorc> OGRHarcEskiBorc { get; set; }
        public virtual DbSet<OGRHarcEskiBorcSilinen> OGRHarcEskiBorcSilinen { get; set; }
        public virtual DbSet<OGRHarcFaizOran> OGRHarcFaizOran { get; set; }
        public virtual DbSet<OGRHarcHesapAciklama> OGRHarcHesapAciklama { get; set; }
        public virtual DbSet<OGRHarcIade> OGRHarcIade { get; set; }
        public virtual DbSet<OGRHarcKrediUcretlendirmesiBorc> OGRHarcKrediUcretlendirmesiBorc { get; set; }
        public virtual DbSet<OGRHarcKrediUcretlendirmesiIade> OGRHarcKrediUcretlendirmesiIade { get; set; }
        public virtual DbSet<OGRHarcKrediUcretlendirmesiOdeme> OGRHarcKrediUcretlendirmesiOdeme { get; set; }
        public virtual DbSet<OGRHarcKrediUcretlendirmesiOdeyecek> OGRHarcKrediUcretlendirmesiOdeyecek { get; set; }
        public virtual DbSet<OGRHarcMiktar> OGRHarcMiktar { get; set; }
        public virtual DbSet<OGRHarcOdeme> OGRHarcOdeme { get; set; }
        public virtual DbSet<OGRHarcSonOdemeTarih> OGRHarcSonOdemeTarih { get; set; }
        public virtual DbSet<OGRHarcYuzdeOn> OGRHarcYuzdeOn { get; set; }
        public virtual DbSet<OGRHazirlik> OGRHazirlik { get; set; }
        public virtual DbSet<OGRHazirlikNotAralik> OGRHazirlikNotAralik { get; set; }
        public virtual DbSet<OGRIntibak> OGRIntibak { get; set; }
        public virtual DbSet<OGRIntibakBirim> OGRIntibakBirim { get; set; }
        public virtual DbSet<OGRIntibakDers> OGRIntibakDers { get; set; }
        public virtual DbSet<OGRKayitBelgeleri> OGRKayitBelgeleri { get; set; }
        public virtual DbSet<OGRKayitDondurma> OGRKayitDondurma { get; set; }
        public virtual DbSet<OGRKimlik> OGRKimlik { get; set; }
        public virtual DbSet<OGRKimlikDetay> OGRKimlikDetay { get; set; }
        public virtual DbSet<OGRKimlikFarkliGelis> OGRKimlikFarkliGelis { get; set; }
        public virtual DbSet<OGRKurulKarar> OGRKurulKarar { get; set; }
        public virtual DbSet<OGRKYKBurs> OGRKYKBurs { get; set; }
        public virtual DbSet<OGRKYKBursAktarma> OGRKYKBursAktarma { get; set; }
        public virtual DbSet<OGRKYKOgrenim> OGRKYKOgrenim { get; set; }
        public virtual DbSet<OGRKYKOgrenimAktarma> OGRKYKOgrenimAktarma { get; set; }
        public virtual DbSet<OGRMazeret> OGRMazeret { get; set; }
        public virtual DbSet<OGRMezunBilgi> OGRMezunBilgi { get; set; }
        public virtual DbSet<OGRNotAktarimLog> OGRNotAktarimLog { get; set; }
        public virtual DbSet<OGROgrenciDersPlan> OGROgrenciDersPlan { get; set; }
        public virtual DbSet<OGROgrenciEtiket> OGROgrenciEtiket { get; set; }
        public virtual DbSet<OGROgrenciEtiketIslem> OGROgrenciEtiketIslem { get; set; }
        public virtual DbSet<OGROgrenciGecmis> OGROgrenciGecmis { get; set; }
        public virtual DbSet<OGROgrenciHatirlatma> OGROgrenciHatirlatma { get; set; }
        public virtual DbSet<OGROgrenciNot> OGROgrenciNot { get; set; }
        public virtual DbSet<OGROgrenciOrtalama> OGROgrenciOrtalama { get; set; }
        public virtual DbSet<OGROgrenciSorun> OGROgrenciSorun { get; set; }
        public virtual DbSet<OGROgrenciTemsilci> OGROgrenciTemsilci { get; set; }
        public virtual DbSet<OGROgrenciYazilma> OGROgrenciYazilma { get; set; }
        public virtual DbSet<OgrOgrenciYonetmelik> OgrOgrenciYonetmelik { get; set; }
        public virtual DbSet<OGROSYMGecici> OGROSYMGecici { get; set; }
        public virtual DbSet<OGRRaporTanim> OGRRaporTanim { get; set; }
        public virtual DbSet<OGRSaglik> OGRSaglik { get; set; }
        public virtual DbSet<OGRSakatlikBilgi> OGRSakatlikBilgi { get; set; }
        public virtual DbSet<OGRStaj> OGRStaj { get; set; }
        public virtual DbSet<OGRTekDers> OGRTekDers { get; set; }
        public virtual DbSet<OGRTnmAFKanunlari> OGRTnmAFKanunlari { get; set; }
        public virtual DbSet<OGRTNMBelge> OGRTNMBelge { get; set; }
        public virtual DbSet<OGRTNMBilimDali> OGRTNMBilimDali { get; set; }
        public virtual DbSet<OGRTNMBolumYilaGore> OGRTNMBolumYilaGore { get; set; }
        public virtual DbSet<OGRTNMDersEtiket> OGRTNMDersEtiket { get; set; }
        public virtual DbSet<OGRTNMDonemBilgi> OGRTNMDonemBilgi { get; set; }
        public virtual DbSet<OGRTNMEskiRektor> OGRTNMEskiRektor { get; set; }
        public virtual DbSet<OGRTNMForm> OGRTNMForm { get; set; }
        public virtual DbSet<OGRTNMGenel> OGRTNMGenel { get; set; }
        public virtual DbSet<OGRTNMOgrenciEtiket> OGRTNMOgrenciEtiket { get; set; }
        public virtual DbSet<OGRTNMOkulOSYMKod> OGRTNMOkulOSYMKod { get; set; }
        public virtual DbSet<OGRTNMOrtaOgretimKurumlari> OGRTNMOrtaOgretimKurumlari { get; set; }
        public virtual DbSet<OGRTNMSaat> OGRTNMSaat { get; set; }
        public virtual DbSet<OGRTNMSaatBilgi> OGRTNMSaatBilgi { get; set; }
        public virtual DbSet<OGRTNMUniversite> OGRTNMUniversite { get; set; }
        public virtual DbSet<OGRTNMYonetmelik> OGRTNMYonetmelik { get; set; }
        public virtual DbSet<OGRTNMYuzlukKarsilik> OGRTNMYuzlukKarsilik { get; set; }
        public virtual DbSet<OGRUniversiteDisiGorevli> OGRUniversiteDisiGorevli { get; set; }
        public virtual DbSet<OGRYazilmaDanismanOnay> OGRYazilmaDanismanOnay { get; set; }
        public virtual DbSet<OSYMSonuc> OSYMSonuc { get; set; }
        public virtual DbSet<SinavGorevli> SinavGorevli { get; set; }
        public virtual DbSet<SinavMekan> SinavMekan { get; set; }
        public virtual DbSet<SinavTakvim> SinavTakvim { get; set; }
        public virtual DbSet<TNMEnstituKontenjanAdi> TNMEnstituKontenjanAdi { get; set; }
        public virtual DbSet<TNMSporBranslari> TNMSporBranslari { get; set; }
        public virtual DbSet<TNMSporOzGecmisPuan> TNMSporOzGecmisPuan { get; set; }
        public virtual DbSet<TNMSporYilPuan> TNMSporYilPuan { get; set; }
        public virtual DbSet<YOKAktarim> YOKAktarim { get; set; }
        public virtual DbSet<YOKBildirilenOgrenciler> YOKBildirilenOgrenciler { get; set; }
        public virtual DbSet<YoksisAktarimDurumu> YoksisAktarimDurumu { get; set; }
        public virtual DbSet<YoksisAktarimRaporu> YoksisAktarimRaporu { get; set; }
        public virtual DbSet<YoksisAktarimTalep> YoksisAktarimTalep { get; set; }
        public virtual DbSet<YOKTeyit> YOKTeyit { get; set; }
        public virtual DbSet<OgretimUyeSayilar> OgretimUyeSayilar { get; set; }
        public virtual DbSet<OsymApiSorguLog> OsymApiSorguLog { get; set; }
        public virtual DbSet<OsymApiUygulamaYetki> OsymApiUygulamaYetki { get; set; }
        public virtual DbSet<OsymApiUygulamaYetkiIP> OsymApiUygulamaYetkiIP { get; set; }
        public virtual DbSet<OsymBasari> OsymBasari { get; set; }
        public virtual DbSet<OsymKodlar1> OsymKodlar1 { get; set; }
        public virtual DbSet<OsymOgrenciBasari> OsymOgrenciBasari { get; set; }
        public virtual DbSet<UniversiteKodlari> UniversiteKodlari { get; set; }
        public virtual DbSet<AsaletTasdiki> AsaletTasdiki { get; set; }
        public virtual DbSet<AsaletTasdikiDetay> AsaletTasdikiDetay { get; set; }
        public virtual DbSet<ASVPersonel> ASVPersonel { get; set; }
        public virtual DbSet<ASVPersonelLog> ASVPersonelLog { get; set; }
        public virtual DbSet<AtanmaSureleri> AtanmaSureleri { get; set; }
        public virtual DbSet<AtanmaSureleriDetay> AtanmaSureleriDetay { get; set; }
        public virtual DbSet<BILAkademik> BILAkademik { get; set; }
        public virtual DbSet<BILAkademikAtiflar> BILAkademikAtiflar { get; set; }
        public virtual DbSet<BYOK> BYOK { get; set; }
        public virtual DbSet<DereceIlerletme> DereceIlerletme { get; set; }
        public virtual DbSet<DereceIlerletmeDetay> DereceIlerletmeDetay { get; set; }
        public virtual DbSet<DisGorevlendirme> DisGorevlendirme { get; set; }
        public virtual DbSet<Docentlik> Docentlik { get; set; }
        public virtual DbSet<EgitimBilgisi> EgitimBilgisi { get; set; }
        public virtual DbSet<FiiliGorevYeri> FiiliGorevYeri { get; set; }
        public virtual DbSet<Hareket> Hareket { get; set; }
        public virtual DbSet<IdariGorev> IdariGorev { get; set; }
        public virtual DbSet<Izin> Izin { get; set; }
        public virtual DbSet<IzinKalan> IzinKalan { get; set; }
        public virtual DbSet<IzinSakli> IzinSakli { get; set; }
        public virtual DbSet<IzinSakliDetay> IzinSakliDetay { get; set; }
        public virtual DbSet<IzinSakliToplam> IzinSakliToplam { get; set; }
        public virtual DbSet<IzinYOK> IzinYOK { get; set; }
        public virtual DbSet<IzinYOKDetay> IzinYOKDetay { get; set; }
        public virtual DbSet<IzinYOKDoluDetay> IzinYOKDoluDetay { get; set; }
        public virtual DbSet<KadroBilgisi> KadroBilgisi { get; set; }
        public virtual DbSet<KaraListe> KaraListe { get; set; }
        public virtual DbSet<KurumDisiGorevlendirme> KurumDisiGorevlendirme { get; set; }
        public virtual DbSet<LojmanBilgisi> LojmanBilgisi { get; set; }
        public virtual DbSet<Madde37> Madde37 { get; set; }
        public virtual DbSet<Madde38> Madde38 { get; set; }
        public virtual DbSet<Madde39> Madde39 { get; set; }
        public virtual DbSet<Menuler> Menuler { get; set; }
        public virtual DbSet<OgrenimBilgisi> OgrenimBilgisi { get; set; }
        public virtual DbSet<PersonelBilgisi> PersonelBilgisi { get; set; }
        public virtual DbSet<Rapor> Rapor { get; set; }
        public virtual DbSet<SBTanahtarSozcukler> SBTanahtarSozcukler { get; set; }
        public virtual DbSet<SBTAtanmaSekli> SBTAtanmaSekli { get; set; }
        public virtual DbSet<SBTbilimAlanlari> SBTbilimAlanlari { get; set; }
        public virtual DbSet<SBTBirimTuru> SBTBirimTuru { get; set; }
        public virtual DbSet<SBTDereceSinirlari> SBTDereceSinirlari { get; set; }
        public virtual DbSet<SBTOgrenimTurleri> SBTOgrenimTurleri { get; set; }
        public virtual DbSet<SBTtemelAlanlar> SBTtemelAlanlar { get; set; }
        public virtual DbSet<SBTuyelik> SBTuyelik { get; set; }
        public virtual DbSet<Sendika> Sendika { get; set; }
        public virtual DbSet<SGKHizmetAcikSure> SGKHizmetAcikSure { get; set; }
        public virtual DbSet<SGKHizmetAskerlik> SGKHizmetAskerlik { get; set; }
        public virtual DbSet<SGKHizmetBirlestirme> SGKHizmetBirlestirme { get; set; }
        public virtual DbSet<SGKHizmetBorclanma> SGKHizmetBorclanma { get; set; }
        public virtual DbSet<SGKHizmetCetveli> SGKHizmetCetveli { get; set; }
        public virtual DbSet<SGKHizmetIHS> SGKHizmetIHS { get; set; }
        public virtual DbSet<SGKHizmetKurs> SGKHizmetKurs { get; set; }
        public virtual DbSet<SGKHizmetMahkeme> SGKHizmetMahkeme { get; set; }
        public virtual DbSet<SGKHizmetNufus> SGKHizmetNufus { get; set; }
        public virtual DbSet<SGKHizmetOkul> SGKHizmetOkul { get; set; }
        public virtual DbSet<SGKHizmetTazminat> SGKHizmetTazminat { get; set; }
        public virtual DbSet<SGKHizmetUnvan> SGKHizmetUnvan { get; set; }
        public virtual DbSet<TerfiBilgisi> TerfiBilgisi { get; set; }
        public virtual DbSet<TerfiBilgisiDetay> TerfiBilgisiDetay { get; set; }
        public virtual DbSet<TerfiProfesorOdenek> TerfiProfesorOdenek { get; set; }
        public virtual DbSet<TNMAciklama> TNMAciklama { get; set; }
        public virtual DbSet<TNMAnaBilimDali> TNMAnaBilimDali { get; set; }
        public virtual DbSet<TNMAyrilma> TNMAyrilma { get; set; }
        public virtual DbSet<TNMBolum> TNMBolum { get; set; }
        public virtual DbSet<TNMDayanak> TNMDayanak { get; set; }
        public virtual DbSet<TNMDisKurum> TNMDisKurum { get; set; }
        public virtual DbSet<TNMEgitimBilgisi> TNMEgitimBilgisi { get; set; }
        public virtual DbSet<TNMEgitimKonusu> TNMEgitimKonusu { get; set; }
        public virtual DbSet<TNMEgitimYeri> TNMEgitimYeri { get; set; }
        public virtual DbSet<TNMEkGosterge> TNMEkGosterge { get; set; }
        public virtual DbSet<TNMFakulte> TNMFakulte { get; set; }
        public virtual DbSet<TNMGorevTazminati> TNMGorevTazminati { get; set; }
        public virtual DbSet<TNMGorevUnvani> TNMGorevUnvani { get; set; }
        public virtual DbSet<TNMIdariGorev> TNMIdariGorev { get; set; }
        public virtual DbSet<TNMIdariOdenekler> TNMIdariOdenekler { get; set; }
        public virtual DbSet<TNMKadroUnvani> TNMKadroUnvani { get; set; }
        public virtual DbSet<TNMKanunMaddeNo> TNMKanunMaddeNo { get; set; }
        public virtual DbSet<TNMKullanicilar> TNMKullanicilar { get; set; }
        public virtual DbSet<TNMKurumBilgisi> TNMKurumBilgisi { get; set; }
        public virtual DbSet<TNMLojman> TNMLojman { get; set; }
        public virtual DbSet<TNMMakamTazminati> TNMMakamTazminati { get; set; }
        public virtual DbSet<TNMOgrenimYerleri> TNMOgrenimYerleri { get; set; }
        public virtual DbSet<TNMSendika> TNMSendika { get; set; }
        public virtual DbSet<TNMSistemAyar> TNMSistemAyar { get; set; }
        public virtual DbSet<TNMUniversite> TNMUniversite { get; set; }
        public virtual DbSet<TNMUniversiteOdenegi> TNMUniversiteOdenegi { get; set; }
        public virtual DbSet<TNMUnvan> TNMUnvan { get; set; }
        public virtual DbSet<TNMYetkiGrup> TNMYetkiGrup { get; set; }
        public virtual DbSet<UAKDocentlik> UAKDocentlik { get; set; }
        public virtual DbSet<UAKtemelAlan> UAKtemelAlan { get; set; }
        public virtual DbSet<uyelik> uyelik { get; set; }
        public virtual DbSet<YabanciDil> YabanciDil { get; set; }
        public virtual DbSet<YetkiKullaniciMenu> YetkiKullaniciMenu { get; set; }
        public virtual DbSet<YetkiKullaniciRol> YetkiKullaniciRol { get; set; }
        public virtual DbSet<YetkiRolMenu> YetkiRolMenu { get; set; }
        public virtual DbSet<YetkiRolTanim> YetkiRolTanim { get; set; }
        public virtual DbSet<Zimmetler> Zimmetler { get; set; }
        public virtual DbSet<RandevuAyar> RandevuAyar { get; set; }
        public virtual DbSet<RandevuKayit> RandevuKayit { get; set; }
        public virtual DbSet<RandevuOturum> RandevuOturum { get; set; }
        public virtual DbSet<RaporTanim> RaporTanim { get; set; }
        public virtual DbSet<RaporTasarim1> RaporTasarim1 { get; set; }
        public virtual DbSet<YokApiSorguLog> YokApiSorguLog { get; set; }
        public virtual DbSet<YokApiUygulamaYetki> YokApiUygulamaYetki { get; set; }
        public virtual DbSet<YokApiUygulamaYetkiIP> YokApiUygulamaYetkiIP { get; set; }
        public virtual DbSet<EPostaOnay> EPostaOnay { get; set; }
        public virtual DbSet<Grup> Grup { get; set; }
        public virtual DbSet<GrupKullanici> GrupKullanici { get; set; }
        public virtual DbSet<HesapYetki> HesapYetki { get; set; }
        public virtual DbSet<IslemLog> IslemLog { get; set; }
        public virtual DbSet<Kullanici> Kullanici { get; set; }
        public virtual DbSet<KullaniciAlias> KullaniciAlias { get; set; }
        public virtual DbSet<KullaniciCihaz> KullaniciCihaz { get; set; }
        public virtual DbSet<KullaniciDetay> KullaniciDetay { get; set; }
        public virtual DbSet<KullaniciGuvenlik> KullaniciGuvenlik { get; set; }
        public virtual DbSet<KullaniciGuvenlik2> KullaniciGuvenlik2 { get; set; }
        public virtual DbSet<KullaniciHesapAyar> KullaniciHesapAyar { get; set; }
        public virtual DbSet<KullaniciTicket> KullaniciTicket { get; set; }
        public virtual DbSet<KullaniciToken> KullaniciToken { get; set; }
        public virtual DbSet<KullaniciTokenHistory> KullaniciTokenHistory { get; set; }
        public virtual DbSet<KullaniciTOTP> KullaniciTOTP { get; set; }
        public virtual DbSet<LoginGuvenlik> LoginGuvenlik { get; set; }
        public virtual DbSet<LoginYetkiliIP> LoginYetkiliIP { get; set; }
        public virtual DbSet<Modul> Modul { get; set; }
        public virtual DbSet<ModulIPGuvenlik> ModulIPGuvenlik { get; set; }
        public virtual DbSet<ModulRol> ModulRol { get; set; }
        public virtual DbSet<Proje> Proje { get; set; }
        public virtual DbSet<Rol> Rol { get; set; }
        public virtual DbSet<RolGrup> RolGrup { get; set; }
        public virtual DbSet<RolKullanici> RolKullanici { get; set; }
        public virtual DbSet<SifreDogrulamaServisLog> SifreDogrulamaServisLog { get; set; }
        public virtual DbSet<SifreDogrulamaServisYetki> SifreDogrulamaServisYetki { get; set; }
        public virtual DbSet<SifreSifirlaAktivasyon> SifreSifirlaAktivasyon { get; set; }
        public virtual DbSet<TNMDomain> TNMDomain { get; set; }
        public virtual DbSet<YetkiLog> YetkiLog { get; set; }
        public virtual DbSet<ABSDokumanGrup> ABSDokumanGrup { get; set; }
        public virtual DbSet<ABSGeriBildirim> ABSGeriBildirim { get; set; }
        public virtual DbSet<ABSOrtakDegerlendirme> ABSOrtakDegerlendirme { get; set; }
        public virtual DbSet<ABSOrtakDegerlendirmeGrup> ABSOrtakDegerlendirmeGrup { get; set; }
        public virtual DbSet<ABSProjeKonu> ABSProjeKonu { get; set; }
        public virtual DbSet<ABSProjeOgrenci> ABSProjeOgrenci { get; set; }
        public virtual DbSet<ABSProjeRapor> ABSProjeRapor { get; set; }
        public virtual DbSet<ABSProjeTakvim> ABSProjeTakvim { get; set; }
        public virtual DbSet<ABSTestCevapAnahtari> ABSTestCevapAnahtari { get; set; }
        public virtual DbSet<ABSYoklamaOtomatik> ABSYoklamaOtomatik { get; set; }
        public virtual DbSet<ABSYoklamaOtomatikDersProgram> ABSYoklamaOtomatikDersProgram { get; set; }
        public virtual DbSet<AKRGenelOgrenmeCiktilari> AKRGenelOgrenmeCiktilari { get; set; }
        public virtual DbSet<AKRGenelProgramCiktilari> AKRGenelProgramCiktilari { get; set; }
        public virtual DbSet<OOdev> OOdev { get; set; }
        public virtual DbSet<OOdevGrup> OOdevGrup { get; set; }
        public virtual DbSet<OOdevOgrenci> OOdevOgrenci { get; set; }
        public virtual DbSet<OSinavCevap> OSinavCevap { get; set; }
        public virtual DbSet<OSinavDosya> OSinavDosya { get; set; }
        public virtual DbSet<OSinavOgrenci> OSinavOgrenci { get; set; }
        public virtual DbSet<OSinavOturum> OSinavOturum { get; set; }
        public virtual DbSet<OSinavSoru> OSinavSoru { get; set; }
        public virtual DbSet<OsinavSoruGrup> OsinavSoruGrup { get; set; }
        public virtual DbSet<PayRandevu> PayRandevu { get; set; }
        public virtual DbSet<SanalSinif> SanalSinif { get; set; }
        public virtual DbSet<SanalSinifGrup> SanalSinifGrup { get; set; }
        public virtual DbSet<SanalSinifOgrenci> SanalSinifOgrenci { get; set; }
        public virtual DbSet<SanalSinifProvider> SanalSinifProvider { get; set; }
        public virtual DbSet<AuditLogDetails> AuditLogDetails { get; set; }
        public virtual DbSet<AuditLogs> AuditLogs { get; set; }
        public virtual DbSet<WebBolumAlanYeterlilikler> WebBolumAlanYeterlilikler { get; set; }
        public virtual DbSet<WebYetkiGrupIliski> WebYetkiGrupIliski { get; set; }
        public virtual DbSet<WebYetkiler> WebYetkiler { get; set; }
        public virtual DbSet<BirimDil> BirimDil { get; set; }
        public virtual DbSet<MekanV2Yedek> MekanV2Yedek { get; set; }
        public virtual DbSet<AbsAnketAKTS> AbsAnketAKTS { get; set; }
        public virtual DbSet<AbsAnketDers> AbsAnketDers { get; set; }
        public virtual DbSet<AbsAnketDersSonuclari> AbsAnketDersSonuclari { get; set; }
        public virtual DbSet<AbsAnketOgretimSonuclari> AbsAnketOgretimSonuclari { get; set; }
        public virtual DbSet<AbsIsYukuSonuclari> AbsIsYukuSonuclari { get; set; }
        public virtual DbSet<OGRCapDersPlan> OGRCapDersPlan { get; set; }
        public virtual DbSet<OGRCapIslemIzin> OGRCapIslemIzin { get; set; }
        public virtual DbSet<OGRCapSecmeliTanim> OGRCapSecmeliTanim { get; set; }
        public virtual DbSet<OGREnstituYeterlilikKomite> OGREnstituYeterlilikKomite { get; set; }
        public virtual DbSet<OGROgrenciDersGrupTercih> OGROgrenciDersGrupTercih { get; set; }
        public virtual DbSet<OGROSYMOgrenciAktarim> OGROSYMOgrenciAktarim { get; set; }
        public virtual DbSet<OGROzelOgrenciExceldenTranskript> OGROzelOgrenciExceldenTranskript { get; set; }
        public virtual DbSet<OGRTersineIntibak> OGRTersineIntibak { get; set; }
        public virtual DbSet<BILAkademikYazarlar> BILAkademikYazarlar { get; set; }
        public virtual DbSet<KadroTakasi> KadroTakasi { get; set; }
        public virtual DbSet<SBTHizmetSebepTur> SBTHizmetSebepTur { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ABSBasariNot>()
                .Property(e => e.InKod)
                .HasPrecision(18, 0);

            modelBuilder.Entity<ABSBasariNot>()
                .HasMany(e => e.OGRHazirlik)
                .WithOptional(e => e.ABSBasariNot)
                .HasForeignKey(e => e.FKABSBasariNotID);

            modelBuilder.Entity<ABSBasariNot>()
                .HasMany(e => e.OGROgrenciNot)
                .WithOptional(e => e.ABSBasariNot)
                .HasForeignKey(e => e.FKABSBasariNot);

            modelBuilder.Entity<ABSDegerlendirme>()
                .HasMany(e => e.ABSBasariNot)
                .WithOptional(e => e.ABSDegerlendirme)
                .HasForeignKey(e => e.FKDegerlendirmeID);

            modelBuilder.Entity<ABSDegerlendirme>()
                .HasMany(e => e.ABSDegerlendirmeDosya)
                .WithRequired(e => e.ABSDegerlendirme)
                .HasForeignKey(e => e.FKDegerlendirmeID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ABSDegerlendirme>()
                .HasMany(e => e.ABSOgrenciNot)
                .WithOptional(e => e.ABSDegerlendirme)
                .HasForeignKey(e => e.FKDegerlendirmeID);

            modelBuilder.Entity<ABSDegerlendirme>()
                .HasMany(e => e.ABSYayinlananBasariNotlar)
                .WithOptional(e => e.ABSDegerlendirme)
                .HasForeignKey(e => e.FKABSDegerlendirme);

            modelBuilder.Entity<ABSDegerlendirme>()
                .HasMany(e => e.OGRBarkodOkuma)
                .WithOptional(e => e.ABSDegerlendirme)
                .HasForeignKey(e => e.FKABSDegerlendirme);

            modelBuilder.Entity<ABSDokuman>()
                .HasMany(e => e.ABSDokumanGrup)
                .WithRequired(e => e.ABSDokuman)
                .HasForeignKey(e => e.FKABSDokumanID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ABSGrupCalisma>()
                .Property(e => e.Konu)
                .IsUnicode(false);

            modelBuilder.Entity<ABSOgrenciDanismanlik>()
                .Property(e => e.kullanici)
                .IsUnicode(false);

            modelBuilder.Entity<ABSOgrenciNot>()
                .Property(e => e.InKod)
                .HasPrecision(18, 0);

            modelBuilder.Entity<ABSOgrenciNot>()
                .HasMany(e => e.ABSOgrenciNotDetay)
                .WithOptional(e => e.ABSOgrenciNot)
                .HasForeignKey(e => e.FKOgrenciNotID);

            modelBuilder.Entity<ABSPaylar>()
                .Property(e => e.InKod)
                .HasPrecision(18, 0);

            modelBuilder.Entity<ABSPaylar>()
                .HasMany(e => e.ABSOgrenciNot)
                .WithOptional(e => e.ABSPaylar)
                .HasForeignKey(e => e.FKAbsPaylarID);

            modelBuilder.Entity<ABSPaylar>()
                .HasMany(e => e.ABSPaylarOlcme)
                .WithOptional(e => e.ABSPaylar)
                .HasForeignKey(e => e.FKPaylarID);

            modelBuilder.Entity<ABSPaylar>()
                .HasMany(e => e.ABSYayinlananPayNotlar)
                .WithRequired(e => e.ABSPaylar)
                .HasForeignKey(e => e.FKABSPayID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ABSPaylar>()
                .HasMany(e => e.OSinavGrup)
                .WithRequired(e => e.ABSPaylar)
                .HasForeignKey(e => e.PayID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ABSPaylar>()
                .HasMany(e => e.PayRandevu)
                .WithRequired(e => e.ABSPaylar)
                .HasForeignKey(e => e.FKPayID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ABSPaylarOlcme>()
                .HasMany(e => e.ABSOgrenciNotDetay)
                .WithOptional(e => e.ABSPaylarOlcme)
                .HasForeignKey(e => e.FKAbsPaylarOlcmeID);

            modelBuilder.Entity<ABSPaylarOlcme>()
                .HasMany(e => e.ABSPaylarOlcmeOgrenme)
                .WithOptional(e => e.ABSPaylarOlcme)
                .HasForeignKey(e => e.FKPaylarOlcmeID);

            modelBuilder.Entity<ABSPaylarOlcme>()
                .HasMany(e => e.ABSPaylarOlcmeProgram)
                .WithOptional(e => e.ABSPaylarOlcme)
                .HasForeignKey(e => e.FKPaylarOlcmeID);

            modelBuilder.Entity<ABSPaylarOlcme>()
                .HasMany(e => e.OSinavSoru)
                .WithOptional(e => e.ABSPaylarOlcme)
                .HasForeignKey(e => e.OlcmeID);

            modelBuilder.Entity<ABSTNMHafta>()
                .HasMany(e => e.ABSDevamTakipKontrol)
                .WithRequired(e => e.ABSTNMHafta)
                .HasForeignKey(e => e.FKHaftaID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ABSYetkiliKullanici>()
                .HasMany(e => e.ABSYetki)
                .WithOptional(e => e.ABSYetkiliKullanici)
                .HasForeignKey(e => e.FKYetkiliPersonelID);

            modelBuilder.Entity<ABSYetkiModul>()
                .HasMany(e => e.ABSYetki)
                .WithRequired(e => e.ABSYetkiModul)
                .HasForeignKey(e => e.FKYetliliModulID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ABSYetkliRolleri>()
                .HasMany(e => e.ABSYetki)
                .WithOptional(e => e.ABSYetkliRolleri)
                .HasForeignKey(e => e.FKYetkiliRolID);

            modelBuilder.Entity<ABSYetkliRolleri>()
                .HasMany(e => e.ABSYetkiliKullanici)
                .WithOptional(e => e.ABSYetkliRolleri)
                .HasForeignKey(e => e.FKYetkiRolID);

            modelBuilder.Entity<DanismanlikBilgileri>()
                .HasOptional(e => e.DanismanlikBilgileri1)
                .WithRequired(e => e.DanismanlikBilgileri2);

            modelBuilder.Entity<OSinav>()
                .HasMany(e => e.OSinavGrup)
                .WithRequired(e => e.OSinav)
                .HasForeignKey(e => e.FKSinavID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OSinav>()
                .HasMany(e => e.OSinavDosya)
                .WithOptional(e => e.OSinav)
                .HasForeignKey(e => e.SinavID);

            modelBuilder.Entity<OSinav>()
                .HasMany(e => e.OSinavSoru)
                .WithRequired(e => e.OSinav)
                .HasForeignKey(e => e.SinavID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OSinav>()
                .HasMany(e => e.OSinavCevap)
                .WithRequired(e => e.OSinav)
                .HasForeignKey(e => e.SinavID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CapYandalKontenjan>()
                .HasMany(e => e.CapYandalTercih)
                .WithRequired(e => e.CapYandalKontenjan)
                .HasForeignKey(e => e.FKCapYandalKontenjanID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<GecisKontenjan>()
                .HasMany(e => e.GecisTercih)
                .WithRequired(e => e.GecisKontenjan)
                .HasForeignKey(e => e.FKGecisKontenjanID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<KitapBursu>()
                .HasMany(e => e.KitapBursuKardesBilgisi)
                .WithRequired(e => e.KitapBursu)
                .HasForeignKey(e => e.FKKitapBursuID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MuafiyetSinavBatchKey>()
                .HasMany(e => e.MuafiyetSinavTIN)
                .WithRequired(e => e.MuafiyetSinavBatchKey)
                .HasForeignKey(e => e.FKBatchKeyID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MuafiyetSinavOturum>()
                .HasMany(e => e.MuafiyetSinavBasvuru)
                .WithOptional(e => e.MuafiyetSinavOturum)
                .HasForeignKey(e => e.FKMuafiyetSinavOturumID);

            modelBuilder.Entity<MuafiyetSinavSalonTanim>()
                .HasMany(e => e.MuafiyetSinavOturum)
                .WithRequired(e => e.MuafiyetSinavSalonTanim)
                .HasForeignKey(e => e.FKMuafiyetSinavSalonTanimID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MuafiyetSinavTanim>()
                .HasMany(e => e.MuafiyetSinavBasvuru)
                .WithRequired(e => e.MuafiyetSinavTanim)
                .HasForeignKey(e => e.FKMuafiyetSinavTanimID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MuafiyetSinavTanim>()
                .HasMany(e => e.MuafiyetSinavOturum)
                .WithRequired(e => e.MuafiyetSinavTanim)
                .HasForeignKey(e => e.FKMuafiyetSinavTanimID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<YazOkuluOnKayit>()
                .HasMany(e => e.YazOkuluOnKayitDersler)
                .WithRequired(e => e.YazOkuluOnKayit)
                .HasForeignKey(e => e.FKYazOkuluOnKayitID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BankaKurumHesapNumaralari>()
                .HasMany(e => e.BankaWebServisHareketi)
                .WithRequired(e => e.BankaKurumHesapNumaralari)
                .HasForeignKey(e => e.FKHesapID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BankaWebServisHareketi>()
                .HasMany(e => e.BankaOdemeler)
                .WithRequired(e => e.BankaWebServisHareketi)
                .HasForeignKey(e => e.FKServisHareketID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OnlineHarcOdemeleri>()
                .HasMany(e => e.OnlineHarcBorclari)
                .WithOptional(e => e.OnlineHarcOdemeleri)
                .HasForeignKey(e => e.FKOdemeID);

            modelBuilder.Entity<OnlineHarcOdemeleri>()
                .HasMany(e => e.OGRGeciciOgrenciNumaralari)
                .WithOptional(e => e.OnlineHarcOdemeleri)
                .HasForeignKey(e => e.FKOdemeID);

            modelBuilder.Entity<OnlineHarcOdemeleri>()
                .HasMany(e => e.OGRHarcOdeme)
                .WithOptional(e => e.OnlineHarcOdemeleri)
                .HasForeignKey(e => e.FKOdemeID);

            modelBuilder.Entity<UKKullanicilar>()
                .Property(e => e.ogrFakKod)
                .IsUnicode(false);

            modelBuilder.Entity<UKKullanicilar>()
                .Property(e => e.ogrBolKod)
                .IsUnicode(false);

            modelBuilder.Entity<DegisimDonem>()
                .Property(e => e.DonemAd)
                .IsUnicode(false);

            modelBuilder.Entity<DegisimDonem>()
                .Property(e => e.DonemAdEn)
                .IsUnicode(false);

            modelBuilder.Entity<DegisimKayit>()
                .HasMany(e => e.DegisimKayitDonemleri)
                .WithRequired(e => e.DegisimKayit)
                .HasForeignKey(e => e.FKDegisimKayitID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BolumBilgi>()
                .HasMany(e => e.DilBolumBilgi)
                .WithOptional(e => e.BolumBilgi)
                .HasForeignKey(e => e.FKBolumBilgiID);

            modelBuilder.Entity<BolumYeterlilik>()
                .HasMany(e => e.ABSPaylarOlcmeProgram)
                .WithOptional(e => e.BolumYeterlilik)
                .HasForeignKey(e => e.FKProgramCiktiID);

            modelBuilder.Entity<BolumYeterlilik>()
                .HasMany(e => e.AKRGenelProgramCiktilari)
                .WithOptional(e => e.BolumYeterlilik)
                .HasForeignKey(e => e.FKProgramCiktiID);

            modelBuilder.Entity<BolumYeterlilik>()
                .HasMany(e => e.BolumYeterlilikKategoriIliski)
                .WithOptional(e => e.BolumYeterlilik)
                .HasForeignKey(e => e.FKYeterlilikID);

            modelBuilder.Entity<BolumYeterlilik>()
                .HasMany(e => e.DersYeterlilikIliski)
                .WithOptional(e => e.BolumYeterlilik)
                .HasForeignKey(e => e.FKYeterlilikID);

            modelBuilder.Entity<BolumYeterlilik>()
                .HasMany(e => e.DilBolumYeterlilik)
                .WithOptional(e => e.BolumYeterlilik)
                .HasForeignKey(e => e.FKBolumYeterlilikID);

            modelBuilder.Entity<DersAktsTnm>()
                .HasMany(e => e.DersAktsYukIliski)
                .WithOptional(e => e.DersAktsTnm)
                .HasForeignKey(e => e.FKAktsID);

            modelBuilder.Entity<DersAktsTnm>()
                .HasMany(e => e.DilDersAktsTnm)
                .WithOptional(e => e.DersAktsTnm)
                .HasForeignKey(e => e.FKDersAktsTnmID);

            modelBuilder.Entity<DersAktsYukIliski>()
                .Property(e => e.ID)
                .HasPrecision(18, 0);

            modelBuilder.Entity<DersAlanTnm>()
                .Property(e => e.Yil)
                .IsFixedLength();

            modelBuilder.Entity<DersAlanTnm>()
                .HasMany(e => e.DersAlaniIliski)
                .WithOptional(e => e.DersAlanTnm)
                .HasForeignKey(e => e.FKDersAlanID);

            modelBuilder.Entity<DersAlanTnm>()
                .HasMany(e => e.DilDersAlanTnm)
                .WithOptional(e => e.DersAlanTnm)
                .HasForeignKey(e => e.FKDersAlanTnmID);

            modelBuilder.Entity<DersBilgi>()
                .Property(e => e.Ekleyen)
                .IsUnicode(false);

            modelBuilder.Entity<DersBilgi>()
                .HasMany(e => e.DersKoordinator)
                .WithOptional(e => e.DersBilgi)
                .HasForeignKey(e => e.FKDersBilgiID);

            modelBuilder.Entity<DersCikti>()
                .HasMany(e => e.ABSPaylarOlcmeOgrenme)
                .WithOptional(e => e.DersCikti)
                .HasForeignKey(e => e.FKOgrenmeCiktiID);

            modelBuilder.Entity<DersCikti>()
                .HasMany(e => e.AKRGenelOgrenmeCiktilari)
                .WithRequired(e => e.DersCikti)
                .HasForeignKey(e => e.FKOgrenmeCiktiID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DersCikti>()
                .HasMany(e => e.DersCiktiYontemIliski)
                .WithOptional(e => e.DersCikti)
                .HasForeignKey(e => e.FKCiktiID);

            modelBuilder.Entity<DersCikti>()
                .HasMany(e => e.DilDersCikti)
                .WithOptional(e => e.DersCikti)
                .HasForeignKey(e => e.FKDersCiktiID);

            modelBuilder.Entity<DersCiktiYontem>()
                .HasMany(e => e.DersCiktiYontemIliski)
                .WithOptional(e => e.DersCiktiYontem)
                .HasForeignKey(e => e.FKCiktiYontemID);

            modelBuilder.Entity<DersCiktiYontem>()
                .HasMany(e => e.DilDersCiktiYontem)
                .WithOptional(e => e.DersCiktiYontem)
                .HasForeignKey(e => e.FKCiktiYontemID);

            modelBuilder.Entity<DersHaftaIliski>()
                .HasMany(e => e.DersKonu)
                .WithOptional(e => e.DersHaftaIliski)
                .HasForeignKey(e => e.FKDersHaftaIliskiID);

            modelBuilder.Entity<DersHaftaIliski>()
                .HasMany(e => e.OGRDersProgramiv2)
                .WithOptional(e => e.DersHaftaIliski)
                .HasForeignKey(e => e.FKDersHaftaIliskiID);

            modelBuilder.Entity<DersKategoriTnm>()
                .HasMany(e => e.DersKategoriIliski)
                .WithOptional(e => e.DersKategoriTnm)
                .HasForeignKey(e => e.FKDersKategoriID);

            modelBuilder.Entity<DersKategoriTnm>()
                .HasMany(e => e.DilDersKategoriTnm)
                .WithOptional(e => e.DersKategoriTnm)
                .HasForeignKey(e => e.FKDersKategoriTnmID);

            modelBuilder.Entity<DersKonu>()
                .HasMany(e => e.DilDersKonu)
                .WithOptional(e => e.DersKonu)
                .HasForeignKey(e => e.FKDersKonuID);

            modelBuilder.Entity<DersKonu>()
                .HasMany(e => e.OGRDersProgramiv2)
                .WithOptional(e => e.DersKonu)
                .HasForeignKey(e => e.FKDersKonuID);

            modelBuilder.Entity<DersKoordinator>()
                .Property(e => e.Ekleyen)
                .IsFixedLength();

            modelBuilder.Entity<DersPlanAnaEtiket>()
                .HasMany(e => e.DersPlanEtiket)
                .WithRequired(e => e.DersPlanAnaEtiket)
                .HasForeignKey(e => e.FKDersPlanAnaEtiketID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Dil>()
                .Property(e => e.Dil_Kod)
                .IsFixedLength();

            modelBuilder.Entity<Dil>()
                .HasMany(e => e.DersBilgi)
                .WithOptional(e => e.Dil)
                .HasForeignKey(e => e.FKDilID);

            modelBuilder.Entity<DilBolumBilgi>()
                .Property(e => e.Icerik)
                .IsUnicode(false);

            modelBuilder.Entity<DilBolumYeterlilik>()
                .Property(e => e.Icerik)
                .IsUnicode(false);

            modelBuilder.Entity<DilDersAktsTnm>()
                .Property(e => e.Icerik)
                .IsUnicode(false);

            modelBuilder.Entity<DilDersAlanTnm>()
                .Property(e => e.Icerik)
                .IsUnicode(false);

            modelBuilder.Entity<DilDersBilgi>()
                .Property(e => e.IcerikOnKosul)
                .IsUnicode(false);

            modelBuilder.Entity<DilDersBilgi>()
                .Property(e => e.IcerikOnerilenSecmeli)
                .IsUnicode(false);

            modelBuilder.Entity<DilDersBilgi>()
                .Property(e => e.IcerikDersYardimcilar)
                .IsUnicode(false);

            modelBuilder.Entity<DilDersBilgi>()
                .Property(e => e.IcerikDersAmac)
                .IsUnicode(false);

            modelBuilder.Entity<DilDersBilgi>()
                .Property(e => e.IcerikDers)
                .IsUnicode(false);

            modelBuilder.Entity<DilDersBilgi>()
                .Property(e => e.IcerikDersNot)
                .IsUnicode(false);

            modelBuilder.Entity<DilDersBilgi>()
                .Property(e => e.IcerikDersKaynak)
                .IsUnicode(false);

            modelBuilder.Entity<DilDersCikti>()
                .Property(e => e.Icerik)
                .IsUnicode(false);

            modelBuilder.Entity<DilDersCiktiYontem>()
                .Property(e => e.Icerik)
                .IsUnicode(false);

            modelBuilder.Entity<DilDersKategoriTnm>()
                .Property(e => e.Icerik)
                .IsUnicode(false);

            modelBuilder.Entity<DilDersKonu>()
                .Property(e => e.Ekleyen)
                .IsFixedLength();

            modelBuilder.Entity<DilDuzeyYeterlilik>()
                .Property(e => e.Icerik)
                .IsUnicode(false);

            modelBuilder.Entity<DilTemelAlan>()
                .Property(e => e.Icerik)
                .IsUnicode(false);

            modelBuilder.Entity<DilTemelAlanYeterlilik>()
                .Property(e => e.Icerik)
                .IsUnicode(false);

            modelBuilder.Entity<DilYeterlilikAltKategori>()
                .Property(e => e.Icerik)
                .IsUnicode(false);

            modelBuilder.Entity<DilYeterlilikAnaKategori>()
                .Property(e => e.Icerik)
                .IsUnicode(false);

            modelBuilder.Entity<DuzeyYeterlilik>()
                .HasMany(e => e.DilDuzeyYeterlilik)
                .WithOptional(e => e.DuzeyYeterlilik)
                .HasForeignKey(e => e.FKDuzeyYeterlilikID);

            modelBuilder.Entity<SistemAyar>()
                .Property(e => e.CevrimBasTarih)
                .IsUnicode(false);

            modelBuilder.Entity<SistemAyar>()
                .Property(e => e.CevrimBitTarih)
                .IsUnicode(false);

            modelBuilder.Entity<SistemAyar>()
                .Property(e => e.OzelYetkililer)
                .IsUnicode(false);

            modelBuilder.Entity<TemelAlan>()
                .Property(e => e.Kod)
                .IsUnicode(false);

            modelBuilder.Entity<TemelAlan>()
                .HasMany(e => e.DilTemelAlan)
                .WithOptional(e => e.TemelAlan)
                .HasForeignKey(e => e.FKTemelAlanID);

            modelBuilder.Entity<TemelAlan>()
                .HasMany(e => e.TemelAlanBolumIliski)
                .WithOptional(e => e.TemelAlan)
                .HasForeignKey(e => e.FKTemelAlanID);

            modelBuilder.Entity<TemelAlan>()
                .HasMany(e => e.TemelAlanYeterlilikKategoriIliski)
                .WithOptional(e => e.TemelAlan)
                .HasForeignKey(e => e.FKTemelAlanID);

            modelBuilder.Entity<TemelAlanYeterlilikKategoriIliski>()
                .HasMany(e => e.DilTemelAlanYeterlilik)
                .WithOptional(e => e.TemelAlanYeterlilikKategoriIliski)
                .HasForeignKey(e => e.FKTemelAlanYeterlilikID);

            modelBuilder.Entity<WebBolumAmacHedef>()
                .Property(e => e.FakKod)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<WebBolumAmacHedef>()
                .Property(e => e.BolKod)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<WebBolumAmacHedef>()
                .Property(e => e.Amac)
                .IsUnicode(false);

            modelBuilder.Entity<WebBolumAmacHedef>()
                .Property(e => e.Hedef)
                .IsUnicode(false);

            modelBuilder.Entity<WebBolumAmacHedef>()
                .Property(e => e.AlDerece)
                .IsUnicode(false);

            modelBuilder.Entity<WebBolumAmacHedef>()
                .Property(e => e.KabKosul)
                .IsUnicode(false);

            modelBuilder.Entity<WebBolumAmacHedef>()
                .Property(e => e.OzelKabulKosul)
                .IsUnicode(false);

            modelBuilder.Entity<WebBolumAmacHedef>()
                .Property(e => e.OncekiOgretimTanima)
                .IsUnicode(false);

            modelBuilder.Entity<WebBolumAmacHedef>()
                .Property(e => e.MezKosul)
                .IsUnicode(false);

            modelBuilder.Entity<WebBolumAmacHedef>()
                .Property(e => e.SinavDegerKural)
                .IsUnicode(false);

            modelBuilder.Entity<WebBolumAmacHedef>()
                .Property(e => e.UstKadGecis)
                .IsUnicode(false);

            modelBuilder.Entity<WebBolumAmacHedef>()
                .Property(e => e.BolumBsk)
                .IsUnicode(false);

            modelBuilder.Entity<WebBolumAmacHedef>()
                .Property(e => e.AKTSKoord)
                .IsUnicode(false);

            modelBuilder.Entity<WebBolumDuzeyYeterlilik>()
                .Property(e => e.Aciklama)
                .IsUnicode(false);

            modelBuilder.Entity<WebBolumKategoriIliski>()
                .Property(e => e.FakKod)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<WebBolumKategoriIliski>()
                .Property(e => e.BolKod)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<WebBolumKategoriIliski>()
                .HasOptional(e => e.WebBolumKategoriIliski1)
                .WithRequired(e => e.WebBolumKategoriIliski2);

            modelBuilder.Entity<WebBolumKategoriTnm>()
                .Property(e => e.InKod)
                .HasPrecision(18, 0);

            modelBuilder.Entity<WebBolumTemelAlan>()
                .Property(e => e.Kod)
                .IsUnicode(false);

            modelBuilder.Entity<WebBolumTemelAlan>()
                .Property(e => e.Tanim)
                .IsUnicode(false);

            modelBuilder.Entity<WebBolumTemelAlan>()
                .Property(e => e.TanimEng)
                .IsUnicode(false);

            modelBuilder.Entity<WebBolumTemelAlanIliski>()
                .Property(e => e.FakKod)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<WebBolumTemelAlanIliski>()
                .Property(e => e.BolKod)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<WebBolumTemelAlanIliski>()
                .Property(e => e.Tur)
                .IsUnicode(false);

            modelBuilder.Entity<WebBolumYeterlilik>()
                .Property(e => e.FakKod)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<WebBolumYeterlilik>()
                .Property(e => e.BolKod)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<WebBolumYeterlilik>()
                .Property(e => e.YetIcerik)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersAktsTnm>()
                .Property(e => e.InKod)
                .HasPrecision(18, 0);

            modelBuilder.Entity<WebDersAktsYuk>()
                .Property(e => e.InKod)
                .HasPrecision(18, 0);

            modelBuilder.Entity<WebDersAktsYuk>()
                .Property(e => e.AnaDersPlanID)
                .HasPrecision(18, 0);

            modelBuilder.Entity<WebDersAktsYuk>()
                .Property(e => e.AktsID)
                .HasPrecision(18, 0);

            modelBuilder.Entity<WebDersKategoriTnm>()
                .Property(e => e.Kategori)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<WebDersSosyalYeterlilikIliski>()
                .Property(e => e.InKod)
                .HasPrecision(18, 0);

            modelBuilder.Entity<WebDersSosyalYeterlilikIliski>()
                .Property(e => e.AnaDersPlanID)
                .HasPrecision(18, 0);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.OnKosul)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.Koordinator)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.DersiVeren)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.DersYardimci)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.DersinAmaci)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.DersIcerigi)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.DersNotu)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.DersKaynak)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.H1)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.H2)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.H3)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.H4)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.H5)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.H6)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.H7)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.H8)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.H9)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.H10)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.H11)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.H12)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.H13)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.H14)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.OH1)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.OH2)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.OH3)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.OH4)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.OH5)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.OH6)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.OH7)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.OH8)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.OH9)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.OH10)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.OH11)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.OH12)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.OH13)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.OH14)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.Ekleyen)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.H15)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.H18)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.H16)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.H17)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.H19)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.H20)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.H21)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.H22)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.H23)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.H24)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.H25)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.H26)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.H27)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.H28)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.H29)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.H30)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.H31)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.H32)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.H33)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.H34)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.H35)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.H36)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.OH15)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.OH16)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.OH17)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.OH18)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.OH19)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.OH20)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.OH21)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.OH22)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.OH23)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.OH24)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.OH25)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.OH26)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.OH27)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.OH28)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.OH29)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.OH30)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.OH31)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.OH32)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.OH33)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.OH34)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.OH35)
                .IsUnicode(false);

            modelBuilder.Entity<WebDersTemel>()
                .Property(e => e.OH36)
                .IsUnicode(false);

            modelBuilder.Entity<WebDuzeyYeterlilikler>()
                .Property(e => e.Aciklama)
                .IsUnicode(false);

            modelBuilder.Entity<WebSistemAyar>()
                .Property(e => e.CevrimBasTarih)
                .IsUnicode(false);

            modelBuilder.Entity<WebSistemAyar>()
                .Property(e => e.CevrimBitTarih)
                .IsUnicode(false);

            modelBuilder.Entity<WebSistemAyar>()
                .Property(e => e.OzelYetkililer)
                .IsUnicode(false);

            modelBuilder.Entity<WebYetkililer>()
                .Property(e => e.YetkiTarih1)
                .IsUnicode(false);

            modelBuilder.Entity<WebYetkililer>()
                .Property(e => e.YetkiTarih2)
                .IsUnicode(false);

            modelBuilder.Entity<WebYetkililer>()
                .Property(e => e.Ekleyen)
                .IsUnicode(false);

            modelBuilder.Entity<WebZorunluTipTnm>()
                .Property(e => e.RenkKod)
                .IsFixedLength();

            modelBuilder.Entity<YazOkuluDersler>()
                .Property(e => e.EkleyenKullanici)
                .IsUnicode(false);

            modelBuilder.Entity<YeterlilikAltKategori>()
                .HasMany(e => e.BolumYeterlilikKategoriIliski)
                .WithRequired(e => e.YeterlilikAltKategori)
                .HasForeignKey(e => e.FKYeterlilikAltKategoriID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<YeterlilikAltKategori>()
                .HasMany(e => e.DilYeterlilikAltKategori)
                .WithOptional(e => e.YeterlilikAltKategori)
                .HasForeignKey(e => e.FKYeterlilikAltKategoriID);

            modelBuilder.Entity<YeterlilikAltKategori>()
                .HasMany(e => e.DuzeyYeterlilik)
                .WithOptional(e => e.YeterlilikAltKategori)
                .HasForeignKey(e => e.FKYeterlilikAltKategoriID);

            modelBuilder.Entity<YeterlilikAltKategori>()
                .HasMany(e => e.TemelAlanYeterlilikKategoriIliski)
                .WithOptional(e => e.YeterlilikAltKategori)
                .HasForeignKey(e => e.FKYeterlilikAltKategoriID);

            modelBuilder.Entity<YeterlilikAnaKategori>()
                .HasMany(e => e.DilYeterlilikAnaKategori)
                .WithOptional(e => e.YeterlilikAnaKategori)
                .HasForeignKey(e => e.FKYeterlilikAnaKategoriID);

            modelBuilder.Entity<YeterlilikAnaKategori>()
                .HasMany(e => e.YeterlilikAltKategori)
                .WithOptional(e => e.YeterlilikAnaKategori)
                .HasForeignKey(e => e.FKAnaKategoriID);

            modelBuilder.Entity<Yetkililer>()
                .Property(e => e.SGKullanici)
                .IsUnicode(false);

            modelBuilder.Entity<Birimler>()
                .Property(e => e.EgitimEsasi)
                .IsUnicode(false);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.ABSBirimSorumlu)
                .WithRequired(e => e.Birimler)
                .HasForeignKey(e => e.FKBirimID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.ABSRolYetki)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FK_YonetecegiFakID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.ABSRolYetki1)
                .WithOptional(e => e.Birimler1)
                .HasForeignKey(e => e.FK_YonetecegiBolID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.ABSSistemTarihleri)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FK_BolumID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.ABSSistemTarihleri1)
                .WithOptional(e => e.Birimler1)
                .HasForeignKey(e => e.FK_FakulteID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.ABSYetkiliKullanici)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKFakulteID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.ABSYetkiliKullanici1)
                .WithOptional(e => e.Birimler1)
                .HasForeignKey(e => e.FKBolumID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.UKKullanicilar)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKFakulteBirimID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.UKKullanicilar1)
                .WithOptional(e => e.Birimler1)
                .HasForeignKey(e => e.FKBolumBirimID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.DegisimKayit)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKUniversiteID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.BolumBilgi)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKProgramBirimID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.BolumYeterlilik)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKProgramBirimID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.BolumYeterlilikKategoriIliski)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKProgramBirimID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.DersPlanAnaEtiket)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKFakulteBirimID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.DersPlanEtiket)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKProgramBirimID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.DersYeterlilikIliski)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKProgramBirimID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.TemelAlanBolumIliski)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKProgramBirimID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.BirimKurulusDayanagi)
                .WithRequired(e => e.Birimler)
                .HasForeignKey(e => e.FKBirimID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.AsaletTasdiki)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKFakulteID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.AsaletTasdiki1)
                .WithOptional(e => e.Birimler1)
                .HasForeignKey(e => e.FKUniversiteID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.AsaletTasdikiDetay)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKUniversiteID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.AsaletTasdikiDetay1)
                .WithOptional(e => e.Birimler1)
                .HasForeignKey(e => e.FKFakulteID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.AtanmaSureleri)
                .WithRequired(e => e.Birimler)
                .HasForeignKey(e => e.FKFakulteID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.AtanmaSureleriDetay)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKFGYFakulteID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.AtanmaSureleriDetay1)
                .WithOptional(e => e.Birimler1)
                .HasForeignKey(e => e.FKFGYBolumID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.BirimDil)
                .WithRequired(e => e.Birimler)
                .HasForeignKey(e => e.FKBirimID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.Birimler1)
                .WithOptional(e => e.Birimler2)
                .HasForeignKey(e => e.UstBirimID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.Birimler11)
                .WithOptional(e => e.Birimler3)
                .HasForeignKey(e => e.FKOncekiVersiyonID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.DereceIlerletme)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKFakulteID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.DereceIlerletme1)
                .WithOptional(e => e.Birimler1)
                .HasForeignKey(e => e.FKUniversiteID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.DereceIlerletmeDetay)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKFakulteID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.DereceIlerletmeDetay1)
                .WithOptional(e => e.Birimler1)
                .HasForeignKey(e => e.FKUniversiteID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.DisGorevlendirme)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKKadroBirimiID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.EgitimBilgisi)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKGorevYeriID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.FiiliGorevYeri)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKGorYerAnaBilimDaliID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.FiiliGorevYeri1)
                .WithOptional(e => e.Birimler1)
                .HasForeignKey(e => e.FKGorYerBolumID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.FiiliGorevYeri2)
                .WithOptional(e => e.Birimler2)
                .HasForeignKey(e => e.FKGorYerFakulteID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.FiiliGorevYeri3)
                .WithOptional(e => e.Birimler3)
                .HasForeignKey(e => e.FKGorYerUniversiteID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.Hareket)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKEAnaBilimDaliID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.Hareket1)
                .WithOptional(e => e.Birimler1)
                .HasForeignKey(e => e.FKEBolumID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.Hareket2)
                .WithOptional(e => e.Birimler2)
                .HasForeignKey(e => e.FKEFakulteID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.Hareket3)
                .WithOptional(e => e.Birimler3)
                .HasForeignKey(e => e.FKEUniversiteID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.Hareket4)
                .WithOptional(e => e.Birimler4)
                .HasForeignKey(e => e.FKYAnaBilimDaliID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.Hareket5)
                .WithOptional(e => e.Birimler5)
                .HasForeignKey(e => e.FKYBolumID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.Hareket6)
                .WithOptional(e => e.Birimler6)
                .HasForeignKey(e => e.FKYFakulteID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.Hareket7)
                .WithOptional(e => e.Birimler7)
                .HasForeignKey(e => e.FKYUniversiteID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.IdariGorev)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.BirimID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.IdariGorev1)
                .WithOptional(e => e.Birimler1)
                .HasForeignKey(e => e.FKFakulteID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.IdariGorev2)
                .WithOptional(e => e.Birimler2)
                .HasForeignKey(e => e.FKBolumID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.IdariGorev3)
                .WithOptional(e => e.Birimler3)
                .HasForeignKey(e => e.FKAnaBilimDaliID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.Izin)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKBirimID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.IzinYOKDetay)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKEAnaBilimDaliID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.IzinYOKDetay1)
                .WithOptional(e => e.Birimler1)
                .HasForeignKey(e => e.FKEBolumID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.IzinYOKDetay2)
                .WithOptional(e => e.Birimler2)
                .HasForeignKey(e => e.FKEFakulteID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.IzinYOKDetay3)
                .WithOptional(e => e.Birimler3)
                .HasForeignKey(e => e.FKEUniversiteID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.IzinYOKDetay4)
                .WithOptional(e => e.Birimler4)
                .HasForeignKey(e => e.FKYAnaBilimDaliID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.IzinYOKDetay5)
                .WithOptional(e => e.Birimler5)
                .HasForeignKey(e => e.FKYBolumID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.IzinYOKDetay6)
                .WithOptional(e => e.Birimler6)
                .HasForeignKey(e => e.FKYFakulteID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.IzinYOKDetay7)
                .WithOptional(e => e.Birimler7)
                .HasForeignKey(e => e.FKYUniversiteID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.KadroBilgisi)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKAnaBilimDaliID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.KadroBilgisi1)
                .WithOptional(e => e.Birimler1)
                .HasForeignKey(e => e.FKBolumID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.KadroBilgisi2)
                .WithOptional(e => e.Birimler2)
                .HasForeignKey(e => e.FKFakulteID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.KadroBilgisi3)
                .WithOptional(e => e.Birimler3)
                .HasForeignKey(e => e.FKUniversiteID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.Mekan)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKBolumPrBirimID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.MekanBinalar1)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKFakulteID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.MekanV2)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKBirimID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.OBSOnKayitAyarHazirlik)
                .WithRequired(e => e.Birimler)
                .HasForeignKey(e => e.FKBirimPrgID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.OBSYazilmaAyar)
                .WithRequired(e => e.Birimler)
                .HasForeignKey(e => e.FKBirimID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.OGRAkademikTakvim)
                .WithRequired(e => e.Birimler)
                .HasForeignKey(e => e.FKBirimID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.OGRBelgeSayiOnur)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKFakulteBirimID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.OGRDegisim)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKUniversiteID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.OGRDersGrup)
                .WithRequired(e => e.Birimler)
                .HasForeignKey(e => e.FKBirimID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.OGRDersPlan)
                .WithRequired(e => e.Birimler)
                .HasForeignKey(e => e.FKBirimID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.OGRDersPlan1)
                .WithOptional(e => e.Birimler1)
                .HasForeignKey(e => e.FKBolumPrBirimID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.OGRDersPlan2)
                .WithOptional(e => e.Birimler2)
                .HasForeignKey(e => e.FKFakulteBirimID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.OGRDersPlanFiltre)
                .WithRequired(e => e.Birimler)
                .HasForeignKey(e => e.FKBirimID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.OGRDiplomaFormat)
                .WithRequired(e => e.Birimler)
                .HasForeignKey(e => e.FKBirimID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.OGREgitimBilgileri)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKYLisansUnivID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.OGREgitimBilgileri1)
                .WithOptional(e => e.Birimler1)
                .HasForeignKey(e => e.FKYLisansUnivID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.OGREnstituKimlik)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKBolumPrBirimID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.OGREnstituKomitelerJuriler)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKBirimID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.OGREnstituKontenjan)
                .WithRequired(e => e.Birimler)
                .HasForeignKey(e => e.FKBolumPrBirimID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.OGREnstituYeterlilikKomite)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKBolumPrBirimID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.OGRGeciciOgrenciNumaralari)
                .WithRequired(e => e.Birimler)
                .HasForeignKey(e => e.FKBirimID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.OGRHarcHesapAciklama)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKFakulteBirimID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.OGRHarcMiktar)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKBolumPrBirimID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.OGRHarcSonOdemeTarih)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKBolumPrBirimID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.OGRIntibakBirim)
                .WithRequired(e => e.Birimler)
                .HasForeignKey(e => e.FKBirimID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.OGRKimlik)
                .WithRequired(e => e.Birimler)
                .HasForeignKey(e => e.FKBirimID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.OGRKimlik1)
                .WithRequired(e => e.Birimler1)
                .HasForeignKey(e => e.FKKayitBirimID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.OGRKimlik2)
                .WithRequired(e => e.Birimler2)
                .HasForeignKey(e => e.FKDiplomaBirimID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.OGRKimlik3)
                .WithRequired(e => e.Birimler3)
                .HasForeignKey(e => e.FKBolumPrBirimID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.OGRKurulKarar)
                .WithRequired(e => e.Birimler)
                .HasForeignKey(e => e.FKBirimID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.OGROSYMOgrenciAktarim)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKBirimID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.OGRRaporTanim)
                .WithRequired(e => e.Birimler)
                .HasForeignKey(e => e.FKBirimID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.OGRTNMBelge)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKFakulteBirimID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.OGRTNMBilimDali)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKBolumPrBirimID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.OGRTNMBolumYilaGore)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKFakulteBirimID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.OGRTNMDonemBilgi)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKBolumPrBirimID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.OGRTNMOkulOSYMKod)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKBolumPrBirimID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.PersonelBilgisi)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FK_GorevBolID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.PersonelBilgisi1)
                .WithOptional(e => e.Birimler1)
                .HasForeignKey(e => e.FK_GorevFakID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.OGRTNMSaatBilgi)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKBolumPrBirimID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.OGRDiplomaUnvan)
                .WithRequired(e => e.Birimler)
                .HasForeignKey(e => e.FKBirimID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.OBSOnKayit)
                .WithRequired(e => e.Birimler)
                .HasForeignKey(e => e.FKBolumPrBirimID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.PersonelBilgisi2)
                .WithOptional(e => e.Birimler2)
                .HasForeignKey(e => e.FK_GorevAbdID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.TerfiBilgisi)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKFakulteID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.TerfiBilgisi1)
                .WithOptional(e => e.Birimler1)
                .HasForeignKey(e => e.FKUniversiteID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.TerfiBilgisiDetay)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKFakulteID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.TerfiBilgisiDetay1)
                .WithOptional(e => e.Birimler1)
                .HasForeignKey(e => e.FKUniversiteID);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.TNMEnstituKontenjanAdi)
                .WithRequired(e => e.Birimler)
                .HasForeignKey(e => e.FKBirimID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Birimler>()
                .HasMany(e => e.TNMKullanicilar)
                .WithOptional(e => e.Birimler)
                .HasForeignKey(e => e.FKFakulteID);

            modelBuilder.Entity<Birimler>()
                .HasOptional(e => e.TNMKurumBilgisi)
                .WithRequired(e => e.Birimler);

            modelBuilder.Entity<Kisi>()
                .Property(e => e.TC)
                .IsUnicode(false);

            modelBuilder.Entity<Kisi>()
                .HasMany(e => e.AdresBilgisi)
                .WithRequired(e => e.Kisi)
                .HasForeignKey(e => e.FKKisiID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Kisi>()
                .HasMany(e => e.IBANBilgisi)
                .WithRequired(e => e.Kisi)
                .HasForeignKey(e => e.FKKisiID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Kisi>()
                .HasMany(e => e.Kullanici1)
                .WithOptional(e => e.Kisi)
                .HasForeignKey(e => e.FKKisiID);

            modelBuilder.Entity<Kisi>()
                .HasMany(e => e.OGRAskerlik)
                .WithRequired(e => e.Kisi)
                .HasForeignKey(e => e.FKKisiID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Kisi>()
                .HasMany(e => e.OGRBelgeAyar)
                .WithOptional(e => e.Kisi)
                .HasForeignKey(e => e.FKKisiID);

            modelBuilder.Entity<Kisi>()
                .HasMany(e => e.OGRHarcKrediUcretlendirmesiIade)
                .WithRequired(e => e.Kisi)
                .HasForeignKey(e => e.FKKisiID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Kisi>()
                .HasMany(e => e.OGRHarcKrediUcretlendirmesiOdeme)
                .WithOptional(e => e.Kisi)
                .HasForeignKey(e => e.FKKisiID);

            modelBuilder.Entity<Kisi>()
                .HasMany(e => e.OGRKimlik)
                .WithRequired(e => e.Kisi)
                .HasForeignKey(e => e.FKKisiID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Kisi>()
                .HasMany(e => e.OGRSaglik)
                .WithRequired(e => e.Kisi)
                .HasForeignKey(e => e.FKKisiID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Kisi>()
                .HasMany(e => e.PersonelBilgisi)
                .WithRequired(e => e.Kisi)
                .HasForeignKey(e => e.FKKisiID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Kisi>()
                .HasMany(e => e.TelefonBilgisi)
                .WithRequired(e => e.Kisi)
                .HasForeignKey(e => e.FKKisiID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<mekanBinalar>()
                .Property(e => e.kodBina)
                .IsUnicode(false);

            modelBuilder.Entity<mekanBinalar>()
                .Property(e => e.foto)
                .IsUnicode(false);

            modelBuilder.Entity<mekanBinalar>()
                .Property(e => e.fakAd)
                .IsUnicode(false);

            modelBuilder.Entity<mekanBinalar>()
                .Property(e => e.EngfakAd)
                .IsUnicode(false);

            modelBuilder.Entity<Nufus>()
                .Property(e => e.MahalleKoy)
                .IsUnicode(false);

            modelBuilder.Entity<Nufus>()
                .HasMany(e => e.Kisi)
                .WithOptional(e => e.Nufus)
                .HasForeignKey(e => e.FKNufusID);

            modelBuilder.Entity<osymSiralama>()
                .Property(e => e.EnDusuk)
                .HasPrecision(10, 5);

            modelBuilder.Entity<osymSiralama>()
                .Property(e => e.EnYuksek)
                .HasPrecision(10, 5);

            modelBuilder.Entity<RaporTasarim>()
                .HasMany(e => e.OGRDiplomaFormat)
                .WithRequired(e => e.RaporTasarim)
                .HasForeignKey(e => e.FKRaporTasarimID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<RaporTasarim>()
                .HasMany(e => e.OGRDiplomaFormat1)
                .WithRequired(e => e.RaporTasarim1)
                .HasForeignKey(e => e.FKRaporTasarimArkaID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Sayilar>()
                .Property(e => e.AciklamaTR)
                .IsUnicode(false);

            modelBuilder.Entity<Sayilar>()
                .Property(e => e.AciklamaEN)
                .IsUnicode(false);

            modelBuilder.Entity<Sayilar>()
                .Property(e => e.sira)
                .IsUnicode(false);

            modelBuilder.Entity<Sayilar>()
                .Property(e => e.deger1)
                .IsUnicode(false);

            modelBuilder.Entity<Sayilar>()
                .Property(e => e.deger2)
                .IsUnicode(false);

            modelBuilder.Entity<Sayilar>()
                .Property(e => e.deger3)
                .IsUnicode(false);

            modelBuilder.Entity<Sayilar>()
                .Property(e => e.deger4)
                .IsUnicode(false);

            modelBuilder.Entity<Sayilar>()
                .Property(e => e.deger0)
                .IsUnicode(false);

            modelBuilder.Entity<Sayilar>()
                .Property(e => e.deger5)
                .IsUnicode(false);

            modelBuilder.Entity<TNMIl>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<TNMIl>()
                .Property(e => e.Uyruk)
                .IsFixedLength();

            modelBuilder.Entity<TNMIl>()
                .HasMany(e => e.DanismanlikBilgileri)
                .WithOptional(e => e.TNMIl)
                .HasForeignKey(e => e.FK_KaldinizSehirID);

            modelBuilder.Entity<TNMIl>()
                .HasMany(e => e.AdresBilgisi)
                .WithOptional(e => e.TNMIl)
                .HasForeignKey(e => e.FKIlID);

            modelBuilder.Entity<TNMIl>()
                .HasMany(e => e.Nufus)
                .WithOptional(e => e.TNMIl)
                .HasForeignKey(e => e.FKDogIlID);

            modelBuilder.Entity<TNMIl>()
                .HasMany(e => e.Nufus1)
                .WithOptional(e => e.TNMIl1)
                .HasForeignKey(e => e.FKNKIlID);

            modelBuilder.Entity<TNMIl>()
                .HasMany(e => e.OBSOnKayitAdres)
                .WithOptional(e => e.TNMIl)
                .HasForeignKey(e => e.FKIlID);

            modelBuilder.Entity<TNMIl>()
                .HasMany(e => e.OGREgitimBilgileri)
                .WithOptional(e => e.TNMIl)
                .HasForeignKey(e => e.FKLiseIlID);

            modelBuilder.Entity<TNMIl>()
                .HasMany(e => e.OgrenimBilgisi)
                .WithOptional(e => e.TNMIl)
                .HasForeignKey(e => e.FKIlID);

            modelBuilder.Entity<TNMIl>()
                .HasMany(e => e.TNMIlce)
                .WithRequired(e => e.TNMIl)
                .HasForeignKey(e => e.FKIlID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TNMIl>()
                .HasMany(e => e.TNMEgitimYeri)
                .WithOptional(e => e.TNMIl)
                .HasForeignKey(e => e.FKIlID);

            modelBuilder.Entity<TNMIl>()
                .HasMany(e => e.TNMIlce1)
                .WithRequired(e => e.TNMIl1)
                .HasForeignKey(e => e.FKIlID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TNMIl>()
                .HasMany(e => e.TNMOgrenimYerleri)
                .WithOptional(e => e.TNMIl)
                .HasForeignKey(e => e.FKIlID);

            modelBuilder.Entity<TNMIl>()
                .HasMany(e => e.OGRUniversiteDisiGorevli)
                .WithOptional(e => e.TNMIl)
                .HasForeignKey(e => e.FKGorevIlID);

            modelBuilder.Entity<TNMIlce>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<TNMIlce>()
                .HasMany(e => e.DanismanlikBilgileri)
                .WithOptional(e => e.TNMIlce)
                .HasForeignKey(e => e.FK_KaldiginizIlceID);

            modelBuilder.Entity<TNMIlce>()
                .HasMany(e => e.AdresBilgisi)
                .WithOptional(e => e.TNMIlce)
                .HasForeignKey(e => e.FKIlceID);

            modelBuilder.Entity<TNMIlce>()
                .HasMany(e => e.Nufus)
                .WithOptional(e => e.TNMIlce)
                .HasForeignKey(e => e.FKDogIlceID);

            modelBuilder.Entity<TNMIlce>()
                .HasMany(e => e.Nufus1)
                .WithOptional(e => e.TNMIlce1)
                .HasForeignKey(e => e.FKNKIlceID);

            modelBuilder.Entity<TNMIlce>()
                .HasMany(e => e.OBSOnKayitAdres)
                .WithOptional(e => e.TNMIlce)
                .HasForeignKey(e => e.FKIlceID);

            modelBuilder.Entity<TNMIlce>()
                .HasMany(e => e.OgrenimBilgisi)
                .WithOptional(e => e.TNMIlce)
                .HasForeignKey(e => e.FKIlceID);

            modelBuilder.Entity<TNMIlce>()
                .HasMany(e => e.TNMEgitimYeri)
                .WithOptional(e => e.TNMIlce)
                .HasForeignKey(e => e.FKIlceID);

            modelBuilder.Entity<TNMIlce>()
                .HasMany(e => e.TNMOgrenimYerleri)
                .WithOptional(e => e.TNMIlce)
                .HasForeignKey(e => e.FKIlceID);

            modelBuilder.Entity<TNMUlke>()
                .HasMany(e => e.DegisimKayit)
                .WithOptional(e => e.TNMUlke)
                .HasForeignKey(e => e.FKUlkeID);

            modelBuilder.Entity<TNMUlke>()
                .HasMany(e => e.AdresBilgisi)
                .WithRequired(e => e.TNMUlke)
                .HasForeignKey(e => e.FKUlkeID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TNMUlke>()
                .HasMany(e => e.Nufus)
                .WithOptional(e => e.TNMUlke)
                .HasForeignKey(e => e.FKDogUlkeID);

            modelBuilder.Entity<TNMUlke>()
                .HasMany(e => e.Nufus1)
                .WithOptional(e => e.TNMUlke1)
                .HasForeignKey(e => e.FKNKUlkeID);

            modelBuilder.Entity<TNMUlke>()
                .HasMany(e => e.Nufus2)
                .WithOptional(e => e.TNMUlke2)
                .HasForeignKey(e => e.FKUyruk2ID);

            modelBuilder.Entity<TNMUlke>()
                .HasMany(e => e.Nufus3)
                .WithOptional(e => e.TNMUlke3)
                .HasForeignKey(e => e.FKUyrukID);

            modelBuilder.Entity<TNMUlke>()
                .HasMany(e => e.TNMIl)
                .WithOptional(e => e.TNMUlke)
                .HasForeignKey(e => e.FKUlkeID);

            modelBuilder.Entity<TNMUlke>()
                .HasMany(e => e.AsaletTasdiki)
                .WithOptional(e => e.TNMUlke)
                .HasForeignKey(e => e.FKUlkeID);

            modelBuilder.Entity<TNMUlke>()
                .HasMany(e => e.AsaletTasdikiDetay)
                .WithOptional(e => e.TNMUlke)
                .HasForeignKey(e => e.FKUlkeID);

            modelBuilder.Entity<TNMUlke>()
                .HasMany(e => e.DereceIlerletme)
                .WithOptional(e => e.TNMUlke)
                .HasForeignKey(e => e.FKUlkeID);

            modelBuilder.Entity<TNMUlke>()
                .HasMany(e => e.DereceIlerletmeDetay)
                .WithOptional(e => e.TNMUlke)
                .HasForeignKey(e => e.FKUlkeID);

            modelBuilder.Entity<TNMUlke>()
                .HasMany(e => e.OGRDegisim)
                .WithOptional(e => e.TNMUlke)
                .HasForeignKey(e => e.FKUlkeID);

            modelBuilder.Entity<TNMUlke>()
                .HasMany(e => e.OgrenimBilgisi)
                .WithOptional(e => e.TNMUlke)
                .HasForeignKey(e => e.FKUlkeID);

            modelBuilder.Entity<TNMUlke>()
                .HasMany(e => e.OGREnstituBasvuruKimlik)
                .WithOptional(e => e.TNMUlke)
                .HasForeignKey(e => e.FKUyrukID);

            modelBuilder.Entity<TNMUlke>()
                .HasMany(e => e.TNMFakulte)
                .WithRequired(e => e.TNMUlke)
                .HasForeignKey(e => e.FKUlkeID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TNMUlke>()
                .HasMany(e => e.TNMOgrenimYerleri)
                .WithOptional(e => e.TNMUlke)
                .HasForeignKey(e => e.FKUlkeID);

            modelBuilder.Entity<TNMUlke>()
                .HasMany(e => e.TNMUniversite)
                .WithRequired(e => e.TNMUlke)
                .HasForeignKey(e => e.FKUlkeID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<YokBirimler>()
                .Property(e => e.BirimUzunAdi)
                .IsFixedLength();

            modelBuilder.Entity<MekanV2>()
                .HasMany(e => e.MekanCihazV2)
                .WithRequired(e => e.MekanV2)
                .HasForeignKey(e => e.FKMekanID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MekanV2>()
                .HasMany(e => e.MekanFotoV2)
                .WithRequired(e => e.MekanV2)
                .HasForeignKey(e => e.FKMekanID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MekanV2>()
                .HasMany(e => e.MekanKisiV2)
                .WithOptional(e => e.MekanV2)
                .HasForeignKey(e => e.FKMekanID);

            modelBuilder.Entity<MekanV2>()
                .HasMany(e => e.MekanV21)
                .WithOptional(e => e.MekanV22)
                .HasForeignKey(e => e.UstMekanID);

            modelBuilder.Entity<MekanV2>()
                .HasMany(e => e.OGRDersProgramiv2)
                .WithRequired(e => e.MekanV2)
                .HasForeignKey(e => e.FKMekanv2ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MekanV2>()
                .HasMany(e => e.OGREnstituTezTizTosSinavi)
                .WithOptional(e => e.MekanV2)
                .HasForeignKey(e => e.FKSinavMekanID);

            modelBuilder.Entity<MekanV2>()
                .HasMany(e => e.OGRMazeret)
                .WithOptional(e => e.MekanV2)
                .HasForeignKey(e => e.SinavMekanID);

            modelBuilder.Entity<MekanV2>()
                .HasMany(e => e.SinavMekan)
                .WithRequired(e => e.MekanV2)
                .HasForeignKey(e => e.FKMekanID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TNMMekanCihazV2>()
                .HasMany(e => e.MekanCihazV2)
                .WithRequired(e => e.TNMMekanCihazV2)
                .HasForeignKey(e => e.FKCihazID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TNMMekanTipV2>()
                .HasMany(e => e.MekanV2)
                .WithRequired(e => e.TNMMekanTipV2)
                .HasForeignKey(e => e.FKMekanTipID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MernisUygulamaYetki>()
                .HasMany(e => e.MernisUygulamaYetkiIP)
                .WithRequired(e => e.MernisUygulamaYetki)
                .HasForeignKey(e => e.FKUygulamaID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BarinmaKisiler>()
                .Property(e => e.Kullanici)
                .IsUnicode(false);

            modelBuilder.Entity<BarinmaKisiler>()
                .Property(e => e.Aciklama)
                .IsUnicode(false);

            modelBuilder.Entity<Mekan>()
                .Property(e => e.MekanKod)
                .IsUnicode(false);

            modelBuilder.Entity<Mekan>()
                .Property(e => e.MekanAD)
                .IsUnicode(false);

            modelBuilder.Entity<Mekan>()
                .Property(e => e.Aciklama)
                .IsUnicode(false);

            modelBuilder.Entity<Mekan>()
                .Property(e => e.kullanici)
                .IsUnicode(false);

            modelBuilder.Entity<Mekan>()
                .Property(e => e.Mekan1)
                .IsUnicode(false);

            modelBuilder.Entity<Mekan>()
                .Property(e => e.Kod)
                .IsUnicode(false);

            modelBuilder.Entity<Mekan>()
                .HasMany(e => e.MekanV2)
                .WithOptional(e => e.Mekan)
                .HasForeignKey(e => e.EskiID);

            modelBuilder.Entity<Mekan>()
                .HasMany(e => e.BarinmaKisiler)
                .WithOptional(e => e.Mekan)
                .HasForeignKey(e => e.FKMekanID);

            modelBuilder.Entity<Mekan>()
                .HasMany(e => e.OGRDersProgramiv2)
                .WithOptional(e => e.Mekan)
                .HasForeignKey(e => e.FKEskiMekanID);

            modelBuilder.Entity<Mekan>()
                .HasMany(e => e.OGREnstituSeminer)
                .WithOptional(e => e.Mekan)
                .HasForeignKey(e => e.FKMekanID);

            modelBuilder.Entity<Mekan>()
                .HasMany(e => e.OGREnstituTIZ)
                .WithOptional(e => e.Mekan)
                .HasForeignKey(e => e.FKMekanID);

            modelBuilder.Entity<Mekan>()
                .HasMany(e => e.OGREnstituTOS)
                .WithOptional(e => e.Mekan)
                .HasForeignKey(e => e.FKmekanID);

            modelBuilder.Entity<Mekan>()
                .HasMany(e => e.OGREnstituYeterlik)
                .WithOptional(e => e.Mekan)
                .HasForeignKey(e => e.FKMekanID);

            modelBuilder.Entity<Mekan>()
                .HasMany(e => e.MekanCihazlar)
                .WithOptional(e => e.Mekan)
                .HasForeignKey(e => e.FKMekanID);

            modelBuilder.Entity<Mekan>()
                .HasMany(e => e.MekanFoto)
                .WithOptional(e => e.Mekan)
                .HasForeignKey(e => e.FKMekanID);

            modelBuilder.Entity<Mekan>()
                .HasMany(e => e.MekanKisiler)
                .WithOptional(e => e.Mekan)
                .HasForeignKey(e => e.FKMekanID);

            modelBuilder.Entity<MekanBinalar1>()
                .Property(e => e.kodBina)
                .IsUnicode(false);

            modelBuilder.Entity<MekanBinalar1>()
                .Property(e => e.fakAd)
                .IsUnicode(false);

            modelBuilder.Entity<MekanBinalar1>()
                .Property(e => e.kod)
                .IsUnicode(false);

            modelBuilder.Entity<MekanBinalar1>()
                .Property(e => e.Kullanici)
                .IsUnicode(false);

            modelBuilder.Entity<MekanBinalar1>()
                .HasMany(e => e.Mekan)
                .WithOptional(e => e.MekanBinalar1)
                .HasForeignKey(e => e.FKBinaID);

            modelBuilder.Entity<MekanCihazlar>()
                .Property(e => e.CihazAd)
                .IsUnicode(false);

            modelBuilder.Entity<MekanCihazlar>()
                .Property(e => e.CihazOzellik)
                .IsUnicode(false);

            modelBuilder.Entity<TNMMekanIslevler>()
                .Property(e => e.islev)
                .IsUnicode(false);

            modelBuilder.Entity<TNMMekanIslevler>()
                .HasMany(e => e.MekanBinalar1)
                .WithOptional(e => e.TNMMekanIslevler)
                .HasForeignKey(e => e.FKIslevID);

            modelBuilder.Entity<TNMMekanKampusler>()
                .HasMany(e => e.MekanBinalar1)
                .WithOptional(e => e.TNMMekanKampusler)
                .HasForeignKey(e => e.FKKampusID);

            modelBuilder.Entity<TNMMekanTip>()
                .HasMany(e => e.Mekan)
                .WithOptional(e => e.TNMMekanTip)
                .HasForeignKey(e => e.FKMekanTipID);

            modelBuilder.Entity<Mesaj>()
                .HasMany(e => e.Bildirim)
                .WithRequired(e => e.Mesaj)
                .HasForeignKey(e => e.FKMesajID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OBSOnKayit>()
                .HasMany(e => e.OBSOnKayitAdres)
                .WithOptional(e => e.OBSOnKayit)
                .HasForeignKey(e => e.FKOnKayitID);

            modelBuilder.Entity<OGRArsiv>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRBarkodOkuma>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRBarkodOkuma>()
                .HasMany(e => e.OGROgrenciNot)
                .WithOptional(e => e.OGRBarkodOkuma)
                .HasForeignKey(e => e.FKBarkodOkuma);

            modelBuilder.Entity<OGRBelgeIsoNo>()
                .Property(e => e.Parametre1)
                .IsUnicode(false);

            modelBuilder.Entity<OGRBelgeIsoNo>()
                .Property(e => e.Parametre2)
                .IsUnicode(false);

            modelBuilder.Entity<OGRBelgeIsoNo>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRBelgeSayi>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRBelgeSayiOnur>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRCapYandalTanim>()
                .HasMany(e => e.CapYandalKontenjan)
                .WithRequired(e => e.OGRCapYandalTanim)
                .HasForeignKey(e => e.FKCapYandalTanimID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRDanismanlik>()
                .Property(e => e.CalistigiKurum)
                .IsUnicode(false);

            modelBuilder.Entity<OGRDanismanlik>()
                .Property(e => e.AnneMeslek)
                .IsUnicode(false);

            modelBuilder.Entity<OGRDanismanlik>()
                .Property(e => e.BabaMeslek)
                .IsUnicode(false);

            modelBuilder.Entity<OGRDanismanlik>()
                .Property(e => e.KronikHastalik)
                .IsUnicode(false);

            modelBuilder.Entity<OGRDanismanlik>()
                .Property(e => e.Burslar)
                .IsUnicode(false);

            modelBuilder.Entity<OGRDanismanlik>()
                .Property(e => e.BilgisayarBilgisi)
                .IsUnicode(false);

            modelBuilder.Entity<OGRDanismanlik>()
                .Property(e => e.YabancidilBilgisi)
                .IsUnicode(false);

            modelBuilder.Entity<OGRDanismanlik>()
                .Property(e => e.KursSertifikalar)
                .IsUnicode(false);

            modelBuilder.Entity<OGRDanismanlik>()
                .Property(e => e.isDeneyimi)
                .IsUnicode(false);

            modelBuilder.Entity<OGRDegisim>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRDersGrup>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRDersGrup>()
                .HasMany(e => e.ABSBasariNot)
                .WithOptional(e => e.OGRDersGrup)
                .HasForeignKey(e => e.FKDersGrupID);

            modelBuilder.Entity<OGRDersGrup>()
                .HasMany(e => e.ABSDegerlendirme)
                .WithOptional(e => e.OGRDersGrup)
                .HasForeignKey(e => e.FKDersGrupID);

            modelBuilder.Entity<OGRDersGrup>()
                .HasMany(e => e.ABSDegerlendirmeDosya)
                .WithRequired(e => e.OGRDersGrup)
                .HasForeignKey(e => e.FKDersGrupID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRDersGrup>()
                .HasMany(e => e.ABSDevamTakipKontrol)
                .WithRequired(e => e.OGRDersGrup)
                .HasForeignKey(e => e.FKDersGrupID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRDersGrup>()
                .HasMany(e => e.ABSGrupCalisma)
                .WithRequired(e => e.OGRDersGrup)
                .HasForeignKey(e => e.FK_DersGrupID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRDersGrup>()
                .HasMany(e => e.ABSMesaj)
                .WithOptional(e => e.OGRDersGrup)
                .HasForeignKey(e => e.FKDersGrupID);

            modelBuilder.Entity<OGRDersGrup>()
                .HasMany(e => e.ABSOgrenciDanismanlik)
                .WithOptional(e => e.OGRDersGrup)
                .HasForeignKey(e => e.FKDersGrupID);

            modelBuilder.Entity<OGRDersGrup>()
                .HasMany(e => e.ABSOgrenciNot)
                .WithOptional(e => e.OGRDersGrup)
                .HasForeignKey(e => e.FKDersGrupID);

            modelBuilder.Entity<OGRDersGrup>()
                .HasMany(e => e.ABSOgrenciNotDetay)
                .WithOptional(e => e.OGRDersGrup)
                .HasForeignKey(e => e.FKDersGrupID);

            modelBuilder.Entity<OGRDersGrup>()
                .HasMany(e => e.ABSPaylar)
                .WithOptional(e => e.OGRDersGrup)
                .HasForeignKey(e => e.FKDersGrupID);

            modelBuilder.Entity<OGRDersGrup>()
                .HasMany(e => e.ABSYayinlananBasariNotlar)
                .WithOptional(e => e.OGRDersGrup)
                .HasForeignKey(e => e.FKDersGrupID);

            modelBuilder.Entity<OGRDersGrup>()
                .HasMany(e => e.ABSYayinlananPayNotlar)
                .WithOptional(e => e.OGRDersGrup)
                .HasForeignKey(e => e.FKDersGrupID);

            modelBuilder.Entity<OGRDersGrup>()
                .HasMany(e => e.OSinavGrup)
                .WithRequired(e => e.OGRDersGrup)
                .HasForeignKey(e => e.FKDersGrupID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRDersGrup>()
                .HasMany(e => e.DersIcerikPuani)
                .WithRequired(e => e.OGRDersGrup)
                .HasForeignKey(e => e.FKDersGrupID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRDersGrup>()
                .HasMany(e => e.OgrenciDersCevap)
                .WithRequired(e => e.OGRDersGrup)
                .HasForeignKey(e => e.FKDersGrupID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRDersGrup>()
                .HasMany(e => e.YazOkuluOnKayitDersler)
                .WithRequired(e => e.OGRDersGrup)
                .HasForeignKey(e => e.FKDersGrupID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRDersGrup>()
                .HasMany(e => e.ABSDokumanGrup)
                .WithRequired(e => e.OGRDersGrup)
                .HasForeignKey(e => e.FKOGRDersGrupID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRDersGrup>()
                .HasMany(e => e.ABSProjeTakvim)
                .WithRequired(e => e.OGRDersGrup)
                .HasForeignKey(e => e.FKDersGrupID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRDersGrup>()
                .HasMany(e => e.AKRGenelOgrenmeCiktilari)
                .WithRequired(e => e.OGRDersGrup)
                .HasForeignKey(e => e.FKDersGrupID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRDersGrup>()
                .HasMany(e => e.AKRGenelProgramCiktilari)
                .WithRequired(e => e.OGRDersGrup)
                .HasForeignKey(e => e.FKDersGrupID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRDersGrup>()
                .HasMany(e => e.OGRDersGrup1)
                .WithOptional(e => e.OGRDersGrup2)
                .HasForeignKey(e => e.FKRedirectToDersGrupID);

            modelBuilder.Entity<OGRDersGrup>()
                .HasMany(e => e.OGRDersGrupHoca)
                .WithOptional(e => e.OGRDersGrup)
                .HasForeignKey(e => e.FKDersGrupID);

            modelBuilder.Entity<OGRDersGrup>()
                .HasMany(e => e.OGROgrenciYazilma)
                .WithOptional(e => e.OGRDersGrup)
                .HasForeignKey(e => e.FKDersGrupID);

            modelBuilder.Entity<OGRDersGrup>()
                .HasMany(e => e.OGROgrenciYazilma1)
                .WithOptional(e => e.OGRDersGrup1)
                .HasForeignKey(e => e.FKRedirectFromDersGrupID);

            modelBuilder.Entity<OGRDersGrup>()
                .HasMany(e => e.OGRMazeret)
                .WithRequired(e => e.OGRDersGrup)
                .HasForeignKey(e => e.FKDersGrupID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRDersGrup>()
                .HasMany(e => e.OGROgrenciNot)
                .WithOptional(e => e.OGRDersGrup)
                .HasForeignKey(e => e.FKDersGrupID);

            modelBuilder.Entity<OGRDersGrup>()
                .HasMany(e => e.PayRandevu)
                .WithRequired(e => e.OGRDersGrup)
                .HasForeignKey(e => e.FKDersGrupID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRDersGrupHoca>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRDersGrupHoca>()
                .HasMany(e => e.OGRDersProgramiv2)
                .WithRequired(e => e.OGRDersGrupHoca)
                .HasForeignKey(e => e.FKDersGrupHocaID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRDersPlan>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRDersPlan>()
                .HasMany(e => e.ABSBasariNot)
                .WithOptional(e => e.OGRDersPlan)
                .HasForeignKey(e => e.FKDersPlanID);

            modelBuilder.Entity<OGRDersPlan>()
                .HasMany(e => e.ABSOgrenciNot)
                .WithOptional(e => e.OGRDersPlan)
                .HasForeignKey(e => e.FKDersPlanID);

            modelBuilder.Entity<OGRDersPlan>()
                .HasMany(e => e.ABSPaylar)
                .WithOptional(e => e.OGRDersPlan)
                .HasForeignKey(e => e.FKDersPlanID);

            modelBuilder.Entity<OGRDersPlan>()
                .HasMany(e => e.OSinavGrup)
                .WithRequired(e => e.OGRDersPlan)
                .HasForeignKey(e => e.FKDersPlanID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRDersPlan>()
                .HasMany(e => e.DersYeterlilikIliski)
                .WithOptional(e => e.OGRDersPlan)
                .HasForeignKey(e => e.FKDersPlanID);

            modelBuilder.Entity<OGRDersPlan>()
                .HasMany(e => e.OGRDersGrup)
                .WithOptional(e => e.OGRDersPlan)
                .HasForeignKey(e => e.FKDersPlanID);

            modelBuilder.Entity<OGRDersPlan>()
                .HasMany(e => e.OGRDersPlan1)
                .WithOptional(e => e.OGRDersPlan2)
                .HasForeignKey(e => e.FKDersPlanID);

            modelBuilder.Entity<OGRDersPlan>()
                .HasMany(e => e.OGRDersPlanDegisiklikAciklama)
                .WithOptional(e => e.OGRDersPlan)
                .HasForeignKey(e => e.FKDersPlanID);

            modelBuilder.Entity<OGRDersPlan>()
                .HasMany(e => e.OGRDersPlanFiltre)
                .WithRequired(e => e.OGRDersPlan)
                .HasForeignKey(e => e.FKDersPlanID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRDersPlan>()
                .HasMany(e => e.OGROgrenciYazilma)
                .WithOptional(e => e.OGRDersPlan)
                .HasForeignKey(e => e.FKDersPlanID);

            modelBuilder.Entity<OGRDersPlan>()
                .HasMany(e => e.OGRMazeret)
                .WithRequired(e => e.OGRDersPlan)
                .HasForeignKey(e => e.FKDersPlanID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRDersPlan>()
                .HasMany(e => e.OGROgrenciNot)
                .WithOptional(e => e.OGRDersPlan)
                .HasForeignKey(e => e.FKDersPlanID);

            modelBuilder.Entity<OGRDersPlan>()
                .HasMany(e => e.OGRTekDers)
                .WithRequired(e => e.OGRDersPlan)
                .HasForeignKey(e => e.FKDersPlanID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRDersPlan>()
                .HasMany(e => e.SinavTakvim)
                .WithRequired(e => e.OGRDersPlan)
                .HasForeignKey(e => e.FKDersPlanID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRDersPlanAna>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRDersPlanAna>()
                .HasMany(e => e.ABSBasariNot)
                .WithOptional(e => e.OGRDersPlanAna)
                .HasForeignKey(e => e.FKDersPlanAnaID);

            modelBuilder.Entity<OGRDersPlanAna>()
                .HasMany(e => e.ABSDegerlendirmeKural)
                .WithRequired(e => e.OGRDersPlanAna)
                .HasForeignKey(e => e.FKDersPlanAnaID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRDersPlanAna>()
                .HasMany(e => e.ABSDokuman)
                .WithRequired(e => e.OGRDersPlanAna)
                .HasForeignKey(e => e.FKDersPlanAnaID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRDersPlanAna>()
                .HasMany(e => e.ABSOgrenciNot)
                .WithRequired(e => e.OGRDersPlanAna)
                .HasForeignKey(e => e.FKDersPlanAnaID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRDersPlanAna>()
                .HasMany(e => e.ABSPaylar)
                .WithOptional(e => e.OGRDersPlanAna)
                .HasForeignKey(e => e.FKDersPlanAnaID);

            modelBuilder.Entity<OGRDersPlanAna>()
                .HasMany(e => e.ABSPaylar1)
                .WithOptional(e => e.OGRDersPlanAna1)
                .HasForeignKey(e => e.FKAltDersPlanAnaID);

            modelBuilder.Entity<OGRDersPlanAna>()
                .HasMany(e => e.ABSYayinlananBasariNotlar)
                .WithOptional(e => e.OGRDersPlanAna)
                .HasForeignKey(e => e.FKDersPlanAnaID);

            modelBuilder.Entity<OGRDersPlanAna>()
                .HasMany(e => e.ABSYayinlananPayNotlar)
                .WithOptional(e => e.OGRDersPlanAna)
                .HasForeignKey(e => e.FKDersPlanAnaID);

            modelBuilder.Entity<OGRDersPlanAna>()
                .HasMany(e => e.OSinavGrup)
                .WithRequired(e => e.OGRDersPlanAna)
                .HasForeignKey(e => e.FKDersPlanAnaID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRDersPlanAna>()
                .HasMany(e => e.DersAktsYukIliski)
                .WithOptional(e => e.OGRDersPlanAna)
                .HasForeignKey(e => e.FKDersPlanAnaID);

            modelBuilder.Entity<OGRDersPlanAna>()
                .HasMany(e => e.DersAlaniIliski)
                .WithOptional(e => e.OGRDersPlanAna)
                .HasForeignKey(e => e.FKDersPlanAnaID);

            modelBuilder.Entity<OGRDersPlanAna>()
                .HasMany(e => e.DersBilgi)
                .WithOptional(e => e.OGRDersPlanAna)
                .HasForeignKey(e => e.FKDersPlanAnaID);

            modelBuilder.Entity<OGRDersPlanAna>()
                .HasMany(e => e.DersCikti)
                .WithOptional(e => e.OGRDersPlanAna)
                .HasForeignKey(e => e.FKDersPlanAnaID);

            modelBuilder.Entity<OGRDersPlanAna>()
                .HasMany(e => e.DersHaftaIliski)
                .WithOptional(e => e.OGRDersPlanAna)
                .HasForeignKey(e => e.FKDersPlanAnaID);

            modelBuilder.Entity<OGRDersPlanAna>()
                .HasMany(e => e.DersKategoriIliski)
                .WithOptional(e => e.OGRDersPlanAna)
                .HasForeignKey(e => e.FKDersPlanAnaID);

            modelBuilder.Entity<OGRDersPlanAna>()
                .HasMany(e => e.DersKonu)
                .WithOptional(e => e.OGRDersPlanAna)
                .HasForeignKey(e => e.FKDersPlanAnaID);

            modelBuilder.Entity<OGRDersPlanAna>()
                .HasMany(e => e.DersKoordinator)
                .WithRequired(e => e.OGRDersPlanAna)
                .HasForeignKey(e => e.FKDersPlanAnaID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRDersPlanAna>()
                .HasMany(e => e.DersYeterlilikIliski)
                .WithOptional(e => e.OGRDersPlanAna)
                .HasForeignKey(e => e.FKDersPlanAnaID);

            modelBuilder.Entity<OGRDersPlanAna>()
                .HasMany(e => e.YazOkuluDersler)
                .WithOptional(e => e.OGRDersPlanAna)
                .HasForeignKey(e => e.FKDersPlanAnaID);

            modelBuilder.Entity<OGRDersPlanAna>()
                .HasMany(e => e.OGRDersGrup)
                .WithOptional(e => e.OGRDersPlanAna)
                .HasForeignKey(e => e.FKDersPlanAnaID);

            modelBuilder.Entity<OGRDersPlanAna>()
                .HasMany(e => e.OGRDersPlan)
                .WithOptional(e => e.OGRDersPlanAna)
                .HasForeignKey(e => e.FKDersPlanAnaID);

            modelBuilder.Entity<OGRDersPlanAna>()
                .HasMany(e => e.OGRDersPlanAna1)
                .WithOptional(e => e.OGRDersPlanAna2)
                .HasForeignKey(e => e.FKDersPlanAnaID);

            modelBuilder.Entity<OGRDersPlanAna>()
                .HasMany(e => e.OGRIntibak)
                .WithOptional(e => e.OGRDersPlanAna)
                .HasForeignKey(e => e.FKDersPlanAnaID_A);

            modelBuilder.Entity<OGRDersPlanAna>()
                .HasMany(e => e.OGRIntibak1)
                .WithOptional(e => e.OGRDersPlanAna1)
                .HasForeignKey(e => e.FKDersPlanAnaID_B);

            modelBuilder.Entity<OGRDersPlanAna>()
                .HasMany(e => e.OGRIntibak2)
                .WithOptional(e => e.OGRDersPlanAna2)
                .HasForeignKey(e => e.FKDersPlanAnaID_C);

            modelBuilder.Entity<OGRDersPlanAna>()
                .HasMany(e => e.OGRDersPlanAna11)
                .WithOptional(e => e.OGRDersPlanAna3)
                .HasForeignKey(e => e.FKDersPlanAnaUstID);

            modelBuilder.Entity<OGRDersPlanAna>()
                .HasMany(e => e.OGRDersPlanAna12)
                .WithOptional(e => e.OGRDersPlanAna4)
                .HasForeignKey(e => e.FKHiyerarsikKokID);

            modelBuilder.Entity<OGRDersPlanAna>()
                .HasMany(e => e.OGROgrenciYazilma)
                .WithOptional(e => e.OGRDersPlanAna)
                .HasForeignKey(e => e.FKDersPlanAnaID);

            modelBuilder.Entity<OGRDersPlanAna>()
                .HasMany(e => e.OGRIntibakDers)
                .WithRequired(e => e.OGRDersPlanAna)
                .HasForeignKey(e => e.FKDersPlanAnaID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRDersPlanAna>()
                .HasMany(e => e.OGRMazeret)
                .WithRequired(e => e.OGRDersPlanAna)
                .HasForeignKey(e => e.FKDersPlanAnaID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRDersPlanAna>()
                .HasMany(e => e.OGROgrenciNot)
                .WithOptional(e => e.OGRDersPlanAna)
                .HasForeignKey(e => e.FKDersPlanAnaID);

            modelBuilder.Entity<OGRDersPlanAna>()
                .HasMany(e => e.OGRTekDers)
                .WithRequired(e => e.OGRDersPlanAna)
                .HasForeignKey(e => e.FKDersPlanAnaID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRDersPlanAna>()
                .HasMany(e => e.ABSProjeTakvim)
                .WithRequired(e => e.OGRDersPlanAna)
                .HasForeignKey(e => e.FKDersPlanAnaID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRDersPlanAna>()
                .HasMany(e => e.OGRTersineIntibak)
                .WithOptional(e => e.OGRDersPlanAna)
                .HasForeignKey(e => e.FKDPADersiniAlanID);

            modelBuilder.Entity<OGRDersPlanAna>()
                .HasMany(e => e.OGRTersineIntibak1)
                .WithOptional(e => e.OGRDersPlanAna1)
                .HasForeignKey(e => e.FKDPADersiniAlmisSayilirID);

            modelBuilder.Entity<OGRDersPlanDegisiklikAciklama>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRDersProgramiv2>()
                .HasMany(e => e.ABSDevamTakipV2)
                .WithRequired(e => e.OGRDersProgramiv2)
                .HasForeignKey(e => e.FK_DersProgramV2ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRDiplomaNo>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRDondurmaCeza>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRDosya>()
                .HasMany(e => e.OGRBelgeSayi)
                .WithOptional(e => e.OGRDosya)
                .HasForeignKey(e => e.FKDosyaID);

            modelBuilder.Entity<OGREnstituBasvuruKimlik>()
                .HasMany(e => e.OGREnstituBasvuruTercih)
                .WithRequired(e => e.OGREnstituBasvuruKimlik)
                .HasForeignKey(e => e.FKKimlikID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGREnstituBilimselHazirlik>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGREnstituDanisman>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGREnstituDonemAtlatma>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGREnstituEkSure>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGREnstituIngilizceHazirlik>()
                .Property(e => e.BNot)
                .IsUnicode(false);

            modelBuilder.Entity<OGREnstituIngilizceHazirlik>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGREnstituKimlik>()
                .Property(e => e.foto)
                .IsUnicode(false);

            modelBuilder.Entity<OGREnstituKimlik>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGREnstituKomiteJuriUyeleri>()
                .Property(e => e.HariciUyeUnvani)
                .IsFixedLength();

            modelBuilder.Entity<OGREnstituKomiteJuriUyeleri>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGREnstituKomitelerJuriler>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGREnstituKomitelerJuriler>()
                .HasMany(e => e.OGREnstituKomiteJuriUyeleri)
                .WithRequired(e => e.OGREnstituKomitelerJuriler)
                .HasForeignKey(e => e.FKKomiteJuriID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGREnstituKomitelerJuriler>()
                .HasMany(e => e.OGREnstituTezTizTosSinavi)
                .WithOptional(e => e.OGREnstituKomitelerJuriler)
                .HasForeignKey(e => e.FKKomiteJuriID);

            modelBuilder.Entity<OGREnstituKontenjan>()
                .Property(e => e.Nitelik)
                .IsUnicode(false);

            modelBuilder.Entity<OGREnstituKontenjan>()
                .HasMany(e => e.OGREnstituBasvuruTercih)
                .WithOptional(e => e.OGREnstituKontenjan)
                .HasForeignKey(e => e.FKBirimKontenjanID);

            modelBuilder.Entity<OGREnstituSeminer>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGREnstituTezIzlemeKomitesi>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGREnstituTezKonusu>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGREnstituTezKonusu>()
                .HasMany(e => e.OGREnstituTezTizTosSinavi)
                .WithOptional(e => e.OGREnstituTezKonusu)
                .HasForeignKey(e => e.FKTezID);

            modelBuilder.Entity<OGREnstituTezTizTosSinavi>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGREnstituTIZ>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGREnstituTOS>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGREnstituYayin>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGREnstituYeterlik>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRHarcBankadanGelen>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRHarcBankayaGiden>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRHarcBasvuru>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRHarcBilgi>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRHarcDersUcreti>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRHarcEskiBorc>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRHarcFaizOran>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRHarcHesapAciklama>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRHarcIade>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRHarcKrediUcretlendirmesiBorc>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRHarcKrediUcretlendirmesiIade>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRHarcKrediUcretlendirmesiOdeme>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRHarcMiktar>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRHarcOdeme>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRHarcSonOdemeTarih>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRHarcYuzdeOn>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRHazirlik>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRHazirlikNotAralik>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRIntibak>()
                .HasMany(e => e.OGRIntibakBirim)
                .WithRequired(e => e.OGRIntibak)
                .HasForeignKey(e => e.FKIntibakID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRIntibak>()
                .HasMany(e => e.OGRIntibakDers)
                .WithRequired(e => e.OGRIntibak)
                .HasForeignKey(e => e.FKIntibakID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKayitDondurma>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRKimlik>()
                .Property(e => e.foto)
                .IsUnicode(false);

            modelBuilder.Entity<OGRKimlik>()
                .Property(e => e.OgrenciID)
                .IsUnicode(false);

            modelBuilder.Entity<OGRKimlik>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.ABSBasariNot)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.ABSDanismanOnay)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.ABSDevamTakipKontrol)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.ABSDevamTakipV2)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FK_OgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.ABSGrupCalisma)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FK_OgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.ABSOgrenciDanismanlik)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.ABSOgrenciIleGorusme)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FK_OgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.ABSOgrenciNot)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.ABSOgrenciNotDetay)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.ABSOgrenciyeMesaj)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.DanismanlikBilgileri)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OgrenciDersCevap)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.CapYandalTercih)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.EnkaBarinmaBursu)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.GecisTercih)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.HazirlikBasvuru)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.KitapBursu)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.MuafiyetSinavBasvuru)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.MuafiyetSinavTIN)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OgrenciEtkinlikBilgiFormu)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OnlineHarcBorclari)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OnlineHarcOdemeleri)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.DegisimKayit)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.AdresBilgisi)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.TelefonBilgisi)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.FormasyonBasvuru)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OBSIliskiKesme)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OBSMezuniyetAnketi)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGRArsiv)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGRBelgeSayiOnur)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGRDanismanGorusme)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGRDanismanlik)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGRDanismanMesaj)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGRDegisim)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGRDersPlanDegisiklikAciklama)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGRDiplomaNo)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGRDisiplinCeza)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGRDondurmaCeza)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGRDuplicateNo)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGREgitimBilgileri)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGREnstituBilimselHazirlik)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGREnstituDanisman)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGREnstituDonemAtlatma)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGREnstituIngilizceHazirlik)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGREnstituKomitelerJuriler)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGREnstituSeminer)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGREnstituTezIzlemeKomitesi)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGREnstituTezKonusu)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGREnstituTezTizTosSinavi)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGREnstituTIZ)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGREnstituTOS)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGREnstituYayin)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGREnstituYeterlik)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGRGeciciOgrenciNumaralari)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGRHarcBankadanGelen)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGRHarcBankayaGiden)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGRHarcBilgi)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGRHarcDersUcreti)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGRHarcEskiBorc)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGRHarcEskiBorcSilinen)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGRHarcIade)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGRHarcKrediUcretlendirmesiBorc)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGRHarcOdeme)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGRHarcYuzdeOn)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGRHazirlik)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGRKayitBelgeleri)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGRKayitDondurma)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.ABSGeriBildirim)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.OgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGRKimlikFarkliGelis)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.Kullanici1)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGROgrenciHatirlatma)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGROgrenciSorun)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGROgrenciYazilma)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGRKimlik1)
                .WithOptional(e => e.OGRKimlik2)
                .HasForeignKey(e => e.FKAnadalID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGRKimlikDetay)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGRKYKBurs)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGRKYKOgrenim)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGRMazeret)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGROgrenciDersPlan)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGROgrenciEtiket)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGROgrenciGecmis)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGROgrenciNot)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGROgrenciOrtalama)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGROgrenciTemsilci)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OgrOgrenciYonetmelik)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FkOgrenciId);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGROzelOgrenciExceldenTranskript)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGRStaj)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGRTekDers)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGRYazilmaDanismanOnay)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OOdevOgrenci)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OsymOgrenciBasari)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.OGRSakatlikBilgi)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.YOKAktarim)
                .WithOptional(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID);

            modelBuilder.Entity<OGRKimlik>()
                .HasOptional(e => e.YOKBildirilenOgrenciler)
                .WithRequired(e => e.OGRKimlik);

            modelBuilder.Entity<OGRKimlik>()
                .HasMany(e => e.YoksisAktarimDurumu)
                .WithRequired(e => e.OGRKimlik)
                .HasForeignKey(e => e.FKOgrenciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRKimlikFarkliGelis>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRKurulKarar>()
                .HasMany(e => e.OGROgrenciNot)
                .WithOptional(e => e.OGRKurulKarar)
                .HasForeignKey(e => e.FKKurulKararID);

            modelBuilder.Entity<OGRMezunBilgi>()
                .Property(e => e.ePosta)
                .IsUnicode(false);

            modelBuilder.Entity<OGRMezunBilgi>()
                .Property(e => e.web)
                .IsUnicode(false);

            modelBuilder.Entity<OGRMezunBilgi>()
                .Property(e => e.telefon)
                .IsUnicode(false);

            modelBuilder.Entity<OGRMezunBilgi>()
                .Property(e => e.gsm)
                .IsUnicode(false);

            modelBuilder.Entity<OGRMezunBilgi>()
                .Property(e => e.kurum)
                .IsUnicode(false);

            modelBuilder.Entity<OGRMezunBilgi>()
                .Property(e => e.adres)
                .IsUnicode(false);

            modelBuilder.Entity<OGRMezunBilgi>()
                .Property(e => e.unvan)
                .IsUnicode(false);

            modelBuilder.Entity<OGRMezunBilgi>()
                .Property(e => e.aciklama)
                .IsUnicode(false);

            modelBuilder.Entity<OGRMezunBilgi>()
                .Property(e => e.kullanici)
                .IsUnicode(false);

            modelBuilder.Entity<OGROgrenciDersPlan>()
                .HasMany(e => e.OGROgrenciDersPlan1)
                .WithOptional(e => e.OGROgrenciDersPlan2)
                .HasForeignKey(e => e.FKOgrenciDersPlanID);

            modelBuilder.Entity<OGROgrenciDersPlan>()
                .HasMany(e => e.OGROgrenciDersPlan11)
                .WithOptional(e => e.OGROgrenciDersPlan3)
                .HasForeignKey(e => e.FKIkameNotOgrenciDersPlanID);

            modelBuilder.Entity<OGROgrenciDersPlan>()
                .HasMany(e => e.OGROgrenciDersPlan12)
                .WithOptional(e => e.OGROgrenciDersPlan4)
                .HasForeignKey(e => e.FKReferansOgrenciDersPlanID);

            modelBuilder.Entity<OGROgrenciDersPlan>()
                .HasMany(e => e.OGROgrenciNot1)
                .WithOptional(e => e.OGROgrenciDersPlan1)
                .HasForeignKey(e => e.FKOgrenciPlanID);

            modelBuilder.Entity<OGROgrenciDersPlan>()
                .HasMany(e => e.OGROgrenciYazilma1)
                .WithOptional(e => e.OGROgrenciDersPlan1)
                .HasForeignKey(e => e.FKOgrenciDersPlanID);

            modelBuilder.Entity<OGROgrenciHatirlatma>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGROgrenciNot>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGROgrenciNot>()
                .HasMany(e => e.OGROgrenciDersPlan)
                .WithOptional(e => e.OGROgrenciNot)
                .HasForeignKey(e => e.FKOgrenciNotID);

            modelBuilder.Entity<OGROgrenciNot>()
                .HasMany(e => e.OGROgrenciNot1)
                .WithOptional(e => e.OGROgrenciNot2)
                .HasForeignKey(e => e.FKOgrenciNotID);

            modelBuilder.Entity<OGROgrenciNot>()
                .HasMany(e => e.OGROgrenciYazilma)
                .WithOptional(e => e.OGROgrenciNot)
                .HasForeignKey(e => e.FKOgrenciNotID);

            modelBuilder.Entity<OGROgrenciSorun>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGROgrenciTemsilci>()
                .Property(e => e.TelefonNo)
                .IsUnicode(false);

            modelBuilder.Entity<OGROgrenciTemsilci>()
                .Property(e => e.Kullanici)
                .IsUnicode(false);

            modelBuilder.Entity<OGROgrenciTemsilci>()
                .Property(e => e.SGKullanici)
                .IsUnicode(false);

            modelBuilder.Entity<OGROgrenciYazilma>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGROgrenciYazilma>()
                .HasMany(e => e.OGROgrenciDersPlan)
                .WithOptional(e => e.OGROgrenciYazilma)
                .HasForeignKey(e => e.FKYazilmaID);

            modelBuilder.Entity<OGRSakatlikBilgi>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRTnmAFKanunlari>()
                .Property(e => e.KanunSayisi)
                .IsFixedLength();

            modelBuilder.Entity<OGRTnmAFKanunlari>()
                .Property(e => e.ResmiGazeteNo)
                .IsFixedLength();

            modelBuilder.Entity<OGRTnmAFKanunlari>()
                .HasMany(e => e.OGRKimlikFarkliGelis)
                .WithOptional(e => e.OGRTnmAFKanunlari)
                .HasForeignKey(e => e.FKAFKanunID);

            modelBuilder.Entity<OGRTNMBelge>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRTNMBilimDali>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRTNMBilimDali>()
                .HasMany(e => e.OGREnstituKontenjan)
                .WithOptional(e => e.OGRTNMBilimDali)
                .HasForeignKey(e => e.FKBilimDaliID);

            modelBuilder.Entity<OGRTNMBilimDali>()
                .HasMany(e => e.OGRKimlik)
                .WithOptional(e => e.OGRTNMBilimDali)
                .HasForeignKey(e => e.FKBilimID);

            modelBuilder.Entity<OGRTNMBolumYilaGore>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRTNMDersEtiket>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRTNMDersEtiket>()
                .HasMany(e => e.OGRDersPlan)
                .WithOptional(e => e.OGRTNMDersEtiket)
                .HasForeignKey(e => e.FKEtiketID);

            modelBuilder.Entity<OGRTNMDersEtiket>()
                .HasMany(e => e.OGRTNMSaatBilgi)
                .WithOptional(e => e.OGRTNMDersEtiket)
                .HasForeignKey(e => e.FKEtiketID);

            modelBuilder.Entity<OGRTNMDonemBilgi>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRTNMEskiRektor>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRTNMGenel>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRTNMOgrenciEtiket>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRTNMOgrenciEtiket>()
                .HasMany(e => e.OGROgrenciEtiket)
                .WithRequired(e => e.OGRTNMOgrenciEtiket)
                .HasForeignKey(e => e.FKEtiketID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRTNMOgrenciEtiket>()
                .HasMany(e => e.OGROgrenciEtiketIslem)
                .WithRequired(e => e.OGRTNMOgrenciEtiket)
                .HasForeignKey(e => e.FKOgrenciEtiketTanimID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OGRTNMOkulOSYMKod>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRTNMSaat>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRTNMSaatBilgi>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRTNMUniversite>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRTNMYonetmelik>()
                .HasMany(e => e.OgrOgrenciYonetmelik)
                .WithRequired(e => e.OGRTNMYonetmelik)
                .HasForeignKey(e => e.FkYonetmelikId);

            modelBuilder.Entity<OGRTNMYuzlukKarsilik>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRUniversiteDisiGorevli>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<OGRYazilmaDanismanOnay>()
                .HasMany(e => e.OGROgrenciYazilma)
                .WithOptional(e => e.OGRYazilmaDanismanOnay)
                .HasForeignKey(e => e.FKDanismanOnayID);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.YABANCI_DIL)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.OKUL_KOD)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.OKUL_TUR)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.ONCEKI_YIL_YERLESME_DURUMU)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.LYS1_MATEMATIK_DOGRU)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.LYS1_MATEMATIK_YANLIS)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.LYS1_GEOMETRI_DOGRU)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.LYS1_GEOMETRI_YANLIS)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.LYS2_FIZIK_DOGRU)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.LYS2_FIZIK_YANLIS)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.LYS2_KIMYA_DOGRU)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.LYS2_KIMYA_YANLIS)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.LYS2_BIYOLOJI_DOGRU)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.LYS2_BIYOLOJI_YANLIS)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.LYS3_EDEBIYAT_DOGRU)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.LYS3_EDEBIYAT_YANLIS)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.LYS3_COGRAFYA1_DOGRU)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.LYS3_COGRAFYA1_YANLIS)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.LYS4_TARIH_DOGRU)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.LYS4_TARIH_YANLIS)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.LYS4_COGRAFYA2_DOGRU)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.LYS4_COGRAFYA2_YANLIS)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.LYS4_FELSEFE_DOGRU)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.LYS4_FELSEFE_YANLIS)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.LYS5_DIL_DOGRU)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.LYS5_DIL_YANLIS)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.YGS_TURKCE_DOGRU)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.YGS_TURKCE_YANLIS)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.YGS_SOSYAL_BILIMLER_DOGRU)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.YGS_SOSYAL_BILIMLER_YANLIS)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.YGS_TEMEL_MATEMATIK_DOGRU)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.YGS_TEMEL_MATEMATIK_YANLIS)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.YGS_FEN_BILIMLERI_DOGRU)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.YGS_FEN_BILIMLERI_YANLIS)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.MF1_YERLESTIRME)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.MF1_YERLESTIRME_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.MF2_YERLESTIRME)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.MF2_YERLESTIRME_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.MF3_YERLESTIRME)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.MF3_YERLESTIRME_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.MF4_YERLESTIRME)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.MF4_YERLESTIRME_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.TM1_YERLESTIRME)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.TM1_YERLESTIRME_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.TM2_YERLESTIRME)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.TM2_YERLESTIRME_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.TM3_YERLESTIRME)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.TM3_YERLESTIRME_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.TS1_YERLESTIRME)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.TS1_YERLESTIRME_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.TS2_YERLESTIRME)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.TS2_YERLESTIRME_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.DIL1_YERLESTIRME)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.DIL1_YERLESTIRME_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.DIL2_YERLESTIRME)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.DIL2_YERLESTIRME_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.DIL3_YERLESTIRME)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.DIL3_YERLESTIRME_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.YGS1_YERLESTIRME)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.YGS1_YERLESTIRME_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.YGS2_YERLESTIRME)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.YGS2_YERLESTIRME_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.YGS3_YERLESTIRME)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.YGS3_YERLESTIRME_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.YGS4_YERLESTIRME)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.YGS4_YERLESTIRME_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.YGS5_YERLESTIRME)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.YGS5_YERLESTIRME_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.YGS6_YERLESTIRME)
                .IsUnicode(false);

            modelBuilder.Entity<OSYMSonuc>()
                .Property(e => e.YGS6_YERLESTIRME_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<SinavMekan>()
                .HasMany(e => e.SinavGorevli)
                .WithRequired(e => e.SinavMekan)
                .HasForeignKey(e => e.FKSinavMekanID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SinavTakvim>()
                .HasMany(e => e.SinavMekan)
                .WithRequired(e => e.SinavTakvim)
                .HasForeignKey(e => e.FKSinavTavimID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TNMEnstituKontenjanAdi>()
                .HasMany(e => e.OGREnstituKontenjan)
                .WithOptional(e => e.TNMEnstituKontenjanAdi)
                .HasForeignKey(e => e.FKKontenjanAdiID);

            modelBuilder.Entity<TNMSporBranslari>()
                .HasMany(e => e.TNMSporOzGecmisPuan)
                .WithRequired(e => e.TNMSporBranslari)
                .HasForeignKey(e => e.FKBransID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<YOKAktarim>()
                .Property(e => e.DiplomaNotu)
                .HasPrecision(6, 3);

            modelBuilder.Entity<YOKAktarim>()
                .Property(e => e.Kullanici)
                .IsFixedLength();

            modelBuilder.Entity<YOKAktarim>()
                .Property(e => e.Makina)
                .IsFixedLength();

            modelBuilder.Entity<YOKAktarim>()
                .Property(e => e.Akts)
                .HasPrecision(6, 2);

            modelBuilder.Entity<YOKAktarim>()
                .HasMany(e => e.YOKBildirilenOgrenciler)
                .WithRequired(e => e.YOKAktarim)
                .HasForeignKey(e => e.FKYokAktarimID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<YOKAktarim>()
                .HasMany(e => e.YoksisAktarimDurumu)
                .WithOptional(e => e.YOKAktarim)
                .HasForeignKey(e => e.FKYokAktarimID);

            modelBuilder.Entity<OgretimUyeSayilar>()
                .Property(e => e.Kod)
                .IsUnicode(false);

            modelBuilder.Entity<OgretimUyeSayilar>()
                .Property(e => e.ProgramAdi)
                .IsUnicode(false);

            modelBuilder.Entity<OsymApiUygulamaYetki>()
                .HasMany(e => e.OsymApiUygulamaYetkiIP)
                .WithRequired(e => e.OsymApiUygulamaYetki)
                .HasForeignKey(e => e.FKUygulamaID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OsymBasari>()
                .Property(e => e.ProgramAdi)
                .IsUnicode(false);

            modelBuilder.Entity<OsymBasari>()
                .Property(e => e.PuanTuru)
                .IsUnicode(false);

            modelBuilder.Entity<OsymBasari>()
                .Property(e => e.EnKucuk)
                .IsUnicode(false);

            modelBuilder.Entity<OsymBasari>()
                .Property(e => e.EnBuyuk)
                .IsUnicode(false);

            modelBuilder.Entity<OsymKodlar1>()
                .Property(e => e.UnvAd)
                .IsUnicode(false);

            modelBuilder.Entity<OsymKodlar1>()
                .Property(e => e.FakulteAd)
                .IsUnicode(false);

            modelBuilder.Entity<OsymKodlar1>()
                .Property(e => e.YOAd)
                .IsUnicode(false);

            modelBuilder.Entity<OsymKodlar1>()
                .Property(e => e.ProgramAd)
                .IsUnicode(false);

            modelBuilder.Entity<OsymKodlar1>()
                .Property(e => e.TabloAd)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.TCNO)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.YABANCI_DIL)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.OKUL_KOD)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.OKUL_TUR)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.DIPLOMA_NOTU)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.ONCEKI_YIL_YERLESME_DURUMU)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.LYS1_MATEMATIK_DOGRU)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.LYS1_MATEMATIK_YANLIS)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.LYS1_GEOMETRI_DOGRU)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.LYS1_GEOMETRI_YANLIS)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.LYS2_FIZIK_DOGRU)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.LYS2_FIZIK_YANLIS)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.LYS2_KIMYA_DOGRU)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.LYS2_KIMYA_YANLIS)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.LYS2_BIYOLOJI_DOGRU)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.LYS2_BIYOLOJI_YANLIS)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.LYS3_EDEBIYAT_DOGRU)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.LYS3_EDEBIYAT_YANLIS)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.LYS3_COGRAFYA1_DOGRU)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.LYS3_COGRAFYA1_YANLIS)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.LYS4_TARIH_DOGRU)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.LYS4_TARIH_YANLIS)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.LYS4_COGRAFYA2_DOGRU)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.LYS4_COGRAFYA2_YANLIS)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.LYS4_FELSEFE_DOGRU)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.LYS4_FELSEFE_YANLIS)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.LYS5_DIL_DOGRU)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.LYS5_DIL_YANLIS)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.MF1)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.MF1_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.MF2)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.MF2_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.MF3)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.MF3_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.MF4)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.MF4_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.MF1_YERLESTIRME)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.MF1_YERLESTIRME_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.MF2_YERLESTIRME)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.MF2_YERLESTIRME_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.MF3_YERLESTIRME)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.MF3_YERLESTIRME_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.MF4_YERLESTIRME)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.MF4_YERLESTIRME_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.TM1)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.TM1_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.TM2)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.TM2_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.TM3)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.TM3_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.TM1_YERLESTIRME)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.TM1_YERLESTIRME_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.TM2_YERLESTIRME)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.TM2_YERLESTIRME_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.TM3_YERLESTIRME)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.TM3_YERLESTIRME_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.TS1)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.TS1_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.TS2)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.TS2_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.TS1_YERLESTIRME)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.TS1_YERLESTIRME_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.TS2_YERLESTIRME)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.TS2_YERLESTIRME_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.DIL1_YERLESTIRME)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.DIL1_YERLESTIRME_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.DIL2_YERLESTIRME)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.DIL2_YERLESTIRME_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.DIL3_YERLESTIRME)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.DIL3_YERLESTIRME_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.YGS1_YERLESTIRME)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.YGS1_YERLESTIRME_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.YGS2_YERLESTIRME)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.YGS2_YERLESTIRME_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.YGS3_YERLESTIRME)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.YGS3_YERLESTIRME_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.YGS4_YERLESTIRME)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.YGS4_YERLESTIRME_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.YGS5_YERLESTIRME)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.YGS5_YERLESTIRME_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.YGS6_YERLESTIRME)
                .IsUnicode(false);

            modelBuilder.Entity<OsymOgrenciBasari>()
                .Property(e => e.YGS6_YERLESTIRME_BASARI_SIRASI)
                .IsUnicode(false);

            modelBuilder.Entity<UniversiteKodlari>()
                .Property(e => e.Ad)
                .IsUnicode(false);

            modelBuilder.Entity<AsaletTasdiki>()
                .HasMany(e => e.AsaletTasdikiDetay)
                .WithRequired(e => e.AsaletTasdiki)
                .HasForeignKey(e => e.FKAsaletTasdikiID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ASVPersonel>()
                .Property(e => e.Aciklama)
                .IsUnicode(false);

            modelBuilder.Entity<ASVPersonelLog>()
                .Property(e => e.Aciklama)
                .IsUnicode(false);

            modelBuilder.Entity<AtanmaSureleri>()
                .HasMany(e => e.AtanmaSureleriDetay)
                .WithOptional(e => e.AtanmaSureleri)
                .HasForeignKey(e => e.FKAtanmaSureleriID);

            modelBuilder.Entity<BILAkademik>()
                .Property(e => e.YT)
                .IsUnicode(false);

            modelBuilder.Entity<BILAkademik>()
                .Property(e => e.link)
                .IsUnicode(false);

            modelBuilder.Entity<BILAkademik>()
                .Property(e => e.kullanici)
                .IsUnicode(false);

            modelBuilder.Entity<BILAkademik>()
                .Property(e => e.y1)
                .IsUnicode(false);

            modelBuilder.Entity<BILAkademik>()
                .Property(e => e.y2)
                .IsUnicode(false);

            modelBuilder.Entity<BILAkademik>()
                .Property(e => e.y3)
                .IsUnicode(false);

            modelBuilder.Entity<BILAkademik>()
                .Property(e => e.y4)
                .IsUnicode(false);

            modelBuilder.Entity<BILAkademik>()
                .Property(e => e.y5)
                .IsUnicode(false);

            modelBuilder.Entity<BILAkademik>()
                .Property(e => e.y6)
                .IsUnicode(false);

            modelBuilder.Entity<BILAkademik>()
                .Property(e => e.y7)
                .IsUnicode(false);

            modelBuilder.Entity<BILAkademik>()
                .Property(e => e.y8)
                .IsUnicode(false);

            modelBuilder.Entity<BILAkademik>()
                .Property(e => e.y9)
                .IsUnicode(false);

            modelBuilder.Entity<BILAkademik>()
                .Property(e => e.y10)
                .IsUnicode(false);

            modelBuilder.Entity<BILAkademikAtiflar>()
                .Property(e => e.PY)
                .IsUnicode(false);

            modelBuilder.Entity<BYOK>()
                .Property(e => e.NKMahalleKoy)
                .IsUnicode(false);

            modelBuilder.Entity<DereceIlerletme>()
                .HasMany(e => e.DereceIlerletmeDetay)
                .WithRequired(e => e.DereceIlerletme)
                .HasForeignKey(e => e.FKDereceIlerletmeID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DereceIlerletmeDetay>()
                .Property(e => e.SicilNo)
                .IsUnicode(false);

            modelBuilder.Entity<DereceIlerletmeDetay>()
                .Property(e => e.Tahsili)
                .IsUnicode(false);

            modelBuilder.Entity<DisGorevlendirme>()
                .Property(e => e.UYKNo)
                .IsUnicode(false);

            modelBuilder.Entity<DisGorevlendirme>()
                .Property(e => e.FYKNo)
                .IsUnicode(false);

            modelBuilder.Entity<DisGorevlendirme>()
                .Property(e => e.Ad)
                .IsUnicode(false);

            modelBuilder.Entity<DisGorevlendirme>()
                .Property(e => e.Soyad)
                .IsUnicode(false);

            modelBuilder.Entity<EgitimBilgisi>()
                .HasOptional(e => e.EgitimBilgisi1)
                .WithRequired(e => e.EgitimBilgisi2);

            modelBuilder.Entity<Hareket>()
                .Property(e => e.EAkademikUnvan)
                .IsUnicode(false);

            modelBuilder.Entity<Hareket>()
                .Property(e => e.YSinifi)
                .IsUnicode(false);

            modelBuilder.Entity<Hareket>()
                .Property(e => e.YUlke)
                .IsUnicode(false);

            modelBuilder.Entity<Hareket>()
                .Property(e => e.YUniversite)
                .IsUnicode(false);

            modelBuilder.Entity<Hareket>()
                .Property(e => e.YFakulte)
                .IsUnicode(false);

            modelBuilder.Entity<Hareket>()
                .Property(e => e.YBolum)
                .IsUnicode(false);

            modelBuilder.Entity<Hareket>()
                .Property(e => e.YAnaBilimDali)
                .IsUnicode(false);

            modelBuilder.Entity<Hareket>()
                .Property(e => e.YKadroUnvani)
                .IsUnicode(false);

            modelBuilder.Entity<Hareket>()
                .Property(e => e.YAkademikUnvan)
                .IsUnicode(false);

            modelBuilder.Entity<Hareket>()
                .HasMany(e => e.KadroTakasi)
                .WithRequired(e => e.Hareket)
                .HasForeignKey(e => e.FKHareketID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<IdariGorev>()
                .HasMany(e => e.Izin)
                .WithOptional(e => e.IdariGorev)
                .HasForeignKey(e => e.FKIzinVerenYetkiliIdariGorevID);

            modelBuilder.Entity<IdariGorev>()
                .HasMany(e => e.Izin1)
                .WithOptional(e => e.IdariGorev1)
                .HasForeignKey(e => e.FKOnaylayanIdariGorevID);

            modelBuilder.Entity<IzinSakli>()
                .Property(e => e.EvrakNo)
                .IsUnicode(false);

            modelBuilder.Entity<IzinSakli>()
                .Property(e => e.Konu)
                .IsUnicode(false);

            modelBuilder.Entity<IzinSakli>()
                .Property(e => e.Aciklama)
                .IsUnicode(false);

            modelBuilder.Entity<IzinSakli>()
                .Property(e => e.YOKEvrakNo)
                .IsUnicode(false);

            modelBuilder.Entity<IzinSakli>()
                .Property(e => e.UYKEvrakNo)
                .IsUnicode(false);

            modelBuilder.Entity<IzinSakli>()
                .HasMany(e => e.IzinSakliDetay)
                .WithRequired(e => e.IzinSakli)
                .HasForeignKey(e => e.FKFormID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<IzinSakli>()
                .HasMany(e => e.IzinSakliToplam)
                .WithRequired(e => e.IzinSakli)
                .HasForeignKey(e => e.FKFormID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<IzinYOK>()
                .Property(e => e.EvrakNo)
                .IsUnicode(false);

            modelBuilder.Entity<IzinYOK>()
                .Property(e => e.Konu)
                .IsUnicode(false);

            modelBuilder.Entity<IzinYOK>()
                .Property(e => e.Aciklama)
                .IsUnicode(false);

            modelBuilder.Entity<IzinYOK>()
                .Property(e => e.YOKEvrakNo)
                .IsUnicode(false);

            modelBuilder.Entity<IzinYOK>()
                .Property(e => e.UYKEvrakNo)
                .IsUnicode(false);

            modelBuilder.Entity<IzinYOK>()
                .HasMany(e => e.IzinYOKDetay)
                .WithRequired(e => e.IzinYOK)
                .HasForeignKey(e => e.FKIzinYOKID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<IzinYOK>()
                .HasMany(e => e.IzinYOKDoluDetay)
                .WithRequired(e => e.IzinYOK)
                .HasForeignKey(e => e.FKIzinYOKID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<IzinYOKDoluDetay>()
                .Property(e => e.Tahsil)
                .IsUnicode(false);

            modelBuilder.Entity<IzinYOKDoluDetay>()
                .Property(e => e.KadroYeriFakulte)
                .IsUnicode(false);

            modelBuilder.Entity<KadroBilgisi>()
                .HasMany(e => e.Hareket)
                .WithOptional(e => e.KadroBilgisi)
                .HasForeignKey(e => e.FKEKadroBilgisiID);

            modelBuilder.Entity<KadroBilgisi>()
                .HasMany(e => e.Hareket1)
                .WithOptional(e => e.KadroBilgisi1)
                .HasForeignKey(e => e.FKYKadroBilgisiID);

            modelBuilder.Entity<KadroBilgisi>()
                .HasMany(e => e.IzinYOKDetay)
                .WithRequired(e => e.KadroBilgisi)
                .HasForeignKey(e => e.FKEKadroBilgisiID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<KadroBilgisi>()
                .HasMany(e => e.IzinYOKDoluDetay)
                .WithRequired(e => e.KadroBilgisi)
                .HasForeignKey(e => e.FKEKadroBilgisiID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<KadroBilgisi>()
                .HasMany(e => e.KadroTakasi)
                .WithRequired(e => e.KadroBilgisi)
                .HasForeignKey(e => e.FKEHareketKadroBilgisiID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<KadroBilgisi>()
                .HasMany(e => e.KadroTakasi1)
                .WithRequired(e => e.KadroBilgisi1)
                .HasForeignKey(e => e.FKETakasKadroBilgisiID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Madde37>()
                .Property(e => e.Aciklama)
                .IsUnicode(false);

            modelBuilder.Entity<Madde38>()
                .Property(e => e.Aciklama)
                .IsUnicode(false);

            modelBuilder.Entity<Menuler>()
                .HasMany(e => e.YetkiKullaniciMenu)
                .WithRequired(e => e.Menuler)
                .HasForeignKey(e => e.FKMenuID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Menuler>()
                .HasMany(e => e.YetkiRolMenu)
                .WithRequired(e => e.Menuler)
                .HasForeignKey(e => e.FKMenuID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OgrenimBilgisi>()
                .Property(e => e.MahalleKoy)
                .IsUnicode(false);

            modelBuilder.Entity<PersonelBilgisi>()
                .Property(e => e.SicilNo)
                .IsUnicode(false);

            modelBuilder.Entity<PersonelBilgisi>()
                .Property(e => e.TC)
                .IsUnicode(false);

            modelBuilder.Entity<PersonelBilgisi>()
                .Property(e => e.KullaniciAdi)
                .IsUnicode(false);

            modelBuilder.Entity<PersonelBilgisi>()
                .Property(e => e.Ad)
                .IsUnicode(false);

            modelBuilder.Entity<PersonelBilgisi>()
                .Property(e => e.Soyad)
                .IsUnicode(false);

            modelBuilder.Entity<PersonelBilgisi>()
                .Property(e => e.UnvanAd)
                .IsUnicode(false);

            modelBuilder.Entity<PersonelBilgisi>()
                .Property(e => e.GorevAd)
                .IsUnicode(false);

            modelBuilder.Entity<PersonelBilgisi>()
                .Property(e => e.GorevFakAd)
                .IsUnicode(false);

            modelBuilder.Entity<PersonelBilgisi>()
                .Property(e => e.GorevBolAd)
                .IsUnicode(false);

            modelBuilder.Entity<PersonelBilgisi>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<PersonelBilgisi>()
                .Property(e => e.GorevABDAd)
                .IsUnicode(false);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.ABSDanismanOnay)
                .WithRequired(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKDanismanID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.ABSDegerlendirme)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.ABSDevamTakipKontrol)
                .WithRequired(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.ABSDokuman)
                .WithRequired(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.ABSGrupCalisma)
                .WithRequired(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FK_PersonelID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.ABSMesaj)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.ABSOgrenciDanismanlik)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.ABSOgrenciIleGorusme)
                .WithRequired(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FK_PersonelID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.ABSOgrenciyeMesaj)
                .WithRequired(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.ABSPaylar)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.ABSRolYetki)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FK_PersonelID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.ABSYetkiliKullanici)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.DersIcerikPuani)
                .WithRequired(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.OgrenciDersCevap)
                .WithRequired(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.UKKullanicilar)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.PersonelID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.DegisimKayit)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.DersKoordinator)
                .WithRequired(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.AdresBilgisi)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.TelefonBilgisi)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.MekanKisiV2)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKKisiID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.MekanV2)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKSorumluKisiID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.BarinmaKisiler)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.MekanKisiler)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.MekanKisiler1)
                .WithOptional(e => e.PersonelBilgisi1)
                .HasForeignKey(e => e.FKPersonelID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.OGRDanismanGorusme)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.OGRDanismanMesaj)
                .WithRequired(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKDanismanPersonelID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.OGRDersGrupHoca)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.OGREnstituDanisman)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKDanismanID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.OGREnstituSeminer)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKDanismanID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.OGREnstituSeminer1)
                .WithOptional(e => e.PersonelBilgisi1)
                .HasForeignKey(e => e.FKUye1);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.OGREnstituSeminer2)
                .WithOptional(e => e.PersonelBilgisi2)
                .HasForeignKey(e => e.FKUye2);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.OGREnstituTezIzlemeKomitesi)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKDanismanID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.OGREnstituTezIzlemeKomitesi1)
                .WithOptional(e => e.PersonelBilgisi1)
                .HasForeignKey(e => e.FKOgrEleman1ID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.OGREnstituTezIzlemeKomitesi2)
                .WithOptional(e => e.PersonelBilgisi2)
                .HasForeignKey(e => e.FKOgrEleman2ID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.OGREnstituTOS)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKDanismanID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.OGREnstituYeterlik)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKDanismanID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.OGREnstituYeterlik1)
                .WithOptional(e => e.PersonelBilgisi1)
                .HasForeignKey(e => e.FKOgrEleman1ID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.OGREnstituYeterlik2)
                .WithOptional(e => e.PersonelBilgisi2)
                .HasForeignKey(e => e.FKOgrEleman2ID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.OGREnstituYeterlik3)
                .WithOptional(e => e.PersonelBilgisi3)
                .HasForeignKey(e => e.FKOgrEleman3ID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.OGREnstituYeterlik4)
                .WithOptional(e => e.PersonelBilgisi4)
                .HasForeignKey(e => e.FKOgrEleman4ID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.OGRKimlik)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKDanismanID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.OGRMazeret)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKHocaID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.OGRTekDers)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKHocaID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.OGRYazilmaDanismanOnay)
                .WithRequired(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.SinavGorevli)
                .WithRequired(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.AsaletTasdikiDetay)
                .WithRequired(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelBilgisiID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.ASVPersonel)
                .WithRequired(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelBilgisiID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.ASVPersonelLog)
                .WithRequired(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelBilgisiID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.AtanmaSureleriDetay)
                .WithRequired(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelBilgisiID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.BYOK)
                .WithRequired(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelBilgisiID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.DisGorevlendirme)
                .WithRequired(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelBilgisiID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.EgitimBilgisi)
                .WithRequired(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelBilgisiID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.FiiliGorevYeri)
                .WithRequired(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelBilgisiID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.Hareket)
                .WithRequired(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelBilgisiID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.IdariGorev)
                .WithRequired(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelBilgisiID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.Izin)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelBilgisiID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.Izin1)
                .WithOptional(e => e.PersonelBilgisi1)
                .HasForeignKey(e => e.FKIzinVerenYetkiliID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.Izin2)
                .WithOptional(e => e.PersonelBilgisi2)
                .HasForeignKey(e => e.FKOnaylayanID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.IzinKalan)
                .WithRequired(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelBilgisiID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.IzinYOKDetay)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelBilgisiID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.IzinYOKDoluDetay)
                .WithRequired(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelBilgisiID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.KadroBilgisi)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelBilgisiID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.KurumDisiGorevlendirme)
                .WithRequired(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelBilgisiID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.LojmanBilgisi)
                .WithRequired(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelBilgisiID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.OgrenimBilgisi)
                .WithRequired(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelBilgisiID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.Kullanici1)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.OGREnstituYeterlilikKomite)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKBaskanID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.OGREnstituYeterlilikKomite1)
                .WithOptional(e => e.PersonelBilgisi1)
                .HasForeignKey(e => e.FKUye1ID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.OGREnstituYeterlilikKomite2)
                .WithOptional(e => e.PersonelBilgisi2)
                .HasForeignKey(e => e.FKUye2ID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.OGREnstituYeterlilikKomite3)
                .WithOptional(e => e.PersonelBilgisi3)
                .HasForeignKey(e => e.FKUye3ID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.OGREnstituYeterlilikKomite4)
                .WithOptional(e => e.PersonelBilgisi4)
                .HasForeignKey(e => e.FKUye4ID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.Sendika)
                .WithRequired(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelBilgisiID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.SGKHizmetAcikSure)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelBilgisiID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.SGKHizmetAskerlik)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelBilgisiID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.SGKHizmetBirlestirme)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelBilgisiID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.SGKHizmetBorclanma)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelBilgisiID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.SGKHizmetCetveli)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelBilgisiID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.SGKHizmetIHS)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelBilgisiID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.SGKHizmetKurs)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelBilgisiID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.SGKHizmetMahkeme)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelBilgisiID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.SGKHizmetNufus)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelBilgisiID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.SGKHizmetOkul)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelBilgisiID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.SGKHizmetTazminat)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelBilgisiID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.SGKHizmetUnvan)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelBilgisiID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.TNMKullanicilar)
                .WithOptional(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelBilgisiID);

            modelBuilder.Entity<PersonelBilgisi>()
                .HasMany(e => e.YabanciDil)
                .WithRequired(e => e.PersonelBilgisi)
                .HasForeignKey(e => e.FKPersonelBilgisiID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SBTanahtarSozcukler>()
                .Property(e => e.ID)
                .IsUnicode(false);

            modelBuilder.Entity<SBTanahtarSozcukler>()
                .Property(e => e.anahtarSozcuk)
                .IsUnicode(false);

            modelBuilder.Entity<SBTanahtarSozcukler>()
                .HasMany(e => e.UAKtemelAlan)
                .WithOptional(e => e.SBTanahtarSozcukler)
                .HasForeignKey(e => e.anahtar1);

            modelBuilder.Entity<SBTanahtarSozcukler>()
                .HasMany(e => e.UAKtemelAlan1)
                .WithOptional(e => e.SBTanahtarSozcukler1)
                .HasForeignKey(e => e.anahtar2);

            modelBuilder.Entity<SBTanahtarSozcukler>()
                .HasMany(e => e.UAKtemelAlan2)
                .WithOptional(e => e.SBTanahtarSozcukler2)
                .HasForeignKey(e => e.anahtar3);

            modelBuilder.Entity<SBTanahtarSozcukler>()
                .HasMany(e => e.UAKtemelAlan3)
                .WithOptional(e => e.SBTanahtarSozcukler3)
                .HasForeignKey(e => e.anahtar4);

            modelBuilder.Entity<SBTanahtarSozcukler>()
                .HasMany(e => e.UAKtemelAlan4)
                .WithOptional(e => e.SBTanahtarSozcukler4)
                .HasForeignKey(e => e.anahtar5);

            modelBuilder.Entity<SBTAtanmaSekli>()
                .Property(e => e.Ad)
                .IsUnicode(false);

            modelBuilder.Entity<SBTAtanmaSekli>()
                .HasMany(e => e.PersonelBilgisi)
                .WithOptional(e => e.SBTAtanmaSekli)
                .HasForeignKey(e => e.FKUniversiteAtanmaSekliID);

            modelBuilder.Entity<SBTbilimAlanlari>()
                .Property(e => e.ID)
                .IsUnicode(false);

            modelBuilder.Entity<SBTbilimAlanlari>()
                .Property(e => e.bilim_alani)
                .IsUnicode(false);

            modelBuilder.Entity<SBTbilimAlanlari>()
                .HasMany(e => e.UAKtemelAlan)
                .WithOptional(e => e.SBTbilimAlanlari)
                .HasForeignKey(e => e.FKbilimAlanID);

            modelBuilder.Entity<SBTBirimTuru>()
                .Property(e => e.Ad)
                .IsUnicode(false);

            modelBuilder.Entity<SBTBirimTuru>()
                .HasMany(e => e.TNMFakulte)
                .WithRequired(e => e.SBTBirimTuru)
                .HasForeignKey(e => e.FKBirimTuru)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SBTtemelAlanlar>()
                .Property(e => e.temelAlan)
                .IsUnicode(false);

            modelBuilder.Entity<SBTtemelAlanlar>()
                .HasMany(e => e.SBTbilimAlanlari)
                .WithOptional(e => e.SBTtemelAlanlar)
                .HasForeignKey(e => e.temel_alan_id);

            modelBuilder.Entity<SBTtemelAlanlar>()
                .HasMany(e => e.UAKtemelAlan)
                .WithOptional(e => e.SBTtemelAlanlar)
                .HasForeignKey(e => e.FKtemelAlanID);

            modelBuilder.Entity<SBTuyelik>()
                .Property(e => e.uyelik)
                .IsUnicode(false);

            modelBuilder.Entity<SBTuyelik>()
                .HasMany(e => e.uyelik1)
                .WithOptional(e => e.SBTuyelik)
                .HasForeignKey(e => e.FKuyelikID);

            modelBuilder.Entity<SGKHizmetBirlestirme>()
                .Property(e => e.Tckn)
                .IsUnicode(false);

            modelBuilder.Entity<SGKHizmetCetveli>()
                .Property(e => e.Tckn)
                .IsUnicode(false);

            modelBuilder.Entity<SGKHizmetUnvan>()
                .Property(e => e.fhzOrani)
                .HasPrecision(18, 0);

            modelBuilder.Entity<TerfiBilgisi>()
                .HasMany(e => e.TerfiBilgisiDetay)
                .WithRequired(e => e.TerfiBilgisi)
                .HasForeignKey(e => e.FKTerfiBilgisiID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TerfiBilgisiDetay>()
                .Property(e => e.SicilNo)
                .IsUnicode(false);

            modelBuilder.Entity<TerfiBilgisiDetay>()
                .Property(e => e.Tahsili)
                .IsUnicode(false);

            modelBuilder.Entity<TerfiProfesorOdenek>()
                .Property(e => e.EvrakNo)
                .IsUnicode(false);

            modelBuilder.Entity<TerfiProfesorOdenek>()
                .Property(e => e.SicilNo)
                .IsUnicode(false);

            modelBuilder.Entity<TerfiProfesorOdenek>()
                .Property(e => e.AdiSoyadi)
                .IsUnicode(false);

            modelBuilder.Entity<TerfiProfesorOdenek>()
                .Property(e => e.Unvani)
                .IsUnicode(false);

            modelBuilder.Entity<TerfiProfesorOdenek>()
                .Property(e => e.ProfAtanmaTarihi)
                .IsUnicode(false);

            modelBuilder.Entity<TerfiProfesorOdenek>()
                .Property(e => e.EKidemYili)
                .IsUnicode(false);

            modelBuilder.Entity<TerfiProfesorOdenek>()
                .Property(e => e.EEkGosterge)
                .IsUnicode(false);

            modelBuilder.Entity<TerfiProfesorOdenek>()
                .Property(e => e.EMakamTazminati)
                .IsUnicode(false);

            modelBuilder.Entity<TerfiProfesorOdenek>()
                .Property(e => e.EUniversiteOdenegi)
                .IsUnicode(false);

            modelBuilder.Entity<TerfiProfesorOdenek>()
                .Property(e => e.YKidemYili)
                .IsUnicode(false);

            modelBuilder.Entity<TerfiProfesorOdenek>()
                .Property(e => e.YEkGosterge)
                .IsUnicode(false);

            modelBuilder.Entity<TerfiProfesorOdenek>()
                .Property(e => e.YMakamTazminati)
                .IsUnicode(false);

            modelBuilder.Entity<TerfiProfesorOdenek>()
                .Property(e => e.YUniversiteOdenegi)
                .IsUnicode(false);

            modelBuilder.Entity<TerfiProfesorOdenek>()
                .Property(e => e.IslemKod)
                .IsUnicode(false);

            modelBuilder.Entity<TerfiProfesorOdenek>()
                .Property(e => e.GorevYeri)
                .IsUnicode(false);

            modelBuilder.Entity<TNMAciklama>()
                .Property(e => e.AciklamaAd)
                .IsUnicode(false);

            modelBuilder.Entity<TNMAnaBilimDali>()
                .Property(e => e.ACYokEvrakNo)
                .IsUnicode(false);

            modelBuilder.Entity<TNMAnaBilimDali>()
                .Property(e => e.ACResmiGazeteSayisi)
                .IsUnicode(false);

            modelBuilder.Entity<TNMAnaBilimDali>()
                .HasMany(e => e.IzinSakliDetay)
                .WithOptional(e => e.TNMAnaBilimDali)
                .HasForeignKey(e => e.FKEAnaBilimDaliID);

            modelBuilder.Entity<TNMAnaBilimDali>()
                .HasMany(e => e.IzinSakliDetay1)
                .WithOptional(e => e.TNMAnaBilimDali1)
                .HasForeignKey(e => e.FKYAnaBilimDaliID);

            modelBuilder.Entity<TNMAnaBilimDali>()
                .HasMany(e => e.IzinSakliToplam)
                .WithOptional(e => e.TNMAnaBilimDali)
                .HasForeignKey(e => e.FKEAnaBilimDaliID);

            modelBuilder.Entity<TNMAnaBilimDali>()
                .HasMany(e => e.IzinSakliToplam1)
                .WithOptional(e => e.TNMAnaBilimDali1)
                .HasForeignKey(e => e.FKYAnaBilimDaliID);

            modelBuilder.Entity<TNMBolum>()
                .Property(e => e.ACYokEvrakNo)
                .IsUnicode(false);

            modelBuilder.Entity<TNMBolum>()
                .Property(e => e.ACResmiGazeteSayisi)
                .IsUnicode(false);

            modelBuilder.Entity<TNMBolum>()
                .HasMany(e => e.IzinSakliDetay)
                .WithOptional(e => e.TNMBolum)
                .HasForeignKey(e => e.FKEBolumID);

            modelBuilder.Entity<TNMBolum>()
                .HasMany(e => e.IzinSakliDetay1)
                .WithOptional(e => e.TNMBolum1)
                .HasForeignKey(e => e.FKYBolumID);

            modelBuilder.Entity<TNMBolum>()
                .HasMany(e => e.IzinSakliToplam)
                .WithOptional(e => e.TNMBolum)
                .HasForeignKey(e => e.FKEBolumID);

            modelBuilder.Entity<TNMBolum>()
                .HasMany(e => e.IzinSakliToplam1)
                .WithOptional(e => e.TNMBolum1)
                .HasForeignKey(e => e.FKYBolumID);

            modelBuilder.Entity<TNMBolum>()
                .HasMany(e => e.TNMAnaBilimDali)
                .WithRequired(e => e.TNMBolum)
                .HasForeignKey(e => e.FKBolumID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TNMDayanak>()
                .Property(e => e.DayanakAd)
                .IsUnicode(false);

            modelBuilder.Entity<TNMDisKurum>()
                .Property(e => e.Kullanici)
                .IsUnicode(false);

            modelBuilder.Entity<TNMDisKurum>()
                .Property(e => e.KurumAd)
                .IsUnicode(false);

            modelBuilder.Entity<TNMDisKurum>()
                .HasMany(e => e.DisGorevlendirme)
                .WithOptional(e => e.TNMDisKurum)
                .HasForeignKey(e => e.FKDisKurumID);

            modelBuilder.Entity<TNMDisKurum>()
                .HasMany(e => e.DisGorevlendirme1)
                .WithOptional(e => e.TNMDisKurum1)
                .HasForeignKey(e => e.FKDisKurumID);

            modelBuilder.Entity<TNMDisKurum>()
                .HasMany(e => e.TNMDisKurum1)
                .WithOptional(e => e.TNMDisKurum2)
                .HasForeignKey(e => e.FKUstID);

            modelBuilder.Entity<TNMEgitimBilgisi>()
                .HasMany(e => e.EgitimBilgisi)
                .WithRequired(e => e.TNMEgitimBilgisi)
                .HasForeignKey(e => e.FKEgitimBilgisiTanimID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TNMEgitimKonusu>()
                .HasMany(e => e.EgitimBilgisi)
                .WithOptional(e => e.TNMEgitimKonusu)
                .HasForeignKey(e => e.FKEgitimKonusuID);

            modelBuilder.Entity<TNMEgitimKonusu>()
                .HasMany(e => e.TNMEgitimBilgisi)
                .WithRequired(e => e.TNMEgitimKonusu)
                .HasForeignKey(e => e.FKEgitimKonusuID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TNMEgitimYeri>()
                .HasMany(e => e.EgitimBilgisi)
                .WithOptional(e => e.TNMEgitimYeri)
                .HasForeignKey(e => e.FKEgitimYeriID);

            modelBuilder.Entity<TNMEgitimYeri>()
                .HasMany(e => e.TNMEgitimBilgisi)
                .WithRequired(e => e.TNMEgitimYeri)
                .HasForeignKey(e => e.FKEgitimYeriID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TNMFakulte>()
                .Property(e => e.FakulteAd)
                .IsUnicode(false);

            modelBuilder.Entity<TNMFakulte>()
                .Property(e => e.ACYokEvrakNo)
                .IsUnicode(false);

            modelBuilder.Entity<TNMFakulte>()
                .Property(e => e.ACResmiGazeteSayisi)
                .IsUnicode(false);

            modelBuilder.Entity<TNMFakulte>()
                .HasMany(e => e.IzinSakliDetay)
                .WithOptional(e => e.TNMFakulte)
                .HasForeignKey(e => e.FKEFakulteID);

            modelBuilder.Entity<TNMFakulte>()
                .HasMany(e => e.IzinSakliDetay1)
                .WithOptional(e => e.TNMFakulte1)
                .HasForeignKey(e => e.FKYFakulteID);

            modelBuilder.Entity<TNMFakulte>()
                .HasMany(e => e.IzinSakliToplam)
                .WithOptional(e => e.TNMFakulte)
                .HasForeignKey(e => e.FKEFakulteID);

            modelBuilder.Entity<TNMFakulte>()
                .HasMany(e => e.IzinSakliToplam1)
                .WithOptional(e => e.TNMFakulte1)
                .HasForeignKey(e => e.FKYFakulteID);

            modelBuilder.Entity<TNMFakulte>()
                .HasMany(e => e.KaraListe)
                .WithRequired(e => e.TNMFakulte)
                .HasForeignKey(e => e.FKFakulteID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TNMFakulte>()
                .HasMany(e => e.TNMAnaBilimDali)
                .WithRequired(e => e.TNMFakulte)
                .HasForeignKey(e => e.FKFakulteID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TNMFakulte>()
                .HasMany(e => e.TNMBolum)
                .WithRequired(e => e.TNMFakulte)
                .HasForeignKey(e => e.FKFakulteID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TNMGorevUnvani>()
                .HasMany(e => e.IdariGorev)
                .WithOptional(e => e.TNMGorevUnvani)
                .HasForeignKey(e => e.FKGorevUnvaniID);

            modelBuilder.Entity<TNMGorevUnvani>()
                .HasMany(e => e.Grup)
                .WithOptional(e => e.TNMGorevUnvani)
                .HasForeignKey(e => e.FKTNMGorevUnvaniID);

            modelBuilder.Entity<TNMGorevUnvani>()
                .HasMany(e => e.TNMIdariGorev)
                .WithRequired(e => e.TNMGorevUnvani)
                .HasForeignKey(e => e.FKGorevUnvaniID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TNMIdariGorev>()
                .HasMany(e => e.IdariGorev)
                .WithOptional(e => e.TNMIdariGorev)
                .HasForeignKey(e => e.FKIdariGorevID);

            modelBuilder.Entity<TNMIdariOdenekler>()
                .Property(e => e.KadroUnvaniAd)
                .IsUnicode(false);

            modelBuilder.Entity<TNMIdariOdenekler>()
                .Property(e => e.Aciklama)
                .IsUnicode(false);

            modelBuilder.Entity<TNMKadroUnvani>()
                .Property(e => e.UnvanKisaltma)
                .IsUnicode(false);

            modelBuilder.Entity<TNMKadroUnvani>()
                .Property(e => e.UnvanAd)
                .IsUnicode(false);

            modelBuilder.Entity<TNMKadroUnvani>()
                .Property(e => e.UnvanSinifi)
                .IsUnicode(false);

            modelBuilder.Entity<TNMKadroUnvani>()
                .HasMany(e => e.AtanmaSureleriDetay)
                .WithRequired(e => e.TNMKadroUnvani)
                .HasForeignKey(e => e.FKKadroUnvaniID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TNMKadroUnvani>()
                .HasMany(e => e.EgitimBilgisi)
                .WithOptional(e => e.TNMKadroUnvani)
                .HasForeignKey(e => e.FKKadroUnvaniID);

            modelBuilder.Entity<TNMKadroUnvani>()
                .HasMany(e => e.Izin)
                .WithOptional(e => e.TNMKadroUnvani)
                .HasForeignKey(e => e.FKUnvanID);

            modelBuilder.Entity<TNMKadroUnvani>()
                .HasMany(e => e.Izin1)
                .WithOptional(e => e.TNMKadroUnvani1)
                .HasForeignKey(e => e.FKIzinVerenYetkiliUnvanID);

            modelBuilder.Entity<TNMKadroUnvani>()
                .HasMany(e => e.Izin2)
                .WithOptional(e => e.TNMKadroUnvani2)
                .HasForeignKey(e => e.FKOnaylayanUnvanID);

            modelBuilder.Entity<TNMKadroUnvani>()
                .HasMany(e => e.IzinSakliToplam)
                .WithRequired(e => e.TNMKadroUnvani)
                .HasForeignKey(e => e.FKKadroUnvaniID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TNMKadroUnvani>()
                .HasMany(e => e.IzinYOKDetay)
                .WithOptional(e => e.TNMKadroUnvani)
                .HasForeignKey(e => e.FKEKadroUnvaniID);

            modelBuilder.Entity<TNMKadroUnvani>()
                .HasMany(e => e.IzinYOKDetay1)
                .WithOptional(e => e.TNMKadroUnvani1)
                .HasForeignKey(e => e.FKYKadroUnvaniID);

            modelBuilder.Entity<TNMKadroUnvani>()
                .HasMany(e => e.KadroBilgisi)
                .WithOptional(e => e.TNMKadroUnvani)
                .HasForeignKey(e => e.FKKadroUnvaniID);

            modelBuilder.Entity<TNMKadroUnvani>()
                .HasMany(e => e.PersonelBilgisi)
                .WithOptional(e => e.TNMKadroUnvani)
                .HasForeignKey(e => e.FKKadroUnvaniID);

            modelBuilder.Entity<TNMKadroUnvani>()
                .HasMany(e => e.TNMEkGosterge)
                .WithOptional(e => e.TNMKadroUnvani)
                .HasForeignKey(e => e.FKKadroUnvaniID);

            modelBuilder.Entity<TNMKadroUnvani>()
                .HasMany(e => e.TNMGorevTazminati)
                .WithOptional(e => e.TNMKadroUnvani)
                .HasForeignKey(e => e.FKKadroUnvaniID);

            modelBuilder.Entity<TNMKadroUnvani>()
                .HasMany(e => e.TNMIdariOdenekler)
                .WithRequired(e => e.TNMKadroUnvani)
                .HasForeignKey(e => e.FKKadroUnvaniID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TNMKadroUnvani>()
                .HasMany(e => e.TNMKanunMaddeNo)
                .WithOptional(e => e.TNMKadroUnvani)
                .HasForeignKey(e => e.FKKadroUnvaniID);

            modelBuilder.Entity<TNMKadroUnvani>()
                .HasMany(e => e.TNMMakamTazminati)
                .WithOptional(e => e.TNMKadroUnvani)
                .HasForeignKey(e => e.FKKadroUnvaniID);

            modelBuilder.Entity<TNMKadroUnvani>()
                .HasMany(e => e.TNMUniversiteOdenegi)
                .WithOptional(e => e.TNMKadroUnvani)
                .HasForeignKey(e => e.FKKadroUnvaniID);

            modelBuilder.Entity<TNMKanunMaddeNo>()
                .HasMany(e => e.DisGorevlendirme)
                .WithRequired(e => e.TNMKanunMaddeNo)
                .HasForeignKey(e => e.FKKanunMaddeNoID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TNMKanunMaddeNo>()
                .HasMany(e => e.FiiliGorevYeri)
                .WithOptional(e => e.TNMKanunMaddeNo)
                .HasForeignKey(e => e.FKKanunMaddeNoID);

            modelBuilder.Entity<TNMKullanicilar>()
                .Property(e => e.KullaniciAd)
                .IsUnicode(false);

            modelBuilder.Entity<TNMKullanicilar>()
                .Property(e => e.AdSoyad)
                .IsUnicode(false);

            modelBuilder.Entity<TNMKullanicilar>()
                .HasMany(e => e.YetkiKullaniciMenu)
                .WithRequired(e => e.TNMKullanicilar)
                .HasForeignKey(e => e.FKKullaniciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TNMKullanicilar>()
                .HasMany(e => e.YetkiKullaniciRol)
                .WithRequired(e => e.TNMKullanicilar)
                .HasForeignKey(e => e.FKKullaniciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TNMKurumBilgisi>()
                .Property(e => e.KurumAd)
                .IsUnicode(false);

            modelBuilder.Entity<TNMKurumBilgisi>()
                .Property(e => e.PostaKodu)
                .IsUnicode(false);

            modelBuilder.Entity<TNMLojman>()
                .HasMany(e => e.LojmanBilgisi)
                .WithRequired(e => e.TNMLojman)
                .HasForeignKey(e => e.FKLojmanID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TNMSendika>()
                .HasMany(e => e.Sendika)
                .WithRequired(e => e.TNMSendika)
                .HasForeignKey(e => e.FKSendikaID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TNMSistemAyar>()
                .Property(e => e.DagitimAka)
                .IsUnicode(false);

            modelBuilder.Entity<TNMSistemAyar>()
                .Property(e => e.DagitimIdr)
                .IsUnicode(false);

            modelBuilder.Entity<TNMSistemAyar>()
                .Property(e => e.MailServer)
                .IsUnicode(false);

            modelBuilder.Entity<TNMSistemAyar>()
                .Property(e => e.DNSSonEki)
                .IsUnicode(false);

            modelBuilder.Entity<TNMUniversite>()
                .Property(e => e.ACYokEvrakNo)
                .IsUnicode(false);

            modelBuilder.Entity<TNMUniversite>()
                .Property(e => e.ACResmiGazeteSayisi)
                .IsUnicode(false);

            modelBuilder.Entity<TNMUniversite>()
                .HasMany(e => e.IzinSakliDetay)
                .WithOptional(e => e.TNMUniversite)
                .HasForeignKey(e => e.FKEUniversiteID);

            modelBuilder.Entity<TNMUniversite>()
                .HasMany(e => e.IzinSakliDetay1)
                .WithOptional(e => e.TNMUniversite1)
                .HasForeignKey(e => e.FKYUniversiteID);

            modelBuilder.Entity<TNMUniversite>()
                .HasMany(e => e.IzinSakliToplam)
                .WithOptional(e => e.TNMUniversite)
                .HasForeignKey(e => e.FKEUniversiteID);

            modelBuilder.Entity<TNMUniversite>()
                .HasMany(e => e.IzinSakliToplam1)
                .WithOptional(e => e.TNMUniversite1)
                .HasForeignKey(e => e.FKYUniversiteID);

            modelBuilder.Entity<TNMUniversite>()
                .HasMany(e => e.KaraListe)
                .WithRequired(e => e.TNMUniversite)
                .HasForeignKey(e => e.FKUniversiteID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TNMUniversite>()
                .HasMany(e => e.TNMAnaBilimDali)
                .WithRequired(e => e.TNMUniversite)
                .HasForeignKey(e => e.FKUniversiteID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TNMUniversite>()
                .HasMany(e => e.TNMBolum)
                .WithRequired(e => e.TNMUniversite)
                .HasForeignKey(e => e.FKUniversiteID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TNMUniversite>()
                .HasMany(e => e.TNMFakulte)
                .WithRequired(e => e.TNMUniversite)
                .HasForeignKey(e => e.FKUniversiteID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TNMUnvan>()
                .Property(e => e.UnvanKisaltma)
                .IsUnicode(false);

            modelBuilder.Entity<TNMUnvan>()
                .Property(e => e.UnvanAd)
                .IsUnicode(false);

            modelBuilder.Entity<TNMUnvan>()
                .HasMany(e => e.DisGorevlendirme)
                .WithOptional(e => e.TNMUnvan)
                .HasForeignKey(e => e.FKAkademikUnvanID);

            modelBuilder.Entity<TNMUnvan>()
                .HasMany(e => e.Hareket)
                .WithOptional(e => e.TNMUnvan)
                .HasForeignKey(e => e.FKEAkademikUnvanID);

            modelBuilder.Entity<TNMUnvan>()
                .HasMany(e => e.Hareket1)
                .WithOptional(e => e.TNMUnvan1)
                .HasForeignKey(e => e.FKYAkademikUnvanID);

            modelBuilder.Entity<TNMUnvan>()
                .HasMany(e => e.PersonelBilgisi)
                .WithOptional(e => e.TNMUnvan)
                .HasForeignKey(e => e.FKAkademikUnvanID);

            modelBuilder.Entity<TNMYetkiGrup>()
                .Property(e => e.GrupAdi)
                .IsUnicode(false);

            modelBuilder.Entity<TNMYetkiGrup>()
                .HasMany(e => e.TNMKullanicilar)
                .WithOptional(e => e.TNMYetkiGrup)
                .HasForeignKey(e => e.FKGrupID);

            modelBuilder.Entity<UAKtemelAlan>()
                .Property(e => e.tc_no)
                .IsUnicode(false);

            modelBuilder.Entity<UAKtemelAlan>()
                .Property(e => e.FKbilimAlanID)
                .IsUnicode(false);

            modelBuilder.Entity<UAKtemelAlan>()
                .Property(e => e.anahtar1)
                .IsUnicode(false);

            modelBuilder.Entity<UAKtemelAlan>()
                .Property(e => e.anahtar2)
                .IsUnicode(false);

            modelBuilder.Entity<UAKtemelAlan>()
                .Property(e => e.anahtar3)
                .IsUnicode(false);

            modelBuilder.Entity<UAKtemelAlan>()
                .Property(e => e.anahtar4)
                .IsUnicode(false);

            modelBuilder.Entity<UAKtemelAlan>()
                .Property(e => e.anahtar5)
                .IsUnicode(false);

            modelBuilder.Entity<UAKtemelAlan>()
                .Property(e => e.kullanici)
                .IsUnicode(false);

            modelBuilder.Entity<uyelik>()
                .Property(e => e.tc_no)
                .IsUnicode(false);

            modelBuilder.Entity<uyelik>()
                .Property(e => e.kurum)
                .IsUnicode(false);

            modelBuilder.Entity<uyelik>()
                .Property(e => e.kullanici)
                .IsUnicode(false);

            modelBuilder.Entity<YetkiRolTanim>()
                .HasMany(e => e.YetkiKullaniciRol)
                .WithRequired(e => e.YetkiRolTanim)
                .HasForeignKey(e => e.FKRolID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<YetkiRolTanim>()
                .HasMany(e => e.YetkiRolMenu)
                .WithRequired(e => e.YetkiRolTanim)
                .HasForeignKey(e => e.FKRolID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<RandevuAyar>()
                .HasMany(e => e.RandevuKayit)
                .WithRequired(e => e.RandevuAyar)
                .HasForeignKey(e => e.FKAyarID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<RandevuOturum>()
                .HasMany(e => e.RandevuAyar)
                .WithRequired(e => e.RandevuOturum)
                .HasForeignKey(e => e.FKRandevuOturumID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<RandevuOturum>()
                .HasMany(e => e.RandevuKayit)
                .WithRequired(e => e.RandevuOturum)
                .HasForeignKey(e => e.FKRandevuOturumID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<RandevuOturum>()
                .HasMany(e => e.PayRandevu)
                .WithRequired(e => e.RandevuOturum)
                .HasForeignKey(e => e.FKRandevuID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<RaporTanim>()
                .HasMany(e => e.RaporTasarim1)
                .WithRequired(e => e.RaporTanim)
                .HasForeignKey(e => e.FKRaporTanimID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<YokApiUygulamaYetki>()
                .HasMany(e => e.YokApiUygulamaYetkiIP)
                .WithRequired(e => e.YokApiUygulamaYetki)
                .HasForeignKey(e => e.FKUygulamaID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Grup>()
                .HasMany(e => e.GrupKullanici)
                .WithRequired(e => e.Grup)
                .HasForeignKey(e => e.FKGrupID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Grup>()
                .HasMany(e => e.RolGrup)
                .WithRequired(e => e.Grup)
                .HasForeignKey(e => e.FKGrupID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Kullanici>()
                .HasMany(e => e.Bildirim)
                .WithRequired(e => e.Kullanici)
                .HasForeignKey(e => e.FKKullaniciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Kullanici>()
                .HasMany(e => e.Mesaj)
                .WithRequired(e => e.Kullanici)
                .HasForeignKey(e => e.FKGonderenKullaniciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Kullanici>()
                .HasMany(e => e.RandevuKayit)
                .WithRequired(e => e.Kullanici)
                .HasForeignKey(e => e.FKKullaniciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Kullanici>()
                .HasMany(e => e.EPostaOnay)
                .WithRequired(e => e.Kullanici)
                .HasForeignKey(e => e.FKKullaniciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Kullanici>()
                .HasMany(e => e.GrupKullanici)
                .WithRequired(e => e.Kullanici)
                .HasForeignKey(e => e.FKKullaniciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Kullanici>()
                .HasMany(e => e.HesapYetki)
                .WithRequired(e => e.Kullanici)
                .HasForeignKey(e => e.FKYetkiliKullaniciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Kullanici>()
                .HasMany(e => e.HesapYetki1)
                .WithRequired(e => e.Kullanici1)
                .HasForeignKey(e => e.FKKullaniciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Kullanici>()
                .HasMany(e => e.IslemLog)
                .WithOptional(e => e.Kullanici)
                .HasForeignKey(e => e.FKKullaniciID);

            modelBuilder.Entity<Kullanici>()
                .HasMany(e => e.KullaniciAlias)
                .WithRequired(e => e.Kullanici)
                .HasForeignKey(e => e.FKKullaniciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Kullanici>()
                .HasMany(e => e.KullaniciCihaz)
                .WithRequired(e => e.Kullanici)
                .HasForeignKey(e => e.FKKullaniciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Kullanici>()
                .HasMany(e => e.KullaniciDetay)
                .WithRequired(e => e.Kullanici)
                .HasForeignKey(e => e.FKKullaniciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Kullanici>()
                .HasMany(e => e.KullaniciGuvenlik)
                .WithRequired(e => e.Kullanici)
                .HasForeignKey(e => e.FKKullaniciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Kullanici>()
                .HasMany(e => e.KullaniciGuvenlik2)
                .WithRequired(e => e.Kullanici)
                .HasForeignKey(e => e.FKKullaniciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Kullanici>()
                .HasMany(e => e.KullaniciHesapAyar)
                .WithRequired(e => e.Kullanici)
                .HasForeignKey(e => e.FKKullaniciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Kullanici>()
                .HasMany(e => e.KullaniciToken)
                .WithRequired(e => e.Kullanici)
                .HasForeignKey(e => e.FKKullaniciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Kullanici>()
                .HasMany(e => e.KullaniciTokenHistory)
                .WithRequired(e => e.Kullanici)
                .HasForeignKey(e => e.FKKullaniciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Kullanici>()
                .HasMany(e => e.KullaniciTOTP)
                .WithRequired(e => e.Kullanici)
                .HasForeignKey(e => e.FKKullaniciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Kullanici>()
                .HasMany(e => e.ModulIPGuvenlik)
                .WithRequired(e => e.Kullanici1)
                .HasForeignKey(e => e.FKSorumluKullaniciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Kullanici>()
                .HasMany(e => e.RolKullanici)
                .WithRequired(e => e.Kullanici)
                .HasForeignKey(e => e.FKKullaniciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Kullanici>()
                .HasMany(e => e.SifreSifirlaAktivasyon)
                .WithRequired(e => e.Kullanici)
                .HasForeignKey(e => e.FKKullaniciID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Modul>()
                .HasMany(e => e.Modul1)
                .WithOptional(e => e.Modul2)
                .HasForeignKey(e => e.FKUstModulID);

            modelBuilder.Entity<Modul>()
                .HasMany(e => e.ModulIPGuvenlik)
                .WithOptional(e => e.Modul)
                .HasForeignKey(e => e.FKModulID);

            modelBuilder.Entity<Modul>()
                .HasMany(e => e.ModulRol)
                .WithRequired(e => e.Modul)
                .HasForeignKey(e => e.FKModulID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Proje>()
                .HasMany(e => e.Mesaj)
                .WithRequired(e => e.Proje)
                .HasForeignKey(e => e.FKProjeID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Proje>()
                .HasMany(e => e.Modul)
                .WithRequired(e => e.Proje)
                .HasForeignKey(e => e.FKProjeID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Proje>()
                .HasMany(e => e.ModulIPGuvenlik)
                .WithRequired(e => e.Proje)
                .HasForeignKey(e => e.FKProjeID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Proje>()
                .HasMany(e => e.Rol)
                .WithRequired(e => e.Proje)
                .HasForeignKey(e => e.FKProjeID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Rol>()
                .HasMany(e => e.ModulRol)
                .WithRequired(e => e.Rol)
                .HasForeignKey(e => e.FKRolID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Rol>()
                .HasMany(e => e.RolGrup)
                .WithRequired(e => e.Rol)
                .HasForeignKey(e => e.FKRolID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Rol>()
                .HasMany(e => e.RolKullanici)
                .WithRequired(e => e.Rol)
                .HasForeignKey(e => e.FKRolID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TNMDomain>()
                .HasMany(e => e.KullaniciAlias)
                .WithRequired(e => e.TNMDomain)
                .HasForeignKey(e => e.FKDomainID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<WebBolumAlanYeterlilikler>()
                .Property(e => e.TemelAlan)
                .IsUnicode(false);

            modelBuilder.Entity<WebBolumAlanYeterlilikler>()
                .Property(e => e.Turu)
                .IsUnicode(false);

            modelBuilder.Entity<WebBolumAlanYeterlilikler>()
                .Property(e => e.Aciklama)
                .IsUnicode(false);

            modelBuilder.Entity<OGREnstituYeterlilikKomite>()
                .Property(e => e.SGKullanici)
                .IsFixedLength();

            modelBuilder.Entity<BILAkademikYazarlar>()
                .Property(e => e.tc_no)
                .IsUnicode(false);

            modelBuilder.Entity<BILAkademikYazarlar>()
                .Property(e => e.py)
                .IsUnicode(false);

            modelBuilder.Entity<BILAkademikYazarlar>()
                .Property(e => e.yayin)
                .IsUnicode(false);

            modelBuilder.Entity<BILAkademikYazarlar>()
                .Property(e => e.YT)
                .IsUnicode(false);

            modelBuilder.Entity<BILAkademikYazarlar>()
                .Property(e => e.YT1_aciklama)
                .IsUnicode(false);
        }
    }
}
