namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.DersKategoriTnm")]
    public partial class DersKategoriTnm
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DersKategoriTnm()
        {
            DersKategoriIliski = new HashSet<DersKategoriIliski>();
            DilDersKategoriTnm = new HashSet<DilDersKategoriTnm>();
        }

        public int ID { get; set; }

        public short? Yil { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DersKategoriIliski> DersKategoriIliski { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DilDersKategoriTnm> DilDersKategoriTnm { get; set; }
    }
}
