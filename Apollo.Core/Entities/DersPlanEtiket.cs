namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.DersPlanEtiket")]
    public partial class DersPlanEtiket
    {
        public int ID { get; set; }

        [StringLength(50)]
        public string BolKodAd { get; set; }

        [StringLength(50)]
        public string DersKod { get; set; }

        public int Yariyil { get; set; }

        public short? Zorunlu { get; set; }

        public byte? GosterimSekli { get; set; }

        public int? Yil { get; set; }

        public int FKDersPlanAnaEtiketID { get; set; }

        public int? FKProgramBirimID { get; set; }

        [Required]
        [StringLength(50)]
        public string Ekleyen { get; set; }

        public DateTime Zaman { get; set; }

        public virtual DersPlanAnaEtiket DersPlanAnaEtiket { get; set; }

        public virtual Birimler Birimler { get; set; }
    }
}
