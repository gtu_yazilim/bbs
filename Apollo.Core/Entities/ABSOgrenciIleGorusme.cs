namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.ABSOgrenciIleGorusme")]
    public partial class ABSOgrenciIleGorusme
    {
        public int ID { get; set; }

        [Required]
        [StringLength(500)]
        public string GorusmeKayit { get; set; }

        public DateTime GorusmeTarhi { get; set; }

        public int FK_PersonelID { get; set; }

        public int FK_OgrenciID { get; set; }

        public DateTime EklenmeTarhi { get; set; }

        public int? GorusmeTip { get; set; }

        public int? FKDersGrupID { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }
    }
}
