namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.YetkiRolTanim")]
    public partial class YetkiRolTanim
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public YetkiRolTanim()
        {
            YetkiKullaniciRol = new HashSet<YetkiKullaniciRol>();
            YetkiRolMenu = new HashSet<YetkiRolMenu>();
        }

        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool AktifMi { get; set; }

        public int? EnumKadroTipi { get; set; }

        [Required]
        [StringLength(50)]
        public string RolAdi { get; set; }

        [StringLength(250)]
        public string Aciklama { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<YetkiKullaniciRol> YetkiKullaniciRol { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<YetkiRolMenu> YetkiRolMenu { get; set; }
    }
}
