namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("basvuru.MuafiyetSinavTIN")]
    public partial class MuafiyetSinavTIN
    {
        public int ID { get; set; }

        public int TIN { get; set; }

        public int FKBatchKeyID { get; set; }

        public bool Kullanildi { get; set; }

        public bool Iptal { get; set; }

        public int? FKOgrenciID { get; set; }

        public virtual MuafiyetSinavBatchKey MuafiyetSinavBatchKey { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
