namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.ABSMesaj")]
    public partial class ABSMesaj
    {
        public int ID { get; set; }

        public int? FKPersonelID { get; set; }

        public int? FKDersGrupID { get; set; }

        [StringLength(200)]
        public string Konu { get; set; }

        public string Mesaj { get; set; }

        public DateTime? Tarih { get; set; }

        public int? Yil { get; set; }

        public int? Donem { get; set; }

        [StringLength(50)]
        public string EkleyenIP { get; set; }

        public virtual OGRDersGrup OGRDersGrup { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }
    }
}
