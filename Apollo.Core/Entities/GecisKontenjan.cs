namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("basvuru.GecisKontenjan")]
    public partial class GecisKontenjan
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public GecisKontenjan()
        {
            GecisTercih = new HashSet<GecisTercih>();
        }

        public int ID { get; set; }

        public int EnumGecisTipi { get; set; }

        public DateTime TarihKayit { get; set; }

        public int Yil { get; set; }

        public int Donem { get; set; }

        public int? FKAnaProgramID { get; set; }

        public int FKGecisProgramID { get; set; }

        public int Adet { get; set; }

        public bool Silindi { get; set; }

        [Required]
        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(250)]
        public string GecisProgramAd { get; set; }

        public bool KayitAcik { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GecisTercih> GecisTercih { get; set; }
    }
}
