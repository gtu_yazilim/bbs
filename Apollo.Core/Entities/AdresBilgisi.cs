namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("gen.AdresBilgisi")]
    public partial class AdresBilgisi
    {
        public int ID { get; set; }

        public DateTime TarihKayit { get; set; }

        [StringLength(250)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public DateTime GuncellemeTarih { get; set; }

        [StringLength(250)]
        public string GuncellemeKullanici { get; set; }

        public int FKKisiID { get; set; }

        public int? FKOgrenciID { get; set; }

        public int? FKPersonelID { get; set; }

        public int EnumAdresTip { get; set; }

        public bool Silinemez { get; set; }

        public bool TercihEdilen { get; set; }

        public int EnumVeriKayitTip { get; set; }

        public bool Silindi { get; set; }

        [Required]
        [StringLength(250)]
        public string Adres { get; set; }

        [StringLength(250)]
        public string BucakKoyBelde { get; set; }

        [StringLength(250)]
        public string MevkiMezra { get; set; }

        [StringLength(250)]
        public string Mahalle { get; set; }

        [StringLength(250)]
        public string MeydanBulvar { get; set; }

        [StringLength(250)]
        public string CaddeSokak { get; set; }

        [StringLength(250)]
        public string KumeEvler { get; set; }

        [StringLength(250)]
        public string Site { get; set; }

        [StringLength(250)]
        public string Blok { get; set; }

        [StringLength(250)]
        public string DisKapiNo { get; set; }

        [StringLength(250)]
        public string IcKapiNo { get; set; }

        [StringLength(250)]
        public string PostaKodu { get; set; }

        public int FKUlkeID { get; set; }

        public int? FKIlID { get; set; }

        public int? FKIlceID { get; set; }

        [StringLength(50)]
        public string IlceString { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public virtual Kisi Kisi { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }

        public virtual TNMIl TNMIl { get; set; }

        public virtual TNMIlce TNMIlce { get; set; }

        public virtual TNMUlke TNMUlke { get; set; }
    }
}
