namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("gen.Nufus")]
    public partial class Nufus
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Nufus()
        {
            Kisi = new HashSet<Kisi>();
        }

        public int ID { get; set; }

        public int? FKUyrukID { get; set; }

        public int? FKUyruk2ID { get; set; }

        [StringLength(10)]
        public string SeriNo { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? SonGecerlilikTarihi { get; set; }

        [StringLength(4)]
        public string CuzdanSeri { get; set; }

        [StringLength(10)]
        public string CuzdanNo { get; set; }

        [StringLength(50)]
        public string OncekiSoyadi { get; set; }

        [StringLength(50)]
        public string KizlikSoyadi { get; set; }

        [StringLength(50)]
        public string BabaAdi { get; set; }

        [StringLength(50)]
        public string AnneAdi { get; set; }

        [StringLength(50)]
        public string DogumYeri { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? DogumTarihi { get; set; }

        [StringLength(50)]
        public string MahalleKoy { get; set; }

        [StringLength(10)]
        public string CiltNo { get; set; }

        [StringLength(10)]
        public string AileSiraNo { get; set; }

        [StringLength(10)]
        public string SiraNo { get; set; }

        [StringLength(50)]
        public string VerildigiYer { get; set; }

        [StringLength(25)]
        public string VerilisNedeni { get; set; }

        [StringLength(10)]
        public string KayitNo { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? VerilisTarihi { get; set; }

        [StringLength(50)]
        public string VerenMakam { get; set; }

        public int? ENUMVerilisNedeni { get; set; }

        public int? ENUMMedeniHal { get; set; }

        public int? ENUMCinsiyet { get; set; }

        public int? ENUMKanGrubu { get; set; }

        public int? ENUMDin { get; set; }

        public int? FKDogUlkeID { get; set; }

        public int? FKDogIlID { get; set; }

        public int? FKDogIlceID { get; set; }

        public int? FKNKUlkeID { get; set; }

        public int? FKNKIlID { get; set; }

        public int? FKNKIlceID { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public DateTime? Zaman { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public DateTime? OlumTarihi { get; set; }

        public bool? MaviKartVarMi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Kisi> Kisi { get; set; }

        public virtual TNMIlce TNMIlce { get; set; }

        public virtual TNMIlce TNMIlce1 { get; set; }

        public virtual TNMIl TNMIl { get; set; }

        public virtual TNMIl TNMIl1 { get; set; }

        public virtual TNMUlke TNMUlke { get; set; }

        public virtual TNMUlke TNMUlke1 { get; set; }

        public virtual TNMUlke TNMUlke2 { get; set; }

        public virtual TNMUlke TNMUlke3 { get; set; }
    }
}
