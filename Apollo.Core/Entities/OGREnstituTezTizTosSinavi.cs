namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGREnstituTezTizTosSinavi")]
    public partial class OGREnstituTezTizTosSinavi
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public DateTime? Tarih { get; set; }

        [StringLength(10)]
        public string YKNo { get; set; }

        public int FKOgrenciID { get; set; }

        public int EnumSinavTipi { get; set; }

        public int? FKKomiteJuriID { get; set; }

        public int? FKTezID { get; set; }

        public DateTime? SinavZamani { get; set; }

        public int? EnumEnstituYeterlikDurum { get; set; }

        public int? BNot { get; set; }

        public int? FKSinavMekanID { get; set; }

        [StringLength(150)]
        public string SinavYeri { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public int? EnumEnstituYeterlilikSinavTipi { get; set; }

        public virtual MekanV2 MekanV2 { get; set; }

        public virtual OGREnstituKomitelerJuriler OGREnstituKomitelerJuriler { get; set; }

        public virtual OGREnstituTezKonusu OGREnstituTezKonusu { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
