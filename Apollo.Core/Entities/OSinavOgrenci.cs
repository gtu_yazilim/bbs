namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.OSinavOgrenci")]
    public partial class OSinavOgrenci
    {
        [Key]
        [Column(Order = 0)]
        public int EID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DersYazilmaID { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SinavID { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int OgrenciID { get; set; }

        public double? Notu { get; set; }

        [StringLength(4000)]
        public string NotAciklama { get; set; }

        [Key]
        [Column(Order = 4)]
        public byte Durum { get; set; }

        [Key]
        [Column(Order = 5)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int OturumDurum { get; set; }

        [Key]
        [Column(Order = 6)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SoruSayisi { get; set; }

        [Key]
        [Column(Order = 7)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DogruSayisi { get; set; }

        [Key]
        [Column(Order = 8)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int YanlisSayisi { get; set; }

        public DateTime? TarihKatilimIlk { get; set; }

        public DateTime? TarihKatilimSon { get; set; }

        public DateTime? TarihOturumBaslangic { get; set; }

        public DateTime? TarihOturumBitis { get; set; }

        public DateTime? TarihMazeretOturumBaslangic { get; set; }

        public DateTime? TarihMazeretOturumBitis { get; set; }

        [Key]
        [Column(Order = 9)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MazeretDogruSayisi { get; set; }

        [Key]
        [Column(Order = 10)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MazeretYanlisSayisi { get; set; }

        public double? MazeretNotu { get; set; }

        public double? SonNot { get; set; }

        [Key]
        [Column(Order = 11)]
        public bool Silindi { get; set; }
    }
}
