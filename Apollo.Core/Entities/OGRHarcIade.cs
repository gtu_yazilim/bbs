namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRHarcIade")]
    public partial class OGRHarcIade
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(500)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? InKod { get; set; }

        public int? Yil { get; set; }

        public double? Miktar { get; set; }

        public DateTime? Tarih { get; set; }

        [StringLength(250)]
        public string Aciklama { get; set; }

        public int? EnumIadeTip { get; set; }

        public int? EnumIadeTur { get; set; }

        public int? EnumIadeNedeni { get; set; }

        public int? EnumDonem { get; set; }

        public int? FKOgrenciID { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
