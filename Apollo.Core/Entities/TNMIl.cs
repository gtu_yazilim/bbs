namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("gen.TNMIl")]
    public partial class TNMIl
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TNMIl()
        {
            DanismanlikBilgileri = new HashSet<DanismanlikBilgileri>();
            AdresBilgisi = new HashSet<AdresBilgisi>();
            Nufus = new HashSet<Nufus>();
            Nufus1 = new HashSet<Nufus>();
            OBSOnKayitAdres = new HashSet<OBSOnKayitAdres>();
            OGREgitimBilgileri = new HashSet<OGREgitimBilgileri>();
            OgrenimBilgisi = new HashSet<OgrenimBilgisi>();
            TNMIlce = new HashSet<TNMIlce>();
            TNMEgitimYeri = new HashSet<TNMEgitimYeri>();
            TNMIlce1 = new HashSet<TNMIlce>();
            TNMOgrenimYerleri = new HashSet<TNMOgrenimYerleri>();
            OGRUniversiteDisiGorevli = new HashSet<OGRUniversiteDisiGorevli>();
        }

        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? InKod { get; set; }

        [StringLength(50)]
        public string Kod { get; set; }

        [Required]
        [StringLength(250)]
        public string Ad { get; set; }

        public int? AlanKodu { get; set; }

        [StringLength(500)]
        public string AskerlikDairesi { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public int? IlKod { get; set; }

        [StringLength(50)]
        public string Uyruk { get; set; }

        public int? FKUlkeID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DanismanlikBilgileri> DanismanlikBilgileri { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AdresBilgisi> AdresBilgisi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Nufus> Nufus { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Nufus> Nufus1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OBSOnKayitAdres> OBSOnKayitAdres { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREgitimBilgileri> OGREgitimBilgileri { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OgrenimBilgisi> OgrenimBilgisi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TNMIlce> TNMIlce { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TNMEgitimYeri> TNMEgitimYeri { get; set; }

        public virtual TNMUlke TNMUlke { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TNMIlce> TNMIlce1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TNMOgrenimYerleri> TNMOgrenimYerleri { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRUniversiteDisiGorevli> OGRUniversiteDisiGorevli { get; set; }
    }
}
