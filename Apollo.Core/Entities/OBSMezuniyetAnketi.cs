namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("obs.OBSMezuniyetAnketi")]
    public partial class OBSMezuniyetAnketi
    {
        public int ID { get; set; }

        public int FKOgrenciID { get; set; }

        public DateTime TarihKayit { get; set; }

        [Required]
        public string JSON { get; set; }

        public bool TeslimEdildiMi { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
