namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRDersPlanAna")]
    public partial class OGRDersPlanAna
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OGRDersPlanAna()
        {
            ABSBasariNot = new HashSet<ABSBasariNot>();
            ABSDegerlendirmeKural = new HashSet<ABSDegerlendirmeKural>();
            ABSDokuman = new HashSet<ABSDokuman>();
            ABSOgrenciNot = new HashSet<ABSOgrenciNot>();
            ABSPaylar = new HashSet<ABSPaylar>();
            ABSPaylar1 = new HashSet<ABSPaylar>();
            ABSYayinlananBasariNotlar = new HashSet<ABSYayinlananBasariNotlar>();
            ABSYayinlananPayNotlar = new HashSet<ABSYayinlananPayNotlar>();
            OSinavGrup = new HashSet<OSinavGrup>();
            DersAktsYukIliski = new HashSet<DersAktsYukIliski>();
            DersAlaniIliski = new HashSet<DersAlaniIliski>();
            DersBilgi = new HashSet<DersBilgi>();
            DersCikti = new HashSet<DersCikti>();
            DersHaftaIliski = new HashSet<DersHaftaIliski>();
            DersKategoriIliski = new HashSet<DersKategoriIliski>();
            DersKonu = new HashSet<DersKonu>();
            DersKoordinator = new HashSet<DersKoordinator>();
            DersYeterlilikIliski = new HashSet<DersYeterlilikIliski>();
            YazOkuluDersler = new HashSet<YazOkuluDersler>();
            OGRDersGrup = new HashSet<OGRDersGrup>();
            OGRDersPlan = new HashSet<OGRDersPlan>();
            OGRDersPlanAna1 = new HashSet<OGRDersPlanAna>();
            OGRIntibak = new HashSet<OGRIntibak>();
            OGRIntibak1 = new HashSet<OGRIntibak>();
            OGRIntibak2 = new HashSet<OGRIntibak>();
            OGRDersPlanAna11 = new HashSet<OGRDersPlanAna>();
            OGRDersPlanAna12 = new HashSet<OGRDersPlanAna>();
            OGROgrenciYazilma = new HashSet<OGROgrenciYazilma>();
            OGRIntibakDers = new HashSet<OGRIntibakDers>();
            OGRMazeret = new HashSet<OGRMazeret>();
            OGROgrenciNot = new HashSet<OGROgrenciNot>();
            OGRTekDers = new HashSet<OGRTekDers>();
            ABSProjeTakvim = new HashSet<ABSProjeTakvim>();
            OGRTersineIntibak = new HashSet<OGRTersineIntibak>();
            OGRTersineIntibak1 = new HashSet<OGRTersineIntibak>();
        }

        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(500)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? InKod { get; set; }

        [StringLength(50)]
        public string ECTSKod { get; set; }

        public int? ECTSKredi { get; set; }

        public int? HesaplananECTS { get; set; }

        [StringLength(50)]
        public string DersYerFakulteKodu { get; set; }

        [StringLength(250)]
        public string DersAd { get; set; }

        [StringLength(250)]
        public string YabDersAd { get; set; }

        public int? DSaat { get; set; }

        public int? USaat { get; set; }

        public int? Kredi { get; set; }

        public int? Yil { get; set; }

        public int? EnumEBSOnay { get; set; }

        public int? EnumDersAktif { get; set; }

        public int? EnumDersTipi { get; set; }

        public int? EnumDersYer { get; set; }

        public int? FKDersPlanAnaID { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string Koordinator { get; set; }

        public int? EnumDersPlanAnaZorunlu { get; set; }

        public int? FKKoordinatorID { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public int? Lab { get; set; }

        public int? Statu { get; set; }

        public int? EnumEBSGosterim { get; set; }

        public Guid? GecmisGUID { get; set; }

        public int? FKDersPlanAnaUstID { get; set; }

        public bool Hiyerarsik { get; set; }

        public int? FKHiyerarsikKokID { get; set; }

        public int EnumKokDersTipi { get; set; }

        public int EnumDersSureTip { get; set; }

        public int EnumDegerlendirmeTipi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSBasariNot> ABSBasariNot { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSDegerlendirmeKural> ABSDegerlendirmeKural { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSDokuman> ABSDokuman { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSOgrenciNot> ABSOgrenciNot { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSPaylar> ABSPaylar { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSPaylar> ABSPaylar1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSYayinlananBasariNotlar> ABSYayinlananBasariNotlar { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSYayinlananPayNotlar> ABSYayinlananPayNotlar { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OSinavGrup> OSinavGrup { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DersAktsYukIliski> DersAktsYukIliski { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DersAlaniIliski> DersAlaniIliski { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DersBilgi> DersBilgi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DersCikti> DersCikti { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DersHaftaIliski> DersHaftaIliski { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DersKategoriIliski> DersKategoriIliski { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DersKonu> DersKonu { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DersKoordinator> DersKoordinator { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DersYeterlilikIliski> DersYeterlilikIliski { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<YazOkuluDersler> YazOkuluDersler { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRDersGrup> OGRDersGrup { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRDersPlan> OGRDersPlan { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRDersPlanAna> OGRDersPlanAna1 { get; set; }

        public virtual OGRDersPlanAna OGRDersPlanAna2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRIntibak> OGRIntibak { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRIntibak> OGRIntibak1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRIntibak> OGRIntibak2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRDersPlanAna> OGRDersPlanAna11 { get; set; }

        public virtual OGRDersPlanAna OGRDersPlanAna3 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRDersPlanAna> OGRDersPlanAna12 { get; set; }

        public virtual OGRDersPlanAna OGRDersPlanAna4 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGROgrenciYazilma> OGROgrenciYazilma { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRIntibakDers> OGRIntibakDers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRMazeret> OGRMazeret { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGROgrenciNot> OGROgrenciNot { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRTekDers> OGRTekDers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSProjeTakvim> ABSProjeTakvim { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRTersineIntibak> OGRTersineIntibak { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRTersineIntibak> OGRTersineIntibak1 { get; set; }
    }
}
