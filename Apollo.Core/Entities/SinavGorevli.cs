namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.SinavGorevli")]
    public partial class SinavGorevli
    {
        public int ID { get; set; }

        public int FKSinavMekanID { get; set; }

        public int FKPersonelID { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }

        public virtual SinavMekan SinavMekan { get; set; }
    }
}
