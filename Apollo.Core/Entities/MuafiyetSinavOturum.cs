namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("basvuru.MuafiyetSinavOturum")]
    public partial class MuafiyetSinavOturum
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MuafiyetSinavOturum()
        {
            MuafiyetSinavBasvuru = new HashSet<MuafiyetSinavBasvuru>();
        }

        public int ID { get; set; }

        public int FKMuafiyetSinavTanimID { get; set; }

        public DateTime TarihKayit { get; set; }

        public DateTime? TarihGuncelleme { get; set; }

        [Required]
        [StringLength(500)]
        public string Kullanici { get; set; }

        [Required]
        [StringLength(50)]
        public string IP { get; set; }

        public DateTime TarihSinav { get; set; }

        public int FKMuafiyetSinavSalonTanimID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MuafiyetSinavBasvuru> MuafiyetSinavBasvuru { get; set; }

        public virtual MuafiyetSinavSalonTanim MuafiyetSinavSalonTanim { get; set; }

        public virtual MuafiyetSinavTanim MuafiyetSinavTanim { get; set; }
    }
}
