namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.ABSOgrenciDanismanlik")]
    public partial class ABSOgrenciDanismanlik
    {
        public int ID { get; set; }

        public int? FKOgrenciID { get; set; }

        public int? FKDersGrupID { get; set; }

        public int? FKPersonelID { get; set; }

        [StringLength(100)]
        public string kullanici { get; set; }

        public DateTime? zaman { get; set; }

        public virtual OGRDersGrup OGRDersGrup { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }
    }
}
