namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.ABSYayinlananBasariNotlar")]
    public partial class ABSYayinlananBasariNotlar
    {
        public int ID { get; set; }

        public int? FKDersGrupID { get; set; }

        public DateTime YayinlanmaTarihi { get; set; }

        public bool Yayinlandi { get; set; }

        public string LogTakip { get; set; }

        public int? EnumDonem { get; set; }

        public int? Yil { get; set; }

        public int? FKABSDegerlendirme { get; set; }

        public bool ButunlemeMi { get; set; }

        public int? FKDersPlanAnaID { get; set; }

        public virtual ABSDegerlendirme ABSDegerlendirme { get; set; }

        public virtual OGRDersGrup OGRDersGrup { get; set; }

        public virtual OGRDersPlanAna OGRDersPlanAna { get; set; }
    }
}
