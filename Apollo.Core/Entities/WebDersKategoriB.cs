namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.WebDersKategoriB")]
    public partial class WebDersKategoriB
    {
        [Key]
        [Column(Order = 0)]
        public int InKod { get; set; }

        public int? AnaDersPlanID { get; set; }

        public int? KategoriNo { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short Yil1 { get; set; }

        [StringLength(30)]
        public string Ekleyen { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? Zaman { get; set; }
    }
}
