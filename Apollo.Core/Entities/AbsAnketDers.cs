namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("obs.AbsAnketDers")]
    public partial class AbsAnketDers
    {
        [Key]
        [Column(Order = 0)]
        public int ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FKOgrenciID { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FKDersGrupID { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(2)]
        public string GrupAd { get; set; }

        [Key]
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Yil { get; set; }

        [Key]
        [Column(Order = 5)]
        public byte Donem { get; set; }

        public byte? S1_Ders { get; set; }

        public byte? S2_Ders { get; set; }

        public byte? S3_Ders { get; set; }

        public byte? S4_Ders { get; set; }

        public byte? S5_Ders { get; set; }

        public byte? S6_Ders { get; set; }

        public byte? S7_Ders { get; set; }

        public byte? S8_Ders { get; set; }

        public byte? S1_Ogretim { get; set; }

        public byte? S2_Ogretim { get; set; }

        public byte? S3_Ogretim { get; set; }

        public byte? S4_Ogretim { get; set; }

        public byte? S5_Ogretim { get; set; }

        public byte? S6_Ogretim { get; set; }

        public byte? S7_Ogretim { get; set; }

        public byte? S8_Ogretim { get; set; }

        public byte? S9_Ogretim { get; set; }

        public byte? S10_Ogretim { get; set; }

        public byte? S1_IsYuku { get; set; }

        public byte? S2_IsYuku { get; set; }

        public byte? S3_IsYuku { get; set; }

        public byte? S4_IsYuku { get; set; }

        public byte? S5_IsYuku { get; set; }

        public byte? S6_IsYuku { get; set; }

        public byte? S7_IsYuku { get; set; }

        public byte? S8_IsYuku { get; set; }

        public byte? S9_IsYuku { get; set; }

        public byte? S10_IsYuku { get; set; }

        public byte? S11_IsYuku { get; set; }

        public byte? S12_IsYuku { get; set; }

        public byte? S13_IsYuku { get; set; }

        public byte? S14_IsYuku { get; set; }

        public byte? S15_IsYuku { get; set; }

        [Key]
        [Column(Order = 6, TypeName = "smalldatetime")]
        public DateTime Tarih { get; set; }
    }
}
