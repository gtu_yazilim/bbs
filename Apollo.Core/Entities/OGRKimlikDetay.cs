namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRKimlikDetay")]
    public partial class OGRKimlikDetay
    {
        public int ID { get; set; }

        public int FKOgrenciID { get; set; }

        public int? EnumKadroTip { get; set; }

        public bool TezAsamasinda { get; set; }

        [StringLength(100)]
        public string BilimDali { get; set; }

        [StringLength(100)]
        public string BabaMeslek { get; set; }

        [StringLength(100)]
        public string AnneMeslek { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public DateTime? Zaman { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
