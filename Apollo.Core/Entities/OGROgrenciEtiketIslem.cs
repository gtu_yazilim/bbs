namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGROgrenciEtiketIslem")]
    public partial class OGROgrenciEtiketIslem
    {
        public int ID { get; set; }

        public int EnumOgrenciEtiketIslem { get; set; }

        public int FKOgrenciEtiketTanimID { get; set; }

        public bool Silindi { get; set; }

        public DateTime TarihKayit { get; set; }

        [Required]
        [StringLength(50)]
        public string IP { get; set; }

        [Required]
        [StringLength(50)]
        public string Kullanici { get; set; }

        public DateTime? TarihGuncelleme { get; set; }

        public virtual OGRTNMOgrenciEtiket OGRTNMOgrenciEtiket { get; set; }
    }
}
