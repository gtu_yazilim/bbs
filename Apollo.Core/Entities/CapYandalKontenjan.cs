namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("basvuru.CapYandalKontenjan")]
    public partial class CapYandalKontenjan
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CapYandalKontenjan()
        {
            CapYandalTercih = new HashSet<CapYandalTercih>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string Kullanici { get; set; }

        public DateTime TarihKayit { get; set; }

        public int FKCapYandalTanimID { get; set; }

        public int Yil { get; set; }

        public int Donem { get; set; }

        public int Adet { get; set; }

        public bool Silindi { get; set; }

        public bool KayitAcik { get; set; }

        public virtual OGRCapYandalTanim OGRCapYandalTanim { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CapYandalTercih> CapYandalTercih { get; set; }
    }
}
