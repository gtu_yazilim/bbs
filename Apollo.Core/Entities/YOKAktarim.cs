namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.YOKAktarim")]
    public partial class YOKAktarim
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public YOKAktarim()
        {
            YOKBildirilenOgrenciler = new HashSet<YOKBildirilenOgrenciler>();
            YoksisAktarimDurumu = new HashSet<YoksisAktarimDurumu>();
        }

        public int ID { get; set; }

        public long TCKimlik { get; set; }

        [StringLength(50)]
        public string PasaportNo { get; set; }

        [StringLength(50)]
        public string KampKarti { get; set; }

        public int? DOYKM { get; set; }

        public long BirimID { get; set; }

        public int? GirisTuru { get; set; }

        [StringLength(50)]
        public string GirisPuani { get; set; }

        public int? GirisPuanTuru { get; set; }

        public int? AyrilmaNedeni { get; set; }

        [StringLength(50)]
        public string AyrilmaTarihi { get; set; }

        public int? Sinifi { get; set; }

        public int? AktifDonemNo { get; set; }

        [StringLength(50)]
        public string KayitTarihi { get; set; }

        public int? OgrencilikStatusu { get; set; }

        public decimal? DiplomaNotu { get; set; }

        public int? DiplomaNotSistemi { get; set; }

        [StringLength(50)]
        public string DiplomaNo { get; set; }

        public int? OgrencilikHakkiVarMi { get; set; }

        [StringLength(50)]
        public string OgrencilikHakkiniKaybettigiTarih { get; set; }

        public int? EngelTuru { get; set; }

        public int? EngelOrani { get; set; }

        public int? SehitGaziYakini { get; set; }

        public int? Yil { get; set; }

        public int? Donem { get; set; }

        public DateTime? SonHareketTarihi { get; set; }

        public bool? AktarildiMi { get; set; }

        public DateTime? AktarimZamani { get; set; }

        [StringLength(500)]
        public string AktarimMesaji { get; set; }

        [StringLength(30)]
        public string AktarimOlusturmaNedenleri { get; set; }

        [StringLength(11)]
        public string OgrenciNo { get; set; }

        public int? FKOgrenciID { get; set; }

        public DateTime Zaman { get; set; }

        public int EnumYokServisiMetodu { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool DiplomaNotuFieldSpecified { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Akts { get; set; }

        public bool AktsFieldSpecified { get; set; }

        public int HazirlikSinifiOkudumu { get; set; }

        public int KacDonemHazirlikSinifiOkudu { get; set; }

        public long? YatayBirimId { get; set; }

        [StringLength(50)]
        public string DiplomaAd { get; set; }

        [StringLength(50)]
        public string DiplomaSoyad { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<YOKBildirilenOgrenciler> YOKBildirilenOgrenciler { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<YoksisAktarimDurumu> YoksisAktarimDurumu { get; set; }
    }
}
