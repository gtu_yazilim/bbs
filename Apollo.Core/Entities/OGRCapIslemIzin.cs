namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRCapIslemIzin")]
    public partial class OGRCapIslemIzin
    {
        public int Id { get; set; }

        public bool? Izin { get; set; }
    }
}
