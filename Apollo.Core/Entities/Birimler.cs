namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("gen.Birimler")]
    public partial class Birimler
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Birimler()
        {
            ABSBirimSorumlu = new HashSet<ABSBirimSorumlu>();
            ABSRolYetki = new HashSet<ABSRolYetki>();
            ABSRolYetki1 = new HashSet<ABSRolYetki>();
            ABSSistemTarihleri = new HashSet<ABSSistemTarihleri>();
            ABSSistemTarihleri1 = new HashSet<ABSSistemTarihleri>();
            ABSYetkiliKullanici = new HashSet<ABSYetkiliKullanici>();
            ABSYetkiliKullanici1 = new HashSet<ABSYetkiliKullanici>();
            UKKullanicilar = new HashSet<UKKullanicilar>();
            UKKullanicilar1 = new HashSet<UKKullanicilar>();
            DegisimKayit = new HashSet<DegisimKayit>();
            BolumBilgi = new HashSet<BolumBilgi>();
            BolumYeterlilik = new HashSet<BolumYeterlilik>();
            BolumYeterlilikKategoriIliski = new HashSet<BolumYeterlilikKategoriIliski>();
            DersPlanAnaEtiket = new HashSet<DersPlanAnaEtiket>();
            DersPlanEtiket = new HashSet<DersPlanEtiket>();
            DersYeterlilikIliski = new HashSet<DersYeterlilikIliski>();
            TemelAlanBolumIliski = new HashSet<TemelAlanBolumIliski>();
            BirimKurulusDayanagi = new HashSet<BirimKurulusDayanagi>();
            AsaletTasdiki = new HashSet<AsaletTasdiki>();
            AsaletTasdiki1 = new HashSet<AsaletTasdiki>();
            AsaletTasdikiDetay = new HashSet<AsaletTasdikiDetay>();
            AsaletTasdikiDetay1 = new HashSet<AsaletTasdikiDetay>();
            AtanmaSureleri = new HashSet<AtanmaSureleri>();
            AtanmaSureleriDetay = new HashSet<AtanmaSureleriDetay>();
            AtanmaSureleriDetay1 = new HashSet<AtanmaSureleriDetay>();
            BirimDil = new HashSet<BirimDil>();
            Birimler1 = new HashSet<Birimler>();
            Birimler11 = new HashSet<Birimler>();
            DereceIlerletme = new HashSet<DereceIlerletme>();
            DereceIlerletme1 = new HashSet<DereceIlerletme>();
            DereceIlerletmeDetay = new HashSet<DereceIlerletmeDetay>();
            DereceIlerletmeDetay1 = new HashSet<DereceIlerletmeDetay>();
            DisGorevlendirme = new HashSet<DisGorevlendirme>();
            EgitimBilgisi = new HashSet<EgitimBilgisi>();
            FiiliGorevYeri = new HashSet<FiiliGorevYeri>();
            FiiliGorevYeri1 = new HashSet<FiiliGorevYeri>();
            FiiliGorevYeri2 = new HashSet<FiiliGorevYeri>();
            FiiliGorevYeri3 = new HashSet<FiiliGorevYeri>();
            Hareket = new HashSet<Hareket>();
            Hareket1 = new HashSet<Hareket>();
            Hareket2 = new HashSet<Hareket>();
            Hareket3 = new HashSet<Hareket>();
            Hareket4 = new HashSet<Hareket>();
            Hareket5 = new HashSet<Hareket>();
            Hareket6 = new HashSet<Hareket>();
            Hareket7 = new HashSet<Hareket>();
            IdariGorev = new HashSet<IdariGorev>();
            IdariGorev1 = new HashSet<IdariGorev>();
            IdariGorev2 = new HashSet<IdariGorev>();
            IdariGorev3 = new HashSet<IdariGorev>();
            Izin = new HashSet<Izin>();
            IzinYOKDetay = new HashSet<IzinYOKDetay>();
            IzinYOKDetay1 = new HashSet<IzinYOKDetay>();
            IzinYOKDetay2 = new HashSet<IzinYOKDetay>();
            IzinYOKDetay3 = new HashSet<IzinYOKDetay>();
            IzinYOKDetay4 = new HashSet<IzinYOKDetay>();
            IzinYOKDetay5 = new HashSet<IzinYOKDetay>();
            IzinYOKDetay6 = new HashSet<IzinYOKDetay>();
            IzinYOKDetay7 = new HashSet<IzinYOKDetay>();
            KadroBilgisi = new HashSet<KadroBilgisi>();
            KadroBilgisi1 = new HashSet<KadroBilgisi>();
            KadroBilgisi2 = new HashSet<KadroBilgisi>();
            KadroBilgisi3 = new HashSet<KadroBilgisi>();
            Mekan = new HashSet<Mekan>();
            MekanBinalar1 = new HashSet<MekanBinalar1>();
            MekanV2 = new HashSet<MekanV2>();
            OBSOnKayitAyarHazirlik = new HashSet<OBSOnKayitAyarHazirlik>();
            OBSYazilmaAyar = new HashSet<OBSYazilmaAyar>();
            OGRAkademikTakvim = new HashSet<OGRAkademikTakvim>();
            OGRBelgeSayiOnur = new HashSet<OGRBelgeSayiOnur>();
            OGRDegisim = new HashSet<OGRDegisim>();
            OGRDersGrup = new HashSet<OGRDersGrup>();
            OGRDersPlan = new HashSet<OGRDersPlan>();
            OGRDersPlan1 = new HashSet<OGRDersPlan>();
            OGRDersPlan2 = new HashSet<OGRDersPlan>();
            OGRDersPlanFiltre = new HashSet<OGRDersPlanFiltre>();
            OGRDiplomaFormat = new HashSet<OGRDiplomaFormat>();
            OGREgitimBilgileri = new HashSet<OGREgitimBilgileri>();
            OGREgitimBilgileri1 = new HashSet<OGREgitimBilgileri>();
            OGREnstituKimlik = new HashSet<OGREnstituKimlik>();
            OGREnstituKomitelerJuriler = new HashSet<OGREnstituKomitelerJuriler>();
            OGREnstituKontenjan = new HashSet<OGREnstituKontenjan>();
            OGREnstituYeterlilikKomite = new HashSet<OGREnstituYeterlilikKomite>();
            OGRGeciciOgrenciNumaralari = new HashSet<OGRGeciciOgrenciNumaralari>();
            OGRHarcHesapAciklama = new HashSet<OGRHarcHesapAciklama>();
            OGRHarcMiktar = new HashSet<OGRHarcMiktar>();
            OGRHarcSonOdemeTarih = new HashSet<OGRHarcSonOdemeTarih>();
            OGRIntibakBirim = new HashSet<OGRIntibakBirim>();
            OGRKimlik = new HashSet<OGRKimlik>();
            OGRKimlik1 = new HashSet<OGRKimlik>();
            OGRKimlik2 = new HashSet<OGRKimlik>();
            OGRKimlik3 = new HashSet<OGRKimlik>();
            OGRKurulKarar = new HashSet<OGRKurulKarar>();
            OGROSYMOgrenciAktarim = new HashSet<OGROSYMOgrenciAktarim>();
            OGRRaporTanim = new HashSet<OGRRaporTanim>();
            OGRTNMBelge = new HashSet<OGRTNMBelge>();
            OGRTNMBilimDali = new HashSet<OGRTNMBilimDali>();
            OGRTNMBolumYilaGore = new HashSet<OGRTNMBolumYilaGore>();
            OGRTNMDonemBilgi = new HashSet<OGRTNMDonemBilgi>();
            OGRTNMOkulOSYMKod = new HashSet<OGRTNMOkulOSYMKod>();
            PersonelBilgisi = new HashSet<PersonelBilgisi>();
            PersonelBilgisi1 = new HashSet<PersonelBilgisi>();
            OGRTNMSaatBilgi = new HashSet<OGRTNMSaatBilgi>();
            OGRDiplomaUnvan = new HashSet<OGRDiplomaUnvan>();
            OBSOnKayit = new HashSet<OBSOnKayit>();
            PersonelBilgisi2 = new HashSet<PersonelBilgisi>();
            TerfiBilgisi = new HashSet<TerfiBilgisi>();
            TerfiBilgisi1 = new HashSet<TerfiBilgisi>();
            TerfiBilgisiDetay = new HashSet<TerfiBilgisiDetay>();
            TerfiBilgisiDetay1 = new HashSet<TerfiBilgisiDetay>();
            TNMEnstituKontenjanAdi = new HashSet<TNMEnstituKontenjanAdi>();
            TNMKullanicilar = new HashSet<TNMKullanicilar>();
        }

        public int ID { get; set; }

        public long? YoksisID { get; set; }

        [Required]
        [StringLength(300)]
        public string BirimAdi { get; set; }

        [StringLength(300)]
        public string BirimAdiIngilizce { get; set; }

        public int? UstBirimID { get; set; }

        public bool Aktif { get; set; }

        public int EnumAktiflikStatusu { get; set; }

        [StringLength(50)]
        public string BirimTuruAdi { get; set; }

        [StringLength(500)]
        public string BirimUzunAdi { get; set; }

        public int? EnumBirimTuru { get; set; }

        public int? IlKodu { get; set; }

        public int? IlceKodu { get; set; }

        [StringLength(50)]
        public string OgrenimDili { get; set; }

        public int? OgrenimSuresi { get; set; }

        [StringLength(50)]
        public string OgrenimTuru { get; set; }

        public int? EnumOgrenimTuru { get; set; }

        [StringLength(500)]
        public string AcikAdres { get; set; }

        public long? KlavuzKodu { get; set; }

        public int? EnumKategori { get; set; }

        public int? FKUniversiteID { get; set; }

        public int? FKUlkeID { get; set; }

        public int? FKYonlendirilenPrID { get; set; }

        public int? UlkeKod { get; set; }

        public int? UniversiteKod { get; set; }

        public int? FakulteKod { get; set; }

        public int? BolumKod { get; set; }

        public int? AnaBilimDaliKod { get; set; }

        [StringLength(5)]
        public string Ogr_FakulteKod { get; set; }

        [StringLength(5)]
        public string Ogr_BolumKod { get; set; }

        public int? InKod { get; set; }

        public int? Sene { get; set; }

        [StringLength(100)]
        public string AdGerman { get; set; }

        public int? AzamiSure { get; set; }

        public bool? DerslereDevamMecburiyeti { get; set; }

        [StringLength(50)]
        public string EgitimEsasi { get; set; }

        [StringLength(50)]
        public string DekanMakam { get; set; }

        [StringLength(50)]
        public string DekanMakamEng { get; set; }

        [StringLength(300)]
        public string BasilanBirimAdi { get; set; }

        [StringLength(300)]
        public string BasilanBirimAdiEng { get; set; }

        public int? EnumSeviye { get; set; }

        [StringLength(30)]
        public string BirimKodu { get; set; }

        public Guid? GrupGUID { get; set; }

        [StringLength(300)]
        public string BasilanBirimAdiKisa { get; set; }

        [StringLength(300)]
        public string BasilanBirimAdiKisaEng { get; set; }

        public int? EnstituFakulteIliskiID { get; set; }

        public int VersiyonBaslangicYil { get; set; }

        public int? VersiyonBitisYil { get; set; }

        public int? FKOncekiVersiyonID { get; set; }

        public int? OgretimTurleri { get; set; }

        public DateTime TarihKayit { get; set; }

        public DateTime? TarihGuncelleme { get; set; }

        public int? EnumDersSureTip { get; set; }

        public int? EnumTabanNotTipi { get; set; }

        public bool Silindi { get; set; }

        public int? EnumEgitimDili { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSBirimSorumlu> ABSBirimSorumlu { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSRolYetki> ABSRolYetki { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSRolYetki> ABSRolYetki1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSSistemTarihleri> ABSSistemTarihleri { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSSistemTarihleri> ABSSistemTarihleri1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSYetkiliKullanici> ABSYetkiliKullanici { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSYetkiliKullanici> ABSYetkiliKullanici1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UKKullanicilar> UKKullanicilar { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UKKullanicilar> UKKullanicilar1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DegisimKayit> DegisimKayit { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BolumBilgi> BolumBilgi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BolumYeterlilik> BolumYeterlilik { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BolumYeterlilikKategoriIliski> BolumYeterlilikKategoriIliski { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DersPlanAnaEtiket> DersPlanAnaEtiket { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DersPlanEtiket> DersPlanEtiket { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DersYeterlilikIliski> DersYeterlilikIliski { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TemelAlanBolumIliski> TemelAlanBolumIliski { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BirimKurulusDayanagi> BirimKurulusDayanagi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AsaletTasdiki> AsaletTasdiki { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AsaletTasdiki> AsaletTasdiki1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AsaletTasdikiDetay> AsaletTasdikiDetay { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AsaletTasdikiDetay> AsaletTasdikiDetay1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AtanmaSureleri> AtanmaSureleri { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AtanmaSureleriDetay> AtanmaSureleriDetay { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AtanmaSureleriDetay> AtanmaSureleriDetay1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BirimDil> BirimDil { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Birimler> Birimler1 { get; set; }

        public virtual Birimler Birimler2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Birimler> Birimler11 { get; set; }

        public virtual Birimler Birimler3 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DereceIlerletme> DereceIlerletme { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DereceIlerletme> DereceIlerletme1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DereceIlerletmeDetay> DereceIlerletmeDetay { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DereceIlerletmeDetay> DereceIlerletmeDetay1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DisGorevlendirme> DisGorevlendirme { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EgitimBilgisi> EgitimBilgisi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FiiliGorevYeri> FiiliGorevYeri { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FiiliGorevYeri> FiiliGorevYeri1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FiiliGorevYeri> FiiliGorevYeri2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FiiliGorevYeri> FiiliGorevYeri3 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Hareket> Hareket { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Hareket> Hareket1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Hareket> Hareket2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Hareket> Hareket3 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Hareket> Hareket4 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Hareket> Hareket5 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Hareket> Hareket6 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Hareket> Hareket7 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IdariGorev> IdariGorev { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IdariGorev> IdariGorev1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IdariGorev> IdariGorev2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IdariGorev> IdariGorev3 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Izin> Izin { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IzinYOKDetay> IzinYOKDetay { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IzinYOKDetay> IzinYOKDetay1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IzinYOKDetay> IzinYOKDetay2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IzinYOKDetay> IzinYOKDetay3 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IzinYOKDetay> IzinYOKDetay4 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IzinYOKDetay> IzinYOKDetay5 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IzinYOKDetay> IzinYOKDetay6 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IzinYOKDetay> IzinYOKDetay7 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<KadroBilgisi> KadroBilgisi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<KadroBilgisi> KadroBilgisi1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<KadroBilgisi> KadroBilgisi2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<KadroBilgisi> KadroBilgisi3 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Mekan> Mekan { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MekanBinalar1> MekanBinalar1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MekanV2> MekanV2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OBSOnKayitAyarHazirlik> OBSOnKayitAyarHazirlik { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OBSYazilmaAyar> OBSYazilmaAyar { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRAkademikTakvim> OGRAkademikTakvim { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRBelgeSayiOnur> OGRBelgeSayiOnur { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRDegisim> OGRDegisim { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRDersGrup> OGRDersGrup { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRDersPlan> OGRDersPlan { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRDersPlan> OGRDersPlan1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRDersPlan> OGRDersPlan2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRDersPlanFiltre> OGRDersPlanFiltre { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRDiplomaFormat> OGRDiplomaFormat { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREgitimBilgileri> OGREgitimBilgileri { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREgitimBilgileri> OGREgitimBilgileri1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituKimlik> OGREnstituKimlik { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituKomitelerJuriler> OGREnstituKomitelerJuriler { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituKontenjan> OGREnstituKontenjan { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituYeterlilikKomite> OGREnstituYeterlilikKomite { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRGeciciOgrenciNumaralari> OGRGeciciOgrenciNumaralari { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRHarcHesapAciklama> OGRHarcHesapAciklama { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRHarcMiktar> OGRHarcMiktar { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRHarcSonOdemeTarih> OGRHarcSonOdemeTarih { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRIntibakBirim> OGRIntibakBirim { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRKimlik> OGRKimlik { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRKimlik> OGRKimlik1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRKimlik> OGRKimlik2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRKimlik> OGRKimlik3 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRKurulKarar> OGRKurulKarar { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGROSYMOgrenciAktarim> OGROSYMOgrenciAktarim { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRRaporTanim> OGRRaporTanim { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRTNMBelge> OGRTNMBelge { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRTNMBilimDali> OGRTNMBilimDali { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRTNMBolumYilaGore> OGRTNMBolumYilaGore { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRTNMDonemBilgi> OGRTNMDonemBilgi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRTNMOkulOSYMKod> OGRTNMOkulOSYMKod { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PersonelBilgisi> PersonelBilgisi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PersonelBilgisi> PersonelBilgisi1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRTNMSaatBilgi> OGRTNMSaatBilgi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGRDiplomaUnvan> OGRDiplomaUnvan { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OBSOnKayit> OBSOnKayit { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PersonelBilgisi> PersonelBilgisi2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TerfiBilgisi> TerfiBilgisi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TerfiBilgisi> TerfiBilgisi1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TerfiBilgisiDetay> TerfiBilgisiDetay { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TerfiBilgisiDetay> TerfiBilgisiDetay1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TNMEnstituKontenjanAdi> TNMEnstituKontenjanAdi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TNMKullanicilar> TNMKullanicilar { get; set; }

        public virtual TNMKurumBilgisi TNMKurumBilgisi { get; set; }
    }
}
