namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("basvuru.KitapBursu")]
    public partial class KitapBursu
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public KitapBursu()
        {
            KitapBursuKardesBilgisi = new HashSet<KitapBursuKardesBilgisi>();
        }

        public int ID { get; set; }

        public DateTime TarihKayit { get; set; }

        [Required]
        [StringLength(250)]
        public string Kullanici { get; set; }

        public DateTime? TarihGuncelleme { get; set; }

        public int BasvurmakIstiyor { get; set; }

        public int AnneSag { get; set; }

        [StringLength(500)]
        public string AnneAdSoyad { get; set; }

        [StringLength(50)]
        public string AnneCepTel { get; set; }

        [StringLength(250)]
        public string AnneEposta { get; set; }

        [StringLength(500)]
        public string AnneMeslek { get; set; }

        [StringLength(50)]
        public string AnneGelir { get; set; }

        public int AnneEgitimDurumu { get; set; }

        public int BabaSag { get; set; }

        [StringLength(500)]
        public string BabaAdSoyad { get; set; }

        [StringLength(50)]
        public string BabaCepTel { get; set; }

        [StringLength(250)]
        public string BabaEposta { get; set; }

        [StringLength(500)]
        public string BabaMeslek { get; set; }

        [StringLength(50)]
        public string BabaGelir { get; set; }

        public int BabaEgitimDurumu { get; set; }

        public bool AnneBabaBirlikte { get; set; }

        public bool BaskaYerdenBurs { get; set; }

        [StringLength(500)]
        public string AcilDurumAdSoyad { get; set; }

        [StringLength(50)]
        public string AcilDurumCep { get; set; }

        [StringLength(250)]
        public string AcilDurumEposta { get; set; }

        [StringLength(250)]
        public string AcilDurumYakinlik { get; set; }

        public int KardesSayisi { get; set; }

        public int FKOgrenciID { get; set; }

        [StringLength(500)]
        public string BaskaYerdenBursYeri { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<KitapBursuKardesBilgisi> KitapBursuKardesBilgisi { get; set; }
    }
}
