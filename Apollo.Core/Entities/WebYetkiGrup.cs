namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.WebYetkiGrup")]
    public partial class WebYetkiGrup
    {
        [Key]
        public int InKod { get; set; }

        [StringLength(50)]
        public string YetkiliGrupAd { get; set; }

        [StringLength(50)]
        public string Ekleyen { get; set; }

        public DateTime? Zaman { get; set; }
    }
}
