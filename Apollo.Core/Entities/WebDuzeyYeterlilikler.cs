namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ebs.WebDuzeyYeterlilikler")]
    public partial class WebDuzeyYeterlilikler
    {
        [Key]
        public int InKod { get; set; }

        public int? Duzey { get; set; }

        public int? Kategori { get; set; }

        [Column(TypeName = "text")]
        public string Aciklama { get; set; }

        public byte? Sira { get; set; }

        public int? FormDil { get; set; }

        [StringLength(50)]
        public string Ekleyen { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? Zaman { get; set; }
    }
}
