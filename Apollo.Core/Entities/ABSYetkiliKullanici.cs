namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.ABSYetkiliKullanici")]
    public partial class ABSYetkiliKullanici
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ABSYetkiliKullanici()
        {
            ABSYetki = new HashSet<ABSYetki>();
        }

        public int ID { get; set; }

        public int? FKPersonelID { get; set; }

        public int? FKFakulteID { get; set; }

        public int? FKBolumID { get; set; }

        public int? FKYetkiRolID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ABSYetki> ABSYetki { get; set; }

        public virtual ABSYetkliRolleri ABSYetkliRolleri { get; set; }

        public virtual Birimler Birimler { get; set; }

        public virtual Birimler Birimler1 { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }
    }
}
