namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGRYazilmaDanismanOnay")]
    public partial class OGRYazilmaDanismanOnay
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OGRYazilmaDanismanOnay()
        {
            OGROgrenciYazilma = new HashSet<OGROgrenciYazilma>();
        }

        public int ID { get; set; }

        public DateTime TarihKayit { get; set; }

        public DateTime? TarihOnay { get; set; }

        public int FKOgrenciID { get; set; }

        public int EnumDanismanOnayDurumu { get; set; }

        public int FKPersonelID { get; set; }

        public string Aciklama { get; set; }

        public int Yil { get; set; }

        public int EnumDonem { get; set; }

        public DateTime TarihSonOnay { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGROgrenciYazilma> OGROgrenciYazilma { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }
    }
}
