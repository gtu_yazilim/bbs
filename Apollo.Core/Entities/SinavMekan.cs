namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.SinavMekan")]
    public partial class SinavMekan
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SinavMekan()
        {
            SinavGorevli = new HashSet<SinavGorevli>();
        }

        public int ID { get; set; }

        public int FKMekanID { get; set; }

        public int FKSinavTavimID { get; set; }

        public virtual MekanV2 MekanV2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SinavGorevli> SinavGorevli { get; set; }

        public virtual SinavTakvim SinavTakvim { get; set; }
    }
}
