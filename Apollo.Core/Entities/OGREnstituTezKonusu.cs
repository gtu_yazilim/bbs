namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGREnstituTezKonusu")]
    public partial class OGREnstituTezKonusu
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OGREnstituTezKonusu()
        {
            OGREnstituTezTizTosSinavi = new HashSet<OGREnstituTezTizTosSinavi>();
        }

        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public bool? AktarilanVeriMi { get; set; }

        public int? InKod { get; set; }

        [StringLength(512)]
        public string Konu { get; set; }

        public DateTime? TarihBaslangic { get; set; }

        [StringLength(10)]
        public string YKNo { get; set; }

        public DateTime? TarihBitis { get; set; }

        public int? FKOgrenciID { get; set; }

        public bool? GuncelMi { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public int EnumEnstituCalismaTipi { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituTezTizTosSinavi> OGREnstituTezTizTosSinavi { get; set; }
    }
}
