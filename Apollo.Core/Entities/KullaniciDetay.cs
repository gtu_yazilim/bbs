namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ytk.KullaniciDetay")]
    public partial class KullaniciDetay
    {
        public int ID { get; set; }

        public DateTime? HesapAcilisTarihi { get; set; }

        public DateTime? SifreDegistirmeTarihi { get; set; }

        public DateTime? GoogleHesapAcilisTarihi { get; set; }

        public DateTime? SonGirisTarihi { get; set; }

        public DateTime? AktivasyonTarihi { get; set; }

        public DateTime? IlkKullanimTarihi { get; set; }

        public int FKKullaniciID { get; set; }

        public int GirisDenemeSayisi { get; set; }

        public DateTime SyncLDAPTarih { get; set; }

        public DateTime SyncADTarih { get; set; }

        public DateTime SyncGoogleTarih { get; set; }

        public DateTime? KullaniciSozlesmesiOnayTarihi { get; set; }

        public DateTime? EPostaKontrolTarihi { get; set; }

        public virtual Kullanici Kullanici { get; set; }
    }
}
