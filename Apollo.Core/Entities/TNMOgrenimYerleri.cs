namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.TNMOgrenimYerleri")]
    public partial class TNMOgrenimYerleri
    {
        public int ID { get; set; }

        [Required]
        [StringLength(300)]
        public string BirimAdi { get; set; }

        public int ENUMBirimTuru { get; set; }

        public int ENUMOgrenimYeriTuru { get; set; }

        public int UstBirimID { get; set; }

        public long? BIRIM_ID { get; set; }

        public long? BAGLI_OLDUGU_BIRIM_ID { get; set; }

        public int? FKUlkeID { get; set; }

        public int? FKIlID { get; set; }

        public int? FKIlceID { get; set; }

        public virtual TNMIl TNMIl { get; set; }

        public virtual TNMIlce TNMIlce { get; set; }

        public virtual TNMUlke TNMUlke { get; set; }
    }
}
