namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("per.SGKHizmetOkul")]
    public partial class SGKHizmetOkul
    {
        public int ID { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? Zaman { get; set; }

        public int? kayitNo { get; set; }

        [StringLength(11)]
        public string Tckn { get; set; }

        public int? ogrenimDurumu { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? mezuniyetTarihi { get; set; }

        public string okulAd { get; set; }

        public string bolum { get; set; }

        [StringLength(1)]
        public string ogrenimYer { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? denklikTarihi { get; set; }

        public string denklikOkul { get; set; }

        public string denklikBolum { get; set; }

        public int? ogrenimSuresi { get; set; }

        public int? hazirlik { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? kurumOnayTarihi { get; set; }

        [StringLength(100)]
        public string kullaniciAd { get; set; }

        [StringLength(100)]
        public string sifre { get; set; }

        public int? FKPersonelBilgisiID { get; set; }

        public bool Durum { get; set; }

        public virtual PersonelBilgisi PersonelBilgisi { get; set; }
    }
}
