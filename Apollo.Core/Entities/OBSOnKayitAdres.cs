namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("obs.OBSOnKayitAdres")]
    public partial class OBSOnKayitAdres
    {
        public int ID { get; set; }

        public DateTime Tarih { get; set; }

        public int? EnumAdresTip { get; set; }

        [StringLength(200)]
        public string Adres1 { get; set; }

        [StringLength(100)]
        public string Adres2 { get; set; }

        public int? FKIlID { get; set; }

        public int? FKIlceID { get; set; }

        [StringLength(5)]
        public string PKod { get; set; }

        [StringLength(20)]
        public string Tel { get; set; }

        [StringLength(20)]
        public string Gsm { get; set; }

        public int? FKOnKayitID { get; set; }

        public virtual TNMIl TNMIl { get; set; }

        public virtual TNMIlce TNMIlce { get; set; }

        public virtual OBSOnKayit OBSOnKayit { get; set; }
    }
}
