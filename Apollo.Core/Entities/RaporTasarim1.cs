namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("rapor.RaporTasarim")]
    public partial class RaporTasarim1
    {
        public int ID { get; set; }

        public DateTime TarihKayit { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string IP { get; set; }

        public int FKRaporTanimID { get; set; }

        public DateTime? TarihSilinme { get; set; }

        public bool Aktif { get; set; }

        [Required]
        [StringLength(50)]
        public string VersiyonNo { get; set; }

        [StringLength(100)]
        public string Aciklama { get; set; }

        public bool Silindi { get; set; }

        public virtual RaporTanim RaporTanim { get; set; }
    }
}
