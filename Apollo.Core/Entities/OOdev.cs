namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.OOdev")]
    public partial class OOdev
    {
        [Key]
        [Column(Order = 0)]
        public int ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string Ad { get; set; }

        public string Aciklama { get; set; }

        public DateTime? Baslangic { get; set; }

        public DateTime? Bitis { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FKAbsPaylarID { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FKPersonelID { get; set; }

        [Key]
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MaxYuklemeSayisi { get; set; }

        [StringLength(50)]
        public string OdevDosyaKey { get; set; }

        [Key]
        [Column(Order = 5)]
        public double OdevDosyaBoyut { get; set; }

        [StringLength(10)]
        public string OdevDosyaTuru { get; set; }

        [Key]
        [Column(Order = 6)]
        public bool Yayinlandi { get; set; }

        [Key]
        [Column(Order = 7)]
        public bool OnlineOdevMi { get; set; }

        [Key]
        [Column(Order = 8)]
        public bool OrtakOdevMi { get; set; }

        [Key]
        [Column(Order = 9)]
        public bool GenelOdevMi { get; set; }

        [Key]
        [Column(Order = 10)]
        public DateTime SGTarih { get; set; }

        [Key]
        [Column(Order = 11)]
        [StringLength(30)]
        public string SGKullanici { get; set; }

        [Key]
        [Column(Order = 12)]
        [StringLength(20)]
        public string SGIP { get; set; }

        [Key]
        [Column(Order = 13)]
        public bool Silindi { get; set; }
    }
}
