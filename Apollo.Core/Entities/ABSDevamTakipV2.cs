namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("abs.ABSDevamTakipV2")]
    public partial class ABSDevamTakipV2
    {
        public int ID { get; set; }

        public int FK_DersProgramV2ID { get; set; }

        public DateTime YoklamaTarihi { get; set; }

        public int? Yil { get; set; }

        public int? Donem { get; set; }

        public int FK_OgrenciID { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string IP { get; set; }

        public bool Silindi { get; set; }

        public virtual OGRDersProgramiv2 OGRDersProgramiv2 { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }
    }
}
