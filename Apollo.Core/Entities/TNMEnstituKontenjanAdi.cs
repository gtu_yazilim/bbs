namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.TNMEnstituKontenjanAdi")]
    public partial class TNMEnstituKontenjanAdi
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TNMEnstituKontenjanAdi()
        {
            OGREnstituKontenjan = new HashSet<OGREnstituKontenjan>();
        }

        public int ID { get; set; }

        public int Yil { get; set; }

        public int Donem { get; set; }

        [Required]
        [StringLength(100)]
        public string KontenjanAdi { get; set; }

        public int FKBirimID { get; set; }

        [StringLength(250)]
        public string Aciklama { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        public DateTime? Zaman { get; set; }

        public virtual Birimler Birimler { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OGREnstituKontenjan> OGREnstituKontenjan { get; set; }
    }
}
