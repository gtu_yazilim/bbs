namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ogr.OGREnstituKomiteJuriUyeleri")]
    public partial class OGREnstituKomiteJuriUyeleri
    {
        public int ID { get; set; }

        public DateTime? Zaman { get; set; }

        [StringLength(50)]
        public string Kullanici { get; set; }

        [StringLength(50)]
        public string Makina { get; set; }

        public int? FKUyePersonelID { get; set; }

        public int? EnumUyeTipi { get; set; }

        public bool AsilMi { get; set; }

        [StringLength(100)]
        public string HariciUyeAdi { get; set; }

        [StringLength(100)]
        public string HariciUyeSoyadi { get; set; }

        [StringLength(20)]
        public string HariciUyeUnvani { get; set; }

        public int? HariciUyeFKUniversiteID { get; set; }

        [StringLength(100)]
        public string HariciUyeFakulteAdi { get; set; }

        [StringLength(100)]
        public string HariciUyeBolumAdi { get; set; }

        public int FKKomiteJuriID { get; set; }

        [StringLength(50)]
        public string Aciklama { get; set; }

        public DateTime? SGZaman { get; set; }

        [StringLength(50)]
        public string SGKullanici { get; set; }

        public virtual OGREnstituKomitelerJuriler OGREnstituKomitelerJuriler { get; set; }
    }
}
