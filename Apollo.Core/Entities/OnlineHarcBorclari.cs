namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("bnk.OnlineHarcBorclari")]
    public partial class OnlineHarcBorclari
    {
        public int ID { get; set; }

        public long TCKimlikNo { get; set; }

        public int? FKOgrenciID { get; set; }

        [Required]
        [StringLength(200)]
        public string AdSoyad { get; set; }

        public int FKBirimID { get; set; }

        [Required]
        [StringLength(11)]
        public string OgrenciNo { get; set; }

        public int Yil { get; set; }

        public int Donem { get; set; }

        public DateTime Zaman { get; set; }

        public double HarcMiktari { get; set; }

        public DateTime SonOdemeTarihi { get; set; }

        public int? FKOdemeID { get; set; }

        [StringLength(20)]
        public string Kullanici { get; set; }

        [StringLength(20)]
        public string SGKullanici { get; set; }

        public DateTime? SGZamani { get; set; }

        public bool EskiBorcVarMi { get; set; }

        public virtual OGRKimlik OGRKimlik { get; set; }

        public virtual OnlineHarcOdemeleri OnlineHarcOdemeleri { get; set; }
    }
}
