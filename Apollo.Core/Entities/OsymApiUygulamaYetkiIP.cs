namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("osym.OsymApiUygulamaYetkiIP")]
    public partial class OsymApiUygulamaYetkiIP
    {
        public int ID { get; set; }

        public int FKUygulamaID { get; set; }

        [Required]
        [StringLength(250)]
        public string IP { get; set; }

        [Required]
        [StringLength(50)]
        public string Kullanici { get; set; }

        public DateTime TarihKayit { get; set; }

        public bool Silindi { get; set; }

        public virtual OsymApiUygulamaYetki OsymApiUygulamaYetki { get; set; }
    }
}
