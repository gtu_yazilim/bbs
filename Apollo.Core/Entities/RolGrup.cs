namespace Apollo.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ytk.RolGrup")]
    public partial class RolGrup
    {
        public int ID { get; set; }

        public int FKRolID { get; set; }

        public int FKGrupID { get; set; }

        public virtual Grup Grup { get; set; }

        public virtual Rol Rol { get; set; }
    }
}
