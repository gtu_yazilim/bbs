﻿using Sabis.OICore.dto.yazilma;
using Sabis.Core;
using System;
using Sabis.EnumShared;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Apollo.Core.Entities;


// GTU EKLEME
namespace Apollo.Core.OgrenciYonetmelik
{
    public class OgrenciYonetmelikRepo
    {
        public const int TEMPORALDB_YONETMELIK_DONEM_NO_SONSUZ = 100000;
        public const int TEMPORALDB_YONETMELIK_YIL_BASLANGIC = 1900;
        public static readonly DateTime TEMPORALDB_TARIH_SONSUZ = new DateTime(3000, 1, 1);

        private static Dictionary<int, string> yonetmelikAdlari
        {
            get
            {
                if (_yonetmelikAdlari == null)
                {
                    updateDict();
                }

                return _yonetmelikAdlari;
            }
        }

        private static void updateDict()
        {
            using (Uys context = new Uys())
            {
                _yonetmelikAdlari = context.OGRTNMYonetmelik.ToDictionary(a => a.ID, a => a.YonetmelikAdi);
            }
        }

        public static EnumIslemSonuc GetirIslemSonucFailSuc(bool basarili)
        {
            return basarili ? Sabis.EnumShared.EnumIslemSonuc.Basari : EnumIslemSonuc.Hata;
        }

        private static Dictionary<int, string> _yonetmelikAdlari = null;


        public static IslemSonuc<string> GetirYonetmelikAdi(int yid)
        {
            string name;
            IslemSonuc<string> res;

            bool s = yonetmelikAdlari.TryGetValue(yid, out name);
            if (!s)
            {
                // VT ye yeni yonetmelik eklenmiş olabilir
                updateDict();
                s = yonetmelikAdlari.TryGetValue(yid, out name);
            }

            return new IslemSonuc<string>(name, GetirIslemSonucFailSuc(s));
        }

        public static IslemSonuc<int> GetirYonetmelikId(int ogrenciId)
        {
            bool s;
            int? yidNullable;
            using (Uys context = new Uys())
            {
                yidNullable = context.OgrOgrenciYonetmelik
                    .Where(a => a.FkOgrenciId == ogrenciId &&
                        a.GorunumBitis == TEMPORALDB_TARIH_SONSUZ &&
                        a.BitisDonemNo == TEMPORALDB_YONETMELIK_DONEM_NO_SONSUZ)
                    .Select(a => a.FkYonetmelikId)
                    .FirstOrDefault();
            }
            int yid = yidNullable ?? -1;
            return new IslemSonuc<int>(yid, GetirIslemSonucFailSuc(yidNullable != null));
        }

        public static IslemSonuc<string> GetirYonetmelikAdiOgrenciNoIle(int ogrenciId)
        {
            IslemSonuc<int> yidIs = GetirYonetmelikId(ogrenciId);
            bool s = false;
            string res;
            if (yidIs.EnumIslemSonuc == EnumIslemSonuc.Basari)
            {
                IslemSonuc<string> nameIs = GetirYonetmelikAdi(yidIs.Data);
                if (nameIs.EnumIslemSonuc == EnumIslemSonuc.Basari)
                {
                    s = true;
                    res = nameIs.Data;
                }
                else
                    res = $"Yonetmelik Adi Bulunamadi; Yid: {yidIs.Data.ToString()}";
            }
            else
                res = "Yönetmelik Bilgisine Ulasilamadi";
            return new IslemSonuc<string>(res, GetirIslemSonucFailSuc(s));
        }


        public static IslemSonuc<string> GetirYonetmelikOzet(int ogrenciId)
        {
            IslemSonuc<int> yidIs = GetirYonetmelikId(ogrenciId);
            bool s = false;
            string res;
            if (yidIs.EnumIslemSonuc == EnumIslemSonuc.Basari)
            {
                IslemSonuc<string> nameIs = GetirYonetmelikAdi(yidIs.Data);
                if (nameIs.EnumIslemSonuc == EnumIslemSonuc.Basari)
                {
                    s = true;
                    res = $"{yidIs.Data.ToString()} - {nameIs.Data}";
                }
                else
                    res = $"Yonetmelik Adi Bulunamadi; Yid: {yidIs.Data.ToString()}";
            }
            else
                res = "Yönetmelik Bilgisine Ulasilamadi";
            return new IslemSonuc<string>(res, GetirIslemSonucFailSuc(s));
        }


    }
}
