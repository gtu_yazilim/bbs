﻿using Sabis.Bolum.Core.ABS.DERS.DERSDETAY;
using Sabis.Core;
using Sabis.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Apollo.Core.OgrenciDers
{
    public class OgrenciNot
    {
        public static IEnumerable<EnumDTO> GetirHocaninVerebilecegiHarfNotlari(int ogrenciID, DersDetayVM dersDetay)
        {
            IslemSonuc<int> yonetmelikIS = Apollo.Core.OgrenciYonetmelik.OgrenciYonetmelikRepo.GetirYonetmelikId(ogrenciID);
            if (yonetmelikIS.EnumIslemSonuc == Sabis.EnumShared.EnumIslemSonuc.Hata)
                throw new Exception("Öğrencinin Yönetmeliğine Ulaşılamıyor");

            int yonetmelikId = yonetmelikIS.Data;

            List<Type> includeAttr = new List<Type>();
            List<Type> excldAttr = new List<Type>();

            includeAttr.Add(typeof(HocaVerebilir));

            if ((dersDetay.BolumKod.ToUpper() == "FBE" && dersDetay.DersKod == "501")
                || (dersDetay.BolumKod.ToUpper() == "SOS" && dersDetay.DersKod == "501") || (dersDetay.BolumKod.ToUpper() == "BTEC"))
            {
                List<EnumDTO> list = new List<EnumDTO>(new EnumDTO[] { new EnumDTO(EnumHarfBasari.BELIRSIZ), new EnumDTO(EnumHarfBasari.P), new EnumDTO(EnumHarfBasari.F) });

                if (dersDetay.ButunlemeMiOrijinal)
                    list.Add(new EnumDTO(EnumHarfBasari.GR));

                return list;
                //return new EnumDTO[] { new EnumDTO(EnumHarfBasari.BELIRSIZ), new EnumDTO(EnumHarfBasari.P), new EnumDTO(EnumHarfBasari.F) };
            }


            switch (dersDetay.EnumKokDersTipi)
            {
                case (int)EnumKokDersTipi.UZMANLIKALANI:
                    includeAttr.Add(typeof(TezHoca));
                    break;
                case (int)EnumKokDersTipi.ENSTITU_PROJE:
                case (int)EnumKokDersTipi.BILIMSEL_UYUM:
                case (int)EnumKokDersTipi.SEMINER:
                case (int)EnumKokDersTipi.KREDISIZ_SECMELI:
                case (int)EnumKokDersTipi.STAJ:
                    includeAttr.Add(typeof(GectiKaldiTipi));
                    break;
                default:
                    excldAttr.Add(typeof(GectiKaldiTipi));
                    excldAttr.Add(typeof(TezHoca));
                    break;
            }


            if (dersDetay.EnumOgretimSeviye == (int)EnumOgretimSeviye.LISANS)
                excldAttr.Add(typeof(LisanstaYok));
            else
                excldAttr.Add(typeof(YLisanstaYok));

            if (dersDetay.ButunlemeMiOrijinal)
                excldAttr.Add(typeof(FinalNotTipi));
            else
                excldAttr.Add(typeof(ButunlemeNotTipi));

            return Utils.ConvertEnumToListFilterByAttributes<EnumHarfBasari>(includeAttr, excldAttr, a => Utils.YonetmelikteGecerli(a, yonetmelikId));



        }
    }
}
