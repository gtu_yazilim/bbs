﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sabis.Enum;
using Sabis.Bolum.Core.ABS.DEGERLENDIRME;
using System.Configuration;

namespace NOTDEGERLENDIRME
{
    class Ortak
    {
        public static List<Istatistik> IstatistikListeDuzeltMutlak(List<Istatistik> liste)
        {
            foreach (EnumHarfMutlak item in Enum.GetValues(typeof(EnumHarfMutlak)))
            {
                if (item == EnumHarfMutlak.BELIRSIZ)
                {
                    continue;
                }
                string harf = item.ToString();
                if (!liste.Where(x => x.Harf == harf).Any())
                {
                    liste.Add(new Istatistik() { Adet = 0, Harf = harf, Yuzde = 0, EnumHarf = (int)item });
                }
            }
            liste = liste.OrderBy(x => x.Harf).ToList();
            return liste;
        }

        public static List<Istatistik> IstatistikListeDuzeltBagil(List<Istatistik> liste)
        {
            foreach (EnumHarfBasari item in Enum.GetValues(typeof(EnumHarfBasari)))
            {
                if (item == EnumHarfBasari.BELIRSIZ)
                {
                    continue;
                }
                string harf = item.ToString();
                if (!liste.Where(x => x.Harf == harf).Any())
                {
                    liste.Add(new Istatistik() { Adet = 0, Harf = harf, Yuzde = 0, EnumHarf = (int)item });
                }
            }
            liste = liste.OrderBy(x => x.Harf).ToList();
            return liste;
        }

        public static EnumHarfMutlak DonusturMutlakHarfNotu(double deger)
        {
            EnumHarfMutlak result = EnumHarfMutlak.BELIRSIZ;
            switch (EnumUniversite)
            {
                case EnumUniversite.SAU:
                    result = DonusturMutlakHarfNotuSAU(deger);
                    break;
                case EnumUniversite.Kisbu:
                    result = DonusturMutlakHarfNotuKisbu(deger);
                    break;
                default:
                    result = DonusturMutlakHarfNotuSAU(deger);
                    break;
            }
            return result;
        }

        private static EnumHarfMutlak DonusturMutlakHarfNotuSAU(double deger)
        {
            EnumHarfMutlak result = EnumHarfMutlak.BELIRSIZ;
            if (deger >= 90 && deger < 101)
            {
                result = EnumHarfMutlak.AA;
            }
            else if (deger >= 85 && deger < 90)
            {
                result = EnumHarfMutlak.BA;
            }
            else if (deger >= 80 && deger < 85)
            {
                result = EnumHarfMutlak.BB;
            }
            else if (deger >= 75 && deger < 80)
            {
                result = EnumHarfMutlak.CB;
            }
            else if (deger >= 65 && deger < 75)
            {
                result = EnumHarfMutlak.CC;
            }
            else if (deger >= 58 && deger < 65)
            {
                result = EnumHarfMutlak.DC;
            }
            else if (deger >= 50 && deger < 58)
            {
                result = EnumHarfMutlak.DD;
            }
            else if (deger < 50)
            {
                result = EnumHarfMutlak.FF;
            }
            else if (deger == 120)
            {
                result = EnumHarfMutlak.GR;
            }
            else if (deger == 130)
            {
                result = EnumHarfMutlak.DZ;
            }
            return result;
        }

        private static EnumHarfMutlak DonusturMutlakHarfNotuKisbu(double deger)
        {
            EnumHarfMutlak result = EnumHarfMutlak.BELIRSIZ;
            if (deger >= 90 && deger < 101)
            {
                result = EnumHarfMutlak.AA;
            }
            else if (deger >= 85 && deger < 90)
            {
                result = EnumHarfMutlak.BA;
            }
            else if (deger >= 80 && deger < 85)
            {
                result = EnumHarfMutlak.BB;
            }
            else if (deger >= 70 && deger < 80)
            {
                result = EnumHarfMutlak.CB;
            }
            else if (deger >= 60 && deger < 70)
            {
                result = EnumHarfMutlak.CC;
            }
            else if (deger >= 55 && deger < 60)
            {
                result = EnumHarfMutlak.DC;
            }
            else if (deger >= 50 && deger < 55)
            {
                result = EnumHarfMutlak.DD;
            }
            else if (deger >= 30 && deger < 50)
            {
                result = EnumHarfMutlak.FD;
            }
            else if (deger < 30)
            {
                result = EnumHarfMutlak.FF;
            }
            else if (deger == 120)
            {
                result = EnumHarfMutlak.GR;
            }
            else if (deger == 130)
            {
                result = EnumHarfMutlak.DZ;
            }
            return result;
        }

        public static EnumHarfBasari DonusturBasariHarfNotu(EnumHarfMutlak mutlakHarf)
        {
            EnumHarfBasari result;
            switch (mutlakHarf)
            {
                case EnumHarfMutlak.BELIRSIZ:
                    result = EnumHarfBasari.BELIRSIZ;
                    break;
                case EnumHarfMutlak.AA:
                    result = EnumHarfBasari.AA;
                    break;
                case EnumHarfMutlak.BA:
                    result = EnumHarfBasari.BA;
                    break;
                case EnumHarfMutlak.BB:
                    result = EnumHarfBasari.BB;
                    break;
                case EnumHarfMutlak.CB:
                    result = EnumHarfBasari.CB;
                    break;
                case EnumHarfMutlak.CC:
                    result = EnumHarfBasari.CC;
                    break;
                case EnumHarfMutlak.DC:
                    result = EnumHarfBasari.DC;
                    break;
                case EnumHarfMutlak.DD:
                    result = EnumHarfBasari.DD;
                    break;
                case EnumHarfMutlak.FF:
                    result = EnumHarfBasari.FF;
                    break;
                case EnumHarfMutlak.FD:
                    result = EnumHarfBasari.FD;
                    break;
                case EnumHarfMutlak.GR:
                    result = EnumHarfBasari.GR;
                    break;
                case EnumHarfMutlak.DZ:
                    result = EnumHarfBasari.DZ;
                    break;
                default:
                    result = EnumHarfBasari.BELIRSIZ;
                    break;
            }
            return result;
        }

        public static EnumHarfBasari DonusturBasariHarfNotu(double deger, double sinir, EnumDersPlanOrtalama enumOrtalama)
        {
            EnumHarfBasari result = EnumHarfBasari.BELIRSIZ;
            switch (EnumUniversite)
            {
                case EnumUniversite.SAU:
                    result = DonusturBasariHarfNotuSAU(deger, sinir, enumOrtalama);
                    break;
                case EnumUniversite.Kisbu:
                    result = DonusturBasariHarfNotuKisbu(deger, sinir, enumOrtalama);
                    break;
                default:
                    result = DonusturBasariHarfNotuSAU(deger, sinir, enumOrtalama);
                    break;
            }
            return result;
        }

        public static EnumHarfBasari DonusturBasariHarfNotuSAU(double deger, double sinir, EnumDersPlanOrtalama enumOrtalama)
        {
            EnumHarfBasari result = EnumHarfBasari.BELIRSIZ;
            if (deger >= 90 && deger < 105)
            {
                result = EnumHarfBasari.AA;
            }
            else if (deger >= 85 && deger < 90)
            {
                result = EnumHarfBasari.BA;
            }
            else if (deger >= 80 && deger < 85)
            {
                result = EnumHarfBasari.BB;
            }
            else if (deger >= 75 && deger < 80)
            {
                result = EnumHarfBasari.CB;
            }
            else if (deger >= 65 && deger < 75)
            {
                result = EnumHarfBasari.CC;
            }
            else if (deger >= 58 && deger < 65)
            {
                result = EnumHarfBasari.DC;
            }
            else if (deger >= 50 && deger < 58)
            {
                result = EnumHarfBasari.DD;
            }
            else if (deger < 50)
            {
                result = EnumHarfBasari.FF;
            }

            if (enumOrtalama == EnumDersPlanOrtalama.ORTALAMAYA_GIRMEZ)
            {
                //double sinir = 50;
                //if (Sinif == OgrenciSinif.Lisans)
                //{
                //    sinir = 50;
                //}
                //else if (Sinif == OgrenciSinif.YuksekLisans)
                //{
                //    sinir = 65;
                //}
                //else if (Sinif == OgrenciSinif.Doktora)
                //{
                //    sinir = 75;
                //}

                if (deger < sinir)
                {
                    result = EnumHarfBasari.YZ;
                }
                else if (deger >= sinir && deger <= 105)
                {
                    result = EnumHarfBasari.YT;
                }
            }

            if (deger == 120)
            {
                result = EnumHarfBasari.GR;
            }
            else if (deger == 130)
            {
                result = EnumHarfBasari.DZ;
            }
            return result;
        }

        public static EnumHarfBasari DonusturBasariHarfNotuKisbu(double deger, double sinir, EnumDersPlanOrtalama enumOrtalama)
        {
            EnumHarfBasari result = EnumHarfBasari.BELIRSIZ;
            if (deger >= 90 && deger < 105)
            {
                result = EnumHarfBasari.AA;
            }
            else if (deger >= 85 && deger < 90)
            {
                result = EnumHarfBasari.BA;
            }
            else if (deger >= 80 && deger < 85)
            {
                result = EnumHarfBasari.BB;
            }
            else if (deger >= 70 && deger < 80)
            {
                result = EnumHarfBasari.CB;
            }
            else if (deger >= 60 && deger < 70)
            {
                result = EnumHarfBasari.CC;
            }
            else if (deger >= 55 && deger < 60)
            {
                result = EnumHarfBasari.DC;
            }
            else if (deger >= 50 && deger < 55)
            {
                result = EnumHarfBasari.DD;
            }
            else if (deger >= 30 && deger < 50)
            {
                result = EnumHarfBasari.FD;
            }
            else if (deger < 30)
            {
                result = EnumHarfBasari.FF;
            }

            if (enumOrtalama == EnumDersPlanOrtalama.ORTALAMAYA_GIRMEZ)
            {
                //double sinir = 50;
                //if (Sinif == OgrenciSinif.Lisans)
                //{
                //    sinir = 50;
                //}
                //else if (Sinif == OgrenciSinif.YuksekLisans)
                //{
                //    sinir = 65;
                //}
                //else if (Sinif == OgrenciSinif.Doktora)
                //{
                //    sinir = 75;
                //}

                if (deger < sinir)
                {
                    result = EnumHarfBasari.YZ;
                }
                else if (deger >= sinir && deger <= 105)
                {
                    result = EnumHarfBasari.YT;
                }
            }

            if (deger == 120)
            {
                result = EnumHarfBasari.GR;
            }
            else if (deger == 130)
            {
                result = EnumHarfBasari.DZ;
            }
            return result;
        }

        public static Sabis.Enum.EnumUniversite EnumUniversite
        {
            get
            {
                Sabis.Enum.EnumUniversite result = Sabis.Enum.EnumUniversite.SAU;
                if (ConfigurationManager.AppSettings["EnumUniversite"] == "SAU")
                {
                    result = Sabis.Enum.EnumUniversite.SAU;
                }
                else if (ConfigurationManager.AppSettings["EnumUniversite"] == "Kisbu")
                {
                    result = Sabis.Enum.EnumUniversite.Kisbu;
                }
                return result;
            }
        }
    }

    public static class Extensions
    {
        public static double StdDev(this IEnumerable<double> values)
        {
            double ret = 0;
            int count = values.Count();
            if (count > 1)
            {
                //Ortalama hesapla
                double avg = values.Average();

                //toplamlarını al (value-avg)^2
                double sum = values.Sum(d => (d - avg) * (d - avg));

                //Yılmaz Özkan ilgili yazısında bunun 30 dan küçük öğrenciler için 1 eksik olmasını yazdı
                if (count < 30)
                {
                    count--;
                }

                //Karekökünü al
                ret = Math.Sqrt(sum / count);
            }
            return ret;
        }
    }
}
