﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sabis.Bolum.Core.ABS.DEGERLENDIRME;
using Sabis.Enum;

namespace NOTDEGERLENDIRME
{
    public class Degerlendirme : Sabis.Bolum.Core.ABS.DEGERLENDIRME.CiktiDTO
    {
        public Degerlendirme(List<OgrenciPayveNotBilgileriDTO> _liste, double? _ustDeger, double? _bagilAritmetikOrtalama = null, EnumDersPlanOrtalama _enumOrtalama = EnumDersPlanOrtalama.BELIRSIZ, OgrenciSinif _sinif = OgrenciSinif.Lisans, bool hatalariGec = false, DegerlendirmeKural kural = null)
        {
            Butunleme = false;
            HatalariGec = hatalariGec;
            DegerlendirmeKural = kural;
            if (HatalariGec == false)
            {
                if (_ustDeger > 100)
                {
                    throw new DegerlendirmeException("Üst değer 100'den büyük olamaz!");
                }
                if (_bagilAritmetikOrtalama > 100)
                {
                    throw new DegerlendirmeException("Bağıl aritmetik ortalama 100'den büyük olamaz!");
                }
            }
            try
            {
                List<OgrNot> list = new List<OgrNot>();
                foreach (OgrenciPayveNotBilgileriDTO ogr in _liste)
                {

                    //Ortalama hesabı
                    //Dönem içini hesapla
                    double donemIciOrt = 0;
                    double notDonemIci = 0;

                    donemIciOrt = ogr.listPaylarVeNotlar.Where(x => (int)x.EnumCalismaTip < 10).Where(x => x.NotDeger.HasValue).Where(x => x.NotDeger <= 100).Sum(x => (double)(x.NotDeger * x.Yuzde) / (double)100);
                    //Donem içinin toplama etkisini hesapla
                    double mutlakOrt = 0;
                    mutlakOrt = (ogr.listPaylarVeNotlar.First(x => x.EnumCalismaTip == EnumCalismaTip.YilIcininBasariya).Yuzde * donemIciOrt) / 100;
                    notDonemIci = mutlakOrt;
                    //Finali ekle
                    double notFinal = 0;
                    PaylarVeNotlarDTO final = ogr.listPaylarVeNotlar.First(x => x.EnumCalismaTip == EnumCalismaTip.Final);
                    if (final.NotDeger.HasValue)
                    {
                        notFinal = final.NotDeger.Value;
                        if (final.NotDeger <= 100)
                        {
                            mutlakOrt += (double)(final.NotDeger * final.Yuzde) / (double)100;
                        }
                        else
                        {
                            mutlakOrt = final.NotDeger.Value;
                        }
                    }
                    else
                    {
                        mutlakOrt = 0;
                    }
                    OgrNot notEleman = new OgrNot();
                    notEleman.AdSoyad = ogr.AdSoyad;
                    notEleman.GrupID = ogr.DersGrupID.Value;
                    notEleman.DersPlanAnaID = ogr.DersPlanAnaID;
                    notEleman.DersPlanID = ogr.DersPlanID;
                    notEleman.Not = mutlakOrt;
                    notEleman.Numara = ogr.Numara;
                    notEleman.OgrID = ogr.OgrenciID;
                    notEleman.NotFinal = notFinal;
                    notEleman.NotYilIci = notDonemIci;
                    notEleman.ButunlemeMi = false;
                    list.Add(notEleman);
                }
                Degerlendirme2(list, _ustDeger, _bagilAritmetikOrtalama, _enumOrtalama, _sinif);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Bütünleme hesabı için
        /// </summary>
        /// <param name="_liste"></param>
        /// <param name="_ustDeger"></param>
        /// <param name="_bagilAritmetikOrtalama"></param>
        /// <param name="_aritmetikOrtalama"></param>
        /// <param name="_standartSapma"></param>
        /// <param name="_enBuyukNot"></param>
        /// <param name="_enumOrtalama"></param>
        /// <param name="_sinif"></param>
        public Degerlendirme(List<OgrenciPayveNotBilgileriDTO> _liste, double _ustDeger, double _bagilAritmetikOrtalama, double _aritmetikOrtalama, double _standartSapma, double _enBuyukNot, EnumDersPlanOrtalama _enumOrtalama = EnumDersPlanOrtalama.BELIRSIZ, OgrenciSinif _sinif = OgrenciSinif.Lisans, bool hatalariGec = false, DegerlendirmeKural kural = null)
        {
            Butunleme = true;
            HatalariGec = hatalariGec;
            DegerlendirmeKural = kural;
            if (HatalariGec == false)
            {
                if (_ustDeger > 100)
                {
                    throw new DegerlendirmeException("Üst değer 100'den büyük olamaz!");
                }
                if (_bagilAritmetikOrtalama > 100)
                {
                    throw new DegerlendirmeException("Bağıl aritmetik ortalama 100'den büyük olamaz!");
                }
            }
            Ortalama = _aritmetikOrtalama;
            StandartSapma = _standartSapma;
            EnBuyukNot = _enBuyukNot;
            try
            {
                List<OgrNot> list = new List<OgrNot>();
                foreach (OgrenciPayveNotBilgileriDTO ogr in _liste)
                {

                    //Ortalama hesabı
                    //Dönem içini hesapla
                    double donemIciOrt = 0;
                    double notDonemIci = 0;
                    donemIciOrt = ogr.listPaylarVeNotlar.Where(x => (int)x.EnumCalismaTip < 10).Where(x => x.NotDeger.HasValue).Where(x => x.NotDeger <= 100).Sum(x => (double)(x.NotDeger * x.Yuzde) / (double)100);
                    //Donem içinin toplama etkisini hesapla
                    double mutlakOrt = 0;
                    mutlakOrt = (ogr.listPaylarVeNotlar.First(x => x.EnumCalismaTip == EnumCalismaTip.YilIcininBasariya).Yuzde * donemIciOrt) / 100;
                    notDonemIci = mutlakOrt;
                    //Finali ekle
                    double notFinal = 0;
                    PaylarVeNotlarDTO final = ogr.listPaylarVeNotlar.First(x => x.EnumCalismaTip == EnumCalismaTip.Butunleme);
                    if (final.NotDeger.HasValue)
                    {
                        notFinal = final.NotDeger.Value;
                        if (final.NotDeger <= 100)
                        {
                            mutlakOrt += (double)(final.NotDeger * final.Yuzde) / (double)100;
                        }
                        else
                        {
                            mutlakOrt = final.NotDeger.Value;
                        }
                    }
                    else
                    {
                        mutlakOrt = 0;
                    }
                    OgrNot notEleman = new OgrNot();
                    notEleman.AdSoyad = ogr.AdSoyad;
                    notEleman.GrupID = ogr.DersGrupID.Value;
                    notEleman.DersPlanAnaID = ogr.DersPlanAnaID;
                    notEleman.DersPlanID = ogr.DersPlanID;
                    notEleman.Not = mutlakOrt;
                    notEleman.Numara = ogr.Numara;
                    notEleman.OgrID = ogr.OgrenciID;
                    notEleman.ButunlemeMi = ogr.ButunlemeMi;
                    notEleman.NotYilIci = notDonemIci;
                    notEleman.NotFinal = notFinal;
                    notEleman.ButunlemeMi = true;
                    list.Add(notEleman);
                }
                Degerlendirme2(list, _ustDeger, _bagilAritmetikOrtalama, _enumOrtalama, _sinif);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Degerlendirme2(List<OgrNot> _liste, double? _ustDeger, double? _bagilAritmetikOrtalama = null, EnumDersPlanOrtalama _enumOrtalama = EnumDersPlanOrtalama.BELIRSIZ, OgrenciSinif _sinif = OgrenciSinif.Lisans)
        {
            if (HatalariGec == false)
            {
                if (_liste.Count == 0)
                {
                    throw new DegerlendirmeException("Boş not listesi hesaplanamaz!");
                }
            }
            NotList = _liste;
            if (_sinif == OgrenciSinif.Lisans)
            {
                MutlakDegerlendir = false;
            }
            else
            {
                MutlakDegerlendir = true;
            }

            if (Ortak.EnumUniversite == EnumUniversite.Kisbu)
            {
                MutlakDegerlendir = true;
            }

            Sinif = _sinif;
            MinNot = 40;
            if (!Butunleme)
            {
                EnBuyukNot = 0;
                OrtalamaHesap();
                StandartSapmaHesap();
            }

            if (!_bagilAritmetikOrtalama.HasValue)
            {
                _bagilAritmetikOrtalama = Ortalama;
                BagilAritmetikOrtalama = Ortalama;
            }
            else if (_bagilAritmetikOrtalama.Value == 0)
            {
                BagilAritmetikOrtalama = Ortalama;
                _bagilAritmetikOrtalama = Ortalama;
            }
            else
            {
                BagilAritmetikOrtalama = _bagilAritmetikOrtalama.Value;
            }
            if (HatalariGec == false)
            {
                if (Math.Round(BagilAritmetikOrtalama, 3) < Math.Round(Ortalama, 3))
                {
                    throw new DegerlendirmeException("Bağıl aritmetik ortalama aritmetik ortalamadan düşük olamaz!");
                }
            }
            if (_ustDeger.HasValue)
            {
                UstDeger = _ustDeger.Value;
            }
            else
            {
                UstDeger = _liste.Max(x => x.Not);
            }
            if (!Butunleme)
            {
                if (HatalariGec == false)
                {
                    double maxMutOrt = 0;
                    if (_liste.Any(x => x.Not <= 100))
                    {
                        maxMutOrt = _liste.Where(x => x.Not <= 100).Max(x => x.Not);
                    }

                    if (UstDeger < maxMutOrt && !MutlakDegerlendir)
                    {
                        throw new DegerlendirmeException("Üst değer en yüksek mutlak ortalamadan düşük olamaz!");
                    }
                }
            }

            BagilAritmetikOrtalama = _bagilAritmetikOrtalama.Value;
            EnumOrtalama = _enumOrtalama;
        }

        public List<OgrNot> Hesapla()
        {
            if (!Butunleme)
            {
                OrtalamaHesap();
                if (BagilAritmetikOrtalama == 0)
                {
                    BagilAritmetikOrtalama = Ortalama;
                }
                StandartSapmaHesap();
                if (NotList.Where(x => x.Not <= 100).Any())
                {
                    EnBuyukNot = NotList.Where(x => x.Not <= 100).Max(x => x.Not);
                    EnKucukNot = NotList.Where(x => x.Not <= 100).Min(x => x.Not);
                }
            }


            double zDeger = 0;
            if (StandartSapma != 0)
            {
                zDeger = (UstDeger - BagilAritmetikOrtalama) / ((EnBuyukNot - Ortalama) / StandartSapma);
            }

            double minYilIci = 0;
            double minFinal = 0;
            double minOrtalama = MinNot;
            if (EnumOrtalama == EnumDersPlanOrtalama.ORTALAMAYA_GIRMEZ)
            {
                minOrtalama = SinirDeger;
            }
            if (DegerlendirmeKural != null)
            {
                if (DegerlendirmeKural.MinOrtalama > 0)
                {
                    minOrtalama = DegerlendirmeKural.MinOrtalama;
                }
                minYilIci = DegerlendirmeKural.MinYilIci;
                minFinal = DegerlendirmeKural.MinFinal;
            }


            foreach (OgrNot item in NotList)
            {
                item.MutlakNot = Ortak.DonusturMutlakHarfNotu(item.Not);

                //Not 40'ın altındaysa FF
                if (item.Not < minOrtalama)
                {
                    if (EnumOrtalama == EnumDersPlanOrtalama.ORTALAMAYA_GIRMEZ)
                    {
                        item.BasariHarf = EnumHarfBasari.YZ;
                    }
                    else
                    {
                        item.BasariHarf = EnumHarfBasari.FF;
                    }
                    //item.BasariHarf = EnumHarfBasari.FF;
                }
                else if (minFinal > 0 && item.NotFinal < minFinal)
                {
                    if (EnumOrtalama == EnumDersPlanOrtalama.ORTALAMAYA_GIRER)
                    {
                        item.BasariHarf = EnumHarfBasari.FF;
                    }
                    else
                    {
                        item.BasariHarf = EnumHarfBasari.YZ;
                    }
                }
                else if (minYilIci > 0 && item.NotYilIci < minYilIci)
                {
                    if (EnumOrtalama == EnumDersPlanOrtalama.ORTALAMAYA_GIRER)
                    {
                        item.BasariHarf = EnumHarfBasari.FF;
                    }
                    else
                    {
                        item.BasariHarf = EnumHarfBasari.YZ;
                    }
                }
                else if (item.Not <= 100)
                {
                    if ((Ortalama > 0 && StandartSapma > 0) && MutlakDegerlendir == false)
                    {
                        double tmpNot = item.Not;
                        //Not en büyük nottan büyük ve bütünleme ise mutlak değerlendirme yapılır.
                        if (Butunleme && tmpNot > EnBuyukNot)
                        {
                            //bütünlemede alınan not en büyük nottan büyük ise mutlak değerlendir
                            //tmpNot = EnBuyukNot;//kaldırıldı (2013-2.dönem)
                            double maxBasariNot = 0;
                            if (EnBuyukNot >= 40)
                            {
                                maxBasariNot = BagilAritmetikOrtalama + (zDeger * ((EnBuyukNot - Ortalama) / StandartSapma));
                            }
                            if (maxBasariNot > tmpNot)
                            {
                                item.BasariNot = maxBasariNot;
                                item.BasariHarf = Ortak.DonusturBasariHarfNotu(item.BasariNot, SinirDeger, EnumOrtalama);
                            }
                            else
                            {
                                item.BasariNot = item.Not;
                                item.BasariHarf = Ortak.DonusturBasariHarfNotu(item.BasariNot, SinirDeger, EnumOrtalama);
                            }
                        }
                        else
                        {
                            item.BasariNot = BagilAritmetikOrtalama + (zDeger * ((tmpNot - Ortalama) / StandartSapma));
                            item.BasariHarf = Ortak.DonusturBasariHarfNotu(item.BasariNot, SinirDeger, EnumOrtalama);
                        }
                    }
                    else //Tüm notlar aynı girilmiş ise ya da mutlak değerlendirme yapılacaksa
                    {
                        item.BasariNot = item.Not;
                        item.BasariHarf = Ortak.DonusturBasariHarfNotu(item.Not, SinirDeger, EnumOrtalama);
                    }
                }
                else
                {
                    item.MutlakNot = Ortak.DonusturMutlakHarfNotu(item.Not);
                    item.BasariNot = item.Not;
                    item.BasariHarf = Ortak.DonusturBasariHarfNotu(item.Not, SinirDeger, EnumOrtalama);
                }
                //virgülden sonra 3 hane olacak şekilde sonucu yuvarlıyoruz (Sadece gösterim için)
                item.BasariNot = Math.Round(item.BasariNot, 3);
            }

            IstatistikMutlak = NotList.GroupBy(x => x.MutlakNot).
                Select(x => new Istatistik { Harf = x.Key.ToString(), Adet = x.Count(), Yuzde = Math.Round(((double)x.Count() / (double)NotList.Count) * 100, 2), EnumHarf = (int)x.Key })
                .OrderBy(x => x.Harf).ToList();
            IstatistikMutlak = Ortak.IstatistikListeDuzeltMutlak(IstatistikMutlak);
            IstatistikBagil = NotList.GroupBy(x => x.BasariHarf)
                .Select(x => new Istatistik { Harf = x.Key.ToString(), Adet = x.Count(), Yuzde = Math.Round(((double)x.Count() / (double)NotList.Count) * 100, 2), EnumHarf = (int)x.Key })
                .OrderBy(x => x.Harf).ToList();
            IstatistikBagil = Ortak.IstatistikListeDuzeltBagil(IstatistikBagil);
            return NotList;
        }

        public double OrtalamaHesap()
        {
            if (NotList.Any(x => x.Not <= 100))
            {
                Ortalama = NotList.Where(x => x.Not <= 100).Average(x => x.Not);
            }
            else
            {
                Ortalama = 0;
            }

            return Ortalama;
        }

        public double StandartSapmaHesap()
        {
            if (NotList.Select(x => x.Not).Any(x => x <= 100))
            {
                StandartSapma = NotList.Select(x => x.Not).Where(x => x <= 100).StdDev();
            }
            else
            {
                StandartSapma = 0;
            }
            return StandartSapma;
        }

        private double SinirDeger
        {
            get
            {
                double sinir = 50;
                if (Sinif == OgrenciSinif.Lisans)
                {
                    sinir = 50;
                }
                else if (Sinif == OgrenciSinif.YuksekLisans)
                {
                    sinir = 65;
                }
                else if (Sinif == OgrenciSinif.Doktora)
                {
                    sinir = 75;
                }
                return sinir;
            }
        }
    }
}
