﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sabis.Enum;
using Sabis.Bolum.Core.ABS.DEGERLENDIRME;
using Sabis.Bolum.Core.ABS.DEGERLENDIRME.Hiyerarsik;

namespace NOTDEGERLENDIRME
{
    public class HDegerlendirme
    {
        public static CiktiDTO Hesapla(Dictionary<OgrenciBilgi, List<HPayOgrenciNot>> liste, DegerlendirmeKural degerlendirmeKural, bool butunleme)
        {
            CiktiDTO result = new CiktiDTO();
            result.NotList = new List<OgrNot>();
            foreach (var ogrenciPay in liste)
            {
                List<HPayOgrenciNot> payList = ogrenciPay.Value;
                var ogrenciBilgi = ogrenciPay.Key;
                OgrNot ogrNot = null;
                if (degerlendirmeKural != null && degerlendirmeKural.YiliciDegerlendirTip)
                {
                    ogrNot = KokHesapTip(payList, ogrenciBilgi, degerlendirmeKural);
                }
                else
                {
                    ogrNot = KokHesap(payList, ogrenciBilgi, degerlendirmeKural);
                }
                ogrNot.ButunlemeMi = butunleme;
                result.NotList.Add(ogrNot);
            }

            result.IstatistikMutlak = result.NotList.GroupBy(x => x.MutlakNot).
                Select(x => new Istatistik { Harf = x.Key.ToString(), Adet = x.Count(), Yuzde = Math.Round(((double)x.Count() / (double)result.NotList.Count) * 100, 2), EnumHarf = (int)x.Key })
                .OrderBy(x => x.Harf).ToList();
            result.IstatistikMutlak = Ortak.IstatistikListeDuzeltMutlak(result.IstatistikMutlak);
            result.IstatistikBagil = result.NotList.GroupBy(x => x.BasariHarf)
                .Select(x => new Istatistik { Harf = x.Key.ToString(), Adet = x.Count(), Yuzde = Math.Round(((double)x.Count() / (double)result.NotList.Count) * 100, 2), EnumHarf = (int)x.Key })
                .OrderBy(x => x.Harf).ToList();
            result.IstatistikBagil = Ortak.IstatistikListeDuzeltBagil(result.IstatistikBagil);

            return result;
        }

        private static OgrNot KokHesap(List<HPayOgrenciNot> liste, OgrenciBilgi ogrenciBilgi, DegerlendirmeKural degerlendirmeKural)
        {
            OgrNot result = new OgrNot();

            //Ortalama hesabı
            double mutlakOrt = 0;
            //Dönem içini hesapla
            double donemIciOrt = 0;
            double yilIciOrt = 0;

            donemIciOrt = liste.Where(x => (int)x.EnumCalismaTip < 10 || x.EnumCalismaTip == EnumCalismaTip.PRATIK || x.EnumCalismaTip == EnumCalismaTip.TEORIK).Where(x => x.NotDeger.HasValue).Where(x => x.NotDeger <= 100).Sum(x => (double)(x.NotDeger * x.Yuzde) / (double)100);

            mutlakOrt += donemIciOrt;

            var altYilIciList = liste.Where(x => x.EnumCalismaTip == EnumCalismaTip.AltDersinYiliciKatkisi).ToList();
            foreach (var item in altYilIciList)
            {
                if (item.AltHPayList != null)
                {
                    //TIP Fakültesi hesabı
                    if (item.AltHPayList.Any(x => x.AltHPayList.Any(y => y.EnumCalismaTip == EnumCalismaTip.TEORIK || y.EnumCalismaTip == EnumCalismaTip.PRATIK)))
                    {
                        var teorikToplam = item.AltHPayList.SelectMany(x => x.AltHPayList.Where(y => y.EnumCalismaTip == EnumCalismaTip.TEORIK)).Sum(x => ((double)x.NotDeger.Value * x.Yuzde));
                        if (teorikToplam < 0)
                        {
                            teorikToplam = 0;
                        }


                        var pratikToplam = item.AltHPayList.SelectMany(x => x.AltHPayList.Where(y => y.EnumCalismaTip == EnumCalismaTip.PRATIK)).Sum(x => ((double)x.NotDeger.Value * x.Yuzde));
                        var kurulOrtalama = (teorikToplam + pratikToplam);
                        mutlakOrt += kurulOrtalama * item.Yuzde;
                        if (kurulOrtalama < degerlendirmeKural.MinKurulOrtalamaTip)
                        {
                            result.BasariHarf = EnumHarfBasari.YZ;
                        }
                    }
                    else
                    {
                        mutlakOrt += (double)(AltHesap(item.AltHPayList, false) * item.Yuzde) / (double)100;
                    }
                }
            }

            yilIciOrt = mutlakOrt;
            //Donem içinin toplama etkisini hesapla

            if (liste.Any(x => x.EnumCalismaTip == EnumCalismaTip.Final || x.EnumCalismaTip == EnumCalismaTip.Butunleme))
            {
                if (degerlendirmeKural.YiliciDegerlendirTip && !(mutlakOrt < degerlendirmeKural.MinYilIciOrtalamaTip || result.BasariHarf == EnumHarfBasari.YZ))
                {
                    //adam yıl içinde başarılı
                }
                else
                {
                    if (degerlendirmeKural.YiliciDegerlendirTip && (mutlakOrt < degerlendirmeKural.MinYilIciOrtalamaTip || result.BasariHarf == EnumHarfBasari.YZ))
                    {
                        //adam yıl içinde başarısız
                        mutlakOrt = liste.First(x => x.EnumCalismaTip == EnumCalismaTip.YilIcininBasariya).Yuzde * mutlakOrt / 100;
                    }
                    else
                    {
                        //diğerleri
                        mutlakOrt = (liste.First(x => x.EnumCalismaTip == EnumCalismaTip.YilIcininBasariya).Yuzde * donemIciOrt) / 100;
                    }
                    HPayOgrenciNot final = liste.FirstOrDefault(x => x.EnumCalismaTip == EnumCalismaTip.Final);
                    HPayOgrenciNot butunleme = liste.FirstOrDefault(x => x.EnumCalismaTip == EnumCalismaTip.Butunleme);
                    if (butunleme != null && butunleme.NotDeger.HasValue)
                    {
                        if (butunleme.NotDeger <= 100)
                        {
                            mutlakOrt += (double)(butunleme.NotDeger * butunleme.Yuzde) / (double)100;
                        }
                        else
                        {
                            mutlakOrt = butunleme.NotDeger.Value;
                        }
                    }
                    else if (final != null && final.NotDeger.HasValue)
                    {
                        if (final.NotDeger <= 100)
                        {
                            mutlakOrt += (double)(final.NotDeger * final.Yuzde) / (double)100;
                        }
                        else
                        {
                            mutlakOrt = final.NotDeger.Value;
                        }
                    }
                    else
                    {
                        mutlakOrt = 0;
                    }
                }

            }

            //Finali ekle
            var altFinalList = liste.Where(x => x.EnumCalismaTip == EnumCalismaTip.AltDersinFinalKatkisi).ToList();
            foreach (var item in altFinalList)
            {
                if (item.AltHPayList != null)
                {
                    var altHesap = AltHesap(item.AltHPayList, false);
                    mutlakOrt += (double)(altHesap * item.Yuzde) / (double)100;
                }
            }

            //Alt dersleri ekle
            var altDersList = liste.Where(x => x.EnumCalismaTip == EnumCalismaTip.AltDersinKatkisi).ToList();
            foreach (var item in altDersList)
            {
                if (item.AltHPayList != null)
                {
                    var altDeger = AltHesap(item.AltHPayList, true);
                    mutlakOrt += (double)(altDeger * item.Yuzde) / (double)100;
                }
            }

            result.AdSoyad = ogrenciBilgi.AdSoyad;
            result.GrupID = 0;
            result.Not = mutlakOrt;
            result.Numara = ogrenciBilgi.Numara;
            result.OgrID = ogrenciBilgi.OgrenciID;
            result.BasariNot = mutlakOrt;

            result.MutlakNot = Ortak.DonusturMutlakHarfNotu(mutlakOrt);
            int mutlakOrtSinir = 0;
            if (degerlendirmeKural != null)
            {
                if (degerlendirmeKural.MinOrtalama > 0)
                {
                    mutlakOrtSinir = degerlendirmeKural.MinOrtalama;
                }
                if (degerlendirmeKural.YiliciDegerlendirTip)
                {
                    mutlakOrtSinir = degerlendirmeKural.MinYilIciOrtalamaTip;
                }
            }
            if (mutlakOrt < mutlakOrtSinir || result.BasariHarf == EnumHarfBasari.YZ)
            {
                result.BasariHarf = EnumHarfBasari.YZ;
            }
            else if (mutlakOrt >= mutlakOrtSinir && mutlakOrt <= 100)
            {
                result.BasariHarf = EnumHarfBasari.YT;
            }
            else
            {
                result.BasariHarf = EnumHarfBasari.BELIRSIZ;
            }
            return result;
        }

        private static OgrNot KokHesapTip(List<HPayOgrenciNot> liste, OgrenciBilgi ogrenciBilgi, DegerlendirmeKural degerlendirmeKural)
        {
            OgrNot result = new OgrNot();

            //Ortalama hesabı
            double mutlakOrt = 0;
            //Dönem içini hesapla
            double donemIciOrt = 0;
            double yilIciOrt = 0;
            bool finaleGirdi = false;

            donemIciOrt = liste.Where(x => (int)x.EnumCalismaTip < 10 || x.EnumCalismaTip == EnumCalismaTip.PRATIK || x.EnumCalismaTip == EnumCalismaTip.TEORIK).Where(x => x.NotDeger.HasValue).Where(x => x.NotDeger <= 100).Sum(x => (double)(x.NotDeger * x.Yuzde) / (double)100);

            mutlakOrt += donemIciOrt;

            var altYilIciList = liste.Where(x => x.EnumCalismaTip == EnumCalismaTip.AltDersinYiliciKatkisi).ToList();
            foreach (var item in altYilIciList)
            {
                if (item.AltHPayList != null)
                {
                    //TIP Fakültesi hesabı
                    if (item.AltHPayList.Any(x => x.AltHPayList.Any(y => y.EnumCalismaTip == EnumCalismaTip.TEORIK || y.EnumCalismaTip == EnumCalismaTip.PRATIK)))
                    {
                        var teorikToplam = item.AltHPayList.SelectMany(x => x.AltHPayList.Where(y => y.NotDeger <= 100 && y.EnumCalismaTip == EnumCalismaTip.TEORIK)).Sum(x => ((double)x.NotDeger.Value * x.Yuzde));
                        if (teorikToplam < 0)
                        {
                            teorikToplam = 0;
                        }


                        var pratikToplam = item.AltHPayList.SelectMany(x => x.AltHPayList.Where(y => y.NotDeger <= 100 && y.EnumCalismaTip == EnumCalismaTip.PRATIK)).Sum(x => ((double)x.NotDeger.Value * x.Yuzde));
                        var kurulOrtalama = (teorikToplam + pratikToplam);
                        mutlakOrt += kurulOrtalama * item.Yuzde;
                        if (kurulOrtalama < degerlendirmeKural.MinKurulOrtalamaTip)
                        {
                            result.BasariHarf = EnumHarfBasari.YZ;
                        }
                    }
                    else
                    {
                        mutlakOrt += (double)(AltHesap(item.AltHPayList, false) * item.Yuzde) / (double)100;
                    }
                }
            }

            yilIciOrt = mutlakOrt;
            //Donem içinin toplama etkisini hesapla

            if (liste.Any(x => x.EnumCalismaTip == EnumCalismaTip.Final || x.EnumCalismaTip == EnumCalismaTip.Butunleme))
            {
                if (degerlendirmeKural.YiliciDegerlendirTip && !(mutlakOrt < degerlendirmeKural.MinYilIciOrtalamaTip || result.BasariHarf == EnumHarfBasari.YZ))
                {
                    //adam yıl içinde başarılı
                }
                else
                {
                    if (degerlendirmeKural.YiliciDegerlendirTip && (mutlakOrt < degerlendirmeKural.MinYilIciOrtalamaTip || result.BasariHarf == EnumHarfBasari.YZ))
                    {
                        //adam yıl içinde başarısız
                        mutlakOrt = liste.First(x => x.EnumCalismaTip == EnumCalismaTip.YilIcininBasariya).Yuzde * mutlakOrt / 100;
                    }
                    else
                    {
                        //diğerleri
                        mutlakOrt = (liste.First(x => x.EnumCalismaTip == EnumCalismaTip.YilIcininBasariya).Yuzde * donemIciOrt) / 100;
                    }

                    HPayOgrenciNot final = liste.FirstOrDefault(x => x.EnumCalismaTip == EnumCalismaTip.Final);
                    HPayOgrenciNot butunleme = liste.FirstOrDefault(x => x.EnumCalismaTip == EnumCalismaTip.Butunleme);
                    if (butunleme != null && butunleme.NotDeger.HasValue)
                    {
                        if (butunleme.NotDeger <= 100)
                        {
                            mutlakOrt += (double)(butunleme.NotDeger * butunleme.Yuzde) / (double)100;
                            finaleGirdi = true;
                        }
                        else if (butunleme.NotDeger == 120)
                        {
                            result.BasariHarf = EnumHarfBasari.GR;
                            //mutlakOrt = butunleme.NotDeger.Value;
                        }
                        else if (butunleme.NotDeger == 130)
                        {
                            result.BasariHarf = EnumHarfBasari.DZ;
                            //mutlakOrt = butunleme.NotDeger.Value;
                        }
                        else
                        {
                            result.BasariHarf = EnumHarfBasari.YZ;
                            mutlakOrt = butunleme.NotDeger.Value;
                        }
                    }
                    else if (final != null && final.NotDeger.HasValue)
                    {
                        if (final.NotDeger <= 100)
                        {
                            mutlakOrt += (double)(final.NotDeger * final.Yuzde) / (double)100;
                            finaleGirdi = true;
                        }
                        else if (final.NotDeger == 120)
                        {
                            result.BasariHarf = EnumHarfBasari.GR;
                            //mutlakOrt = final.NotDeger.Value;
                        }
                        else if (final.NotDeger == 130)
                        {
                            result.BasariHarf = EnumHarfBasari.DZ;
                            //mutlakOrt = final.NotDeger.Value;
                        }
                        else
                        {
                            result.BasariHarf = EnumHarfBasari.YZ;
                            mutlakOrt = final.NotDeger.Value;
                        }
                    }
                    else
                    {
                        //mutlakOrt = 0;
                    }
                }

            }

            //Finali ekle
            var altFinalList = liste.Where(x => x.EnumCalismaTip == EnumCalismaTip.AltDersinFinalKatkisi).ToList();
            foreach (var item in altFinalList)
            {
                if (item.AltHPayList != null)
                {
                    mutlakOrt += (double)(AltHesap(item.AltHPayList, false) * item.Yuzde) / (double)100;
                }
            }

            //Alt dersleri ekle
            var altDersList = liste.Where(x => x.EnumCalismaTip == EnumCalismaTip.AltDersinKatkisi).ToList();
            foreach (var item in altDersList)
            {
                if (item.AltHPayList != null)
                {
                    mutlakOrt += (double)(AltHesap(item.AltHPayList, true) * item.Yuzde) / (double)100;
                }
            }

            result.AdSoyad = ogrenciBilgi.AdSoyad;
            result.GrupID = 0;
            result.Not = mutlakOrt;
            result.Numara = ogrenciBilgi.Numara;
            result.OgrID = ogrenciBilgi.OgrenciID;
            result.BasariNot = mutlakOrt;

            result.MutlakNot = Ortak.DonusturMutlakHarfNotu(mutlakOrt);
            int mutlakOrtSinir = 0;
            int minYiliciSinir = 0;
            if (degerlendirmeKural != null)
            {
                if (degerlendirmeKural.MinOrtalama > 0)
                {
                    mutlakOrtSinir = degerlendirmeKural.MinOrtalama;
                }
                if (degerlendirmeKural.YiliciDegerlendirTip)
                {
                    minYiliciSinir = degerlendirmeKural.MinYilIciOrtalamaTip;
                }
            }
            //finale girdi ise
            if (finaleGirdi)
            {
                if (mutlakOrt < mutlakOrtSinir)
                {
                    result.BasariHarf = EnumHarfBasari.YZ;
                }
                else
                {
                    result.BasariHarf = EnumHarfBasari.YT;
                }
            }
            //finale girmedi sadece yıliçi değerlendirilecek
            else
            {
                if (result.BasariHarf == EnumHarfBasari.DZ || result.BasariHarf == EnumHarfBasari.GR || result.BasariHarf == EnumHarfBasari.YZ)
                {

                }
                else if (mutlakOrt < minYiliciSinir)
                {
                    result.BasariHarf = EnumHarfBasari.YZ;
                }
                else
                {
                    result.BasariHarf = EnumHarfBasari.YT;
                }
            }
            return result;
        }

        private static double AltHesap(List<HPayOgrenciNot> liste, bool paketHesap)
        {
            double mutlakOrt = 0;
            var yilIci = liste.FirstOrDefault(x => x.EnumCalismaTip == EnumCalismaTip.YilIcininBasariya);
            double yilIciOran = 1;
            if (paketHesap)
            {
                if (yilIci == null)
                {
                    throw new Exception("Yıl içi katkı payı tanımlanmalıdır!");
                }
                yilIciOran = yilIci.Yuzde / (double)100;
            }

            foreach (var pay in liste)
            {
                //TODO pay.not hasvalue
                if (pay.EnumCalismaTip != EnumCalismaTip.AltDersinYiliciKatkisi && pay.EnumCalismaTip != EnumCalismaTip.AltDersinFinalKatkisi && pay.EnumCalismaTip != EnumCalismaTip.AltDersinKatkisi)
                {
                    if (pay.NotDeger <= 100)
                    {
                        if (pay.EnumCalismaTip == EnumCalismaTip.Final || pay.EnumCalismaTip == EnumCalismaTip.Butunleme)
                        {
                            if (paketHesap)
                            {
                                mutlakOrt += ((double)pay.NotDeger.Value * pay.Yuzde) / (double)100;
                            }
                            else
                            {
                                //mutlakOrt += pay.NotDeger.Value;
                                mutlakOrt += ((double)pay.NotDeger.Value * pay.Yuzde) / (double)100;
                            }
                        }
                        else
                        {
                            mutlakOrt += yilIciOran * ((double)(pay.NotDeger.Value * pay.Yuzde)) / (double)100;
                        }
                    }
                }
                else if (pay.AltHPayList != null)
                {

                    mutlakOrt = AltHesap(pay.AltHPayList, paketHesap);
                }
                else
                {
                    mutlakOrt = 0;
                }
            }
            return mutlakOrt;
        }
    }
}
