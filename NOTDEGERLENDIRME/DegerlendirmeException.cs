﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.Serialization;

namespace NOTDEGERLENDIRME
{
    public class DegerlendirmeException : Exception
    {
        public DegerlendirmeException()
            : base()
        { }

        public DegerlendirmeException(string message)
            : base(message)
        { }

        public DegerlendirmeException(string format, params object[] args)
            : base(string.Format(format, args))
        { }

        public DegerlendirmeException(string message, Exception innerException)
            : base(message, innerException)
        { }

        public DegerlendirmeException(string format, Exception innerException, params object[] args)
            : base(string.Format(format, args), innerException)
        { }

        protected DegerlendirmeException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        { }
    }
}
