namespace Sabis.Bolum.Rapor
{
    partial class YoklamaListe
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(YoklamaListe));
            Telerik.Reporting.Barcodes.QRCodeEncoder qrCodeEncoder1 = new Telerik.Reporting.Barcodes.QRCodeEncoder();
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.pLogo = new Telerik.Reporting.PictureBox();
            this.txtOgretimUyesi = new Telerik.Reporting.TextBox();
            this.txtDersAdi = new Telerik.Reporting.TextBox();
            this.barcode1 = new Telerik.Reporting.Barcode();
            this.htmlTextBox1 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox2 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox3 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox4 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox5 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox6 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox7 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox8 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox9 = new Telerik.Reporting.HtmlTextBox();
            this.shape2 = new Telerik.Reporting.Shape();
            this.txtFakulteAdi = new Telerik.Reporting.HtmlTextBox();
            this.txtBolumAdi = new Telerik.Reporting.HtmlTextBox();
            this.shape3 = new Telerik.Reporting.Shape();
            this.txtTarih = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.txtToplamSayi = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.shape1 = new Telerik.Reporting.Shape();
            this.shape4 = new Telerik.Reporting.Shape();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(4.6500000953674316D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pLogo,
            this.txtOgretimUyesi,
            this.txtDersAdi,
            this.barcode1,
            this.htmlTextBox1,
            this.htmlTextBox2,
            this.htmlTextBox3,
            this.htmlTextBox4,
            this.htmlTextBox5,
            this.htmlTextBox6,
            this.htmlTextBox7,
            this.htmlTextBox8,
            this.htmlTextBox9,
            this.shape2,
            this.txtFakulteAdi,
            this.txtBolumAdi,
            this.shape3,
            this.txtTarih});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            this.pageHeaderSection1.Style.Font.Name = "Tahoma";
            // 
            // pLogo
            // 
            this.pLogo.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.7225477222382324E-06D), Telerik.Reporting.Drawing.Unit.Cm(0.30579873919487D));
            this.pLogo.MimeType = "";
            this.pLogo.Name = "pLogo";
            this.pLogo.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.3998003005981445D), Telerik.Reporting.Drawing.Unit.Cm(2.9950993061065674D));
            this.pLogo.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pLogo.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.pLogo.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.pLogo.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.pLogo.Style.Font.Name = "Tahoma";
            this.pLogo.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0.10000000149011612D);
            this.pLogo.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0.10000000149011612D);
            this.pLogo.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Cm(0.10000000149011612D);
            this.pLogo.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0.10000000149011612D);
            this.pLogo.Value = "";
            // 
            // txtOgretimUyesi
            // 
            this.txtOgretimUyesi.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.4000015258789062D), Telerik.Reporting.Drawing.Unit.Cm(2.1062977313995361D));
            this.txtOgretimUyesi.Name = "txtOgretimUyesi";
            this.txtOgretimUyesi.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.548822402954102D), Telerik.Reporting.Drawing.Unit.Cm(0.60000002384185791D));
            this.txtOgretimUyesi.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtOgretimUyesi.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.txtOgretimUyesi.Style.Font.Name = "Tahoma";
            this.txtOgretimUyesi.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.txtOgretimUyesi.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.txtOgretimUyesi, "txtOgretimUyesi");
            // 
            // txtDersAdi
            // 
            this.txtDersAdi.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.4000015258789062D), Telerik.Reporting.Drawing.Unit.Cm(1.5061987638473511D));
            this.txtDersAdi.Name = "txtDersAdi";
            this.txtDersAdi.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.548820495605469D), Telerik.Reporting.Drawing.Unit.Cm(0.60000002384185791D));
            this.txtDersAdi.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtDersAdi.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.txtDersAdi.Style.Font.Name = "Tahoma";
            this.txtDersAdi.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.txtDersAdi.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.txtDersAdi, "txtDersAdi");
            // 
            // barcode1
            // 
            qrCodeEncoder1.ErrorCorrectionLevel = Telerik.Reporting.Barcodes.QRCode.ErrorCorrectionLevel.H;
            qrCodeEncoder1.Mode = Telerik.Reporting.Barcodes.QRCode.CodeMode.Alphanumeric;
            this.barcode1.Encoder = qrCodeEncoder1;
            this.barcode1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.94902229309082D), Telerik.Reporting.Drawing.Unit.Cm(0.30009967088699341D));
            this.barcode1.Name = "barcode1";
            this.barcode1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9708797931671143D), Telerik.Reporting.Drawing.Unit.Cm(3.0007984638214111D));
            this.barcode1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.barcode1.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            // 
            // htmlTextBox1
            // 
            this.htmlTextBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.7225477222382324E-06D), Telerik.Reporting.Drawing.Unit.Cm(4.1968135833740234D));
            this.htmlTextBox1.Name = "htmlTextBox1";
            this.htmlTextBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9199999570846558D), Telerik.Reporting.Drawing.Unit.Cm(0.44999998807907104D));
            this.htmlTextBox1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox1.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.htmlTextBox1.Style.Font.Bold = true;
            this.htmlTextBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.htmlTextBox1.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.htmlTextBox1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0.10000000149011612D);
            this.htmlTextBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.htmlTextBox1, "htmlTextBox1");
            // 
            // htmlTextBox2
            // 
            this.htmlTextBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.9192092418670654D), Telerik.Reporting.Drawing.Unit.Cm(4.1968140602111816D));
            this.htmlTextBox2.Name = "htmlTextBox2";
            this.htmlTextBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(0.44999998807907104D));
            this.htmlTextBox2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox2.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.htmlTextBox2.Style.Font.Bold = true;
            this.htmlTextBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.htmlTextBox2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0.10000000149011612D);
            this.htmlTextBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.htmlTextBox2, "htmlTextBox2");
            // 
            // htmlTextBox3
            // 
            this.htmlTextBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.919410228729248D), Telerik.Reporting.Drawing.Unit.Cm(4.1968140602111816D));
            this.htmlTextBox3.Name = "htmlTextBox3";
            this.htmlTextBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7200000286102295D), Telerik.Reporting.Drawing.Unit.Cm(0.44999998807907104D));
            this.htmlTextBox3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox3.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.htmlTextBox3.Style.Font.Bold = true;
            this.htmlTextBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.htmlTextBox3.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0.10000000149011612D);
            this.htmlTextBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.htmlTextBox3, "htmlTextBox3");
            // 
            // htmlTextBox4
            // 
            this.htmlTextBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.560955047607422D), Telerik.Reporting.Drawing.Unit.Cm(4.1968140602111816D));
            this.htmlTextBox4.Name = "htmlTextBox4";
            this.htmlTextBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7200000286102295D), Telerik.Reporting.Drawing.Unit.Cm(0.44999998807907104D));
            this.htmlTextBox4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox4.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.htmlTextBox4.Style.Font.Bold = true;
            this.htmlTextBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.htmlTextBox4.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0.10000000149011612D);
            this.htmlTextBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.htmlTextBox4, "htmlTextBox4");
            // 
            // htmlTextBox5
            // 
            this.htmlTextBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.560755729675293D), Telerik.Reporting.Drawing.Unit.Cm(4.1969137191772461D));
            this.htmlTextBox5.Name = "htmlTextBox5";
            this.htmlTextBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(0.44999998807907104D));
            this.htmlTextBox5.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox5.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.htmlTextBox5.Style.Font.Bold = true;
            this.htmlTextBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.htmlTextBox5.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0.10000000149011612D);
            this.htmlTextBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.htmlTextBox5, "htmlTextBox5");
            // 
            // htmlTextBox6
            // 
            this.htmlTextBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.6405539512634277D), Telerik.Reporting.Drawing.Unit.Cm(4.1968140602111816D));
            this.htmlTextBox6.Name = "htmlTextBox6";
            this.htmlTextBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9199999570846558D), Telerik.Reporting.Drawing.Unit.Cm(0.44999998807907104D));
            this.htmlTextBox6.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox6.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.htmlTextBox6.Style.Font.Bold = true;
            this.htmlTextBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.htmlTextBox6.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0.10000000149011612D);
            this.htmlTextBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.htmlTextBox6, "htmlTextBox6");
            // 
            // htmlTextBox7
            // 
            this.htmlTextBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(18.200197219848633D), Telerik.Reporting.Drawing.Unit.Cm(4.1968140602111816D));
            this.htmlTextBox7.Name = "htmlTextBox7";
            this.htmlTextBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.719700813293457D), Telerik.Reporting.Drawing.Unit.Cm(0.44999998807907104D));
            this.htmlTextBox7.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox7.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.htmlTextBox7.Style.Font.Bold = true;
            this.htmlTextBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.htmlTextBox7.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0.10000000149011612D);
            this.htmlTextBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.htmlTextBox7, "htmlTextBox7");
            // 
            // htmlTextBox8
            // 
            this.htmlTextBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.20000171661377D), Telerik.Reporting.Drawing.Unit.Cm(4.1968140602111816D));
            this.htmlTextBox8.Name = "htmlTextBox8";
            this.htmlTextBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(0.44999998807907104D));
            this.htmlTextBox8.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox8.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.htmlTextBox8.Style.Font.Bold = true;
            this.htmlTextBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.htmlTextBox8.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0.10000000149011612D);
            this.htmlTextBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.htmlTextBox8, "htmlTextBox8");
            // 
            // htmlTextBox9
            // 
            this.htmlTextBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.281152725219727D), Telerik.Reporting.Drawing.Unit.Cm(4.1969137191772461D));
            this.htmlTextBox9.Name = "htmlTextBox9";
            this.htmlTextBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9199999570846558D), Telerik.Reporting.Drawing.Unit.Cm(0.44999998807907104D));
            this.htmlTextBox9.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox9.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.htmlTextBox9.Style.Font.Bold = true;
            this.htmlTextBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.htmlTextBox9.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0.10000000149011612D);
            this.htmlTextBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.htmlTextBox9, "htmlTextBox9");
            // 
            // shape2
            // 
            this.shape2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(19.469999313354492D), Telerik.Reporting.Drawing.Unit.Cm(3.7200000286102295D));
            this.shape2.Name = "shape2";
            this.shape2.ShapeType = new Telerik.Reporting.Drawing.Shapes.EllipseShape(0D);
            this.shape2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.44999998807907104D), Telerik.Reporting.Drawing.Unit.Cm(0.44999998807907104D));
            this.shape2.Stretch = false;
            this.shape2.Style.BackgroundColor = System.Drawing.Color.Black;
            // 
            // txtFakulteAdi
            // 
            this.txtFakulteAdi.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.4000015258789062D), Telerik.Reporting.Drawing.Unit.Cm(0.30579873919487D));
            this.txtFakulteAdi.Name = "txtFakulteAdi";
            this.txtFakulteAdi.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.548820495605469D), Telerik.Reporting.Drawing.Unit.Cm(0.60000002384185791D));
            this.txtFakulteAdi.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtFakulteAdi.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.txtFakulteAdi.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.txtFakulteAdi.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.txtFakulteAdi, "txtFakulteAdi");
            // 
            // txtBolumAdi
            // 
            this.txtBolumAdi.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.4000015258789062D), Telerik.Reporting.Drawing.Unit.Cm(0.905998706817627D));
            this.txtBolumAdi.Name = "txtBolumAdi";
            this.txtBolumAdi.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.548820495605469D), Telerik.Reporting.Drawing.Unit.Cm(0.60000002384185791D));
            this.txtBolumAdi.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtBolumAdi.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.txtBolumAdi.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.txtBolumAdi.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.txtBolumAdi.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.txtBolumAdi.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.txtBolumAdi.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.txtBolumAdi, "txtBolumAdi");
            // 
            // shape3
            // 
            this.shape3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(3.7200000286102295D));
            this.shape3.Name = "shape3";
            this.shape3.ShapeType = new Telerik.Reporting.Drawing.Shapes.EllipseShape(0D);
            this.shape3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.44999998807907104D), Telerik.Reporting.Drawing.Unit.Cm(0.44999998807907104D));
            this.shape3.Style.BackgroundColor = System.Drawing.Color.Black;
            // 
            // txtTarih
            // 
            this.txtTarih.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.4000015258789062D), Telerik.Reporting.Drawing.Unit.Cm(2.7064974308013916D));
            this.txtTarih.Name = "txtTarih";
            this.txtTarih.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.548822402954102D), Telerik.Reporting.Drawing.Unit.Cm(0.60000002384185791D));
            this.txtTarih.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtTarih.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.txtTarih.Style.Font.Name = "Tahoma";
            this.txtTarih.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.txtTarih.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.txtTarih, "txtTarih");
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table1});
            this.detail.Name = "detail";
            this.detail.Style.BackgroundImage.Repeat = Telerik.Reporting.Drawing.BackgroundRepeat.RepeatY;
            this.detail.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.detail.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.detail.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.detail.Style.Font.Name = "Tahoma";
            this.detail.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.detail.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.detail.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            // 
            // table1
            // 
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.9194358587265015D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.0003585815429688D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.7202054262161255D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D)));
            this.table1.Body.SetCellContent(0, 0, this.textBox4);
            this.table1.Body.SetCellContent(0, 1, this.textBox5);
            this.table1.Body.SetCellContent(0, 2, this.textBox6);
            this.table1.ColumnGroups.Add(tableGroup1);
            this.table1.ColumnGroups.Add(tableGroup2);
            this.table1.ColumnGroups.Add(tableGroup3);
            this.table1.Docking = Telerik.Reporting.DockingStyle.Top;
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox4,
            this.textBox5,
            this.textBox6});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.table1.Name = "table1";
            tableGroup4.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup4.Name = "DetailGroup";
            this.table1.RowGroups.Add(tableGroup4);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.6399998664855957D), Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D));
            this.table1.Style.Font.Name = "Tahoma";
            this.table1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.table1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            // 
            // textBox4
            // 
            this.textBox4.CanGrow = false;
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.919435977935791D), Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D));
            this.textBox4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox4.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox4.Style.Font.Name = "Tahoma";
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox4.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox4.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox4.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.textBox4, "textBox4");
            // 
            // textBox5
            // 
            this.textBox5.CanGrow = false;
            this.textBox5.Multiline = false;
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0003585815429688D), Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D));
            this.textBox5.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox5.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox5.Style.Font.Name = "Tahoma";
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox5.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox5.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox5.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.textBox5, "textBox5");
            // 
            // textBox6
            // 
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7202054262161255D), Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D));
            this.textBox6.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox6.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox6.Style.Font.Name = "Tahoma";
            this.textBox6.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox6.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(1.6799999475479126D);
            this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox17,
            this.textBox27,
            this.txtToplamSayi,
            this.textBox25,
            this.shape1,
            this.shape4});
            this.pageFooterSection1.Name = "pageFooterSection1";
            this.pageFooterSection1.Style.Font.Name = "Tahoma";
            // 
            // textBox17
            // 
            this.textBox17.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(18D), Telerik.Reporting.Drawing.Unit.Cm(0.57290339469909668D));
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8940505981445313D), Telerik.Reporting.Drawing.Unit.Cm(0.39999979734420776D));
            this.textBox17.Style.Font.Bold = false;
            this.textBox17.Style.Font.Name = "Tahoma";
            this.textBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            resources.ApplyResources(this.textBox17, "textBox17");
            // 
            // textBox27
            // 
            this.textBox27.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.5D), Telerik.Reporting.Drawing.Unit.Cm(0.57290339469909668D));
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.1998996734619141D), Telerik.Reporting.Drawing.Unit.Cm(0.40000030398368835D));
            this.textBox27.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            resources.ApplyResources(this.textBox27, "textBox27");
            // 
            // txtToplamSayi
            // 
            this.txtToplamSayi.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.5002002716064453D), Telerik.Reporting.Drawing.Unit.Cm(0.56160175800323486D));
            this.txtToplamSayi.Name = "txtToplamSayi";
            this.txtToplamSayi.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.3997989892959595D), Telerik.Reporting.Drawing.Unit.Cm(0.39990031719207764D));
            resources.ApplyResources(this.txtToplamSayi, "txtToplamSayi");
            // 
            // textBox25
            // 
            this.textBox25.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.0372210463601732E-08D), Telerik.Reporting.Drawing.Unit.Cm(0.54999959468841553D));
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5D), Telerik.Reporting.Drawing.Unit.Cm(0.39990031719207764D));
            resources.ApplyResources(this.textBox25, "textBox25");
            // 
            // shape1
            // 
            this.shape1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.shape1.Name = "shape1";
            this.shape1.ShapeType = new Telerik.Reporting.Drawing.Shapes.EllipseShape(0D);
            this.shape1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.44999998807907104D), Telerik.Reporting.Drawing.Unit.Cm(0.44999998807907104D));
            this.shape1.Style.BackgroundColor = System.Drawing.Color.Black;
            // 
            // shape4
            // 
            this.shape4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(19.469898223876953D), Telerik.Reporting.Drawing.Unit.Cm(9.9907767435070127E-05D));
            this.shape4.Name = "shape4";
            this.shape4.ShapeType = new Telerik.Reporting.Drawing.Shapes.EllipseShape(0D);
            this.shape4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.44999998807907104D), Telerik.Reporting.Drawing.Unit.Cm(0.44999998807907104D));
            this.shape4.Style.BackgroundColor = System.Drawing.Color.Black;
            // 
            // YoklamaListe
            // 
            this.Culture = new System.Globalization.CultureInfo("tr-TR");
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1});
            this.Name = "YoklamaListe";
            this.PageSettings.ColumnCount = 3;
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Mm(5.4000000953674316D), Telerik.Reporting.Drawing.Unit.Mm(5.4000000953674316D), Telerik.Reporting.Drawing.Unit.Mm(5.4000000953674316D), Telerik.Reporting.Drawing.Unit.Mm(3D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Style.BackgroundColor = System.Drawing.Color.White;
            this.Style.BackgroundImage.MimeType = "";
            this.Style.BackgroundImage.Repeat = Telerik.Reporting.Drawing.BackgroundRepeat.RepeatY;
            this.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Width = Telerik.Reporting.Drawing.Unit.Cm(6.6399998664855957D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.PictureBox pLogo;
        private Telerik.Reporting.TextBox txtOgretimUyesi;
        private Telerik.Reporting.TextBox txtDersAdi;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox txtToplamSayi;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.Barcode barcode1;
        private Telerik.Reporting.HtmlTextBox htmlTextBox1;
        private Telerik.Reporting.HtmlTextBox htmlTextBox2;
        private Telerik.Reporting.HtmlTextBox htmlTextBox3;
        private Telerik.Reporting.HtmlTextBox htmlTextBox4;
        private Telerik.Reporting.HtmlTextBox htmlTextBox5;
        private Telerik.Reporting.HtmlTextBox htmlTextBox6;
        private Telerik.Reporting.HtmlTextBox htmlTextBox7;
        private Telerik.Reporting.HtmlTextBox htmlTextBox8;
        private Telerik.Reporting.HtmlTextBox htmlTextBox9;
        private Telerik.Reporting.Shape shape2;
        private Telerik.Reporting.HtmlTextBox txtFakulteAdi;
        private Telerik.Reporting.HtmlTextBox txtBolumAdi;
        private Telerik.Reporting.Shape shape3;
        private Telerik.Reporting.TextBox txtTarih;
        private Telerik.Reporting.Shape shape1;
        private Telerik.Reporting.Shape shape4;
    }
}