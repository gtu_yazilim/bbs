namespace Sabis.Bolum.Rapor
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using Telerik.Reporting.Drawing;
    using System.Data;
    using System.Collections;
    using System.Linq;
    using System.Collections.Generic;
    using Sabis.Bolum.Core.ABS.DEGERLENDIRME;
    using Sabis.Enum;
    public partial class YardimciListe : Telerik.Reporting.Report
    {
        DataTable veri = new DataTable();

        public YardimciListe(BasariListesiDTO _gelen, string universiteAdi, string logoUrl)
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();
            this.DocumentName = string.Format("{0}_{1}_{2}.pdf", _gelen.Ders.DersKod, DateTime.Now.ToString("ddMMHHmmss"), "yardimci");
            //table1.DataSource = datatable;
            txtBaslik.Value = universiteAdi.Replace("\\n", Environment.NewLine) + "\n" + _gelen.Fakulte.BirimAdi + "\n" + _gelen.Bolum.BirimAdi;
            txtDersAdi.Value = _gelen.Ders.DersAd + "(" + _gelen.Ders.DersGrupAd + ")";
            txtDonem.Value = Sabis.Enum.Utils.GetEnumDescription((EnumDonem)_gelen.Ders.Donem);
            txtKoduSaati.Value = _gelen.Ders.DersKod + " / " + _gelen.Ders.DSaat + "+" + _gelen.Ders.USaat;
            txtOgretimTuru.Value = _gelen.Ders.OgretimTuruAd;
            txtOgretimUyesi.Value = string.Format("{0} {1} {2}", _gelen.Ders.Personel.UnvanAd, _gelen.Ders.Personel.Ad, _gelen.Ders.Personel.Soyad);
            txtOgretimYili.Value = string.Format("{0}-{1}", _gelen.Ders.Yil.ToString(), (_gelen.Ders.Yil + 1).ToString());
            txtToplamSayi.Value = _gelen.NotList.Count.ToString();
            pLogo.Value = logoUrl;

            veri = Donustur(_gelen.NotList.OrderBy(x => x.BasariHarf).ThenByDescending(x => x.MutlakNot).ToList());

            table1.DataSource = veri;
        }
        int paySayisi = 0;
        private DataTable Donustur(List<OgrenciPayveNotBilgileriDTO> bilgiler)
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn { Caption = "NUMARA", ColumnName = "NUMARA" });
            dataTable.Columns.Add(new DataColumn { Caption = "AD SOYAD", ColumnName = "ADSOYAD" });
            int say = 1;
            paySayisi = bilgiler.First().listPaylarVeNotlar.Count;
            //foreach (PaylarVeNotlar item in bilgiler.First().listPaylarVeNotlar)
            //{
            //    string colAd = item.Sira.ToString() + "." + PayKisaAd(item.EnumCalismaTip) + say;
            //    colAd = colAd.Replace(".", "x").Replace(" ", "");
            //    colAd = "abc" + say.ToString();
            //    string caption = item.Sira.ToString() + "." + PayKisaAd(item.EnumCalismaTip);
            //    if (dataTable.Columns[colAd] == null)
            //    {
            //        dataTable.Columns.Add(new DataColumn { Caption = caption, ColumnName = colAd });
            //    }
            //    say++;
            //}
            dataTable.Columns.Add(new DataColumn { Caption = "Harfli Mutlak", ColumnName = "MutlakHarf" });
            dataTable.Columns.Add(new DataColumn { Caption = "MUTLAK ORT.", ColumnName = "MutlakNot" });
            dataTable.Columns.Add(new DataColumn { Caption = "BA�ARI NOTU", ColumnName = "BasariHarf" });


            foreach (OgrenciPayveNotBilgileriDTO item in bilgiler)
            {
                DataRow dr = dataTable.NewRow();

                List<string> satirDegerleri = new List<string>();
                satirDegerleri.Add(item.Numara.ToUpper());
                satirDegerleri.Add(item.AdSoyad.ToUpper());

                if (item.MutlakNot <= 100)
                {
                    var tempNot = Math.Round(item.MutlakNot, 2);
                    satirDegerleri.Add(tempNot.ToString());
                }
                else if (item.MutlakNot == 120)
                {
                    satirDegerleri.Add("GR");
                }
                else if (item.MutlakNot == 130)
                {
                    satirDegerleri.Add("DZ");
                }
                //satirDegerleri.Add(item.MutlakNot.ToString());
                satirDegerleri.Add(item.MutlakHarf.ToString());
                satirDegerleri.Add(item.BasariHarf.ToString());
                dr.ItemArray = satirDegerleri.ToArray();
                dataTable.Rows.Add(dr);
            }
            return dataTable;
        }

        private string PayKisaAd(EnumCalismaTip tip)
        {
            string result = string.Empty;
            switch (tip)
            {
                case EnumCalismaTip.AraSinav:
                    result = "Ara Snv.";
                    break;
                case EnumCalismaTip.KisaSinav:
                    result = "K.Snv.";
                    break;
                case EnumCalismaTip.Odev:
                    result = "�dev";
                    break;
                case EnumCalismaTip.SozluSinav:
                    result = "S�z.Snv.";
                    break;
                case EnumCalismaTip.ProjeTasarim:
                    result = "Proj.Tas.";
                    break;
                case EnumCalismaTip.PerformansGoreviUygulama:
                    result = "K.Snv.";
                    break;
                case EnumCalismaTip.PerformanGoreviLabaratuvar:
                    result = "Perf.Gor.Lab.";
                    break;
                case EnumCalismaTip.PerformansGoreviAtolye:
                    result = "Perf.Gor.Atolye";
                    break;
                case EnumCalismaTip.PerformansGoreviSeminer:
                    result = "Perf.Gor.Atolye";
                    break;
                case EnumCalismaTip.PerformansGoreviAraziCalismasi:
                    result = "Perf.Gor.Arazi";
                    break;
                case EnumCalismaTip.YilIciOrtalamasi:
                    result = "Y�li�i Ort.";
                    break;
                case EnumCalismaTip.YilIcininBasariya:
                    result = "Y�li�i Ba�ar�";
                    break;
                case EnumCalismaTip.Final:
                    result = "Final";
                    break;
                default:
                    break;
            }
            return result;
        }

        private void table1_ItemDataBinding(object sender, EventArgs e)
        {

        }
    }
}