namespace Sabis.Bolum.Rapor
{
    partial class BasariListesi
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Barcodes.Code128Encoder code128Encoder1 = new Telerik.Reporting.Barcodes.Code128Encoder();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.txtBaslik = new Telerik.Reporting.TextBox();
            this.pLogo = new Telerik.Reporting.PictureBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.txtOgretimUyesi = new Telerik.Reporting.TextBox();
            this.txtKoduSaati = new Telerik.Reporting.TextBox();
            this.txtDersAdi = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.txtOgretimTuru = new Telerik.Reporting.TextBox();
            this.txtOgretimYili = new Telerik.Reporting.TextBox();
            this.txtDonem = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.textBox35 = new Telerik.Reporting.TextBox();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.panel1 = new Telerik.Reporting.Panel();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.panel2 = new Telerik.Reporting.Panel();
            this.panel3 = new Telerik.Reporting.Panel();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.textBox38 = new Telerik.Reporting.TextBox();
            this.textBox41 = new Telerik.Reporting.TextBox();
            this.textBox42 = new Telerik.Reporting.TextBox();
            this.textBox43 = new Telerik.Reporting.TextBox();
            this.textBox44 = new Telerik.Reporting.TextBox();
            this.textBox45 = new Telerik.Reporting.TextBox();
            this.textBox46 = new Telerik.Reporting.TextBox();
            this.textBox47 = new Telerik.Reporting.TextBox();
            this.textBox48 = new Telerik.Reporting.TextBox();
            this.textBox49 = new Telerik.Reporting.TextBox();
            this.textBox50 = new Telerik.Reporting.TextBox();
            this.txtSayiGR = new Telerik.Reporting.TextBox();
            this.txtSayiDZ = new Telerik.Reporting.TextBox();
            this.txtSayiFF = new Telerik.Reporting.TextBox();
            this.txtSayiDD = new Telerik.Reporting.TextBox();
            this.txtSayiDC = new Telerik.Reporting.TextBox();
            this.txtSayiCC = new Telerik.Reporting.TextBox();
            this.txtSayiCB = new Telerik.Reporting.TextBox();
            this.txtSayiBB = new Telerik.Reporting.TextBox();
            this.txtSayiBA = new Telerik.Reporting.TextBox();
            this.txtSayiAA = new Telerik.Reporting.TextBox();
            this.txtYuzdeAA = new Telerik.Reporting.TextBox();
            this.txtYuzdeBA = new Telerik.Reporting.TextBox();
            this.txtYuzdeBB = new Telerik.Reporting.TextBox();
            this.txtYuzdeCB = new Telerik.Reporting.TextBox();
            this.txtYuzdeCC = new Telerik.Reporting.TextBox();
            this.txtYuzdeDC = new Telerik.Reporting.TextBox();
            this.txtYuzdeFF = new Telerik.Reporting.TextBox();
            this.txtYuzdeDZ = new Telerik.Reporting.TextBox();
            this.txtYuzdeGR = new Telerik.Reporting.TextBox();
            this.txtYuzdeDD = new Telerik.Reporting.TextBox();
            this.panel4 = new Telerik.Reporting.Panel();
            this.textBox51 = new Telerik.Reporting.TextBox();
            this.txtToplamSayi = new Telerik.Reporting.TextBox();
            this.txtAritmetikOrtalama = new Telerik.Reporting.TextBox();
            this.textBox52 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.txtSatandartSapma = new Telerik.Reporting.TextBox();
            this.barcode1 = new Telerik.Reporting.Barcode();
            this.txtBilgiIcindir = new Telerik.Reporting.TextBox();
            this.txtIlanTarihiYazi = new Telerik.Reporting.TextBox();
            this.txtIlanTarihi = new Telerik.Reporting.TextBox();
            this.txtImzaYazi = new Telerik.Reporting.TextBox();
            this.txtImzaIsim = new Telerik.Reporting.TextBox();
            this.textBox39 = new Telerik.Reporting.TextBox();
            this.textBox40 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox1
            // 
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.3236474990844727D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox1.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Name = "Tahoma";
            this.textBox1.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // textBox2
            // 
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.27823543548584D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox2.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox2.Style.Font.Bold = true;
            this.textBox2.Style.Font.Name = "Tahoma";
            this.textBox2.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // textBox3
            // 
            this.textBox3.Multiline = false;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.5687066316604614D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox3.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox3.Style.Font.Bold = true;
            this.textBox3.Style.Font.Name = "Tahoma";
            this.textBox3.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // textBox7
            // 
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7286745309829712D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox7.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox7.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox7.Style.Font.Bold = true;
            this.textBox7.Style.Font.Name = "Tahoma";
            this.textBox7.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox7.StyleName = "";
            // 
            // textBox9
            // 
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.3287570476531982D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox9.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox9.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox9.Style.Font.Bold = true;
            this.textBox9.Style.Font.Name = "Tahoma";
            this.textBox9.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox9.StyleName = "";
            // 
            // textBox11
            // 
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.0888068675994873D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox11.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox11.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox11.Style.Font.Bold = true;
            this.textBox11.Style.Font.Name = "Tahoma";
            this.textBox11.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox11.StyleName = "";
            // 
            // textBox19
            // 
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.0621448755264282D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox19.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox19.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox19.Style.Font.Bold = true;
            this.textBox19.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox19.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox19.StyleName = "";
            // 
            // textBox26
            // 
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.055532693862915D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox26.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox26.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox26.Style.Font.Bold = true;
            this.textBox26.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox26.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox26.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox26.StyleName = "";
            // 
            // textBox30
            // 
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1355165243148804D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox30.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox30.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox30.Style.Font.Bold = true;
            this.textBox30.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox30.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox30.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox30.StyleName = "";
            // 
            // textBox32
            // 
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.0821943283081055D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox32.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox32.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox32.Style.Font.Bold = true;
            this.textBox32.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox32.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox32.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox32.StyleName = "";
            // 
            // textBox34
            // 
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9484813213348389D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox34.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox34.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox34.Style.Font.Bold = true;
            this.textBox34.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox34.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox34.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox34.StyleName = "";
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(5.600100040435791D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.txtBaslik,
            this.pLogo,
            this.textBox14,
            this.textBox15,
            this.textBox16,
            this.txtOgretimUyesi,
            this.txtKoduSaati,
            this.txtDersAdi,
            this.textBox20,
            this.textBox21,
            this.textBox22,
            this.txtOgretimTuru,
            this.txtOgretimYili,
            this.txtDonem,
            this.textBox13});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            this.pageHeaderSection1.Style.Font.Name = "Tahoma";
            // 
            // txtBaslik
            // 
            this.txtBaslik.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.00010012308484874666D), Telerik.Reporting.Drawing.Unit.Cm(9.9921220680698752E-05D));
            this.txtBaslik.Name = "txtBaslik";
            this.txtBaslik.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.600498199462891D), Telerik.Reporting.Drawing.Unit.Cm(2.9998002052307129D));
            this.txtBaslik.Style.Font.Bold = true;
            this.txtBaslik.Style.Font.Name = "Tahoma";
            this.txtBaslik.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.txtBaslik.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtBaslik.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtBaslik.Value = "";
            // 
            // pLogo
            // 
            this.pLogo.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.99999988079071045D), Telerik.Reporting.Drawing.Unit.Cm(0.099899925291538239D));
            this.pLogo.MimeType = "";
            this.pLogo.Name = "pLogo";
            this.pLogo.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4000000953674316D), Telerik.Reporting.Drawing.Unit.Cm(2.9000000953674316D));
            this.pLogo.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pLogo.Style.Font.Name = "Tahoma";
            this.pLogo.Value = "";
            // 
            // textBox14
            // 
            this.textBox14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(3.2999999523162842D));
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9000000953674316D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox14.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox14.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox14.Style.Font.Name = "Tahoma";
            this.textBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox14.Value = "Dersin Ad�";
            // 
            // textBox15
            // 
            this.textBox15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(3.8000001907348633D));
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9000000953674316D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox15.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox15.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox15.Style.Font.Name = "Tahoma";
            this.textBox15.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox15.Value = "Kodu / Saati";
            // 
            // textBox16
            // 
            this.textBox16.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(4.2999997138977051D));
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9000000953674316D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox16.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox16.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox16.Style.Font.Name = "Tahoma";
            this.textBox16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox16.Value = "��retim �yesi";
            // 
            // txtOgretimUyesi
            // 
            this.txtOgretimUyesi.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.9002006053924561D), Telerik.Reporting.Drawing.Unit.Cm(4.3000001907348633D));
            this.txtOgretimUyesi.Name = "txtOgretimUyesi";
            this.txtOgretimUyesi.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.1000003814697266D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.txtOgretimUyesi.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtOgretimUyesi.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.txtOgretimUyesi.Style.Font.Name = "Tahoma";
            this.txtOgretimUyesi.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtOgretimUyesi.Value = "��retim �yesi";
            // 
            // txtKoduSaati
            // 
            this.txtKoduSaati.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.9002006053924561D), Telerik.Reporting.Drawing.Unit.Cm(3.8000001907348633D));
            this.txtKoduSaati.Name = "txtKoduSaati";
            this.txtKoduSaati.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.1000003814697266D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.txtKoduSaati.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtKoduSaati.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.txtKoduSaati.Style.Font.Name = "Tahoma";
            this.txtKoduSaati.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtKoduSaati.Value = "Kodu / Saati";
            // 
            // txtDersAdi
            // 
            this.txtDersAdi.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.9002006053924561D), Telerik.Reporting.Drawing.Unit.Cm(3.2999999523162842D));
            this.txtDersAdi.Name = "txtDersAdi";
            this.txtDersAdi.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.1000003814697266D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.txtDersAdi.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtDersAdi.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.txtDersAdi.Style.Font.Name = "Tahoma";
            this.txtDersAdi.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtDersAdi.Value = "Dersin Ad�";
            // 
            // textBox20
            // 
            this.textBox20.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.000400543212891D), Telerik.Reporting.Drawing.Unit.Cm(4.3000001907348633D));
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5999999046325684D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox20.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox20.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox20.Style.Font.Name = "Tahoma";
            this.textBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox20.Value = "D�nem";
            // 
            // textBox21
            // 
            this.textBox21.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.000400543212891D), Telerik.Reporting.Drawing.Unit.Cm(3.8000001907348633D));
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5999999046325684D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox21.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox21.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox21.Style.Font.Name = "Tahoma";
            this.textBox21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox21.Value = "��retim Y�l�";
            // 
            // textBox22
            // 
            this.textBox22.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.000399589538574D), Telerik.Reporting.Drawing.Unit.Cm(3.2999999523162842D));
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5999999046325684D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox22.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox22.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox22.Style.Font.Name = "Tahoma";
            this.textBox22.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox22.Value = "��retim T�r�";
            // 
            // txtOgretimTuru
            // 
            this.txtOgretimTuru.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.60059928894043D), Telerik.Reporting.Drawing.Unit.Cm(3.2999999523162842D));
            this.txtOgretimTuru.Name = "txtOgretimTuru";
            this.txtOgretimTuru.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.txtOgretimTuru.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtOgretimTuru.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.txtOgretimTuru.Style.Font.Name = "Tahoma";
            this.txtOgretimTuru.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtOgretimTuru.Value = "��r. T�r.";
            // 
            // txtOgretimYili
            // 
            this.txtOgretimYili.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.60059928894043D), Telerik.Reporting.Drawing.Unit.Cm(3.7999997138977051D));
            this.txtOgretimYili.Name = "txtOgretimYili";
            this.txtOgretimYili.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.txtOgretimYili.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtOgretimYili.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.txtOgretimYili.Style.Font.Name = "Tahoma";
            this.txtOgretimYili.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtOgretimYili.Value = "Kodu / Saati";
            // 
            // txtDonem
            // 
            this.txtDonem.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.60059928894043D), Telerik.Reporting.Drawing.Unit.Cm(4.3000001907348633D));
            this.txtDonem.Name = "txtDonem";
            this.txtDonem.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.txtDonem.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtDonem.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.txtDonem.Style.Font.Name = "Tahoma";
            this.txtDonem.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtDonem.Value = "D�nem";
            // 
            // textBox13
            // 
            this.textBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.00010012308484874666D), Telerik.Reporting.Drawing.Unit.Cm(5D));
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.60059928894043D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.textBox13.Style.Font.Bold = true;
            this.textBox13.Style.Font.Name = "Tahoma";
            this.textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox13.Value = "YARIYILSONU BA�ARI L�STES�";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Cm(1.2999000549316406D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table1});
            this.detail.Name = "detail";
            this.detail.Style.Font.Name = "Tahoma";
            // 
            // table1
            // 
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.3236474990844727D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.2782368659973145D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.5687068700790405D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.7286748886108398D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.3287571668624878D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.0888068675994873D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.0621448755264282D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.055532693862915D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.1355165243148804D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.0821943283081055D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.948481559753418D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.64994978904724121D)));
            this.table1.Body.SetCellContent(0, 0, this.textBox4);
            this.table1.Body.SetCellContent(0, 1, this.textBox5);
            this.table1.Body.SetCellContent(0, 2, this.textBox6);
            this.table1.Body.SetCellContent(0, 3, this.textBox8);
            this.table1.Body.SetCellContent(0, 4, this.textBox10);
            this.table1.Body.SetCellContent(0, 5, this.textBox12);
            this.table1.Body.SetCellContent(0, 6, this.textBox23);
            this.table1.Body.SetCellContent(0, 7, this.textBox29);
            this.table1.Body.SetCellContent(0, 8, this.textBox31);
            this.table1.Body.SetCellContent(0, 9, this.textBox33);
            this.table1.Body.SetCellContent(0, 10, this.textBox35);
            tableGroup1.ReportItem = this.textBox1;
            tableGroup2.ReportItem = this.textBox2;
            tableGroup3.ReportItem = this.textBox3;
            tableGroup4.Name = "Group1";
            tableGroup4.ReportItem = this.textBox7;
            tableGroup5.Name = "Group2";
            tableGroup5.ReportItem = this.textBox9;
            tableGroup6.Name = "Group3";
            tableGroup6.ReportItem = this.textBox11;
            tableGroup7.Name = "Group4";
            tableGroup7.ReportItem = this.textBox19;
            tableGroup8.Name = "Group5";
            tableGroup8.ReportItem = this.textBox26;
            tableGroup9.Name = "Group6";
            tableGroup9.ReportItem = this.textBox30;
            tableGroup10.Name = "Group7";
            tableGroup10.ReportItem = this.textBox32;
            tableGroup11.Name = "Group8";
            tableGroup11.ReportItem = this.textBox34;
            this.table1.ColumnGroups.Add(tableGroup1);
            this.table1.ColumnGroups.Add(tableGroup2);
            this.table1.ColumnGroups.Add(tableGroup3);
            this.table1.ColumnGroups.Add(tableGroup4);
            this.table1.ColumnGroups.Add(tableGroup5);
            this.table1.ColumnGroups.Add(tableGroup6);
            this.table1.ColumnGroups.Add(tableGroup7);
            this.table1.ColumnGroups.Add(tableGroup8);
            this.table1.ColumnGroups.Add(tableGroup9);
            this.table1.ColumnGroups.Add(tableGroup10);
            this.table1.ColumnGroups.Add(tableGroup11);
            this.table1.Docking = Telerik.Reporting.DockingStyle.Top;
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox4,
            this.textBox5,
            this.textBox6,
            this.textBox8,
            this.textBox10,
            this.textBox12,
            this.textBox23,
            this.textBox29,
            this.textBox31,
            this.textBox33,
            this.textBox35,
            this.textBox1,
            this.textBox2,
            this.textBox3,
            this.textBox7,
            this.textBox9,
            this.textBox11,
            this.textBox19,
            this.textBox26,
            this.textBox30,
            this.textBox32,
            this.textBox34});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.table1.Name = "table1";
            tableGroup12.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup12.Name = "DetailGroup";
            this.table1.RowGroups.Add(tableGroup12);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.600700378417969D), Telerik.Reporting.Drawing.Unit.Cm(1.1499497890472412D));
            this.table1.Style.Font.Name = "Tahoma";
            this.table1.ItemDataBinding += new System.EventHandler(this.table1_ItemDataBinding);
            // 
            // textBox4
            // 
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.3236474990844727D), Telerik.Reporting.Drawing.Unit.Cm(0.64994978904724121D));
            this.textBox4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox4.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox4.Style.Font.Name = "Tahoma";
            this.textBox4.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox4.Value = "=Fields.NUMARA";
            // 
            // textBox5
            // 
            this.textBox5.Multiline = false;
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.278235912322998D), Telerik.Reporting.Drawing.Unit.Cm(0.64994978904724121D));
            this.textBox5.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox5.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox5.Style.Font.Name = "Tahoma";
            this.textBox5.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox5.Value = "=Fields.ADSOYAD";
            // 
            // textBox6
            // 
            this.textBox6.Multiline = false;
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.5687066316604614D), Telerik.Reporting.Drawing.Unit.Cm(0.64994978904724121D));
            this.textBox6.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox6.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox6.Style.Font.Name = "Tahoma";
            this.textBox6.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox6.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox6.Value = "=Fields.abc1";
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7286747694015503D), Telerik.Reporting.Drawing.Unit.Cm(0.64994978904724121D));
            this.textBox8.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox8.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox8.Style.Font.Name = "Tahoma";
            this.textBox8.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox8.StyleName = "";
            // 
            // textBox10
            // 
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.3287571668624878D), Telerik.Reporting.Drawing.Unit.Cm(0.64994978904724121D));
            this.textBox10.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox10.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox10.Style.Font.Name = "Tahoma";
            this.textBox10.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox10.StyleName = "";
            // 
            // textBox12
            // 
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.0888067483901978D), Telerik.Reporting.Drawing.Unit.Cm(0.64994978904724121D));
            this.textBox12.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox12.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox12.Style.Font.Name = "Tahoma";
            this.textBox12.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox12.StyleName = "";
            this.textBox12.Value = "";
            // 
            // textBox23
            // 
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.0621448755264282D), Telerik.Reporting.Drawing.Unit.Cm(0.64994978904724121D));
            this.textBox23.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox23.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox23.Style.Font.Name = "Tahoma";
            this.textBox23.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox23.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox23.StyleName = "";
            this.textBox23.Value = "";
            // 
            // textBox29
            // 
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.055532693862915D), Telerik.Reporting.Drawing.Unit.Cm(0.64994978904724121D));
            this.textBox29.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox29.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox29.Style.Font.Name = "Tahoma";
            this.textBox29.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox29.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox29.StyleName = "";
            this.textBox29.Value = "";
            // 
            // textBox31
            // 
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1355164051055908D), Telerik.Reporting.Drawing.Unit.Cm(0.64994978904724121D));
            this.textBox31.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox31.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox31.Style.Font.Name = "Tahoma";
            this.textBox31.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox31.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox31.StyleName = "";
            this.textBox31.Value = "";
            // 
            // textBox33
            // 
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.0821943283081055D), Telerik.Reporting.Drawing.Unit.Cm(0.64994978904724121D));
            this.textBox33.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox33.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox33.Style.Font.Name = "Tahoma";
            this.textBox33.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox33.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox33.StyleName = "";
            this.textBox33.Value = "";
            // 
            // textBox35
            // 
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9484817981719971D), Telerik.Reporting.Drawing.Unit.Cm(0.64994978904724121D));
            this.textBox35.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox35.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox35.Style.Font.Name = "Tahoma";
            this.textBox35.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox35.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox35.StyleName = "";
            this.textBox35.Value = "";
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(7.4776401519775391D);
            this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox17,
            this.textBox18,
            this.textBox24,
            this.textBox27,
            this.textBox28,
            this.panel1,
            this.barcode1,
            this.txtBilgiIcindir,
            this.txtIlanTarihiYazi,
            this.txtIlanTarihi,
            this.txtImzaYazi,
            this.txtImzaIsim});
            this.pageFooterSection1.Name = "pageFooterSection1";
            this.pageFooterSection1.Style.Font.Name = "Tahoma";
            // 
            // textBox17
            // 
            this.textBox17.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.295553207397461D), Telerik.Reporting.Drawing.Unit.Cm(4.8743062019348145D));
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.3050448894500732D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.textBox17.Style.Font.Bold = false;
            this.textBox17.Style.Font.Name = "Tahoma";
            this.textBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox17.Value = "72.01.FR.10";
            // 
            // textBox18
            // 
            this.textBox18.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.095552444458008D), Telerik.Reporting.Drawing.Unit.Cm(6.67380428314209D));
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5050458908081055D), Telerik.Reporting.Drawing.Unit.Cm(0.30010038614273071D));
            this.textBox18.Style.Font.Bold = false;
            this.textBox18.Style.Font.Name = "Tahoma";
            this.textBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox18.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox18.Value = "SAB�S � 2013 SA�";
            // 
            // textBox24
            // 
            this.textBox24.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(6.6738033294677734D));
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.300000190734863D), Telerik.Reporting.Drawing.Unit.Cm(0.30010038614273071D));
            this.textBox24.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox24.Style.Visible = false;
            this.textBox24.Value = "SAB�S :: Akademik Bilgi Sistemi :: https://abs.sakarya.edu.tr";
            // 
            // textBox27
            // 
            this.textBox27.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.00010012308484874666D), Telerik.Reporting.Drawing.Unit.Cm(5.47450590133667D));
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.1998996734619141D), Telerik.Reporting.Drawing.Unit.Cm(0.599999725818634D));
            this.textBox27.Value = "= \"Sayfa \" + PageNumber + \"/\"+ PageCount";
            // 
            // textBox28
            // 
            this.textBox28.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.00010012308484874666D), Telerik.Reporting.Drawing.Unit.Cm(4.8743062019348145D));
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.1998996734619141D), Telerik.Reporting.Drawing.Unit.Cm(0.599999725818634D));
            this.textBox28.Value = "01";
            // 
            // panel1
            // 
            this.panel1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox36,
            this.panel2,
            this.panel4});
            this.panel1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.3076391220092773D), Telerik.Reporting.Drawing.Unit.Cm(0.20763857662677765D));
            this.panel1.Name = "panel1";
            this.panel1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.892359733581543D), Telerik.Reporting.Drawing.Unit.Cm(3.3000004291534424D));
            // 
            // textBox36
            // 
            this.textBox36.Anchoring = ((Telerik.Reporting.AnchoringStyles)(((Telerik.Reporting.AnchoringStyles.Top | Telerik.Reporting.AnchoringStyles.Left) 
            | Telerik.Reporting.AnchoringStyles.Right)));
            this.textBox36.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0.00010012308484874666D));
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.892258644104004D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox36.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox36.Style.Font.Bold = true;
            this.textBox36.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox36.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox36.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox36.Value = "�STAT�ST�KLER";
            // 
            // panel2
            // 
            this.panel2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.panel3,
            this.textBox41,
            this.textBox42,
            this.textBox43,
            this.textBox44,
            this.textBox45,
            this.textBox46,
            this.textBox47,
            this.textBox48,
            this.textBox49,
            this.textBox50,
            this.txtSayiGR,
            this.txtSayiDZ,
            this.txtSayiFF,
            this.txtSayiDD,
            this.txtSayiDC,
            this.txtSayiCC,
            this.txtSayiCB,
            this.txtSayiBB,
            this.txtSayiBA,
            this.txtSayiAA,
            this.txtYuzdeAA,
            this.txtYuzdeBA,
            this.txtYuzdeBB,
            this.txtYuzdeCB,
            this.txtYuzdeCC,
            this.txtYuzdeDC,
            this.txtYuzdeFF,
            this.txtYuzdeDZ,
            this.txtYuzdeGR,
            this.txtYuzdeDD});
            this.panel2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0.70000004768371582D));
            this.panel2.Name = "panel2";
            this.panel2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.892359733581543D), Telerik.Reporting.Drawing.Unit.Cm(1.9999997615814209D));
            this.panel2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel2.Style.LineColor = System.Drawing.Color.Black;
            this.panel2.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            // 
            // panel3
            // 
            this.panel3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox37,
            this.textBox38});
            this.panel3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.panel3.Name = "panel3";
            this.panel3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(1.9998996257781982D));
            this.panel3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel3.Style.LineColor = System.Drawing.Color.Black;
            this.panel3.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            // 
            // textBox37
            // 
            this.textBox37.Anchoring = ((Telerik.Reporting.AnchoringStyles)(((Telerik.Reporting.AnchoringStyles.Top | Telerik.Reporting.AnchoringStyles.Left) 
            | Telerik.Reporting.AnchoringStyles.Right)));
            this.textBox37.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.00010012308484874666D), Telerik.Reporting.Drawing.Unit.Cm(0.80000042915344238D));
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9997987747192383D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox37.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox37.Style.Font.Bold = true;
            this.textBox37.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox37.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox37.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox37.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox37.Value = "SAYI";
            // 
            // textBox38
            // 
            this.textBox38.Anchoring = ((Telerik.Reporting.AnchoringStyles)(((Telerik.Reporting.AnchoringStyles.Top | Telerik.Reporting.AnchoringStyles.Left) 
            | Telerik.Reporting.AnchoringStyles.Right)));
            this.textBox38.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(1.4000009298324585D));
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9997987747192383D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox38.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox38.Style.Font.Bold = true;
            this.textBox38.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox38.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox38.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox38.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox38.Value = "Y�ZDE (%)";
            // 
            // textBox41
            // 
            this.textBox41.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.1000001430511475D), Telerik.Reporting.Drawing.Unit.Cm(0.19999989867210388D));
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.textBox41.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox41.Style.Font.Bold = true;
            this.textBox41.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox41.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox41.Value = "GR";
            // 
            // textBox42
            // 
            this.textBox42.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.0695829391479492D), Telerik.Reporting.Drawing.Unit.Cm(0.19999989867210388D));
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.textBox42.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox42.Style.Font.Bold = true;
            this.textBox42.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox42.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox42.Value = "DZ";
            // 
            // textBox43
            // 
            this.textBox43.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.03916597366333D), Telerik.Reporting.Drawing.Unit.Cm(0.19999989867210388D));
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.textBox43.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox43.Style.Font.Bold = true;
            this.textBox43.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox43.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox43.Value = "FF";
            // 
            // textBox44
            // 
            this.textBox44.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.0087485313415527D), Telerik.Reporting.Drawing.Unit.Cm(0.19999989867210388D));
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.textBox44.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox44.Style.Font.Bold = true;
            this.textBox44.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox44.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox44.Value = "DD";
            // 
            // textBox45
            // 
            this.textBox45.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.9783315658569336D), Telerik.Reporting.Drawing.Unit.Cm(0.19999989867210388D));
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.textBox45.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox45.Style.Font.Bold = true;
            this.textBox45.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox45.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox45.Value = "DC";
            // 
            // textBox46
            // 
            this.textBox46.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.9479146003723145D), Telerik.Reporting.Drawing.Unit.Cm(0.19999989867210388D));
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.textBox46.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox46.Style.Font.Bold = true;
            this.textBox46.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox46.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox46.Value = "CC";
            // 
            // textBox47
            // 
            this.textBox47.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.9174976348876953D), Telerik.Reporting.Drawing.Unit.Cm(0.19999989867210388D));
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.textBox47.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox47.Style.Font.Bold = true;
            this.textBox47.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox47.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox47.Value = "CB";
            // 
            // textBox48
            // 
            this.textBox48.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.887080192565918D), Telerik.Reporting.Drawing.Unit.Cm(0.19999989867210388D));
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.textBox48.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox48.Style.Font.Bold = true;
            this.textBox48.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox48.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox48.Value = "BB";
            // 
            // textBox49
            // 
            this.textBox49.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.856663703918457D), Telerik.Reporting.Drawing.Unit.Cm(0.19999989867210388D));
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.textBox49.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox49.Style.Font.Bold = true;
            this.textBox49.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox49.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox49.Value = "BA";
            // 
            // textBox50
            // 
            this.textBox50.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.826245307922363D), Telerik.Reporting.Drawing.Unit.Cm(0.19999989867210388D));
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.textBox50.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox50.Style.Font.Bold = true;
            this.textBox50.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox50.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox50.Value = "AA";
            // 
            // txtSayiGR
            // 
            this.txtSayiGR.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.1000001430511475D), Telerik.Reporting.Drawing.Unit.Cm(0.79999959468841553D));
            this.txtSayiGR.Name = "txtSayiGR";
            this.txtSayiGR.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.txtSayiGR.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtSayiGR.Style.Font.Bold = false;
            this.txtSayiGR.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtSayiGR.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtSayiGR.Value = "0";
            // 
            // txtSayiDZ
            // 
            this.txtSayiDZ.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.0695829391479492D), Telerik.Reporting.Drawing.Unit.Cm(0.79999959468841553D));
            this.txtSayiDZ.Name = "txtSayiDZ";
            this.txtSayiDZ.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.txtSayiDZ.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtSayiDZ.Style.Font.Bold = false;
            this.txtSayiDZ.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtSayiDZ.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtSayiDZ.Value = "0";
            // 
            // txtSayiFF
            // 
            this.txtSayiFF.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.0391654968261719D), Telerik.Reporting.Drawing.Unit.Cm(0.79999959468841553D));
            this.txtSayiFF.Name = "txtSayiFF";
            this.txtSayiFF.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.txtSayiFF.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtSayiFF.Style.Font.Bold = false;
            this.txtSayiFF.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtSayiFF.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtSayiFF.Value = "0";
            // 
            // txtSayiDD
            // 
            this.txtSayiDD.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.0087480545043945D), Telerik.Reporting.Drawing.Unit.Cm(0.79999959468841553D));
            this.txtSayiDD.Name = "txtSayiDD";
            this.txtSayiDD.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.txtSayiDD.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtSayiDD.Style.Font.Bold = false;
            this.txtSayiDD.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtSayiDD.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtSayiDD.Value = "0";
            // 
            // txtSayiDC
            // 
            this.txtSayiDC.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.9783310890197754D), Telerik.Reporting.Drawing.Unit.Cm(0.79999959468841553D));
            this.txtSayiDC.Name = "txtSayiDC";
            this.txtSayiDC.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.txtSayiDC.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtSayiDC.Style.Font.Bold = false;
            this.txtSayiDC.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtSayiDC.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtSayiDC.Value = "0";
            // 
            // txtSayiCC
            // 
            this.txtSayiCC.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.947913646697998D), Telerik.Reporting.Drawing.Unit.Cm(0.79999959468841553D));
            this.txtSayiCC.Name = "txtSayiCC";
            this.txtSayiCC.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.txtSayiCC.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtSayiCC.Style.Font.Bold = false;
            this.txtSayiCC.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtSayiCC.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtSayiCC.Value = "0";
            // 
            // txtSayiCB
            // 
            this.txtSayiCB.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.9174966812133789D), Telerik.Reporting.Drawing.Unit.Cm(0.79999959468841553D));
            this.txtSayiCB.Name = "txtSayiCB";
            this.txtSayiCB.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.txtSayiCB.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtSayiCB.Style.Font.Bold = false;
            this.txtSayiCB.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtSayiCB.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtSayiCB.Value = "0";
            // 
            // txtSayiBB
            // 
            this.txtSayiBB.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.8870792388916016D), Telerik.Reporting.Drawing.Unit.Cm(0.79999959468841553D));
            this.txtSayiBB.Name = "txtSayiBB";
            this.txtSayiBB.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.txtSayiBB.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtSayiBB.Style.Font.Bold = false;
            this.txtSayiBB.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtSayiBB.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtSayiBB.Value = "0";
            // 
            // txtSayiBA
            // 
            this.txtSayiBA.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.85666275024414D), Telerik.Reporting.Drawing.Unit.Cm(0.79999959468841553D));
            this.txtSayiBA.Name = "txtSayiBA";
            this.txtSayiBA.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.txtSayiBA.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtSayiBA.Style.Font.Bold = false;
            this.txtSayiBA.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtSayiBA.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtSayiBA.Value = "0";
            // 
            // txtSayiAA
            // 
            this.txtSayiAA.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.826244354248047D), Telerik.Reporting.Drawing.Unit.Cm(0.79999959468841553D));
            this.txtSayiAA.Name = "txtSayiAA";
            this.txtSayiAA.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.txtSayiAA.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtSayiAA.Style.Font.Bold = false;
            this.txtSayiAA.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtSayiAA.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtSayiAA.Value = "0";
            // 
            // txtYuzdeAA
            // 
            this.txtYuzdeAA.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.829160690307617D), Telerik.Reporting.Drawing.Unit.Cm(1.4000000953674316D));
            this.txtYuzdeAA.Name = "txtYuzdeAA";
            this.txtYuzdeAA.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.txtYuzdeAA.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtYuzdeAA.Style.Font.Bold = false;
            this.txtYuzdeAA.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtYuzdeAA.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtYuzdeAA.Value = "0";
            // 
            // txtYuzdeBA
            // 
            this.txtYuzdeBA.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.859950065612793D), Telerik.Reporting.Drawing.Unit.Cm(1.4000000953674316D));
            this.txtYuzdeBA.Name = "txtYuzdeBA";
            this.txtYuzdeBA.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.txtYuzdeBA.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtYuzdeBA.Style.Font.Bold = false;
            this.txtYuzdeBA.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtYuzdeBA.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtYuzdeBA.Value = "0";
            // 
            // txtYuzdeBB
            // 
            this.txtYuzdeBB.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.8907375335693359D), Telerik.Reporting.Drawing.Unit.Cm(1.4000000953674316D));
            this.txtYuzdeBB.Name = "txtYuzdeBB";
            this.txtYuzdeBB.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.txtYuzdeBB.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtYuzdeBB.Style.Font.Bold = false;
            this.txtYuzdeBB.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtYuzdeBB.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtYuzdeBB.Value = "0";
            // 
            // txtYuzdeCB
            // 
            this.txtYuzdeCB.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.9215250015258789D), Telerik.Reporting.Drawing.Unit.Cm(1.4000000953674316D));
            this.txtYuzdeCB.Name = "txtYuzdeCB";
            this.txtYuzdeCB.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.txtYuzdeCB.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtYuzdeCB.Style.Font.Bold = false;
            this.txtYuzdeCB.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtYuzdeCB.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtYuzdeCB.Value = "0";
            // 
            // txtYuzdeCC
            // 
            this.txtYuzdeCC.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.9523124694824219D), Telerik.Reporting.Drawing.Unit.Cm(1.4000000953674316D));
            this.txtYuzdeCC.Name = "txtYuzdeCC";
            this.txtYuzdeCC.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.txtYuzdeCC.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtYuzdeCC.Style.Font.Bold = false;
            this.txtYuzdeCC.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtYuzdeCC.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtYuzdeCC.Value = "0";
            // 
            // txtYuzdeDC
            // 
            this.txtYuzdeDC.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.983100414276123D), Telerik.Reporting.Drawing.Unit.Cm(1.4000000953674316D));
            this.txtYuzdeDC.Name = "txtYuzdeDC";
            this.txtYuzdeDC.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.txtYuzdeDC.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtYuzdeDC.Style.Font.Bold = false;
            this.txtYuzdeDC.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtYuzdeDC.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtYuzdeDC.Value = "0";
            // 
            // txtYuzdeFF
            // 
            this.txtYuzdeFF.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.044675350189209D), Telerik.Reporting.Drawing.Unit.Cm(1.4000000953674316D));
            this.txtYuzdeFF.Name = "txtYuzdeFF";
            this.txtYuzdeFF.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.txtYuzdeFF.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtYuzdeFF.Style.Font.Bold = false;
            this.txtYuzdeFF.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtYuzdeFF.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtYuzdeFF.Value = "99,99";
            // 
            // txtYuzdeDZ
            // 
            this.txtYuzdeDZ.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.075462818145752D), Telerik.Reporting.Drawing.Unit.Cm(1.4000000953674316D));
            this.txtYuzdeDZ.Name = "txtYuzdeDZ";
            this.txtYuzdeDZ.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.txtYuzdeDZ.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtYuzdeDZ.Style.Font.Bold = false;
            this.txtYuzdeDZ.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtYuzdeDZ.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtYuzdeDZ.Value = "0";
            // 
            // txtYuzdeGR
            // 
            this.txtYuzdeGR.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.106250524520874D), Telerik.Reporting.Drawing.Unit.Cm(1.4000000953674316D));
            this.txtYuzdeGR.Name = "txtYuzdeGR";
            this.txtYuzdeGR.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.txtYuzdeGR.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtYuzdeGR.Style.Font.Bold = false;
            this.txtYuzdeGR.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtYuzdeGR.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtYuzdeGR.Value = "0";
            // 
            // txtYuzdeDD
            // 
            this.txtYuzdeDD.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.013887882232666D), Telerik.Reporting.Drawing.Unit.Cm(1.4000000953674316D));
            this.txtYuzdeDD.Name = "txtYuzdeDD";
            this.txtYuzdeDD.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.txtYuzdeDD.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtYuzdeDD.Style.Font.Bold = false;
            this.txtYuzdeDD.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtYuzdeDD.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtYuzdeDD.Value = "0";
            // 
            // panel4
            // 
            this.panel4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox51,
            this.txtToplamSayi,
            this.txtAritmetikOrtalama,
            this.textBox52,
            this.textBox25,
            this.txtSatandartSapma});
            this.panel4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.00010012308484874666D), Telerik.Reporting.Drawing.Unit.Cm(2.700200080871582D));
            this.panel4.Name = "panel4";
            this.panel4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.892159461975098D), Telerik.Reporting.Drawing.Unit.Cm(0.60010063648223877D));
            this.panel4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            // 
            // textBox51
            // 
            this.textBox51.Anchoring = ((Telerik.Reporting.AnchoringStyles)(((Telerik.Reporting.AnchoringStyles.Top | Telerik.Reporting.AnchoringStyles.Left) 
            | Telerik.Reporting.AnchoringStyles.Right)));
            this.textBox51.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2996997833251953D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox51.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox51.Style.Font.Bold = true;
            this.textBox51.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox51.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox51.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox51.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox51.Value = "��renci Say�s�:";
            // 
            // txtToplamSayi
            // 
            this.txtToplamSayi.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.1998996734619141D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.txtToplamSayi.Name = "txtToplamSayi";
            this.txtToplamSayi.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1778382062911987D), Telerik.Reporting.Drawing.Unit.Cm(0.49990004301071167D));
            this.txtToplamSayi.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtToplamSayi.Value = "999";
            // 
            // txtAritmetikOrtalama
            // 
            this.txtAritmetikOrtalama.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.6005120277404785D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.txtAritmetikOrtalama.Name = "txtAritmetikOrtalama";
            this.txtAritmetikOrtalama.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2917482852935791D), Telerik.Reporting.Drawing.Unit.Cm(0.49990004301071167D));
            this.txtAritmetikOrtalama.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtAritmetikOrtalama.Value = "99,999";
            // 
            // textBox52
            // 
            this.textBox52.Anchoring = ((Telerik.Reporting.AnchoringStyles)(((Telerik.Reporting.AnchoringStyles.Top | Telerik.Reporting.AnchoringStyles.Left) 
            | Telerik.Reporting.AnchoringStyles.Right)));
            this.textBox52.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.3779380321502686D), Telerik.Reporting.Drawing.Unit.Cm(0.00010012308484874666D));
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.3160223960876465D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox52.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox52.Style.Font.Bold = true;
            this.textBox52.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox52.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox52.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox52.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox52.Value = "Aritmetik Ortalama:";
            // 
            // textBox25
            // 
            this.textBox25.Anchoring = ((Telerik.Reporting.AnchoringStyles)(((Telerik.Reporting.AnchoringStyles.Top | Telerik.Reporting.AnchoringStyles.Left) 
            | Telerik.Reporting.AnchoringStyles.Right)));
            this.textBox25.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.8924603462219238D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5998008251190186D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox25.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox25.Style.Font.Bold = true;
            this.textBox25.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox25.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox25.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox25.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox25.Value = "Standart Sapma:";
            // 
            // txtSatandartSapma
            // 
            this.txtSatandartSapma.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.392460823059082D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.txtSatandartSapma.Name = "txtSatandartSapma";
            this.txtSatandartSapma.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.3995977640151978D), Telerik.Reporting.Drawing.Unit.Cm(0.49990004301071167D));
            this.txtSatandartSapma.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtSatandartSapma.Value = "99,999";
            // 
            // barcode1
            // 
            this.barcode1.BarAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.barcode1.Encoder = code128Encoder1;
            this.barcode1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.2999997138977051D), Telerik.Reporting.Drawing.Unit.Cm(4.8743066787719727D));
            this.barcode1.Name = "barcode1";
            this.barcode1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.0168290138244629D), Telerik.Reporting.Drawing.Unit.Cm(1.2001991271972656D));
            this.barcode1.Value = "12345678-1234567891";
            // 
            // txtBilgiIcindir
            // 
            this.txtBilgiIcindir.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.2999997138977051D), Telerik.Reporting.Drawing.Unit.Cm(4.8743066787719727D));
            this.txtBilgiIcindir.Name = "txtBilgiIcindir";
            this.txtBilgiIcindir.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.8001995086669922D), Telerik.Reporting.Drawing.Unit.Cm(0.99999922513961792D));
            this.txtBilgiIcindir.Style.Color = System.Drawing.Color.Red;
            this.txtBilgiIcindir.Style.Font.Bold = true;
            this.txtBilgiIcindir.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(16D);
            this.txtBilgiIcindir.Style.Visible = false;
            this.txtBilgiIcindir.Value = "B�LG� ���ND�R";
            // 
            // txtIlanTarihiYazi
            // 
            this.txtIlanTarihiYazi.Anchoring = ((Telerik.Reporting.AnchoringStyles)(((Telerik.Reporting.AnchoringStyles.Top | Telerik.Reporting.AnchoringStyles.Left) 
            | Telerik.Reporting.AnchoringStyles.Right)));
            this.txtIlanTarihiYazi.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.3076391220092773D), Telerik.Reporting.Drawing.Unit.Cm(3.7000000476837158D));
            this.txtIlanTarihiYazi.Name = "txtIlanTarihiYazi";
            this.txtIlanTarihiYazi.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.5923607349395752D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.txtIlanTarihiYazi.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.txtIlanTarihiYazi.Style.Font.Bold = true;
            this.txtIlanTarihiYazi.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtIlanTarihiYazi.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.txtIlanTarihiYazi.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txtIlanTarihiYazi.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtIlanTarihiYazi.Style.Visible = false;
            this.txtIlanTarihiYazi.Value = "�lan Tarihi:";
            // 
            // txtIlanTarihi
            // 
            this.txtIlanTarihi.Anchoring = ((Telerik.Reporting.AnchoringStyles)(((Telerik.Reporting.AnchoringStyles.Top | Telerik.Reporting.AnchoringStyles.Left) 
            | Telerik.Reporting.AnchoringStyles.Right)));
            this.txtIlanTarihi.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6D), Telerik.Reporting.Drawing.Unit.Cm(3.7000000476837158D));
            this.txtIlanTarihi.Name = "txtIlanTarihi";
            this.txtIlanTarihi.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1937503814697266D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.txtIlanTarihi.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.txtIlanTarihi.Style.Font.Bold = true;
            this.txtIlanTarihi.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtIlanTarihi.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.txtIlanTarihi.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txtIlanTarihi.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtIlanTarihi.Style.Visible = false;
            this.txtIlanTarihi.Value = "01.01.2013 00:00";
            // 
            // txtImzaYazi
            // 
            this.txtImzaYazi.Anchoring = ((Telerik.Reporting.AnchoringStyles)(((Telerik.Reporting.AnchoringStyles.Top | Telerik.Reporting.AnchoringStyles.Left) 
            | Telerik.Reporting.AnchoringStyles.Right)));
            this.txtImzaYazi.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.107639312744141D), Telerik.Reporting.Drawing.Unit.Cm(3.7000000476837158D));
            this.txtImzaYazi.Name = "txtImzaYazi";
            this.txtImzaYazi.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.5923607349395752D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.txtImzaYazi.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.txtImzaYazi.Style.Font.Bold = true;
            this.txtImzaYazi.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtImzaYazi.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.txtImzaYazi.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtImzaYazi.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtImzaYazi.Style.Visible = false;
            this.txtImzaYazi.Value = "�mza";
            // 
            // txtImzaIsim
            // 
            this.txtImzaIsim.Anchoring = ((Telerik.Reporting.AnchoringStyles)(((Telerik.Reporting.AnchoringStyles.Top | Telerik.Reporting.AnchoringStyles.Left) 
            | Telerik.Reporting.AnchoringStyles.Right)));
            this.txtImzaIsim.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.5D), Telerik.Reporting.Drawing.Unit.Cm(4.20020055770874D));
            this.txtImzaIsim.Name = "txtImzaIsim";
            this.txtImzaIsim.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.3999996185302734D), Telerik.Reporting.Drawing.Unit.Cm(0.77390605211257935D));
            this.txtImzaIsim.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.txtImzaIsim.Style.Font.Bold = true;
            this.txtImzaIsim.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtImzaIsim.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.txtImzaIsim.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtImzaIsim.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.txtImzaIsim.Style.Visible = false;
            this.txtImzaIsim.Value = "Prof.Dr. Hayrullah Hasano�luo�ullar�";
            // 
            // textBox39
            // 
            this.textBox39.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.1000001430511475D), Telerik.Reporting.Drawing.Unit.Cm(0.10000035166740418D));
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.59999954700469971D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox39.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox39.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox39.Style.Font.Bold = true;
            this.textBox39.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox39.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox39.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox39.Value = "GR";
            // 
            // textBox40
            // 
            this.textBox40.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.1000001430511475D), Telerik.Reporting.Drawing.Unit.Cm(0.10000035166740418D));
            this.textBox40.Name = "textBox39";
            this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.59999954700469971D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox40.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox40.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox40.Value = "GR";
            // 
            // BasariListesi
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1});
            this.Name = "BasariListesi";
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Mm(5.4000000953674316D), Telerik.Reporting.Drawing.Unit.Mm(5.4000000953674316D), Telerik.Reporting.Drawing.Unit.Mm(5.4000000953674316D), Telerik.Reporting.Drawing.Unit.Mm(5.4000000953674316D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Style.BackgroundColor = System.Drawing.Color.White;
            this.Width = Telerik.Reporting.Drawing.Unit.Cm(19.600698471069336D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.TextBox txtBaslik;
        private Telerik.Reporting.PictureBox pLogo;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox txtOgretimUyesi;
        private Telerik.Reporting.TextBox txtKoduSaati;
        private Telerik.Reporting.TextBox txtDersAdi;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox txtOgretimTuru;
        private Telerik.Reporting.TextBox txtOgretimYili;
        private Telerik.Reporting.TextBox txtDonem;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox35;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.TextBox textBox40;
        private Telerik.Reporting.Panel panel1;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.Panel panel2;
        private Telerik.Reporting.Panel panel3;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.TextBox textBox38;
        private Telerik.Reporting.TextBox textBox41;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.TextBox textBox44;
        private Telerik.Reporting.TextBox textBox45;
        private Telerik.Reporting.TextBox textBox46;
        private Telerik.Reporting.TextBox textBox47;
        private Telerik.Reporting.TextBox textBox48;
        private Telerik.Reporting.TextBox textBox49;
        private Telerik.Reporting.TextBox textBox50;
        private Telerik.Reporting.TextBox txtSayiGR;
        private Telerik.Reporting.TextBox txtSayiDZ;
        private Telerik.Reporting.TextBox txtSayiFF;
        private Telerik.Reporting.TextBox txtSayiDD;
        private Telerik.Reporting.TextBox txtSayiDC;
        private Telerik.Reporting.TextBox txtSayiCC;
        private Telerik.Reporting.TextBox txtSayiCB;
        private Telerik.Reporting.TextBox txtSayiBB;
        private Telerik.Reporting.TextBox txtSayiBA;
        private Telerik.Reporting.TextBox txtSayiAA;
        private Telerik.Reporting.TextBox txtYuzdeAA;
        private Telerik.Reporting.TextBox txtYuzdeBA;
        private Telerik.Reporting.TextBox txtYuzdeBB;
        private Telerik.Reporting.TextBox txtYuzdeCB;
        private Telerik.Reporting.TextBox txtYuzdeCC;
        private Telerik.Reporting.TextBox txtYuzdeDC;
        private Telerik.Reporting.TextBox txtYuzdeFF;
        private Telerik.Reporting.TextBox txtYuzdeDZ;
        private Telerik.Reporting.TextBox txtYuzdeGR;
        private Telerik.Reporting.TextBox txtYuzdeDD;
        private Telerik.Reporting.Panel panel4;
        private Telerik.Reporting.TextBox textBox51;
        private Telerik.Reporting.TextBox txtToplamSayi;
        private Telerik.Reporting.TextBox txtAritmetikOrtalama;
        private Telerik.Reporting.TextBox textBox52;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox txtSatandartSapma;
        private Telerik.Reporting.Barcode barcode1;
        private Telerik.Reporting.TextBox txtBilgiIcindir;
        private Telerik.Reporting.TextBox txtIlanTarihiYazi;
        private Telerik.Reporting.TextBox txtIlanTarihi;
        private Telerik.Reporting.TextBox txtImzaYazi;
        private Telerik.Reporting.TextBox txtImzaIsim;
    }
}