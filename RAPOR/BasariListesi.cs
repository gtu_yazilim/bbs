namespace Sabis.Bolum.Rapor
{
    using System;
    using System.Drawing;
    using Telerik.Reporting.Drawing;
    using System.Data;
    using System.Linq;
    using System.Collections.Generic;
    using Sabis.Enum;
    using Sabis.Bolum.Core.ABS.DEGERLENDIRME;
    public partial class BasariListesi : Telerik.Reporting.Report
    {
        DataTable veri = new DataTable();
        public BasariListesi(BasariListesiDTO _gelen, string universiteAdi, string logoUrl)
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();
            this.DocumentName = string.Format("{0}_{1}_{2}.pdf", _gelen.Ders.DersPlanID, DateTime.Now.ToString("MMdd_HHmmss"), "basari");
            //table1.DataSource = datatable;
            //txtBaslik.Value = "T.C.\nSAKARYA �N�VERS�TES�\n" + _gelen.Fakulte.BirimAdi + "\n" + _gelen.Bolum.BirimAdi;
            txtBaslik.Value = universiteAdi.Replace("\\n", Environment.NewLine) + "\n" + _gelen.Fakulte.BirimAdi + "\n" + _gelen.Bolum.BirimAdi;
            txtDersAdi.Value = _gelen.Ders.DersAd + " (" + _gelen.Ders.DersGrupAd + ")";
            txtDonem.Value = Sabis.Enum.Utils.GetEnumDescription((EnumDonem)_gelen.Ders.Donem);
            if (_gelen.Ders.ButunlemeMi)
            {
                txtDonem.Value += "-B�t�nleme";
            }
            txtKoduSaati.Value = _gelen.Ders.DersKod + " / " + _gelen.Ders.DSaat + "+" + _gelen.Ders.USaat;
            txtOgretimTuru.Value = _gelen.Ders.OgretimTuruAd;
            txtOgretimUyesi.Value = string.Format("{0} {1} {2}", _gelen.Ders.Personel.UnvanAd, _gelen.Ders.Personel.Ad, _gelen.Ders.Personel.Soyad);
            txtOgretimYili.Value = string.Format("{0}-{1}", _gelen.Ders.Yil.ToString(), (_gelen.Ders.Yil + 1).ToString());
            txtToplamSayi.Value = _gelen.NotList.Count.ToString();

            pLogo.Value = logoUrl;

            if (_gelen.Barkod != null)
            {
                barcode1.Value = _gelen.Barkod;
                barcode1.Visible = true;
                txtBilgiIcindir.Visible = false;
                txtIlanTarihiYazi.Visible = true;
                txtIlanTarihi.Visible = true;
                txtIlanTarihi.Value = _gelen.SonHalTarihi.ToString("dd/MM/yyyy HH:mm:ss");
                txtImzaYazi.Visible = true;
                txtImzaIsim.Visible = true;
                txtImzaIsim.Value = string.Format("{0} {1} {2}", _gelen.Ders.Personel.UnvanAd, _gelen.Ders.Personel.Ad, _gelen.Ders.Personel.Soyad);
            }
            else
            {
                barcode1.Visible = false;
                txtBilgiIcindir.Visible = true;

                txtImzaYazi.Visible = false;
                txtImzaIsim.Visible = false;
                txtIlanTarihiYazi.Visible = false;
                txtIlanTarihi.Visible = false;
            }

            List<OgrenciPayveNotBilgileriDTO> bilgiler = _gelen.NotList.OrderBy(x => x.BasariHarf).ThenByDescending(x => x.MutlakNot).ToList();
            bilgiler.Where(x => x.BasariHarf == EnumHarfBasari.AA).Count().ToString();

            txtSayiAA.Value = bilgiler.Where(x => x.BasariHarf == EnumHarfBasari.AA).Count().ToString();
            txtSayiBA.Value = bilgiler.Where(x => x.BasariHarf == EnumHarfBasari.BA).Count().ToString();
            txtSayiBB.Value = bilgiler.Where(x => x.BasariHarf == EnumHarfBasari.BB).Count().ToString();
            txtSayiCB.Value = bilgiler.Where(x => x.BasariHarf == EnumHarfBasari.CB).Count().ToString();
            txtSayiCC.Value = bilgiler.Where(x => x.BasariHarf == EnumHarfBasari.CC).Count().ToString();
            txtSayiDC.Value = bilgiler.Where(x => x.BasariHarf == EnumHarfBasari.DC).Count().ToString();
            txtSayiDD.Value = bilgiler.Where(x => x.BasariHarf == EnumHarfBasari.DD).Count().ToString();
            txtSayiFF.Value = bilgiler.Where(x => x.BasariHarf == EnumHarfBasari.FF).Count().ToString();
            txtSayiDZ.Value = bilgiler.Where(x => x.BasariHarf == EnumHarfBasari.DZ).Count().ToString();
            txtSayiGR.Value = bilgiler.Where(x => x.BasariHarf == EnumHarfBasari.GR).Count().ToString();

            double topAdet = (double)bilgiler.Count();
            txtYuzdeAA.Value = Math.Round((double)bilgiler.Where(x => x.BasariHarf == EnumHarfBasari.AA).Count() * 100 / topAdet, 2).ToString();
            txtYuzdeBA.Value = Math.Round((double)bilgiler.Where(x => x.BasariHarf == EnumHarfBasari.BA).Count() * 100 / topAdet, 2).ToString();
            txtYuzdeBB.Value = Math.Round((double)bilgiler.Where(x => x.BasariHarf == EnumHarfBasari.BB).Count() * 100 / topAdet, 2).ToString();
            txtYuzdeCB.Value = Math.Round((double)bilgiler.Where(x => x.BasariHarf == EnumHarfBasari.CB).Count() * 100 / topAdet, 2).ToString();
            txtYuzdeCC.Value = Math.Round((double)bilgiler.Where(x => x.BasariHarf == EnumHarfBasari.CC).Count() * 100 / topAdet, 2).ToString();
            txtYuzdeDC.Value = Math.Round((double)bilgiler.Where(x => x.BasariHarf == EnumHarfBasari.DC).Count() * 100 / topAdet, 2).ToString();
            txtYuzdeDD.Value = Math.Round((double)bilgiler.Where(x => x.BasariHarf == EnumHarfBasari.DD).Count() * 100 / topAdet, 2).ToString();
            txtYuzdeFF.Value = Math.Round((double)bilgiler.Where(x => x.BasariHarf == EnumHarfBasari.FF).Count() * 100 / topAdet, 2).ToString();
            txtYuzdeDZ.Value = Math.Round((double)bilgiler.Where(x => x.BasariHarf == EnumHarfBasari.DZ).Count() * 100 / topAdet, 2).ToString();
            txtYuzdeGR.Value = Math.Round((double)bilgiler.Where(x => x.BasariHarf == EnumHarfBasari.GR).Count() * 100 / topAdet, 2).ToString();

            txtAritmetikOrtalama.Value = Math.Round(_gelen.AritmetikOrtalama, 3).ToString();
            txtSatandartSapma.Value = Math.Round(_gelen.StandartSapma, 3).ToString();
            veri = Donustur(bilgiler);
            table1.DataSource = veri;
        }
        int paySayisi = 0;
        private DataTable Donustur(List<OgrenciPayveNotBilgileriDTO> bilgiler)
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn { Caption = "NUMARA", ColumnName = "NUMARA" });
            dataTable.Columns.Add(new DataColumn { Caption = "AD SOYAD", ColumnName = "ADSOYAD" });
            int say = 1;
            paySayisi = bilgiler.First().listPaylarVeNotlar.Count + 3;
            foreach (PaylarVeNotlarDTO item in bilgiler.First().listPaylarVeNotlar.OrderBy(d => d.EnumCalismaTip))
            {
                string colAd = item.Sira.ToString() + "." + PayKisaAd(item.EnumCalismaTip) + say;
                colAd = colAd.Replace(".", "x").Replace(" ", "");
                colAd = "abc" + say.ToString();
                string caption = item.Sira.ToString() + "." + PayKisaAd(item.EnumCalismaTip) + " " + Math.Round(item.Yuzde, 2).ToString() + "%";
                if (dataTable.Columns[colAd] == null)
                {
                    dataTable.Columns.Add(new DataColumn { Caption = caption, ColumnName = colAd });
                }
                say++;
            }
            dataTable.Columns.Add(new DataColumn { Caption = "M.Not", ColumnName = "MNot" });
            dataTable.Columns.Add(new DataColumn { Caption = "M.Harf", ColumnName = "MHarf" });
            dataTable.Columns.Add(new DataColumn { Caption = "Ba�ar�", ColumnName = "Basari" });


            foreach (OgrenciPayveNotBilgileriDTO item in bilgiler)
            {
                DataRow dr = dataTable.NewRow();

                List<string> satirDegerleri = new List<string>();
                satirDegerleri.Add(item.Numara.ToUpper());
                satirDegerleri.Add(item.AdSoyad.ToUpper());
                if (item.listPaylarVeNotlar != null)
                {
                    foreach (PaylarVeNotlarDTO pay in item.listPaylarVeNotlar.OrderBy(d => d.EnumCalismaTip))
                    {
                        if (pay.NotDeger.HasValue)
                        {
                            int nott = Convert.ToInt32(pay.NotDeger);
                            if (nott <= 100)
                            {
                                var tempNot = Math.Round(pay.NotDeger.Value, 2);
                                satirDegerleri.Add(tempNot.ToString());
                            }
                            else if (nott == 120)
                            {
                                satirDegerleri.Add("GR");
                            }
                            else if (nott == 130)
                            {
                                satirDegerleri.Add("DZ");
                            }
                        }
                        else
                        {
                            satirDegerleri.Add(" ");
                        }
                    }
                }
                if (item.MutlakNot <= 100)
                {
                    var tempNot = Math.Round(item.MutlakNot, 2);
                    satirDegerleri.Add(tempNot.ToString());
                }
                else if (item.MutlakNot == 120)
                {
                    satirDegerleri.Add("GR");
                }
                else if (item.MutlakNot == 130)
                {
                    satirDegerleri.Add("DZ");
                }
                //satirDegerleri.Add(item.MutlakNot.ToString());
                satirDegerleri.Add(item.MutlakHarf.ToString());
                satirDegerleri.Add(item.BasariHarf.ToString());
                dr.ItemArray = satirDegerleri.ToArray();
                dataTable.Rows.Add(dr);
            }
            return dataTable;
        }

        private string PayKisaAd(EnumCalismaTip tip)
        {
            string result = string.Empty;
            switch (tip)
            {
                case EnumCalismaTip.AraSinav:
                    result = "Ara Snv.";
                    break;
                case EnumCalismaTip.KisaSinav:
                    result = "K.Snv.";
                    break;
                case EnumCalismaTip.Odev:
                    result = "�dev";
                    break;
                case EnumCalismaTip.SozluSinav:
                    result = "S�z.Snv.";
                    break;
                case EnumCalismaTip.ProjeTasarim:
                    result = "Proj.Tas.";
                    break;
                case EnumCalismaTip.PerformansGoreviUygulama:
                    result = "K.Snv.";
                    break;
                case EnumCalismaTip.PerformanGoreviLabaratuvar:
                    result = "Perf.Gor.Lab.";
                    break;
                case EnumCalismaTip.PerformansGoreviAtolye:
                    result = "Perf.Gor.Atolye";
                    break;
                case EnumCalismaTip.PerformansGoreviSeminer:
                    result = "Perf.Gor.Atolye";
                    break;
                case EnumCalismaTip.PerformansGoreviAraziCalismasi:
                    result = "Perf.Gor.Arazi";
                    break;
                case EnumCalismaTip.YilIciOrtalamasi:
                    result = "Y�li�i Ort.";
                    break;
                case EnumCalismaTip.YilIcininBasariya:
                    result = "Y�li�i Ba�ar�";
                    break;
                case EnumCalismaTip.Final:
                    result = "Final";
                    break;
                case EnumCalismaTip.Butunleme:
                    result = "B�t�nleme";
                    break;
                default:
                    break;
            }
            return result;
        }

        private void table1_ItemDataBinding(object sender, EventArgs e)
        {
            try
            {
                double maxWidth = 19.6;
                double numaraWidth = 2.30;
                double notWidth = 1.3;
                if (paySayisi > 10)
                {
                    notWidth = 0.9;
                }
                if (paySayisi > 18)
                {
                    notWidth = 0.6;
                }
                if (paySayisi > 25)
                {
                    notWidth = 0.5;
                }
                table1.DataSource = veri;

                double toplamNotGenislik = notWidth * paySayisi;
                double isimWidth = maxWidth - toplamNotGenislik - numaraWidth;

                //create two HtmlTextBox items (one for header and one for data) which would be added to the items collection of the table
                Telerik.Reporting.HtmlTextBox textboxGroup;
                Telerik.Reporting.HtmlTextBox textBoxTable;

                //we do not clear the Rows collection, since we have a details row group and need to create columns only
                this.table1.ColumnGroups.Clear();
                this.table1.Body.Columns.Clear();
                this.table1.Body.Rows.Clear();

                int i = 0;
                this.table1.ColumnHeadersPrintOnEveryPage = true;

                int colonSay = 0;
                foreach (System.Data.DataColumn dc in veri.Columns)
                {
                    //Geni�likleri pay say�s�na g�re hesapla
                    double genislik = notWidth;
                    switch (colonSay)
                    {
                        case 0: genislik = numaraWidth; break;
                        case 1: genislik = isimWidth; break;
                        default: genislik = notWidth; break;
                    }
                    //Tablo gruplar�n� olu�tur
                    Telerik.Reporting.TableGroup tableGroupColumn = new Telerik.Reporting.TableGroup();
                    tableGroupColumn.Name = colonSay.ToString();
                    this.table1.ColumnGroups.Add(tableGroupColumn);
                    this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Unit.Cm(genislik)));

                    //Ba�l�k textboxlar�n� olu�tur
                    textboxGroup = new Telerik.Reporting.HtmlTextBox();
                    //Ba�l�k stilini de�i�tir.
                    textboxGroup.Style.BorderColor.Default = Color.Black;
                    textboxGroup.Style.BorderStyle.Default = BorderType.Solid;
                    textboxGroup.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
                    textboxGroup.Style.Font.Name = "Tahoma";
                    textboxGroup.Style.Font.Bold = true;
                    textboxGroup.Style.Padding.Left = new Unit(1);

                    textboxGroup.Value = dc.Caption.ToString();
                    textboxGroup.Size = new SizeU(Unit.Cm(genislik), Unit.Cm(0.5));
                    tableGroupColumn.ReportItem = textboxGroup;

                    //��erik textboxlar�n� olu�tur
                    textBoxTable = new Telerik.Reporting.HtmlTextBox();
                    textBoxTable.Style.BorderColor.Default = Color.Black;
                    textBoxTable.Style.BorderStyle.Default = BorderType.Solid;
                    textBoxTable.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
                    textBoxTable.Style.Font.Name = "Tahoma";
                    textBoxTable.Style.Padding.Left = new Unit(2);
                    if (colonSay > 1)
                    {
                        textBoxTable.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
                    }
                    if (dc.ColumnName == "Basari")
                    {
                        textBoxTable.Style.Font.Bold = true;
                    }
                    textBoxTable.Value = "=Fields." + dc.ColumnName;// +".ToString('$0.')";
                    textBoxTable.Size = new SizeU(Unit.Cm(genislik), Unit.Cm(0.5));
                    this.table1.Body.SetCellContent(0, i++, textBoxTable);
                    this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] { textBoxTable, textboxGroup });

                    colonSay++;
                }

            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }
    }
}