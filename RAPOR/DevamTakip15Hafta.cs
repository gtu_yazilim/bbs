namespace Sabis.Bolum.Rapor
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;
    using System.Data;
    using System.Collections;

    /// <summary>
    /// Summary description for DevamTakip5Hafta.
    /// </summary>
    public partial class DevamTakip15Hafta : Telerik.Reporting.Report
    {
        public DevamTakip15Hafta(string fakulte, string bolum, string dersAdi, string kod, string dersSaati, string ogretimUyesi, string ogretimTuru, string ogretimYili, string Donemi, IList datatable, string universiteAdi, string logoUrl)
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();
            table1.DataSource = datatable;
            txtBaslik.Value = universiteAdi.Replace("\\n", Environment.NewLine) + "\n" + fakulte + "\n" + bolum;
            txtDersAdi.Value = dersAdi;
            txtDonem.Value = Donemi;
            txtKoduSaati.Value = kod + " / " + dersSaati;
            txtOgretimTuru.Value = ogretimTuru;
            txtOgretimUyesi.Value = ogretimUyesi;
            txtOgretimYili.Value = ogretimYili;
            txtToplamSayi.Value = datatable.Count.ToString();
            pLogo.Value = logoUrl;

            //kod ile girmeyince ekranda görünmüyor
            textBox3.Value = "1";
            textBox49.Value = "2";
            textBox9.Value = "3";
            textBox11.Value = "4";
            textBox19.Value = "5";
            textBox26.Value = "6";
            textBox30.Value = "7";
            textBox32.Value = "8";
            textBox34.Value = "9";
            textBox36.Value = "10";
            textBox38.Value = "11";
            textBox40.Value = "12";
            textBox42.Value = "13";
            textBox44.Value = "14";
            textBox46.Value = "15";

        }
    }
}