﻿namespace Sabis.Bolum.Rapor
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;
    using System.Data;
    using System.Collections;
    using System.Linq;
    /// <summary>
    /// Summary description for YoklamaListe.
    /// </summary>
    public partial class YoklamaListe : Telerik.Reporting.Report
    {
        public YoklamaListe(string fakulte, string bolum, string dersAdi, string kod, DateTime dersSaati, int sure, int dersProgramiv2ID, string ogretimUyesi, string ogretimTuru, string ogretimYili, string Donemi, IList datatable, string qrCode, string universiteAdi, string logoUrl)
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();
            table1.DataSource = datatable;
            txtDersAdi.Value = dersAdi + " " + kod + " / " + ogretimYili + " " + Donemi;
            txtOgretimUyesi.Value = ogretimUyesi;
            txtToplamSayi.Value = datatable.Count.ToString();

            //qrCode = qrCode + "+DersProgramiv2ID=" + dersProgramiv2ID;
            barcode1.Value = qrCode;

            htmlTextBox1.Value = "NUMARA";
            htmlTextBox2.Value = "AD SOYAD";
            htmlTextBox3.Value = "İMZA";
            htmlTextBox4.Value = "İMZA";
            htmlTextBox5.Value = "AD SOYAD";
            htmlTextBox6.Value = "NUMARA";
            htmlTextBox7.Value = "İMZA";
            htmlTextBox8.Value = "AD SOYAD";
            htmlTextBox9.Value = "NUMARA";

            txtFakulteAdi.Value = fakulte;
            txtBolumAdi.Value = bolum + " - " + ogretimTuru;
            pLogo.Value = logoUrl;

            string saat = dersSaati.AddHours(sure).Hour.ToString() + ":00"; // + dersSaati.Minute.ToString();

            

            txtTarih.Value = dersSaati.ToString("dd.MM.yyyy HH:mm") + " - " + saat + " - (" + sure + " Ders Saati)";
        }
    }



}