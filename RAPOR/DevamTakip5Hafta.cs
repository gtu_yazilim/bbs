namespace Sabis.Bolum.Rapor
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;
    using System.Data;
    using System.Collections;
    using System.Linq;
    /// <summary>
    /// Summary description for DevamTakip5Hafta.
    /// </summary>
    public partial class DevamTakip5Hafta : Telerik.Reporting.Report
    {
        public DevamTakip5Hafta(string fakulte, string bolum, string dersAdi, string kod, string dersSaati, string ogretimUyesi, string ogretimTuru, string ogretimYili, string Donemi, IList datatable, string universiteAdi, string logoUrl)
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();
            table1.DataSource = datatable;
            txtBaslik.Value = universiteAdi.Replace("\\n", Environment.NewLine) + "\n" + fakulte + "\n" + bolum;
            txtDersAdi.Value = dersAdi;
            txtDonem.Value = Donemi;
            txtKoduSaati.Value = kod + " / " + dersSaati;
            txtOgretimTuru.Value = ogretimTuru;
            txtOgretimUyesi.Value = ogretimUyesi;
            txtOgretimYili.Value = ogretimYili;
            txtToplamSayi.Value = datatable.Count.ToString();
            pLogo.Value = logoUrl;

            textBox11.Value = ". . / . . / . . . .";
            textBox26.Value = ". . / . . / . . . .";
            textBox19.Value = ". . / . . / . . . .";
            textBox3.Value = ". . / . . / . . . .";
            textBox7.Value = ". . / . . / . . . .";
        }
    }
}