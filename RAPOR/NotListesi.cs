namespace Sabis.Bolum.Rapor
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using Telerik.Reporting.Drawing;
    using System.Data;
    using System.Collections;
    using System.Linq;
    using System.Collections.Generic;
    using Sabis.Bolum.Core;
    using Sabis.Bolum.Core.ABS.DEGERLENDIRME;
    using Sabis.Bolum.Core.ABS.NOT;
    using Sabis.Enum;
    using System.Text.RegularExpressions;

    public partial class NotListesi : Telerik.Reporting.Report
    {
        DataTable veri = new DataTable();

        public NotListesi(string fakulte, string bolum, string dersAdi, string kod, string dersSaati, string ogretimUyesi, string ogretimTuru, string ogretimYili, string Donemi, List<OgrenciListesiVM> bilgiler, string universiteAdi, string logoUrl)
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();
            var cultureInfo = new System.Globalization.CultureInfo("tr-TR");

            // Set the language for static text (i.e. column headings, titles)
            System.Threading.Thread.CurrentThread.CurrentUICulture = cultureInfo;

            // Set the language for dynamic text (i.e. date, time, money)
            System.Threading.Thread.CurrentThread.CurrentCulture = cultureInfo;
            //table1.DataSource = datatable;
            txtBaslik.Value = universiteAdi.Replace("\\n", Environment.NewLine) + "\n" + fakulte + "\n" + bolum;
            //txtBaslik.Value = Regex.Replace(universiteAdi, @"\t|\n|\r", "") + "\n" + fakulte + "\n" + bolum;
            //txtBaslik.Value = String.Join("", universiteAdi.Where(c => c != '\n' && c != '\r' && c != '\t')) + "\n" + fakulte + "\n" + bolum;


            txtDersAdi.Value = dersAdi;
            txtDonem.Value = Donemi;
            txtKoduSaati.Value = kod + " / " + dersSaati;
            txtOgretimTuru.Value = ogretimTuru;
            txtOgretimUyesi.Value = ogretimUyesi;
            txtOgretimUyesiAlt.Value = ogretimUyesi;
            txtOgretimYili.Value = ogretimYili + " - " + (Convert.ToInt32(ogretimYili)+1);
            txtToplamSayi.Value = bilgiler.Count.ToString();
            pLogo.Value = logoUrl;

            veri = Donustur(bilgiler);

            table1.DataSource = veri;
        }
        int paySayisi = 0;
        private DataTable Donustur(List<OgrenciListesiVM> bilgiler)
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn { Caption = "NUMARA", ColumnName = "NUMARA" });
            dataTable.Columns.Add(new DataColumn { Caption = "AD SOYAD", ColumnName = "ADSOYAD" });
            int say = 1;
            paySayisi = bilgiler.First().OgrenciNotListesi.Count;
            foreach (Sabis.Bolum.Core.ABS.NOT.OgrenciNotlariVM item in bilgiler.First().OgrenciNotListesi.OrderBy(d=>d.EnumCalismaTip).ToList())
            {
                string colAd = item.Sira.ToString() + "." + PayKisaAd((EnumCalismaTip)item.EnumCalismaTip) + say;
                colAd = colAd.Replace(".", "x").Replace(" ", "");
                colAd = "abc" + say.ToString();
                string caption = item.Sira.ToString() + "." + PayKisaAd((EnumCalismaTip)item.EnumCalismaTip);
                if (dataTable.Columns[colAd] == null)
                {
                    dataTable.Columns.Add(new DataColumn { Caption = caption, ColumnName = colAd });
                }
                say++;
            }



            foreach (OgrenciListesiVM item in bilgiler)
            {
                DataRow dr = dataTable.NewRow();

                List<string> satirDegerleri = new List<string>();
                satirDegerleri.Add(item.Numara.ToUpper());
                satirDegerleri.Add(item.Ad.ToUpper() + " " + item.Soyad.ToUpper());
                foreach (Sabis.Bolum.Core.ABS.NOT.OgrenciNotlariVM pay in item.OgrenciNotListesi.OrderBy(d => d.EnumCalismaTip))
                {
                    satirDegerleri.Add(pay.Notu);
                    //if (!string.IsNullOrEmpty(pay.Notu))
                    //{
                    //    int nott = Convert.ToInt32(pay.Notu);
                    //    if (nott <= 100)
                    //    {
                    //        satirDegerleri.Add(pay.Notu.ToString());
                    //    }
                    //    else if (nott == 120)
                    //    {
                    //        satirDegerleri.Add("GR");
                    //    }
                    //    else if (nott == 130)
                    //    {
                    //        satirDegerleri.Add("DZ");
                    //    }
                    //}
                    //else
                    //{
                    //    satirDegerleri.Add(" ");
                    //}
                }
                dr.ItemArray = satirDegerleri.ToArray();
                dataTable.Rows.Add(dr);
            }
            return dataTable;
        }

        private string PayKisaAd(EnumCalismaTip tip)
        {
            string result = string.Empty;
            switch (tip)
            {
                case EnumCalismaTip.AraSinav:
                    result = "Ara Snv.";
                    break;
                case EnumCalismaTip.KisaSinav:
                    result = "K.Snv.";
                    break;
                case EnumCalismaTip.Odev:
                    result = "�dev";
                    break;
                case EnumCalismaTip.SozluSinav:
                    result = "S�z.Snv.";
                    break;
                case EnumCalismaTip.ProjeTasarim:
                    result = "Proj.Tas.";
                    break;
                case EnumCalismaTip.PerformansGoreviUygulama:
                    result = "K.Snv.";
                    break;
                case EnumCalismaTip.PerformanGoreviLabaratuvar:
                    result = "Perf.Gor.Lab.";
                    break;
                case EnumCalismaTip.PerformansGoreviAtolye:
                    result = "Perf.Gor.Atolye";
                    break;
                case EnumCalismaTip.PerformansGoreviSeminer:
                    result = "Perf.Gor.Atolye";
                    break;
                case EnumCalismaTip.PerformansGoreviAraziCalismasi:
                    result = "Perf.Gor.Arazi";
                    break;
                case EnumCalismaTip.YilIciOrtalamasi:
                    result = "Y�li�i Ort.";
                    break;
                case EnumCalismaTip.YilIcininBasariya:
                    result = "Y�li�i Ba�ar�";
                    break;
                case EnumCalismaTip.Final:
                    result = "Final";
                    break;
                case EnumCalismaTip.Butunleme:
                    result = "B�t�nleme";
                    break;
                default:
                    break;
            }
            return result;
        }

        private void table1_ItemDataBinding(object sender, EventArgs e)
        {
            try
            {
                double maxWidth = 19.6;
                double numaraWidth = 2.30;
                double notWidth = 1;
                table1.DataSource = veri;

                double toplamNotGenislik = notWidth * paySayisi;
                double isimWidth = maxWidth - toplamNotGenislik - numaraWidth;

                //create two HtmlTextBox items (one for header and one for data) which would be added to the items collection of the table
                Telerik.Reporting.HtmlTextBox textboxGroup;
                Telerik.Reporting.HtmlTextBox textBoxTable;

                //we do not clear the Rows collection, since we have a details row group and need to create columns only
                this.table1.ColumnGroups.Clear();
                this.table1.Body.Columns.Clear();
                this.table1.Body.Rows.Clear();
                int i = 0;
                this.table1.ColumnHeadersPrintOnEveryPage = true;
                int colonSay = 0;
                foreach (DataColumn dc in veri.Columns)
                {
                    //Geni�likleri pay say�s�na g�re hesapla
                    double genislik = notWidth;
                    switch (colonSay)
                    {
                        case 0: genislik = numaraWidth; break;
                        case 1: genislik = isimWidth; break;
                        default: genislik = notWidth; break;
                    }
                    //Tablo gruplar�n� olu�tur
                    Telerik.Reporting.TableGroup tableGroupColumn = new Telerik.Reporting.TableGroup();
                    tableGroupColumn.Name = colonSay.ToString();
                    this.table1.ColumnGroups.Add(tableGroupColumn);
                    this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Unit.Cm(genislik)));

                    //Ba�l�k textboxlar�n� olu�tur
                    textboxGroup = new Telerik.Reporting.HtmlTextBox();
                    //Ba�l�k stilini de�i�tir.
                    textboxGroup.Style.BorderColor.Default = Color.Black;
                    textboxGroup.Style.BorderStyle.Default = BorderType.Solid;
                    textboxGroup.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
                    textboxGroup.Style.Font.Name = "Tahoma";
                    textboxGroup.Style.Font.Bold = true;
                    textboxGroup.Style.Padding.Left = new Unit(1);

                    textboxGroup.Value = dc.Caption.ToString();
                    textboxGroup.Size = new SizeU(Unit.Cm(genislik), Unit.Cm(0.5));
                    tableGroupColumn.ReportItem = textboxGroup;

                    //��erik textboxlar�n� olu�tur
                    textBoxTable = new Telerik.Reporting.HtmlTextBox();
                    textBoxTable.Style.BorderColor.Default = Color.Black;
                    textBoxTable.Style.BorderStyle.Default = BorderType.Solid;
                    textBoxTable.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
                    textBoxTable.Style.Font.Name = "Tahoma";
                    textBoxTable.Style.Padding.Left = new Unit(2);
                    if (colonSay > 1)
                    {
                        textBoxTable.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
                    }
                    textBoxTable.Value = "=Fields." + dc.ColumnName;// +".ToString('$0.')";
                    textBoxTable.Size = new SizeU(Unit.Cm(genislik), Unit.Cm(0.5));
                    this.table1.Body.SetCellContent(0, i++, textBoxTable);
                    this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] { textBoxTable, textboxGroup });

                    colonSay++;
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }
    }
}